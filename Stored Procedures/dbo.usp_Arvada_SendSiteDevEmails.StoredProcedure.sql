USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_SendSiteDevEmails]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[usp_Arvada_SendSiteDevEmails]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_SendSiteDevEmails]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_Arvada_SendSiteDevEmails]
	@TestMode int = 0
as
/**********************************************************************************************************************
* Creator:      	 
* Date:         	 
* Title:			 
* Description:  	 
* Examples:    

EXECUTE dbo.usp_Arvada_SendSiteDevEmails @TestMode = 1

update EmailSiteDevPermits
set EmailSent = 0

SELECT * 
FROM crw_reports.[dbo].[ExecutionLogStorage]
ORDER BY timestart DESC


select * 
from crw_Reports.dbo.[Subscriptions] a
inner join crw_reports.[dbo].[Catalog] c
	on c.ITemID = a.report_oid

UPDATE crw_reports.dbo.Subscriptions
SET OwnerID = (Select UserID from crw_reports.dbo.Users where UserName = 'ISYS\setup')
WHERE OwnerID = (Select UserID from Reportserver.dbo.Users where UserName = 'Domain\DeletedUserID')

--drop table EmailSiteDevPermits
create table dbo.EmailSiteDevPermits
(
	permit_no varchar(15) not null,
	InspectionRecordID varchar(30) not null,
	EmailToValue nvarchar(255) not null,
	EmailSent int not null constraint df_EmailSiteDevPermits_EmailSent default 0,
	DateSent datetime2,
	DateSentAttempted datetime2,
	LoadDate datetime2 not null constraint df_EmailSiteDevPermits_LoadDate default getdate(),
	LastOutcome varchar(500),
	rid int not null identity(1,1) 


	constraint pkc_EmailSiteDevPermits_permitno_RecID_Rid
		primary key clustered(Permit_no, InspectionRecordID, rid)
)

					 
* Modifications:  	
  Developer Name		Date		Description
  ------------------	----------	----------------------------------------------------------- 
*
**********************************************************************************************************************/

set nocount on
 
 --------------------------
 --- Declare Variables   --
 --------------------------
 DECLARE 
		@ReplaceEmail nvarchar(500),
		@ReplacePermit nvarchar(500),
		@ReplaceRecID nvarchar(500),
		@sql nvarchar(max),
		@s int,
		@e int,
		@skip int,
		@returnvalue int,
		@RecID varchar(30)
 --------------------------
 --- Create Temp Tables  --
 --------------------------
 
if object_id('tempdb.dbo.#SendEmail') is not null drop table #SendEmail
create table #SendEmail
(
	permitno varchar(30),
	recordid varchar(30),
	emailValue nvarchar(255),
	OrigRid int,
	Outcome varchar(500),
	rid int identity(1,1),
	ReplaceEmail nvarchar(500),
	ReplacePermit nvarchar(500),
	ReplaceInspID nvarchar(500),
	DateSentAttempted datetime2
)

if object_id('tempdb.dbo.#hold') is not null drop table #hold
create table #hold
(
	OrigParams XML,
	OrigEmail XML,
	UpdateParams XML,
	UpdateEmail XML,
	ReplaceEmail nvarchar(500),
	ReplacePermit nvarchar(500),
	ReplaceInspID nvarchar(500),
	SubscriptionID uniqueIdentifier
)

if object_id('tempdb.dbo.#OriginalSub') is not null drop table #OriginalSub
create table #OriginalSub
(
	OrigParams XML,
	OrigEmail XML,
	SubscriptionID uniqueIdentifier
)
	 
 --------------------------
 --- Body of Procedure   --
 --------------------------
 
insert into dbo.EmailSiteDevPermits
(
	permit_no,
	InspectionRecordID,
	EmailToValue,
	EmailSent
)
select distinct --removes dupe contacts, if necessary
	m.permit_no,
	i.recordid,
	pep.email,
	0 as emailSent
from dbo.permit_main m
inner join dbo.Permit_Inspections i
	on m.permit_no = i.permit_no
inner join dbo.permit_people pep
	on pep.PERMIT_NO = m.PERMIT_NO
	and pep.NAMETYPE = 'SWAMP ADMIN'
where
	m.permitType = 'SITE DEV'
and i.InspectionType in ('Routine','Follow up','Other-Site Dev','Final Site develop**')
and i.COMPLETED_DATE is not null --the inspection must be completed
and pep.email is not null --has to have an email
and pep.email != ''
--lets make sure we have not already or are planning to send this email for this inspection
and not exists(select 1
				from dbo.EmailSiteDevPermits sentEmail
				where sentEmail.InspectionRecordID = i.recordid
				and sentEmail.EmailToValue = pep.email
				)

--if we have not sent an email and we can no longer find the inspect or person, then cancel out of trying:
update a
set EmailSent = 2
from  dbo.EmailSiteDevPermits a
left join
	(
		select distinct --removes dupe contacts, if necessary
			m.permit_no,
			i.recordid,
			pep.email,
			0 as emailSent
		from dbo.permit_main m
		inner join dbo.Permit_Inspections i
			on m.permit_no = i.permit_no
		inner join dbo.permit_people pep
			on pep.PERMIT_NO = m.PERMIT_NO
			and pep.NAMETYPE = 'SWAMP ADMIN'
		where
			m.permitType = 'SITE DEV'
		and i.InspectionType in ('Routine','Follow up','Other-Site Dev','Final Site develop**')
		and i.COMPLETED_DATE is not null --the inspection must be completed
		and pep.email is not null --has to have an email
		and pep.email != ''
	) as b
	on b.permit_no = a.permit_no
	and b.recordid = a.InspectionRecordID
	and b.email = a.EmailToValue
where b.permit_no is null

--get our the original subscription info in case it gets junked up later

declare 
	@SubscriptionID uniqueIDentifier = '205B2E1B-91E9-45A4-A6CB-5ADE5B53BAA0',
	@SubscriptionIDVar varchar(50) = '205B2E1B-91E9-45A4-A6CB-5ADE5B53BAA0'

insert into #OriginalSub
(
	OrigEmail,
	OrigParams,
	SubscriptionID
	
)
select
	s.ExtensionSettings,
	s.[parameters],
	s.SubscriptionID
FROM crw_reports.[dbo].[Subscriptions] s
WHERE s.SubscriptionID = @SubscriptionID


--gather the list of emails we want to send
insert into #SendEmail
(
	permitno ,
	recordid ,
	emailValue,
	OrigRid
)
select
	permit_no,
	InspectionRecordID,
	EmailToValue,
	rid
from dbo.EmailSiteDevPermits
where EmailSent = 0
and LoadDate >= dateadd(day, -30, convert(Date, getdate()))

 


select 
	@s = min(rid),
	@e = max(rid)
from #SendEmail

while @s <= @e
begin
	
	--clear out temp table and skip variable for another loop through
	truncate table #hold
	set @skip = 0

	--reload our hold table, which is used so that we can do the xml modify in dynamic sql
	insert into #hold
	(
		OrigEmail,
		OrigParams,
		UpdateEmail,
		UpdateParams,
		SubscriptionID
	
	)
	select
		s.OrigEmail,
		s.OrigParams,
		s.OrigEmail,
		s.OrigParams,
		s.SubscriptionID
	FROM #OriginalSub  s

	--switch out the parameter string in our temp table to the new things:
	select 
		@ReplaceEmail = emailValue,
		@ReplacePermit = permitno,
		@ReplaceRecID = recordid
	from #SendEmail
	where rid = @s

	if @TestMode = 1
	begin
		if @s%2 = 0
			set @ReplaceEmail = 'jhutchens@arvada.org'
		else
			set @ReplaceEmail = 'jesse_hutchens@hotmail.com'
	end

	--just so we can verify in testing later we have variables set correctly:
	update s
	set 
		ReplaceEmail = @ReplaceEmail,
		ReplacePermit = @ReplacePermit,
		ReplaceInspID = @ReplaceRecID
	from #SendEmail s
	where rid = @s
	 
	 --actually modify the emails and parameters:
	set @sql = '
	update h
	set UpdateEmail.modify(''replace value of (/ParameterValues/ParameterValue/Value[1]/text())[1] with "'+@ReplaceEmail+'"''),
		UpdateParams.modify(''replace value of (/ParameterValues/ParameterValue/Value[1]/text())[1] with "'+@ReplacePermit+'"'')
	from #hold h'
	exec(@sql)

	set @sql = '
	update h
	set UpdateParams.modify(''replace value of (/ParameterValues/ParameterValue/Value[1]/text())[2] with "'+@ReplaceRecID+'"'')
	from #hold h'
	exec(@sql)

	--now actually update the subscription meta data so when the report runs, it runs with new email and permit and recid	
	UPDATE s
	SET [parameters] = CONVERT(NTEXT, CONVERT(NVARCHAR(MAX),h.UpdateParams)),
		ExtensionSettings = CONVERT(NTEXT, CONVERT(NVARCHAR(MAX),h.UpdateEmail)),
		[Description] = 'Send e-mail to ' + @ReplaceEmail,
		ModifiedDate = getdate()
	FROM crw_reports.[dbo].[Subscriptions] s
	inner join #hold h
		on h.SubscriptionID = s.SubscriptionID 

  
	if @@error != 0
	begin
		
		update s
		set Outcome = 'Parameter and email update failed'
		from #SendEmail s
		where rid = @s

		set @skip = 1
	end

	if @skip = 0
	begin

		EXEC @returnvalue = [CRW_Reports].dbo.AddEvent @EventType='TimedSubscription', @EventData=  @SubscriptionIDVar  --'205b2e1b-91e9-45a4-a6cb-5ade5b53baa0'


		select @RecID =  'SYS:'+ right(('00' + str(DATEPART(yy, GETDATE()))),2)
			+ right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)
			+ right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
			+ right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)
			+ right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
			+ right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)
			+ right(('0000000'+ ltrim(str(1))),4)

		if @returnvalue != 0
		begin
			update s
			set Outcome = isnull(Outcome,'') + case when outcome is not null then ', ' else '' end + 'Parameter and email update failed return value ' + convert(Varchar(100),@returnvalue),
				DateSentAttempted = getdate()
			from #SendEmail s
			where rid = @s

			Insert into Permit_Actions ( [PERMIT_NO], [ACTION_DATE], [ACTION_TYPE], [ACTION_BY], [RECORDID], [completed_Date]) 
			Values ( @ReplacePermit,  getdate(), 'EMAIL', 'System User', @RecID, getdate())

			exec tsp_GblAENotes @NoteID='0',
			@UserID='SYS',
			@ActivityGroup='PERMIT',
			@ActivityNo=@ReplacePermit,
			@SubGroup='ACTION',
			@SubGroupRecordID=@RecID,
			@Notes='Email of Site Development Inspection Report Error'

		end
		else
		begin
			update s
			set Outcome = 'Success',
				DateSentAttempted = getdate()
			from #SendEmail s
			where rid = @s



			Insert into Permit_Actions ( [PERMIT_NO], [ACTION_DATE], [ACTION_TYPE], [ACTION_BY], [RECORDID], [completed_Date]) 
			Values ( @ReplacePermit,  getdate(), 'EMAIL', 'System User', @RecID, getdate())

			exec tsp_GblAENotes @NoteID='0',
			@UserID='SYS',
			@ActivityGroup='PERMIT',
			@ActivityNo=@ReplacePermit,
			@SubGroup='ACTION',
			@SubGroupRecordID=@RecID,
			@Notes='Email of Site Development Inspection Report Success'
		end


	end
	
	--wait 10 seconds before sending out the next email, this seems to allow SQL Server to recognize param and email changes. 
	waitfor delay '00:00:15'
	set @s = @s + 1

end

 
--now we have looped through and got our errors/successes update the live table:
update a
set
	EmailSent = case when s.outcome = 'Success' then 1 else EmailSent end,
	DateSent = case when s.outcome = 'Success' then s.DateSentAttempted else DateSent end,
	DateSentAttempted = s.DateSentAttempted,
	LastOutcome = s.Outcome
from dbo.EmailSiteDevPermits a
inner join #SendEmail s
	on a.rid = s.origRid
GO
