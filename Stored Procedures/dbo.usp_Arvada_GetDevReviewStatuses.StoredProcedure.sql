USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_GetDevReviewStatuses]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[usp_Arvada_GetDevReviewStatuses]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_GetDevReviewStatuses]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_Arvada_GetDevReviewStatuses]

as 
begin

	create table #status
	(
		status varchar(50),
		orderid int
	)

	insert into #status
	(
		status,
		orderid
	)
	Select [status], orderid
	from Prmry_Status 
	WHERE GROUPNAME = 'PROJECTS' 
	and status in 
		(
		'1ST REVIEW','2ND REVIEW','DECISION REVIEW','COMPLETENESS REVIEW', 
		'UNDER REVIEW', 'RESPONSE PEND',  'APPLIED','ON HOLD','CUSTOMER RESP. PEND','CONDITIONAL APPROVAL','PH PENDING',
	
		'1ST REVIEW',
		'CUSTOMER RESP. PEND',
		'2ND REVIEW',
		'2ND CUSTOMER RESP',
		'FULL CUSTOMER RESP',
		'DECISION REVIEW',
		'APP WITH COND',
		'APPLIED APPROVE-NO CONSTR',
		'APPROVED',
		'CLOSED',
		'COMPLETENESS REVIEW',
		'DENIED',
		'LAPSED',
		'ON HOLD PH PENDING',
		'REJECTED',
		'SCHEDULED',
		'UNDER CONST',
		'UNDER REVIEW',
		'VOID',
		'WITHDRAWN'
		
		)



	insert into #status
	select 
		'',
		(select max(orderid)+1 from #status)


	select
		status
	from #status
	order by 
		orderid



end
GO
