USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_t9_BuildChecklists]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_t9_BuildChecklists]
GO
/****** Object:  StoredProcedure [dbo].[tsp_t9_BuildChecklists]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

		create Procedure [dbo].[tsp_t9_BuildChecklists]
       @List as [CheckListBuildType] readonly
as
begin 
              

       Declare @CheckListValue table(NewCheckListValueID int null, itemText varchar(300) Null)
       
       
       Insert into ChecklistItems(itemText, ChecklistGroupType, active) 
       output Inserted.CHECKLISTID  as NewCheckListValueID, inserted.ItemText into @CheckListValue                     
       Select ItemText, 'NON-MASTER', 1 from @List  where CheckListID = 0 and Active = 99 and ItemText is not null And itemTExt <> ''

       Insert into CheckLists (ChecKlistItemID, ItemOrder, GroupID, GroupType, SubGroupID, SubGroupType,Active)
       Select NewCheckListValueID, ItemOrder, GroupID, GroupType, SubGroupID, SubGroupType,Active from @List L
       Join @CheckListValue cv on cv.itemText = L.ItemText
       where CheckListID = 0 and Active = 99    and L.ItemText is not null And L.itemTExt <> ''
              
       

       Insert into Checklists(ChecKlistItemID, ItemOrder, GroupID, GroupType, SubGroupID, SubGroupType,Active)
       Select ChecKlistItemID, ItemOrder, GroupID, GroupType, SubGroupID, SubGroupType,Active from @List where CheckListID = 0 and Active <> 2
       AND (ITEMTEXT IS NOT NULL OR ITEMTEXT <> '')

       Update C Set C.ItemOrder = L.ItemOrder From CheckLists C Join @List L on C.CheckLIstID = L.CheckListID and C.Active = 1 and C.ChecklistID <> 0
	   Update CLI Set CLI.ItemText = L.ItemText From CheckListItems CLI Join @List L on CLI.CheckLIstID = L.CheckListItemID and CLI.Active = 1 and CLI.ChecklistID <> 0

                           
      --delete from master list the single create items
          Delete C from CheckListItems C Where C.CheckListID =  
              (Select top 1 CL.CheckListItemID from CheckLists CL 
                     Join @List L on L.CheckListID = CL.ChecklistID and L.Active = 2)
              and C.CheckListGroupType = 'NON-MASTER' 

       --Delete items
       Delete C from Checklists C Join  @List L on L.CheckListID = C.ChecklistID And L.Active = 2

       --if there is a delete all run that
       Delete C from checklists C Join @List L on L.GroupID = C.groupID and L.SubGroupID = C.subGroupID and L.SubGroupType = C.subGroupType and L.GroupType = C.groupType 
       And L.Active = 3

	   	   --separate delete all when both grouptype and subgrouptype = ALL
	   Delete C from checklists C Join @List L on L.GroupID = C.groupID and L.SubGroupID = C.subGroupID  And L.Active = 4

End

GO
