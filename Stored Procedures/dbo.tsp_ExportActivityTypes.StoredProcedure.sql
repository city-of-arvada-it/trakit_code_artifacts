USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ExportActivityTypes]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_ExportActivityTypes]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ExportActivityTypes]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  
					  					  					  /*
tsp_ExportActivityTypes @DBName='DEV', @FilePath='C:\CRW\iTrakitExports\', @FileName='ActivityTypesExport'

*/

CREATE Procedure [dbo].[tsp_ExportActivityTypes](
@DBName Varchar(50),
@FilePath Varchar(1000),
@FileName Varchar(150),
@ToggleXP_CMDShell INT = 1
)

As

SET NOCOUNT ON

Declare @SQL Varchar(8000)

If @ToggleXP_CMDShell Is Null Set @ToggleXP_CMDShell = 1

IF EXISTS (SELECT * FROM tempdb.sys.objects WHERE object_id = OBJECT_ID(N'tempdb..ActivityTypes') AND type in (N'U'))
	Drop Table tempdb..ActivityTypes

Create Table tempdb..ActivityTypes(GroupName Varchar(200),TypeName Varchar(200),[Description] Varchar(200),OwnerDept Varchar(200),Account Varchar(200),ATTACHMENT_LOCK Varchar(200),ORDERID Varchar(200))


Insert Into tempdb..ActivityTypes(GroupName,TypeName,[Description],OwnerDept,Account,ATTACHMENT_LOCK,ORDERID)
Values('GroupName','TypeName','Description','OwnerDept','Account','ATTACHMENT_LOCK','ORDERID')

Set @SQL = '
Insert Into tempdb..ActivityTypes([GroupName],[TypeName],[Description],[OwnerDept],[Account],[ATTACHMENT_LOCK],[ORDERID])
Select 
GroupName=CASE WHEN GroupName = '''' THEN Null ELSE GroupName END,
TypeName=CASE WHEN TypeName = '''' THEN Null ELSE REPLACE(REPLACE(REPLACE(TypeName,CHAR(9),''''),CHAR(10),''''),CHAR(13),'''') END,
[Description]=CASE WHEN [Description] = '''' THEN Null ELSE REPLACE(REPLACE(REPLACE([Description],CHAR(9),''''),CHAR(10),''''),CHAR(13),'''') END,
OwnerDept=CASE WHEN OwnerDept = '''' THEN Null ELSE REPLACE(REPLACE(REPLACE(OwnerDept,CHAR(9),''''),CHAR(10),''''),CHAR(13),'''') END,
Account=CASE WHEN Account = '''' THEN Null ELSE REPLACE(REPLACE(REPLACE(Account,CHAR(9),''''),CHAR(10),''''),CHAR(13),'''') END,
ATTACHMENT_LOCK=CASE WHEN ATTACHMENT_LOCK = '''' THEN Null ELSE ATTACHMENT_LOCK END,
ORDERID=CASE WHEN ORDERID Is Null THEN ''999'' ELSE Convert(Varchar,ORDERID) END
From ' + @DBName + '..Prmry_Types
ORDER BY GroupName,ORDERID'
--Print(@SQL)
Exec(@SQL)


Declare @vDate Varchar(100)
Declare @OutputFileName Varchar(1000)

Set @vDate = Replace(Convert(Varchar,GetDate(),102),'.','_')
Print(@vDate)

--Set @OutputFileName = '"' + @FilePath + @FileName + '_' + @vDate + '.csv"'
Set @OutputFileName = '"' + @FilePath + @FileName + '.csv"'


If @ToggleXP_CMDShell = 1
 Begin
	--Enable xp_cmdshell
	Set @SQL = '
	EXEC sp_configure ''show advanced options'', 1
	RECONFIGURE
	EXEC sp_configure ''xp_cmdshell'', 1
	RECONFIGURE
	'
	Print(@SQL)
	Exec(@SQL)
 End

select @sql = 'bcp "select * from tempdb..ActivityTypes" queryout ' + @OutputFileName + ' -c -t"|" -T -S'
+ @@servername
--Print(@SQL)
exec master..xp_cmdshell @sql

Drop Table tempdb..ActivityTypes



Set @SQL = '
DELETE ' + @DBName + '.dbo.iTrakitFeeds Where [FileName] = ''' + @FileName + '.csv''

INSERT INTO ' + @DBName + '.dbo.iTrakitFeeds([filename],[data])
SELECT ''' + @FileName + '.csv'', * FROM OPENROWSET(
  BULK N''' + REPLACE(@OutputFileName,'"','') + ''', SINGLE_BLOB
) bt'
Print(@SQL)
Exec(@SQL)

If @ToggleXP_CMDShell = 1
 Begin
	-- Disable xp_cmdshell
	Set @SQL = '
	EXEC sp_configure ''xp_cmdshell'', 0
	RECONFIGURE
	EXEC sp_configure ''show advanced options'', 0
	RECONFIGURE
	'
	Print(@SQL)
	Exec(@SQL)
 End
 
 
/*
tsp_ExportActivityTypes @DBName='DEV', @FilePath='C:\CRW\iTrakitExports\', @FileName='ActivityTypesExport'

*/






GO
