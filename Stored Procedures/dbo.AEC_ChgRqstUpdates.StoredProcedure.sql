USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[AEC_ChgRqstUpdates]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[AEC_ChgRqstUpdates]
GO
/****** Object:  StoredProcedure [dbo].[AEC_ChgRqstUpdates]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[AEC_ChgRqstUpdates] AS
----------------------------------------------------------------------------------
-- Interface: dbo.AEC_ChgRqstUpdates 
-- Database: SQL Server 2008r2
-- Client: Arvada, CO
-- Created By: Nikoel Carter
-- Created Date: 6/1/2015
----------------------------------------------------------------------------------
Declare

-- Source Variables
@szStLicNo		VARCHAR(20),
@dtStLicExp		DATETIME,
@szCompany		VARCHAR(100),
@szAECType		VARCHAR(60),
@szSubType		VARCHAR(60),
@szLicGrade1	VARCHAR(50),
@szLicGrade2	VARCHAR(50),
@szMatch2		VARCHAR(50),
@szLicGrade3	VARCHAR(50),
@szMatch3		VARCHAR(50),
@szLicGrade4	VARCHAR(50),
@szMatch4		VARCHAR(50),
@szLicGrade5	VARCHAR(50),
@szMatch5		VARCHAR(50),
@szLicGrade6	VARCHAR(50),
@szMatch6		VARCHAR(50),
@szAssocRec1	VARCHAR(50),
@szAssocRec2	VARCHAR(50),
@szAssocRec3	VARCHAR(50),
@szAssocRec4	VARCHAR(50),
@szAssocRec5	VARCHAR(50),
@szAssocRec6	VARCHAR(50),
@szAssocRec7	VARCHAR(50),
@szAssocRec8	VARCHAR(50),
@szAssocRec9	VARCHAR(50),
@szAssocRec10	VARCHAR(50),

-- Procedural Variables
@lCounter		INT,
@lNewAECCounter	INT,
@szRecordid		VARCHAR(50),
@szAECRecID		VARCHAR(50),
@szNotes		VARCHAR(2000),
@dtStLicIss		DATETIME,
@szNewStLicNo	VARCHAR(50),
@szNeedUpdtLicNoID	VARCHAR(50),
@szAECPath		VARCHAR(50),
@szCopy			VARCHAR(3000),
@szRename		VARCHAR(1000),
@szFileName		VARCHAR(200)

--EXEC sp_configure 'show advanced options', 1
--GO
---- To update the currently configured value for advanced options.
--RECONFIGURE
--GO
---- To enable the feature.
--EXEC sp_configure 'xp_cmdshell', 1
--GO
---- To update the currently configured value for this feature.
--RECONFIGURE
--GO

		SELECT @lCounter = 1, @lNewAECCounter = 2500, @szAECPath = 'c:\CRW\Data\Attachments\AEC\'

--CREATE TABLE ResultSet (Directory varchar(200))

--INSERT INTO ResultSet
--EXEC master.dbo.xp_subdirs 'c:\CRW\Data\Attachments\AEC\'

--STEP 1 IN CHANGE REQUEST
update aec_main set PARENT_AEC_ST_LIC_NO = NULL
delete from aec_main where substring(st_lic_no,1,3) = 'cmp'
delete from aec_udf where substring(st_lic_no,1,3) = 'cmp'
delete from aec_fees where substring(st_lic_no,1,3) = 'cmp'
delete from aec_insurance where substring(st_lic_no,1,3) = 'cmp'
delete from aec_subfees where substring(st_lic_no,1,3) = 'cmp'
delete from aec_actions where substring(st_lic_no,1,3) = 'cmp'
delete from aec_people where substring(st_lic_no,1,3) = 'cmp'

--CLEANUP REDUNDANT LICENSE #'S
--update masterforreview set match2 = NULL WHERE st_lic_no in (select st_lic_no from aec_main) and match2 is not null and match2 in (select st_lic_no from masterforreview)
--update masterforreview set match3 = NULL WHERE st_lic_no in (select st_lic_no from aec_main) and MATCH3 is not null and MATCH3 in (select st_lic_no from masterforreview)
--update masterforreview set match4 = NULL WHERE st_lic_no in (select st_lic_no from aec_main) and MATCH4 is not null and MATCH4 in (select st_lic_no from masterforreview)
--update masterforreview set match5 = NULL WHERE st_lic_no in (select st_lic_no from aec_main) and match5 is not null and match5 in (select st_lic_no from masterforreview)
--update masterforreview set match6 = NULL WHERE st_lic_no in (select st_lic_no from aec_main) and match6 is not null and match6 in (select st_lic_no from masterforreview)
update masterforreview set assoc_record1 = NULL WHERE st_lic_no in (select st_lic_no from aec_main) and assoc_record1 is not null and assoc_record1 in (select st_lic_no from masterforreview)
update masterforreview set assoc_record2 = NULL WHERE st_lic_no in (select st_lic_no from aec_main) and assoc_record2 is not null and assoc_record2 in (select st_lic_no from masterforreview)
update masterforreview set assoc_record3 = NULL WHERE st_lic_no in (select st_lic_no from aec_main) and assoc_record3 is not null and assoc_record3 in (select st_lic_no from masterforreview)
update masterforreview set assoc_record4 = NULL WHERE st_lic_no in (select st_lic_no from aec_main) and assoc_record4 is not null and assoc_record4 in (select st_lic_no from masterforreview)
update masterforreview set assoc_record4 = NULL WHERE st_lic_no in (select st_lic_no from aec_main) and assoc_record5 is not null and assoc_record5 in (select st_lic_no from masterforreview)
update masterforreview set assoc_record4 = NULL WHERE st_lic_no in (select st_lic_no from aec_main) and assoc_record6 is not null and assoc_record6 in (select st_lic_no from masterforreview)
update masterforreview set assoc_record4 = NULL WHERE st_lic_no in (select st_lic_no from aec_main) and assoc_record7 is not null and assoc_record7 in (select st_lic_no from masterforreview)
update masterforreview set assoc_record4 = NULL WHERE st_lic_no in (select st_lic_no from aec_main) and assoc_record8 is not null and assoc_record8 in (select st_lic_no from masterforreview)
update masterforreview set assoc_record4 = NULL WHERE st_lic_no in (select st_lic_no from aec_main) and assoc_record9 is not null and assoc_record9 in (select st_lic_no from masterforreview)
update masterforreview set assoc_record4 = NULL WHERE st_lic_no in (select st_lic_no from aec_main) and assoc_record10 is not null and assoc_record10 in (select st_lic_no from masterforreview)

update masterforreview set assoc_record6 = NULL where st_lic_no in (select st_lic_no from aec_main) and assoc_record10 is not null and assoc_record10 = assoc_record9
update masterforreview set assoc_record6 = NULL where st_lic_no in (select st_lic_no from aec_main) and assoc_record9 is not null and assoc_record9 = assoc_record8
update masterforreview set assoc_record6 = NULL where st_lic_no in (select st_lic_no from aec_main) and assoc_record8 is not null and assoc_record8 = assoc_record7
update masterforreview set assoc_record6 = NULL where st_lic_no in (select st_lic_no from aec_main) and assoc_record7 is not null and assoc_record7 = assoc_record6
update masterforreview set assoc_record6 = NULL where st_lic_no in (select st_lic_no from aec_main) and assoc_record6 is not null and assoc_record6 = assoc_record5
update masterforreview set assoc_record5 = NULL where st_lic_no in (select st_lic_no from aec_main) and assoc_record5 is not null and assoc_record5 = assoc_record4
update masterforreview set assoc_record4 = NULL where st_lic_no in (select st_lic_no from aec_main) and assoc_record4 is not null and assoc_record4 = assoc_record3
update masterforreview set assoc_record3 = NULL where st_lic_no in (select st_lic_no from aec_main) and assoc_record3 is not null and assoc_record3 = assoc_record2
update masterforreview set assoc_record2 = NULL where st_lic_no in (select st_lic_no from aec_main) and assoc_record2 is not null and assoc_record2 = assoc_record1
update masterforreview set assoc_record1 = NULL where st_lic_no in (select st_lic_no from aec_main) and assoc_record1 is not null and assoc_record1 = st_lic_no

Begin

	-- Grab Source records for updates
	DECLARE cGetMasterForReview CURSOR LOCAL FOR        
	SELECT ST_LIC_NO, COMPANY, AECTYPE, AECSUBTYPE, LIC_GRADE_1, LIC_GRADE_2, MATCH2, LIC_GRADE_3, MATCH3, LIC_GRADE_4,
		   MATCH4, LIC_GRADE_5, MATCH5, LIC_GRADE_6, MATCH6, ASSOC_RECORD1, ASSOC_RECORD2, ASSOC_RECORD3, ASSOC_RECORD4,
		   ASSOC_RECORD5, ASSOC_RECORD6, ASSOC_RECORD7, ASSOC_RECORD8, ASSOC_RECORD9, ASSOC_RECORD10
	FROM MasterForReview
	--where st_lic_no = '2b-1088'
					
	Open cGetMasterForReview
	Fetch Next From cGetMasterForReview Into @szStLicNo, @szCompany, @szAECType, @szSubType, @szLicGrade1, @szLicGrade2, @szMatch2, @szLicGrade3, @szMatch3, @szLicGrade4,
											 @szMatch4, @szLicGrade5, @szMatch5, @szLicGrade6, @szMatch6, @szAssocRec1, @szAssocRec2, @szAssocRec3, @szAssocRec4,
											 @szAssocRec5, @szAssocRec6, @szAssocRec7, @szAssocRec8, @szAssocRec9, @szAssocRec10
	While @@Fetch_status <> -1 
	Begin
		IF SUBSTRING(@szStLicNo,1,3) = 'AEC'
			SELECT @szAECRecID = RECORDID FROM AEC_MAIN WHERE ST_LIC_NO = @szStLicNo

		IF @szAECRecID IS NOT NULL AND @szAECRecID <> ''
		BEGIN
			UPDATE AEC_MAIN SET COMPANY = @szCompany, LIC_GRADE_1 = @szLicGrade1, AECTYPE = @szAECType, AECSUBTYPE = @szSubType WHERE RECORDID = @szAECRecID
			IF @szMatch2 IS NOT NULL AND @szMatch2 <> ''
				UPDATE AEC_MAIN SET LIC_GRADE_2 = @szLicGrade2 WHERE RECORDID = @szAECRecID
			IF @szMatch2 IS NOT NULL AND @szMatch2 <> '' AND ISNULL(@szMatch2,' ') <> @szStLicNo
			BEGIN
					-- Insert action record to log merge activity
					-- Set unique RecordID
					SET @szRecordID = NULL
					-- Reset counter for unique number string at end of RecordID
					SET @lCounter = @lCounter + 1
					SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
						+ right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)
						+ right(('000000'+ ltrim(str(@lCounter))),6)

					INSERT INTO AEC_ACTIONS (RECORDID, ST_LIC_NO, ACTION_TYPE, ACTION_DATE, COMPLETED_DATE)
										VALUES (@szRecordID, @szStLicNo, 'MERGED RECORD '+@szMatch2, GETDATE(), GETDATE())

					SELECT @dtStLicIss = ST_LIC_ISSUE, @dtStLicExp = ST_LIC_EXPIRE FROM AEC_MAIN WHERE ST_LIC_NO = @szMatch2
					SET @szNotes = 'MERGED AEC RECORD: ' + @szMatch2
					IF @dtStLicIss IS NOT NULL AND @dtStLicIss <> ''
						SET @szNotes = @szNotes + '   ISSUED:  ' + CAST(@dtStLicIss AS VARCHAR)
					IF @dtStLicExp IS NOT NULL AND @dtStLicExp <> ''
						SET @szNotes = @szNotes + '   EXPIRED:  ' + CAST(@dtStLicExp AS VARCHAR)

					INSERT INTO PRMRY_NOTES (ACTIVITYGROUP, ACTIVITYNO, ACTIVITYRECORDID, SUBGROUP, SUBGROUPRECORDID, NOTES, DATEENTERED)
										VALUES ('AEC', @szStLicNo, @szAECRecID, 'ACTION', @szRecordID, @szNotes, GETDATE())

				-- update module and other associated records with new merged aec st_lic_no
				UPDATE AEC_FEES SET ST_LIC_NO = @szStLicNo WHERE ST_LIC_NO = @szMatch2
				UPDATE AEC_SUBFEES SET ST_LIC_NO = @szStLicNo WHERE ST_LIC_NO = @szMatch2
				UPDATE AEC_ACTIONS SET ST_LIC_NO = @szStLicNo WHERE ST_LIC_NO = @szMatch2

				IF (SELECT COUNT(*) FROM AEC_ATTACHMENTS WHERE ST_LIC_NO = @szMatch2) > 0
				BEGIN
						-- Get files
						DECLARE cGetFiles CURSOR LOCAL FOR        
						SELECT PATHNAME
						FROM aec_attachments
						WHERE ST_LIC_NO = @szMatch2
						Open cGetFiles
						Fetch Next From cGetFiles Into @szFileName
						While @@Fetch_status <> -1 
						Begin
							-- Copy files
							SET @szCopy = NULL
							SET @szCopy = 'copy ' + @szAECPath + @szMatch2 + '\"' + @szMatch2 + ' ' + @szFileName + '"' + ' ' + @szAECPath + @szStLicNo
							exec master..xp_cmdshell @szCopy, NO_OUTPUT
							-- Delete files
							SET @szCopy = NULL
							SET @szCopy = 'del ' + @szAECPath + @szMatch2 + '\"' + @szMatch2 + ' ' + @szFileName + '"'
							exec master..xp_cmdshell @szCopy, NO_OUTPUT

						Fetch Next From cGetFiles Into @szFileName
						End --While
						Close cGetFiles
						Deallocate cGetFiles

						UPDATE AEC_ATTACHMENTS SET ST_LIC_NO = @szStLicNo WHERE ST_LIC_NO = @szMatch2
				END

				UPDATE PERMIT_PEOPLE SET BUS_LICENSE = @szStLicNo, ID = 'C:'+ @szStLicNo WHERE SUBSTRING(ID,3,20) = @szMatch2 OR ISNULL(BUS_LICENSE,' ') = @szMatch2
				UPDATE GEO_PEOPLE SET BUS_LICENSE = @szStLicNo, ID = 'C:'+ @szStLicNo WHERE SUBSTRING(ID,3,20) = @szMatch2 OR ISNULL(BUS_LICENSE,' ') = @szMatch2
				UPDATE PROJECT_PEOPLE SET BUS_LICENSE = @szStLicNo, ID = 'C:'+ @szStLicNo WHERE SUBSTRING(ID,3,20) = @szMatch2 OR ISNULL(BUS_LICENSE,' ') = @szMatch2
			END

			IF @szMatch3 IS NOT NULL AND @szMatch3 <> ''
				UPDATE AEC_MAIN SET LIC_GRADE_3 = @szLicGrade3 WHERE RECORDID = @szAECRecID
			IF @szMatch3 IS NOT NULL AND @szMatch3 <> '' AND ISNULL(@szMatch3,' ') <> @szStLicNo
			BEGIN
				    -- Insert action record to log merge activity
					-- Set unique RecordID
					SET @szRecordID = NULL
					-- Reset counter for unique number string at end of RecordID
					SET @lCounter = @lCounter + 1
					SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
						+ right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)
						+ right(('000000'+ ltrim(str(@lCounter))),6)

					INSERT INTO AEC_ACTIONS (RECORDID, ST_LIC_NO, ACTION_TYPE, ACTION_DATE, COMPLETED_DATE)
										VALUES (@szRecordID, @szStLicNo, 'MERGED RECORD '+@szMatch3, GETDATE(), GETDATE())

					SET @szNotes = NULL
					SELECT @dtStLicIss = ST_LIC_ISSUE, @dtStLicExp = ST_LIC_EXPIRE FROM AEC_MAIN WHERE ST_LIC_NO = @szMatch3
					SET @szNotes = 'MERGED AEC RECORD: ' + @szMatch3
					IF @dtStLicIss IS NOT NULL AND @dtStLicIss <> ''
						SET @szNotes = @szNotes + '   ISSUED:  ' + CAST(@dtStLicIss AS VARCHAR)
					IF @dtStLicExp IS NOT NULL AND @dtStLicExp <> ''
						SET @szNotes = @szNotes + '   EXPIRED:  ' + CAST(@dtStLicExp AS VARCHAR)

					INSERT INTO PRMRY_NOTES (ACTIVITYGROUP, ACTIVITYNO, ACTIVITYRECORDID, SUBGROUP, SUBGROUPRECORDID, NOTES, DATEENTERED)
										VALUES ('AEC', @szStLicNo, @szAECRecID, 'ACTION', @szRecordID, @szNotes, GETDATE())

				-- update module and other associated records with new merged aec st_lic_no
				UPDATE AEC_FEES SET ST_LIC_NO = @szStLicNo WHERE ST_LIC_NO = @szMatch3
				UPDATE AEC_SUBFEES SET ST_LIC_NO = @szStLicNo WHERE ST_LIC_NO = @szMatch3
				UPDATE AEC_ACTIONS SET ST_LIC_NO = @szStLicNo WHERE ST_LIC_NO = @szMatch3

				IF (SELECT COUNT(*) FROM AEC_ATTACHMENTS WHERE ST_LIC_NO = @szMatch3) > 0
				BEGIN
							  -- Get files
							  DECLARE cGetFiles CURSOR LOCAL FOR        
							  SELECT PATHNAME
								FROM aec_attachments
								WHERE ST_LIC_NO = @szMatch3
							  Open cGetFiles
							  Fetch Next From cGetFiles Into @szFileName
							  While @@Fetch_status <> -1 
							  Begin
									-- Copy files
									SET @szCopy = NULL
									SET @szCopy = 'copy ' + @szAECPath + @szMatch3 + '\"' + @szMatch3 + ' ' +@szFileName + '"' + ' ' + @szAECPath + @szStLicNo
									exec master..xp_cmdshell @szCopy, NO_OUTPUT
									-- Delete files
									SET @szCopy = NULL
									SET @szCopy = 'del ' + @szAECPath + @szMatch3 + '\"' + @szMatch3 + ' ' + @szFileName + '"'
									exec master..xp_cmdshell @szCopy, NO_OUTPUT

							  Fetch Next From cGetFiles Into @szFileName
							  End --While
							  Close cGetFiles
							  Deallocate cGetFiles

						UPDATE AEC_ATTACHMENTS SET ST_LIC_NO = @szStLicNo WHERE ST_LIC_NO = @szMatch3
				END

				UPDATE PERMIT_PEOPLE SET BUS_LICENSE = @szStLicNo, ID = 'C:'+ @szStLicNo WHERE SUBSTRING(ID,3,20) = @szMatch3 OR ISNULL(BUS_LICENSE,' ') = @szMatch3
				UPDATE GEO_PEOPLE SET BUS_LICENSE = @szStLicNo, ID = 'C:'+ @szStLicNo WHERE SUBSTRING(ID,3,20) = @szMatch3 OR ISNULL(BUS_LICENSE,' ') = @szMatch3
				UPDATE PROJECT_PEOPLE SET BUS_LICENSE = @szStLicNo, ID = 'C:'+ @szStLicNo WHERE SUBSTRING(ID,3,20) = @szMatch3 OR ISNULL(BUS_LICENSE,' ') = @szMatch3
			END

			IF @szMatch4 IS NOT NULL AND @szMatch4 <> ''
				UPDATE AEC_MAIN SET LIC_GRADE_4 = @szLicGrade4 WHERE RECORDID = @szAECRecID
			IF @szMatch4 IS NOT NULL AND @szMatch4 <> '' AND ISNULL(@szMatch4,' ') <> @szStLicNo
			BEGIN
				-- Insert action record to log merge activity
					-- Set unique RecordID
					SET @szRecordID = NULL
					-- Reset counter for unique number string at end of RecordID
					SET @lCounter = @lCounter + 1
					SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
						+ right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)
						+ right(('000000'+ ltrim(str(@lCounter))),6)

					INSERT INTO AEC_ACTIONS (RECORDID, ST_LIC_NO, ACTION_TYPE, ACTION_DATE, COMPLETED_DATE)
										VALUES (@szRecordID, @szStLicNo, 'MERGED RECORD '+@szMatch4, GETDATE(), GETDATE())

					SET @szNotes = NULL
					SELECT @dtStLicIss = ST_LIC_ISSUE, @dtStLicExp = ST_LIC_EXPIRE FROM AEC_MAIN WHERE ST_LIC_NO = @szMatch4
					SET @szNotes = 'MERGED AEC RECORD: ' + @szMatch4
					IF @dtStLicIss IS NOT NULL AND @dtStLicIss <> ''
						SET @szNotes = @szNotes + '   ISSUED:  ' + CAST(@dtStLicIss AS VARCHAR)
					IF @dtStLicExp IS NOT NULL AND @dtStLicExp <> ''
						SET @szNotes = @szNotes + '   EXPIRED:  ' + CAST(@dtStLicExp AS VARCHAR)

					INSERT INTO PRMRY_NOTES (ACTIVITYGROUP, ACTIVITYNO, ACTIVITYRECORDID, SUBGROUP, SUBGROUPRECORDID, NOTES, DATEENTERED)
										VALUES ('AEC', @szStLicNo, @szAECRecID, 'ACTION', @szRecordID, @szNotes, GETDATE())

				-- update module and other associated records with new merged aec st_lic_no
				UPDATE AEC_FEES SET ST_LIC_NO = @szStLicNo WHERE ST_LIC_NO = @szMatch4
				UPDATE AEC_SUBFEES SET ST_LIC_NO = @szStLicNo WHERE ST_LIC_NO = @szMatch4
				UPDATE AEC_ACTIONS SET ST_LIC_NO = @szStLicNo WHERE ST_LIC_NO = @szMatch4

				IF (SELECT COUNT(*) FROM AEC_ATTACHMENTS WHERE ST_LIC_NO = @szMatch4) > 0
				BEGIN
							  -- Get files
							  DECLARE cGetFiles CURSOR LOCAL FOR        
							  SELECT PATHNAME
								FROM aec_attachments
								WHERE ST_LIC_NO = @szMatch4
							  Open cGetFiles
							  Fetch Next From cGetFiles Into @szFileName
							  While @@Fetch_status <> -1 
							  Begin
									-- Copy files
									SET @szCopy = NULL
									SET @szCopy = 'copy ' + @szAECPath + @szMatch4 + '\"' +  @szMatch4 + ' ' +@szFileName + '"' + ' ' + @szAECPath + @szStLicNo
									exec master..xp_cmdshell @szCopy, NO_OUTPUT
									-- Delete files
									SET @szCopy = NULL
									SET @szCopy = 'del ' + @szAECPath + @szMatch4 + '\"' + @szMatch4 + ' ' + @szFileName + '"'
									exec master..xp_cmdshell @szCopy, NO_OUTPUT

							  Fetch Next From cGetFiles Into @szFileName
							  End --While
							  Close cGetFiles
							  Deallocate cGetFiles

						UPDATE AEC_ATTACHMENTS SET ST_LIC_NO = @szStLicNo WHERE ST_LIC_NO = @szMatch4
				END

				UPDATE PERMIT_PEOPLE SET BUS_LICENSE = @szStLicNo, ID = 'C:'+ @szStLicNo WHERE SUBSTRING(ID,3,20) = @szMatch4 OR ISNULL(BUS_LICENSE,' ') = @szMatch4
				UPDATE GEO_PEOPLE SET BUS_LICENSE = @szStLicNo, ID = 'C:'+ @szStLicNo WHERE SUBSTRING(ID,3,20) = @szMatch4 OR ISNULL(BUS_LICENSE,' ') = @szMatch4
				UPDATE PROJECT_PEOPLE SET BUS_LICENSE = @szStLicNo, ID = 'C:'+ @szStLicNo WHERE SUBSTRING(ID,3,20) = @szMatch4 OR ISNULL(BUS_LICENSE,' ') = @szMatch4
			END

			IF @szMatch5 IS NOT NULL AND @szMatch5 <> ''
				UPDATE AEC_MAIN SET LIC_GRADE_5 = @szLicGrade5 WHERE RECORDID = @szAECRecID
			IF @szMatch5 IS NOT NULL AND @szMatch5 <> '' AND ISNULL(@szMatch5,' ') <> @szStLicNo
			BEGIN
				-- Insert action record to log merge activity
					-- Set unique RecordID
					SET @szRecordID = NULL
					-- Reset counter for unique number string at end of RecordID
					SET @lCounter = @lCounter + 1
					SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
						+ right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)
						+ right(('000000'+ ltrim(str(@lCounter))),6)

					INSERT INTO AEC_ACTIONS (RECORDID, ST_LIC_NO, ACTION_TYPE, ACTION_DATE, COMPLETED_DATE)
										VALUES (@szRecordID, @szStLicNo, 'MERGED RECORD '+@szMatch5, GETDATE(), GETDATE())

					SET @szNotes = NULL
					SELECT @dtStLicIss = ST_LIC_ISSUE, @dtStLicExp = ST_LIC_EXPIRE FROM AEC_MAIN WHERE ST_LIC_NO = @szMatch5
					SET @szNotes = 'MERGED AEC RECORD: ' + @szMatch5
					IF @dtStLicIss IS NOT NULL AND @dtStLicIss <> ''
						SET @szNotes = @szNotes + '   ISSUED:  ' + CAST(@dtStLicIss AS VARCHAR)
					IF @dtStLicExp IS NOT NULL AND @dtStLicExp <> ''
						SET @szNotes = @szNotes + '   EXPIRED:  ' + CAST(@dtStLicExp AS VARCHAR)

					INSERT INTO PRMRY_NOTES (ACTIVITYGROUP, ACTIVITYNO, ACTIVITYRECORDID, SUBGROUP, SUBGROUPRECORDID, NOTES, DATEENTERED)
										VALUES ('AEC', @szStLicNo, @szAECRecID, 'ACTION', @szRecordID, @szNotes, GETDATE())

				-- update module and other associated records with new merged aec st_lic_no
				UPDATE AEC_FEES SET ST_LIC_NO = @szStLicNo WHERE ST_LIC_NO = @szMatch5
				UPDATE AEC_SUBFEES SET ST_LIC_NO = @szStLicNo WHERE ST_LIC_NO = @szMatch5
				UPDATE AEC_ACTIONS SET ST_LIC_NO = @szStLicNo WHERE ST_LIC_NO = @szMatch5

				IF (SELECT COUNT(*) FROM AEC_ATTACHMENTS WHERE ST_LIC_NO = @szMatch5) > 0
				BEGIN
							  -- Get files
							  DECLARE cGetFiles CURSOR LOCAL FOR        
							  SELECT PATHNAME
								FROM aec_attachments
								WHERE ST_LIC_NO = @szMatch5
							  Open cGetFiles
							  Fetch Next From cGetFiles Into @szFileName
							  While @@Fetch_status <> -1 
							  Begin
									-- Copy files
									SET @szCopy = NULL
									SET @szCopy = 'copy ' + @szAECPath + @szMatch5 + '\"' + @szMatch5 + ' ' + @szFileName + '"' + ' ' + @szAECPath + @szStLicNo
									exec master..xp_cmdshell @szCopy, NO_OUTPUT
									-- Delete files
									SET @szCopy = NULL
									SET @szCopy = 'del ' + @szAECPath + @szMatch5 + '\"' + @szMatch5 + ' ' + @szFileName + '"'
									exec master..xp_cmdshell @szCopy, NO_OUTPUT

							  Fetch Next From cGetFiles Into @szFileName
							  End --While
							  Close cGetFiles
							  Deallocate cGetFiles

						UPDATE AEC_ATTACHMENTS SET ST_LIC_NO = @szStLicNo WHERE ST_LIC_NO = @szMatch5
				END

				UPDATE PERMIT_PEOPLE SET BUS_LICENSE = @szStLicNo, ID = 'C:'+ @szStLicNo WHERE SUBSTRING(ID,3,20) = @szMatch5 OR ISNULL(BUS_LICENSE,' ') = @szMatch5
				UPDATE GEO_PEOPLE SET BUS_LICENSE = @szStLicNo, ID = 'C:'+ @szStLicNo WHERE SUBSTRING(ID,3,20) = @szMatch5 OR ISNULL(BUS_LICENSE,' ') = @szMatch5
				UPDATE PROJECT_PEOPLE SET BUS_LICENSE = @szStLicNo, ID = 'C:'+ @szStLicNo WHERE SUBSTRING(ID,3,20) = @szMatch5 OR ISNULL(BUS_LICENSE,' ') = @szMatch5
			END

			IF @szMatch6 IS NOT NULL AND @szMatch6 <> ''
				UPDATE AEC_MAIN SET LIC_GRADE_6 = @szLicGrade6 WHERE RECORDID = @szAECRecID
			IF @szMatch6 IS NOT NULL AND @szMatch6 <> '' AND ISNULL(@szMatch6,' ') <> @szStLicNo
			BEGIN
				-- Insert action record to log merge activity
					-- Set unique RecordID
					SET @szRecordID = NULL
					-- Reset counter for unique number string at end of RecordID
					SET @lCounter = @lCounter + 1
					SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
						+ right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)
						+ right(('000000'+ ltrim(str(@lCounter))),6)

					INSERT INTO AEC_ACTIONS (RECORDID, ST_LIC_NO, ACTION_TYPE, ACTION_DATE, COMPLETED_DATE)
										VALUES (@szRecordID, @szStLicNo, 'MERGED RECORD '+@szMatch6, GETDATE(), GETDATE())

					SET @szNotes = NULL
					SELECT @dtStLicIss = ST_LIC_ISSUE, @dtStLicExp = ST_LIC_EXPIRE FROM AEC_MAIN WHERE ST_LIC_NO = @szMatch6
					SET @szNotes = 'MERGED AEC RECORD: ' + @szMatch6
					IF @dtStLicIss IS NOT NULL AND @dtStLicIss <> ''
						SET @szNotes = @szNotes + '   ISSUED:  ' + CAST(@dtStLicIss AS VARCHAR)
					IF @dtStLicExp IS NOT NULL AND @dtStLicExp <> ''
						SET @szNotes = @szNotes + '   EXPIRED:  ' + CAST(@dtStLicExp AS VARCHAR)

					INSERT INTO PRMRY_NOTES (ACTIVITYGROUP, ACTIVITYNO, ACTIVITYRECORDID, SUBGROUP, SUBGROUPRECORDID, NOTES, DATEENTERED)
										VALUES ('AEC', @szStLicNo, @szAECRecID, 'ACTION', @szRecordID, @szNotes, GETDATE())

				-- update module and other associated records with new merged aec st_lic_no
				UPDATE AEC_FEES SET ST_LIC_NO = @szStLicNo WHERE ST_LIC_NO = @szMatch6
				UPDATE AEC_SUBFEES SET ST_LIC_NO = @szStLicNo WHERE ST_LIC_NO = @szMatch6
				UPDATE AEC_ACTIONS SET ST_LIC_NO = @szStLicNo WHERE ST_LIC_NO = @szMatch6

				IF (SELECT COUNT(*) FROM AEC_ATTACHMENTS WHERE ST_LIC_NO = @szMatch6) > 0
				BEGIN
							  -- Get files
							  DECLARE cGetFiles CURSOR LOCAL FOR        
							  SELECT PATHNAME
								FROM aec_attachments
								WHERE ST_LIC_NO = @szMatch6
							  Open cGetFiles
							  Fetch Next From cGetFiles Into @szFileName
							  While @@Fetch_status <> -1 
							  Begin
									-- Copy files
									SET @szCopy = NULL
									SET @szCopy = 'copy ' + @szAECPath + @szMatch6 + '\"' + @szMatch6 + ' ' + @szFileName + '"' + ' ' + @szAECPath + @szStLicNo
									exec master..xp_cmdshell @szCopy, NO_OUTPUT
									-- Delete files
									SET @szCopy = NULL
									SET @szCopy = 'del ' + @szAECPath + @szMatch6 + '\"' + @szMatch6 + ' ' + @szFileName + '"'
									exec master..xp_cmdshell @szCopy, NO_OUTPUT

							  Fetch Next From cGetFiles Into @szFileName
							  End --While
							  Close cGetFiles
							  Deallocate cGetFiles

						UPDATE AEC_ATTACHMENTS SET ST_LIC_NO = @szStLicNo WHERE ST_LIC_NO = @szMatch6
				END

				UPDATE PERMIT_PEOPLE SET BUS_LICENSE = @szStLicNo, ID = 'C:'+ @szStLicNo WHERE SUBSTRING(ID,3,20) = @szMatch6 OR ISNULL(BUS_LICENSE,' ') = @szMatch6
				UPDATE GEO_PEOPLE SET BUS_LICENSE = @szStLicNo, ID = 'C:'+ @szStLicNo WHERE SUBSTRING(ID,3,20) = @szMatch6 OR ISNULL(BUS_LICENSE,' ') = @szMatch6
				UPDATE PROJECT_PEOPLE SET BUS_LICENSE = @szStLicNo, ID = 'C:'+ @szStLicNo WHERE SUBSTRING(ID,3,20) = @szMatch6 OR ISNULL(BUS_LICENSE,' ') = @szMatch6
			END

			IF @szAssocRec1 IS NOT NULL AND @szAssocRec1 <> '' AND ISNULL(@szAssocRec1,' ') <> @szStLicNo
			BEGIN
				-- Insert action record to log merge activity
					-- Set unique RecordID
					SET @szRecordID = NULL
					-- Reset counter for unique number string at end of RecordID
					SET @lCounter = @lCounter + 1
					SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
						+ right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)
						+ right(('000000'+ ltrim(str(@lCounter))),6)

					INSERT INTO AEC_ACTIONS (RECORDID, ST_LIC_NO, ACTION_TYPE, ACTION_DATE, COMPLETED_DATE)
										VALUES (@szRecordID, @szStLicNo, 'MERGED RECORD '+@szAssocRec1, GETDATE(), GETDATE())

					SET @szNotes = NULL
					SELECT @dtStLicIss = ST_LIC_ISSUE, @dtStLicExp = ST_LIC_EXPIRE FROM AEC_MAIN WHERE ST_LIC_NO = @szAssocRec1
					SET @szNotes = 'MERGED AEC RECORD: ' + @szAssocRec1
					IF @dtStLicIss IS NOT NULL AND @dtStLicIss <> ''
						SET @szNotes = @szNotes + '   ISSUED:  ' + CAST(@dtStLicIss AS VARCHAR)
					IF @dtStLicExp IS NOT NULL AND @dtStLicExp <> ''
						SET @szNotes = @szNotes + '   EXPIRED:  ' + CAST(@dtStLicExp AS VARCHAR)

					INSERT INTO PRMRY_NOTES (ACTIVITYGROUP, ACTIVITYNO, ACTIVITYRECORDID, SUBGROUP, SUBGROUPRECORDID, NOTES, DATEENTERED)
										VALUES ('AEC', @szStLicNo, @szAECRecID, 'ACTION', @szRecordID, @szNotes, GETDATE())

				-- update module and other associated records with new merged aec st_lic_no
				UPDATE PERMIT_PEOPLE SET BUS_LICENSE = @szStLicNo, ID = 'C:'+ @szStLicNo WHERE SUBSTRING(ID,3,20) = @szAssocRec1 OR ISNULL(BUS_LICENSE,' ') = @szAssocRec1
				UPDATE PROJECT_PEOPLE SET BUS_LICENSE = @szStLicNo, ID = 'C:'+ @szStLicNo WHERE SUBSTRING(ID,3,20) = @szAssocRec1 OR ISNULL(BUS_LICENSE,' ') = @szAssocRec1
				UPDATE GEO_PEOPLE SET BUS_LICENSE = @szStLicNo, ID = 'C:'+ @szStLicNo WHERE SUBSTRING(ID,3,20) = @szAssocRec1 OR ISNULL(BUS_LICENSE,' ') = @szAssocRec1
			END

			IF @szAssocRec2 IS NOT NULL AND @szAssocRec2 <> '' AND ISNULL(@szAssocRec2,' ') <> @szStLicNo
			BEGIN
				-- Insert action record to log merge activity
					-- Set unique RecordID
					SET @szRecordID = NULL
					-- Reset counter for unique number string at end of RecordID
					SET @lCounter = @lCounter + 1
					SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
						+ right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)
						+ right(('000000'+ ltrim(str(@lCounter))),6)

					INSERT INTO AEC_ACTIONS (RECORDID, ST_LIC_NO, ACTION_TYPE, ACTION_DATE, COMPLETED_DATE)
										VALUES (@szRecordID, @szStLicNo, 'MERGED RECORD '+@szAssocRec2, GETDATE(), GETDATE())

					SET @szNotes = NULL
					SELECT @dtStLicIss = ST_LIC_ISSUE, @dtStLicExp = ST_LIC_EXPIRE FROM AEC_MAIN WHERE ST_LIC_NO = @szAssocRec2
					SET @szNotes = 'MERGED AEC RECORD: ' + @szAssocRec2
					IF @dtStLicIss IS NOT NULL AND @dtStLicIss <> ''
						SET @szNotes = @szNotes + '   ISSUED:  ' + CAST(@dtStLicIss AS VARCHAR)
					IF @dtStLicExp IS NOT NULL AND @dtStLicExp <> ''
						SET @szNotes = @szNotes + '   EXPIRED:  ' + CAST(@dtStLicExp AS VARCHAR)

					INSERT INTO PRMRY_NOTES (ACTIVITYGROUP, ACTIVITYNO, ACTIVITYRECORDID, SUBGROUP, SUBGROUPRECORDID, NOTES, DATEENTERED)
										VALUES ('AEC', @szStLicNo, @szAECRecID, 'ACTION', @szRecordID, @szNotes, GETDATE())

				-- update module and other associated records with new merged aec st_lic_no
				UPDATE PERMIT_PEOPLE SET BUS_LICENSE = @szStLicNo, ID = 'C:'+ @szStLicNo WHERE SUBSTRING(ID,3,20) = @szAssocRec2 OR ISNULL(BUS_LICENSE,' ') = @szAssocRec2
				UPDATE PROJECT_PEOPLE SET BUS_LICENSE = @szStLicNo, ID = 'C:'+ @szStLicNo WHERE SUBSTRING(ID,3,20) = @szAssocRec2 OR ISNULL(BUS_LICENSE,' ') = @szAssocRec2
				UPDATE GEO_PEOPLE SET BUS_LICENSE = @szStLicNo, ID = 'C:'+ @szStLicNo WHERE SUBSTRING(ID,3,20) = @szAssocRec2 OR ISNULL(BUS_LICENSE,' ') = @szAssocRec2
			END

			IF @szAssocRec3 IS NOT NULL AND @szAssocRec3 <> '' AND ISNULL(@szAssocRec3,' ') <> @szStLicNo
			BEGIN
				-- Insert action record to log merge activity
					-- Set unique RecordID
					SET @szRecordID = NULL
					-- Reset counter for unique number string at end of RecordID
					SET @lCounter = @lCounter + 1
					SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
						+ right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)
						+ right(('000000'+ ltrim(str(@lCounter))),6)

					INSERT INTO AEC_ACTIONS (RECORDID, ST_LIC_NO, ACTION_TYPE, ACTION_DATE, COMPLETED_DATE)
										VALUES (@szRecordID, @szStLicNo, 'MERGED RECORD '+@szAssocRec3, GETDATE(), GETDATE())

					SET @szNotes = NULL
					SELECT @dtStLicIss = ST_LIC_ISSUE, @dtStLicExp = ST_LIC_EXPIRE FROM AEC_MAIN WHERE ST_LIC_NO = @szAssocRec3
					SET @szNotes = 'MERGED AEC RECORD: ' + @szAssocRec3
					IF @dtStLicIss IS NOT NULL AND @dtStLicIss <> ''
						SET @szNotes = @szNotes + '   ISSUED:  ' + CAST(@dtStLicIss AS VARCHAR)
					IF @dtStLicExp IS NOT NULL AND @dtStLicExp <> ''
						SET @szNotes = @szNotes + '   EXPIRED:  ' + CAST(@dtStLicExp AS VARCHAR)

					INSERT INTO PRMRY_NOTES (ACTIVITYGROUP, ACTIVITYNO, ACTIVITYRECORDID, SUBGROUP, SUBGROUPRECORDID, NOTES, DATEENTERED)
										VALUES ('AEC', @szStLicNo, @szAECRecID, 'ACTION', @szRecordID, @szNotes, GETDATE())

				-- update module and other associated records with new merged aec st_lic_no
				UPDATE PERMIT_PEOPLE SET BUS_LICENSE = @szStLicNo, ID = 'C:'+ @szStLicNo WHERE SUBSTRING(ID,3,20) = @szAssocRec3 OR ISNULL(BUS_LICENSE,' ') = @szAssocRec3
				UPDATE PROJECT_PEOPLE SET BUS_LICENSE = @szStLicNo, ID = 'C:'+ @szStLicNo WHERE SUBSTRING(ID,3,20) = @szAssocRec3 OR ISNULL(BUS_LICENSE,' ') = @szAssocRec3
				UPDATE GEO_PEOPLE SET BUS_LICENSE = @szStLicNo, ID = 'C:'+ @szStLicNo WHERE SUBSTRING(ID,3,20) = @szAssocRec3 OR ISNULL(BUS_LICENSE,' ') = @szAssocRec3
			END

			IF @szAssocRec4 IS NOT NULL AND @szAssocRec4 <> '' AND ISNULL(@szAssocRec4,' ') <> @szStLicNo
			BEGIN
				-- Insert action record to log merge activity
					-- Set unique RecordID
					SET @szRecordID = NULL
					-- Reset counter for unique number string at end of RecordID
					SET @lCounter = @lCounter + 1
					SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
						+ right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)
						+ right(('000000'+ ltrim(str(@lCounter))),6)

					INSERT INTO AEC_ACTIONS (RECORDID, ST_LIC_NO, ACTION_TYPE, ACTION_DATE, COMPLETED_DATE)
										VALUES (@szRecordID, @szStLicNo, 'MERGED RECORD '+@szAssocRec4, GETDATE(), GETDATE())

					SET @szNotes = NULL
					SELECT @dtStLicIss = ST_LIC_ISSUE, @dtStLicExp = ST_LIC_EXPIRE FROM AEC_MAIN WHERE ST_LIC_NO = @szAssocRec4
					SET @szNotes = 'MERGED AEC RECORD: ' + @szAssocRec4
					IF @dtStLicIss IS NOT NULL AND @dtStLicIss <> ''
						SET @szNotes = @szNotes + '   ISSUED:  ' + CAST(@dtStLicIss AS VARCHAR)
					IF @dtStLicExp IS NOT NULL AND @dtStLicExp <> ''
						SET @szNotes = @szNotes + '   EXPIRED:  ' + CAST(@dtStLicExp AS VARCHAR)

					INSERT INTO PRMRY_NOTES (ACTIVITYGROUP, ACTIVITYNO, ACTIVITYRECORDID, SUBGROUP, SUBGROUPRECORDID, NOTES, DATEENTERED)
										VALUES ('AEC', @szStLicNo, @szAECRecID, 'ACTION', @szRecordID, @szNotes, GETDATE())

				-- update module and other associated records with new merged aec st_lic_no
				UPDATE PERMIT_PEOPLE SET BUS_LICENSE = @szStLicNo, ID = 'C:'+ @szStLicNo WHERE SUBSTRING(ID,3,20) = @szAssocRec4 OR ISNULL(BUS_LICENSE,' ') = @szAssocRec4
				UPDATE PROJECT_PEOPLE SET BUS_LICENSE = @szStLicNo, ID = 'C:'+ @szStLicNo WHERE SUBSTRING(ID,3,20) = @szAssocRec4 OR ISNULL(BUS_LICENSE,' ') = @szAssocRec4
				UPDATE GEO_PEOPLE SET BUS_LICENSE = @szStLicNo, ID = 'C:'+ @szStLicNo WHERE SUBSTRING(ID,3,20) = @szAssocRec4 OR ISNULL(BUS_LICENSE,' ') = @szAssocRec4
			END

			IF @szAssocRec5 IS NOT NULL AND @szAssocRec5 <> '' AND ISNULL(@szAssocRec5,' ') <> @szStLicNo
			BEGIN
				-- Insert action record to log merge activity
					-- Set unique RecordID
					SET @szRecordID = NULL
					-- Reset counter for unique number string at end of RecordID
					SET @lCounter = @lCounter + 1
					SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
						+ right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)
						+ right(('000000'+ ltrim(str(@lCounter))),6)

					INSERT INTO AEC_ACTIONS (RECORDID, ST_LIC_NO, ACTION_TYPE, ACTION_DATE, COMPLETED_DATE)
										VALUES (@szRecordID, @szStLicNo, 'MERGED RECORD '+@szAssocRec5, GETDATE(), GETDATE())

					SET @szNotes = NULL
					SELECT @dtStLicIss = ST_LIC_ISSUE, @dtStLicExp = ST_LIC_EXPIRE FROM AEC_MAIN WHERE ST_LIC_NO = @szAssocRec5
					SET @szNotes = 'MERGED AEC RECORD: ' + @szAssocRec5
					IF @dtStLicIss IS NOT NULL AND @dtStLicIss <> ''
						SET @szNotes = @szNotes + '   ISSUED:  ' + CAST(@dtStLicIss AS VARCHAR)
					IF @dtStLicExp IS NOT NULL AND @dtStLicExp <> ''
						SET @szNotes = @szNotes + '   EXPIRED:  ' + CAST(@dtStLicExp AS VARCHAR)

					INSERT INTO PRMRY_NOTES (ACTIVITYGROUP, ACTIVITYNO, ACTIVITYRECORDID, SUBGROUP, SUBGROUPRECORDID, NOTES, DATEENTERED)
										VALUES ('AEC', @szStLicNo, @szAECRecID, 'ACTION', @szRecordID, @szNotes, GETDATE())

				-- update module and other associated records with new merged aec st_lic_no
				UPDATE PERMIT_PEOPLE SET BUS_LICENSE = @szStLicNo, ID = 'C:'+ @szStLicNo WHERE SUBSTRING(ID,3,20) = @szAssocRec5 OR ISNULL(BUS_LICENSE,' ') = @szAssocRec5
				UPDATE PROJECT_PEOPLE SET BUS_LICENSE = @szStLicNo, ID = 'C:'+ @szStLicNo WHERE SUBSTRING(ID,3,20) = @szAssocRec5 OR ISNULL(BUS_LICENSE,' ') = @szAssocRec5
				UPDATE GEO_PEOPLE SET BUS_LICENSE = @szStLicNo, ID = 'C:'+ @szStLicNo WHERE SUBSTRING(ID,3,20) = @szAssocRec5 OR ISNULL(BUS_LICENSE,' ') = @szAssocRec5
			END

			IF @szAssocRec6 IS NOT NULL AND @szAssocRec6 <> '' AND ISNULL(@szAssocRec6,' ') <> @szStLicNo
			BEGIN
				-- Insert action record to log merge activity
					-- Set unique RecordID
					SET @szRecordID = NULL
					-- Reset counter for unique number string at end of RecordID
					SET @lCounter = @lCounter + 1
					SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
						+ right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)
						+ right(('000000'+ ltrim(str(@lCounter))),6)

					INSERT INTO AEC_ACTIONS (RECORDID, ST_LIC_NO, ACTION_TYPE, ACTION_DATE, COMPLETED_DATE)
										VALUES (@szRecordID, @szStLicNo, 'MERGED RECORD '+@szAssocRec6, GETDATE(), GETDATE())

					SET @szNotes = NULL
					SELECT @dtStLicIss = ST_LIC_ISSUE, @dtStLicExp = ST_LIC_EXPIRE FROM AEC_MAIN WHERE ST_LIC_NO = @szAssocRec6
					SET @szNotes = 'MERGED AEC RECORD: ' + @szAssocRec6
					IF @dtStLicIss IS NOT NULL AND @dtStLicIss <> ''
						SET @szNotes = @szNotes + '   ISSUED:  ' + CAST(@dtStLicIss AS VARCHAR)
					IF @dtStLicExp IS NOT NULL AND @dtStLicExp <> ''
						SET @szNotes = @szNotes + '   EXPIRED:  ' + CAST(@dtStLicExp AS VARCHAR)

					INSERT INTO PRMRY_NOTES (ACTIVITYGROUP, ACTIVITYNO, ACTIVITYRECORDID, SUBGROUP, SUBGROUPRECORDID, NOTES, DATEENTERED)
									 VALUES ('AEC', @szStLicNo, @szAECRecID, 'ACTION', @szRecordID, @szNotes, GETDATE())

				-- update module and other associated records with new merged aec st_lic_no
				UPDATE PERMIT_PEOPLE SET BUS_LICENSE = @szStLicNo, ID = 'C:'+ @szStLicNo WHERE SUBSTRING(ID,3,20) = @szAssocRec6 OR ISNULL(BUS_LICENSE,' ') = @szAssocRec6
				UPDATE PROJECT_PEOPLE SET BUS_LICENSE = @szStLicNo, ID = 'C:'+ @szStLicNo WHERE SUBSTRING(ID,3,20) = @szAssocRec6 OR ISNULL(BUS_LICENSE,' ') = @szAssocRec6
				UPDATE GEO_PEOPLE SET BUS_LICENSE = @szStLicNo, ID = 'C:'+ @szStLicNo WHERE SUBSTRING(ID,3,20) = @szAssocRec6 OR ISNULL(BUS_LICENSE,' ') = @szAssocRec6
			END

			IF @szAssocRec7 IS NOT NULL AND @szAssocRec7 <> '' AND ISNULL(@szAssocRec7,' ') <> @szStLicNo
			BEGIN
				-- Insert action record to log merge activity
					-- Set unique RecordID
					SET @szRecordID = NULL
					-- Reset counter for unique number string at end of RecordID
					SET @lCounter = @lCounter + 1
					SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
						+ right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)
						+ right(('000000'+ ltrim(str(@lCounter))),6)

					INSERT INTO AEC_ACTIONS (RECORDID, ST_LIC_NO, ACTION_TYPE, ACTION_DATE, COMPLETED_DATE)
										VALUES (@szRecordID, @szStLicNo, 'MERGED RECORD '+@szAssocRec7, GETDATE(), GETDATE())

					SET @szNotes = NULL
					SELECT @dtStLicIss = ST_LIC_ISSUE, @dtStLicExp = ST_LIC_EXPIRE FROM AEC_MAIN WHERE ST_LIC_NO = @szAssocRec7
					SET @szNotes = 'MERGED AEC RECORD: ' + @szAssocRec7
					IF @dtStLicIss IS NOT NULL AND @dtStLicIss <> ''
						SET @szNotes = @szNotes + '   ISSUED:  ' + CAST(@dtStLicIss AS VARCHAR)
					IF @dtStLicExp IS NOT NULL AND @dtStLicExp <> ''
						SET @szNotes = @szNotes + '   EXPIRED:  ' + CAST(@dtStLicExp AS VARCHAR)

					INSERT INTO PRMRY_NOTES (ACTIVITYGROUP, ACTIVITYNO, ACTIVITYRECORDID, SUBGROUP, SUBGROUPRECORDID, NOTES, DATEENTERED)
									 VALUES ('AEC', @szStLicNo, @szAECRecID, 'ACTION', @szRecordID, @szNotes, GETDATE())

				-- update module and other associated records with new merged aec st_lic_no
				UPDATE PERMIT_PEOPLE SET BUS_LICENSE = @szStLicNo, ID = 'C:'+ @szStLicNo WHERE SUBSTRING(ID,3,20) = @szAssocRec7 OR ISNULL(BUS_LICENSE,' ') = @szAssocRec7
				UPDATE PROJECT_PEOPLE SET BUS_LICENSE = @szStLicNo, ID = 'C:'+ @szStLicNo WHERE SUBSTRING(ID,3,20) = @szAssocRec7 OR ISNULL(BUS_LICENSE,' ') = @szAssocRec7
				UPDATE GEO_PEOPLE SET BUS_LICENSE = @szStLicNo, ID = 'C:'+ @szStLicNo WHERE SUBSTRING(ID,3,20) = @szAssocRec7 OR ISNULL(BUS_LICENSE,' ') = @szAssocRec7
			END

		END
		ELSE
		BEGIN
			SELECT @szNeedUpdtLicNoID = RECORDID FROM AEC_MAIN WHERE ST_LIC_NO = @szStLicNo

			-- Set new STLICNO
			SET @szNewStLicNo = NULL
			SET @lNewAECCounter = @lNewAECCounter + 1
			SET @szNewStLicNo ='AEC' + CAST(@lNewAECCounter AS VARCHAR)

			UPDATE AEC_FEES SET ST_LIC_NO = @szNewStLicNo WHERE ST_LIC_NO = @szStLicNo
			UPDATE AEC_SUBFEES SET ST_LIC_NO = @szNewStLicNo WHERE ST_LIC_NO = @szStLicNo
			UPDATE AEC_ACTIONS SET ST_LIC_NO = @szNewStLicNo WHERE ST_LIC_NO = @szStLicNo
			UPDATE AEC_UDF SET ST_LIC_NO = @szNewStLicNo WHERE ST_LIC_NO = @szStLicNo
			UPDATE AEC_INSURANCE SET ST_LIC_NO = @szNewStLicNo WHERE ST_LIC_NO = @szStLicNo
			UPDATE AEC_PEOPLE SET ST_LIC_NO = @szNewStLicNo WHERE ST_LIC_NO = @szStLicNo
			IF (SELECT COUNT(*) FROM AEC_ATTACHMENTS WHERE ST_LIC_NO = @szStLicNo) > 0
				INSERT INTO RESULTSET (DIRECTORY) VALUES(@szNewStLicNo)
			UPDATE AEC_ATTACHMENTS SET ST_LIC_NO = @szNewStLicNo WHERE ST_LIC_NO = @szStLicNo
						SET @szRename = NULL
						SET @szRename = 'ren ' + @szAECPath + @szStLicNo + ' ' + '"' + @szNewStLicNo + '"'
						EXEC master..xp_cmdshell @szRename, no_output

			UPDATE PERMIT_PEOPLE SET BUS_LICENSE = @szNewStLicNo, ID = 'C:'+ @szNewStLicNo WHERE SUBSTRING(ID,3,20) = @szStLicNo OR ISNULL(BUS_LICENSE,' ') = @szStLicNo
			UPDATE GEO_PEOPLE SET BUS_LICENSE = @szNewStLicNo, ID = 'C:'+ @szNewStLicNo WHERE SUBSTRING(ID,3,20) = @szStLicNo OR ISNULL(BUS_LICENSE,' ') = @szStLicNo
			UPDATE PROJECT_PEOPLE SET BUS_LICENSE = @szNewStLicNo, ID = 'C:'+ @szNewStLicNo WHERE SUBSTRING(ID,3,20) = @szStLicNo OR ISNULL(BUS_LICENSE,' ') = @szStLicNo

			UPDATE AEC_MAIN SET ST_LIC_NO = @szNewStLicNo, COMPANY = @szCompany, LIC_GRADE_1 = @szLicGrade1, AECTYPE = @szAECType, AECSUBTYPE = @szSubType WHERE RECORDID = @szNeedUpdtLicNoID

			-- Insert action record to log renumbering activity
			-- Set unique RecordID
			SET @szRecordID = NULL
			-- Reset counter for unique number string at end of RecordID
			SET @lCounter = @lCounter + 1
			SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
				+ right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)
				+ right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
				+ right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)
				+ right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
				+ right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)
				+ right(('000000'+ ltrim(str(@lCounter))),6)

			INSERT INTO AEC_ACTIONS (RECORDID, ST_LIC_NO, ACTION_TYPE, ACTION_DATE, COMPLETED_DATE)
								VALUES (@szRecordID, @szNewStLicNo, 'MERGED RECORD '+@szStLicNo, GETDATE(), GETDATE())

			--updates for MATCH fields
			IF @szMatch2 IS NOT NULL AND @szMatch2 <> ''
				UPDATE AEC_MAIN SET LIC_GRADE_2 = @szLicGrade2 WHERE RECORDID = @szNeedUpdtLicNoID
			IF @szMatch2 IS NOT NULL AND @szMatch2 <> '' AND ISNULL(@szMatch2,' ') <> @szStLicNo
			BEGIN
				-- Insert action record to log merge activity
					-- Set unique RecordID
					SET @szRecordID = NULL
					-- Reset counter for unique number string at end of RecordID
					SET @lCounter = @lCounter + 1
					SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
						+ right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)
						+ right(('000000'+ ltrim(str(@lCounter))),6)

					INSERT INTO AEC_ACTIONS (RECORDID, ST_LIC_NO, ACTION_TYPE, ACTION_DATE, COMPLETED_DATE)
										VALUES (@szRecordID, @szNewStLicNo, 'MERGED RECORD '+@szMatch2, GETDATE(), GETDATE())

					SELECT @dtStLicIss = ST_LIC_ISSUE, @dtStLicExp = ST_LIC_EXPIRE FROM AEC_MAIN WHERE ST_LIC_NO = @szMatch2
					SET @szNotes = 'MERGED AEC RECORD: ' + @szMatch2
					IF @dtStLicIss IS NOT NULL AND @dtStLicIss <> ''
						SET @szNotes = @szNotes + '   ISSUED:  ' + CAST(@dtStLicIss AS VARCHAR)
					IF @dtStLicExp IS NOT NULL AND @dtStLicExp <> ''
						SET @szNotes = @szNotes + '   EXPIRED:  ' + CAST(@dtStLicExp AS VARCHAR)

					INSERT INTO PRMRY_NOTES (ACTIVITYGROUP, ACTIVITYNO, ACTIVITYRECORDID, SUBGROUP, SUBGROUPRECORDID, NOTES, DATEENTERED)
										VALUES ('AEC', @szNewStLicNo, @szNeedUpdtLicNoID, 'ACTION', @szRecordID, @szNotes, GETDATE())

				-- update module and other associated records with new merged aec st_lic_no
				UPDATE AEC_FEES SET ST_LIC_NO = @szNewStLicNo WHERE ST_LIC_NO = @szMatch2
				UPDATE AEC_SUBFEES SET ST_LIC_NO = @szNewStLicNo WHERE ST_LIC_NO = @szMatch2
				UPDATE AEC_ACTIONS SET ST_LIC_NO = @szNewStLicNo WHERE ST_LIC_NO = @szMatch2

				IF (SELECT COUNT(*) FROM AEC_ATTACHMENTS WHERE ST_LIC_NO = @szMatch2) > 0
				BEGIN
						IF (select count(*) from resultset where directory = @szNewStLicNo) = 1
						BEGIN
							  -- Get files
							  DECLARE cGetFiles CURSOR LOCAL FOR        
							  SELECT PATHNAME
								FROM AEC_ATTACHMENTS
								WHERE ST_LIC_NO = @szMatch2
							  Open cGetFiles
							  Fetch Next From cGetFiles Into @szFileName
							  While @@Fetch_status <> -1 
							  Begin
									-- Copy files
									SET @szCopy = NULL
									SET @szCopy = 'copy ' + @szAECPath + @szMatch2 + '\"' + @szMatch2 + ' ' + @szFileName + '"' + ' ' + @szAECPath + @szNewStLicNo
									exec master..xp_cmdshell @szCopy, NO_OUTPUT
									-- Delete files
									SET @szCopy = NULL
									SET @szCopy = 'del ' + @szAECPath + @szMatch2 + '\"' + @szMatch2 + ' ' + @szFileName + '"'
									exec master..xp_cmdshell @szCopy, NO_OUTPUT

							  Fetch Next From cGetFiles Into @szFileName
							  End --While
							  Close cGetFiles
							  Deallocate cGetFiles
						END
						ELSE
						BEGIN
							SET @szRename = NULL
							SET @szRename = 'ren ' + @szAECPath + @szMatch2 + ' ' + '"' + @szNewStLicNo + '"'
							EXEC master..xp_cmdshell @szRename, no_output
						END

						UPDATE AEC_ATTACHMENTS SET ST_LIC_NO = @szNewStLicNo WHERE ST_LIC_NO = @szMatch2
				END

				UPDATE PERMIT_PEOPLE SET BUS_LICENSE = @szNewStLicNo, ID = 'C:'+ @szNewStLicNo WHERE SUBSTRING(ID,3,20) = @szMatch2 OR ISNULL(BUS_LICENSE,' ') = @szMatch2
				UPDATE GEO_PEOPLE SET BUS_LICENSE = @szNewStLicNo, ID = 'C:'+ @szNewStLicNo WHERE SUBSTRING(ID,3,20) = @szMatch2 OR ISNULL(BUS_LICENSE,' ') = @szMatch2
				UPDATE PROJECT_PEOPLE SET BUS_LICENSE = @szStLicNo, ID = 'C:'+ @szNewStLicNo WHERE SUBSTRING(ID,3,20) = @szMatch2 OR ISNULL(BUS_LICENSE,' ') = @szMatch2
			END
			
			IF @szMatch3 IS NOT NULL AND @szMatch3 <> ''
				UPDATE AEC_MAIN SET LIC_GRADE_3 = @szLicGrade3 WHERE RECORDID = @szNeedUpdtLicNoID
			IF @szMatch3 IS NOT NULL AND @szMatch3 <> '' AND ISNULL(@szMatch3,' ') <> @szStLicNo
			BEGIN
				-- Insert action record to log merge activity
					-- Set unique RecordID
					SET @szRecordID = NULL
					-- Reset counter for unique number string at end of RecordID
					SET @lCounter = @lCounter + 1
					SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
						+ right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)
						+ right(('000000'+ ltrim(str(@lCounter))),6)

					INSERT INTO AEC_ACTIONS (RECORDID, ST_LIC_NO, ACTION_TYPE, ACTION_DATE, COMPLETED_DATE)
										VALUES (@szRecordID, @szNewStLicNo, 'MERGED RECORD '+@szMatch3, GETDATE(), GETDATE())

					SET @szNotes = NULL
					SELECT @dtStLicIss = ST_LIC_ISSUE, @dtStLicExp = ST_LIC_EXPIRE FROM AEC_MAIN WHERE ST_LIC_NO = @szMatch3
					SET @szNotes = 'MERGED AEC RECORD: ' + @szMatch3
					IF @dtStLicIss IS NOT NULL AND @dtStLicIss <> ''
						SET @szNotes = @szNotes + '   ISSUED:  ' + CAST(@dtStLicIss AS VARCHAR)
					IF @dtStLicExp IS NOT NULL AND @dtStLicExp <> ''
						SET @szNotes = @szNotes + '   EXPIRED:  ' + CAST(@dtStLicExp AS VARCHAR)

					INSERT INTO PRMRY_NOTES (ACTIVITYGROUP, ACTIVITYNO, ACTIVITYRECORDID, SUBGROUP, SUBGROUPRECORDID, NOTES, DATEENTERED)
										VALUES ('AEC', @szNewStLicNo, @szNeedUpdtLicNoID, 'ACTION', @szRecordID, @szNotes, GETDATE())

				-- update module and other associated records with new merged aec st_lic_no
				UPDATE AEC_FEES SET ST_LIC_NO = @szNewStLicNo WHERE ST_LIC_NO = @szMatch3
				UPDATE AEC_SUBFEES SET ST_LIC_NO = @szNewStLicNo WHERE ST_LIC_NO = @szMatch3
				UPDATE AEC_ACTIONS SET ST_LIC_NO = @szNewStLicNo WHERE ST_LIC_NO = @szMatch3

				IF (SELECT COUNT(*) FROM AEC_ATTACHMENTS WHERE ST_LIC_NO = @szMatch3) > 0
				BEGIN
						IF (select count(*) from resultset where directory = @szNewStLicNo) = 1
						BEGIN
							  -- Get files
							  DECLARE cGetFiles CURSOR LOCAL FOR        
							  SELECT PATHNAME
								FROM aec_attachments
								WHERE ST_LIC_NO = @szMatch3
							  Open cGetFiles
							  Fetch Next From cGetFiles Into @szFileName
							  While @@Fetch_status <> -1 
							  Begin
									-- Copy files
									SET @szCopy = NULL
									SET @szCopy = 'copy ' + @szAECPath + @szMatch3 + '\"' + @szMatch3 + ' ' + @szFileName + '"' + ' ' + @szAECPath + @szNewStLicNo
									exec master..xp_cmdshell @szCopy, NO_OUTPUT
									-- Delete files
									SET @szCopy = NULL
									SET @szCopy = 'del ' + @szAECPath + @szMatch3 + '\"' + @szMatch3 + ' ' + @szFileName + '"'
									exec master..xp_cmdshell @szCopy, NO_OUTPUT

							  Fetch Next From cGetFiles Into @szFileName
							  End --While
							  Close cGetFiles
							  Deallocate cGetFiles
						END
						ELSE
						BEGIN
							SET @szRename = NULL
							SET @szRename = 'ren ' + @szAECPath + @szMatch3 + ' ' + '"' + @szNewStLicNo + '"'
							EXEC master..xp_cmdshell @szRename, no_output
						END

						UPDATE AEC_ATTACHMENTS SET ST_LIC_NO = @szNewStLicNo WHERE ST_LIC_NO = @szMatch3
				END

				UPDATE PERMIT_PEOPLE SET BUS_LICENSE = @szNewStLicNo, ID = 'C:'+ @szNewStLicNo WHERE SUBSTRING(ID,3,20) = @szMatch3 OR ISNULL(BUS_LICENSE,' ') = @szMatch3
				UPDATE GEO_PEOPLE SET BUS_LICENSE = @szNewStLicNo, ID = 'C:'+ @szNewStLicNo WHERE SUBSTRING(ID,3,20) = @szMatch3 OR ISNULL(BUS_LICENSE,' ') = @szMatch3
				UPDATE PROJECT_PEOPLE SET BUS_LICENSE = @szStLicNo, ID = 'C:'+ @szNewStLicNo WHERE SUBSTRING(ID,3,20) = @szMatch3 OR ISNULL(BUS_LICENSE,' ') = @szMatch3
			END

			IF @szMatch4 IS NOT NULL AND @szMatch4 <> ''
				UPDATE AEC_MAIN SET LIC_GRADE_4 = @szLicGrade4 WHERE RECORDID = @szNeedUpdtLicNoID
			IF @szMatch4 IS NOT NULL AND @szMatch4 <> '' AND ISNULL(@szMatch4,' ') <> @szStLicNo
			BEGIN
				-- Insert action record to log merge activity
					-- Set unique RecordID
					SET @szRecordID = NULL
					-- Reset counter for unique number string at end of RecordID
					SET @lCounter = @lCounter + 1
					SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
						+ right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)
						+ right(('000000'+ ltrim(str(@lCounter))),6)

					INSERT INTO AEC_ACTIONS (RECORDID, ST_LIC_NO, ACTION_TYPE, ACTION_DATE, COMPLETED_DATE)
										VALUES (@szRecordID, @szNewStLicNo, 'MERGED RECORD '+@szMatch4, GETDATE(), GETDATE())

					SET @szNotes = NULL
					SELECT @dtStLicIss = ST_LIC_ISSUE, @dtStLicExp = ST_LIC_EXPIRE FROM AEC_MAIN WHERE ST_LIC_NO = @szMatch4
					SET @szNotes = 'MERGED AEC RECORD: ' + @szMatch4
					IF @dtStLicIss IS NOT NULL AND @dtStLicIss <> ''
						SET @szNotes = @szNotes + '   ISSUED:  ' + CAST(@dtStLicIss AS VARCHAR)
					IF @dtStLicExp IS NOT NULL AND @dtStLicExp <> ''
						SET @szNotes = @szNotes + '   EXPIRED:  ' + CAST(@dtStLicExp AS VARCHAR)

					INSERT INTO PRMRY_NOTES (ACTIVITYGROUP, ACTIVITYNO, ACTIVITYRECORDID, SUBGROUP, SUBGROUPRECORDID, NOTES, DATEENTERED)
										VALUES ('AEC', @szNewStLicNo, @szNeedUpdtLicNoID, 'ACTION', @szRecordID, @szNotes, GETDATE())

				-- update module and other associated records with new merged aec st_lic_no
				UPDATE AEC_FEES SET ST_LIC_NO = @szNewStLicNo WHERE ST_LIC_NO = @szMatch4
				UPDATE AEC_SUBFEES SET ST_LIC_NO = @szNewStLicNo WHERE ST_LIC_NO = @szMatch4
				UPDATE AEC_ACTIONS SET ST_LIC_NO = @szNewStLicNo WHERE ST_LIC_NO = @szMatch4

				IF (SELECT COUNT(*) FROM AEC_ATTACHMENTS WHERE ST_LIC_NO = @szMatch4) > 0
				BEGIN
						IF (select count(*) from resultset where directory = @szNewStLicNo) = 1
						BEGIN
							  -- Get files
							  DECLARE cGetFiles CURSOR LOCAL FOR        
							  SELECT PATHNAME
								FROM aec_attachments
								WHERE ST_LIC_NO = @szMatch4
							  Open cGetFiles
							  Fetch Next From cGetFiles Into @szFileName
							  While @@Fetch_status <> -1 
							  Begin
									-- Copy files
									SET @szCopy = NULL
									SET @szCopy = 'copy ' + @szAECPath + @szMatch4 + '\"' + @szMatch4 + ' ' + @szFileName + '"' + ' ' + @szAECPath + @szNewStLicNo
									exec master..xp_cmdshell @szCopy, NO_OUTPUT
									-- Delete files
									SET @szCopy = NULL
									SET @szCopy = 'del ' + @szAECPath + @szMatch4 + '\"' + @szMatch4 + ' ' + @szFileName + '"'
									exec master..xp_cmdshell @szCopy, NO_OUTPUT

							  Fetch Next From cGetFiles Into @szFileName
							  End --While
							  Close cGetFiles
							  Deallocate cGetFiles
						END
						ELSE
						BEGIN
							SET @szRename = NULL
							SET @szRename = 'ren ' + @szAECPath + @szMatch4 + ' ' + '"' + @szNewStLicNo + '"'
							EXEC master..xp_cmdshell @szRename, no_output
						END

						UPDATE AEC_ATTACHMENTS SET ST_LIC_NO = @szNewStLicNo WHERE ST_LIC_NO = @szMatch4
				END

				UPDATE PERMIT_PEOPLE SET BUS_LICENSE = @szNewStLicNo, ID = 'C:'+ @szNewStLicNo WHERE SUBSTRING(ID,3,20) = @szMatch4 OR ISNULL(BUS_LICENSE,' ') = @szMatch4
				UPDATE GEO_PEOPLE SET BUS_LICENSE = @szNewStLicNo, ID = 'C:'+ @szNewStLicNo WHERE SUBSTRING(ID,3,20) = @szMatch4 OR ISNULL(BUS_LICENSE,' ') = @szMatch4
				UPDATE PROJECT_PEOPLE SET BUS_LICENSE = @szStLicNo, ID = 'C:'+ @szNewStLicNo WHERE SUBSTRING(ID,3,20) = @szMatch4 OR ISNULL(BUS_LICENSE,' ') = @szMatch4
			END

			IF @szMatch5 IS NOT NULL AND @szMatch5 <> ''
				UPDATE AEC_MAIN SET LIC_GRADE_5 = @szLicGrade5 WHERE RECORDID = @szAECRecID
			IF @szMatch5 IS NOT NULL AND @szMatch5 <> '' AND ISNULL(@szMatch5,' ') <> @szStLicNo
			BEGIN
				-- Insert action record to log merge activity
					-- Set unique RecordID
					SET @szRecordID = NULL
					-- Reset counter for unique number string at end of RecordID
					SET @lCounter = @lCounter + 1
					SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
						+ right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)
						+ right(('000000'+ ltrim(str(@lCounter))),6)

					INSERT INTO AEC_ACTIONS (RECORDID, ST_LIC_NO, ACTION_TYPE, ACTION_DATE, COMPLETED_DATE)
										VALUES (@szRecordID, @szNewStLicNo, 'MERGED RECORD '+@szMatch5, GETDATE(), GETDATE())

					SET @szNotes = NULL
					SELECT @dtStLicIss = ST_LIC_ISSUE, @dtStLicExp = ST_LIC_EXPIRE FROM AEC_MAIN WHERE ST_LIC_NO = @szMatch5
					SET @szNotes = 'MERGED AEC RECORD: ' + @szMatch5
					IF @dtStLicIss IS NOT NULL AND @dtStLicIss <> ''
						SET @szNotes = @szNotes + '   ISSUED:  ' + CAST(@dtStLicIss AS VARCHAR)
					IF @dtStLicExp IS NOT NULL AND @dtStLicExp <> ''
						SET @szNotes = @szNotes + '   EXPIRED:  ' + CAST(@dtStLicExp AS VARCHAR)

					INSERT INTO PRMRY_NOTES (ACTIVITYGROUP, ACTIVITYNO, ACTIVITYRECORDID, SUBGROUP, SUBGROUPRECORDID, NOTES, DATEENTERED)
										VALUES ('AEC', @szNewStLicNo, @szNeedUpdtLicNoID, 'ACTION', @szRecordID, @szNotes, GETDATE())

				-- update module and other associated records with new merged aec st_lic_no
				UPDATE AEC_FEES SET ST_LIC_NO = @szNewStLicNo WHERE ST_LIC_NO = @szMatch5
				UPDATE AEC_SUBFEES SET ST_LIC_NO = @szNewStLicNo WHERE ST_LIC_NO = @szMatch5
				UPDATE AEC_ACTIONS SET ST_LIC_NO = @szNewStLicNo WHERE ST_LIC_NO = @szMatch5

				IF (SELECT COUNT(*) FROM AEC_ATTACHMENTS WHERE ST_LIC_NO = @szMatch5) > 0
				BEGIN
						IF (select count(*) from resultset where directory = @szNewStLicNo) = 1
						BEGIN
							  -- Get files
							  DECLARE cGetFiles CURSOR LOCAL FOR        
							  SELECT PATHNAME
								FROM aec_attachments
								WHERE ST_LIC_NO = @szMatch5
							  Open cGetFiles
							  Fetch Next From cGetFiles Into @szFileName
							  While @@Fetch_status <> -1 
							  Begin
									-- Copy files
									SET @szCopy = NULL
									SET @szCopy = 'copy ' + @szAECPath + @szMatch5 + '\"' + @szMatch5 + ' ' + @szFileName + '"' + ' ' + @szAECPath + @szNewStLicNo
									exec master..xp_cmdshell @szCopy, NO_OUTPUT
									-- Delete files
									SET @szCopy = NULL
									SET @szCopy = 'del ' + @szAECPath + @szMatch5 + '\"' + @szMatch5 + ' ' + @szFileName + '"'
									exec master..xp_cmdshell @szCopy, NO_OUTPUT

							  Fetch Next From cGetFiles Into @szFileName
							  End --While
							  Close cGetFiles
							  Deallocate cGetFiles
						END
						ELSE
						BEGIN
							SET @szRename = NULL
							SET @szRename = 'ren ' + @szAECPath + @szMatch5 + ' ' + '"' + @szNewStLicNo + '"'
							EXEC master..xp_cmdshell @szRename, no_output
						END

						UPDATE AEC_ATTACHMENTS SET ST_LIC_NO = @szNewStLicNo WHERE ST_LIC_NO = @szMatch5
				END

				UPDATE PERMIT_PEOPLE SET BUS_LICENSE = @szNewStLicNo, ID = 'C:'+ @szNewStLicNo WHERE SUBSTRING(ID,3,20) = @szMatch5 OR ISNULL(BUS_LICENSE,' ') = @szMatch5
				UPDATE GEO_PEOPLE SET BUS_LICENSE = @szNewStLicNo, ID = 'C:'+ @szNewStLicNo WHERE SUBSTRING(ID,3,20) = @szMatch5 OR ISNULL(BUS_LICENSE,' ') = @szMatch5
				UPDATE PROJECT_PEOPLE SET BUS_LICENSE = @szStLicNo, ID = 'C:'+ @szNewStLicNo WHERE SUBSTRING(ID,3,20) = @szMatch5 OR ISNULL(BUS_LICENSE,' ') = @szMatch5
			END

			IF @szMatch6 IS NOT NULL AND @szMatch6 <> ''
				UPDATE AEC_MAIN SET LIC_GRADE_6 = @szLicGrade6 WHERE RECORDID = @szAECRecID
			IF @szMatch6 IS NOT NULL AND @szMatch6 <> '' AND ISNULL(@szMatch6,' ') <> @szStLicNo
			BEGIN
				-- Insert action record to log merge activity
					-- Set unique RecordID
					SET @szRecordID = NULL
					-- Reset counter for unique number string at end of RecordID
					SET @lCounter = @lCounter + 1
					SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
						+ right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)
						+ right(('000000'+ ltrim(str(@lCounter))),6)

					INSERT INTO AEC_ACTIONS (RECORDID, ST_LIC_NO, ACTION_TYPE, ACTION_DATE, COMPLETED_DATE)
										VALUES (@szRecordID, @szNewStLicNo, 'MERGED RECORD '+@szMatch6, GETDATE(), GETDATE())

					SET @szNotes = NULL
					SELECT @dtStLicIss = ST_LIC_ISSUE, @dtStLicExp = ST_LIC_EXPIRE FROM AEC_MAIN WHERE ST_LIC_NO = @szMatch6
					SET @szNotes = 'MERGED AEC RECORD: ' + @szMatch6
					IF @dtStLicIss IS NOT NULL AND @dtStLicIss <> ''
						SET @szNotes = @szNotes + '   ISSUED:  ' + CAST(@dtStLicIss AS VARCHAR)
					IF @dtStLicExp IS NOT NULL AND @dtStLicExp <> ''
						SET @szNotes = @szNotes + '   EXPIRED:  ' + CAST(@dtStLicExp AS VARCHAR)

					INSERT INTO PRMRY_NOTES (ACTIVITYGROUP, ACTIVITYNO, ACTIVITYRECORDID, SUBGROUP, SUBGROUPRECORDID, NOTES, DATEENTERED)
										VALUES ('AEC', @szNewStLicNo, @szNeedUpdtLicNoID, 'ACTION', @szRecordID, @szNotes, GETDATE())

				-- update module and other associated records with new merged aec st_lic_no
				UPDATE AEC_FEES SET ST_LIC_NO = @szNewStLicNo WHERE ST_LIC_NO = @szMatch6
				UPDATE AEC_SUBFEES SET ST_LIC_NO = @szNewStLicNo WHERE ST_LIC_NO = @szMatch6
				UPDATE AEC_ACTIONS SET ST_LIC_NO = @szNewStLicNo WHERE ST_LIC_NO = @szMatch6

				IF (SELECT COUNT(*) FROM AEC_ATTACHMENTS WHERE ST_LIC_NO = @szMatch6) > 0
				BEGIN
						IF (select count(*) from resultset where directory = @szNewStLicNo) = 1
						BEGIN
							  -- Get files
							  DECLARE cGetFiles CURSOR LOCAL FOR        
							  SELECT PATHNAME
								FROM aec_attachments
								WHERE ST_LIC_NO = @szMatch6
							  Open cGetFiles
							  Fetch Next From cGetFiles Into @szFileName
							  While @@Fetch_status <> -1 
							  Begin
									-- Copy files
									SET @szCopy = NULL
									SET @szCopy = 'copy ' + @szAECPath + @szMatch6 + '\"' + @szMatch6 + ' ' + @szFileName + '"' + ' ' + @szAECPath + @szNewStLicNo
									exec master..xp_cmdshell @szCopy, NO_OUTPUT
									-- Delete files
									SET @szCopy = NULL
									SET @szCopy = 'del ' + @szAECPath + @szMatch6 + '\"' + @szMatch6 + ' ' + @szFileName + '"'
									exec master..xp_cmdshell @szCopy, NO_OUTPUT

							  Fetch Next From cGetFiles Into @szFileName
							  End --While
							  Close cGetFiles
							  Deallocate cGetFiles
						END
						ELSE
						BEGIN
							SET @szRename = NULL
							SET @szRename = 'ren ' + @szAECPath + @szMatch6 + ' ' + '"' + @szNewStLicNo + '"'
							EXEC master..xp_cmdshell @szRename, no_output
						END

						UPDATE AEC_ATTACHMENTS SET ST_LIC_NO = @szNewStLicNo WHERE ST_LIC_NO = @szMatch6
				END

				UPDATE PERMIT_PEOPLE SET BUS_LICENSE = @szNewStLicNo, ID = 'C:'+ @szNewStLicNo WHERE SUBSTRING(ID,3,20) = @szMatch6 OR ISNULL(BUS_LICENSE,' ') = @szMatch6
				UPDATE GEO_PEOPLE SET BUS_LICENSE = @szNewStLicNo, ID = 'C:'+ @szNewStLicNo WHERE SUBSTRING(ID,3,20) = @szMatch6 OR ISNULL(BUS_LICENSE,' ') = @szMatch6
				UPDATE PROJECT_PEOPLE SET BUS_LICENSE = @szStLicNo, ID = 'C:'+ @szNewStLicNo WHERE SUBSTRING(ID,3,20) = @szMatch6 OR ISNULL(BUS_LICENSE,' ') = @szMatch6
			END

			IF @szAssocRec1 IS NOT NULL AND @szAssocRec1 <> '' AND ISNULL(@szAssocRec1,' ') <> @szStLicNo
			BEGIN
				-- Insert action record to log merge activity
					-- Set unique RecordID
					SET @szRecordID = NULL
					-- Reset counter for unique number string at end of RecordID
					SET @lCounter = @lCounter + 1
					SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
						+ right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)
						+ right(('000000'+ ltrim(str(@lCounter))),6)

					INSERT INTO AEC_ACTIONS (RECORDID, ST_LIC_NO, ACTION_TYPE, ACTION_DATE, COMPLETED_DATE)
										VALUES (@szRecordID, @szNewStLicNo, 'MERGED RECORD '+@szAssocRec1, GETDATE(), GETDATE())

					SET @szNotes = NULL
					SELECT @dtStLicIss = ST_LIC_ISSUE, @dtStLicExp = ST_LIC_EXPIRE FROM AEC_MAIN WHERE ST_LIC_NO = @szAssocRec1
					SET @szNotes = 'MERGED AEC RECORD: ' + @szAssocRec1
					IF @dtStLicIss IS NOT NULL AND @dtStLicIss <> ''
						SET @szNotes = @szNotes + '   ISSUED:  ' + CAST(@dtStLicIss AS VARCHAR)
					IF @dtStLicExp IS NOT NULL AND @dtStLicExp <> ''
						SET @szNotes = @szNotes + '   EXPIRED:  ' + CAST(@dtStLicExp AS VARCHAR)

					INSERT INTO PRMRY_NOTES (ACTIVITYGROUP, ACTIVITYNO, ACTIVITYRECORDID, SUBGROUP, SUBGROUPRECORDID, NOTES, DATEENTERED)
										VALUES ('AEC', @szNewStLicNo, @szNeedUpdtLicNoID, 'ACTION', @szRecordID, @szNotes, GETDATE())

				-- update module and other associated records with new merged aec st_lic_no
				UPDATE PERMIT_PEOPLE SET BUS_LICENSE = @szNewStLicNo, ID = 'C:'+ @szNewStLicNo WHERE SUBSTRING(ID,3,20) = @szAssocRec1 OR ISNULL(BUS_LICENSE,' ') = @szAssocRec1
				UPDATE PROJECT_PEOPLE SET BUS_LICENSE = @szNewStLicNo, ID = 'C:'+ @szNewStLicNo WHERE SUBSTRING(ID,3,20) = @szAssocRec1 OR ISNULL(BUS_LICENSE,' ') = @szAssocRec1
				UPDATE GEO_PEOPLE SET BUS_LICENSE = @szNewStLicNo, ID = 'C:'+ @szNewStLicNo WHERE SUBSTRING(ID,3,20) = @szAssocRec1 OR ISNULL(BUS_LICENSE,' ') = @szAssocRec1
			END

			IF @szAssocRec2 IS NOT NULL AND @szAssocRec2 <> '' AND ISNULL(@szAssocRec2,' ') <> @szStLicNo
			BEGIN
				-- Insert action record to log merge activity
					-- Set unique RecordID
					SET @szRecordID = NULL
					-- Reset counter for unique number string at end of RecordID
					SET @lCounter = @lCounter + 1
					SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
						+ right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)
						+ right(('000000'+ ltrim(str(@lCounter))),6)

					INSERT INTO AEC_ACTIONS (RECORDID, ST_LIC_NO, ACTION_TYPE, ACTION_DATE, COMPLETED_DATE)
										VALUES (@szRecordID, @szNewStLicNo, 'MERGED RECORD '+@szAssocRec2, GETDATE(), GETDATE())

					SET @szNotes = NULL
					SELECT @dtStLicIss = ST_LIC_ISSUE, @dtStLicExp = ST_LIC_EXPIRE FROM AEC_MAIN WHERE ST_LIC_NO = @szAssocRec2
					SET @szNotes = 'MERGED AEC RECORD: ' + @szAssocRec2
					IF @dtStLicIss IS NOT NULL AND @dtStLicIss <> ''
						SET @szNotes = @szNotes + '   ISSUED:  ' + CAST(@dtStLicIss AS VARCHAR)
					IF @dtStLicExp IS NOT NULL AND @dtStLicExp <> ''
						SET @szNotes = @szNotes + '   EXPIRED:  ' + CAST(@dtStLicExp AS VARCHAR)

					INSERT INTO PRMRY_NOTES (ACTIVITYGROUP, ACTIVITYNO, ACTIVITYRECORDID, SUBGROUP, SUBGROUPRECORDID, NOTES, DATEENTERED)
										VALUES ('AEC', @szNewStLicNo, @szNeedUpdtLicNoID, 'ACTION', @szRecordID, @szNotes, GETDATE())

				-- update module and other associated records with new merged aec st_lic_no
				UPDATE PERMIT_PEOPLE SET BUS_LICENSE = @szNewStLicNo, ID = 'C:'+ @szNewStLicNo WHERE SUBSTRING(ID,3,20) = @szAssocRec2 OR ISNULL(BUS_LICENSE,' ') = @szAssocRec2
				UPDATE PROJECT_PEOPLE SET BUS_LICENSE = @szNewStLicNo, ID = 'C:'+ @szNewStLicNo WHERE SUBSTRING(ID,3,20) = @szAssocRec2 OR ISNULL(BUS_LICENSE,' ') = @szAssocRec2
				UPDATE GEO_PEOPLE SET BUS_LICENSE = @szNewStLicNo, ID = 'C:'+ @szNewStLicNo WHERE SUBSTRING(ID,3,20) = @szAssocRec2 OR ISNULL(BUS_LICENSE,' ') = @szAssocRec2
			END

			IF @szAssocRec3 IS NOT NULL AND @szAssocRec3 <> '' AND ISNULL(@szAssocRec3,' ') <> @szStLicNo
			BEGIN
				-- Insert action record to log merge activity
					-- Set unique RecordID
					SET @szRecordID = NULL
					-- Reset counter for unique number string at end of RecordID
					SET @lCounter = @lCounter + 1
					SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
						+ right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)
						+ right(('000000'+ ltrim(str(@lCounter))),6)

					INSERT INTO AEC_ACTIONS (RECORDID, ST_LIC_NO, ACTION_TYPE, ACTION_DATE, COMPLETED_DATE)
										VALUES (@szRecordID, @szNewStLicNo, 'MERGED RECORD '+@szAssocRec3, GETDATE(), GETDATE())

					SET @szNotes = NULL
					SELECT @dtStLicIss = ST_LIC_ISSUE, @dtStLicExp = ST_LIC_EXPIRE FROM AEC_MAIN WHERE ST_LIC_NO = @szAssocRec3
					SET @szNotes = 'MERGED AEC RECORD: ' + @szAssocRec3
					IF @dtStLicIss IS NOT NULL AND @dtStLicIss <> ''
						SET @szNotes = @szNotes + '   ISSUED:  ' + CAST(@dtStLicIss AS VARCHAR)
					IF @dtStLicExp IS NOT NULL AND @dtStLicExp <> ''
						SET @szNotes = @szNotes + '   EXPIRED:  ' + CAST(@dtStLicExp AS VARCHAR)

					INSERT INTO PRMRY_NOTES (ACTIVITYGROUP, ACTIVITYNO, ACTIVITYRECORDID, SUBGROUP, SUBGROUPRECORDID, NOTES, DATEENTERED)
										VALUES ('AEC', @szNewStLicNo, @szNeedUpdtLicNoID, 'ACTION', @szRecordID, @szNotes, GETDATE())

				-- update module and other associated records with new merged aec st_lic_no
				UPDATE PERMIT_PEOPLE SET BUS_LICENSE = @szNewStLicNo, ID = 'C:'+ @szNewStLicNo WHERE SUBSTRING(ID,3,20) = @szAssocRec3 OR ISNULL(BUS_LICENSE,' ') = @szAssocRec3
				UPDATE PROJECT_PEOPLE SET BUS_LICENSE = @szNewStLicNo, ID = 'C:'+ @szNewStLicNo WHERE SUBSTRING(ID,3,20) = @szAssocRec3 OR ISNULL(BUS_LICENSE,' ') = @szAssocRec3
				UPDATE GEO_PEOPLE SET BUS_LICENSE = @szNewStLicNo, ID = 'C:'+ @szNewStLicNo WHERE SUBSTRING(ID,3,20) = @szAssocRec3 OR ISNULL(BUS_LICENSE,' ') = @szAssocRec3
			END

			IF @szAssocRec4 IS NOT NULL AND @szAssocRec4 <> '' AND ISNULL(@szAssocRec4,' ') <> @szStLicNo
			BEGIN
				-- Insert action record to log merge activity
					-- Set unique RecordID
					SET @szRecordID = NULL
					-- Reset counter for unique number string at end of RecordID
					SET @lCounter = @lCounter + 1
					SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
						+ right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)
						+ right(('000000'+ ltrim(str(@lCounter))),6)

					INSERT INTO AEC_ACTIONS (RECORDID, ST_LIC_NO, ACTION_TYPE, ACTION_DATE, COMPLETED_DATE)
										VALUES (@szRecordID, @szNewStLicNo, 'MERGED RECORD '+@szAssocRec4, GETDATE(), GETDATE())

					SET @szNotes = NULL
					SELECT @dtStLicIss = ST_LIC_ISSUE, @dtStLicExp = ST_LIC_EXPIRE FROM AEC_MAIN WHERE ST_LIC_NO = @szAssocRec4
					SET @szNotes = 'MERGED AEC RECORD: ' + @szAssocRec4
					IF @dtStLicIss IS NOT NULL AND @dtStLicIss <> ''
						SET @szNotes = @szNotes + '   ISSUED:  ' + CAST(@dtStLicIss AS VARCHAR)
					IF @dtStLicExp IS NOT NULL AND @dtStLicExp <> ''
						SET @szNotes = @szNotes + '   EXPIRED:  ' + CAST(@dtStLicExp AS VARCHAR)

					INSERT INTO PRMRY_NOTES (ACTIVITYGROUP, ACTIVITYNO, ACTIVITYRECORDID, SUBGROUP, SUBGROUPRECORDID, NOTES, DATEENTERED)
										VALUES ('AEC', @szNewStLicNo, @szNeedUpdtLicNoID, 'ACTION', @szRecordID, @szNotes, GETDATE())

				-- update module and other associated records with new merged aec st_lic_no
				UPDATE PERMIT_PEOPLE SET BUS_LICENSE = @szNewStLicNo, ID = 'C:'+ @szNewStLicNo WHERE SUBSTRING(ID,3,20) = @szAssocRec4 OR ISNULL(BUS_LICENSE,' ') = @szAssocRec4
				UPDATE PROJECT_PEOPLE SET BUS_LICENSE = @szNewStLicNo, ID = 'C:'+ @szNewStLicNo WHERE SUBSTRING(ID,3,20) = @szAssocRec4 OR ISNULL(BUS_LICENSE,' ') = @szAssocRec4
				UPDATE GEO_PEOPLE SET BUS_LICENSE = @szNewStLicNo, ID = 'C:'+ @szNewStLicNo WHERE SUBSTRING(ID,3,20) = @szAssocRec4 OR ISNULL(BUS_LICENSE,' ') = @szAssocRec4
			END

			IF @szAssocRec5 IS NOT NULL AND @szAssocRec5 <> '' AND ISNULL(@szAssocRec5,' ') <> @szStLicNo
			BEGIN
				-- Insert action record to log merge activity
					-- Set unique RecordID
					SET @szRecordID = NULL
					-- Reset counter for unique number string at end of RecordID
					SET @lCounter = @lCounter + 1
					SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
						+ right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)
						+ right(('000000'+ ltrim(str(@lCounter))),6)

					INSERT INTO AEC_ACTIONS (RECORDID, ST_LIC_NO, ACTION_TYPE, ACTION_DATE, COMPLETED_DATE)
										VALUES (@szRecordID, @szNewStLicNo, 'MERGED RECORD '+@szAssocRec5, GETDATE(), GETDATE())

					SET @szNotes = NULL
					SELECT @dtStLicIss = ST_LIC_ISSUE, @dtStLicExp = ST_LIC_EXPIRE FROM AEC_MAIN WHERE ST_LIC_NO = @szAssocRec5
					SET @szNotes = 'MERGED AEC RECORD: ' + @szAssocRec5
					IF @dtStLicIss IS NOT NULL AND @dtStLicIss <> ''
						SET @szNotes = @szNotes + '   ISSUED:  ' + CAST(@dtStLicIss AS VARCHAR)
					IF @dtStLicExp IS NOT NULL AND @dtStLicExp <> ''
						SET @szNotes = @szNotes + '   EXPIRED:  ' + CAST(@dtStLicExp AS VARCHAR)

					INSERT INTO PRMRY_NOTES (ACTIVITYGROUP, ACTIVITYNO, ACTIVITYRECORDID, SUBGROUP, SUBGROUPRECORDID, NOTES, DATEENTERED)
										VALUES ('AEC', @szNewStLicNo, @szNeedUpdtLicNoID, 'ACTION', @szRecordID, @szNotes, GETDATE())

				-- update module and other associated records with new merged aec st_lic_no
				UPDATE PERMIT_PEOPLE SET BUS_LICENSE = @szNewStLicNo, ID = 'C:'+ @szNewStLicNo WHERE SUBSTRING(ID,3,20) = @szAssocRec5 OR ISNULL(BUS_LICENSE,' ') = @szAssocRec5
				UPDATE PROJECT_PEOPLE SET BUS_LICENSE = @szNewStLicNo, ID = 'C:'+ @szNewStLicNo WHERE SUBSTRING(ID,3,20) = @szAssocRec5 OR ISNULL(BUS_LICENSE,' ') = @szAssocRec5
				UPDATE GEO_PEOPLE SET BUS_LICENSE = @szNewStLicNo, ID = 'C:'+ @szNewStLicNo WHERE SUBSTRING(ID,3,20) = @szAssocRec5 OR ISNULL(BUS_LICENSE,' ') = @szAssocRec5
			END

			IF @szAssocRec6 IS NOT NULL AND @szAssocRec6 <> '' AND ISNULL(@szAssocRec6,' ') <> @szStLicNo
			BEGIN
				-- Insert action record to log merge activity
					-- Set unique RecordID
					SET @szRecordID = NULL
					-- Reset counter for unique number string at end of RecordID
					SET @lCounter = @lCounter + 1
					SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
						+ right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)
						+ right(('000000'+ ltrim(str(@lCounter))),6)

					INSERT INTO AEC_ACTIONS (RECORDID, ST_LIC_NO, ACTION_TYPE, ACTION_DATE, COMPLETED_DATE)
										VALUES (@szRecordID, @szNewStLicNo, 'MERGED RECORD '+@szAssocRec6, GETDATE(), GETDATE())

					SET @szNotes = NULL
					SELECT @dtStLicIss = ST_LIC_ISSUE, @dtStLicExp = ST_LIC_EXPIRE FROM AEC_MAIN WHERE ST_LIC_NO = @szAssocRec6
					SET @szNotes = 'MERGED AEC RECORD: ' + @szAssocRec6
					IF @dtStLicIss IS NOT NULL AND @dtStLicIss <> ''
						SET @szNotes = @szNotes + '   ISSUED:  ' + CAST(@dtStLicIss AS VARCHAR)
					IF @dtStLicExp IS NOT NULL AND @dtStLicExp <> ''
						SET @szNotes = @szNotes + '   EXPIRED:  ' + CAST(@dtStLicExp AS VARCHAR)

					INSERT INTO PRMRY_NOTES (ACTIVITYGROUP, ACTIVITYNO, ACTIVITYRECORDID, SUBGROUP, SUBGROUPRECORDID, NOTES, DATEENTERED)
										VALUES ('AEC', @szNewStLicNo, @szNeedUpdtLicNoID, 'ACTION', @szRecordID, @szNotes, GETDATE())

				-- update module and other associated records with new merged aec st_lic_no
				UPDATE PERMIT_PEOPLE SET BUS_LICENSE = @szNewStLicNo, ID = 'C:'+ @szNewStLicNo WHERE SUBSTRING(ID,3,20) = @szAssocRec6 OR ISNULL(BUS_LICENSE,' ') = @szAssocRec6
				UPDATE PROJECT_PEOPLE SET BUS_LICENSE = @szNewStLicNo, ID = 'C:'+ @szNewStLicNo WHERE SUBSTRING(ID,3,20) = @szAssocRec6 OR ISNULL(BUS_LICENSE,' ') = @szAssocRec6
				UPDATE GEO_PEOPLE SET BUS_LICENSE = @szNewStLicNo, ID = 'C:'+ @szNewStLicNo WHERE SUBSTRING(ID,3,20) = @szAssocRec6 OR ISNULL(BUS_LICENSE,' ') = @szAssocRec6
			END

			IF @szAssocRec7 IS NOT NULL AND @szAssocRec7 <> '' AND ISNULL(@szAssocRec7,' ') <> @szStLicNo
			BEGIN
				-- Insert action record to log merge activity
					-- Set unique RecordID
					SET @szRecordID = NULL
					-- Reset counter for unique number string at end of RecordID
					SET @lCounter = @lCounter + 1
					SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
						+ right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
						+ right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)
						+ right(('000000'+ ltrim(str(@lCounter))),6)

					INSERT INTO AEC_ACTIONS (RECORDID, ST_LIC_NO, ACTION_TYPE, ACTION_DATE, COMPLETED_DATE)
										VALUES (@szRecordID, @szNewStLicNo, 'MERGED RECORD '+@szAssocRec7, GETDATE(), GETDATE())

					SET @szNotes = NULL
					SELECT @dtStLicIss = ST_LIC_ISSUE, @dtStLicExp = ST_LIC_EXPIRE FROM AEC_MAIN WHERE ST_LIC_NO = @szAssocRec7
					SET @szNotes = 'MERGED AEC RECORD: ' + @szAssocRec7
					IF @dtStLicIss IS NOT NULL AND @dtStLicIss <> ''
						SET @szNotes = @szNotes + '   ISSUED:  ' + CAST(@dtStLicIss AS VARCHAR)
					IF @dtStLicExp IS NOT NULL AND @dtStLicExp <> ''
						SET @szNotes = @szNotes + '   EXPIRED:  ' + CAST(@dtStLicExp AS VARCHAR)

					INSERT INTO PRMRY_NOTES (ACTIVITYGROUP, ACTIVITYNO, ACTIVITYRECORDID, SUBGROUP, SUBGROUPRECORDID, NOTES, DATEENTERED)
										VALUES ('AEC', @szNewStLicNo, @szNeedUpdtLicNoID, 'ACTION', @szRecordID, @szNotes, GETDATE())

				-- update module and other associated records with new merged aec st_lic_no
				UPDATE PERMIT_PEOPLE SET BUS_LICENSE = @szNewStLicNo, ID = 'C:'+ @szNewStLicNo WHERE SUBSTRING(ID,3,20) = @szAssocRec7 OR ISNULL(BUS_LICENSE,' ') = @szAssocRec7
				UPDATE PROJECT_PEOPLE SET BUS_LICENSE = @szNewStLicNo, ID = 'C:'+ @szNewStLicNo WHERE SUBSTRING(ID,3,20) = @szAssocRec7 OR ISNULL(BUS_LICENSE,' ') = @szAssocRec7
				UPDATE GEO_PEOPLE SET BUS_LICENSE = @szNewStLicNo, ID = 'C:'+ @szNewStLicNo WHERE SUBSTRING(ID,3,20) = @szAssocRec7 OR ISNULL(BUS_LICENSE,' ') = @szAssocRec7
			END
		END

	-- Reset Variables
	SELECT @szRecordid = NULL, @szAECRecID = NULL, @dtStLicIss = NULL, @szNotes = NULL, @szNewStLicNo = NULL, @szNeedUpdtLicNoID = NULL

	Fetch Next From cGetMasterForReview Into @szStLicNo, @szCompany, @szAECType, @szSubType, @szLicGrade1, @szLicGrade2, @szMatch2, @szLicGrade3, @szMatch3, @szLicGrade4,
											 @szMatch4, @szLicGrade5, @szMatch5, @szLicGrade6, @szMatch6, @szAssocRec1, @szAssocRec2, @szAssocRec3, @szAssocRec4,
											 @szAssocRec5, @szAssocRec6, @szAssocRec7, @szAssocRec8, @szAssocRec9, @szAssocRec10
	End --While
	Close cGetMasterForReview 
	Deallocate cGetMasterForReview
	--End updates

delete from aec_main where st_lic_no in (select match2 from masterforreview where match2 is not null and match2 <> st_lic_no)
delete from aec_udf where st_lic_no in (select match2 from masterforreview where match2 is not null and match2 <> st_lic_no)
delete from aec_insurance where st_lic_no in (select match2 from masterforreview where match2 is not null and match2 <> st_lic_no)
delete from aec_people where st_lic_no in (select match2 from masterforreview where match2 is not null and match2 <> st_lic_no)

delete from aec_main where st_lic_no in (select match3 from masterforreview where match3 is not null and match3 <> st_lic_no)
delete from aec_udf where st_lic_no in (select match3 from masterforreview where match3 is not null and match3 <> st_lic_no)
delete from aec_insurance where st_lic_no in (select match3 from masterforreview where match3 is not null and match3 <> st_lic_no)
delete from aec_people where st_lic_no in (select match3 from masterforreview where match3 is not null and match3 <> st_lic_no)

delete from aec_main where st_lic_no in (select match4 from masterforreview where match4 is not null and match4 <> st_lic_no)
delete from aec_udf where st_lic_no in (select match4 from masterforreview where match4 is not null and match4 <> st_lic_no)
delete from aec_insurance where st_lic_no in (select match4 from masterforreview where match4 is not null and match4 <> st_lic_no)
delete from aec_people where st_lic_no in (select match4 from masterforreview where match4 is not null and match4 <> st_lic_no)

delete from aec_main where st_lic_no in (select match5 from masterforreview where match5 is not null and match5 <> st_lic_no)
delete from aec_udf where st_lic_no in (select match5 from masterforreview where match5 is not null and match5 <> st_lic_no)
delete from aec_insurance where st_lic_no in (select match5 from masterforreview where match5 is not null and match5 <> st_lic_no)
delete from aec_people where st_lic_no in (select match5 from masterforreview where match5 is not null and match5 <> st_lic_no)

delete from aec_main where st_lic_no in (select match6 from masterforreview where match6 is not null and match6 <> st_lic_no)
delete from aec_udf where st_lic_no in (select match6 from masterforreview where match6 is not null and match6 <> st_lic_no)
delete from aec_insurance where st_lic_no in (select match6 from masterforreview where match6 is not null and match6 <> st_lic_no)
delete from aec_people where st_lic_no in (select match6 from masterforreview where match6 is not null and match6 <> st_lic_no)

delete from aec_main where st_lic_no in (select assoc_record1 from masterforreview where assoc_record1 is not null and assoc_record1 <> st_lic_no)
delete from aec_udf where st_lic_no in (select assoc_record1 from masterforreview where assoc_record1 is not null and assoc_record1 <> st_lic_no)
delete from aec_insurance where st_lic_no in (select assoc_record1 from masterforreview where assoc_record1 is not null and assoc_record1 <> st_lic_no)
delete from aec_people where st_lic_no in (select assoc_record1 from masterforreview where assoc_record1 is not null and assoc_record1 <> st_lic_no)

delete from aec_main where st_lic_no in (select assoc_record2 from masterforreview where assoc_record2 is not null and assoc_record2 <> st_lic_no)
delete from aec_udf where st_lic_no in (select assoc_record2 from masterforreview where assoc_record2 is not null and assoc_record2 <> st_lic_no)
delete from aec_insurance where st_lic_no in (select assoc_record2 from masterforreview where assoc_record2 is not null and assoc_record2 <> st_lic_no)
delete from aec_people where st_lic_no in (select assoc_record2 from masterforreview where assoc_record2 is not null and assoc_record2 <> st_lic_no)

delete from aec_main where st_lic_no in (select assoc_record3 from masterforreview where assoc_record3 is not null and assoc_record3 <> st_lic_no)
delete from aec_udf where st_lic_no in (select assoc_record3 from masterforreview where assoc_record3 is not null and assoc_record3 <> st_lic_no)
delete from aec_insurance where st_lic_no in (select assoc_record3 from masterforreview where assoc_record3 is not null and assoc_record3 <> st_lic_no)
delete from aec_people where st_lic_no in (select assoc_record3 from masterforreview where assoc_record3 is not null and assoc_record3 <> st_lic_no)

delete from aec_main where st_lic_no in (select assoc_record4 from masterforreview where assoc_record4 is not null and assoc_record4 <> st_lic_no)
delete from aec_udf where st_lic_no in (select assoc_record4 from masterforreview where assoc_record4 is not null and assoc_record4 <> st_lic_no)
delete from aec_insurance where st_lic_no in (select assoc_record4 from masterforreview where assoc_record4 is not null and assoc_record4 <> st_lic_no)
delete from aec_people where st_lic_no in (select assoc_record4 from masterforreview where assoc_record4 is not null and assoc_record4 <> st_lic_no)

delete from aec_main where st_lic_no in (select assoc_record5 from masterforreview where assoc_record5 is not null and assoc_record5 <> st_lic_no)
delete from aec_udf where st_lic_no in (select assoc_record5 from masterforreview where assoc_record5 is not null and assoc_record5 <> st_lic_no)
delete from aec_insurance where st_lic_no in (select assoc_record5 from masterforreview where assoc_record5 is not null and assoc_record5 <> st_lic_no)
delete from aec_people where st_lic_no in (select assoc_record5 from masterforreview where assoc_record5 is not null and assoc_record5 <> st_lic_no)

delete from aec_main where st_lic_no in (select assoc_record6 from masterforreview where assoc_record6 is not null and assoc_record6 <> st_lic_no)
delete from aec_udf where st_lic_no in (select assoc_record6 from masterforreview where assoc_record6 is not null and assoc_record6 <> st_lic_no)
delete from aec_insurance where st_lic_no in (select assoc_record6 from masterforreview where assoc_record6 is not null and assoc_record6 <> st_lic_no)
delete from aec_people where st_lic_no in (select assoc_record6 from masterforreview where assoc_record6 is not null and assoc_record6 <> st_lic_no)

delete from aec_main where st_lic_no in (select assoc_record7 from masterforreview where assoc_record7 is not null and assoc_record7 <> st_lic_no)
delete from aec_udf where st_lic_no in (select assoc_record7 from masterforreview where assoc_record7 is not null and assoc_record7 <> st_lic_no)
delete from aec_insurance where st_lic_no in (select assoc_record7 from masterforreview where assoc_record7 is not null and assoc_record7 <> st_lic_no)
delete from aec_people where st_lic_no in (select assoc_record7 from masterforreview where assoc_record7 is not null and assoc_record7 <> st_lic_no)

delete from aec_main where st_lic_no in (select assoc_record8 from masterforreview where assoc_record8 is not null and assoc_record8 <> st_lic_no)
delete from aec_udf where st_lic_no in (select assoc_record8 from masterforreview where assoc_record8 is not null and assoc_record8 <> st_lic_no)
delete from aec_insurance where st_lic_no in (select assoc_record8 from masterforreview where assoc_record8 is not null and assoc_record8 <> st_lic_no)
delete from aec_people where st_lic_no in (select assoc_record8 from masterforreview where assoc_record8 is not null and assoc_record8 <> st_lic_no)

delete from aec_main where st_lic_no in (select assoc_record9 from masterforreview where assoc_record9 is not null and assoc_record9 <> st_lic_no)
delete from aec_udf where st_lic_no in (select assoc_record9 from masterforreview where assoc_record9 is not null and assoc_record9 <> st_lic_no)
delete from aec_insurance where st_lic_no in (select assoc_record9 from masterforreview where assoc_record9 is not null and assoc_record9 <> st_lic_no)
delete from aec_people where st_lic_no in (select assoc_record9 from masterforreview where assoc_record9 is not null and assoc_record9 <> st_lic_no)

delete from aec_main where st_lic_no in (select assoc_record10 from masterforreview where assoc_record10 is not null and assoc_record10 <> st_lic_no)
delete from aec_udf where st_lic_no in (select assoc_record10 from masterforreview where assoc_record10 is not null and assoc_record10 <> st_lic_no)
delete from aec_insurance where st_lic_no in (select assoc_record10 from masterforreview where assoc_record10 is not null and assoc_record10 <> st_lic_no)
delete from aec_people where st_lic_no in (select assoc_record10 from masterforreview where assoc_record10 is not null and assoc_record10 <> st_lic_no)

End
GO
