USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[lp_focus_1to2fam_15days_review]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[lp_focus_1to2fam_15days_review]
GO
/****** Object:  StoredProcedure [dbo].[lp_focus_1to2fam_15days_review]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* 
--Ychavez 1/11/2015 - FOR FOCUS Measures report
-------------------------------------------------------------------------------------------
--1 1-2 family home reviews provided 
Criteria:
Count total reviews completed with return date
Review name  BUILDING PERMIT.
Type: New Single Family, Residential
Subtypes: NEW SF DETACHED, NEW DUPLEX, NEW SF ATTACHED
Count total number of reviews completed for the month that is queried.

exec lp_focus_1to2fam_15days_review @APPLIED_FROM = '02/01/2014' , @APPLIED_TO = '03/01/2014'
-------------------------------------------------------------------------------------------
*/
CREATE procedure [dbo].[lp_focus_1to2fam_15days_review]
		@APPLIED_FROM datetime = null,
		@APPLIED_TO datetime = null
as
begin

--------------------------------------------------------------------------------
--80% of 1-2 family home plan reviews completed within 15 business days.--
--CALCULATE BY APPLIED - DATE_RECEIVED
-----------------------------------------------------------------------------
--DROP TABLE #TMP_CALC_FAM_PLAN_TOTALS
SELECT DATEDIFF(DAY, APPLIED, DATE_RECEIVED) AS DATE_DIFF, APPLIED, DATE_RECEIVED, P.PERMIT_NO, 1 AS counted

INTO #TMP_CALC_FAM_PLAN_TOTALS
 FROM PERMIT_MAIN P
JOIN PERMIT_REVIEWS R ON P.PERMIT_NO = R.pERMIT_NO AND REVIEWTYPE = 'BUILDING PERMIT'
WHERE APPLIED BETWEEN  @APPLIED_FROM and @APPLIED_TO --SAME AS APPLIED DATE
AND PERMITSUBTYPE IN ('NEW SF DETACHED', 'NEW SF ATTACHED', 'NEW DUPLEX')
ORDER BY APPLIED
-----------------------------------------------------------------------------------------
--DROP TABLE #tmp_calc_measure_tot
-----------------------------------------------------------------------------------------
select CASE WHEN date_diff IS NULL THEN SUM(counted) END AS no_permits_not_rcvd,
CASE WHEN permit_no LIKE '%-%' THEN SUM(permit_no) END AS  permits_rcvd 
 into #tmp_calc_measure_tot
  from #TMP_CALC_FAM_PLAN_TOTALS
  	
 
  SELECT * FROM  #TMP_CALC_FAM_PLAN_TOTALS
 -----------------------------------------------------------------------------------------
--
-----------------------------------------------------------------------------------------

select total_permits
from #tmp_calc_measure_tot
--group by total_permits, IN_MEASURE_COUNT

SELECT * FROM PERMIT_REVIEWS 

end
GO
