USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_LicenseRenew_CalculateBalanceForward]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_LicenseRenew_CalculateBalanceForward]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_LicenseRenew_CalculateBalanceForward]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  -- =============================================
-- 1/7/2015: only CREATES the balance forwards in the table, does not create the fees
--			see tsp_T9_LicenseRenew_CreateBalanceForwardFees
-- =============================================
CREATE PROCEDURE [dbo].[tsp_T9_LicenseRenew_CalculateBalanceForward] 
(
	@table_name nvarchar(100)='LicenseRenew_Review_Fees',
	@user nvarchar(20),
	@PARAMETERSETID INT
)
		
AS
BEGIN
	SET NOCOUNT ON;

INSERT INTO LicenseRenew_EventLog (SourceMode,EventText,USERID) VALUES ('CALBALF', 'Calculating balance forwards.',@USER)

DELETE FROM LicenseRenew_BalanceForwards

insert into [dbo].[LicenseRenew_BalanceForwards]
([license_no],[BF_Amount],[Notes])
    select
    A.license_no,sum(amount) ,
         stuff((
         select ', ' +'$'+ cast(B.amount as varchar)+':'+B.description
         from [license2_Fees] B
         WHERE A.license_no=B.license_no and paid=0
         for xml path('')),1,1,'')
    From [license2_Fees] A WHERE A.LICENSE_NO IN (SELECT LICENSE_NO FROM LicenseRenew_License_List) group by license_no, paid having paid=0

	delete from [LicenseRenew_BalanceForwards] where bf_amount is null


	-- update existing balance forwards
	declare @sql nvarchar(max)
	set @sql='update ' +@table_name +' set amount=b.BF_Amount, assessed_by= '''+@user+''' , ASSESSED_DATE= getdate() from '
	 +@table_name +' inner join [LicenseRenew_BalanceForwards] b on ' +@table_name +'.license_no=b.license_no'
	exec (@sql)
/*
	set @sql=''

	-- add new balance forwards
	-- for those that exist in the balance forward, but do not have an entry for the license
	DECLARE @BALFWDCODE NVARCHAR(12)
	SET @BALFWDCODE = (SELECT ParameterValue FROM LicenseRenew_License_Parameters WHERE ActiveMode = 'BALANCE' AND ParameterSetID = @PARAMETERSETID)

set @SQL='
	insert into ' + @table_name +' 
		(  [RECORDID],license_no,[CODE],[DESCRIPTION],[DETAIL],[FEETYPE],[AMOUNT],[ASSESSED_DATE],[PAID],[QUANTITY])
	SELECT ''GLR:'' + right((''00'' + str(DATEPART(yy, GETDATE()))),2)
			 + right((''00'' + ltrim(str(DATEPART(mm, GETDATE())))),2)
			 + right((''00'' + ltrim(str(DATEPART(d, GETDATE())))),2)
			 + right((''00'' + ltrim(str(DATEPART(hh, GETDATE())))),2)
			 + right((''00'' + ltrim(str(DATEPART(n, GETDATE())))),2)
			 + right((''00'' + ltrim(str(DATEPART(ss, GETDATE())))),2)
			 + right((''000000''+ CONVERT(varchar,ROW_NUMBER() OVER (ORDER BY (SELECT NULL))+111)),6) RECid,
			 tbl.* from
			 (select license_no,''' + @BALFWDCODE + ''' as  code, cast(DatePart(year,getdate()) as varchar)+'' Balance Forward'' as description,''0'' 
			 as detail,
			 ''FEE'' as feetype,[BF_Amount], getdate() as assest_date,''0'' as paid,''1'' as quantity
			 from [dbo].[LicenseRenew_BalanceForwards] where license_no not in (select license_no from ' + @table_name + ' where 
			 code=''' + @BALFWDCODE + ''' and paid=0)) as tbl'

this one works for the review tables because they have the calc value column
set @SQL='
	insert into ' + @table_name +' 
		(  [RECORDID],license_no,[CODE],[DESCRIPTION],[DETAIL],[FEETYPE],[AMOUNT],[ASSESSED_DATE],[PAID],[QUANTITY],[Calc_Value])
	SELECT ''GLR:'' + right((''00'' + str(DATEPART(yy, GETDATE()))),2)
			 + right((''00'' + ltrim(str(DATEPART(mm, GETDATE())))),2)
			 + right((''00'' + ltrim(str(DATEPART(d, GETDATE())))),2)
			 + right((''00'' + ltrim(str(DATEPART(hh, GETDATE())))),2)
			 + right((''00'' + ltrim(str(DATEPART(n, GETDATE())))),2)
			 + right((''00'' + ltrim(str(DATEPART(ss, GETDATE())))),2)
			 + right((''000000''+ CONVERT(varchar,ROW_NUMBER() OVER (ORDER BY (SELECT NULL))+111)),6) RECid,
			 tbl.* from
			 (select license_no,''BALFWD'' as  code, cast(DatePart(year,getdate()) as varchar)+'' Balance Forward'' as description,''0'' 
			 as detail,
			 ''FEE'' as feetype,[BF_Amount], getdate() as assest_date,''0'' as paid,''1'' as quantity, [BF_Amount] as Calc_Value
			 from [dbo].[LicenseRenew_BalanceForwards] where license_no not in (select license_no from ' + @table_name + ' where 
			 code=''BALFWD'' and paid=0)) as tbl'
*/

--exec (@SQL)

END



GO
