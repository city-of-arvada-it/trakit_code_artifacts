USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetSQLVersion]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[usp_GetSQLVersion]
GO
/****** Object:  StoredProcedure [dbo].[usp_GetSQLVersion]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_GetSQLVersion]
AS
select CAST(LEFT(CAST(SERVERPROPERTY('ProductVersion') AS NVARCHAR(MAX)),CHARINDEX('.',CAST(SERVERPROPERTY('ProductVersion') AS NVARCHAR(MAX))) - 1) AS INT)
GO
