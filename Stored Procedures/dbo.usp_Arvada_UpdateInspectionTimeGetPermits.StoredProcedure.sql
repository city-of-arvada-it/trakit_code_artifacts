USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_UpdateInspectionTimeGetPermits]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[usp_Arvada_UpdateInspectionTimeGetPermits]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_UpdateInspectionTimeGetPermits]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_Arvada_UpdateInspectionTimeGetPermits]
	@Inspectors nvarchar(max),
	@ScheduledDate datetime
as
/**********************************************************************************************************************
* Creator:      	 
* Date:         	 
* Title:			 
* Description:  	 
* Examples:    
			EXECUTE dbo.[usp_Arvada_UpdateInspectionTimeGetPermits] @Inspectors = ''

exec usp_Arvada_UpdateInspectionTimeGetPermits @Inspectors=N'DE',@ScheduledDate='2016-01-18 00:00:00'

select Field03 as Inspectors
from Prmry_InspectionControl
where Category = 'inspector'
and groupname = 'permits'
order by Field03 asc
					 
* Modifications:  	
  Developer Name		Date		Description
  ------------------	----------	----------------------------------------------------------- 
*
**********************************************************************************************************************/

set nocount on
 
 --------------------------
 --- Declare Variables   --
 --------------------------

 
 --------------------------
 --- Create Temp Tables  --
 --------------------------
 

	 
 --------------------------
 --- Body of Procedure   --
 --------------------------

 
select distinct 
		permit_no
from dbo.Permit_Inspections i
inner join [dbo].[tfn_SSRS_String_To_Table](@Inspectors,'|',1) as b
	on b.val = i.INSPECTOR
where
	datediff(day,i.scheduled_Date, @ScheduledDate) = 0
order by 
	permit_no
 
  
GO
