USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_export_CustomObj_ITR_only]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_export_CustomObj_ITR_only]
GO
/****** Object:  StoredProcedure [dbo].[tsp_export_CustomObj_ITR_only]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  

CREATE PROCEDURE [dbo].[tsp_export_CustomObj_ITR_only] (
@OutputPath Varchar(1000) = Null
)

AS
BEGIN
  SET NOCOUNT ON

    --RB Check if table exists before creating it
    IF OBJECT_ID('tempdb..#MyObjectHierarchy ') IS NOT NULL  
	drop table #MyObjectHierarchy 
      
  CREATE TABLE #MyObjectHierarchy 
   (
    HID int identity(1,1) not null primary key,
    ObjectId int,
    TYPE int,OBJECTTYPE AS CASE 
                             WHEN TYPE =  1 THEN 'FUNCTION' 
                             WHEN TYPE =  4 THEN 'VIEW' 
                             WHEN TYPE = 16 THEN 'PROCEDURE'
                             WHEN TYPE = 256 THEN 'TRIGGER'
                             ELSE ''
                           END,
   ONAME varchar(255), 
   OOWNER varchar(255), 
   SEQ int
   )
  --results table

   --RB Check if table exists before creating it
   IF OBJECT_ID('tempdb..#Results') IS NOT NULL  
	  drop table #Results 

  CREATE TABLE #Results(ResultsID int identity(1,1) not null,ResultsText varchar(max) )
  --list of objects in dependancy order
  INSERT #MyObjectHierarchy (TYPE,ONAME,OOWNER,SEQ)
    EXEC sp_msdependencies @intrans = 1 

Update #MyObjectHierarchy SET ObjectId = object_id(OOWNER + '.' + ONAME)

   DELETE FROM #MyObjectHierarchy WHERE objectid in(
    SELECT [object_id] FROM sys.synonyms UNION ALL
    SELECT [object_id] FROM master.sys.synonyms)
    
DELETE FROM #MyObjectHierarchy WHERE OBJECTTYPE = ''

DELETE FROM #MyObjectHierarchy WHERE  (
	(LEFT(ONAME,7) <> 'tvw_ITR' ) 
	and (LEFT(ONAME,7) <> 'tsp_ITR')
	)
	
--Temp only
--DELETE FROM #MyObjectHierarchy WHERE ONAME like 'tsp_SSRS%' OR ONAME like 'tvw_SSRS%' OR ONAME like 'tfn_SSRS%'
	
	
	--Select * From #MyObjectHierarchy
	--Return
  
  DECLARE
    @schemaname     varchar(255),
    @objname        varchar(255),
    @objecttype     varchar(20),
    @ModDate            Datetime,
    @FullObjectName varchar(510),
    @FullText           varchar(max),
    @sp                       varchar(max)
    set @FullText = ''


  DECLARE cur1 CURSOR FOR 
	SELECT OOWNER,ONAME,OBJECTTYPE FROM #MyObjectHierarchy ORDER BY HID
  OPEN cur1
  FETCH NEXT FROM cur1 INTO @schemaname,@objname,@objecttype
  WHILE @@fetch_status <> -1
	BEGIN		
	If  @@fetch_status <> -2
	 BEGIN
		SET @FullObjectName = QUOTENAME(@schemaname) + '.' + QUOTENAME(@objname)
		PRINT @objecttype + ' - ' + @FullObjectName
              
			IF @objecttype = 'FUNCTION'                  
                  BEGIN  
                             
                      select @FullText = @FullText + '
                      IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''' + @FullObjectName + '''))'
                      --select @FullText = @FullText + CHAR(10) + CHAR(13)  
                      select @FullText = @FullText +  '
                      DROP FUNCTION ' + @FullObjectName
                      --select @FullText = @FullText + CHAR(10) + CHAR(13)  
                      select @FullText = @FullText + '
                      GO
                      '
                      --select @FullText = @FullText + CHAR(10) + CHAR(13)  
                      select @FullText = @FullText +  ''        
                                    
                      select @sp =  definition from [sys].[sql_modules] WHERE object_id = OBJECT_ID(N'' + @FullObjectName + '')
                      
                      select @FullText = @FullText + @sp
                      --select @FullText = @FullText + CHAR(10) + CHAR(13)  
                      select @FullText = @FullText + '
                      GO
                      '
                      --select @FullText = @FullText + CHAR(10) + CHAR(13)  
                        
				END		
				IF @objecttype = 'VIEW'
                  BEGIN
                  
                      select @FullText = @FullText + '
                      IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N''' + @FullObjectName + '''))'   
                      --select @FullText = @FullText + CHAR(10) + CHAR(13)              
                      select @FullText = @FullText +  '
                      DROP VIEW ' + @FullObjectName
                      --select @FullText = @FullText + CHAR(10) + CHAR(13)  
                      select @FullText = @FullText + '
                      GO
                      '
                      --select @FullText = @FullText + CHAR(10) + CHAR(13)  
                      
                      select @sp =  definition from [sys].[sql_modules] WHERE object_id = OBJECT_ID(N'' + @FullObjectName + '')
                      
                      select @FullText = @FullText + @sp
                      --select @FullText = @FullText + CHAR(10) + CHAR(13)  
                      select @FullText = @FullText + '
                      GO
                      '
                      --select @FullText = @FullText + CHAR(10) + CHAR(13)  

                  END
            IF @objecttype = 'PROCEDURE'                 
                  BEGIN  
                                                                                   
					  select @FullText = @FullText + '
					  IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''' + @FullObjectName + '''))'
					  --select @FullText = @FullText + CHAR(10) + CHAR(13)  
					  select @FullText = @FullText +  '
					  DROP PROCEDURE ' + @FullObjectName
					  --select @FullText = @FullText + CHAR(10) + CHAR(13)  
					  select @FullText = @FullText + '
					  GO
					  '
					  --select @FullText = @FullText + CHAR(10) + CHAR(13)  
					  select @FullText = @FullText +  ''
                      
                      select @sp =  definition from [sys].[sql_modules] WHERE object_id = OBJECT_ID(N'' + @FullObjectName + '')
                          
                      select @FullText = @FullText + @sp
                      --select @FullText = @FullText + CHAR(10) + CHAR(13)  
                      select @FullText = @FullText + '
                      GO
                      '
                      --select @FullText = @FullText + CHAR(10) + CHAR(13)  

				END
				IF @objecttype = 'TRIGGER'                 
                  BEGIN  
                                                                                   
					  select @FullText = @FullText + '
					  IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''' + @FullObjectName + '''))'
					  --select @FullText = @FullText + CHAR(10) + CHAR(13)  
					  select @FullText = @FullText +  '
					  DROP TRIGGER ' + @FullObjectName
					  --select @FullText = @FullText + CHAR(10) + CHAR(13)  
					  select @FullText = @FullText + '
					  GO
					  '
					  --select @FullText = @FullText + CHAR(10) + CHAR(13)  
					  select @FullText = @FullText +  ''
                      
                      select @sp =  definition from [sys].[sql_modules] WHERE object_id = OBJECT_ID(N'' + @FullObjectName + '')
                          
                      select @FullText = @FullText + @sp
                      --select @FullText = @FullText + CHAR(10) + CHAR(13)  
                      select @FullText = @FullText + '
                      GO
                      '
                      --select @FullText = @FullText + CHAR(10) + CHAR(13)  

				END
				
			 END
            FETCH NEXT FROM cur1 INTO @schemaname,@objname,@objecttype
            
            
	END
  CLOSE cur1
  DEALLOCATE cur1
      
    
  SELECT @FullText AS [processing-instruction(x)] FOR XML PATH('')
  
END





GO
