USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_Security_SetPassword]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_Security_SetPassword]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_Security_SetPassword]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===========================================================================================================================
-- Author:		Erick Hampton
-- Create date: July 7, 2016
-- Description:	Insert the salt and hashed password for a user
-- ===========================================================================================================================
CREATE PROCEDURE [dbo].[tsp_T9_Security_SetPassword]

  @UserID varchar(4)
, @Salt varchar(128)
, @HashedPassword varchar(128)
, @Creator varchar(10)
, @CreationReason varchar(20)

AS
BEGIN
	
SET NOCOUNT ON;
	
insert into [Prmry_Security_UserData]  (UserID, Salt, HashedPassword, Creator, CreationReason, CreationDate) values (@UserID, @Salt, @HashedPassword, @Creator, @CreationReason, getdate())

END
GO
