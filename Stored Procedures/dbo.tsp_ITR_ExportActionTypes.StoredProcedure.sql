USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ITR_ExportActionTypes]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_ITR_ExportActionTypes]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ITR_ExportActionTypes]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  
					  
					  					  					  /*
tsp_ExportActionTypes @DBName='DEV', @FilePath='C:\CRW\iTrakitExports\', @FileName='ActionTypesExport'

Select * from Prmry_Vcodes

*/

CREATE Procedure [dbo].[tsp_ITR_ExportActionTypes](
@DBName Varchar(50),
@FilePath Varchar(1000),
@FileName Varchar(150),
@ToggleXP_CMDShell INT = 1
)

As

SET NOCOUNT ON

Declare @SQL Varchar(8000)

If @ToggleXP_CMDShell Is Null Set @ToggleXP_CMDShell = 1

IF EXISTS (SELECT * FROM tempdb.sys.objects WHERE object_id = OBJECT_ID(N'tempdb..ActionTypes') AND type in (N'U'))
	Drop Table tempdb..ActionTypes

Create Table tempdb..ActionTypes([RECORDID] Varchar(200),[GroupName] Varchar(200)
      ,[CODE] Varchar(200)
      ,[ORDERID] Varchar(200))


Insert Into tempdb..ActionTypes(RECORDID,GROUPNAME,[CODE],ORDERID)
Values('RECORDID','GROUPNAME','CODE','ORDERID')
Set @SQL = '
Insert Into tempdb..ActionTypes([RECORDID],[GROUPNAME],[CODE],[ORDERID])
Select 
RECORDID=CASE WHEN RECORDID = '''' THEN Null ELSE RECORDID END,
GROUPNAME=CASE WHEN GROUPNAME = '''' THEN Null ELSE GROUPNAME END,
[CODE]=CASE WHEN [CODE] = '''' THEN Null ELSE REPLACE(REPLACE(REPLACE([CODE],CHAR(9),''''),CHAR(10),''''),CHAR(13),'''') END,
ORDERID=CASE WHEN ORDERID Is Null THEN ''999'' ELSE Convert(Varchar,ORDERID) END
From ' + @DBName + '..Prmry_Vcodes
ORDER BY GroupName,CODE'
--Print(@SQL)
Exec(@SQL)

Select * From tempdb..ActionTypes


Declare @vDate Varchar(100)
Declare @OutputFileName Varchar(1000)

Set @vDate = Replace(Convert(Varchar,GetDate(),102),'.','_')
Print(@vDate)

--Set @OutputFileName = '"' + @FilePath + @FileName + '_' + @vDate + '.csv"'
Set @OutputFileName = '"' + @FilePath + @FileName + '.csv"'


If @ToggleXP_CMDShell = 1
 Begin
	--Enable xp_cmdshell
	Set @SQL = '
	EXEC sp_configure ''show advanced options'', 1
	RECONFIGURE
	EXEC sp_configure ''xp_cmdshell'', 1
	RECONFIGURE
	'
	Print(@SQL)
	Exec(@SQL)
 End


select @sql = 'bcp "select * from tempdb..ActionTypes" queryout ' + @OutputFileName + ' -c -t"|" -T -S'
+ @@servername
--Print(@SQL)
exec master..xp_cmdshell @sql

Drop Table tempdb..ActionTypes

Set @SQL = '
DELETE ' + @DBName + '.dbo.iTrakitFeeds Where [FileName] = ''' + @FileName + '.csv''

INSERT INTO ' + @DBName + '.dbo.iTrakitFeeds([filename],[data])
SELECT ''' + @FileName + '.csv'', * FROM OPENROWSET(
  BULK N''' + REPLACE(@OutputFileName,'"','') + ''', SINGLE_BLOB
) bt'
Print(@SQL)
Exec(@SQL)


If @ToggleXP_CMDShell = 1
 Begin
	-- Disable xp_cmdshell
	Set @SQL = '
	EXEC sp_configure ''xp_cmdshell'', 0
	RECONFIGURE
	EXEC sp_configure ''show advanced options'', 0
	RECONFIGURE
	'
	Print(@SQL)
	Exec(@SQL)
 End









GO
