USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_e3_Security_SetUserdata_Admin]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_e3_Security_SetUserdata_Admin]
GO
/****** Object:  StoredProcedure [dbo].[tsp_e3_Security_SetUserdata_Admin]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[tsp_e3_Security_SetUserdata_Admin]
  @UserName varchar(8000)
, @UserFullName varchar(8000)
, @UserEmail varchar(8000)
, @SecretQuestion varchar(8000)

AS
BEGIN

  SET NOCOUNT ON;

  -- AdminUserName
  IF NOT EXISTS (SELECT * FROM Etrakit3_Preferences WHERE PREF_NAME = 'AdminUserName')
  BEGIN
    INSERT INTO Etrakit3_Preferences VALUES ('AdminUserName', @UserName);
  END

  ELSE
  BEGIN
    UPDATE Etrakit3_Preferences
    SET PREF_VALUE = @UserName
    WHERE PREF_NAME = 'AdminUserName';
  END;

   -- AdminUserFullName
  IF NOT EXISTS (SELECT * FROM Etrakit3_Preferences WHERE PREF_NAME = 'AdminUserFullName')
  BEGIN
    INSERT INTO Etrakit3_Preferences VALUES ('AdminUserFullName', @UserFullName);
  END

  ELSE
  BEGIN
    UPDATE Etrakit3_Preferences
    SET PREF_VALUE = @UserFullName
    WHERE PREF_NAME = 'AdminUserFullName';
  END;


  -- AdminUserEmail
  IF NOT EXISTS (SELECT * FROM Etrakit3_Preferences WHERE PREF_NAME = 'AdminUserEmail')
  BEGIN
    INSERT INTO Etrakit3_Preferences VALUES ('AdminUserEmail', @UserEmail);
  END

  ELSE
  BEGIN
    UPDATE Etrakit3_Preferences
    SET PREF_VALUE = @UserEmail
    WHERE PREF_NAME = 'AdminUserEmail';
  END;

    -- AdminUserSecretQuestion
  IF NOT EXISTS (SELECT * FROM Etrakit3_Preferences WHERE PREF_NAME = 'AdminUserSecretQuestion')
  BEGIN
    INSERT INTO Etrakit3_Preferences VALUES ('AdminUserSecretQuestion', @SecretQuestion);
  END

  ELSE
  BEGIN
    UPDATE Etrakit3_Preferences
    SET PREF_VALUE = @SecretQuestion
    WHERE PREF_NAME = 'AdminUserSecretQuestion';
  END;

END

GO
