USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ProjectCreateSubPermitLimitationAdd]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_ProjectCreateSubPermitLimitationAdd]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ProjectCreateSubPermitLimitationAdd]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  

-- =====================================================================
-- Revision History:
--	RB - 9/25/2014 - Added to Source Safe

-- =====================================================================  


CREATE Procedure [dbo].[tsp_T9_ProjectCreateSubPermitLimitationAdd]
(  
	   @ProjectNo varchar(20),
	   @PrimaryLocationID varchar(30),
	   @PermitType varchar(60),
	   @PermitCount int
)

as

begin

	Declare @Restrict bit
	
	set @Restrict = 1

	Declare @Conditions table
	(
		ConditionID varchar(30),
		LimitNo int
	)
	
	Declare @GeoConditions table
	(
		ConditionID varchar(30),
		LocationID varchar(30),
		LimitNo int
	)
	
	Declare @PermitTypeConditions table
	(
		ConditionID varchar(30),
		PermitType varchar(60),
		LimitNo int
	)
	
	Declare @Conditions2 table
	(
		ConditionID varchar(30),
		LimitNo int
	)

	insert into @Conditions select Recordid, ISNULL(SUBPERMIT_CREATION_LIMITATION_NO,0) from Project_Conditions2
	where PROJECT_NO = @ProjectNo and DATE_SATISFIED is null and SUBPERMIT_CREATION_LIMITATION = 1

	declare @ConditionsCount int

	select @ConditionsCount = COUNT(*) from @Conditions
	
	Declare @GeoCount int
	
	Declare @PermitTypeCount int
	
	--print 'Conditions Count - ' + CAST(@ConditionsCount as varchar(10))

	if @ConditionsCount > 0
	begin
		insert into @GeoConditions select a.ConditionID, b.LocationID, a.LimitNo from @Conditions a inner join 
		lnk_project_conditions2_geoownership b on a.ConditionID = b.ConditionID 
		where b.LocationID = @PrimaryLocationID
		
		insert into @PermitTypeConditions select a.ConditionID, b.PermitType, a.LimitNo from @Conditions a inner join
		lnk_project_conditions2_permittypes b on a.ConditionID = b.ConditionID
		where b.ConditionID = a.ConditionID
	end
	else
	set @Restrict = 0
	
	select @GeoCount = COUNT(*) from @GeoConditions
	select @PermitTypeCount = COUNT(*) from @PermitTypeConditions
	
	--print 'Geo Count - ' + CAST(@GeoCount as varchar(10))
	--print 'Permit Type Count - ' + CAST(@PermitTypeCount as varchar(10))
	
	declare @Conditions2Count int
	declare @ConditionLimitNo int
	
	if (@GeoCount > 0 and @PermitTypeCount > 0)
	begin
		insert into @Conditions2 select distinct a.ConditionID, a.LimitNo from @GeoConditions a 
		inner join @PermitTypeConditions b on a.ConditionID = b.ConditionID
		group by a.ConditionID, a.LimitNo
	end
	
	select @Conditions2Count = COUNT(*) from @Conditions2
	select @ConditionLimitNo = LimitNo from @Conditions2
	
	--print 'Conditions2 Count - ' + CAST(@Conditions2Count as varchar(10))
	
	if @Conditions2Count = 0 set @Restrict = 0
	
	--print @Restrict
	
	declare @ExistingSubPermitCount int
	declare @ProposedSubPermitCount int
	
	select @ExistingSubPermitCount = COUNT(*) from Permit_Main where PARENT_PROJECT_NO = @ProjectNo
	
	set @ProposedSubPermitCount = (@ExistingSubPermitCount + @PermitCount)
	
	if @Restrict = 1 and (@ProposedSubPermitCount <= @ConditionLimitNo) set @Restrict = 0  
	
	select @Restrict as [Restrict]
	
end




GO
