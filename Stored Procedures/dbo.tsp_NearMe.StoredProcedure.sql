USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_NearMe]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_NearMe]
GO
/****** Object:  StoredProcedure [dbo].[tsp_NearMe]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: Kyle Lankford           
-- Creation date: 3/07/2019
-- Description: Get activities near me. 
-- Change history:
-- =============================================
CREATE PROCEDURE [dbo].[tsp_NearMe] (
	@xN DECIMAL(18,10),          -- Latitude Northern boundary
	@xS DECIMAL(18,10),          -- Latitude Southern boundary
	@yW DECIMAL(18,10),          -- Longitude Western boundary
	@yE DECIMAL(18,10),			 -- Longitude Eastern boundary
	@status varchar(50)		= '',
	@startDate datetime		= null,
	@endDate datetime		= null,	
	@includePermits BIT		= 0,
	@includeProjects BIT	= 0,
	@includeLicenses BIT	= 0,
	@includeCases BIT		= 0	
)

AS
BEGIN

	 SELECT DISTINCT people.ActivityTypeID, 
					 activity.ACTIVITYNO,
					 ISNULL(activity.LAT, 0) AS Latitude,
					 ISNULL(activity.LON,0) AS Longitude,
					 activity.STATUS
	 FROM People people
	 JOIN tvw_GblActivities activity on people.ActivityID = activity.ACTIVITYNO
	 WHERE (activity.LAT BETWEEN @xS AND @xN AND activity.LON BETWEEN @yW AND @yE)
		AND (@status = '' OR activity.[STATUS] = @status)
		AND (		((@includePermits = 1 AND ActivityTypeID = 1)
			 		AND (@startDate IS NULL OR convert(datetime, DATE1) >= convert(datetime, @startDate))
			 		AND (@endDate IS NULL OR  convert(datetime, DATE1) <= convert(datetime, @endDate))
			 		)	
			 	OR ((@includeProjects = 1 AND ActivityTypeID = 2)
			 		AND (@startDate IS NULL OR convert(datetime, DATE1) >= convert(datetime, @startDate))
			 		AND (@endDate IS NULL OR  convert(datetime, DATE1) <= convert(datetime, @endDate))			
			 	)
			 	OR ((@includeLicenses = 1 AND ActivityTypeID = 3)	 
			 		AND (@startDate IS NULL OR convert(datetime, DATE1) >= convert(datetime, @startDate))
			 		AND (@endDate IS NULL OR  convert(datetime, DATE1) <= convert(datetime, @endDate))
			 	)
			 	OR ((@includeCases = 1 AND ActivityTypeID = 4)
			 		AND (@startDate IS NULL OR convert(datetime, DATE5) >= convert(datetime, @startDate))
			 		AND (@endDate IS NULL OR  convert(datetime, DATE5) <= convert(datetime, @endDate))
			 	)
			)
END
GO
