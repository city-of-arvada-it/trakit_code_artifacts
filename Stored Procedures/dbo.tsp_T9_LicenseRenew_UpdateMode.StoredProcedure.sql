USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_LicenseRenew_UpdateMode]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_LicenseRenew_UpdateMode]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_LicenseRenew_UpdateMode]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  
					  


-- =============================================
-- Author:		Micah Neveu
-- ALTER date: 
-- Description:	<Description,,>
-- 8/7/2014 Added USERID and basic logging // mdn
-- 9/16/2014 Added try catches around some bits that can make with the NULL // mdn
-- 12/11/2014 Removed expiration dates and placed into a new SP, tsp_t9_licenserenew_updateexpirationdates
--			expiration date changes are no longer called directly from update mode)
-- 1/13/2015 Changed the parameter processing to have one at the start, and one at the end
--			instead of just the end.
--			Removed lots of commented code
-- =============================================
CREATE PROCEDURE [dbo].[tsp_T9_LicenseRenew_UpdateMode](
	@ParameterSetID INT,
	@USERID NVARCHAR(6)
)

AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @DEBUGMODE INT
	SET @DEBUGMODE = 1

-- LICENSE RENEWAL REVIEW (START)=====================================================================================
--************************************************* Update -START ****************************************************

INSERT INTO LicenseRenew_EventLog (SourceMode,EventText,USERID) VALUES ('UPDATE', 'Started update mode.', @USERID)

--************************************************** PREPROCESS (START) **********************************************
DECLARE @PreCount INT = (SELECT COUNT(*) FROM LicenseRenew_License_Parameters WHERE ParameterSetID = @ParameterSetID AND ActiveMode = 'UPDATEa')

IF @PreCount > 0
	BEGIN
		INSERT INTO LicenseRenew_EventLog (SourceMode,EventText,USERID) VALUES ('UPDATEa', 'Executing pre-processing conditions for update mode.',@USERID)
		EXEC tsp_t9_LicenseRenew_ParameterProcessor @ParameterSetID, 'UPDATEa', @USERID
	END
--************************************************** PREPROCESS (END) ************************************************


--************************************************** HISTORICAL (START) **********************************************
-- HISTORICAL DATA
-- create TABLE IF IT DOESN'T EXIST
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LicenseRenew_HistoricalFees]'))
      BEGIN
		
			-- the license types will need to be updated - this will have to come from the UI that doesn't currently exist

CREATE TABLE [dbo].[LicenseRenew_HistoricalFees](
	[RenewalYear] DateTime NULL,
	[ACCOUNT] [varchar](30) NULL,
	[AMOUNT] [float] NULL,
	[ASSESSED_BY] [varchar](15) NULL,
	[ASSESSED_DATE] [datetime] NULL,
	[CHECK_NO] [varchar](40) NULL,
	[CODE] [varchar](40) NULL,
	[COMMENTS] [varchar](2000) NULL,
	[DESCRIPTION] [varchar](200) NULL,
	[FEETYPE] [varchar](8) NULL,
	[LICENSE_NO] [varchar](20) NULL,
	[PAID] [varchar](1) NULL,
	[PAID_AMOUNT] [float] NULL,
	[PAID_DATE] [datetime] NULL,
	[PAY_METHOD] [varchar](20) NULL,
	[RECEIPT_NO] [varchar](20) NULL,
	[RECORDID] [varchar](30) NOT NULL,
	[parentid] [varchar](30) NULL,
	[YEAR] int) ;

end
--??? What DATE should we use ???--Change to use current year only
-- Historical Fees:
--	Use License2_fees and License2_subfees
--	where the FEE record exists in LicenseRenew_Review_Fees or LicenseRenew_Review_Subfees
INSERT INTO LicenseRenew_HistoricalFees([YEAR], Amount, [ASSESSED_BY],[ASSESSED_DATE],[CHECK_NO],License_No, Account, FeeType,  Code, Description, RecordID, ParentID)
SELECT datepart(year,GetDate()) AS [year], amount, assessed_by,assessed_date, null as checkno, License_No,account, subfeetype AS FeeType, item AS code, description, recordid, parentid FROM License2_subfees
WHERE RecordId in (select RecordID FROM LicenseRenew_Review_SubFees)
UNION
SELECT datepart(year,GetDate()) AS [year],amount, assessed_by,assessed_date, check_no, License_No,account,  
FeeType,  code, description, recordid, null as parentid FROM License2_fees
WHERE RecordId in (select RecordID FROM LicenseRenew_Review_Fees)

-- DEBUG
IF @DEBUGMODE = 1
	BEGIN
		INSERT INTO LicenseRenew_EventLog (SourceMode,EventText,USERID) VALUES ('[debug] Update Mode', 'Finished inserting historical records.',@USERID)
	END
--************************************************** HISTORICAL (END) ************************************************


--************************************************** UPDATE (START) **************************************************
-- the values are already calculated in licenserenew_review_fees and licenserenew_review_subfees

-- overwrite the amounts for license2_fees, subfees and License2_Main.BALANCE_DUE using the LicenseRenew_Review_Fees and _Subfees

-- update the total amount due on the license, and only those that are in the license renew list


UPDATE l SET AMOUNT = LRF.AMOUNT, PAID = 0, PAID_AMOUNT = 0, PAID_BY = NULL, PAID_DATE = NULL, PAY_BY_DEPOSIT = NULL, PAY_METHOD = NULL,RECEIPT_NO = NULL,
	FORMULA =LRF.formula, comments=cast(year(dateadd(year,1,getdate())) AS VARCHAR)
	from License2_Fees l inner join LicenseRenew_Review_Fees LRF on l.recordid=LRF.recordid 
	WHERE l.License_No IN (SELECT License_No FROM LicenseRenew_License_List) and l.code not in 
	(select feecode from LicenseRenew_ExemptedFeeCodes)

UPDATE l SET AMOUNT = l2.amount,	PAID = 0,FORMULA = l2.formula from LICENSE2_SUBFEES l inner join 
LicenseRenew_Review_Fees l2 on l.recordid=l2.recordid
	WHERE l.License_No IN (SELECT License_No FROM LicenseRenew_License_List) and item not in (select FEECODE from 
	LicenseRenew_ExemptedFeeCodes)
UPDATE License2_Main SET Balance_Due = (SELECT SUM(Amount) FROM LicenseRenew_Review_Fees lrf WHERE License2_Main.license_no = lrf.license_no),
	FEES_PAID = 0, FEES_CHARGED = (SELECT SUM(Amount) FROM LicenseRenew_Review_Fees lrf WHERE License2_Main.license_no = lrf.license_no)
	WHERE License2_Main.License_No IN (SELECT License_No FROM LicenseRenew_License_List)
----------
--BALANCE FORWARD
EXEC tsp_T9_LicenseRenew_CreateBalanceForwardFees 1, @ParameterSetID
--BALANCE FORWARD [end]
----------

IF @DEBUGMODE = 1
	BEGIN
		INSERT INTO LicenseRenew_EventLog (SourceMode,EventText,USERID) VALUES ('[debug] Update Mode', 'Finished updating live records with values from review tables.',@USERID)
	END
--************************************************** UPDATE (END) *****************************************************


BEGIN TRY

--================================================  FEES
-- iii.) FEES  ... I think items 1, 2 and 3  all mean the same thing, in which case, just do #3a-f
-- 1 Add current fee codes and payments in the last year to chronology 
-- 2 Chronology action item 

-- 3 Add to license2_actions =========================================================================================
--      @licenseNO variable
--	3a Action_By = 'AUTO'
--	3b Action_Date = 'GetDate()'
--	3c Action_description = YEAR(getdate()) +1 'STATEMENT GENERATED'
--	3d Action_type = 'NOTICE'
--	3e Completed_date = getdate()
--	3f License_no = license2_main.license_no @licenseNo
-- ADD an action item for each ACTIVE record (LicenseRenew_Review_Main - which is the reviewed license list)

-- ALTER the list of licenses that are being edited
------------------------ ADD ACTIONS (START)---------------------------------------------------------

DECLARE @StatementYear varchar(120) = (CONVERT(VARCHAR(50), YEAR(GETDATE()) + 1) + ' STATEMENT GENERATED')
SELECT LICENSE_NO,RECORDID = 'LRN:' + right(('00' + str(DATEPART(yy, GETDATE()))),1)
 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)
 + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)
 + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)
 + right(('000000'+ CONVERT(varchar,ROW_NUMBER() OVER (ORDER BY (SELECT NULL)))),6),
 ACTION_BY =  'AUTO', ACTION_DATE = GETDATE(), ACTION_DESCRIPTION =   @StatementYear, 
	ACTION_TYPE = 'NOTICE', COMPLETED_DATE = GETDATE() INTO #ActiveLicenses FROM LicenseRenew_License_List



ALTER TABLE #ActiveLicenses ADD BUS_LIC_NO VARCHAR(15)
ALTER TABLE #ActiveLicenses ADD LOCKID VARCHAR(30)





-- insert all of these into license2_actions
INSERT INTO LICENSE2_ACTIONS (ACTION_BY, ACTION_DATE,ACTION_DESCRIPTION,ACTION_TYPE, BUS_LIC_NO,COMPLETED_DATE,RECORDID,LOCKID,LICENSE_NO)
	SELECT ACTION_BY, ACTION_DATE,ACTION_DESCRIPTION,ACTION_TYPE, BUS_LIC_NO,COMPLETED_DATE,RECORDID,LOCKID,LICENSE_NO FROM #ActiveLicenses

IF @DEBUGMODE = 1
	BEGIN
		INSERT INTO LicenseRenew_EventLog (SourceMode,EventText,USERID) VALUES ('[debug] Update Mode', 'Finished inserting actions.',@USERID)
	END
END TRY

BEGIN CATCH
	INSERT INTO LicenseRenew_EventLog (SourceMode,EventText,USERID) VALUES ('warning 1', 'Warning received altering ActiveLicenses table.',@USERID)
END CATCH

------------------------ ADD ACTIONS (END)-----------------------------------------------------------
-- 4 Add to prmry_notes =========================================================================================
------------------------ ADD NOTES (START)-----------------------------------------------------------
-- USE FEES as the primary for inserting. Get all of the fees using the license #'s in our active licenses table
-- and is linked with the new recordID from #ActiveLicenses

-- prmry_notes.activitygroup = 'LICENSE2'
-- prmry_notes.activityno = License_no (can come from license2_actions.license_no)
-- prmry_notes.subgroup = 'ACTION'
-- prmry_notes.subgrouprecordid = license2_actions.recordID
-- prmry_notes.userid = 'AUTO'
-- prmry_notes.dateentered = getdate()
-- prmry_notes.notes = FEE DESCRIPTIONS + CURRENT FEE AMOUNTS, Total for renewal: ' totaldue: USE STUFF

BEGIN TRY

INSERT INTO Prmry_Notes (ActivityGroup, ActivityNo, SubGroup, SubGroupRecordID, UserID,DateEntered,Notes)
SELECT 
'LICENSE2', #ActiveLicenses.license_no, 'ACTION', #ActiveLicenses.RECORDID, 'AUTO', GETDATE(), 
STUFF((
          SELECT ', </br> ' + License2_Fees.DESCRIPTION + ' $' + CONVERT(VARCHAR(60), License2_Fees.AMOUNT) 
          FROM License2_fees
          WHERE License2_Fees.LICENSE_NO = #ActiveLicenses.LICENSE_NO
          FOR XML PATH(''), TYPE).value('.', 'NVARCHAR(MAX)'), 1, 1, '')
          + ' </br> - Total for Renewal: $' + CONVERT(VARCHAR(60), (SELECT SUM(amount) FROM License2_Fees 
          WHERE License2_fees.LICENSE_NO = #ActiveLicenses.LICENSE_NO))
FROM #ActiveLicenses

IF @DEBUGMODE = 1
	BEGIN
		INSERT INTO LicenseRenew_EventLog (SourceMode,EventText,USERID) VALUES ('[debug] Update Mode', 'Finished inserting notes.',@USERID)
	END

END TRY

BEGIN CATCH
	INSERT INTO LicenseRenew_EventLog (SourceMode,EventText,USERID) VALUES ('warning 2', 'Error inserting notes.',@USERID)
END CATCH
------------------------ ADD NOTES (END) -------------------------------------------------------------


------------------------ ADD UDF (START) -------------------------------------------------------------
BEGIN TRY


----- create UDF LOOKUP TABLE
IF OBJECT_ID(N'tempdb..#TempTable') IS NOT NULL
             BEGIN
                    DROP TABLE #TempTable
             END
IF EXISTS( SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'UDF_LIST')
             BEGIN
                    DROP TABLE UDF_LIST
             END

CREATE TABLE UDF_LIST (License_no varchar(20), UDF_NAME varchar(100), UDF_VALUE varchar(100),UDF_CAPTION VARCHAR(100));

SELECT  name INTO  #TempTable FROM  sys.columns WHERE object_id = (SELECT  object_id FROM [sys].[objects] WHERE TYPE = 'U' and name = 'LICENSE2_UDF' )  
DECLARE @FieldName NVARCHAR(100)
DECLARE @fieldValue int
DECLARE @fldString as nvarchar(1000)
DECLARE @getcounterID CURSOR
DECLARE @sqlText nvarchar(max);

SET @getcounterID = CURSOR FOR SELECT name FROM #TempTable
OPEN @getcounterID
FETCH NEXT FROM @getcounterID INTO @FieldName
WHILE @@FETCH_STATUS = 0
BEGIN
	IF @FieldName <>'LICENSE_NO' AND @FieldName<>'RECORDID'
	BEGIN
       SET  @sqlText ='';
       SET @sqlText = 'insert into UDF_LIST
         (License_no,UDF_NAME,UDF_VALUE, UDF_CAPTION)
         select  t.license_no,  '''+@FieldName+''',t.'+ @FieldName +
		 ', (SELECT Top 1 Caption FROM prmry_typesudf WHERE DataField = ''2.' + @FieldName + ''')
         from LICENSE2_UDF t       
		 where t.'+ @FieldName +' is not null'
       EXEC (@sqlText)
	END
	FETCH NEXT FROM @getcounterID INTO @FieldName
END
CLOSE @getcounterID
DEALLOCATE @getcounterID

IF OBJECT_ID(N'tempdb..#tempUDFNotes') IS NOT NULL
BEGIN
	DROP TABLE #tempUDFNotes
END

END TRY

BEGIN CATCH
	INSERT INTO LicenseRenew_EventLog (SourceMode,EventText,USERID) VALUES ('warning 3', ERROR_MESSAGE() + ' received in UDF actions.',@USERID)
END CATCH


BEGIN TRY


SELECT AB.License_no, AB.Awesome INTO #tempUDFNotes FROM
(SELECT License_no, STUFF((Select '  ' + CAST(UDF_CAPTION As VARCHAR(100)) + ': ' + CAST(UDF_VALUE AS VARCHAR(100)) + '</br>'  [text()]
FROM UDF_LIST 
WHERE License_no = t.License_no
FOR XML PATH(''), TYPE).value('.','NVARCHAR(MAX)'),1,2,' ') Awesome
FROM UDF_LIST t
group by license_no	) AB

INSERT INTO Prmry_Notes (ActivityGroup, ActivityNo, SubGroup, SubGroupRecordID,UserID,DateEntered,Notes)
SELECT 'LICENSE2', #ActiveLicenses.LICENSE_NO, 'ACTION',#ActiveLicenses.RecordID,'AUTO',GETDATE(), 
(SELECT TOP 1 Awesome from #tempUDFNotes WHERE #tempUDFNotes.license_no = #ActiveLicenses.License_No)
FROM #ActiveLicenses 

END TRY

BEGIN CATCH
	INSERT INTO LicenseRenew_EventLog (SourceMode,EventText,USERID) VALUES ('error 4', 'Finished inserting actions.',@USERID)
END CATCH
------------------------ ADD UDF (END) ---------------------------------------------------------------


-- 5/6 Add License Additional Information Contact =======================================================================
------------------------ ADD CONTACT INFO (START)-----------------------------------------------------------
INSERT INTO Prmry_Notes (ActivityGroup, ActivityNo, SubGroup, SubGroupRecordID, UserID, DateEntered, Notes)
SELECT 'LICENSE2', #ActiveLicenses.LICENSE_NO, 'ACTION',#ActiveLicenses.RECORDID, 'AUTO', GETDATE(), 
 'Mailed Invoice To:' + COALESCE(COMPANY_PRINT_AS, '') + '</br> ' + COALESCE(SITE_ADDR, '') + ' </br>' + 
 ' Correspondence: ' + COALESCE(COMPANY,'') + ' </br>' + COALESCE(MAIL_ADDRESS1,'') + ' </br>' + COALESCE(MAIL_ADDRESS2,'') + ' </br>' + COALESCE(MAIL_CITY,'') + 
 ' ' + COALESCE(MAIL_STATE,'') + ' ' + COALESCE(MAIL_CITY,'')
  FROM #ActiveLicenses JOIN License2_Main ON #ActiveLicenses.LICENSE_NO = License2_Main.LICENSE_NO

  IF @DEBUGMODE = 1
	BEGIN
		INSERT INTO LicenseRenew_EventLog (SourceMode,EventText,USERID) VALUES ('[debug] Update Mode', 'Finished inserting contact info.',@USERID)
	END
------------------------ ADD CONTACT INFO (END)-------------------------------------------------------------


------------------------ FINAL PROCESSING (START)--------------------------------------------------------
DECLARE @PCount INT = (SELECT COUNT(*) FROM LicenseRenew_License_Parameters WHERE ParameterSetID = @ParameterSetID AND ActiveMode = 'UPDATE')

IF @PCount > 0
	BEGIN
		INSERT INTO LicenseRenew_EventLog (SourceMode,EventText,USERID) VALUES ('UPDATE', 'Executing final processing conditions for update mode.',@USERID)
		EXEC tsp_t9_LicenseRenew_ParameterProcessor @ParameterSetID, 'UPDATE', @USERID
	END
------------------------ FINAL PROCESSING (END)----------------------------------------------------------

-- CLEANUP
DROP TABLE #ActiveLicenses

IF @DEBUGMODE = 1
	BEGIN
		INSERT INTO LicenseRenew_EventLog (SourceMode,EventText,USERID) VALUES ('[debug] Update Mode', 'Executed successfully.',@USERID)
	END

END




GO
