USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_e3_GetActiveLicenses]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_e3_GetActiveLicenses]
GO
/****** Object:  StoredProcedure [dbo].[tsp_e3_GetActiveLicenses]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--Updating tsp_e3_GetActiveLicenses to include subType in results
CREATE PROCEDURE  [dbo].[tsp_e3_GetActiveLicenses]
(
	@aLoginType as Varchar(30),
	@aEtrakitName as Varchar(30) 
)
AS
BEGIN

DECLARE @LoginType AS  NVARCHAR(30) = @aLoginType
DECLARE @EtrakitName AS  NVARCHAR(50) = @aEtrakitName

IF @LoginType = 'CONTRACTOR'
BEGIN
	SET @EtrakitName = 'C:' + @aEtrakitName
END

IF @LoginType = 'PUBLIC'
BEGIN
	SET @EtrakitName = @EtrakitName
END

DECLARE @SQL AS NVARCHAR(max) 
SET @SQL = '

SELECT DISTINCT 
	license2_main.recordid, 
    license2_main.license_no, 
    license2_main.loc_recordid AS LocRecordId, 
    SITE_ADDR = CASE 
                    WHEN ( Ltrim(Rtrim(license2_main.site_city)) <> '''' 
                        ) THEN 
                    Isnull(license2_main.site_addr, '''') + '', '' 
                    + Isnull( 
                license2_main.site_city, '''') + '', '' 
                    + Isnull 
                    ( 
                license2_main.site_state, '''') + '' '' 
                    + Isnull 
                    ( 
                license2_main.site_zip, '''') 
                    ELSE license2_main.site_addr 
                END, 
    license2_main.license_type, 
    license2_main.license_subtype,
    license2_main.status, 
    license2_main.balance_due, 
    license2_main.site_apn
FROM   license2_main'

IF @LoginType = 'CONTRACTOR'
BEGIN
	SET @SQL = @SQL + ' WHERE LICENSE2_MAIN.LICENSE_NO IN (SELECT DISTINCT LICENSE2_PEOPLE.LICENSE_NO from LICENSE2_PEOPLE WHERE (ID = ''' + @EtrakitName + ''')) '
END

IF @LoginType = 'PUBLIC'
BEGIN
	SET @SQL = @SQL + ' LEFT JOIN ETRAKIT_LINKS ON LICENSE2_MAIN.LICENSE_NO = ETRAKIT_LINKS.SUBLINK_NO WHERE ETRAKIT_LINKS.ETRAKIT_NAME = ''' + @EtrakitName  + ''' AND ETRAKIT_LINKS.ACTIVITY_GROUP = ''LICENSE2'' '
END

SET @SQL = @SQL + ' order by LICENSE2_MAIN.LICENSE_NO'

EXECUTE sp_executesql @SQL

END
GO
