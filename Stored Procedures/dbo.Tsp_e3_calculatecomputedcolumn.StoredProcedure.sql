USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[Tsp_e3_calculatecomputedcolumn]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[Tsp_e3_calculatecomputedcolumn]
GO
/****** Object:  StoredProcedure [dbo].[Tsp_e3_calculatecomputedcolumn]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<VICTOR SINFUENTE>
-- VS - 02/10/2016 � Issue #  41605 - Fees created on Permit from eTRAKiT are difference than when created in TRAKiT9 
-- =============================================
CREATE PROCEDURE [dbo].[Tsp_e3_calculatecomputedcolumn] @fieldname   VARCHAR(20), 
                                                @activity_no VARCHAR(20) 
AS 
  BEGIN 
      DECLARE @activity_group AS VARCHAR(30) = (SELECT real_groupname 
         FROM   etrakit_cart_activities 
         WHERE  activity_no = @activity_no) 
      DECLARE @tablename AS VARCHAR(30) = '' 
      DECLARE @formula AS VARCHAR(max) 

      IF @activity_group = 'PERMIT' 
        BEGIN 
            SET @tablename = 'PERMIT_UDF' 
        END 

      IF @activity_group = 'PROJECT' 
        BEGIN 
            SET @tablename = 'PROJECT_UDF' 
        END 

      IF @activity_group = 'LICENSE2' 
        BEGIN 
            SET @tablename = 'LICENSE2_UDF' 
        END 

      IF @activity_group = 'LICENSE' 
        BEGIN 
            SET @tablename = 'LICENSE_UDF' 
        END 

      SET @formula = (SELECT TOP 1 sys.computed_columns.definition 
                      FROM   sys.columns 
                             INNER JOIN sys.computed_columns 
                                     ON sys.computed_columns.object_id = 
                                        sys.columns.object_id 
                                        AND sys.computed_columns.NAME = 
                                            sys.columns.NAME 
                      WHERE  sys.columns.is_computed = 1 
                             AND sys.columns.object_id = Object_id(@tablename) 
                             AND sys.columns.NAME = @fieldname) 

      DECLARE @CUR_ACTIVITY_NO VARCHAR(30) = '' 
      DECLARE @CUR_ACTIVITY_GROUP VARCHAR(30) = '' 
      DECLARE @CUR_RECORDID VARCHAR(30) = '' 
      DECLARE @CUR_FIELDNAME VARCHAR(2000) = '' 
      DECLARE @CUR_FIELDVALUE VARCHAR(2000) = '' 
      DECLARE @CUR_SCREENNAME VARCHAR(2000) = '' 
      DECLARE @CUR_UDFFIELDNAME VARCHAR(2000) = '' 
      DECLARE udf_cursor CURSOR FOR 
        SELECT NAME 
        FROM   sys.columns 
        WHERE  object_id = Object_id(@tablename) 
               AND NAME <> @fieldname 


      OPEN udf_cursor; 

      FETCH next FROM udf_cursor INTO @CUR_UDFFIELDNAME 

      WHILE @@FETCH_STATUS = 0 
        BEGIN 
            IF @formula LIKE '%[' + @CUR_UDFFIELDNAME + ']%' 
              BEGIN 
                  SET @CUR_FIELDVALUE = Isnull((SELECT fieldvalue 
                                                FROM   etrakit_cart_udf2 
                                                WHERE 
                                        activity_no = @activity_no 
                                        AND fieldname = 
                                            @CUR_UDFFIELDNAME 
                                               ), 0 
                                        ) 
                  SET @formula = Replace (@formula, '[' + @CUR_UDFFIELDNAME + 
                                                    ']', 
                                 @CUR_FIELDVALUE) 
              END 

            FETCH next FROM udf_cursor INTO @CUR_UDFFIELDNAME 
        END 

      CLOSE udf_cursor 

      DEALLOCATE udf_cursor 

      EXEC('select ' + @formula) 
  END 

GO
