USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearch_DeleteOrphanedGeometry]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_AdvSearch_DeleteOrphanedGeometry]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearch_DeleteOrphanedGeometry]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[tsp_T9_AdvSearch_DeleteOrphanedGeometry]

@UserID varchar(50) = null,
@WipGisRecordID int = -1 -- this is the record ID in the GisTemp table that was created 

AS
BEGIN

	if @UserID is not null
	begin
		
		-- delete any records from GisTemp for the UserID that are not in Current or Last tables, as long as it isn't the record ID in @WipGisRecordID
		delete from Prmry_AdvSearch_GisTemp 
		where Prmry_AdvSearch_GisTemp.UserID=@UserID
			and RecordID <> @WipGisRecordID
			and recordid not in (select GisRecordID from Prmry_AdvSearch_CurrentItem where UserID=@UserID)
			and recordid not in (select GisRecordID from Prmry_AdvSearch_LastItem where UserID=@UserID)

	end
END
GO
