USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[ALP_GetOperationProcessingDetails]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[ALP_GetOperationProcessingDetails]
GO
/****** Object:  StoredProcedure [dbo].[ALP_GetOperationProcessingDetails]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================================================================
-- Author:		Erick Hampton
-- Revised:		Apr 24, 2017
-- Description:	Gets operations processing info for the supplied Batch ID and License Number.
--				Used for drilling down into operation data on ALPHistory.aspx
-- =============================================================================================

CREATE PROCEDURE [dbo].[ALP_GetOperationProcessingDetails]

	@BatchID int = -1,
	@LicenseNumber varchar(20),
	@StatusFilter varchar(10) = 'TOTAL' -- TOTAL, PENDING, SUCCESSFUL, FAILED

AS
BEGIN

	select
		  [status].[OperationID] 
		, [status].[ParameterSetID]
		, [ps].[Name] [ParameterSetName]
		, [o].[OperationName]
		, [os].[Name] [Status]
		, [status].[OperationStart]
		, [status].[OperationEnd]
		
		-- TODO: we need to add a table to house error / exception details for a failed operation and retrieve the details here to display to user

	from [ALP_BatchParameterSetLicenseNumberOperationStatus] [status]
		left join [ALP_OperationStatuses] os on [status].OperationStatusID=os.ID
		left join [ALP_Operations] o on [status].OperationID=o.Id
		left join [ALP_ParameterSetOperations] pso on [status].ParameterSetID=pso.ParameterSetId and [status].OperationID=pso.OperationId
		left join [ALP_ParameterSets] ps on [status].ParameterSetID=ps.Id

	where [status].BatchID = @BatchID and [status].LicenseNumber = @LicenseNumber and (
		  (@StatusFilter = 'TOTAL')
		  or 
		  (@StatusFilter = 'PENDING' and [status].[OperationStatusID] < 2)
		  or
		  (@StatusFilter = 'SUCCESSFUL' and [status].[OperationStatusID] in (3,4))
		  or
		  (@StatusFilter = 'FAILED' and [status].[OperationStatusID] in (2,5)))
		  
	order by [status].[ProcessingOrder], pso.[Sequence]

END

GO
