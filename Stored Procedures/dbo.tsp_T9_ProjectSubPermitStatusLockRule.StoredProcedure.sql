USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ProjectSubPermitStatusLockRule]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_ProjectSubPermitStatusLockRule]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ProjectSubPermitStatusLockRule]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  

					  

-- =====================================================================
-- Revision History:
--	RB - 9/25/2014 - Added to Source Safe

-- =====================================================================  	
CREATE Procedure [dbo].[tsp_T9_ProjectSubPermitStatusLockRule](     
	   @ConditionRecordID			varchar(25),
	   @ProcedureAction				varchar(25),
	   @StatusLock					bit, 
	   @ActivityNo                  Varchar(25), 
	   @TypeName					Varchar(60)

)

As
 BEGIN

---- TEST ONLY
--Declare 
--	   @ConditionRecordID			varchar(25) = 'CRW:1404080910510007',
--	   @ProcedureAction				varchar(25) = 'TESTRULERESULT',
--	   @StatusLock					Varchar(5),
--       @ActivityNo                  Varchar(25) = 'Z14-0002',
--  	   @TypeName					Varchar(60) = 'ROW'


 Declare @SQLStatement     varchar(500)	
 Declare @NSQLStatement    nvarchar(500)
 Declare @ConditionCount   int = 0
 Declare @TypeCount		   int = 0
 Declare @ParentProject	   Varchar(15)
 
  
 if @ProcedureAction is not null and @ProcedureAction <> ''
    Begin    

		Set @ProcedureAction = UPPER(@ProcedureAction)
		
		if @ProcedureAction = 'SELECT' and (@ConditionRecordID is not null and @ConditionRecordID <> '')
		   Begin
		        Set @SQLStatement = 'Select SUBPERMIT_STATUS_LOCK from project_conditions2 where recordid = ' + Char(39) + @ConditionRecordID + CHAR(39)
		   End
		Else if @ProcedureAction = 'UPDATE' and (@ConditionRecordID is not null and @ConditionRecordID <> '')
		   Begin				
				if @StatusLock = 1
				  Begin
					Set @SQLStatement = 'Update project_conditions2 set SUBPERMIT_STATUS_LOCK = 1 where recordid = ' + Char(39) + @ConditionRecordID + CHAR(39)
				  End
				Else
				  Begin
					Set @SQLStatement = 'Update project_conditions2 set SUBPERMIT_STATUS_LOCK = 0 where recordid = ' + Char(39) + @ConditionRecordID + CHAR(39)
				  End
		   End		
		 Else if @ProcedureAction = 'TESTRULE' and (@ActivityNo is not null and @ActivityNo <> '') and (@TypeName  is not null and @TypeName <> '')
		   Begin	
		   
		   
			    Set @ParentProject = (Select Parent_Project_No from Permit_Main where PERMIT_NO = @ActivityNo)
			    
			    If @ParentProject is not null and @ParentProject <> ''
			       Begin	   
						set @TypeCount = (select count(*) from Prmry_Types where GroupName = 'PERMITS' and TypeName = @TypeName and SUBPERMIT_STATUS_LOCK = 1)						                             
										   	   					
						If @TypeCount > 0
						   Begin
   							   set @ConditionCount = (select count(*) from Project_Conditions2 where Project_no = @ParentProject and SUBPERMIT_STATUS_LOCK = 1 and isdate(date_satisfied)= 0)
							                        
							   If @ConditionCount > 0
								  Begin				      
									  Set @SQLStatement = ' select Status as StatusName from prmry_status where GroupName = ' + Char(39) + 'PERMITS' + Char(39) + 
	             											' and (subpermit_status_lock = 1) ' + 
	             											' and Status in(select value1 from Prmry_TypesInfo where GroupName = ' + Char(39) +  'PERMITS' + Char(39) + 
	             											' and Category = ' + Char(39) + 'STATUS' + Char(39) +  
	             											' and Typename = ' + Char(39) + @TypeName  + Char(39) + ')'			 
								      
								  End   
							End	
							
					End
					
			End
    End
 
 if @SQLStatement <> '' 
   Begin
		EXEC(@SQLStatement)
		--print @SQLStatement
   End
		
 END


GO
