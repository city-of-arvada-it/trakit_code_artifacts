USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GblPastDueInspByDateAndUID]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_GblPastDueInspByDateAndUID]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GblPastDueInspByDateAndUID]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  					  
					  					  					  



CREATE Procedure [dbo].[tsp_GblPastDueInspByDateAndUID](
@InspectorID Varchar(6) = Null,
@StartDate varchar(30)= Null
)

As

set nocount on


--RB Check if table exists before creating it
   IF OBJECT_ID('tempdb..#CombinedInspections') IS NOT NULL  
	drop table #CombinedInspections

Create Table #CombinedInspections(
TGroup Varchar(15),
ActivityNo Varchar(15),
CAPOVERRIDE Varchar(4),
COMPLETED_DATE    varchar(50),
COMPLETED_TIME    varchar(10),
CREATED_BY  varchar(6),
CREATED_DATE      varchar(50),
CREATED_TIME      varchar(10),
DURATION    varchar(10),
DURATION_EST      varchar(10),
INSPECTIONTYPE    varchar(20),
INSPECTOR   varchar(10),
LOCKID      varchar(30),
NOTES varchar(2000),
RECORDID    varchar(30),
REMARKS     varchar(40),
RESULT      varchar(50),
SCHEDULED_DATE    varchar(50),
SCHEDULED_TIME    varchar(10),
SEQID varchar(10),
LOC_RECORDID varchar(30),
SITE_APN varchar(50),
GEOTYPE varchar(60),
SITE_ALTERNATE_ID varchar(50),
SITE_ADDR varchar(100),
SITE_CITY varchar(50),
SITE_STATE varchar(10),
SITE_ZIP varchar(10),
CONTACT_NAME varchar(50),
CONTACT_ADDRESS1 varchar(100),
CONTACT_CITY varchar(50),
CONTACT_STATE varchar(10),
CONTACT_ZIP varchar(10),
CONTACT_PHONE varchar(50),
ISVOIDED int ,
ISTODAY int ,
ISPASTDUE int ,
ATTACHMENTCOUNT int
)

Insert Into #CombinedInspections(
TGroup, 
ActivityNo,
CAPOVERRIDE,
COMPLETED_DATE,
COMPLETED_TIME,
CREATED_BY,
CREATED_DATE,
CREATED_TIME,
DURATION ,
DURATION_EST,
INSPECTIONTYPE,
INSPECTOR,
LOCKID,
NOTES,
RECORDID,
REMARKS,
RESULT,
SCHEDULED_DATE,
SCHEDULED_TIME,
SEQID,
LOC_RECORDID,
SITE_APN,
GEOTYPE,
SITE_ALTERNATE_ID,
SITE_ADDR,
SITE_CITY,
SITE_STATE,
SITE_ZIP,
CONTACT_NAME,
CONTACT_ADDRESS1,
CONTACT_CITY,
CONTACT_STATE,
CONTACT_ZIP,
CONTACT_PHONE,
ISVOIDED,
ISTODAY,
ISPASTDUE,
ATTACHMENTCOUNT)
SELECT     'CASE' AS Tgroup, CASE_INSPECTIONS.CASE_NO AS ACTIVITYNO, CAST(CASE_INSPECTIONS.CAPOVERRIDE AS varchar(10)) AS CAPOVERRIDE, 
                      CONVERT(varchar(50), CASE_INSPECTIONS.COMPLETED_DATE, 101) AS COMPLETED_DATE, CASE_INSPECTIONS.COMPLETED_TIME, 
                      CASE_INSPECTIONS.CREATED_BY, CONVERT(varchar(50), CASE_INSPECTIONS.CREATED_DATE, 101) AS CREATED_DATE, CASE_INSPECTIONS.CREATED_TIME, 
                      CAST(CASE_INSPECTIONS.DURATION AS varchar(10)) AS DURATION, CAST(CASE_INSPECTIONS.DURATION_EST AS varchar(10)) AS DURATION_EST, 
                      CASE_INSPECTIONS.InspectionType AS INSPECTIONTYPE, CASE_INSPECTIONS.INSPECTOR, CASE_INSPECTIONS.LOCKID, CASE_INSPECTIONS.NOTES, 
                      CASE_INSPECTIONS.RECORDID, CASE_INSPECTIONS.REMARKS, CASE_INSPECTIONS.RESULT, CONVERT(varchar(50), CASE_INSPECTIONS.SCHEDULED_DATE, 101)
                       AS SCHEDULED_DATE, CASE_INSPECTIONS.SCHEDULED_TIME, CAST(CASE_INSPECTIONS.SEQID AS varchar(10)) AS SEQID, '' as LOC_RECORDID, '' AS SITE_APN, '' AS GEOTYPE, 
                      '' AS SITE_ALTERNATE_ID, 'No Address Available' AS SITE_ADDR, '' AS SITE_CITY, '' AS SITE_STATE, '' AS SITE_ZIP, '' AS CONTACT_NAME, '' AS CONTACT_ADDRESS1, 
                      '' AS CONTACT_CITY, '' AS CONTACT_STATE, '' AS CONTACT_ZIP, '' AS CONTACT_PHONE, 0 AS ISVOIDED, 0 AS ISTODAY, 0 AS ISPASTDUE, 0 AS ATTACHMENTCOUNT
FROM         CASE_INSPECTIONS 
WHERE     (isdate(CASE_INSPECTIONS.COMPLETED_DATE) = 0) AND (CASE_INSPECTIONS.INSPECTOR = @InspectorID) AND 
                      (CASE_INSPECTIONS.SCHEDULED_DATE < CONVERT(DATETIME,@StartDate, 102))                      
UNION
SELECT     'LICENSE' AS Tgroup, LICENSE_INSPECTIONS.BUS_LIC_NO AS ACTIVITYNO, CAST(LICENSE_INSPECTIONS.CAPOVERRIDE AS varchar(10)) AS CAPOVERRIDE, 
                      CONVERT(varchar(50), LICENSE_INSPECTIONS.COMPLETED_DATE, 101) AS COMPLETED_DATE, LICENSE_INSPECTIONS.COMPLETED_TIME, 
                      LICENSE_INSPECTIONS.CREATED_BY, CONVERT(varchar(50), LICENSE_INSPECTIONS.CREATED_DATE, 101) AS CREATED_DATE, 
                      LICENSE_INSPECTIONS.CREATED_TIME, CAST(LICENSE_INSPECTIONS.DURATION AS varchar(10)) AS DURATION, 
                      CAST(LICENSE_INSPECTIONS.DURATION_EST AS varchar(10)) AS DURATION_EST, LICENSE_INSPECTIONS.InspectionType AS INSPECTIONTYPE, 
                      LICENSE_INSPECTIONS.INSPECTOR, LICENSE_INSPECTIONS.LOCKID, LICENSE_INSPECTIONS.NOTES, LICENSE_INSPECTIONS.RECORDID, 
                      LICENSE_INSPECTIONS.REMARKS, LICENSE_INSPECTIONS.RESULT, CONVERT(varchar(50), LICENSE_INSPECTIONS.SCHEDULED_DATE, 101) 
                      AS SCHEDULED_DATE, LICENSE_INSPECTIONS.SCHEDULED_TIME, CAST(LICENSE_INSPECTIONS.SEQID AS varchar(10)) AS SEQID, '' as LOC_RECORDID, '' AS SITE_APN, '' AS GEOTYPE, 
                      '' AS SITE_ALTERNATE_ID, 'No Address Available' AS SITE_ADDR, '' AS SITE_CITY, '' AS SITE_STATE, '' AS SITE_ZIP, '' AS CONTACT_NAME, '' AS CONTACT_ADDRESS1, 
                      '' AS CONTACT_CITY, '' AS CONTACT_STATE, '' AS CONTACT_ZIP, '' AS CONTACT_PHONE, 0 AS ISVOIDED, 0 AS ISTODAY, 0 AS ISPASTDUE, 0 AS ATTACHMENTCOUNT
FROM         LICENSE_INSPECTIONS 
WHERE     (isdate(LICENSE_INSPECTIONS.COMPLETED_DATE) = 0) AND (LICENSE_INSPECTIONS.INSPECTOR = @InspectorID) AND 
                      (LICENSE_INSPECTIONS.SCHEDULED_DATE < CONVERT(DATETIME, @StartDate, 102))
UNION
SELECT     'PERMIT' AS Tgroup, PERMIT_NO AS ACTIVITYNO, CAST(CAPOVERRIDE AS varchar(10)) AS CAPOVERRIDE, CONVERT(varchar(50), COMPLETED_DATE, 101) 
                      AS COMPLETED_DATE, COMPLETED_TIME, CREATED_BY, CONVERT(varchar(50), CREATED_DATE, 101) AS CREATED_DATE, CREATED_TIME, 
                      CAST(DURATION AS varchar(10)) AS DURATION, CAST(DURATION_EST AS varchar(10)) AS DURATION_EST, InspectionType AS INSPECTIONTYPE, INSPECTOR, 
                      LOCKID, NOTES, RECORDID, REMARKS, RESULT, CONVERT(varchar(50), SCHEDULED_DATE, 101) AS SCHEDULED_DATE, SCHEDULED_TIME, 
                      CAST(SEQID AS varchar(10)) AS SEQID, '' as LOC_RECORDID, '' AS SITE_APN, '' AS GEOTYPE, 
                      '' AS SITE_ALTERNATE_ID, 'No Address Available' AS SITE_ADDR, '' AS SITE_CITY, '' AS SITE_STATE, '' AS SITE_ZIP, '' AS CONTACT_NAME, '' AS CONTACT_ADDRESS1, 
                      '' AS CONTACT_CITY, '' AS CONTACT_STATE, '' AS CONTACT_ZIP, '' AS CONTACT_PHONE, 0 AS ISVOIDED, 0 AS ISTODAY, 0 AS ISPASTDUE, 0 AS ATTACHMENTCOUNT
FROM         Permit_Inspections
WHERE     (isdate(Permit_Inspections.COMPLETED_DATE) = 0) AND (Permit_Inspections.INSPECTOR = @InspectorID) AND 
                      (Permit_Inspections.SCHEDULED_DATE < CONVERT(DATETIME, @StartDate, 102))
UNION
SELECT     'PROJECT' AS Tgroup, PROJECT_NO AS ACTIVITYNO, CAST(CAPOVERRIDE AS varchar(10)) AS CAPOVERRIDE, CONVERT(varchar(50), COMPLETED_DATE, 101) 
                      AS COMPLETED_DATE, COMPLETED_TIME, CREATED_BY, CONVERT(varchar(50), CREATED_DATE, 101) AS CREATED_DATE, CREATED_TIME, 
                      CAST(DURATION AS varchar(10)) AS DURATION, CAST(DURATION_EST AS varchar(10)) AS DURATION_EST, InspectionType AS INSPECTIONTYPE, INSPECTOR, 
                      LOCKID, NOTES, RECORDID, REMARKS, RESULT, CONVERT(varchar(50), SCHEDULED_DATE, 101) AS SCHEDULED_DATE, SCHEDULED_TIME, 
                      CAST(SEQID AS varchar(10)) AS SEQID, '' as LOC_RECORDID, '' AS SITE_APN, '' AS GEOTYPE, '' AS SITE_ALTERNATE_ID, 'No Address Available' AS SITE_ADDR, '' AS SITE_CITY, '' AS SITE_STATE, 
                      '' AS SITE_ZIP, '' AS CONTACT_NAME, '' AS CONTACT_ADDRESS1, '' AS CONTACT_CITY, '' AS CONTACT_STATE, '' AS CONTACT_ZIP, '' AS CONTACT_PHONE, 
                      0 AS ISVOIDED, 0 AS ISTODAY, 0 AS ISPASTDUE, 0 AS ATTACHMENTCOUNT
FROM         PROJECT_INSPECTIONS
WHERE     (isdate(PROJECT_INSPECTIONS.COMPLETED_DATE) = 0) AND (PROJECT_INSPECTIONS.INSPECTOR = @InspectorID) AND 
                      (PROJECT_INSPECTIONS.SCHEDULED_DATE < CONVERT(DATETIME, @StartDate, 102))

--Update isvoided
update #CombinedInspections set ISVOIDED = 1 where REMARKS like '%voided%'  

--Update istoday
update #CombinedInspections set ISTODAY = 1 where convert(date,SCHEDULED_DATE,101) = convert(date,GETDATE(),101)

--Update ispastdue
update #CombinedInspections set ISPASTDUE = 1 where convert(date,SCHEDULED_DATE,101) < convert(date,GETDATE(),101)              

--Update Site_addr from _Main tables
update #CombinedInspections set SITE_ADDR = Case_Main.SITE_ADDR, SITE_CITY= Case_Main.SITE_CITY, SITE_STATE= Case_Main.SITE_STATE, SITE_ZIP = Case_Main.SITE_ZIP from Case_Main where Case_Main.CASE_NO = #CombinedInspections.ActivityNo and #CombinedInspections.TGroup = 'CASE' 
update #CombinedInspections set SITE_ADDR = License_Business.LOC_ADDRESS1, SITE_CITY= License_Business.LOC_CITY, SITE_STATE= License_Business.LOC_STATE, SITE_ZIP = License_Business.LOC_ZIP FROM #CombinedInspections INNER JOIN
                      License_Main ON #CombinedInspections.ActivityNo = License_Main.BUS_LIC_NO INNER JOIN
                      License_Business ON License_Main.BUSINESS_NO = License_Business.BUSINESS_NO and #CombinedInspections.TGroup = 'LICENSE' 
update #CombinedInspections set SITE_ADDR = Permit_Main.SITE_ADDR, SITE_CITY= Permit_Main.SITE_CITY, SITE_STATE= Permit_Main.SITE_STATE, SITE_ZIP = Permit_Main.SITE_ZIP from Permit_Main where Permit_Main.PERMIT_NO = #CombinedInspections.ActivityNo and #CombinedInspections.TGroup = 'PERMIT' 
update #CombinedInspections set SITE_ADDR = Project_Main.SITE_ADDR, SITE_CITY= Project_Main.SITE_CITY, SITE_STATE= Project_Main.SITE_STATE, SITE_ZIP = Project_Main.SITE_ZIP from Project_Main where Project_Main.Project_NO = #CombinedInspections.ActivityNo and #CombinedInspections.TGroup = 'PROJECT' 

--Update Location Recordid from _Main tables
update #CombinedInspections set LOC_RECORDID = Case_Main.LOC_RECORDID from Case_Main where Case_Main.CASE_NO = #CombinedInspections.ActivityNo and #CombinedInspections.TGroup = 'CASE'
update #CombinedInspections set LOC_RECORDID = License_Business.LOC_RECORDID FROM #CombinedInspections INNER JOIN
                      License_Main ON #CombinedInspections.ActivityNo = License_Main.BUS_LIC_NO INNER JOIN
                      License_Business ON License_Main.BUSINESS_NO = License_Business.BUSINESS_NO and #CombinedInspections.TGroup = 'LICENSE'
update #CombinedInspections set LOC_RECORDID = Permit_Main.LOC_RECORDID from Permit_Main where Permit_Main.PERMIT_NO = #CombinedInspections.ActivityNo and #CombinedInspections.TGroup = 'PERMIT'
update #CombinedInspections set LOC_RECORDID = Project_Main.LOC_RECORDID from Project_Main where Project_Main.Project_NO = #CombinedInspections.ActivityNo and #CombinedInspections.TGroup = 'PROJECT'

--Update site info from geo using populated Loc_Recordid
update #CombinedInspections set SITE_APN = GEO_OWNERSHIP.SITE_APN From GEO_OWNERSHIP where GEO_OWNERSHIP.RECORDID = #CombinedInspections.LOC_RECORDID and LEN(GEO_OWNERSHIP.SITE_APN) > 0
update #CombinedInspections set GEOTYPE = GEO_OWNERSHIP.GEOTYPE From GEO_OWNERSHIP where GEO_OWNERSHIP.RECORDID = #CombinedInspections.LOC_RECORDID and LEN(GEO_OWNERSHIP.GEOTYPE) > 0
update #CombinedInspections set SITE_ALTERNATE_ID = GEO_OWNERSHIP.SITE_ALTERNATE_ID From GEO_OWNERSHIP where GEO_OWNERSHIP.RECORDID = #CombinedInspections.LOC_RECORDID and LEN(GEO_OWNERSHIP.SITE_ALTERNATE_ID) > 0
update #CombinedInspections set SITE_ADDR = GEO_OWNERSHIP.SITE_ADDR From GEO_OWNERSHIP where GEO_OWNERSHIP.RECORDID = #CombinedInspections.LOC_RECORDID and LEN(GEO_OWNERSHIP.SITE_ADDR) > 0
update #CombinedInspections set SITE_CITY = GEO_OWNERSHIP.SITE_CITY From GEO_OWNERSHIP where GEO_OWNERSHIP.RECORDID = #CombinedInspections.LOC_RECORDID and LEN(GEO_OWNERSHIP.SITE_CITY) > 0
update #CombinedInspections set SITE_STATE = GEO_OWNERSHIP.SITE_STATE From GEO_OWNERSHIP where GEO_OWNERSHIP.RECORDID = #CombinedInspections.LOC_RECORDID and LEN(GEO_OWNERSHIP.SITE_STATE) > 0
update #CombinedInspections set SITE_ZIP = GEO_OWNERSHIP.SITE_ZIP From GEO_OWNERSHIP where GEO_OWNERSHIP.RECORDID = #CombinedInspections.LOC_RECORDID and LEN(GEO_OWNERSHIP.SITE_ZIP) > 0

--Give one last attempt to give something for ipad if all other address info fails

Create Table #MaxZip(
SITE_ZIP varchar(10),
ZIPCOUNT int
)

Insert Into #MaxZip(
SITE_ZIP,
ZIPCOUNT
)
SELECT top 1(SITE_ZIP) as SITE_ZIP, COUNT(SITE_ZIP) AS ZIPCOUNT
FROM         Geo_Ownership
GROUP BY SITE_ZIP
ORDER BY ZIPCOUNT DESC

update #CombinedInspections set SITE_ZIP = #MaxZip.SITE_ZIP From #MaxZip where len(#CombinedInspections.SITE_ZIP) < 1

--Update Contacts 
update #CombinedInspections set
CONTACT_NAME = Case_People.NAME,
CONTACT_ADDRESS1 = Case_People.ADDRESS1,
CONTACT_CITY = Case_People.CITY,
CONTACT_STATE = Case_People.STATE,
CONTACT_ZIP = Case_People.ZIP,
CONTACT_PHONE = Case_People.PHONE
from Case_People where Case_People.CASE_NO = #CombinedInspections.ActivityNo
and Case_People.NAMETYPE = 'OWNER'
and #CombinedInspections.TGroup = 'CASE'

update #CombinedInspections set
CONTACT_NAME = License_People.NAME,
CONTACT_ADDRESS1 = License_People.ADDRESS1,
CONTACT_CITY = License_People.CITY,
CONTACT_STATE = License_People.STATE,
CONTACT_ZIP = License_People.ZIP,
CONTACT_PHONE = License_People.PHONE
from License_People where License_People.BUS_LIC_NO = #CombinedInspections.ActivityNo
and License_People.NAMETYPE = 'OWNER'
and #CombinedInspections.TGroup = 'LICENSE'

update #CombinedInspections set
CONTACT_NAME = Permit_People.NAME,
CONTACT_ADDRESS1 = Permit_People.ADDRESS1,
CONTACT_CITY = Permit_People.CITY,
CONTACT_STATE = Permit_People.STATE,
CONTACT_ZIP = Permit_People.ZIP,
CONTACT_PHONE = Permit_People.PHONE
from Permit_People where Permit_People.PERMIT_NO = #CombinedInspections.ActivityNo
and Permit_People.NAMETYPE = 'OWNER'
and #CombinedInspections.TGroup = 'PERMIT'

update #CombinedInspections set
CONTACT_NAME = Project_People.NAME,
CONTACT_ADDRESS1 = Project_People.ADDRESS1,
CONTACT_CITY = Project_People.CITY,
CONTACT_STATE = Project_People.STATE,
CONTACT_ZIP = Project_People.ZIP,
CONTACT_PHONE = Project_People.PHONE
from Project_People where Project_People.PROJECT_NO = #CombinedInspections.ActivityNo
and Project_People.NAMETYPE = 'OWNER'
and #CombinedInspections.TGroup = 'PROJECT'

----Update attachments (3 types: DOC,PHOTO, IMAGE; exclude DOC)
Create Table #InspectionImages(
TGroup Varchar(15),
ActivityNo Varchar(15),
ATTACHMENTCOUNT int
)
Insert Into #InspectionImages(TGroup,ActivityNo,ATTACHMENTCOUNT)
Select 'CASE' AS Tgroup, Case_Attachments.CASE_NO AS ACTIVITYNO, COUNT(recordid) from Case_Attachments where Case_Attachments.AttachmentType <> 'DOC' and Case_Attachments.CASE_NO in (select #CombinedInspections.ActivityNo from #CombinedInspections where #CombinedInspections.TGroup = 'CASE') group by Case_Attachments.CASE_NO
Union
Select 'LICENSE' AS Tgroup, LICENSE_Attachments.BUS_LIC_NO AS ACTIVITYNO, COUNT(recordid) from LICENSE_Attachments where LICENSE_Attachments.AttachmentType <> 'DOC' and LICENSE_Attachments.BUS_LIC_NO in (select #CombinedInspections.ActivityNo from #CombinedInspections where #CombinedInspections.TGroup = 'LICENSE') group by LICENSE_Attachments.BUS_LIC_NO
Union
Select 'PERMIT' AS Tgroup, Permit_Attachments.permit_NO AS ACTIVITYNO, COUNT(recordid) from Permit_Attachments where Permit_Attachments.AttachmentType <> 'DOC' and Permit_Attachments.permit_NO in (select #CombinedInspections.ActivityNo from #CombinedInspections where #CombinedInspections.TGroup = 'PERMIT') group by Permit_Attachments.permit_NO
Union
Select 'PROJECT' AS Tgroup, Project_Attachments.project_NO AS ACTIVITYNO, COUNT(recordid) from Project_Attachments where Project_Attachments.AttachmentType <> 'DOC' and Project_Attachments.project_NO in (select #CombinedInspections.ActivityNo from #CombinedInspections where #CombinedInspections.TGroup = 'PROJECT') group by Project_Attachments.project_NO

update #CombinedInspections set #CombinedInspections.ATTACHMENTCOUNT = #InspectionImages.ATTACHMENTCOUNT from #InspectionImages inner join #CombinedInspections 
on #CombinedInspections.ActivityNo = #InspectionImages.ActivityNo and  #CombinedInspections.TGroup = #InspectionImages.TGroup


Select * From #CombinedInspections Order By TGroup, ActivityNo















GO
