USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[GetNextLocationNumber]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[GetNextLocationNumber]
GO
/****** Object:  StoredProcedure [dbo].[GetNextLocationNumber]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
		  
CREATE PROCEDURE [dbo].[GetNextLocationNumber]
	@Location_Name VARCHAR(20),	
	@Location_NO int = 0 out

AS
BEGIN
     DECLARE @pi_High as int
	 SELECT @pi_High = MAX(LOCATION_NO) FROM PRMRY_PLANLOCATIONS WHERE GROUPNAME ='$RANGE_HIGH' AND LOCATION_NAME = @Location_Name

     DECLARE @pi_Low as int
	 SELECT @pi_Low = MAX(LOCATION_NO) FROM PRMRY_PLANLOCATIONS WHERE GROUPNAME ='$RANGE_LOW' AND LOCATION_NAME = @Location_Name

	  if (SELECT  count(*) FROM PRMRY_PLANLOCATIONS where 
			LOCATION_NAME = @Location_Name
            and released is not null 
			and location_no between @pi_Low and @pi_High
            and groupname not like '$%'
			and location_no not in 
					(SELECT LOCATION_NO  FROM PRMRY_PLANLOCATIONS where 
					LOCATION_NAME = @Location_Name 
					and released is null 
					and groupname not like '$%'))>0
					begin
					 SELECT TOP(1) @Location_NO =  LOCATION_NO FROM PRMRY_PLANLOCATIONS where 
						LOCATION_NAME = @Location_Name
						and released is not null 
						and location_no between @pi_Low and @pi_High
						and groupname not like '$%'
						and location_no not in 
							(SELECT LOCATION_NO  FROM PRMRY_PLANLOCATIONS where 
							LOCATION_NAME = @Location_Name 
							and released is null 
							and groupname not like '$%') ORDER BY LOCATION_NO
					end
		else
			if (SELECT  count(*)  FROM PRMRY_PLANLOCATIONS where 
				LOCATION_NAME = @Location_Name
				and location_no between @pi_Low and @pi_High
				and groupname not like '$%') =0
					select @Location_NO=@pi_Low 
			ELSE 
				SELECT  @Location_NO= max(location_no) + 1 FROM PRMRY_PLANLOCATIONS where 
					LOCATION_NAME = @Location_Name
					and location_no between @pi_Low and @pi_High
					and groupname not like '$%'


	if @Location_NO>@pi_High set @Location_NO=0
END


GO
