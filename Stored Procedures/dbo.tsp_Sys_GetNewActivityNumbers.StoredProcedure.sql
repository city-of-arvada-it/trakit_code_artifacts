USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_Sys_GetNewActivityNumbers]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_Sys_GetNewActivityNumbers]
GO
/****** Object:  StoredProcedure [dbo].[tsp_Sys_GetNewActivityNumbers]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[tsp_Sys_GetNewActivityNumbers]
@GroupName varchar(255),@SetName varchar(255), @Quantity int, @Prefix varchar(255),
@ReturnValue int output

as


Declare @SQL varchar(8000)
Declare @SQLlog varchar(8000)
Declare @AddSetName int

Set @AddSetName = 1

set @ReturnValue = 1
If @Prefix Is Null Set @Prefix = ''
Set @Prefix = LTRIM(RTRIM(@Prefix))


--Check for setname
select @AddSetName = count(setname) from Prmry_Numbering 
where GROUPNAME = '' + @GroupName + '' and setname = '' + @setname + ''


--insert into prmry_numbering if needed
if @AddSetName = 0
       begin
              set @SQL = 'INSERT INTO Prmry_Numbering (GroupName, SetName, LastNumber) VALUES (''' + @GroupName + ''', ''' + @setname + ''', 0)'

              --print @SQL
              exec(@SQL)
       end
       
       
 If @Prefix <> ''
  Begin      
      --If prefix is populated, remove from setname.  This is for sequential numbering accross all prefixes.
      set @SetName = REPLACE(@SetName, @prefix,'')

      --locate and update
      select @ReturnValue = max(LastNumber) from Prmry_Numbering where GROUPNAME = '' + @GroupName + '' and setname like '' + '%' + @setname + ''

      --increment value by quantity requested
      set @ReturnValue = @ReturnValue + @Quantity

      Select @ReturnValue

      --update prmry_numbering
      set @SQL = 'Update Prmry_Numbering set LASTNUMBER = ' + cast(@ReturnValue as varchar(50)) +  ' where SETNAME like ''%' + @setname  + ''' and GROUPNAME = ''' + @GroupName + ''''

--alter for next insert
--set @SQL = REPLACE(@SQL, '''','''''')

--log record
--set @SQLlog = 'Insert into Prmry_AuditTrail_SQL(SQL_COMMAND,TRANSACTION_DATETIME,USER_ID) values (''' + @SQL + ''', ''' + cast(cast(getdate() as datetime) as varchar(255))  + ''',''-SP'')'

--print @SQLlog
--exec(@SQLlog)
      --print @SQL
      exec(@SQL)
End
ELSE
Begin
      Update pn Set
      @ReturnValue = LastNumber + @Quantity
      ,LASTNUMBER =  LastNumber + @Quantity
      From Prmry_Numbering pn
      where SETNAME = @setname and GROUPNAME = @GroupName 
      
      Insert into Prmry_AuditTrail_SQL(SQL_COMMAND,TRANSACTION_DATETIME,USER_ID) 
      values ('Update Prmry_Numbering - Set LastNumber = ''' + Convert(Varchar,@ReturnValue) + ''' 
      Where SetName = ''' + @SetName + ''' and GroupName = ''' + @GroupName + '''',
      cast(cast(getdate() as datetime) as varchar(255)),'-SP')

         Select @ReturnValue
End
GO
