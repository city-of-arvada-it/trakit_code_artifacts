USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GblSearchMyTasks]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_GblSearchMyTasks]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GblSearchMyTasks]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[tsp_GblSearchMyTasks](
@UserID Varchar(30),
@vStartDate Varchar(100) = Null,
@vEndDate Varchar(100) = Null,
@Top INT = Null,
@ExcludeInspections INT = 0
)

As

Declare @SQL Varchar(8000)
Declare @SQLTop Varchar(800)
Declare @InspectionsWHERE Varchar(1000)
Declare @ReviewsWHERE Varchar(1000)
Declare @ActionsWHERE Varchar(1000)
Declare @ConditionsWHERE Varchar(1000)
Declare @CaseFollowUpWHERE Varchar(1000)
Declare @CRMIssuesWHERE Varchar(1000)
Declare @StartDate Datetime 
Declare @EndDate Datetime
Declare @vTop Varchar(25)

If @ExcludeInspections Is Null Set @ExcludeInspections = 0

Set @InspectionsWHERE = ''
Set @ReviewsWHERE = ''
Set @ActionsWHERE = ''
Set @ConditionsWHERE = ''
Set @CaseFollowUpWHERE = ''
Set @CRMIssuesWHERE = ''


If @Top = 0 Set @Top = Null
--temp logic by mdp to limit total 
--If @Top Is Null Set @Top = 25
If @Top Is Null Set @vTop = '' ELSE Set @vTop = ' Top ' + Convert(Varchar,@Top)


Set @StartDate  = Null
Set @EndDate  = Null
If @vStartDate = '' Set @vStartDate = Null
If @vStartDate = '1/1/0001 12:00:00 AM' Set @vStartDate = Null
If @vEndDate = '' Set @vEndDate = Null
If @vEndDate = '1/1/0001 12:00:00 AM' Set @vEndDate = Null

If @vStartDate Is Null Set @vStartDate = Convert(Varchar,GetDate(),101)
If @vEndDate Is Null Set @vEndDate = Convert(Varchar,GetDate(),101)

Set @vStartDate = CONVERT(Varchar,CONVERT(Datetime,@vStartDate),101)
Set @vEndDate = CONVERT(Varchar,CONVERT(Datetime,@vEndDate),101)

Print(@vStartDate)
Print(@vEndDate)

Set @StartDate = CONVERT(Datetime,@vStartDate)
Set @EndDate = CONVERT(Datetime,@vEndDate)
-- Set @EndDate = CONVERT(Datetime,DATEADD(dd,1,@vEndDate))

Print(@StartDate)
Print(@EndDate)


Create Table #Tasks(
MODULE Varchar(130),
activity_type_id int, 
CATEGORY Varchar(130),
CAT_TYPE Varchar(130),
RECORD_NO Varchar(130),
RECORD_TYPE Varchar(130),
RECORD_STATUS Varchar(130),
SITE_ADDR Varchar(500),
LOC_RECORDID Varchar(130),
SITE_CITY Varchar(130),
ASSIGNEE Varchar(130),
DUE_DATE Datetime,
RECORDID Varchar(130),
SITE_APN Varchar(150),
SITE_ALTERNATE_ID Varchar(150),
SITE_GEOTYPE Varchar(150),
ConditionsTotal INT Default 0,
AttachmentTotal INT Default 0, 
NotesTotal INT Default 0, 
RestrictionsTotal INT Default 0, 
FeesDue Money Default 0, 
RestrictionsSummary Varchar(5000) Default '',
RestrictionsRecordIDUsed Varchar(40),
DESCRIPTION VARCHAR(2000),
SITE_SUBDIVISION Varchar(100) default '',
LockID varchar(50) default '',
Remarks varchar(500) default ''
)

Set @SQLTop = '
Insert Into #Tasks(Module,activity_type_id,Category,Cat_Type,Record_No,Record_Type,Record_Status,Site_Addr,Loc_RecordID,Site_City,Assignee,Due_Date,RecordID,Site_APN,Site_Alternate_ID,Site_GeoType, DESCRIPTION, SITE_SUBDIVISION, LockID, Remarks)
Select ' + @vTop + ' ' 



If @ExcludeInspections = 0
Begin
       --Begin: Inspections
       Set @InspectionsWHERE = @InspectionsWHERE + '
       WHERE S.INSPECTOR = ''' + @USERID + '''
       AND S.COMPLETED_DATE Is Null 
       AND 
      
       (S.SCHEDULED_DATE between ''' + Convert(Varchar,@STARTDATE,101) + ''' 
       AND ''' + Convert(Varchar,@ENDDATE,101) + ''')
	 
       AND (S.REMARKS Is Null OR S.REMARKS NOT LIKE ''VOIDED (%'')'



       --Geo_Inspections
       Set @SQL = @SQLTop + '
              Tgroup as MODULE, 
			  s.ActivityTypeID,
              ''INSPECTION'' as CATEGORY,
              S.INSPECTIONTYPE as CAT_TYPE,
              s.ActivityID AS RECORD_NO,      
              b.ACTIVITYTYPE,
              b.status,
              b.SITE_ADDR + IsNull('', '' + b.SITE_CITY,'''') as SITE_ADDR,
              b.LOC_RECORDID,
              b.SITE_CITY as SITE_CITY,
              S.INSPECTOR as ASSIGNEE,
              S.SCHEDULED_DATE as DUE_DATE,
              s.RECORDID,
              b.SITE_APN,
              b.SITE_ALTERNATE_ID,
              b.geotype,
              b.TITLE,
              b.SITE_SUBDIVISION,
			  LockID, Remarks
       From [dbo].[inspections]  s join [dbo].[ActivityType] a
on a.ActivityTypeID=s.ActivityTypeID
join  tvw_GblActivities b
on b.ActivityNo=s.ActivityID
     ' + @InspectionsWHERE + '
       Order By s.SCHEDULED_DATE DESC '
       Print(@SQL)
       Exec(@SQL)
       --End: Inspections
End

--Begin: Reviews
Set @ReviewsWHERE = @ReviewsWHERE + '
WHERE (r.CONTACT = ''' + @USERID + '''
or left(r.CONTACT,24) = 
(select max(left(USERNAME,24)) from Prmry_Users where USERID = ''' + @USERID + '''))
AND r.DATE_RECEIVED Is Null 
AND 
(r.DATE_DUE < Convert(Varchar,GetDate(),101) 
OR 
(r.DATE_DUE between ''' + Convert(Varchar,@STARTDATE,101) + ''' 
AND  ''' + Convert(Varchar,@ENDDATE,101) + '''))
AND (r.REMARKS Is Null OR r.REMARKS NOT LIKE ''VOIDED (%'')'


--License2_Reviews
Set @SQL = @SQLTop + '
        tgroup as MODULE, 
	    r.ActivityTypeID,
       ''REVIEW'' as CATEGORY,
       r.REVIEWTYPE as CAT_TYPE,
       r.ActivityID AS RECORD_NO,    
       b.ACTIVITYTYPE,
       r.STATUS AS RECORD_STATUS,
       b.SITE_ADDR + IsNull('', '' + b.SITE_CITY,'''') as SITE_ADDR,
       b.LOC_RECORDID,
       b.SITE_CITY,
       r.CONTACT as ASSIGNEE,
       r.DATE_DUE as DUE_DATE,
       r.RECORDID,
       b.SITE_APN,
       b.SITE_ALTERNATE_ID,
       b.geotype,
       b.TITLE as DESCRIPTION,
       b.SITE_SUBDIVISION,
	   LockID, Remarks
From [dbo].[Reviews]  r join [dbo].[ActivityType] a
on a.ActivityTypeID=r.ActivityTypeID
join  tvw_GblActivities b
on b.ActivityNo=r.ActivityID 
' + @ReviewsWHERE + '
Order By r.DATE_DUE DESC '
Print(@SQL)
Exec(@SQL)




--Begin: Actions
Set @ActionsWHERE = @ActionsWHERE + '
WHERE (r.ACTION_BY = ''' + @USERID + '''
or left(r.ACTION_BY,30) = 
(select max(left(USERNAME,30)) from Prmry_Users where USERID = ''' + @USERID + '''))
AND r.COMPLETED_DATE Is Null 
AND 
(r.ACTION_DATE < Convert(Varchar,GetDate(),101) 
OR 
(r.ACTION_DATE between ''' + Convert(Varchar,@STARTDATE,101) + ''' 
AND  ''' + Convert(Varchar,@ENDDATE,101) + '''))
AND (r.ACTION_DESCRIPTION Is Null OR r.ACTION_DESCRIPTION NOT LIKE ''VOIDED (%'')'


--License2_Actions
Set @SQL = @SQLTop + '
       TGROUP as MODULE, 
	   r.ActivityTypeID,
       ''ACTION'' as CATEGORY,
       r.ACTION_TYPE as CAT_TYPE,
       r.ActivityID as activityNO,
       b.ACTIVITYTYPE as RECORD_TYPE,
       b.status as RECORD_STATUS,
       b.SITE_ADDR + IsNull('', '' + b.SITE_CITY,'''') as SITE_ADDR,
       b.LOC_RECORDID,
       b.SITE_CITY,
       r.ACTION_BY as ASSIGNEE,
       r.ACTION_DATE as DUE_DATE,
       r.RECORDID,
       b.SITE_APN,
       b.SITE_ALTERNATE_ID,
       b.geotype as SITE_GEOTYPE,
       b.TITLE as DESCRIPTION,
       b.SITE_SUBDIVISION,
	   r.LockID, Left(r.Action_Description, 500) as Remarks
From [dbo].[actions]  r join [dbo].[ActivityType] a
on a.ActivityTypeID=r.ActivityTypeID
join  tvw_GblActivities b
on b.ActivityNo=r.ActivityID

' + @ActionsWHERE + '
Order By r.ACTION_DATE DESC '
Print(@SQL)
Exec(@SQL)


--End: Actions


--Begin: Conditions
Set @ConditionsWHERE = @ConditionsWHERE + '
WHERE (left(r.CONTACT,80) = 
(select max(left(USERNAME,80)) from Prmry_Users where USERID = ''' + @USERID + '''))
AND r.DATE_SATISFIED Is Null 
AND 
(r.DATE_REQUIRED < Convert(Varchar,GetDate(),101) 
OR 
(r.DATE_REQUIRED between ''' + Convert(Varchar,@STARTDATE,101) + ''' 
AND  ''' + Convert(Varchar,@ENDDATE,101) + ''') OR r.DATE_REQUIRED is null)
AND (r.CONDITION_NOTES Is Null OR r.CONDITION_NOTES NOT LIKE ''VOIDED (%'')'


--License2_Conditions
Set @SQL = @SQLTop + '
       TGROUP as MODULE, 
	   r.ActivityTypeID,
       ''CONDITION'' as CATEGORY,
       r.CONDITION_TYPE as CAT_TYPE,
       r.ActivityID as RECORD_NO,
       b.ACTIVITYTYPE as RECORD_TYPE,
       b.status as RECORD_STATUS,
       b.SITE_ADDR + IsNull('', '' + b.SITE_CITY,'''') as SITE_ADDR,
       b.LOC_RECORDID,
       b.SITE_CITY,
       r.CONTACT as ASSIGNEE,
       r.DATE_REQUIRED as DUE_DATE,
       r.RECORDID as RECORDID,
       b.SITE_APN,
       b.SITE_ALTERNATE_ID,
       b.geotype as SITE_GEOTYPE,
       b.TITLE as DESCRIPTION,
       b.SITE_SUBDIVISION,
	   r.LockID, r.Remarks
From [dbo].[Conditions]  r join [dbo].[ActivityType] a
on a.ActivityTypeID=r.ActivityTypeID
join  tvw_GblActivities b
on b.ActivityNo=r.ActivityID 
' + @ConditionsWHERE + '
Order By r.DATE_REQUIRED DESC '
Print(@SQL)
Exec(@SQL)

--End: Conditions


--Begin: Case Follow Up
Set @CaseFollowUpWHERE = @CaseFollowUpWHERE + '
WHERE left(ASSIGNED_TO,30) = 
(select max(left(USERNAME,30)) from Prmry_Users where USERID = ''' + @USERID + ''')
AND FOLLOWUP between ''' + Convert(Varchar,@STARTDATE,101) + ''' 
AND  ''' + Convert(Varchar,@ENDDATE,101) + ''''


--CodeTRAK Cases by Followup Date
Set @SQL = @SQLTop + '
       ''CASE'' as MODULE, 
	   ActivityTypeID,
       ''FOLLOWUP'' as CATEGORY,
       CASETYPE as CAT_TYPE,
       CASE_NO as RECORD_NO,
       CASETYPE as RECORD_TYPE,
       STATUS as RECORD_STATUS,
       SITE_ADDR + IsNull('', '' + SITE_CITY,'''') as SITE_ADDR,
       LOC_RECORDID as LOC_RECORDID,
       SITE_CITY as SITE_CITY,
       ASSIGNED_TO as ASSIGNEE,
       FOLLOWUP as DUE_DATE,
       RECORDID as RECORDID,
       SITE_APN as SITE_APN,
       SITE_ALTERNATE_ID as SITE_ALTERNATE_ID,
       SITE_GEOTYPE as SITE_GEOTYPE,
       CASE_NAME AS DESCRIPTION,
       SITE_SUBDIVISION as SITE_SUBDIVISION,
	   LockID, 
	   '''' as Remarks
From Case_Main ' + @CaseFollowUpWHERE + '
Order By FOLLOWUP DESC '
Print(@SQL)
Exec(@SQL)
--End: Case Follow Up


--Begin: CRM Issues by Due Date
Set @CRMIssuesWHERE = @CRMIssuesWHERE + '
WHERE M.ASSIGNED_USER_ID = ''' + @USERID + '''
AND M.COMPLETED_DATE Is Null 
AND 
(M.DUE_DATE < Convert(Varchar,GetDate(),101) 
OR 
(M.DUE_DATE between ''' + Convert(Varchar,@STARTDATE,101) + ''' 
AND  ''' + Convert(Varchar,@ENDDATE,101) + '''))
'
--AND TL.LIST_CATEGORY = ''2''
--AND SL.LIST_CATEGORY = ''5'''


--CRM Issues by Due Date Date
Set @SQL = @SQLTop + '
       ''CRM'' as MODULE, 
	   ''0'' as ActivityTypeID,
       ''ISSUE'' as CATEGORY,
       TL.LIST_TEXT as CAT_TYPE,
       M.ISSUE_LABEL as RECORD_NO,
       TL.LIST_TEXT as RECORD_TYPE,
       SL.LIST_TEXT as RECORD_STATUS,
       M.ISSUE_ADDRESS + IsNull('', '' + M.ISSUE_CITY,'''') as SITE_ADDR,
       M.ISSUE_LOC_RECORDID as LOC_RECORDID,
       M.ISSUE_CITY as SITE_CITY,
       M.ASSIGNED_USER_ID as ASSIGNEE,
       M.DUE_DATE as DUE_DATE,
       M.RECORDID as RECORDID,
       M.ISSUE_SITE_APN as SITE_APN,
       M.ISSUE_ALTERNATE_ID as SITE_ALTERNATE_ID,
       M.ISSUE_GEOTYPE as SITE_GEOTYPE ,
       M.DESCRIPTION,
       '''' as SITE_SUBDIVISION,
	   '''' as LockID,
	   '''' as Remarks
From CRM_Issues M
JOIN CRM_Lists TL
ON TL.LIST_ID = M.NATURETYPE_LIST_ID 
JOIN CRM_Lists SL
ON SL.LIST_ID = M.STATUS_LIST_ID ' + @CRMIssuesWHERE + '
Order By M.DUE_DATE DESC '
Print(@SQL)
Exec(@SQL)


--Select * From #Tasks Order By Due_Date Desc



Print 'Note Count'
Update #Tasks Set
NotesTotal = n.Total
From #Tasks tr
 JOIN (
	Select Total=Count(*),SubGroupRecordID From Prmry_Notes
	--WHERE SubGroup = 'INSPECTION'
	Group By SubGroupRecordID
 ) n
  ON tr.RecordID = n.SubGroupRecordID
  
Print 'Note Count - Activities (Case and CRM)'
Update #Tasks Set
NotesTotal =  n.Total 
From #Tasks tr
 JOIN (
       Select Total=Count(*),ActivityRecordID From Prmry_Notes
       --WHERE SubGroup = 'REVIEW'
       Group By ActivityRecordID
) n
  ON tr.RecordID = n.ActivityRecordID
WHERE Category IN ('Issue','FollowUp')


Print 'Conditions Count '
Update #Tasks Set
ConditionsTotal = CASE WHEN c.Total Is Null THEN 0 ELSE c.Total END
From #Tasks tr
 JOIN (
       Select Total=Count(*),ActivityID,ActivityTypeID From Conditions
       Group By ActivityID,ActivityTypeID
) c
  ON tr.Record_No = c.ActivityID
	and tr.activity_type_id=c.ActivityTypeID
  

  

Print 'Restriction Count'
Update #Tasks Set
RestrictionsTotal = CASE WHEN r.Total Is Null THEN 0 ELSE r.Total END
From #Tasks tr
LEFT JOIN (
       Select Total=Count(*),Loc_RecordID From Geo_Restrictions2
       Where Date_Cleared Is Null AND Restriction_Notes Not Like 'VOIDED (%'
       Group By Loc_RecordID
) r
  ON tr.Loc_RecordID = r.Loc_RecordID
  

Print 'Table #Restrictions'
IF OBJECT_ID(N'tempdb..#Restrictions') IS NOT NULL
             BEGIN
                    DROP TABLE #Restrictions
             END
SELECT      DISTINCT
         loc_recordid,

         RESTRICTION=STUFF(
               (SELECT      '<br />' + summary
               FROM      
			   (select loc_recordid, summary= isnull(x2.Restriction_Type,'') + ' ('
				+ Convert(Varchar,isnull(x2.Date_Added,''),101) + ') ' + SubString(x2.Restriction_Notes,1,50)
               FROM      Geo_RESTRICTIONS2 x2
			   where x2.Date_Cleared Is Null AND x2.Restriction_Notes Not Like 'VOIDED (%')
			   --and  loc_recordid='CONV:151116183544066713'
			    AS x2
               WHERE   
			    x.loc_recordid = x2.loc_recordid
			   
               FOR XML PATH('')), 1, 1, '')  
			  into #Restrictions 
FROM      Geo_RESTRICTIONS2 as x 
where loc_Recordid in(select loc_recordid from #Tasks)
ORDER BY   loc_recordid 

Print 'update #Tasks'
	Update #Tasks Set
	RestrictionsSummary = RESTRICTION	
	From #Tasks tr
	JOIN #Restrictions r
	 ON tr.Loc_RecordID = r.Loc_RecordID
 

Print 'Attachments'
Update #Tasks Set
AttachmentTotal = isnull(a.total,0)
From #Tasks tr
 JOIN (
	Select Total=Count(*),ActivityID,ActivityTypeID From attachments
	Group By ActivityID,ActivityTypeID
 ) a
  ON tr.RECORD_NO = a.ActivityID
  and tr.activity_type_id=a.ActivityTypeID


Print ' Fees Due'
Update #Tasks Set
FeesDue =  convert(real,a.Total )
From #Tasks tr
JOIN (	
Select ActivityID ,ActivityTypeID,Total=Sum(amount)-sum(PAID_AMOUNT)   From fees	
		Group By ActivityID ,ActivityTypeID
		having  Sum(amount)-sum(PAID_AMOUNT) >0
 ) a
  ON tr.RECORD_NO = a.ActivityID
and tr.activity_type_id=a.ActivityTypeID

Select *From #Tasks Order By DUE_DATE Desc

GO
