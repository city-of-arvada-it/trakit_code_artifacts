USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GblAdvancedSearch]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_GblAdvancedSearch]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GblAdvancedSearch]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  					  
-- =============================================
-- Revision History:
--     HAN - 11/22/13 - Added extra parameter @ExcludeFromSearch and its related script
-- =============================================   
	
	
CREATE Procedure [dbo].[tsp_GblAdvancedSearch](
@MainSearchTable Varchar(50),
@UDFSearchTable Varchar(50),
@ColumnsToDisplay Varchar(1500),
@ColumnsToDisplayFieldNames Varchar(1000),
@OrderByClause Varchar(300) = Null,
@Search001 Varchar(700),
@Search002 Varchar(700) = Null,
@Search003 Varchar(700) = Null,
@Search004 Varchar(700) = Null,
@Search005 Varchar(700) = Null,
@Search006 Varchar(700) = Null,
@Search007 Varchar(700) = Null,
@Search008 Varchar(700) = Null,
@Search009 Varchar(700) = Null,
@Search010 Varchar(700) = Null,
@Search011 Varchar(700) = Null,
@Search012 Varchar(700) = Null,
@Search013 Varchar(700) = Null,
@Search014 Varchar(700) = Null,
@Search015 Varchar(700) = Null,
@ExcludeFromSearch Varchar(700) = Null,
@LimitByUDFTable INT = Null,
@Top INT = Null
)

As

Set @Search001 = REPLACE(@Search001,'''','''''')
Set @Search002 = REPLACE(@Search002,'''','''''')
Set @Search003 = REPLACE(@Search003,'''','''''')
Set @Search004 = REPLACE(@Search004,'''','''''')
Set @Search005 = REPLACE(@Search005,'''','''''')
Set @Search006 = REPLACE(@Search006,'''','''''')
Set @Search007 = REPLACE(@Search007,'''','''''')
Set @Search008 = REPLACE(@Search008,'''','''''')
Set @Search009 = REPLACE(@Search009,'''','''''')
Set @Search010 = REPLACE(@Search010,'''','''''')
Set @Search011 = REPLACE(@Search011,'''','''''')
Set @Search012 = REPLACE(@Search012,'''','''''')
Set @Search013 = REPLACE(@Search013,'''','''''')
Set @Search014 = REPLACE(@Search014,'''','''''')
Set @Search015 = REPLACE(@Search015,'''','''''')


--RB Check if table exists before creating it
   IF OBJECT_ID('tempdb..#SearchFields') IS NOT NULL  
	drop table #SearchFields

Create Table #SearchFields(
SearchIndex INT Identity,
FieldName Varchar(250), 
FieldType Varchar(250),
Operator Varchar(25),
FieldValue1 Varchar(1000),
FieldValue2 Varchar(500)
)

--RB Check if table exists before creating it
   IF OBJECT_ID('tempdb..#SearchStrings') IS NOT NULL  
	drop table #SearchStrings

Create Table #SearchStrings(
SearchIndex INT Identity,
String Varchar(3000)
)

Declare @StartIndex INT
Set @StartIndex = 1
Declare @CurrentIndex INT
Set @CurrentIndex = 0
Declare @PreviousIndex INT
Set @PreviousIndex = 0
Declare @NextIndex INT
Set @NextIndex = 0
Declare @FieldNameSS Varchar(700)
Declare @LoopIndex INT
Set @LoopIndex = 1
Declare @TotalLoops INT
Set @TotalLoops = 10
Declare @Continue INT
Set @Continue = 1
Declare @SearchString Varchar(3000)
Declare @SearchIndex INT
Set @SearchIndex = 0

Declare @MainTableKey Varchar(100)
Declare @MainTableType Varchar(300)
Declare @MainTableSubType Varchar(300)
Declare @MainTableStatus Varchar(100)

Declare @SQL Varchar(8000)
Declare @SQL2 Varchar(8000)
Declare @SQL3 Varchar(8000)
Declare @isDate int

GOTO ParseSearchStrings


Main:

Declare @FieldNameLoop Varchar(250)
Declare @FieldTypeLoop Varchar(250)
Declare @OperatorLoop Varchar(150)
Declare @FieldValue1Loop Varchar(1000)
Declare @FieldValue2Loop Varchar(500)


--Set @SQL = 'Select '
--If @Top Is Not Null AND @Top > 0 Set @SQL = @SQL + 'Top ' + CONVERT(Varchar,@Top) + ' ' ELSE Set @SQL = @SQL + 'Top 500 '
--Set @SQL = @SQL + @ColumnsToDisplay

Set @SQL = @SQL + 'Select '
If @Top Is Not Null AND @Top > 0 Set @SQL = @SQL + 'Top ' + CONVERT(Varchar,@Top) -- MLM Change -- + ' ' ELSE Set @SQL = @SQL + 'Top 500 '

If @MainSearchTable IN ('Geo_Ownership')
	Set @SQL = @SQL + REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@ColumnsToDisplayFieldNames,'[',''),']',''),'|',','),'GIS_SITE_ALTERNATE_ID','SITE_ALTERNATE_ID'),'GIS_SITE_GEOTYPE','GEOTYPE'),'GIS_SITE_APN','SITE_APN'),'GIS_LOC_RECORDID','RECORDID'),'MainKeyField',@MainSearchTable + '.' + @MainTableKey),'MAIN_TABLE_STATUS',@MainTableStatus)	
ELSE
If @MainSearchTable IN ('tvw_CRMIssues')
	Set @SQL = @SQL + REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@ColumnsToDisplayFieldNames,'[',''),']',''),'|',','),'GIS_SITE_ALTERNATE_ID','ISSUE_ALTERNATE_ID'),'GIS_SITE_GEOTYPE','ISSUE_GEOTYPE'),'GIS_SITE_APN','ISSUE_SITE_APN'),'GIS_LOC_RECORDID','ISSUE_LOC_RECORDID'),'MainKeyField',@MainSearchTable + '.' + @MainTableKey),'MAIN_TABLE_TYPE',@MainTableType),'MAIN_TABLE_SUBTYPE',@MainTableSubType),'MAIN_TABLE_STATUS',@MainTableStatus)
ELSE
	Set @SQL = @SQL + REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@ColumnsToDisplayFieldNames,'[',''),']',''),'|',','),'GIS_SITE_ALTERNATE_ID','SITE_ALTERNATE_ID'),'GIS_SITE_GEOTYPE','SITE_GEOTYPE'),'GIS_SITE_APN','SITE_APN'),'GIS_LOC_RECORDID','LOC_RECORDID'),'MainKeyField',@MainSearchTable + '.' + @MainTableKey),'MAIN_TABLE_TYPE',@MainTableType),'MAIN_TABLE_SUBTYPE',@MainTableSubType),'MAIN_TABLE_STATUS',@MainTableStatus)
 

Set @SQL = @SQL + '
From ' + @MainSearchTable + ''


If @MainSearchTable <> 'tvw_CRMIssues'
 Begin
	If @LimitByUDFTable Is Not Null
		Set @SQL = @SQL + '
		JOIN ' + @UDFSearchTable + '
		ON '+ @MainSearchTable + '.' + @MainTableKey + ' = ' + + @UDFSearchTable + '.' + @MainTableKey
	ELSE
		Set @SQL = @SQL + '
		LEFT JOIN ' + @UDFSearchTable + '
		ON '+ @MainSearchTable + '.' + @MainTableKey + ' = ' + + @UDFSearchTable + '.' + @MainTableKey
 End


Set @SQL = @SQL + '
WHERE 1=1 
'

 While Exists ( Select * From #SearchFields )
  Begin
  	Select Top 1 
  		@SearchIndex=SearchIndex,@FieldNameLoop=FieldName,@FieldTypeLoop=FieldType,
  		@OperatorLoop=Operator,@FieldValue1Loop=FieldValue1,@FieldValue2Loop=FieldValue2
  		From #SearchFields Order By SearchIndex
  		
  		Set @FieldTypeLoop = REPLACE(@FieldTypeLoop,'System.','')
  		Set @FieldValue1Loop = REPLACE(@FieldValue1Loop,'''','''''')
  		Set @FieldValue2Loop = REPLACE(@FieldValue2Loop,'''','''''')
  		
  		Print @FieldTypeLoop
  		Print @FieldNameLoop
  		Print @OperatorLoop
  		
  		If @FieldTypeLoop IN ('String')
  		 Begin
  			If @FieldValue2Loop Is Null OR @FieldValue2Loop = '' OR @FieldValue2Loop = ' '
  			 Begin
  				Set @SQL = @SQL + ' AND ' + @FieldNameLoop + ' '
  				Set @SQL = @SQL + CASE 
  				WHEN @OperatorLoop = 'contains' THEN 'Like ''%' + @FieldValue1Loop + '%'''
  				WHEN @OperatorLoop = 'is' THEN '= ''' + @FieldValue1Loop + ''''
  				WHEN @OperatorLoop = 'is not' THEN '<> ''' + @FieldValue1Loop + ''''
  				WHEN @OperatorLoop = 'begins with' THEN 'Like ''' + @FieldValue1Loop + '%'''
  				WHEN @OperatorLoop = 'in' THEN 'IN (' + REPLACE(@FieldValue1Loop,'''''','''') + ')'
  				END
  			 End
  		 End
  		 
  		 If @FieldTypeLoop IN ('Double','Int32','Int64')
  		 Begin
  			If @FieldValue2Loop Is Null OR @FieldValue2Loop = '' OR @FieldValue2Loop = ' '
  			 Begin
  				Set @SQL = @SQL + ' AND ' + @FieldNameLoop + ' '
  				Set @SQL = @SQL + CASE 
  				WHEN @OperatorLoop = 'is' THEN '= ' + @FieldValue1Loop + ''
  				WHEN @OperatorLoop = 'at least' THEN '>= ' + @FieldValue1Loop + ''
  				WHEN @OperatorLoop = 'less than' THEN '< ' + @FieldValue1Loop + ''
  				END
  			 End
  			 ELSE
  			  Begin
  				Set @SQL = @SQL + ' AND ' + @FieldNameLoop + ' '
  				Set @SQL = @SQL + CASE 
  				WHEN @OperatorLoop = 'between' THEN '>= ' + @FieldValue1Loop + ' AND ' + @FieldNameLoop + ' <= ' + @FieldValue2Loop
  				END
  			  End
  		 End
  		 
  		 If @FieldTypeLoop IN ('DateTime')
  		 Begin
			Set @SQL = @SQL + ' AND ' + @FieldNameLoop + ' '
			Set @SQL = @SQL + CASE 
			WHEN @OperatorLoop = 'between' 
			THEN '>= ''' + @FieldValue1Loop + ''' AND ' + @FieldNameLoop + ' < ''' 
			+ Convert(Varchar,DATEADD(dd,1,Convert(Datetime,@FieldValue2Loop))) + ''''
			END
  		 End
  		 
  		 --Print(@SQL)

	Delete #SearchFields Where SearchIndex = @SearchIndex
  End

--Aspire:11/22/2013 - Exclude search criteria condition is implemented for CRM type restriction related to feature implementation
If @ExcludeFromSearch Is Not Null
	Set @SQL = @SQL + '	AND ' + @ExcludeFromSearch


If @OrderByClause Is Not Null
	Set @SQL = @SQL + '
	ORDER BY ' + @OrderByClause

--Print(@SQL)
--Exec(@SQL)


Set @SQL3 = @SQL2 + @SQL + CHAR(10) + CHAR(10)  

 If @MainSearchTable IN ('Permit_Main','Project_Main','Case_Main','License2_Main','Generic1_Main','Geo_Ownership','tvw_CRMIssues')
  Begin
  Set @SQL3 = @SQL3 + '
	 update tr Set
	 GIS_SITE_ADDR = geo.SITE_ADDR
	 ,GIS_LAT = geo.LAT
	 ,GIS_LON = geo.LON
	 ,GIS_COORD_X = geo.COORD_X
	 ,GIS_COORD_Y = geo.COORD_Y
	 From #Results tr
	 LEFT JOIN Geo_Ownership geo
	  ON tr.[' + @MainSearchTable + '.GIS_LOC_RECORDID] = geo.RecordID
	  '  + CHAR(10) + CHAR(10)
  End

 --If @MainSearchTable IN ('tvw_CRMIssues')
 -- Begin
 -- Set @SQL3 = @SQL3 + '
	-- update tr Set
	-- tr.[tvw_CRMIssues.MAIN_TABLE_TYPE] = cl.List_Text
	-- From #Results tr
	-- LEFT JOIN CRM_Lists cl
	--  ON tr.[' + @MainSearchTable + '.MAIN_TABLE_TYPE] = cl.List_ID
	--  '  + CHAR(10) + CHAR(10)
 --   Set @SQL3 = @SQL3 + '
	-- update tr Set
	-- tr.[tvw_CRMIssues.MAIN_TABLE_SUBTYPE] = cl.List_Text
	-- From #Results tr
	-- LEFT JOIN CRM_Lists cl
	--  ON tr.[' + @MainSearchTable + '.MAIN_TABLE_SUBTYPE] = cl.List_ID
	--  '  + CHAR(10) + CHAR(10)
	--Set @SQL3 = @SQL3 + '
	-- update tr Set
	-- tr.[tvw_CRMIssues.MAIN_TABLE_STATUS] = cl.List_Text
	-- From #Results tr
	-- LEFT JOIN CRM_Lists cl
	--  ON tr.[' + @MainSearchTable + '.MAIN_TABLE_STATUS] = cl.List_ID
	--  '  + CHAR(10) + CHAR(10)
 -- End
  
Set @SQL3 = @SQL3 + 'Select ' + @ColumnsToDisplay + ' From #Results'
--Set @SQL3 = @SQL3 + 'Select * From #Results'
Print(@SQL3)
Print(len(@SQL3))
Exec(@SQL3)


GOTO Finish


/*

tsp_GblAdvancedSearch @MainSearchTable = 'Permit_Main',  @UDFSearchTable = 'Permit_UDF',  @ColumnsToDisplay = '[Geotype]=[Permit_Main.SITE_GEOTYPE],[Applied]=[Permit_Main.APPLIED],[Issued]=[Permit_Main.ISSUED],[Site Address]=[Permit_Main.SITE_ADDR],[Permit Type]=[Permit_Main.PERMITTYPE],[Status]=[Permit_Main.STATUS],[Permit No.]=[Permit_Main.PERMIT_NO],[CONTRACTOR_NAME]=[Permit_Main.CONTRACTOR_NAME],[DESCRIPTION]=[Permit_Main.DESCRIPTION],[RECORDID]=[Permit_Main.RECORDID]',  @ColumnsToDisplayFieldNames = '[Permit_Main.SITE_GEOTYPE]|Convert(Varchar,[Permit_Main.APPLIED],101)|Convert(Varchar,[Permit_Main.ISSUED],101)|[Permit_Main.SITE_ADDR]|[Permit_Main.PERMITTYPE]|[Permit_Main.STATUS]|[Permit_Main.PERMIT_NO]|[Permit_Main.CONTRACTOR_NAME]|[Permit_Main.DESCRIPTION]|[Permit_Main.RECORDID]',  @Top = '0',  @Search001 = 'Permit_Main.PERMITTYPE|System.String|in|^BUILDING COM^||'

tsp_GblAdvancedSearch @MainSearchTable = 'Geo_Ownership',  @UDFSearchTable = 'Geo_UDF',  @ColumnsToDisplay = '[Geotype]=[Geo_Ownership.GEOTYPE],[House Number]=[Geo_Ownership.SITE_NUMBER],[Street Name]=[Geo_Ownership.SITE_STREETNAME],[APN]=[Geo_Ownership.SITE_APN],[Subdivision]=[Geo_Ownership.SITE_SUBDIVISION],[STATUS]=[Geo_Ownership.STATUS],[RECORDID]=[Geo_Ownership.RECORDID]',  @ColumnsToDisplayFieldNames = '[Geo_Ownership.GEOTYPE]|[Geo_Ownership.SITE_NUMBER]|[Geo_Ownership.SITE_STREETNAME]|[Geo_Ownership.SITE_APN]|[Geo_Ownership.SITE_SUBDIVISION]|[Geo_Ownership.STATUS]|[Geo_Ownership.RECORDID]',  @Top = '0',  @Search001 = 'Geo_Ownership.STATUS|System.String|in|^ACTIVE^||'

*/


ParseSearchStrings:

Set @MainSearchTable = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@MainSearchTable,';',''),' ',''),'DELETE ',''),'UPDATE ',''),'INSERT ',''),'SELECT ',''),'SET ',''),'WHERE ',''),'ORDER BY ','')
Set @UDFSearchTable = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@UDFSearchTable,';',''),' ',''),'DELETE ',''),'UPDATE ',''),'INSERT ',''),'SELECT ',''),'SET ',''),'WHERE ',''),'ORDER BY ','')
Set @ColumnsToDisplay = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@ColumnsToDisplay,';',''),'DELETE ',''),'UPDATE ',''),'INSERT ',''),'SELECT ',''),'SET ',''),'WHERE ',''),'ORDER BY ','')
Set @ColumnsToDisplayFieldNames = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@ColumnsToDisplayFieldNames,';',''),'DELETE ',''),'UPDATE ',''),'INSERT ',''),'SELECT ',''),'SET ',''),'WHERE ',''),'ORDER BY ','')
Set @OrderByClause = REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@OrderByClause,';',''),' ',''),'DELETE ',''),'UPDATE ',''),'INSERT ',''),'SELECT ',''),'SET ',''),'WHERE ',''),'ORDER BY ','')
Set @Search001 = REPLACE(@Search001,';','')
Set @Search002 = REPLACE(@Search002,';','')
Set @Search003 = REPLACE(@Search003,';','')
Set @Search004 = REPLACE(@Search004,';','')
Set @Search005 = REPLACE(@Search005,';','')
Set @Search006 = REPLACE(@Search006,';','')
Set @Search007 = REPLACE(@Search007,';','')
Set @Search008 = REPLACE(@Search008,';','')
Set @Search009 = REPLACE(@Search009,';','')
Set @Search010 = REPLACE(@Search010,';','')
Set @Search011 = REPLACE(@Search011,';','')
Set @Search012 = REPLACE(@Search012,';','')
Set @Search013 = REPLACE(@Search013,';','')
Set @Search014 = REPLACE(@Search014,';','')
Set @Search015 = REPLACE(@Search015,';','')

If RIGHT(@Search001,1) <> '|' Set @Search001 = @Search001 + '|'
If RIGHT(@Search001,2) = '||' Set @Search001 = LEFT(@Search001,LEN(@Search001)-2) + '| |'
If RIGHT(@Search002,1) <> '|' Set @Search002 = @Search002 + '|'
If RIGHT(@Search002,2) = '||' Set @Search002 = LEFT(@Search002,LEN(@Search002)-2) + '| |'
If RIGHT(@Search003,1) <> '|' Set @Search003 = @Search003 + '|'
If RIGHT(@Search003,2) = '||' Set @Search003 = LEFT(@Search003,LEN(@Search003)-2) + '| |'
If RIGHT(@Search004,1) <> '|' Set @Search004 = @Search004 + '|'
If RIGHT(@Search004,2) = '||' Set @Search004 = LEFT(@Search004,LEN(@Search004)-2) + '| |'
If RIGHT(@Search005,1) <> '|' Set @Search005 = @Search005 + '|'
If RIGHT(@Search005,2) = '||' Set @Search005 = LEFT(@Search005,LEN(@Search005)-2) + '| |'
If RIGHT(@Search006,1) <> '|' Set @Search006 = @Search006 + '|'
If RIGHT(@Search006,2) = '||' Set @Search006 = LEFT(@Search006,LEN(@Search006)-2) + '| |'
If RIGHT(@Search007,1) <> '|' Set @Search007 = @Search007 + '|'
If RIGHT(@Search007,2) = '||' Set @Search007 = LEFT(@Search007,LEN(@Search007)-2) + '| |'
If RIGHT(@Search008,1) <> '|' Set @Search008 = @Search008 + '|'
If RIGHT(@Search008,2) = '||' Set @Search008 = LEFT(@Search008,LEN(@Search008)-2) + '| |'
If RIGHT(@Search009,1) <> '|' Set @Search009 = @Search009 + '|'
If RIGHT(@Search009,2) = '||' Set @Search009 = LEFT(@Search009,LEN(@Search009)-2) + '| |'
If RIGHT(@Search010,1) <> '|' Set @Search010 = @Search010 + '|'
If RIGHT(@Search010,2) = '||' Set @Search010 = LEFT(@Search010,LEN(@Search010)-2) + '| |'
If RIGHT(@Search011,1) <> '|' Set @Search011 = @Search011 + '|'
If RIGHT(@Search011,2) = '||' Set @Search011 = LEFT(@Search011,LEN(@Search011)-2) + '| |'
If RIGHT(@Search012,1) <> '|' Set @Search012 = @Search012 + '|'
If RIGHT(@Search012,2) = '||' Set @Search012 = LEFT(@Search012,LEN(@Search012)-2) + '| |'
If RIGHT(@Search013,1) <> '|' Set @Search013 = @Search013 + '|'
If RIGHT(@Search013,2) = '||' Set @Search013 = LEFT(@Search013,LEN(@Search013)-2) + '| |'
If RIGHT(@Search014,1) <> '|' Set @Search014 = @Search014 + '|'
If RIGHT(@Search014,2) = '||' Set @Search014 = LEFT(@Search014,LEN(@Search014)-2) + '| |'
If RIGHT(@Search015,1) <> '|' Set @Search015 = @Search015 + '|'
If RIGHT(@Search015,2) = '||' Set @Search015 = LEFT(@Search015,LEN(@Search015)-2) + '| |'


Insert Into #SearchStrings(String)
Values(@Search001)
Insert Into #SearchStrings(String)
Values(@Search002)
Insert Into #SearchStrings(String)
Values(@Search003)
Insert Into #SearchStrings(String)
Values(@Search004)
Insert Into #SearchStrings(String)
Values(@Search005)
Insert Into #SearchStrings(String)
Values(@Search006)
Insert Into #SearchStrings(String)
Values(@Search007)
Insert Into #SearchStrings(String)
Values(@Search008)
Insert Into #SearchStrings(String)
Values(@Search009)
Insert Into #SearchStrings(String)
Values(@Search010)
Insert Into #SearchStrings(String)
Values(@Search011)
Insert Into #SearchStrings(String)
Values(@Search012)
Insert Into #SearchStrings(String)
Values(@Search013)
Insert Into #SearchStrings(String)
Values(@Search014)
Insert Into #SearchStrings(String)
Values(@Search015)

--Select * From #SearchStrings

Update #SearchStrings Set String = REPLACE(String,'^','''''')


If @MainSearchTable = 'Permit_Main' Set @MainTableKey = 'permit_no'
If @MainSearchTable = 'Project_Main' Set @MainTableKey = 'project_no'
If @MainSearchTable = 'Case_Main' Set @MainTableKey = 'case_no'
If @MainSearchTable = 'License2_Main' Set @MainTableKey = 'license_no'

If @MainSearchTable = 'Generic1_Main' Set @MainTableKey = 'activity_no'

If @MainSearchTable = 'Geo_ownership' Set @MainTableKey = 'recordid'
If @MainSearchTable = 'AEC_Main' Set @MainTableKey = 'st_lic_no'
If @MainSearchTable = 'tvw_CRMIssues' Set @MainTableKey = 'issue_label'

If @MainSearchTable = 'Permit_Main' Set @MainTableType = 'permittype'
If @MainSearchTable = 'Project_Main' Set @MainTableType = 'projecttype'
If @MainSearchTable = 'Case_Main' Set @MainTableType = 'casetype'
If @MainSearchTable = 'License2_Main' Set @MainTableType = 'license_type'

If @MainSearchTable = 'Generic1_Main' Set @MainTableType = 'generictype'

If @MainSearchTable = 'AEC_Main' Set @MainTableType = 'aectype'
If @MainSearchTable = 'tvw_CRMIssues' Set @MainTableType = 'naturetype_text'

If @MainSearchTable = 'Permit_Main' Set @MainTableSubType = 'permitsubtype'
If @MainSearchTable = 'Project_Main' Set @MainTableSubType = 'projectsubtype'
If @MainSearchTable = 'Case_Main' Set @MainTableSubType = 'casesubtype'
If @MainSearchTable = 'License2_Main' Set @MainTableSubType = 'license_subtype'

If @MainSearchTable = 'Generic1_Main' Set @MainTableSubType = 'genericsubtype'

If @MainSearchTable = 'AEC_Main' Set @MainTableSubType = 'aecsubtype'
If @MainSearchTable = 'tvw_CRMIssues' Set @MainTableSubType = 'catgry_text'

If @MainSearchTable = 'tvw_CRMIssues' Set @MainTableStatus = 'status_text' Else Set @MainTableStatus = 'Status'

 While Exists (  Select String From #SearchStrings Where String Is Not Null )
  Begin
	Select Top 1 @SearchString=String,@SearchIndex=SearchIndex From #SearchStrings Where String Is Not Null Order By SearchIndex

	Set @SQL = 'Insert Into #SearchFields(FieldName,FieldType,Operator,FieldValue1,FieldValue2)
	Values('

	Set @NextIndex = charindex('|',@SearchString,@StartIndex)
	--Print @NextIndex
	Set @CurrentIndex = charindex('|',@SearchString,@StartIndex)
	--Print @CurrentIndex

	Set @PreviousIndex = @StartIndex

	Set @FieldNameSS = SUBSTRING(@SearchString,@PreviousIndex,@CurrentIndex-1)
	Print @FieldNameSS

	Set @SQL = @SQL + '''' + @FieldNameSS + ''','

	Set @PreviousIndex = @CurrentIndex+1
	Set @CurrentIndex = charindex('|',@SearchString,@NextIndex+1)
	--Print @CurrentIndex


	While @CurrentIndex > 0
	 Begin
		Set @FieldNameSS = SUBSTRING(@SearchString,@PreviousIndex,@CurrentIndex-@PreviousIndex)
		Print @FieldNameSS
		
		Set @SQL = @SQL + '''' + @FieldNameSS + ''','
		
		Set @PreviousIndex = @CurrentIndex+1
		
		Set @CurrentIndex = charindex('|',@SearchString,@PreviousIndex+1)
		--Print @CurrentIndex
		
	 End

	Set @SQL = SUBSTRING(@SQL,1,len(@SQL)-1) + ')'
	Print(@SQL)
	Exec(@SQL)
	
	Delete #SearchStrings Where SearchIndex = @SearchIndex
	
  End
--Select * From #SearchFields


Set @StartIndex = 1
Set @CurrentIndex = 0
Set @PreviousIndex = 0
Set @NextIndex = 0
Declare @FieldNameSSUnEdited Varchar(700)


If @MainSearchTable IN ('Permit_Main','Project_Main','Case_Main','License2_Main','generic1_main','tvw_CRMIssues')
 Begin
	Set @ColumnsToDisplayFieldNames = @ColumnsToDisplayFieldNames 
	+ '|[' + @MainSearchTable + '.GIS_LOC_RECORDID]' 
	+ '|[' + @MainSearchTable + '.GIS_SITE_APN]'
	+ '|[' + @MainSearchTable + '.GIS_SITE_ALTERNATE_ID]'
	+ '|[' + @MainSearchTable + '.GIS_SITE_GEOTYPE]'
	+ '|[' + @MainSearchTable + '.MAIN_TABLE_TYPE]'
	+ '|[' + @MainSearchTable + '.MAIN_TABLE_SUBTYPE]'
	+ '|[' + @MainSearchTable + '.MAIN_TABLE_STATUS]'
	+ '|[MainKeyField]'
	+ '|'
	Set @ColumnsToDisplay = '[GIS_Link]='''',[MainKeyField]=[MainKeyField],[GIS_SITE_ADDR]=[GIS_SITE_ADDR],[GIS_LAT]=[GIS_LAT],[GIS_LON]=[GIS_LON],[GIS_COORD_X]=[GIS_COORD_X],[GIS_COORD_Y]=[GIS_COORD_Y],' 
	+ @ColumnsToDisplay 
	+ ',[GIS_LOC_RECORDID]=[' + @MainSearchTable + '.GIS_LOC_RECORDID]'
	+ ',[GIS_SITE_APN]=[' + @MainSearchTable + '.GIS_SITE_APN]'
	+ ',[GIS_SITE_ALTERNATE_ID]=[' + @MainSearchTable + '.GIS_SITE_ALTERNATE_ID]'
	+ ',[GIS_SITE_GEOTYPE]=[' + @MainSearchTable + '.GIS_SITE_GEOTYPE]'
	+ ',[MAIN_TABLE_TYPE]=[' + @MainSearchTable + '.MAIN_TABLE_TYPE]'
	+ ',[MAIN_TABLE_SUBTYPE]=[' + @MainSearchTable + '.MAIN_TABLE_SUBTYPE]'
	+ ',[MAIN_TABLE_STATUS]=[' + @MainSearchTable + '.MAIN_TABLE_STATUS]'
 End	
ELSE
	If @MainSearchTable IN ('Geo_Ownership')
	 Begin
		Set @ColumnsToDisplayFieldNames = @ColumnsToDisplayFieldNames 
		+ '|[' + @MainSearchTable + '.GIS_LOC_RECORDID]' 
		+ '|[' + @MainSearchTable + '.GIS_SITE_APN]'
		+ '|[' + @MainSearchTable + '.GIS_SITE_ALTERNATE_ID]'
		+ '|[' + @MainSearchTable + '.GIS_SITE_GEOTYPE]'
		+ '|[' + @MainSearchTable + '.MAIN_TABLE_STATUS]'
		+ '|[MainKeyField]'
		+ '|'
		Set @ColumnsToDisplay = '[GIS_Link]='''',[MainKeyField]=[MainKeyField],[GIS_SITE_ADDR]=[GIS_SITE_ADDR],[GIS_LAT]=[GIS_LAT],[GIS_LON]=[GIS_LON],[GIS_COORD_X]=[GIS_COORD_X],[GIS_COORD_Y]=[GIS_COORD_Y],' 
		+ @ColumnsToDisplay 
		+ ',[GIS_LOC_RECORDID]=[' + @MainSearchTable + '.GIS_LOC_RECORDID]'
		+ ',[GIS_SITE_APN]=[' + @MainSearchTable + '.GIS_SITE_APN]'
		+ ',[GIS_SITE_ALTERNATE_ID]=[' + @MainSearchTable + '.GIS_SITE_ALTERNATE_ID]'
		+ ',[GIS_SITE_GEOTYPE]=[' + @MainSearchTable + '.GIS_SITE_GEOTYPE]'
		+ ',[MAIN_TABLE_TYPE]='''''
		+ ',[MAIN_TABLE_SUBTYPE]='''''
		+ ',[MAIN_TABLE_STATUS]=[' + @MainSearchTable + '.MAIN_TABLE_STATUS]'
	 End
	 ELSE
	 Begin
		Set @ColumnsToDisplayFieldNames = @ColumnsToDisplayFieldNames 
		+ '|[' + @MainSearchTable + '.MAIN_TABLE_TYPE]'
		+ '|[' + @MainSearchTable + '.MAIN_TABLE_SUBTYPE]'
		+ '|[' + @MainSearchTable + '.MAIN_TABLE_STATUS]'
		+ '|[MainKeyField]'
		+ '|'
	Set @ColumnsToDisplay = '[GIS_Link]='''',[MainKeyField]=[MainKeyField],[GIS_SITE_ADDR]='''',[GIS_LAT]='''',[GIS_LON]='''',[GIS_COORD_X]='''',[GIS_COORD_Y]='''',' 
	+ @ColumnsToDisplay 
	+ ',[GIS_LOC_RECORDID]='''''
	+ ',[MAIN_TABLE_TYPE]=[' + @MainSearchTable + '.MAIN_TABLE_TYPE]'
	+ ',[MAIN_TABLE_SUBTYPE]=[' + @MainSearchTable + '.MAIN_TABLE_SUBTYPE]'
	+ ',[MAIN_TABLE_STATUS]=[' + @MainSearchTable + '.MAIN_TABLE_STATUS]'
	 End

-- RB Check if temp table exists
IF OBJECT_ID('tempdb..#Results') IS NOT NULL  
	drop table #Results

Set @SQL2 = 'Create Table #Results(GIS_Link Varchar(300),GIS_SITE_ADDR Varchar(700),GIS_COORD_X float,GIS_COORD_Y float,GIS_LAT float,GIS_LON float)' + CHAR(10) 
Set @SQL = 'Insert Into #Results('

Set @NextIndex = charindex('|',@ColumnsToDisplayFieldNames,@StartIndex)
--Print @NextIndex
Set @CurrentIndex = charindex('|',@ColumnsToDisplayFieldNames,@StartIndex)
--Print @CurrentIndex

Set @PreviousIndex = @StartIndex

Set @FieldNameSSUnEdited = SUBSTRING(@ColumnsToDisplayFieldNames,@PreviousIndex,@CurrentIndex-1)
Print @FieldNameSSUnEdited
Set @FieldNameSS = REPLACE(REPLACE(REPLACE(REPLACE(@FieldNameSSUnEdited,'Convert(Varchar,',''),',101)',''),'LTRIM(STR(',''),',100,2))','')

	SET @isDate = patindex('%Convert(Varchar,%,101)%',@FieldNameSSUnEdited)

	if @isDate > 0 
	  BEGIN
			 Set @SQL2 = @SQL2 + 'Alter Table #Results Add ' + @FieldNameSS + ' DateTime' + CHAR(10) 	  
	  END
	Else
	  BEGIN
			Set @SQL2 = @SQL2 + 'Alter Table #Results Add ' + @FieldNameSS + ' Varchar(2000)' + CHAR(10) 	
	  END

--Set @SQL2 = @SQL2 + 'Alter Table #Results Add ' + @FieldNameSS + ' Varchar(2000)' + CHAR(10) 
Set @SQL = @SQL + @FieldNameSS + ','

Set @PreviousIndex = @CurrentIndex+1
Set @CurrentIndex = charindex('|',@ColumnsToDisplayFieldNames,@NextIndex+1)
--Print @CurrentIndex


While @CurrentIndex > 0
 Begin
	Set @isDate = 0
	Set @FieldNameSSUnEdited = SUBSTRING(@ColumnsToDisplayFieldNames,@PreviousIndex,@CurrentIndex-@PreviousIndex)
	Print @FieldNameSSUnEdited
	Set @FieldNameSS = REPLACE(REPLACE(REPLACE(REPLACE(@FieldNameSSUnEdited,'Convert(Varchar,',''),',101)',''),'LTRIM(STR(',''),',100,2))','')
	
	SET @isDate = patindex('%Convert(Varchar,%,101)%',@FieldNameSSUnEdited)

	if @isDate > 0 
	   BEGIN
			Set @SQL2 = @SQL2 + 'Alter Table #Results Add ' + @FieldNameSS + ' DateTime' + CHAR(10) 
	   END
	  ELSE
	   BEGIN
			Set @SQL2 = @SQL2 + 'Alter Table #Results Add ' + @FieldNameSS + ' Varchar(2000)' + CHAR(10) 
	   END

	--Set @SQL2 = @SQL2 + 'Alter Table #Results Add ' + @FieldNameSS + ' Varchar(2000)' + CHAR(10) 
	Set @SQL = @SQL + @FieldNameSS + ','
	
	Set @PreviousIndex = @CurrentIndex+1
	
	Set @CurrentIndex = charindex('|',@ColumnsToDisplayFieldNames,@PreviousIndex+1)
	--Print @CurrentIndex
 End
 

Set @SQL2 = SUBSTRING(@SQL2,1,len(@SQL2)-1) + CHAR(10)
Set @SQL = SUBSTRING(@SQL,1,len(@SQL)-1) + ')' + CHAR(10)
Set @ColumnsToDisplayFieldNames = SUBSTRING(@ColumnsToDisplayFieldNames,1,len(@ColumnsToDisplayFieldNames)-1) 

--Print(@SQL2)
--Exec(@SQL2)


--Return

GOTO Main


Finish:














GO
