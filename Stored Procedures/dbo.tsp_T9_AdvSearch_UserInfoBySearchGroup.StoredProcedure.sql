USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearch_UserInfoBySearchGroup]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_AdvSearch_UserInfoBySearchGroup]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearch_UserInfoBySearchGroup]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

	Create Procedure [dbo].[tsp_T9_AdvSearch_UserInfoBySearchGroup](
			@DepartmentID int = 0
			) As
	Begin
		select D.UserID, UserName, D.DepartmentID from Prmry_AdvSearch_DepartmentMembers D
		join prmry_users U on U.UserID = D.UseriD
		Where D.DepartmentID = @DepartmentID
	End

GO
