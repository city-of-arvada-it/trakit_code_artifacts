USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearch_FetchDbInfo]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_AdvSearch_FetchDbInfo]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearch_FetchDbInfo]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[tsp_T9_AdvSearch_FetchDbInfo] 
(
       @Group Varchar(50) = Null  
)

AS
BEGIN

declare @SQL varchar(max)
declare @WhereClause varchar(max)

declare @MainTable varchar(50)
declare @UdfTable varchar(50) 

-- identify the main table and udf table for the group
if @Group = 'CRM' or @Group = 'GEO'
begin
	 if @Group = 'CRM'
	 begin
		set @MainTable = 'TVW_CRMISSUES2' --> EWH Nov 8, 2017 #11004
		set @UdfTable = ''-- CRM has no UDF table
	 end
	 else
	 begin
		set @MainTable = 'GEO_OWNERSHIP'
		set @UdfTable = 'GEO_UDF'
	 end
end
else
begin
	set @MainTable =  @Group + '_MAIN' 
	set @UdfTable = @Group + '_UDF'
end


select @whereclause =
case when @Group = 'Geo' then 'Geo_Inspections''' + ',' + '''GEO_INSPECTIONS_UDF''' + ',' + '''Geo_Ownership''' + ',' + '''Geo_OwnershipUDF''' + ',' + '''Geo_People''' + ',' + '''Geo_RESTRICTIONS2''' + ',' + '''GEO_UDF'
	 when @Group = 'CRM' then 'TVW_CRMISSUES2''' + ',' + '''CRM_Attachments' --> EWH Nov 8, 2017 #11004
	 else @group + '_Actions''' + ',''' + @group + '_Conditions2''' + ',''' + @group + '_Conditions2_UDF''' + ',''' + @group + '_INSPECTIONS''' + ',''' + @group + '_INSPECTIONS_UDF''' + ',''' + @group + '_Main''' + ',''' + @group + '_People''' + ',''' + @group + '_Reviews''' + ',''' + @group + '_UDF''' + ',''' + @group + '_Violations_UDF''' + ',''' + @group + '_Violations2'
end
              
--print(@whereclause)

if exists (select * from tempdb.dbo.sysobjects o where o.xtype in ('U') and o.id = object_id(N'tempdb.. #AdvSearchByGroupResults'))
drop table #AdvSearchByGroupResults;

create table  #AdvSearchByGroupResults (TableName varchar(255), FieldName varchar(255), DataType varchar(255))

set @SQL = 'insert into #AdvSearchByGroupResults (TableName, FieldName, Datatype)
			select OBJECT_NAME(object_id) as tblName, name as fldName, type_name(system_type_id) as fldType 
			from sys.columns 
			where OBJECT_NAME(object_id) in (''' + @whereclause + ''') 
			order by  OBJECT_NAME(object_id), name'

--print(convert(varchar,getdate(),109))
exec(@sql)

/*
-- the delete is slightly slower
delete t from #AdvSearchByGroupResults t
join [Prmry_AdvSearch_Exclusions] p on t.TableName = p.TableName and t.FieldName = p.[columnname]
*/

select 	  upper(TableName) [TableName]
		, dbo.tfn_T9_AdvSearch_GetFriendlyTableName(TableName) [FriendlyTableName] -- get a friendly table name for drop down lists
		, upper(FieldName) [FieldName]
		, isnull(upper(Datatype),'') [Datatype] 
		, upper(TableName) + '.' + upper(FieldName) [TableDotColumn]
		, case TableName when @MainTable then 1 
						 when @UdfTable	 then 2
						 else				  3
						 end [seq]
from #AdvSearchByGroupResults
where [#AdvSearchByGroupResults].[TableName]+'|'+[#AdvSearchByGroupResults].[FieldName] not in ( select [Prmry_AdvSearch_Exclusions].[tableName]+'|'+[Prmry_AdvSearch_Exclusions].[columnname] from Prmry_AdvSearch_Exclusions ) 
order by seq, TableName, FieldName

--print(convert(varchar,getdate(),109))

--print(@sql)
--exec(@sql)

END

GO
