USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ITR_ExportCaseUDF]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_ITR_ExportCaseUDF]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ITR_ExportCaseUDF]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  
					  
					  					  					  /*
tsp_ExportCaseUDF @DBName='DEV', @FilePath='C:\CRW\iTrakitExports\', @FileName='CaseUDFExport'

*/
-- =====================================================================
-- Revision History:
--	RR - 10/31/13
--	mdm - 11/12/13 - added revision history; expanded picklist
-- =====================================================================    
CREATE Procedure [dbo].[tsp_ITR_ExportCaseUDF](
@DBName Varchar(50),
@FilePath Varchar(1000),
@FileName Varchar(150),
@ToggleXP_CMDShell INT = 1
)

As

SET NOCOUNT ON

Declare @SQL Varchar(8000)

If @ToggleXP_CMDShell Is Null Set @ToggleXP_CMDShell = 1

IF EXISTS (SELECT * FROM tempdb.sys.objects WHERE object_id = OBJECT_ID(N'tempdb..CaseUDF') AND type in (N'U'))
	Drop Table tempdb..CaseUDF

Create Table tempdb..CaseUDF(
[TYPENAME] Varchar(200)
,[SCREEN] Varchar(200)
,[UDF] Varchar(200)
,[DATATYPE] Varchar(200)
,[DATAFIELD] Varchar(200)
,[CAPTION] Varchar(200)
,[PICKLIST] Varchar(8000)
,[PROPERTIES] Varchar(200)
,[ORDERID] Varchar(200)
,[RECORDID] Varchar(200)
)


Insert Into tempdb..CaseUDF(TYPENAME,SCREEN,UDF,DATATYPE,DATAFIELD,CAPTION,PICKLIST,PROPERTIES,ORDERID,RECORDID)
Values('TYPENAME','SCREEN','UDF','DATATYPE','DATAFIELD','CAPTION','PICKLIST','PROPERTIES','ORDERID','RECORDID')

Set @SQL = '
Insert Into tempdb..CaseUDF(TYPENAME,SCREEN,UDF,DATATYPE,DATAFIELD,CAPTION,PICKLIST,PROPERTIES,ORDERID,RECORDID)
Select 
TYPENAME=CASE WHEN TYPENAME = '''' THEN Null ELSE REPLACE(REPLACE(REPLACE(TYPENAME,CHAR(9),''''),CHAR(10),''''),CHAR(13),'''') END,
SCREEN=CASE WHEN SCREEN = '''' THEN Null ELSE REPLACE(REPLACE(REPLACE(SCREEN,CHAR(9),''''),CHAR(10),''''),CHAR(13),'''') END,
UDF=CASE WHEN UDF = '''' THEN Null ELSE UDF END,
DATATYPE=CASE WHEN DATATYPE = '''' THEN Null ELSE DATATYPE END,
DATAFIELD=CASE WHEN DATAFIELD = '''' THEN Null ELSE DATAFIELD END,
CAPTION=CASE WHEN CAPTION = '''' THEN Null ELSE REPLACE(REPLACE(REPLACE(CAPTION,CHAR(9),''''),CHAR(10),''''),CHAR(13),'''') END,
PICKLIST=CASE WHEN PICKLIST = '''' THEN Null ELSE Replace(Replace(Replace(Replace(PICKLIST,CHAR(10),''''),CHAR(13),''''),CHAR(11),''''),CHAR(124),'' '') END,
PROPERTIES=CASE WHEN PROPERTIES = '''' THEN Null ELSE REPLACE(REPLACE(REPLACE(PROPERTIES,CHAR(9),''''),CHAR(10),''''),CHAR(13),'''') END,
ORDERID=CASE WHEN ORDERID = Null THEN ''999'' ELSE Convert(Varchar,ORDERID) END,
RECORDID=CASE WHEN RECORDID = '''' THEN Null ELSE RECORDID END
From ' + @DBName + '..Prmry_TypesUDF
WHERE GROUPNAME = ''VIOLATIONS''
ORDER BY GROUPNAME,TYPENAME,SCREEN,UDF'
--Print(@SQL)
Exec(@SQL)

Select * From tempdb..CaseUDF

Declare @vDate Varchar(100)
Declare @OutputFileName Varchar(1000)

Set @vDate = Replace(Convert(Varchar,GetDate(),102),'.','_')
Print(@vDate)

--Set @OutputFileName = '"' + @FilePath + @FileName + '_' + @vDate + '.csv"'
Set @OutputFileName = '"' + @FilePath + @FileName + '.csv"'


If @ToggleXP_CMDShell = 1
 Begin
	--Enable xp_cmdshell
	Set @SQL = '
	EXEC sp_configure ''show advanced options'', 1
	RECONFIGURE
	EXEC sp_configure ''xp_cmdshell'', 1
	RECONFIGURE
	'
	Print(@SQL)
	Exec(@SQL)
 End

select @sql = 'bcp "select * from tempdb..CaseUDF" queryout ' + @OutputFileName + ' -c -t"|" -T -S'
+ @@servername
--Print(@SQL)
exec master..xp_cmdshell @sql

Drop Table tempdb..CaseUDF

Set @SQL = '
DELETE ' + @DBName + '.dbo.iTrakitFeeds Where [FileName] = ''' + @FileName + '.csv''

INSERT INTO ' + @DBName + '.dbo.iTrakitFeeds([filename],[data])
SELECT ''' + @FileName + '.csv'', * FROM OPENROWSET(
  BULK N''' + REPLACE(@OutputFileName,'"','') + ''', SINGLE_BLOB
) bt'
Print(@SQL)
Exec(@SQL)


If @ToggleXP_CMDShell = 1
 Begin
	-- Disable xp_cmdshell
	Set @SQL = '
	EXEC sp_configure ''xp_cmdshell'', 0
	RECONFIGURE
	EXEC sp_configure ''show advanced options'', 0
	RECONFIGURE
	'
	Print(@SQL)
	Exec(@SQL)
 End










GO
