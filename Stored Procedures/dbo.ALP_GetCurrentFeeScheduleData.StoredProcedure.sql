USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[ALP_GetCurrentFeeScheduleData]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[ALP_GetCurrentFeeScheduleData]
GO
/****** Object:  StoredProcedure [dbo].[ALP_GetCurrentFeeScheduleData]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =========================================================================================
-- Author:		Erick Hampton
-- Create date: Feb 15, 2017
-- Description:	Gets the current fee schedule name and fee data from Prmry_FeeScheduleList
-- =========================================================================================
CREATE PROCEDURE [dbo].[ALP_GetCurrentFeeScheduleData]

	@ScheduleName varchar(40) = 'DEFAULT' out

AS
BEGIN
	-- get the schedule name
	declare @RENEW_DATE DateTime = GetDate()
	DECLARE @SCHEDULE_EFFECTIVE_DATE as DATETIME
	SET @SCHEDULE_EFFECTIVE_DATE = (SELECT MAX(EFFECTIVE_ON) FROM Prmry_FeeScheduleList WHERE Prmry_FeeScheduleList.EFFECTIVE_ON < @RENEW_DATE AND TEST <> 1)
	SET @ScheduleName = isnull((
							SELECT TOP 1 SCHEDULE_NAME 
							FROM Prmry_FeeScheduleList 
							WHERE (TEST <> 1 AND EFFECTIVE_ON = @SCHEDULE_EFFECTIVE_DATE) 
							   or (TEST = 1 AND EFFECTIVE_ON is null) 
							ORDER BY EFFECTIVE_ON desc
					), 'DEFAULT')

	-- get the data for the schedule name
	select PARENT_CODE, CODE, [DESCRIPTION], FORMULA, ACCOUNT 
	from Prmry_FeeSchedule
	where SCHEDULE_NAME=@ScheduleName and FEETYPE='FEE' and GroupName='LICENSE2'
END

GO
