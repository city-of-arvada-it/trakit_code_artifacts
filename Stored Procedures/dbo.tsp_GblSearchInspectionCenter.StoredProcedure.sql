USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GblSearchInspectionCenter]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_GblSearchInspectionCenter]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GblSearchInspectionCenter]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================================================================================================================
-- Revision History:
--     MDM - 01/13/14 - added revision Hist
--     MLM - 01/09/14 - last changes
--	   EWH - Mar 23, 2015 - Issue 36413 & 36410 - items outside search criteria are returned
--							* updated vStart / vEnd empty strings to "1/1/1800" and "1/1/3000" to match getData.aspx.vb GetInspections()
--							* updated start date time to midnight and end date to 11:59:59 PM
--							* added variable for order by, which is the @DateType + '_datetime'; default = 'scheduled_date_datetime'
--							* updated logic for @ShowOverdueItems: include record if (date within range OR completed_date = null)
--							* added with (nolock) to from clauses
--     EWH Apr 23, 2015 - added parentheses to keep from showing overdue items outside of the query constraints
--     DC  Apr 24, 2015 - fixed checks for scheduled date anf incomplete and overdue and incomplete
--	   RB  May 20, 2015 - Added more columns for Inspection Center Sort
--     MJ  Sep 20, 2016 - Removed GenericTrak
--	   AU  Nov 04, 2019 - Reverting change where SITE_ADDR no longer included City
-- =========================================================================================================================================================
					  
					  					  
/*

tsp_GblSearchInspectionCenter @InspectorID='RJE'
,@vStartDate='6/7/2012'
,@vEndDate='6/13/2012'
,@DateType='Scheduled_Date'
,@Group='Permit,Geo,License2'
,@Type='HEALTH,INSP2LIC2,FRAMING'
,@ShowOverdueItems=1

tsp_GblSearchInspectionCenter @InspectorID = 'MDP',  @ShowOverdueItems = '1',  @DateType = 'Scheduled_Date',  
@vStartDate = '2/7/2013 12:00:00 AM',  @vEndDate = '2/7/2013 12:00:00 AM'

tsp_GblSearchInspectionCenter @InspectorID = 'MDP',  @ShowOverdueItems = '1',  @DateType = 'Scheduled_Date',  @vStartDate = '4/22/2013 12:00:00 AM',  @vEndDate = '4/22/2013 12:00:00 AM'

*/

CREATE Procedure [dbo].[tsp_GblSearchInspectionCenter](
@InspectorID Varchar(1000) = Null,
@vStartDate Varchar(100) = Null,
@vEndDate Varchar(100) = Null,
@DateType Varchar(100) = Null,
@Group Varchar(500) = Null,
@Type Varchar(500) = Null,
@ShowOverdueItems INT = 0
)


As

Declare @SQL Varchar(max) 
declare @select varchar(max)
Declare @WHERE Varchar(max) 
Declare @StartDate varchar(30) --Datetime
Declare @EndDate varchar(30) --Datetime
declare @OrderBy varchar(50) = null
Declare @today varchar(30) --Datetime


-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- establish date ranges with date fields
-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------

declare @sd date 
declare @ed date 

If @ShowOverdueItems Is Null or @ShowOverdueItems='' Set @ShowOverdueItems = 0 
If @vStartDate = '' Set @vStartDate = '1/1/1800'
If @vStartDate = '1/1/0001 12:00:00 AM' Set @vStartDate = '1/1/1800'
If @vEndDate = '' Set @vEndDate = '1/1/3000'
If @vEndDate = '1/1/0001 12:00:00 AM' Set @vEndDate = '1/1/3000'
set @today = CONVERT(varchar(4), DATEPART(yyyy, GETDATE())) + '-' + CONVERT(varchar(2), DATEPART(mm, GETDATE())) + '-' + CONVERT(varchar(2), DATEPART(dd, GETDATE())) + ' 23:59:59.000'
-- set start date to the select date at 0:00:00 AM
If @vStartDate Is Null 
begin
	Set @sd = GetDate()
end
else
begin
	Set @sd = CONVERT(Date, @vStartDate)
end
set @StartDate = CONVERT(varchar(4), DATEPART(yyyy, @sd)) + '-'  + CONVERT(varchar(2), DATEPART(mm, @sd)) + '-' + CONVERT(varchar(2), DATEPART(dd, @sd)) + ' 0:00:00.000'

-- set end date to the selected date at 23:59:59.000
If @vEndDate Is Null 
begin
	Set @ed = GetDate()
end
else
begin
	Set @ed = CONVERT(Date, @vEndDate)
end
set @EndDate = CONVERT(varchar(4), DATEPART(yyyy, @ed)) + '-' + CONVERT(varchar(2), DATEPART(mm, @ed)) + '-' + CONVERT(varchar(2), DATEPART(dd, @ed)) + ' 23:59:59.000'

--print (@StartDate)
--print (@EndDate)
-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------


-- ---------------------------------------------------------------------------------------------------------------------------------
-- create where clause with @InspectorID, @Group, @Type, and @ShowOverdueItems filters
-- ---------------------------------------------------------------------------------------------------------------------------------
If @InspectorID = '' Set @InspectorID = Null
If @Group = '' Set @Group = Null
If @Type = '' Set @Type = Null

If @DateType = '' or @DateType is null Set @DateType ='scheduled_date'

Set @WHERE = ''-- where 1=1 '

If @InspectorID Is Not Null Set @WHERE = @WHERE + CHAR(10) + ' AND INSPECTOR IN (''' + REPLACE(@InspectorID,',',''',''') + ''')'
If @Group Is Not Null Set @WHERE = @WHERE + CHAR(10) + ' AND tGroup IN (''' + REPLACE(@Group,',',''',''') + ''')'
If @Type Is Not Null Set @WHERE = @WHERE + CHAR(10) + ' AND InspectionType IN (''' + REPLACE(@Type,',',''',''') + ''')'

-- EWH Apr 23, 2015: added parentheses to keep from showing overdue items outside of the query constraints
-- DC Apr 24 2015: Fixed checks for scheduled and incomplete
-- DC Apr 27 2015: Fixed completed_date check

-- Incomplete and overdue
if  @DateType = 'scheduled_date' and @ShowOverdueItems = 1
Set @WHERE = @WHERE + CHAR(10) + ' AND ((' + @DateType + ' between convert(datetime,''' + @StartDate + ''') and convert(datetime,''' + @EndDate + ''') and completed_date is null) '
else
-- All Scheduled and All Completed
Set @WHERE = @WHERE + CHAR(10) + ' AND ((' + @DateType + ' between convert(datetime,''' + @StartDate + ''') and convert(datetime,''' + @EndDate + '''))'

-- DC Apr 24 2015: Fixed checks for overdue and incomplete
-- DC Apr 28 2015: Fixed Today value 
-- Overdue
If @ShowOverdueItems = 1 Set @WHERE = @WHERE + ' OR ('+ @DateType + ' < CAST(FLOOR(CAST(getdate() AS float)) AS DATETIME) and completed_date is null)'  
Set @WHERE = @WHERE + ') '



--print (@where)
-- ---------------------------------------------------------------------------------------------------------------------------------
 


-- ---------------------------------------------------------------------------------------------------------------------------------
-- create order by 
-- ---------------------------------------------------------------------------------------------------------------------------------

--if @DateType ='' or @DateType is null
--begin
--	set @OrderBy = ' order by scheduled_date_datetime, RECORDID'
--end
--else
--begin
--	set @OrderBy = ' order by ' + @DateType + '_datetime, RECORDID'
--end


--print (@OrderBy)
-- ---------------------------------------------------------------------------------------------------------------------------------

--print (@where)
--print (@OrderBy)
Create Table #Results(ActivityNo Varchar(40),RecordID Varchar(40),Loc_RecordID Varchar(40),tGroup Varchar(40),ActivityTypeID int
,AttachmentTotal INT Default 0, ConditionsTotal INT Default 0, NotesTotal INT Default 0, RestrictionsTotal INT Default 0, FeesDue Money Default 0, RestrictionsSummary Varchar(5000) Default '',RestrictionsRecordIDUsed Varchar(40),RECORD_TYPE Varchar(60),RECORD_STATUS Varchar(60),SITE_ADDR Varchar(500),InspectorName Varchar(150),RECORD_SUBTYPE Varchar(60),SCHEDULED_DATEANDTIME Datetime, SITE_NUMBER Varchar(20), SITE_STREETNAME Varchar(100), SITE_CITY Varchar(50),BondBalanceDue money default 0, PathURL varchar(500))
--declare @SQL varchar(max)
Set @SQL = '
insert into #results
(ActivityNo,RecordID,tGroup,ActivityTypeID,Loc_RecordID,Record_Type,Record_Status,SITE_ADDR,SITE_NUMBER,SITE_STREETNAME,SITE_CITY,
RECORD_SUBTYPE,InspectorName, PathURL)
Select distinct Top 500 ActivityID as ActivityNo,r.RecordID,[ActivityTypeName] as tGroup,a.ActivityTypeID,
b.Loc_RecordID,b.ActivityType,b.[Status],
(IsNull(b.SITE_ADDR,'''') + IsNull('', '' + b.SITE_CITY,'''')) AS SITE_ADDR,
isNUll(b.SITE_NUMBER,''''),
 isNull(b.SITE_STREETNAME,''''),
 isNull(b.SITE_CITY,'''')
,ActivitySubType
,CASE WHEN ic.Field03 IS NULL THEN r.Inspector ELSE ic.Field03 END 
,PathURL
from [dbo].[inspections]  r join [dbo].[ActivityType] a
on a.ActivityTypeID=r.ActivityTypeID
join  tvw_GblActivities b
on b.ActivityNo=r.ActivityID
LEFT JOIN Prmry_InspectionControl ic
ON r.INSPECTOR = ic.Field01
Where 1 = 1 
AND (Remarks Is Null OR Remarks NOT LIKE ''VOIDED (%'')
' + @WHERE
PRINT(@SQL)
EXEC(@SQL)

print 'bond amount'

 Update #Results Set
BondBalanceDue =  a.bond_Amount 
From #Results tr
JOIN (	
select ActivityID,ActivityTypeID,
isnull(sum([bonds].bond_amount), 0 ) bond_Amount
from [bonds] 
where  [bonds].paid = 0 and [bonds].PAID_AMOUNT = 0
group by  ActivityID,ActivityTypeID
 ) a
  ON tr.ActivityNo = a.ActivityID
and tr.ActivityTypeID=a.ActivityTypeID
 

Print 'Note Count'
Update #Results Set
NotesTotal = n.Total
From #Results tr
 JOIN (
	Select Total=Count(*),SubGroupRecordID From Prmry_Notes
	WHERE SubGroup = 'INSPECTION'
	Group By SubGroupRecordID
 ) n
  ON tr.RecordID = n.SubGroupRecordID



  
Print 'Restriction Count'
Update #Results Set
RestrictionsTotal = isnull(r.Total,0)
From #Results tr
 JOIN (
	Select Total=Count(*),Loc_RecordID From Geo_Restrictions2
	Where Date_Cleared Is Null AND Restriction_Notes Not Like 'VOIDED (%'
	Group By Loc_RecordID
 ) r
  ON tr.Loc_RecordID = r.Loc_RecordID
  

IF OBJECT_ID(N'tempdb..#Restrictions') IS NOT NULL
             BEGIN
                    DROP TABLE #Restrictions
             END
SELECT      DISTINCT
         loc_recordid,

         RESTRICTION=STUFF(
               (SELECT      '<br />' + summary
               FROM      
			   (select loc_recordid, summary= isnull(x2.Restriction_Type,'') + ' ('
				+ Convert(Varchar,isnull(x2.Date_Added,''),101) + ') ' + SubString(x2.Restriction_Notes,1,50)
               FROM      Geo_RESTRICTIONS2 x2
			   where x2.Date_Cleared Is Null AND x2.Restriction_Notes Not Like 'VOIDED (%')
			   --and  loc_recordid='CONV:151116183544066713'
			    AS x2
               WHERE   
			    x.loc_recordid = x2.loc_recordid
			   
               FOR XML PATH('')), 1, 1, '')  
			  into #Restrictions 
FROM      Geo_RESTRICTIONS2 as x 
where loc_Recordid in(select loc_recordid from #results)
ORDER BY   loc_recordid 

Print 'update #results'
	Update #Results Set
	RestrictionsSummary = RESTRICTION	
	From #Results tr
	JOIN #Restrictions r
	 ON tr.Loc_RecordID = r.Loc_RecordID
	 
  
Print 'Attachments'
Update #Results Set
AttachmentTotal = isnull(a.total,0)
From #Results tr
 JOIN (
	Select Total=Count(*),ActivityID,ActivityTypeID From attachments
	Group By ActivityID,ActivityTypeID
 ) a
  ON tr.ActivityNo = a.ActivityID
  and tr.ActivityTypeID=a.ActivityTypeID
 

Print ' Fees Due'
Update #Results Set
FeesDue =  a.Total 
From #Results tr
JOIN (	
Select ActivityID ,ActivityTypeID,Total=Sum(amount)-sum(PAID_AMOUNT)  From fees	
		Group By ActivityID ,ActivityTypeID
		having  Sum(amount)-sum(PAID_AMOUNT) >0
 ) a
  ON tr.ActivityNo = a.ActivityID
and tr.ActivityTypeID=a.ActivityTypeID



  
Print 'Conditions Count'
Update #Results Set
ConditionsTotal =  c.Total 
From #Results tr
JOIN (
	Select Total=Count(*),activityid,ActivityTypeID From Conditions
	Group By activityid,ActivityTypeID
 ) c
  ON tr.ActivityNo = c.activityid
WHERE tr.ActivityTypeID =tr.ActivityTypeID



--Set Scheduled_DateAndDate for proper sorting by Scheduled Date and Time
Update tr Set
Scheduled_DateAndTime = SCHEDULED_DATE_DATETIME
From tvw_GblInspections vi with (nolock) 
JOIN #Results tr
  ON vi.RecordID = tr.RecordID

BEGIN TRY
	Update tr Set
	Scheduled_DateAndTime = 
	CASE 
		WHEN REPLACE(Scheduled_Time,' ','') IN ('AM') THEN CONVERT(Datetime,(Scheduled_Date + ' 11:59:59:00'))
		WHEN REPLACE(Scheduled_Time,' ','') IN ('PM') THEN CONVERT(Datetime,(Scheduled_Date + ' 23:59:59:00'))
		ELSE CONVERT(Datetime,(Scheduled_Date + ' ' + IsNull(Scheduled_Time,''))) END
	From tvw_GblInspections vi with (nolock) 
	JOIN #Results tr
	  ON vi.RecordID = tr.RecordID
END TRY
BEGIN CATCH
END CATCH


Select 
tr.AttachmentTotal,tr.NotesTotal,tr.ConditionsTotal,tr.RestrictionsTotal,tr.FeesDue,tr.RestrictionsSummary,tr.Loc_RecordID,tr.Record_Type,tr.Record_Status,tr.Record_Subtype,
tr.Site_Addr,tr.InspectorName,tr.SCHEDULED_DATEANDTIME,vi.*,tr.SITE_NUMBER, tr.SITE_STREETNAME, TR.SITE_CITY,vi.PathURL,vi.Parent_RECORDID
From tvw_GblInspections vi with (nolock) 
JOIN #Results tr
  ON vi.RecordID = tr.RecordID


GO
