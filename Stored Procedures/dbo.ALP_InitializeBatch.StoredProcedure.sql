USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[ALP_InitializeBatch]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[ALP_InitializeBatch]
GO
/****** Object:  StoredProcedure [dbo].[ALP_InitializeBatch]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- ==============================================================================================================================================================================
-- Author:		Erick Hampton
-- Create date: Dec 11, 2016
-- Description:	Initializes a batch for processing by inserting relevant data into the batch operation status tracking table
--				* Can pass a list of license numbers 
--				* Provide a SearchID for a shared search to use the license numbers from the search results 
--				
--				IMPORTANT: BOTH the supplied licenses numbers in LicenseNumberList AND the license numbers derived from the shared search are included in the batch.
--
-- Revisions:	EWH Jun 28, 2017: replace escaped characters; removed unnecessary row number column from @sql
--				EWH Jul 12, 2017: moved where we replace escaped characters in case they are elsewhere in the select statement beside the where clause
-- ==============================================================================================================================================================================
CREATE PROCEDURE [dbo].[ALP_InitializeBatch]

	@CallerID int = -1,
	@LicenseNumberList [ALP_LicenseNumberList] READONLY,
	@SearchID int = -1,
	@ParameterSetID int = -1,
	@IsUpdateMode bit,
	@BatchID int = -1 out,
	@UserNotes varchar(255) = '',
	@ErrorType varchar(255) = '' out -- corresponds to the custom exception types

AS
BEGIN

	-- make sure we have enough info supplied
	if (select count(*) from @LicenseNumberList) > 0 or @SearchID > -1 
	begin
		
		-- this is the default processing order
		declare @DefaultProcessingOrder int = 10

		-- this table gets populated with the distinct license numbers we want
		declare @LicenseNumbers table ([LICENSE_NO] varchar(20), [ProcessingOrder] int NULL)

		-- if we have a search ID, get the license numbers that result from executing the search
		if @SearchID > -1 
		begin
			-- get the alias for license_no for the shared search
			declare @license_no_alias varchar(255) = ''
			select top 1 @license_no_alias=DisplayName from [Prmry_AdvSearch_SharedDetail] where ItemID=@SearchID and TableName='LICENSE2_MAIN' and FieldName='LICENSE_NO' 
			if @license_no_alias='' or @license_no_alias is null 
			begin
				set @ErrorType = 'MissingColumnForLicenseNo'
			end
			else
			begin
				-- license_no is part of the search and we know the alias
		
				-- populate variables needed for search
				-- NOTE: UserID is irrelevent; it is used for CRM searches not LICENSE2 searches
				declare @SearchType varchar(50) = 'LICENSE2_MAIN' 
				declare @Description varchar(250) = null
				declare @SearchName varchar(50) = null
				declare @Group varchar(50) = 'LICENSE2'
				declare @RefSearchID int = null
				declare @RefTable varchar(50) = null
				declare @SelectClause varchar(max) = ''
				declare @SelectAlias varchar(max) = ''
				declare @FromClause varchar(max) = ''
				declare @WhereClause varchar(max) = ''
				declare @GroupByClause varchar(max) = ''
				declare @OrderByClause varchar(max) = @license_no_alias 
				declare @OrderByInOver varchar(max) = '[LICENSE2_MAIN].[LICENSE_NO]' 
				EXEC [tsp_T9_AdvSearch_CompileQuery] '_ALP', 'shared' , @SearchID, @license_no_alias, 'asc', 'NA', 'NA', @SearchType out, @Description out, @SearchName out, @Group out, @RefSearchID out, @RefTable out, @SelectClause out, @SelectAlias out, @FromClause out, @WhereClause out, @OrderByClause out, @OrderByInOver out, 0, 0 

				-- execute shared search to get list of license_no values
				if @WhereClause <> '' set @WhereClause = ' WHERE ' + @WhereClause

				if @GroupByClause <> '' set @GroupByClause = ' GROUP BY ' + @GroupByClause
				declare @sql nvarchar(max) = ''
				--set @sql = 'SELECT ' + @SelectAlias + ' FROM (SELECT ' + @SelectClause + ' FROM ' + @FromClause + @WhereClause + @GroupByClause + ') a ' 		
				set @sql = 'SELECT ' + @SelectAlias + ' FROM (SELECT ' + ' ROW_NUMBER() OVER(ORDER BY [LICENSE2_MAIN].[LICENSE_NO]) as ["row number"], ' + @SelectClause + ' FROM ' + @FromClause + @WhereClause + @GroupByClause + ') a ' 


				-- replace any XML encoding made by SQL Server / ADO.NET
				-- this could be in a few different clauses, so we need to check almost the whole select statement	
				set @sql=replace(replace(replace(replace(replace(@sql,'&amp;','&'),'&apos;',char(39)),'&quot;','"'),'&gt;','>'),'&lt;','<')

				-- it's a little easier to read like this:
				--set @sql=replace(@sql,'&lt;','<')
				--set @sql=replace(@sql,'&gt;','>')
				--set @sql=replace(@sql,'&quot;','"')
				--set @sql=replace(@sql,'&apos;',char(39))
				--set @sql=replace(@sql,'&amp;','&')


				-- insert just get the license_no values into a table parameter
				-- NOTE: we only select LICENSE_NO; sort order is not implemented
				set @sql = 'SELECT DISTINCT ' + @license_no_alias + ' as [LICENSE_NO] FROM (' + @sql + ') alp'

				print @sql

				insert into @LicenseNumbers (LICENSE_NO) exec (@sql)

				-- make sure we have results from the search itself if we weren't given a list of licenses too
				if (select count(*) from @LicenseNumbers) = 0 and (select count(*) from @LicenseNumberList) = 0
				begin
					set @ErrorType = 'NoSearchResults'
				end
			end
		end

		-- only continue if we are error-free
		if @ErrorType = '' or @ErrorType is null
		begin

			-- Add in any license numbers supplied in @LicenseNumberList that aren't already in there
			-- making sure we get only one license number, as there could be multiples with different processing orders
			if (select count(*) from @LicenseNumberList) > 0
			begin
				insert into @LicenseNumbers ([LICENSE_NO], [ProcessingOrder]) 
				select distinct [LICENSE_NO], isnull(ProcessingOrder, @DefaultProcessingOrder) [ProcessingOrder]
				from (	select [LICENSE_NO], min(isnull([ProcessingOrder], @DefaultProcessingOrder)) [ProcessingOrder] 
						from @LicenseNumberList 
						where LICENSE_NO not in (select LICENSE_NO from @LicenseNumbers)
						group by [LICENSE_NO]) a 
				order by ProcessingOrder, LICENSE_NO 
			end


			-- if this is a renewal, delete out any licenses that have already been renewed if we are presently in the documented renewal range
			-- NOTE: need to compare the MAPPED renewal range, not the one stored in [ALP_LicensesRunPerRenewal]
			--		 because the renewal ranges may have changed since the last renewal
			if @ParameterSetID = -1 or @ParameterSetID is null
			begin
				delete from @LicenseNumbers where [LICENSE_NO] in
				(
					SELECT lrpr.[LICENSE_NO]
					FROM [ALP_LicensesRunPerRenewal] lrpr
						inner join [ALP_ParameterSetAndRenewalPeriodByLicenseNumber] psarpbln 
						on lrpr.[LICENSE_NO]=psarpbln.[LICENSE_NO] and lrpr.[ParameterSetID]=psarpbln.[ParameterSetID]
					where getdate() between psarpbln.[RenewalPeriodStart] and psarpbln.[RenewalPeriodEnd]
				)
			end


			-- delete any licenses from @LicenseNumbers that are in batches already that are still processing, and any of the license operations are still processing
			delete from @LicenseNumbers where [LICENSE_NO] in
			(
				select opstatus.[LicenseNumber]
				from [ALP_OperationStatusByBatchParameterSetLicenseNumber] opstatus inner join [ALP_Batches] b on opstatus.BatchID=b.Id
				where b.StatusID in (1, 2, 3) and opstatus.OperationStatusID in (0, 1) 
			)

			-- only proceed if we actually have stuff to process
			if (select count(*) from @LicenseNumbers) > 0
			begin
				-- Test to see if there are any config issues before inserting a row into ALP_Batches
				declare @testPassed bit = 0
				
				if @ParameterSetID = -1 or @ParameterSetID is null
				begin
					-- need to check all of the parameter sets 
					declare @ParameterSetList table (ParameterSetID int)
					insert into @ParameterSetList 
					select distinct ParameterSetID from [ALP_ParameterSetAndRenewalPeriodByLicenseNumber] where license_no in (select license_no from @LicenseNumbers)	
					
					declare @psid int
					declare @counter int=(select count (*) from @ParameterSetList)
					while @counter > 0
					begin
						select top 1 @psid=ParameterSetID from @ParameterSetList order by ParameterSetID
						exec [ALP_CheckParameterSets] @psid, @testPassed out, @ErrorType out
						if @testPassed=1
						begin
							delete from @ParameterSetList where ParameterSetID=@psid
							set @counter = (select count (*) from @ParameterSetList)
						end
						else break
					end
				end
				else
				begin
					-- just check this guy
					exec [ALP_CheckParameterSets] @ParameterSetID, @testPassed out, @ErrorType out
				end


				if @testPassed = 1
				begin
					-- insert a row into the Batches table to get its ID = @BatchID
					insert into ALP_Batches (StatusID, Notes, IsUpdateMode, ParameterSetID, SharedSearchID, CallerID) values (1, @UserNotes, @IsUpdateMode, @ParameterSetID, @SearchID, @CallerID) -- SUBMITTED
					set @BatchID = SCOPE_IDENTITY()

					-- insert info into operation status tracking table [ALP_BatchParameterSetLicenseNumberOperationStatus]
					if @ParameterSetID = -1 or @ParameterSetID is null
					begin
						-- this is a renewal, need to determine the parameter set to use for each license number
						insert into [ALP_BatchParameterSetLicenseNumberOperationStatus] (BatchID, LicenseNumber, ProcessingOrder, ParameterSetID, OperationID, OperationStatusID)
						select @BatchID, license_no, ProcessingOrder, ParameterSetID, OperationId, 0 
						from (  select distinct list.license_no, isnull(list.ProcessingOrder, @DefaultProcessingOrder) [ProcessingOrder], ln.ParameterSetID, pso.OperationId, pso.Sequence [OperationSequence]
								from ALP_LicenseNumbersEligibleForRenewal ln 
									inner join [ALP_ParameterSetOperations] pso on ln.ParameterSetID=pso.ParameterSetId
									inner join @LicenseNumbers list on ln.license_no=list.LICENSE_NO) a
						order by license_no, ParameterSetID, OperationSequence
					end
					else
					begin
						-- use the supplied parameter set for each license number resulting from the shared search
						insert into [ALP_BatchParameterSetLicenseNumberOperationStatus] (BatchID, LicenseNumber, ProcessingOrder, ParameterSetID, OperationID, OperationStatusID)
						select @BatchID, license_no, ProcessingOrder, @ParameterSetID, OperationId, 0 
						from (  select distinct list.license_no, isnull(list.ProcessingOrder, @DefaultProcessingOrder) [ProcessingOrder], pso.OperationId, pso.Sequence [OperationSequence]
								from @LicenseNumbers list
									inner join [ALP_ParameterSetOperations] pso on pso.ParameterSetId=@ParameterSetID) a
						order by license_no, OperationSequence
					end
				end
				else
				begin
					if @ErrorType = '' set @ErrorType = 'ParameterSetOperationsConfigIssue' -- we should always have an @ErrorType if @testPassed=0, but just in case...
				end
			end
			else
			begin
				set @ErrorType = 'NothingToProcess'
			end
		end
	end 
	else
	begin
		set @ErrorType = 'NotEnoughInfoSupplied'
	end

END
GO
