USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[___TRAKiT_173_UPDATE_ERROR_CHECK]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[___TRAKiT_173_UPDATE_ERROR_CHECK]
GO
/****** Object:  StoredProcedure [dbo].[___TRAKiT_173_UPDATE_ERROR_CHECK]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[___TRAKiT_173_UPDATE_ERROR_CHECK]
AS
BEGIN
	SET NOCOUNT ON;
	declare @errMsg nvarchar(max) = '';
	begin try
		if object_id(N'[dbo].[___TRAKiT_173_UPDATE]') is null set @errMsg = 'Database update log table [dbo].[___TRAKiT_173_UPDATE] not found. Execute the process from the beginning.';
		else if (select count(*) from [dbo].[___TRAKiT_173_UPDATE] where successful = 0) > 0 set @errMsg = 'Errors exist in the database update log.  Review the table [dbo].[___TRAKiT_173_UPDATE] and rerun any database update scripts IN ORDER that require attention. ' + @errMsg;
	end try
	begin catch
		set @errMsg = error_message();
	end catch
	if @errMsg <> '' raiserror (@errMsg, 20, 0) with log, nowait; -- no es bueno
END
GO
