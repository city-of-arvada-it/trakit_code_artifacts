USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_DevReviewClearpoint]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[usp_Arvada_DevReviewClearpoint]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_DevReviewClearpoint]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_Arvada_DevReviewClearpoint]
	@Step nvarchar(max) = null,
	@ReportType varchar(50) = 'Summary',
	@startdate datetime,
	@endDate datetime,
	@status varchar(500) = null,
	@ProjectStatus varchar(max) = null,
	@debug int = null
as
begin

/*
exec dbo.usp_Arvada_DevReviewClearpoint
	@status = '',
	@ReportType = 'detail',
	@startdate = '01/01/2017',
	@endDate =  '12/31/2017',
	@Step = '',
	@debug = 1

exec dbo.usp_Arvada_DevReviewClearpoint
	@status = null,
	@ReportType = 'Summary',
	@startdate = null,
	@endDate = null

exec dbo.usp_Arvada_DevReviewClearpoint2 
	@status = null,
	@ReportType = 'Detail',
	@startdate = '03/01/2017',
	@endDate =  '03/01/2017'

exec dbo.usp_Arvada_DevReviewClearpoint 
	@status = 'Missed',
	@ReportType = 'Detail',
	@startdate = '06/01/2017',
	@endDate =  '06/01/2017',
	@Step = ''

exec dbo.usp_Arvada_DevReviewClearpoint 
	@status = null,
	@ReportType = 'ErrorSummary',
	@startdate = null,
	@endDate = null,
	@ProjectStatus = '1ST REVIEW|2ND REVIEW|DECISION REVIEW|COMPLETENESS REVIEW|UNDER REVIEW|RESPONSE PEND|APPLIED|ON HOLD'

*/

--Track Start Chronology event in the month for Completeness Check
--standard track (calc = event scheduled, days between 1st review complete and PAymentDue, 2nd review and Customer 1st resub, Decision and Customer 2nd resub)
--1st review starts when (14 days prior to 1st review complete due)?  2nd review (28 days prior to 2nd review complete due)?  Decision Review (14 days prior to Decision Review Complate due)?

if @startdate = @endDate	
	set @endDate = dateadd(month,1,@startdate)

if @ReportType is null
	set @ReportType = 'Summary'

if @status = ''
	set @status = null

if @Step = ''
	set @Step = null

create table #status
(
	statusValue nvarchar(50)
)

create table #dimension
(
	startdate datetime,
	StepValue varchar(500),
	OrderBy int
)

create table #final
(
	project_no nvarchar(50),
	ProjectStatus nvarchar(50),
	ProjectDesc nvarchar(200),
	ActionName nvarchar(230),
	ActionDate datetime,
	CompletedDate datetime,
	ActionOrder int,
	ProjectTrack nvarchar(50),
	maxVal int,
	StartDate datetime,
	StepStatus varchar(50),
	ReviewGroupExists bit default 0,
	ChronologyExists bit default 0 ,
	RptActionName varchar(100), 
	StartDateMMYY as DATEADD(MONTH, DATEDIFF(MONTH, 0, StartDate), 0),
	ReviewsCompletedBy datetime,
	planner nvarchar(4000),
	udfFields varchar(max),
	ActionDateMMYY as DATEADD(MONTH, DATEDIFF(MONTH, 0, ActionDate), 0)
)

create table #timing
(
	Hearing_Type varchar(60),
	Hearing_Action varchar(30),
	No_Days int,
	OrderBy int,
	DaysBetween int
)

create table #reviews
(
	project_no varchar(60),
	ReviewType varchar(200),
	date_due datetime,
	date_received datetime,
	contact nvarchar(500),
	reviewstatus varchar(500),
	reviewgroup varchar(100),
	AssociatedChronologyEvent varchar(100)
)


--this gets the expected days between events, so we can calculate when 1st review started since we are tracking 1st review complete in the chronology we just work backwards from it
insert into #timing
(
	Hearing_Type ,
	Hearing_Action ,
	No_Days ,
	OrderBy 
)
select
	Hearing_Type ,
	Hearing_Action ,
	No_Days,
	row_number() over (partition by hearing_type order by no_days) as rid
from dbo.Prmry_TimeClock t
WHERE t.GROUPNAME = 'PROJECT' 
and t.Hearing_Type in ('STANDARD TRACK', 'EXTENDED TRACK','Administrative Track','Conditional Approval') 

--days between the steps
update t
set DaysBetween = isnull(t.No_Days - t2.No_Days,0)
from #timing t
left join #timing t2
	on t.Hearing_Type = t2.Hearing_Type
	and t.OrderBy = t2.orderby+1

--get all the steps in their start date order (we cannot rely on users putting them in all the time so we need to account for that in error tracking)
insert into #dimension
(
	startdate,
	StepValue,
	OrderBy
)
select
	dateadd(month,a.Number-1, DATEADD(MONTH, DATEDIFF(MONTH, 0, @startdate), 0) ),
	StepValue,
	OB
from dbo.ArvadaNumbers a
cross join (select 'Completeness Check' as StepValue, 1 as OB
			union all select 'First Review', 2 as OB
			union all select 'Second Review', 3 as OB
			union all select 'Decision Review', 4 as OB
			union all select 'Conditional Approval', 5 as OB
			) as b
where
	a.Number <= DATEDIFF(month, @startdate, @endDate)+1

--used in error tracking report, since we only care about certain statuses that define an open project
if @ProjectStatus = 'Open'
begin
	insert into #status
	(
		statusValue
	)
	exec [dbo].[usp_Arvada_GetDevReviewStatuses]
end



insert into #final
(
	project_no,
	projectstatus,
	ProjectDesc,
	projectTrack,
	ActionName,
	ActionOrder  ,
	planner ,
	udffields
)
select
	m.PROJECT_NO,
	m.[STATUS],
	m.PROJECT_NAME,
	case 
		when u.trk_type = 'Extended' then 'EXTENDED TRACK'
		when u.trk_type = 'Administrative' then 'Administrative Track'
		when u.trk_type = 'Standard' then 'STANDARD TRACK'
		ELSE 'NO TRACK DEFINED'
	END as projTrack,
	b.Hearing_Action,
	b.rid,
	m.PLANNER,
	(case when nullif(u.DA_TYPE_1,'') is not null then u.DA_TYPE_1 else '' end +
				case when nullif(u.DA_TYPE_2,'') is not null then ', ' + u.DA_TYPE_2 else '' end +
				case when nullif(u.DA_TYPE_3,'') is not null then ', ' + u.DA_TYPE_3 else '' end +
				case when nullif(u.DA_TYPE_4,'') is not null then ', ' + u.DA_TYPE_4 else '' end +
				case when nullif(u.DA_TYPE_5,'') is not null then ', ' + u.DA_TYPE_5 else '' end +
				case when nullif(u.DA_TYPE_6,'') is not null then ', ' + u.DA_TYPE_6 else '' end 						
				)
from dbo.project_main m
left join dbo.project_udf u
	on m.PROJECT_NO = u.PROJECT_NO
left join 
	(
		Select 
			Hearing_Type,
			Hearing_Action ,
			OrderBy as rid,
			DaysBetween
		from #timing t 
		where Hearing_Type != 'Conditional Approval'
	) as b
	on b.Hearing_Type = case 
							when u.trk_type = 'Extended' then 'EXTENDED TRACK'
							when u.trk_type = 'Administrative' then 'Administrative Track'
							when u.trk_type = 'Standard' then 'STANDARD TRACK'
							ELSE 'NO TRACK DEFINED'
						END 
WHERE 
   (m.[STATUS] in (select statusValue from #status) or @ProjectStatus is null) --('COMPLETENESS REVIEW', 'UNDER REVIEW', 'RESPONSE PEND',  'APPLIED','ON HOLD'))
and  nullif(m.PARENT_PROJECT_NO,'') is null  --sub projects not included in dev. review
and m.PROJECTTYPE not in ('PREAPPLICATION','OTHER','MINOR MODIFICATION')
and (m.PROJECT_NO like 'da%' --for now, to cut down on clutter during development
		--or m.project_no = 'CRW-PROJECT'
		)
union all

select
	m.PROJECT_NO,
	m.[STATUS],
	m.PROJECT_NAME,
	case 
		when u.trk_type = 'Extended' then 'EXTENDED TRACK'
		when u.trk_type = 'Administrative' then 'Administrative Track'
		when u.trk_type = 'Standard' then 'STANDARD TRACK'
		ELSE 'NO TRACK DEFINED'
	END as projTrack,
	b.Hearing_Action,
	b.rid,
	m.PLANNER,
	(case when nullif(u.DA_TYPE_1,'') is not null then u.DA_TYPE_1 else '' end +
				case when nullif(u.DA_TYPE_2,'') is not null then ', ' + u.DA_TYPE_2 else '' end +
				case when nullif(u.DA_TYPE_3,'') is not null then ', ' + u.DA_TYPE_3 else '' end +
				case when nullif(u.DA_TYPE_4,'') is not null then ', ' + u.DA_TYPE_4 else '' end +
				case when nullif(u.DA_TYPE_5,'') is not null then ', ' + u.DA_TYPE_5 else '' end +
				case when nullif(u.DA_TYPE_6,'') is not null then ', ' + u.DA_TYPE_6 else '' end 						
				)
from dbo.project_main m
left join dbo.project_udf u
	on m.PROJECT_NO = u.PROJECT_NO
cross join 
	(
		Select 
			Hearing_Type,
			Hearing_Action ,
			OrderBy as rid,
			DaysBetween
		from #timing t 
		where Hearing_Type = 'Conditional Approval'
	) as b
WHERE 
   (m.[STATUS] in (select statusValue from #status) or @ProjectStatus is null) --('COMPLETENESS REVIEW', 'UNDER REVIEW', 'RESPONSE PEND',  'APPLIED','ON HOLD'))
and  nullif(m.PARENT_PROJECT_NO,'') is null  --sub projects not included in dev. review
and m.PROJECTTYPE not in ('PREAPPLICATION','OTHER','MINOR MODIFICATION')
and ( m.PROJECT_NO like 'da%' --for now, to cut down on clutter during development
		--or m.project_no = 'CRW-PROJECT'
				)
and exists(
			Select 1 
			from dbo.Project_Actions a  
			where a.ACTION_TYPE in ('Manager Review')
				and a.PROJECT_NO = m.PROJECT_NO
			)


update f
set 
	udfFields = ltrim(rtrim(case when substring(udfFields,1,1) = ',' then substring(udffields,2,len(udffields)) else udffields end))
from #final f


--when did each step we are expecting, was due (action date), completed, and start?
update f
set 
	ActionDate = a.ACTION_DATE,
	CompletedDate = a.COMPLETED_DATE,
	StartDate = dateadd(day, -1*t.DaysBetween, a.ACTION_DATE) --find when the action starts as opposed to when it is expected to be completed
from #final f
inner join dbo.Project_Actions a
	on a.PROJECT_NO = f.Project_No
	and a.ACTION_TYPE = f.ActionName
inner join #timing t
	on t.Hearing_Type = f.projectTrack
	and t.Hearing_Action = f.ActionName

update f
set 
	ActionDate = a.ACTION_DATE,
	CompletedDate = a.COMPLETED_DATE,
	StartDate = dateadd(day, -1*t.DaysBetween, a.ACTION_DATE) --find when the action starts as opposed to when it is expected to be completed
from #final f
inner join dbo.Project_Actions a
	on a.PROJECT_NO = f.Project_No
	and a.ACTION_TYPE = f.ActionName
inner join #timing t
	on t.Hearing_Type = 'Conditional Approval'
	and t.Hearing_Action = f.ActionName
where 
	f.actiondate is null

--used for error checking
update f
set ChronologyExists = 1
from #final f
where exists(select 1 from #final f2 where f.project_no = f2.project_no and f2.ActionDate is not null)

--get reviews so we can do some analysis
insert into	#reviews
(
	project_no ,
	ReviewType ,
	date_due ,
	date_received ,
	contact ,
	reviewstatus ,
	reviewgroup  ,
	AssociatedChronologyEvent
)
select
	r.PROJECT_NO,
	r.REVIEWTYPE,
	r.DATE_DUE,
	r.DATE_RECEIVED,
	r.CONTACT,
	r.STATUS,
	r.REVIEWGROUP,
	case when REVIEWGROUP = '1st Extended Review' then '1st Review complete'
		 when REVIEWGROUP = '1st Standard' then '1st Review Complete'
		 when REVIEWGROUP = '2nd Extended Review' then '2nd Review Complete'
		 when REVIEWGROUP = '2nd Standard Review' then '2nd Review Complete'
		 when REVIEWGROUP = 'Completeness Check' then 'Acceptance Letter'
		 when REVIEWGROUP = 'Decision Review' and f.ProjectTrack = 'Extended Track' then 'Decision Review'
		 when REVIEWGROUP = 'Decision Review'  then 'Decision Review Complete'
		 when REVIEWGROUP = 'Conditional Approval'  then 'Conditional Approval'
		 else ''
	end
from dbo.project_reviews r
inner join (select distinct project_no, projectTrack from #final) f
	on f.project_no = r.PROJECT_NO
where
	r.REVIEWGROUP in ('1st Extended Review', '1st Standard' , '2nd Extended Review' , '2nd Standard Review', 'Completeness Check', 'Decision Review','Conditional Approval' )
and isnull(R.REMARKS,'') not like 'VOIDED%'

--used for error checking
--we dont care about reviews existing for chronology events that arent review related
update f
set ReviewGroupExists = 1
from #final f
where ActionName not in ('Acceptance Letter','1st Review Complete','2nd Review Complete','Decision Review Complete','Decision Review','Staff Review Due')

--we have time to add reviews yet
update f
set ReviewGroupExists = 1
from #final f
where ActionName in ('Acceptance Letter','1st Review Complete','2nd Review Complete','Decision Review Complete','Decision Review','Staff Review Due')
and startdate > getdate()


update f
set ReviewGroupExists = 1
from #final f
where exists(select 1 from #reviews f2 where f.project_no = f2.project_no and f2.AssociatedChronologyEvent = f.ActionName)

update f
set ReviewsCompletedBy = p.DATE_RECEIVED
from #final f
inner join 
	(
		select
			project_no,
			AssociatedChronologyEvent,
			max(DATE_RECEIVED) as DATE_RECEIVED
		from #reviews
		group by 
			project_no,
			AssociatedChronologyEvent
	) as p
	on p.project_no = f.project_no
	and p.AssociatedChronologyEvent = f.ActionName

--if we have 1 rejected review for the step, the step status is rejected
update f
set StepStatus = 'Rejected'
from #final f
inner join #reviews r
	on r.project_no = f.project_no
	and r.AssociatedChronologyEvent = f.ActionName
	and r.reviewstatus = 'REJECTED'

update f
set StepStatus = 'Pending'
from #final f
where
	f.ActionDate >= convert(date, getdate()) --the due date is in the future
and StepStatus is null --we haven't marked as rejected

--the chronology event was completed in time but there were late reviews
update f
set StepStatus = 'Completed Late Review'
from #final f
where
	f.CompletedDate <= f.ActionDate
and StepStatus is null
and exists(select 1
			from #reviews r
			where 
				r.project_no = f.project_no
				and r.AssociatedChronologyEvent = f.ActionName
				and  r.DATE_RECEIVED  > r.date_due
			)


update f
set StepStatus = 'Completed'
from #final f
where
	f.CompletedDate <= f.ActionDate
and StepStatus is null



update f
set StepStatus = 'Missed'
from #final f
where
	(f.CompletedDate > f.ActionDate or f.CompletedDate is null)
and StepStatus is null

update f
set RptActionName =
	case actionName	
		when 'Acceptance Letter' then 'Completeness Check'
		when '1st Review Complete' then 'First Review'
		when '2nd Review Complete' then 'Second Review'
		when  'Decision Review Complete' then 'Decision Review'
		when  'Decision Review' then 'Decision Review'
		when  'Staff Review Due'  then 'Conditional Approval'
	else null end
from #final f


if @debug = 1 and @debug is not null
begin
	select * 
	from #final
	where project_no = 'DA2017-0106'

	select * 
	from #reviews
	where project_no = 'DA2017-0106'

end

 

if @ReportType = 'Summary'
begin

	select
		d.StepValue,
		d.StartDate,
		d.OrderBy,
		isnull(b.completed,0) as completed,
		isnull(b.missed,0) as missed,
		isnull(b.rejected,0) as rejected,
		isnull(b.pending, 0) as pending,
		isnull(b.CompletedLateReview,0) as CompletedLateReview
	from #dimension d
	left join 
	(
		 select
			f.ActionDateMMYY,
			f.RptActionName,
			sum(Case when StepStatus = 'Completed' then 1 else 0 end) as Completed,
			sum(Case when StepStatus = 'Missed' then 1 else 0 end) as Missed,
			sum(Case when StepStatus = 'Rejected' then 1 else 0 end) as Rejected,
			sum(Case when StepStatus = 'Pending' then 1 else 0 end) as Pending,
			sum(case when stepstatus = 'Completed Late Review' then 1 else 0 end) as CompletedLateReview
		 from #final f
		where
				 f.ProjectTrack != 'NO TRACK DEFINED'
			and f.startdate is not null --we could calculate a start date which means chronology exists
			and f.ActionName in ('Acceptance Letter','1st Review Complete','2nd Review Complete','Decision Review Complete','Decision Review','Staff Review Due')
		group by
			f.ActionDateMMYY,
			f.RptActionName
	) as b 
		on b.ActionDateMMYY = d.startdate
		and b.RptActionName = d.StepValue 
	order by  
		d.StartDate,
		d.OrderBy 
end
else if @ReportType = 'Detail'
begin

	select
		project_no,
		ProjectStatus,
		ProjectDesc ,
		ActionName ,
		ActionDate,
		CompletedDate ,
		ActionOrder ,
		ProjectTrack ,
		maxVal ,
		StartDate ,
		StepStatus ,
		ReviewGroupExists ,
		ChronologyExists ,
		RptActionName ,
		StartDateMMYY ,
		ReviewsCompletedBy ,
		datediff(day,ReviewsCompletedBy, CompletedDate) as DaysBetweenReviewSent,
		udfFields
	from #final f
	where
				f.ProjectTrack != 'NO TRACK DEFINED' 
		and ( (isnull(@Step,'All') = 'All' and f.ActionName in ('Acceptance Letter','1st Review Complete','2nd Review Complete','Decision Review Complete','Decision Review','Staff Review Due'))
			 or f.RptActionName = @Step)
		and f.ActionDateMMYY >= @startDate 
		and f.ActionDateMMYY < @endDate 
		and (@status is null
				or StepStatus = @status)
	order by 
		project_no,
		ActionOrder
end
else if @ReportType = 'ErrorSummary'
begin

	
	select 
		'Open Projects in unknown track' as Error, count(distinct project_no) as CountValue, 1 as OB
	from #final f
	where f.projectTrack = 'NO TRACK DEFINED' 

	union all

	select 
		'Open Projects with Track but no Chronology' as Error, count(distinct project_no) as CountValue, 2 as OB
	from #final f
	where f.ChronologyExists = 0
	and f.projectTrack != 'NO TRACK DEFINED' 

	union all

	select 
		'Open Projects with Track and Chronology but no reviews' as Error, count(distinct project_no) as CountValue, 3 as OB
	from #final f
	where f.ReviewGroupExists = 0
	and f.projectTrack != 'NO TRACK DEFINED' 
	and f.ChronologyExists = 1
	order by 
		OB

end
else if @ReportType Like 'ErrorDetail%'
begin

	select
		Error,
		project_no,
		ProjectDesc,
		planner
	from 
	(
		select distinct
			'Open Projects in unknown track' as Error,
			f.project_no,
			f.ProjectDesc,
			f.planner,
			1 as ErrorTypeVal
		from #final f
		where f.projectTrack = 'NO TRACK DEFINED' 

		union all

		select distinct
			'Open Projects with Track but no Chronology' as Error, 
			f.project_no,
			f.ProjectDesc,
			f.planner,
			2 as ErrorTypeVal
		from #final f
		where f.ChronologyExists = 0
		and f.projectTrack != 'NO TRACK DEFINED' 

		union all

		select distinct
			'Open Projects with Track and Chronology but no reviews' as Error,
			f.project_no,
			f.ProjectDesc,
			f.planner,
			3 as ErrorTypeVal
		from #final f
		where f.ReviewGroupExists = 0
		and f.projectTrack != 'NO TRACK DEFINED' 
		and f.ChronologyExists = 1
	) as b
	where
		ErrorTypeVal = right(@ReportType,1)
	order by 
		project_no
end


--select * 
--from #final
--order by 
--	project_no,
--	ActionOrder


end
GO
