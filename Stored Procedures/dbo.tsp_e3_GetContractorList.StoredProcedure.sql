USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_e3_GetContractorList]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_e3_GetContractorList]
GO
/****** Object:  StoredProcedure [dbo].[tsp_e3_GetContractorList]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE  [dbo].[tsp_e3_GetContractorList]
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @SQL AS VARCHAR(max) 
	DECLARE @InspFilter AS VARCHAR(max) 

	SET @InspFilter = (SELECT TOP 1 pref_value 
					   FROM   etrakit3_preferences 
					   WHERE  pref_name = 'Insp_SqlNameList') 

	SET @SQL = 'SELECT st_lic_no, company FROM aec_main ' + @InspFilter + ' order by company'

EXEC(@SQL)  
END
GO
