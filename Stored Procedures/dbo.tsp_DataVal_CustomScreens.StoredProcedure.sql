USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_DataVal_CustomScreens]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_DataVal_CustomScreens]
GO
/****** Object:  StoredProcedure [dbo].[tsp_DataVal_CustomScreens]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  -- =============================================
-- Author:		Erick Hampton
-- Create date: Oct 29, 2014
-- Description:	Test validation rules against Custom Screens data
-- Inputs:		One parameter to identify how to return the data
-- Output:		Table of 2 columns: (1) Validation Rule, (2) Rule Passed (bit); Optionally returns NULL if all rules passed
-- Returns:		Count of failed rules
-- =============================================
CREATE PROCEDURE [dbo].[tsp_DataVal_CustomScreens]
	
	@ReturnNullIfPasses bit = true 

AS
BEGIN
	SET NOCOUNT ON;

	-- counter
	declare @RulesFailed int
	set @RulesFailed = 0 

	-- temp int for comparing result counts
	declare @TempInt int
	set @TempInt=0

	-- temp table for returning pass / fail results

	--RB Check if table exists before creating it
   IF OBJECT_ID('tempdb..#Results') IS NOT NULL  
	drop table #Results

	create table #Results ([Rule] varchar(250), [Passed] bit)

	-- RULE 1: ONLY 1 UDF='TITL' PER SCREEN
	set @TempInt = isnull((
		select top 1 count(UDF) [TITLs] 
		from Prmry_TypesUDF 
		where udf='TITL' 
		group by groupname, TYPENAME, Screen
		order by count(UDF) desc), 0)
	
	insert into #Results values('ONLY 1 UDF="TITL" PER SCREEN', case @TempInt when 1 then 1 else 0 end) -- true if max of UDF='TITL' count == 1 per group + type + screen
	if (@TempInt <> 1 ) set @RulesFailed = @RulesFailed + 1

	-- RULE 2: RECORDID IS NOT NULL
	set @TempInt = isnull((
		select count(*) 
		from Prmry_TypesUDF 
		where recordid is null), 0)
	
	insert into #Results values('RECORDID IS NOT NULL', case @TempInt when 0 then 1 else 0 end) -- true if count of nulls == 0
	if (@TempInt <> 0 ) set @RulesFailed = @RulesFailed + 1


	-- RULE 3: RECORDID IS UNIQUE
	set @TempInt = isnull((
		select top 1 count(recordid)
		from Prmry_TypesUDF 
		group by recordid
		order by count(recordid) desc), 0)
	
	insert into #Results values('RECORDID IS UNIQUE', case @TempInt when 1 then 1 else 0 end) -- true if max count of record == 1
	if (@TempInt <> 1 ) set @RulesFailed = @RulesFailed + 1


	-- ---------------------------------
	-- temp table used for rules 4 and 5
	-- ---------------------------------
	create table #udf (GroupName varchar(20), TypeName varchar(60), Screen int, AlphaUdfs int, NumericUdfs int)
	insert into #udf
	select	  GroupName
			, TypeName
			, Screen
			, sum(case when ISNUMERIC(udf) = 0 then 1 else 0 end) [AlphaUdfs]
			, sum(case when ISNUMERIC(udf) = 1 then 1 else 0 end) [NumericUdfs]
	from Prmry_TypesUDF 
	where udf <> 'TITL' 
	group by groupname, TYPENAME, Screen
	order by count(UDF) desc


	-- RULE 4: EXCEPT FOR 'TITL', UDF VALUES SHOULD ALL BE NUMERIC OR ALL VALUES SHOULD BE ALPHANUMERIC
	set @TempInt = isnull((
		select 
		sum(case when [AlphaUdfs] > 0 and [NumericUdfs] > 0 then 1 else 0 end) [MixedUdfTypes]
		from #udf), 0)
	
	insert into #Results values('EXCEPT FOR "TITL", UDF VALUES SHOULD ALL BE NUMERIC OR ALL VALUES SHOULD BE ALPHANUMERIC', case @TempInt when 0 then 1 else 0 end) -- true if there are no records with positive values in both alpha and numeric columns
	if (@TempInt <> 0 ) set @RulesFailed = @RulesFailed + 1
	

	-- RULE 5: MAX UDF COUNT PER SCREEN = 12 IF UDF IS ALPHANUMERIC, 10 IF UDF IS NUMERIC (NOT INCLUDING 'TITL' RECORD)
	set @TempInt = isnull((
		select count(*)
		from #udf
		where [AlphaUdfs] > 12 or [NumericUdfs] > 10
		), 0)
	
	insert into #Results values('MAX UDF COUNT PER SCREEN = 12 IF UDF IS ALPHANUMERIC, 10 IF UDF IS NUMERIC (NOT INCLUDING "TITL" RECORD)', case @TempInt when 0 then 1 else 0 end) -- true if there are no records where there are more than 12 alpha UDFs or more than 10 numeric UDFs
	if (@TempInt <> 0 ) set @RulesFailed = @RulesFailed + 1


	-- drop temp table
	drop table #udf


	-- RULE 6: MAX SCREEN COUNT PER GROUPNAME +TYPENAME <= 20
	set @TempInt = isnull((
		select top 1 count(*) 
		from Prmry_TypesUDF 
		group by groupname, TYPENAME, Screen
		having count(screen) > 20
		order by count(UDF) desc), 0)
	
	insert into #Results values('MAX SCREEN COUNT PER GROUPNAME +TYPENAME <= 20', case @TempInt when 0 then 1 else 0 end) -- true if the number of screens for a group + type having > 20 screens == 0
	if (@TempInt <> 0 ) set @RulesFailed = @RulesFailed + 1

	-- return results to user
	if @RulesFailed = 0 and @ReturnNullIfPasses = 1
	begin
		select null [Errors]
	end
	else
	begin
		select * from #Results
	end
	return @RulesFailed
END



GO
