USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_UDFAlterDB]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_UDFAlterDB]
GO
/****** Object:  StoredProcedure [dbo].[tsp_UDFAlterDB]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create PROCEDURE [dbo].[tsp_UDFAlterDB]
	@TableName varchar(250),
	@FieldName varchar(255),
	@FieldSize int,
	@FieldType varchar(255),
	@AlterType varchar(10)
AS
BEGIN
	Declare @SQL varchar(max)
	Declare @SQL2 nvarchar(max)

	Declare @NFieldSize nvarchar(10)
	--declare @DEF nvarchar(max) = '
	--@TableName varchar(250),
	--@FieldName varchar(255),
	--@NFieldSize nvarchar(10),
	--@FieldType varchar(255),
	--@output nvarchar(max) out'

	--declare @FromOutput nvarchar(max) = ''


	
	if @FieldSize = 0 And @FieldType = 'varchar' 
		Set @FieldSize = 60

		set @NFieldSize = cast(@FieldSize as nvarchar)
	If (@AlterType = 'ADD')
		Begin		
				
			if @FieldSize = 0
				begin
					SET @SQL = ' ALTER TABLE [' + @TableName + '] ADD [' + @FieldName + '] ' +  @FieldType + ' null'
				end
			else			
				begin
					SET @SQL = ' ALTER TABLE [' + @TableName + '] ADD [' + @FieldName + '] ' + @FieldType + ' (' + @NFieldSize + ') null'
					print @SQL

					--SET @SQL2 = 'Select @output = ''ALTER TABLE ['' + @TableName  + ''] ADD '' + '' @FieldName + '' '' + '' @FieldType ( + '' +  @NFieldSize + '' ) null'''
					--print @SQL2

					--exec (@SQL)

					--Alter Table @TableName Add @FieldName @FieldType(@FieldSize) null
					--execute @SQL2, @TableName, @FieldName, @FieldType, @FieldSize

					--execute sp_executesql @SQL2, @DEF, @TableName, @FieldName, @FieldType, @NFieldSize --, @output = @FromOutput out
					--print @FromOutput
					

				
					
				end
		End
	else if (@AlterType = 'MODIFY')
		Begin
			if @FieldSize is not null
			begin
				SET @SQL = ' ALTER TABLE [' + @TableName + '] ALTER COLUMN [' + @FieldName + '] ' + @FieldType + ' (' + @FieldSize + ') null'
			end
		else			
			begin
				SET @SQL = ' ALTER TABLE [' + @TableName + '] ALTER COLUMN [' + @FieldName + '] ' + @FieldType + ' null'
			end		
		End
	Else IF (@AlterType = 'REMOVE')
		Begin 
			SET  @SQL = ' ALTER TABLE [' + @TableName + '] DROP COLUMN [' + @FieldName  + ']'
		End
	Else
		begin
			SET  @SQL = ''
		end
	
	
	exec (@SQL)
	



	
END
GO
