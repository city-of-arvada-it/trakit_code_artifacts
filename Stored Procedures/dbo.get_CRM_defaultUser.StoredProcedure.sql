USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[get_CRM_defaultUser]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[get_CRM_defaultUser]
GO
/****** Object:  StoredProcedure [dbo].[get_CRM_defaultUser]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<MA>
-- Create date: <11/22/16>
-- Description:	<GetDefaultAssignedToUser for CRM>
-- =============================================
CREATE PROCEDURE [dbo].[get_CRM_defaultUser] 
	-- Add the parameters for the stored procedure here
	@list_id int,
	@Resultado	varchar(20) output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	Declare @userIdado varchar(20) = ''
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	  Set @userIdado = (select CASE WHEN left(action_to_array, charindex(',',action_to_array,0)) = '' THEN action_to_array ELSE left(action_to_array, charindex(',',action_to_array,0)-1) END from crm_actions 
	  where NATURETYPE_LIST_ID = @list_id
	  and STATUS_LIST_ID = (select list_id from crm_lists where list_category = '5' and sequence = '1') 
	  and ASSIGN_TO_USER = '1'
	  and ACTION_TO = '1' )

	  Set @Resultado = @userIdado
END

GO
