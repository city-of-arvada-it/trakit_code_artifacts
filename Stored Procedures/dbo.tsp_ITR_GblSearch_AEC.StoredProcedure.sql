USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ITR_GblSearch_AEC]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_ITR_GblSearch_AEC]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ITR_GblSearch_AEC]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  
					  					  					  

-- =====================================================================
-- Revision History:
--	RR - 10/28/13
--	mdm - 11/5/13 - added revision history	
-- =====================================================================  
CREATE PROCEDURE [dbo].[tsp_ITR_GblSearch_AEC]
	-- Add the parameters for the stored procedure here
	--@searchfield varchar(20) = null,
	@searchString Varchar(500) = null,
	@Top Varchar(100) = null
	
AS
BEGIN

	declare @statement varchar(500)
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

Set @searchString = REPLACE(@searchString,'''','''''')

    -- Insert statements for procedure here
	--set @statement = 'SELECT top 1000 * from tvw_GblActivities where ' +
	--@searchfield + ' like ''%' + @searchString + '%''' +
	--' and tgroup = ''AEC'''
	
	if @top is null or @top = 0 set @top = '' else set @top = 'TOP ' + @top
	
	set @statement = 'SELECT ' + @top + ' ST_LIC_NO,COMPANY,ADDRESS1,BUS_LIC_1_NO,BUS_LIC_2_NO,OWNER_NAME,EMAIL,PHONE_1,AECTYPE from AEC_MAIN where
(company like ''%' + @searchString + '%'' or ST_LIC_NO like ''%' + @searchString + '%'' or ADDRESS1 like ''%' + @searchString + '%'' or BUS_LIC_1_NO like ''%' + @searchString + '%'' or BUS_LIC_2_NO like ''%' + @searchString + '%'' or OWNER_NAME like ''%' + @searchString + '%'' or EMAIL like ''%' + 
@searchString + '%'' or PHONE_1 like ''%' + @searchString + '%'')'
	
	execute(@statement)
	
	print @statement
	   
	
END













GO
