USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[usp_arvada_ProjectistingReport]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[usp_arvada_ProjectistingReport]
GO
/****** Object:  StoredProcedure [dbo].[usp_arvada_ProjectistingReport]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_arvada_ProjectistingReport]

	@ReportType varchar(50)

as

begin

/*
exec [dbo].[usp_arvada_ProjectistingReport] @ReportType = 'GIS'
exec [dbo].[usp_arvada_ProjectistingReport] @ReportType = 'Residential'
exec [dbo].[usp_arvada_ProjectistingReport] @ReportType = 'Non-Residential'

select * 
from dbo.project_udf
where project_no = 'DG2017-0022'





select project_no , projecttype,PARENT_PROJECT_NO,status
from project_main
where project_no in ('da2017-0021') 


*/
 
set nocount on

if @ReportType is null
	set @ReportType = 'ALL'

select 
	m.project_no,
	m.project_name,
	m.PROJECTSUBTYPE,
	m.site_addr,
	p.[NAME] as Applicant,
	p.phone as ApplicantPhone,
	--isnull(g.ZONE_CODE2, g2.zoning) as ExistingZoning,
	isnull(u.PD_PROP_ZONE, m.zoning) as ProposedZoning,
	u.PD_ACRES as Acres,
	u.PD_SF_ATT,
	u.PD_MF_UNITS,
	u.PD_SF_DET,
	case when m.[STATUS] = 'UNDER CONST' then 'Under Construction'
			else 'Under Review' end as ProjectStatus,
	case 
		when u.PD_SF_ATT is not null and u.PD_SF_ATT != 0 then 'Single Family Attached'
		when u.PD_SF_ATT is not null and u.PD_MF_UNITS != 0 then 'Multi-Family'
		when u.PD_SF_ATT is not null and u.PD_SF_DET != 0 then 'Single Family Detached'			
		else  u.PD_LANDUSE end
		as TypeOfUnits,
	case when m.[STATUS] = 'UNDER CONST' then 'Under Construction'
			else 'Under Review' end as  [STATUS],
	case 
		when m.PROJECTSUBTYPE  in ('SINGLE FAMILY', 'MULTI FAMILY','MULTI-FAMILY','SINGLE-FAMILY','RESIDENTIAL') then 'RESIDENTIAL PROPERTY'
		when m.PROJECTSUBTYPE != '' and m.PROJECTSUBTYPE is not null then 'NON-RESIDENTIAL PROPERTY'	
		ELSE 'UNKNOWN PROJECT SUB TYPE'
		END as SubTypeDescription,
	'https://arvadapermits.org/etrakit3/Search/project.aspx?activityno='+m.project_no as ProjectURL,
	nullif(isnull(try_cast(PD_SF_DET as float),0) + 
		isnull(try_Cast(PD_SF_ATT as float),0) + 
		 isnull(try_cast(PD_MF_UNITS as float),0),0) as Units,
	m.PLANNER
from dbo.project_main m
left join  dbo.project_people p
  on p.PROJECT_NO = m.PROJECT_NO
	and p.nametype = 'APPLICANT'
left join dbo.project_udf u
	on u.PROJECT_NO = m.PROJECT_NO
left join dbo.geo_ownership g
	on g.recordid = m.LOC_RECORDID
left join dbo.geo_udf g2
	on g.recordid = g2.LOC_RECORDID
where
	m.[status] in (
					'2ND REVIEW',
					'1ST REVIEW',
					'CONDITIONAL APPROVAL',
					'CUSTOMER RESP. PEND',
					'DECISION REVIEW',
					'RESPONSE PEND',
					'UNDER CONST',
					'UNDER REVIEW',
					'CONDITIONAL APPROVAL',
					'PH PENDING',
					'3RD REVIEW',
					'2ND CUSTOMER RESP',
					'FULL CUSTOMER RESP'
					)
and		
	nullif(PARENT_PROJECT_NO,'') is null
and 
	projecttype not in ('referral', 'telecommunications')
and 
	Prefix not in ('MM','VAR','VAC')
and
	(@ReportType = 'ALL'
		or (@ReportType='Residential' and m.PROJECTSUBTYPE in ('SINGLE FAMILY', 'MULTI FAMILY','MULTI-FAMILY','SINGLE-FAMILY','RESIDENTIAL'))
		or (@ReportType='Non-Residential' and m.PROJECTSUBTYPE  not in ('SINGLE FAMILY', 'MULTI FAMILY','MULTI-FAMILY','SINGLE-FAMILY','RESIDENTIAL') and  m.PROJECTSUBTYPE is not null and  m.PROJECTSUBTYPE != '')
	    or (@ReportType='GIS' and  m.PROJECTSUBTYPE is not null and  m.PROJECTSUBTYPE != '')
	 )
 --and project_no = 'da2017-0021'
order by 
	SubTypeDescription,
	project_no

 
 end 
GO
