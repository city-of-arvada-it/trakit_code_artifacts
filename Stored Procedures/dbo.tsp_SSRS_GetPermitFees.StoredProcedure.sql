USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_SSRS_GetPermitFees]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_SSRS_GetPermitFees]
GO
/****** Object:  StoredProcedure [dbo].[tsp_SSRS_GetPermitFees]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  					  
					  					  					  
CREATE PROCEDURE [dbo].[tsp_SSRS_GetPermitFees]
	@PERMIT_NO VARCHAR(30)
		
AS
BEGIN


--RB Check if table exists before creating it
   IF OBJECT_ID('tempdb..#table1') IS NOT NULL  
	drop table #table1

create table #table1
	(RECORD_NO varchar(20),
	RECORD_TYPE varchar(20),
	CODE varchar(30),
	ITEM varchar(30),
	PARENT_DESCRIPTION varchar(100),
	DESCRIPTION varchar(100),
	ACCOUNT varchar(30),
	QUANTITY float,
	AMOUNT float,
	PAID_AMOUNT float,
	PAID_DATE date,
	RECEIPT_NO varchar(30),
	CHECK_NO varchar(30),
	PAY_METHOD varchar(30),
	PAID_BY varchar(100),
	COLLECTED_BY varchar(100),
	FEETYPE varchar(10),
	DEPOSITID varchar(30),
	SITE_ADDR varchar(100),
	SITE_APN varchar (100)) ;

create index #table1_ix1 on #table1 (RECORD_NO);

--TESTING ONLY---------------------------------------------------------
--Truncate Table #table1
--DECLARE @RECEIPT_NO varchar(30)
--SET @RECEIPT_NO = '1029415-01'
--Select * from #Table1 order by RECORD_NO, CODE, ITEM
--TESTING ONLY---------------------------------------------------------


--PERMITS--------------------------------------------------------------------------------------------------------------------------------------------
BEGIN
	insert into #table1 (RECORD_NO, RECORD_TYPE, CODE, ITEM, DESCRIPTION, ACCOUNT, QUANTITY, AMOUNT, PAID_AMOUNT, PAID_DATE, RECEIPT_NO, CHECK_NO, SITE_ADDR, SITE_APN, PAY_METHOD, COLLECTED_BY, FEETYPE, DEPOSITID, PAID_BY)
	select Permit_Fees.PERMIT_NO, 'PermitTRAK', Permit_Fees.CODE, NULL, Permit_Fees.DESCRIPTION, Permit_Fees.ACCOUNT, Permit_Fees.QUANTITY, Permit_Fees.AMOUNT, Permit_Fees.PAID_AMOUNT, 
		Permit_Fees.PAID_DATE, Permit_Fees.RECEIPT_NO, Permit_Fees.CHECK_NO, Permit_Main.SITE_ADDR, Permit_Main.SITE_APN,
		Permit_Fees.PAY_METHOD, Permit_Fees.COLLECTED_BY, Permit_Fees.FEETYPE, Permit_Fees.DEPOSITID, Permit_Fees.PAID_BY
	from Permit_Fees inner join Permit_Main on Permit_Main.PERMIT_NO = Permit_Fees.PERMIT_NO
	where DETAIL = 0 and Permit_Main.PERMIT_NO = @PERMIT_NO and AMOUNT > 0
	UNION ALL
	select Permit_SubFees.PERMIT_NO, 'PermitTRAK', Permit_SubFees.CODE, Permit_SubFees.ITEM, Permit_SubFees.DESCRIPTION, Permit_SubFees.ACCOUNT, Permit_SubFees.QUANTITY, Permit_SubFees.AMOUNT, Permit_Fees.PAID_AMOUNT, 
		Permit_Fees.PAID_DATE as PAID_DATE, Permit_Fees.RECEIPT_NO as RECEIPT_NO, Permit_Fees.CHECK_NO as CHECK_NO, Permit_Main.SITE_ADDR, Permit_Main.SITE_APN,
		Permit_Fees.PAY_METHOD, Permit_Fees.COLLECTED_BY, Permit_Fees.FEETYPE, Permit_Fees.DEPOSITID, Permit_Fees.PAID_BY
	from Permit_SubFees inner join Permit_Fees on Permit_Fees.RECORDID = Permit_SubFees.PARENTID
	inner join Permit_Main on Permit_Main.PERMIT_NO = Permit_SubFees.PERMIT_NO
	where PARENTID in (Select RECORDID from Permit_Fees where DETAIL = 1 and Permit_Fees.PERMIT_NO = @PERMIT_NO)
	and Permit_SubFees.AMOUNT > 0 ;
END



--Offset any Permit Deposit Reductions	
insert into #table1
	( RECORD_NO, RECORD_TYPE, ACCOUNT, CODE, AMOUNT, PAID_AMOUNT, PAID_DATE, RECEIPT_NO, CHECK_NO, PAY_METHOD, FEETYPE, DEPOSITID, COLLECTED_BY)
select
	  RECORD_NO, RECORD_TYPE, (select ACCOUNT from Permit_Fees where Permit_Fees.RECORDID = #table1.DEPOSITID), CODE, (AMOUNT*-1), (PAID_AMOUNT*-1),  PAID_DATE, 'OFFSET', CHECK_NO, 'OFFSET', 'OFFSET', DEPOSITID, COLLECTED_BY
from #table1 
	where RECORD_TYPE = 'PermitTrak'
	and DEPOSITID in 
		(select RECORDID from Permit_Fees 
			where PERMIT_NO = #table1.RECORD_NO 
			and FEETYPE = 'DEPOSIT')
	and FEETYPE <> 'REFUND' ;
	

-- Permit Trak
update #table1 set PARENT_DESCRIPTION = 
	(select max(DESCRIPTION) from Permit_Fees where PERMIT_NO = #table1.RECORD_NO
		and PERMIT_NO = @PERMIT_NO 
		and CODE = #table1.CODE)
	where RECORD_TYPE = 'PermitTRAK' ;


--Update Credits and Refunds
update #table1 set #table1.AMOUNT = #table1.AMOUNT * -1 where (CODE = 'REFUND' or FEETYPE = 'REFUND') ;
update #table1 set #table1.AMOUNT = #table1.AMOUNT * -1 where (CODE = 'CREDIT' or ITEM = 'CREDIT') ;
update #table1 set #table1.PAID_AMOUNT = #table1.PAID_AMOUNT * -1 where (CODE = 'REFUND' or FEETYPE = 'REFUND') ;
update #table1 set #table1.PAID_AMOUNT = #table1.PAID_AMOUNT * -1 where (CODE = 'CREDIT' or ITEM = 'CREDIT') ;
update #table1 set #table1.ACCOUNT = '*NONE*' where (#table1.ACCOUNT is NULL or #table1.ACCOUNT = '') ;

--THIS SELECT NEEDS TO BE HERE FOR SSRS TO GET DATA------------------------------------------
select * from #table1 ;
---------------------------------------------------------------------------------------------
END





GO
