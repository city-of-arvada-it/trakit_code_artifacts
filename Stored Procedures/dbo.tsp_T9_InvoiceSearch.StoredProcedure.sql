USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_InvoiceSearch]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_InvoiceSearch]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_InvoiceSearch]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================
-- Author:		Rance Richardson
-- Create date: 2013-11-26
-- Description:	Used for Invoice Search
-- Revision History:
--		2013-12-10 - Added AEC and changed to handle search by activity number(RRR)
--		2014-01-11 - Added activity column (RRR)
--		2014-01-23 - Removed activity column (RRR)
--      2016-01-29 - Added @Filter2 param, filter by paid/unpaid invoices for Invoice Enhancement
--                   project. Also fixed orderby by invoice_date in #results (RRR)
-- ==========================================================================================
/* 
tsp_T9_InvoiceSearch @searchString='002', @Filter='invoice_no', @Top=10
tsp_T9_InvoiceSearch @searchString='2411', @Filter='address'
tsp_T9_InvoiceSearch @searchString='hall', @Filter='contact_name'
tsp_T9_InvoiceSearch @searchString='papp', @Filter='record_no'
tsp_T9_InvoiceSearch @searchString='INV', @Filter='invoice_no', @Filter2='unpaid'
*/

CREATE PROCEDURE [dbo].[tsp_T9_InvoiceSearch](	
	@searchString varchar(200),
	@Filter varchar(50),
	@Top int = Null,
	@Filter2 varchar(10) = 'unpaid'  --added by rrr to filter paid and unpaid

)
	
AS
BEGIN
	--SET NOCOUNT ON;
	DECLARE @SQL varchar(8000)
	DECLARE @WHERE varchar(2000)
	DECLARE @strTop varchar(50)
	DECLARE @InvNo as varchar(50) SET @InvNo='%'
	DECLARE @InvAddr as varchar(100) SET @InvAddr='%'
	DECLARE @InvContact as varchar(100) SET @InvContact='%'
	DECLARE @RecNo as varchar(50) SET @RecNo='%'
	DECLARE @PaidCondition as varchar(5) --added by rrr to filter paid and unpaid

	
	IF @Top Is Null 
	BEGIN
		SET @Top = 0
		SET @strTop = ' '
	END
	ELSE SET @strTop = ' TOP(' + CONVERT(varchar, @Top) + ') '
	IF LEFT(@searchString, 1) <> '%' AND RIGHT(@searchString, 1) <> '%'
		SET @searchString = '%' + @searchString + '%'
	SET @searchString = REPLACE(@searchString, '''', '''''')
	
	
	
	IF @Filter = 'invoice_no' SET @InvNo = @searchString 
	IF @Filter = 'address' SET @InvAddr = @searchString
	IF @Filter = 'contact_name' SET @InvContact = @searchString
	IF @Filter = 'record_no' SET @RecNo = @searchString

	IF @Filter2 = 'paid' SET @PaidCondition = '= 0' ELSE SET @PaidCondition = '> 0'   --added by rrr to filter paid and unpaid
	
CREATE TABLE #results(
	invoice_no varchar(200)
	,invoice_date varchar(10)
	,invoice_total money
	,invoice_paid money
	,invoice_unpaid money
	,invoice_address varchar(600)
	,invoice_contact varchar(300)
	,record_no varchar(50)
)
	
-- using Record Number filter	
SET @SQL = 'SELECT M.InvoiceNumber as invoice_no 
		,CONVERT(varchar(12), M.InvoiceDate, 1) as invoice_date
		,M.InvoiceTotal as invoice_total 
		,M.InvoicePaid as  invoice_paid 
		,ISNULL((select SUM(AMOUNT) from AEC_FEES where PAID_AMOUNT = 0 and (PAID_DATE IS NULL or PAID_DATE =  ' + CHAR(39) + '' + CHAR(39) + ') and invoice_no = AECF.INVOICE_NO), 0.00) as invoice_unpaid  
		,M.InvoiceAddress as invoice_address
		,M.InvoiceContact as invoice_contact'			
IF @Filter = 'record_no' SET @SQL = @SQL + ',AECF.ST_LIC_NO as record_no '
SET @SQL = @SQL + '
		INTO #results
	FROM Invoice_Main M 
		LEFT OUTER JOIN AEC_Fees AECF ON M.InvoiceNumber = AECF.INVOICE_NO 
	WHERE M.InvoiceNumber LIKE ''' + @InvNo + ''' 
		AND M.InvoiceAddress LIKE ''' + @InvAddr + '''
		AND M.InvoiceContact LIKE ''' + @InvContact + '''
		AND AECF.ST_LIC_NO LIKE ''' + @RecNo + '''
		AND M.InvoiceTotal - M.InvoicePaid ' + @PaidCondition + '
	UNION
	SELECT M.InvoiceNumber as invoice_no 
		,CONVERT(varchar(12), M.InvoiceDate, 1) as invoice_date
		,M.InvoiceTotal as invoice_total 
		,M.InvoicePaid as  invoice_paid 
		,ISNULL((select SUM(AMOUNT) from CASE_FEES where PAID_AMOUNT = 0 and (PAID_DATE IS NULL or PAID_DATE =  ' + CHAR(39) + '' + CHAR(39) + ') and invoice_no = CASF.INVOICE_NO), 0.00) as invoice_unpaid
		,M.InvoiceAddress as invoice_address
		,M.InvoiceContact as invoice_contact'
IF @Filter = 'record_no' SET @SQL = @SQL + ',CASF.CASE_NO as record_no '
SET @SQL = @SQL + '
	FROM Invoice_Main M 
		LEFT OUTER JOIN Case_Fees CASF ON M.InvoiceNumber = CASF.INVOICE_NO
	WHERE M.InvoiceNumber LIKE ''' + @InvNo + ''' 
		AND M.InvoiceAddress LIKE ''' + @InvAddr + '''
		AND M.InvoiceContact LIKE ''' + @InvContact + '''
		AND CASF.CASE_NO LIKE ''' + @RecNo + '''
		AND M.InvoiceTotal - M.InvoicePaid ' + @PaidCondition + '
	UNION
	SELECT M.InvoiceNumber as invoice_no
		,CONVERT(varchar(12), M.InvoiceDate, 1) as invoice_date
		,M.InvoiceTotal as invoice_total
		,M.InvoicePaid as  invoice_paid 
		,ISNULL((select SUM(AMOUNT) from LICENSE2_FEES where PAID_AMOUNT = 0 and (PAID_DATE IS NULL or PAID_DATE =  ' + CHAR(39) + '' + CHAR(39) + ') and invoice_no = LICF.INVOICE_NO), 0.00) as invoice_unpaid
		,M.InvoiceAddress as invoice_address
		,M.InvoiceContact as invoice_contact'
IF @Filter = 'record_no' SET @SQL = @SQL + ',LICF.LICENSE_NO as record_no '
SET @SQL = @SQL + '
		FROM Invoice_Main M 
		LEFT OUTER JOIN LICENSE2_FEES LICF ON M.InvoiceNumber = LICF.INVOICE_NO
	WHERE M.InvoiceNumber LIKE ''' + @InvNo +  '''
		 AND M.InvoiceAddress LIKE ''' + @InvAddr + '''
		 AND M.InvoiceContact LIKE ''' + @InvContact + '''
		 AND LICF.LICENSE_NO LIKE ''' + @RecNo + '''
		 AND M.InvoiceTotal - M.InvoicePaid ' + @PaidCondition + '
	UNION
	SELECT M.InvoiceNumber as invoice_no 
		,CONVERT(varchar(12), M.InvoiceDate, 1) as invoice_date
		,M.InvoiceTotal as invoice_total
		,M.InvoicePaid as  invoice_paid 
		,ISNULL((select SUM(AMOUNT) from PERMIT_FEES where PAID_AMOUNT = 0 and (PAID_DATE IS NULL or PAID_DATE =  ' + CHAR(39) + '' + CHAR(39) + ') and invoice_no = PMTF.INVOICE_NO), 0.00) as invoice_unpaid
		,M.InvoiceAddress as invoice_address
		,M.InvoiceContact as invoice_contact'
IF @Filter = 'record_no' SET @SQL = @SQL + ',PMTF.PERMIT_NO as record_no '
SET @SQL = @SQL + '
		FROM Invoice_Main M 
		LEFT OUTER JOIN Permit_Fees PMTF ON M.InvoiceNumber = PMTF.INVOICE_NO
	WHERE M.InvoiceNumber LIKE ''' + @InvNo + '''
		 AND M.InvoiceAddress LIKE ''' + @InvAddr + '''
		 AND M.InvoiceContact LIKE ''' + @InvContact + '''
		 AND PMTF.PERMIT_NO LIKE ''' + @RecNo + '''
		 AND M.InvoiceTotal - M.InvoicePaid ' + @PaidCondition + '
	UNION
	SELECT M.InvoiceNumber as invoice_no 
		,CONVERT(varchar(12), M.InvoiceDate, 1) as invoice_date
		,M.InvoiceTotal as invoice_total 
		,M.InvoicePaid as  invoice_paid 
		,ISNULL((select SUM(AMOUNT) from PROJECT_FEES where PAID_AMOUNT = 0 and (PAID_DATE IS NULL or PAID_DATE = ' + CHAR(39) + '' + CHAR(39) + ') and invoice_no = PRJF.INVOICE_NO), 0.00) as invoice_unpaid
		,M.InvoiceAddress as invoice_address
		,M.InvoiceContact as invoice_contact'
IF @Filter = 'record_no' SET @SQL = @SQL + ',PRJF.PROJECT_NO as record_no '
SET @SQL = @SQL + '
		FROM Invoice_Main M 
		LEFT OUTER JOIN Project_Fees PRJF ON M.InvoiceNumber = PRJF.INVOICE_NO
	WHERE M.InvoiceNumber LIKE ''' + @InvNo + '''
		 AND M.InvoiceAddress LIKE ''' + @InvAddr + '''
		 AND M.InvoiceContact LIKE ''' + @InvContact + '''
		 AND PRJF.PROJECT_NO LIKE ''' + @RecNo + '''
		 AND M.InvoiceTotal - M.InvoicePaid ' + @PaidCondition + '
	SELECT' + @strTop + 'invoice_no 
		,invoice_date 
		,invoice_total
		,invoice_paid
		,invoice_unpaid
		,invoice_address
		,invoice_contact'
IF @Filter = 'record_no' SET @SQL = @SQL + ',record_no '
SET @SQL = @SQL + '
	FROM #results 
	ORDER BY RIGHT(invoice_date,2) DESC, LEFT(invoice_date,2) DESC, SUBSTRING(invoice_date,4,2) DESC'
 
--PRINT(@SQL)
EXEC(@SQL)

DROP TABLE #results

 
END


-- ==========================================================================================
-- ============================================= 9.3.4.5 END
--9.3.4.6 no DB changes
-- ============================================= 9.3.4.7 START 02/17/2016
-- SA 12/10/2015 Additions from victor and Mark M

  
--==========================================================================================================================
---*************************************************************************************************************************
--  mdm - 11/18/15 - for Rafael and Monterey


GO
