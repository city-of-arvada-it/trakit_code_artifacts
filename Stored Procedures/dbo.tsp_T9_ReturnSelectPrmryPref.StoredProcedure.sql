USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ReturnSelectPrmryPref]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_ReturnSelectPrmryPref]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ReturnSelectPrmryPref]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  



                                    
-- ==========================================================================================
-- Revision History:
--       2014-07-09
-- ==========================================================================================   

CREATE PROCEDURE [dbo].[tsp_T9_ReturnSelectPrmryPref] 
              @Groupname varchar(10) = '',
              @TypeName varchar(255) = '',
              @UserID varchar(255) = '',
                       @Items varchar(2000)
              
AS
BEGIN

       Declare @SQL Varchar(8000)
          Declare @WhereClause Varchar(8000)

          begin
                     --Add options valuations from template detail
                     set @Items = ''''+REPLACE(@Items,',',''',''')+''''

                     set @WhereClause = 'where item in (' + @Items + ')'

                     if @Groupname <> ''
                     begin
                           set @WhereClause = @WhereClause + ' and Groupname = ''' + @Groupname + ''''
                     end

                     if @TypeName <> ''
                     begin
                           set @WhereClause = @WhereClause + ' and Typename = ''' + @TypeName + ''''
                     end

                     if @UserID <> ''
                     begin
                           set @WhereClause = @WhereClause + ' and UserID = ''' + @UserID + ''''
                     end

                     set @SQL = 'SELECT GroupName, UserID, Item, Value1, Value2, TypeName FROM Prmry_Preferences ' + @WhereClause + ';'


                     --print(@sql)
                     exec(@sql)

        end

END







GO
