USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_PlanningLetterFields]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[usp_Arvada_PlanningLetterFields]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_PlanningLetterFields]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_Arvada_PlanningLetterFields]
	@projectno varchar(50) = null,
	@ReportName varchar(50) = ''

as

/*
This should return all fields needed for the planning letters.
exec usp_Arvada_PlanningLetterFields 'da2018-0055',@ReportName = 'PHOnTrack1'
exec usp_Arvada_PlanningLetterFields 'DA2019-0013',@ReportName = 'PHOnTrack2'
exec usp_Arvada_PlanningLetterFields 'DA2019-0009',@ReportName = 'PHOnTrack2'
exec usp_Arvada_PlanningLetterFields 'DA2019-0013',@ReportName = 'PHOnTrack3'
exec usp_Arvada_PlanningLetterFields 'DA2019-0013',@ReportName = 'PHIssues2'
 select * 
 from project_actions
 where project_no = 'DA2019-0016'

*/

set nocount on

--if @projectno is null set @projectno = 'da2018-0055'

if object_id('tempdb.dbo.#projData') is not null drop table #projData
create table #projData
(
	ApplicantName nvarchar(4000),
	ApplicantAdd1 nvarchar(4000),
	ApplicantAdd2 nvarchar(4000),
	ApplicantCityState nvarchar(4000),
	ProjectNo varchar(50),
	ProjDescription varchar(500),
	ProjAddress nvarchar(4000),
	ProjCityState nvarchar(500),
	CustomScreenCaseType nvarchar(400),
	StartActionDate datetime,
	ProjectManager nvarchar(500),
	ProjectManagerPhoneNumber nvarchar(500),
	ProjectManagerEmail nvarchar(500),
	PlanningCommissionDate datetime,
	BalanceDue float,
	FirstReviewCompleteDate datetime,
	FirstCustomerResubmittalDate datetime,
	SignatureName nvarchar(4000),
	FirstAPPLICANTRESPONSERECEIVEDate datetime,
	FirstAPPLICANTRESPONSERECEIVEDateFMT as FORMAT (FirstAPPLICANTRESPONSERECEIVEDate, 'D', 'en-US' ),

	ADMINSUPPORTDOCFINALDate datetime,
	ADMINSUPPORTDOCFINALDateDateFMT as FORMAT (ADMINSUPPORTDOCFINALDate, 'D', 'en-US' ),

	SECONDCUSTOMERRESUBMITTALDate datetime,
	SECONDCUSTOMERRESUBMITTALDateFmt as FORMAT (SECONDCUSTOMERRESUBMITTALDate, 'D', 'en-US' ),
	DecisionAdminSupport datetime,
	DecisionAdminSupportDateFmt as FORMAT (DecisionAdminSupport, 'D', 'en-US' ),
	
	StartActionDateFmt as FORMAT (StartActionDate, 'D', 'en-US' ),
	PlanningCommissionDateFmt as FORMAT (PlanningCommissionDate, 'D', 'en-US' ),
	FirstReviewCompleteDateFmt as FORMAT (FirstReviewCompleteDate, 'D', 'en-US' ),
	FirstCustomerResubmittalDateFmt as FORMAT (FirstCustomerResubmittalDate, 'D', 'en-US' )
)

--2ND CUSTOMER RESUBMITTAL
--ADMIN SUPPORT FINAL
--select * 
--from project_Actions
--where project_no = 'DA2019-0013'
  

insert into #projData
(
	ProjectNo,
	ProjDescription,
	ProjectManager,
	ApplicantName,
	ApplicantAdd1,
	ApplicantAdd2,
	ApplicantCityState,
	ProjAddress,
	ProjCityState,
	ProjectManagerPhoneNumber,
	ProjectManagerEmail,
	SignatureName, 
	BalanceDue,
	CustomScreenCaseType,
	PlanningCommissionDate,
	StartActionDate,
	FirstReviewCompleteDate,
	FirstCustomerResubmittalDate,
	FirstAPPLICANTRESPONSERECEIVEDate,
	ADMINSUPPORTDOCFINALDate,
	SECONDCUSTOMERRESUBMITTALDate,
	DecisionAdminSupport
)
select 
	m.project_no,
	nullif(m.PROJECT_NAME,''),
	ProjectManager = nullif(m.PLANNER,''),
	ApplicantName = nullif(app.NAME,''),
	--appliedDate = m.applied, 
	ApplicantAdd1 = nullif(app.address1,''),
	ApplicantAdd2= nullif(app.address2,''),
	ApplicantCityState = case when app.CITY is not null and app.CITY != '' then app.CITY  else '' end +
						case when app.[STATE] is not null and app.[STATE] != '' then  ', ' + app.[STATE] else '' end +
						case when app.ZIP is not null and app.ZIP != '' then  ', ' + dbo.fn_Arvada_CleanZip(app.ZIP) else '' end,
	case when m.SITE_ADDR is not null and m.SITE_ADDR != '' then  m.SITE_ADDR else '' end ,
	 
	case when m.SITE_CITY is not null and m.SITE_CITY != '' then m.SITE_CITY else '' end +
	case when m.SITE_STATE is not null and m.SITE_STATE != '' then  ', ' + m.SITE_STATE else '' end +
	case when m.SITE_ZIP is not null and m.SITE_ZIP != '' then  '	' + dbo.fn_Arvada_CleanZip(m.SITE_ZIP) else '' end  as SiteFullAddress,
	isnull(u.phone, '720-898-7435'),
	nullif(u.UserEmailAddress1,''),
	'Rob Smetana, AICP' +char(10)+char(13)+
	'Manager of City Planning and Development'  +char(10)+char(13)+
	'720-898-7438' +char(10)+char(13)+
	'rsmetana@arvada.org'   as SignatureName, 
	m.balance_Due,
	nullif(udf.trk_type,''),
	pc.[Planning Commission Hearing],
	pc.[TRACK START],
	pc.[ADMIN SUPPORT FINAL],
	pc.[1st Customer Resubmittal],
	pc.[1ST APPLICANT RESPONSE RECIEVE],
	pc.[ADMIN SUPPORT DOC FINAL],
	pc.[2ND CUSTOMER RESUBMITTAL],
	pc.[DECISION ADMIN SUPPORT]
from dbo.project_main m
left join dbo.Project_People app with (nolock)
	on app.PROJECT_NO = m.PROJECT_NO
	and app.NAMETYPE = 'applicant'
left join dbo.prmry_users u
	on u.username = m.PLANNER 
left join  dbo.project_udf  udf
	on udf.project_no = m.project_no  
left join 
(
	select
		@projectno as project_no,
		p.[Planning Commission Hearing],
		p.[TRACK START],
		p.[ADMIN SUPPORT FINAL],
		p.[1st Customer Resubmittal],
		[1ST APPLICANT RESPONSE RECIEVE],
		[ADMIN SUPPORT DOC FINAL],
		[2ND CUSTOMER RESUBMITTAL],
		[DECISION ADMIN SUPPORT]
	from 
	(
		select a.project_no, a.action_Type, a.action_Date
		from project_Actions a
		where project_no = @projectno
		and action_Type in ('Planning Commission Hearing','TRACK START','ADMIN SUPPORT FINAL','1st Customer Resubmittal',
							'1ST APPLICANT RESPONSE RECIEVE','ADMIN SUPPORT FINAL','2ND CUSTOMER RESUBMITTAL',
							'DECISION ADMIN SUPPORT')
	) as b
	pivot
	(
		max(action_Date)
		for action_type in ([Planning Commission Hearing],[TRACK START],[ADMIN SUPPORT FINAL],[1st Customer Resubmittal],
							[1ST APPLICANT RESPONSE RECIEVE],[ADMIN SUPPORT DOC FINAL],[2ND CUSTOMER RESUBMITTAL],
							[DECISION ADMIN SUPPORT])
	) as p
) as pc
	on pc.project_no = m.PROJECT_NO
where m.PROJECT_NO = @projectno
 
 	--select distinct a.action_Type
		--from project_Actions a order by 1


declare @emptyFields varchar(max)
select @EmptyFields = 
		case when @ReportName = 'PHOnTrack1' 
				then 'ApplicantName,CustomScreenCaseType,ProjDescription,StartActionDate,ProjectManager,ProjectManagerPhoneNumber,ProjectManagerEmail,PlanningCommissionDate,BalanceDue,FirstReviewCompleteDate,FirstCustomerResubmittalDate,SignatureName' 
			when @ReportName = 'PHOnTrack2' 
				then 'ApplicantName,ProjDescription,StartActionDate,PlanningCommissionDate,FirstAPPLICANTRESPONSERECEIVEDate,ADMINSUPPORTDOCFINALDate,SECONDCUSTOMERRESUBMITTALDate,SignatureName' 					
			when @ReportName = 'PHOnTrack3' 
				then 'ApplicantName,ProjDescription,StartActionDate,PlanningCommissionDate,SECONDCUSTOMERRESUBMITTALDate,ADMINSUPPORTDOCFINALDate,FirstReviewCompleteDate,SignatureName' 								
				 
			when @ReportName = 'PHIssues2' 
				then 'ApplicantName,ProjDescription,StartActionDate,PlanningCommissionDate,SECONDCUSTOMERRESUBMITTALDate,ADMINSUPPORTDOCFINALDate,FirstReviewCompleteDate,SignatureName'

			when @ReportName = 'PHIssues3' 
				then 'ApplicantName,ProjDescription,StartActionDate,PlanningCommissionDate,SECONDCUSTOMERRESUBMITTALDate,DecisionAdminSupport,PlanningCommissionDate,SignatureName' 								
			
			else '' 
				
			end

create table #checkEmpty
(
	FieldName nvarchar(500),
	IsEmpty int default 0,
	rid int identity(1,1)
)

if @EmptyFields != ''
begin

	insert into #checkEmpty
	(
		FieldName
	)
	select
		val
	from [dbo].[tfn_Arvada_SSRS_String_To_Table](@EmptyFields,',',1)

end

declare @s int,
		@e int,
		@CheckCol varchar(500),
		@sql nvarchar(max),
		@MissingColumns nvarchar(max)

if exists(select 1 from #checkEmpty)
begin

	select @s = min(rid),
			@e = max(rid)
	from #checkEmpty

	while @s <= @e
	begin
	
		select @checkCol = FieldName
		from #checkEmpty
		where rid = @s

		if @checkCol is not null
		begin

			select @sql = 
				'update e
				 set IsEmpty = (Select count(1) from #projData  where ['+@CheckCol+'] is null )
				 from #checkEmpty e 
				 where rid = '+convert(varchar(40),@s)+'
				 '
		end
		exec(@sql)			


		set @s = @s+1
	end
end

select @MissingColumns  =
	stuff((select		
		',' + FieldName
	 from #checkEmpty
	 where ISEmpty > 0
	 for xml path('')
	 ),1,1,'')

 
select 
	ProjectNo,
	ProjDescription,
	ProjectManager,
	ApplicantName,
	ApplicantAdd1,
	ApplicantAdd2,
	ApplicantCityState,
	ProjAddress,
	ProjCityState,
	ProjectManagerPhoneNumber,
	ProjectManagerEmail,
	SignatureName, 
	BalanceDue,
	CustomScreenCaseType,
	PlanningCommissionDateFmt as PlanningCommissionDate,
	StartActionDateFmt as StartActionDate,
	FirstReviewCompleteDateFmt as FirstReviewCompleteDate,
	FirstCustomerResubmittalDateFmt as FirstCustomerResubmittalDate ,
	ltrim(rtrim( 
	ApplicantName +
	case when ApplicantAdd1 is not null and ApplicantAdd1!= '' then  char(10) + char(13) + ApplicantAdd1 else '' end +  
	case when ApplicantAdd2 is not null and ApplicantAdd2 != '' then  case when isnull(ApplicantAdd2,'') != '' then char(10) + char(13) else '' end + ApplicantAdd2 else '' end +  
	case when ApplicantCityState is not null and ApplicantCityState != '' then char(10) + char(13) + ApplicantCityState else '' end )) as AppAddFull,
	ltrim(rtrim( 
	ProjectNo +  char(10) + char(13) + 
	ProjDescription +
	case when ProjAddress is not null and ProjAddress!= '' then  char(10) + char(13) + ProjAddress else '' end +  
	case when ProjCityState is not null and ProjCityState != '' then  case when isnull(ProjCityState,'') != '' then char(10) + char(13) else '' end + ProjCityState else '' end
	)) as ProjInfoFull, 
	FirstAPPLICANTRESPONSERECEIVEDateFMT,
	ADMINSUPPORTDOCFINALDateDateFMT,	 
	SECONDCUSTOMERRESUBMITTALDateFmt,
	DecisionAdminSupportDateFmt,
	@MissingColumns as MissingColumns
from #projData
GO
