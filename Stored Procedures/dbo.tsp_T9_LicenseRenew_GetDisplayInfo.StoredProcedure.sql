USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_LicenseRenew_GetDisplayInfo]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_LicenseRenew_GetDisplayInfo]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_LicenseRenew_GetDisplayInfo]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

------------------------------------------------------------------------------------------------------------------------------------------------------------
/*
	Name:			tsp_T9_LicenseRenew_GetDisplayInfo

	Description:	Used by TRAKiT9 v9.3.4.x (JULY 2016 release) to display data relevent to the Parameter Set on the GLR screen instead of the default data
	
	Revision History
	----------------
	v1 - Apr 21, 2016 - EWH - Initial version
	v2 - Aug 02, 2016 - EWH - Now also returns the exception log and events log

*/
------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[tsp_T9_LicenseRenew_GetDisplayInfo]

	@ParameterSetID int = -1,
	@Mode varchar(10) = 'RINFO' -- REVIEW / RINFO or UPDATE / UINFO

AS
BEGIN
SET NOCOUNT ON;



-- make sure we are using the right terminology
if @Mode='REVIEW' set @Mode = 'RINFO'
if @Mode='UPDATE' set @Mode = 'UINFO'


-- decide if we should use the default or a custom data set
declare @USE_DEFAULT bit
if (select count(*) from LicenseRenew_License_Parameters where ParameterSetID = @ParameterSetID and ActiveMode = @Mode) > 0 set @USE_DEFAULT = 0
else set @USE_DEFAULT = 1


if @USE_DEFAULT = 0
begin
	-- execute the stored parameter
	declare @sql varchar(max)
	select @sql = ParameterValue from LicenseRenew_License_Parameters where ParameterSetID = @ParameterSetID and ActiveMode = @Mode

	-- make sure it isn't null or empty first
	if rtrim(ltrim(isnull(@sql, ''))) = '' select null
	else exec(@sql)

end
else
begin
	-- use default query:  License No. | License Type | Expire Date | Fees
	if @Mode = 'RINFO' 
	begin
		-- get data from LicenseRenew_Review_Main and LicenseRenew_Review_Fees 
		select distinct   LM.License_No as [License No.]
						, LM.license_type as [License Type]
						, LM.expired as [Expire Date]
						, COALESCE('<UL>' + (
												select '$' + cast(cast(LF.amount as money) as varchar(20)) + ' ' + cast(LF.code as varchar(20)) as LI 
												from licenserenew_review_fees LF 
													JOIN LicenseRenew_Review_Main LM2 on LF.license_no = LM2.LICENSE_NO 
												WHERE LF.license_no = LM.License_no for xml path('')) + '</UL>','<UL><LI>No Fees</LI></UL>'
											) as [Fees] 
		from LicenseRenew_Review_Main LM 
			left join licenserenew_review_fees LRF on LM.license_no = LRF.LICENSE_NO 
		ORDER BY LM.License_No
	end
	else
		if @Mode = 'UINFO' 
		begin
			-- get data from License2_Main and License2_Fees
			select distinct   LM.License_No as [License No.]
							, LM.license_type as [License Type]
							, LM.expired as [Expire Date]
							, COALESCE('<UL>' + (
													select '$' + cast(cast(LF.amount as money) as varchar(20)) + ' ' + cast(LF.code as varchar(20)) as LI -- varchar(20)
													from license2_fees LF 
														JOIN License2_Main LM2 on LF.license_no = LM2.LICENSE_NO 
													WHERE LF.license_no = LM.License_no for xml path('')) + '</UL>','<UL><LI>No Fees</LI></UL>'
												) as [Fees] 
			from License2_Main LM 
				left join license2_fees LRF on LM.license_no = LRF.LICENSE_NO 
			ORDER BY LM.License_No
		end
	end	


	-- return exception log and event log
	declare @psid int
	declare @start datetime
	declare @end datetime
	select top 1 @end=[RunDateTime], @psid=[ParameterSetID], @mode=[ActionMode] from [LicenseRenew_Audit] order by id desc
	select top 1 @start=isnull([RunDateTime],@end) from [LicenseRenew_Audit] where ID = (select top 1 ID+1 [ID] from [LicenseRenew_Audit] where ParameterSetID <> @psid or ActionMode <> @mode or cast(RunDateTime as date) <> cast(@end as date) order by ID desc)

	-- need to make sure we return only the info for the requested parameter set ID, in case more than 1 user is actively using the GLR process
	if @psid=@ParameterSetID
	begin
		-- return the exception log as well so user can see if there were any issues in processing
		select PROCESSING_DATE [Time], LICENSE_NO [License No.], EXCEPTION_DESC [Exception Message] from [LicenseRenew_Process_Exceptions] where PROCESSING_DATE >= @start order by PROCESSING_DATE 

		-- event log --> only display if in QA mode, as it may contain SQL statements!
		select convert(varchar, [EventDate], 121) [Time], [SourceMode] [Mode], [EventText] [Event] from [LicenseRenew_EventLog] where EventDate >= @start order by [EventDate] 

	end
	else
	begin
		-- return empty datasets 
		select null
		select null
	end

END

GO
