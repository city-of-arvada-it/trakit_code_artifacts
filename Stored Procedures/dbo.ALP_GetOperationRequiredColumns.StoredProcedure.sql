USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[ALP_GetOperationRequiredColumns]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[ALP_GetOperationRequiredColumns]
GO
/****** Object:  StoredProcedure [dbo].[ALP_GetOperationRequiredColumns]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===============================================================================================
-- Author:		Erick Hampton
-- Revised:		Dec 22, 2016
-- Description:	Gets the tables and columns required for an operation to perform its activities
-- ===============================================================================================
CREATE PROCEDURE [dbo].[ALP_GetOperationRequiredColumns]

	@OperationID int = -1 

AS
BEGIN

	select TableName, ColumnName, IsDisplayInfo from [ALP_OperationRequiredColumns] where OperationID=@OperationID

END

GO
