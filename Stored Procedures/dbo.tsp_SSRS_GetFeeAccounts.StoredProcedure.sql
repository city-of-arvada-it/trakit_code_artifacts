USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_SSRS_GetFeeAccounts]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_SSRS_GetFeeAccounts]
GO
/****** Object:  StoredProcedure [dbo].[tsp_SSRS_GetFeeAccounts]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  					  
					  
CREATE PROCEDURE [dbo].[tsp_SSRS_GetFeeAccounts] 
	@FromDate DATETIME,
	@ToDate DATETIME,
	@Module VARCHAR(2000)
	
AS
BEGIN


--RB Check if table exists before creating it
   IF OBJECT_ID('tempdb..#table1') IS NOT NULL  
	drop table #table1

create table #table1
	(RECORD_NO varchar(20),
	RECORD_TYPE varchar(20),
	CODE varchar(30),
	ACCOUNT varchar(30),
	FEETYPE varchar(10),
	DEPOSITID varchar(30))

create index #table1_ix1 on #table1 (RECORD_NO);

--GET FEES--
DECLARE @PERMITS varchar(50)
DECLARE @PROJECTS varchar(50)
DECLARE @CASES varchar(50)
DECLARE @LICENSE2 varchar(50)
DECLARE @AEC varchar(50)

SET @PERMITS = 'PermitTRAK'
SET @PROJECTS = 'ProjectTRAK'
SET @CASES = 'CodeTRAK'
SET @LICENSE2 = 'LicenseTRAK'
SET @AEC = 'AEC TRAK'

--BEGIN of TEST ONLY SCRIPT-------------------------------------------------------------------------------------------------------------------------------------------------
/*
DECLARE	@FromDate DATETIME
DECLARE	@ToDate DATETIME
DECLARE	@Account VARCHAR(2000)
DECLARE	@Module VARCHAR(2000)

SET @Module = ('PERMITS')
SET @Account = ('1015071130244115,1015074230144261')
SET @FromDate = '11/03/2011'
SET @ToDate = '11/03/2011'

update Permit_Fees set ACCOUNT = NULL where ACCOUNT = '' and DETAIL = 0 and PAID = 1 and PAID_DATE between @FromDate and @ToDate  ;
update Permit_SubFees set ACCOUNT = NULL where ACCOUNT = '' and PARENTID in (Select RECORDID from Permit_Fees where DETAIL = 1 and PAID_DATE between @FromDate and @ToDate) ;
*/
--END of TEST ONLY FEATURES--------------------------------------------------------------------------------------------------------------------------------------------------

--PERMITS
IF @PERMITS in (SELECT * FROM [dbo].[tfn_SSRS_String_To_Table](@Module,',',1))

BEGIN
	insert into #table1 (RECORD_NO, RECORD_TYPE, ACCOUNT, FEETYPE,DEPOSITID)
	select Permit_Fees.PERMIT_NO, 'PermitTRAK', Permit_Fees.ACCOUNT, Permit_Fees.FEETYPE, Permit_Fees.DEPOSITID
	from Permit_Fees inner join Permit_Main on Permit_Main.PERMIT_NO = Permit_Fees.PERMIT_NO
	where DETAIL = 0 and PAID = 1 and PAID_AMOUNT > 0 and PAID_DATE between @FromDate and @ToDate 
	UNION ALL
	select Permit_SubFees.PERMIT_NO, 'PermitTRAK', Permit_SubFees.ACCOUNT, Permit_Fees.FEETYPE, Permit_Fees.DEPOSITID
	from Permit_SubFees inner join Permit_Fees on Permit_Fees.RECORDID = Permit_SubFees.PARENTID
	inner join Permit_Main on Permit_Main.PERMIT_NO = Permit_SubFees.PERMIT_NO
	where PARENTID in (Select RECORDID from Permit_Fees where DETAIL = 1 and Permit_Fees.PAID_AMOUNT > 0 and PAID_DATE between @FromDate and @ToDate)
	and Permit_SubFees.PAID = 1  and Permit_SubFees.AMOUNT > 0  ;
	
--Offset any Permit Deposit Reductions
insert into #table1
	( RECORD_NO, RECORD_TYPE, ACCOUNT, DEPOSITID)
select
	  RECORD_NO, RECORD_TYPE,(select ACCOUNT from Permit_Fees where Permit_Fees.RECORDID = #table1 .DEPOSITID), DEPOSITID
from #table1 
	where RECORD_TYPE = 'PermitTRAK'
	and DEPOSITID in 
		(select RECORDID from Permit_Fees 
			where PERMIT_NO = #table1.RECORD_NO 
			and FEETYPE = 'DEPOSIT')
	and (FEETYPE <> 'REFUND') ;
		
END
--Select * from #Table1

--PROJECTS
IF @PROJECTS in (SELECT * FROM [dbo].[tfn_SSRS_String_To_Table](@Module,',',1))

BEGIN
	insert into #table1 (RECORD_NO, RECORD_TYPE, ACCOUNT, FEETYPE, DEPOSITID)
	select Project_Fees.PROJECT_NO, 'ProjectTRAK', Project_Fees.ACCOUNT, Project_Fees.FEETYPE, Project_Fees.DEPOSITID
	from Project_Fees inner join Project_Main on Project_Main.PROJECT_NO = Project_Fees.PROJECT_NO
	where DETAIL = 0 and PAID = 1 and PAID_AMOUNT > 0 and PAID_DATE between @FromDate and @ToDate 
	UNION ALL
	select Project_SubFees.PROJECT_NO, 'ProjectTRAK', Project_SubFees.ACCOUNT, Project_Fees.FEETYPE, Project_Fees.DEPOSITID
	from Project_SubFees inner join Project_Fees on Project_Fees.RECORDID = Project_SubFees.PARENTID
	inner join Project_Main on Project_Main.PROJECT_NO = Project_SubFees.PROJECT_NO
	where PARENTID in (Select RECORDID from Project_Fees where DETAIL = 1 and Project_Fees.PAID_AMOUNT > 0 and PAID_DATE between @FromDate and @ToDate)
	and Project_SubFees.PAID = 1  and Project_SubFees.AMOUNT > 0 ;
	
--Offset any Project Deposit Reductions
insert into #table1
	( RECORD_NO, RECORD_TYPE, ACCOUNT, DEPOSITID)
select
	  RECORD_NO, RECORD_TYPE,(select ACCOUNT from Project_Fees where Project_Fees.RECORDID = #table1 .DEPOSITID), DEPOSITID
from #table1 
	where RECORD_TYPE = 'ProjectTRAK'
	and DEPOSITID in 
		(select RECORDID from Project_Fees 
			where PROJECT_NO = #table1.RECORD_NO 
			and FEETYPE = 'DEPOSIT')
	and (FEETYPE <> 'REFUND') ;

END

--CASES
IF @CASES in (SELECT * FROM [dbo].[tfn_SSRS_String_To_Table](@Module,',',1))

BEGIN
	insert into #table1 (RECORD_NO, RECORD_TYPE, ACCOUNT, FEETYPE, DEPOSITID)
	select Case_Fees.CASE_NO, 'CaseTRAK', Case_Fees.ACCOUNT, Case_Fees.FEETYPE, Case_Fees.DEPOSITID
	from Case_Fees inner join Case_Main on Case_Main.CASE_NO = Case_Fees.CASE_NO
	where DETAIL = 0 and PAID = 1 and PAID_AMOUNT > 0 and PAID_DATE between @FromDate and @ToDate 
	UNION ALL
	select Case_SubFees.CASE_NO, 'CaseTRAK', Case_SubFees.ACCOUNT, Case_Fees.FEETYPE, Case_Fees.DEPOSITID
	from Case_SubFees inner join Case_Fees on Case_Fees.RECORDID = Case_SubFees.PARENTID
	inner join Case_Main on Case_Main.CASE_NO = Case_SubFees.CASE_NO
	where PARENTID in (Select RECORDID from Case_Fees where DETAIL = 1 and Case_Fees.PAID_AMOUNT > 0 and PAID_DATE between @FromDate and @ToDate)
	and Case_SubFees.PAID = 1  and Case_SubFees.AMOUNT > 0 ;
	
--Offset any Case Deposit Reductions
insert into #table1
	( RECORD_NO, RECORD_TYPE, ACCOUNT, DEPOSITID)
select
	  RECORD_NO, RECORD_TYPE,(select ACCOUNT from Case_Fees where Case_Fees.RECORDID = #table1 .DEPOSITID), DEPOSITID
from #table1 
	where RECORD_TYPE = 'CaseTRAK'
	and DEPOSITID in 
		(select RECORDID from Case_Fees 
			where CASE_NO = #table1.RECORD_NO 
			and FEETYPE = 'DEPOSIT')
	and (FEETYPE <> 'REFUND') ;

END

--LICENSE2
IF @LICENSE2 in (SELECT * FROM [dbo].[tfn_SSRS_String_To_Table](@Module,',',1))

BEGIN
	insert into #table1 (RECORD_NO, RECORD_TYPE, ACCOUNT, FEETYPE, DEPOSITID)
	select License2_Fees.LICENSE_NO, 'LicenseTRAK', License2_Fees.ACCOUNT, License2_Fees.FEETYPE, License2_Fees.DEPOSITID
	from License2_Fees inner join License2_Main on License2_Main.LICENSE_NO = License2_Fees.LICENSE_NO
	where DETAIL = 0 and PAID = 1 and PAID_AMOUNT > 0 and PAID_DATE between @FromDate and @ToDate 
	UNION ALL
	select License2_SubFees.LICENSE_NO, 'LicenseTRAK', License2_SubFees.ACCOUNT, License2_Fees.FEETYPE, License2_Fees.DEPOSITID
	from License2_SubFees inner join License2_Fees on License2_Fees.RECORDID = License2_SubFees.PARENTID
	inner join License2_Main on License2_Main.LICENSE_NO = License2_SubFees.LICENSE_NO
	where PARENTID in (Select RECORDID from License2_Fees where DETAIL = 1 and License2_Fees.PAID_AMOUNT > 0 and PAID_DATE between @FromDate and @ToDate)
	and License2_SubFees.PAID = 1  and License2_SubFees.AMOUNT > 0 ;
	
--Offset any License2 Deposit Reductions
insert into #table1
	( RECORD_NO, RECORD_TYPE, ACCOUNT, DEPOSITID)
select
	  RECORD_NO, RECORD_TYPE,(select ACCOUNT from License2_Fees where License2_Fees.RECORDID = #table1 .DEPOSITID), DEPOSITID
from #table1 
	where RECORD_TYPE = 'LicenseTRAK'
	and DEPOSITID in 
		(select RECORDID from License2_Fees 
			where LICENSE_NO = #table1.RECORD_NO 
			and FEETYPE = 'DEPOSIT')
	and (FEETYPE <> 'REFUND') ;
END

--AEC
IF @AEC in (SELECT * FROM [dbo].[tfn_SSRS_String_To_Table](@Module,',',1))

BEGIN
	insert into #table1 (RECORD_NO, RECORD_TYPE, ACCOUNT, FEETYPE, DEPOSITID)
	select AEC_Fees.ST_LIC_NO, 'AEC TRAK', AEC_Fees.ACCOUNT, AEC_Fees.FEETYPE, AEC_Fees.DEPOSITID
	from AEC_Fees inner join AEC_Main on AEC_Main.ST_LIC_NO = AEC_Fees.ST_LIC_NO
	where DETAIL = 0 and PAID = 1 and PAID_AMOUNT > 0 and PAID_DATE between @FromDate and @ToDate 
	UNION ALL
	select AEC_SubFees.ST_LIC_NO, 'AEC TRAK', AEC_SubFees.ACCOUNT, AEC_Fees.FEETYPE, AEC_Fees.DEPOSITID
	from AEC_SubFees inner join AEC_Fees on AEC_Fees.RECORDID = AEC_SubFees.PARENTID
	inner join AEC_Main on AEC_Main.ST_LIC_NO = AEC_SubFees.ST_LIC_NO
	where PARENTID in (Select RECORDID from AEC_Fees where DETAIL = 1 and AEC_Fees.PAID_AMOUNT > 0 and PAID_DATE between @FromDate and @ToDate)
	and AEC_SubFees.PAID = 1  and AEC_SubFees.AMOUNT > 0  ;
	
--Offset any AEC Deposit Reductions
insert into #table1
	( RECORD_NO, RECORD_TYPE, ACCOUNT, DEPOSITID)
select
	  RECORD_NO, RECORD_TYPE,(select ACCOUNT from AEC_Fees where AEC_Fees.RECORDID = #table1 .DEPOSITID), DEPOSITID
from #table1 
	where RECORD_TYPE = 'AEC TRAK'
	and DEPOSITID in 
		(select RECORDID from AEC_Fees 
			where ST_LIC_NO = #table1.RECORD_NO 
			and FEETYPE = 'DEPOSIT')
	and (FEETYPE <> 'REFUND'
	or CODE <> 'REFUND') ;

END

--Update Credits and Refunds
update #table1 set #table1.ACCOUNT = '*NONE*' where (#table1.ACCOUNT is NULL OR #table1.ACCOUNT = '');

--THIS SELECT NEEDS TO BE HERE FOR SSRS TO GET DATA------------------------------------------
select distinct ACCOUNT from #table1 order by ACCOUNT;
---------------------------------------------------------------------------------------------
END





GO
