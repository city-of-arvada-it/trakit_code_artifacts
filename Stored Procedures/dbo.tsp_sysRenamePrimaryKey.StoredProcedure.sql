USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_sysRenamePrimaryKey]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_sysRenamePrimaryKey]
GO
/****** Object:  StoredProcedure [dbo].[tsp_sysRenamePrimaryKey]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  					  					  CREATE PROCEDURE [dbo].[tsp_sysRenamePrimaryKey] 
AS
BEGIN
  SET NOCOUNT ON

	declare @OldDefaultConstraint nvarchar(max)
	declare @NewDefaultConstraint nvarchar(max)
	declare @SqlStmt nvarchar(max)

	declare @TblConstraints table
	(
		OldDefaultConstraint nvarchar(max),
		NewDefaultConstraint nvarchar(max)
	)

	insert into @TblConstraints (OldDefaultConstraint, NewDefaultConstraint)
		   select OldDefaultConstraint , NewDefaultConstraint  from (select name as OldDefaultConstraint, 'PK_' + OBJECT_NAME(parent_object_id) as NewDefaultConstraint  from sys.objects   where SUBSTRING(name,1,4) = 'PK__') mdm

	--select * from @TblConstraints

	declare cur cursor for
	select OldDefaultConstraint,
	NewDefaultConstraint
	from @TblConstraints

	open cur
	while (1=1)
	begin

		fetch next from cur into @OldDefaultConstraint, @NewDefaultConstraint

		if @@FETCH_STATUS <> 0
			break

		set @SqlStmt = N'sp_rename ''' + @OldDefaultConstraint + ''', ''' + @NewDefaultConstraint + ''''

		select @SqlStmt

		exec sp_executesql @SqlStmt

	end

	close cur
	deallocate cur

END




GO
