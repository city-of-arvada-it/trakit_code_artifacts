USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearch_ShareGroupToUser]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_AdvSearch_ShareGroupToUser]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearch_ShareGroupToUser]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

	 Create Procedure [dbo].[tsp_T9_AdvSearch_ShareGroupToUser]
		@List as udtt_T9_AdvSearch_AssignedUserList readonly
		As 
		Begin
			--- this will always only be a single deparment, so likely wont have to deal with the issue of 2 different leaders sharing back and forth.
			--- if this changes will need to add DepartmentLeader to the UDT passed in.
			Delete M from Prmry_AdvSearch_DepartmentMembers M Join @List L on L.DepartmentID = M.DepartmentID
			and M.UserID not in (Select DepartmentLeader from Prmry_AdvSearch_Departments D Join @List Li on Li.DepartmentID = D.DepartmentID)
			
			Insert into Prmry_AdvSearch_DepartmentMembers (departmentid, userid, canget, canshare, canrename, candelete, notifyofnewshare, notifyofdeletedshare)
			Select L.DepartmentID, L.Userid, 1, 1, 0, 0,0,0 from @List L where L.UserID not in  (Select DepartmentLeader from Prmry_AdvSearch_Departments D Join @List Li on Li.DepartmentID = D.DepartmentID)
		end	

GO
