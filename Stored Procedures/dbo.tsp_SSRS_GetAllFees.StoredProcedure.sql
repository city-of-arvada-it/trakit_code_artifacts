USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_SSRS_GetAllFees]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_SSRS_GetAllFees]
GO
/****** Object:  StoredProcedure [dbo].[tsp_SSRS_GetAllFees]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  					  
					  					  					  
CREATE PROCEDURE [dbo].[tsp_SSRS_GetAllFees]
	@PERMIT_NO VARCHAR(30),	
	@PROJECT_NO VARCHAR(30),
	@CASE_NO VARCHAR(30),
	@LICENSE_NO VARCHAR(30),
	@ST_LIC_NO VARCHAR(30)	
AS
BEGIN


--RB Check if table exists before creating it
   IF OBJECT_ID('tempdb..#table1') IS NOT NULL  
	drop table #table1

create table #table1
	(RECORD_NO varchar(20),
	RECORD_TYPE varchar(20),
	CODE varchar(30),
	ITEM varchar(30),
	PARENT_DESCRIPTION varchar(40),
	DESCRIPTION varchar(40),
	ACCOUNT varchar(30),
	QUANTITY float,
	PAID_AMOUNT float,
	PAID_DATE date,
	RECEIPT_NO varchar(30),
	CHECK_NO varchar(30),
	PAY_METHOD varchar(30),
	PAID_BY varchar(30),
	COLLECTED_BY varchar(50),
	FEETYPE varchar(10),
	DEPOSITID varchar(30),
	SITE_ADDR varchar(70),
	SITE_APN varchar (50)) ;

create index #table1_ix1 on #table1 (RECORD_NO);

--TESTING ONLY---------------------------------------------------------
--Truncate Table #table1
--Select * from #Table1 order by RECORD_NO, CODE, ITEM
--TESTING ONLY---------------------------------------------------------

--PERMITS-------------------------------------------------------------------------------------------------------------------------------------------

BEGIN
	insert into #table1 (RECORD_NO, RECORD_TYPE, CODE, ITEM, DESCRIPTION, ACCOUNT, QUANTITY, PAID_AMOUNT, PAID_DATE, RECEIPT_NO, CHECK_NO, PAY_METHOD, 
		COLLECTED_BY, FEETYPE, DEPOSITID, PAID_BY)
	select Permit_Fees.PERMIT_NO, 'PERMIT', Permit_Fees.CODE, NULL, Permit_Fees.DESCRIPTION, Permit_Fees.ACCOUNT, Permit_Fees.QUANTITY, Permit_Fees.PAID_AMOUNT, 
		Permit_Fees.PAID_DATE, Permit_Fees.RECEIPT_NO, Permit_Fees.CHECK_NO,Permit_Fees.PAY_METHOD, Permit_Fees.COLLECTED_BY, Permit_Fees.FEETYPE, 
		Permit_Fees.DEPOSITID, Permit_Fees.PAID_BY
	from Permit_Fees
	where DETAIL = 0 and PERMIT_NO = @PERMIT_NO
	UNION
	select Permit_SubFees.PERMIT_NO, 'PERMIT', Permit_SubFees.CODE, Permit_SubFees.ITEM, Permit_SubFees.DESCRIPTION, Permit_SubFees.ACCOUNT, 
		Permit_SubFees.QUANTITY, Permit_SubFees.AMOUNT as PAID_AMOUNT, Permit_Fees.PAID_DATE as PAID_DATE, Permit_Fees.RECEIPT_NO as RECEIPT_NO, 
		Permit_Fees.CHECK_NO as CHECK_NO, Permit_Fees.PAY_METHOD, Permit_Fees.COLLECTED_BY, Permit_Fees.FEETYPE, Permit_Fees.DEPOSITID, Permit_Fees.PAID_BY
	from Permit_SubFees inner join Permit_Fees on Permit_Fees.RECORDID = Permit_SubFees.PARENTID
	where PARENTID in (Select RECORDID from Permit_Fees where DETAIL = 1 and Permit_Fees.PERMIT_NO = @PERMIT_NO)
	order by PERMIT_NO, CODE ;
END

--PROJECTS---------------------------------------------------------------------------------------------------------------------------------------------

BEGIN
	insert into #table1 (RECORD_NO, RECORD_TYPE, CODE, ITEM, DESCRIPTION, ACCOUNT, QUANTITY, PAID_AMOUNT, PAID_DATE, RECEIPT_NO, CHECK_NO, PAY_METHOD, 
		COLLECTED_BY, FEETYPE, DEPOSITID, PAID_BY)
	select Project_Fees.PROJECT_NO, 'PROJECT', Project_Fees.CODE, NULL, Project_Fees.DESCRIPTION, Project_Fees.ACCOUNT, Project_Fees.QUANTITY, 
		Project_Fees.PAID_AMOUNT, Project_Fees.PAID_DATE, Project_Fees.RECEIPT_NO, Project_Fees.CHECK_NO, Project_Fees.PAY_METHOD, 
		Project_Fees.COLLECTED_BY, Project_Fees.FEETYPE, Project_Fees.DEPOSITID, Project_Fees.PAID_BY
	from Project_Fees
	where DETAIL = 0 and PROJECT_NO = @PROJECT_NO
	UNION
	select Project_SubFees.PROJECT_NO, 'PROJECT', Project_SubFees.CODE, Project_SubFees.ITEM, Project_SubFees.DESCRIPTION, Project_SubFees.ACCOUNT, 
		Project_SubFees.QUANTITY, Project_SubFees.AMOUNT as PAID_AMOUNT, Project_Fees.PAID_DATE as PAID_DATE, Project_Fees.RECEIPT_NO as RECEIPT_NO, 
		Project_Fees.CHECK_NO as CHECK_NO, Project_Fees.PAY_METHOD, Project_Fees.COLLECTED_BY, Project_Fees.FEETYPE, Project_Fees.DEPOSITID, Project_Fees.PAID_BY
	from Project_SubFees inner join Project_Fees on Project_Fees.RECORDID = Project_SubFees.PARENTID
	where PARENTID in (Select RECORDID from Project_Fees where DETAIL = 1 and Project_Fees.PROJECT_NO = @PROJECT_NO)
	order by PROJECT_NO, CODE ;
END

--CASES--------------------------------------------------------------------------------------------------------------------------------------------
BEGIN

	insert into #table1 (RECORD_NO, RECORD_TYPE, CODE, ITEM, DESCRIPTION, ACCOUNT, QUANTITY, PAID_AMOUNT, PAID_DATE, RECEIPT_NO, CHECK_NO, PAY_METHOD, 
		COLLECTED_BY, FEETYPE, DEPOSITID, PAID_BY)
	select Case_Fees.CASE_NO, 'CASE',Case_Fees.CODE, NULL, Case_Fees.DESCRIPTION, Case_Fees.ACCOUNT, Case_Fees.QUANTITY, Case_Fees.PAID_AMOUNT, 
		Case_Fees.PAID_DATE, Case_Fees.RECEIPT_NO, Case_Fees.CHECK_NO, Case_Fees.PAY_METHOD, Case_Fees.COLLECTED_BY, 
		Case_Fees.FEETYPE, Case_Fees.DEPOSITID, Case_Fees.PAID_BY
	from Case_Fees
	where DETAIL = 0 and CASE_NO = @CASE_NO
	UNION
	select Case_SubFees.CASE_NO, 'CASE',Case_SubFees.CODE, Case_SubFees.ITEM, Case_SubFees.DESCRIPTION, Case_SubFees.ACCOUNT, Case_SubFees.QUANTITY, 
		Case_SubFees.AMOUNT as PAID_AMOUNT, Case_Fees.PAID_DATE as PAID_DATE, Case_Fees.RECEIPT_NO as RECEIPT_NO, 
		Case_Fees.CHECK_NO as CHECK_NO,	Case_Fees.PAY_METHOD, Case_Fees.COLLECTED_BY, Case_Fees.FEETYPE, Case_Fees.DEPOSITID, Case_Fees.PAID_BY
	from Case_SubFees inner join Case_Fees on Case_Fees.RECORDID = Case_SubFees.PARENTID
	where PARENTID in (Select RECORDID from Case_Fees where DETAIL = 1 and Case_Fees.CASE_NO = @CASE_NO)
	order by CASE_NO, CODE ;
END

--LICENSES------------------------------------------------------------------------------------------------------------------------------------------

BEGIN
	insert into #table1 (RECORD_NO, RECORD_TYPE, CODE, ITEM, DESCRIPTION, ACCOUNT, QUANTITY, PAID_AMOUNT, PAID_DATE, RECEIPT_NO, CHECK_NO, PAY_METHOD, 
		COLLECTED_BY, FEETYPE, DEPOSITID, PAID_BY)
	select License2_Fees.LICENSE_NO, 'LICENSE', License2_Fees.CODE, NULL, License2_Fees.DESCRIPTION, License2_Fees.ACCOUNT, License2_Fees.QUANTITY, License2_Fees.PAID_AMOUNT, 
		License2_Fees.PAID_DATE, License2_Fees.RECEIPT_NO, License2_Fees.CHECK_NO, License2_Fees.PAY_METHOD, License2_Fees.COLLECTED_BY, License2_Fees.FEETYPE, 
		License2_Fees.DEPOSITID, License2_Fees.PAID_BY
	from License2_Fees
	where DETAIL = 0 and LICENSE_NO = @LICENSE_NO
	UNION
	select License2_SubFees.LICENSE_NO, 'LICENSE', License2_SubFees.CODE, License2_SubFees.ITEM, License2_SubFees.DESCRIPTION, License2_SubFees.ACCOUNT, 
		License2_SubFees.QUANTITY, License2_SubFees.AMOUNT as PAID_AMOUNT, License2_Fees.PAID_DATE as PAID_DATE, License2_Fees.RECEIPT_NO as RECEIPT_NO, 
		License2_Fees.CHECK_NO as CHECK_NO, License2_Fees.PAY_METHOD, License2_Fees.COLLECTED_BY, License2_Fees.FEETYPE, License2_Fees.DEPOSITID, License2_Fees.PAID_BY
	from License2_SubFees inner join License2_Fees on License2_Fees.RECORDID = License2_SubFees.PARENTID
	where PARENTID in (Select RECORDID from License2_Fees where DETAIL = 1 and License2_Fees.LICENSE_NO = @LICENSE_NO)
	order by LICENSE_NO, CODE ;
END

--AEC-----------------------------------------------------------------------------------------------------------------------------------------------

BEGIN
	insert into #table1 (RECORD_NO, RECORD_TYPE, CODE, ITEM, DESCRIPTION, ACCOUNT, QUANTITY, PAID_AMOUNT, PAID_DATE, RECEIPT_NO, CHECK_NO, PAY_METHOD, 
		COLLECTED_BY, FEETYPE, DEPOSITID, PAID_BY)
	select AEC_Fees.ST_LIC_NO, 'AEC', AEC_Fees.CODE, NULL, AEC_Fees.DESCRIPTION, AEC_Fees.ACCOUNT, AEC_Fees.QUANTITY, AEC_Fees.PAID_AMOUNT, 
		AEC_Fees.PAID_DATE, AEC_Fees.RECEIPT_NO, AEC_Fees.CHECK_NO,	AEC_Fees.PAY_METHOD, AEC_Fees.COLLECTED_BY, AEC_Fees.FEETYPE, AEC_Fees.DEPOSITID, AEC_Fees.PAID_BY
	from AEC_Fees
	where DETAIL = 0 and ST_LIC_NO = @ST_LIC_NO
	UNION
	select AEC_SubFees.ST_LIC_NO, 'AEC', AEC_SubFees.CODE, AEC_SubFees.ITEM, AEC_SubFees.DESCRIPTION, AEC_SubFees.ACCOUNT, AEC_SubFees.QUANTITY, 
		AEC_SubFees.AMOUNT as PAID_AMOUNT, AEC_Fees.PAID_DATE as PAID_DATE, AEC_Fees.RECEIPT_NO as RECEIPT_NO, AEC_Fees.CHECK_NO as CHECK_NO, 
		AEC_Fees.PAY_METHOD, AEC_Fees.COLLECTED_BY, AEC_Fees.FEETYPE, AEC_Fees.DEPOSITID, AEC_Fees.PAID_BY
	from AEC_SubFees inner join AEC_Fees on AEC_Fees.RECORDID = AEC_SubFees.PARENTID
	where PARENTID in (Select RECORDID from AEC_Fees where DETAIL = 1 and AEC_Fees.ST_LIC_NO = @ST_LIC_NO)
	order by ST_LIC_NO, CODE ;
END

--Update Credits and Refunds
update #table1 set #table1.PAID_AMOUNT = #table1.PAID_AMOUNT * -1 where CODE = 'REFUND' ;
update #table1 set #table1.RECEIPT_NO = 'REFUND' where #table1.CODE = 'REFUND' ;
update #table1 set #table1.PAID_AMOUNT = #table1.PAID_AMOUNT * -1 where CODE = 'CREDIT' ;
update #table1 set #table1.RECEIPT_NO = 'CREDIT' where #table1.CODE = 'CREDIT' ;
update #table1 set #table1.ACCOUNT = '*NONE*' where #table1.ACCOUNT is NULL ;

--Offset any Permit Deposit Reductions
insert into #table1
	( RECORD_NO, RECORD_TYPE, ACCOUNT, QUANTITY, CODE, PAID_AMOUNT, PAID_DATE, RECEIPT_NO, CHECK_NO, PAY_METHOD, FEETYPE, DEPOSITID, COLLECTED_BY, PAID_BY)
select
	  RECORD_NO, RECORD_TYPE, ACCOUNT, QUANTITY, CODE, (PAID_AMOUNT*-1), PAID_DATE, 'OFFSET', CHECK_NO, 'OFFSET', 'OFFSET', DEPOSITID, COLLECTED_BY, PAID_BY
from #table1 
	where RECORD_TYPE = 'PERMIT'
	and DEPOSITID in 
		(select RECORDID from Permit_Fees 
			where PERMIT_NO = #table1.RECORD_NO 
			and FEETYPE = 'DEPOSIT')
	and FEETYPE <> 'REFUND' ;

--Offset any Project Deposit Reductions
insert into #table1
	( RECORD_NO, RECORD_TYPE, ACCOUNT, QUANTITY, CODE, PAID_AMOUNT, PAID_DATE, RECEIPT_NO, CHECK_NO, PAY_METHOD, FEETYPE, DEPOSITID, COLLECTED_BY, PAID_BY)
select
	  RECORD_NO, RECORD_TYPE, ACCOUNT, QUANTITY, CODE, (PAID_AMOUNT*-1), PAID_DATE, 'OFFSET', CHECK_NO, 'OFFSET', 'OFFSET', DEPOSITID, COLLECTED_BY, PAID_BY
from #table1 
	where RECORD_TYPE = 'PROJECT'
	and DEPOSITID in 
		(select RECORDID from Project_Fees 
			where PROJECT_NO = #table1.RECORD_NO 
			and FEETYPE = 'DEPOSIT')
	and FEETYPE <> 'REFUND' ;

--Offset any Code Deposit Reductions	
insert into #table1
	( RECORD_NO, RECORD_TYPE, ACCOUNT, QUANTITY, CODE, PAID_AMOUNT, PAID_DATE, RECEIPT_NO, CHECK_NO, PAY_METHOD, FEETYPE, DEPOSITID, COLLECTED_BY, PAID_BY)
select
	  RECORD_NO, RECORD_TYPE, ACCOUNT, QUANTITY, CODE, (PAID_AMOUNT*-1), PAID_DATE, 'OFFSET', CHECK_NO, 'OFFSET', 'OFFSET', DEPOSITID, COLLECTED_BY, PAID_BY
from #table1 
	where RECORD_TYPE = 'CASE'
	and DEPOSITID in 
		(select RECORDID from Case_Fees 
			where CASE_NO = #table1.RECORD_NO 
			and FEETYPE = 'DEPOSIT')
	and FEETYPE <> 'REFUND' ;
	
--Offset any License Deposit Reductions	
insert into #table1
	( RECORD_NO, RECORD_TYPE, ACCOUNT, QUANTITY, CODE, PAID_AMOUNT, PAID_DATE, RECEIPT_NO, CHECK_NO, PAY_METHOD, FEETYPE, DEPOSITID, COLLECTED_BY, PAID_BY)
select
	  RECORD_NO, RECORD_TYPE, ACCOUNT, QUANTITY, CODE, (PAID_AMOUNT*-1), PAID_DATE, 'OFFSET', CHECK_NO, 'OFFSET', 'OFFSET', DEPOSITID, COLLECTED_BY, PAID_BY
from #table1 
	where RECORD_TYPE = 'LICENSE'
	and DEPOSITID in 
		(select RECORDID from License2_Fees 
			where LICENSE_NO = #table1.RECORD_NO 
			and FEETYPE = 'DEPOSIT')
	and FEETYPE <> 'REFUND' ;
	
--Offset any AEC Deposit Reductions	
insert into #table1
	( RECORD_NO, RECORD_TYPE, ACCOUNT, QUANTITY, CODE, PAID_AMOUNT, PAID_DATE, RECEIPT_NO, CHECK_NO, PAY_METHOD, FEETYPE, DEPOSITID, COLLECTED_BY, PAID_BY)
select
	  RECORD_NO, RECORD_TYPE, ACCOUNT, QUANTITY, CODE, (PAID_AMOUNT*-1), PAID_DATE, 'OFFSET', CHECK_NO, 'OFFSET', 'OFFSET', DEPOSITID, COLLECTED_BY, PAID_BY
from #table1 
	where RECORD_TYPE = 'AEC'
	and DEPOSITID in 
		(select RECORDID from AEC_Fees 
			where ST_LIC_NO = #table1.RECORD_NO 
			and FEETYPE = 'DEPOSIT')
	and FEETYPE <> 'REFUND' ;

--Set Parent Descriptions------------------------------------------------------------------
-- Permit Trak
update #table1 set PARENT_DESCRIPTION = 
	(select max(DESCRIPTION) from Permit_Fees where PERMIT_NO = #table1.RECORD_NO
		and PERMIT_NO = @PERMIT_NO 
		and CODE = #table1.CODE)
	where RECORD_TYPE = 'PERMIT' ;

-- Project Trak
update #table1 set PARENT_DESCRIPTION = 
	(select max(DESCRIPTION) from Project_Fees where PROJECT_NO = #table1.RECORD_NO
		and PROJECT_NO = @PROJECT_NO 
		and CODE = #table1.CODE)
	where RECORD_TYPE = 'PROJECT' ;

-- Code Trak
update #table1 set PARENT_DESCRIPTION = 
	(select max(DESCRIPTION) from Case_Fees where CASE_NO = #table1.RECORD_NO
		and CASE_NO = @CASE_NO
		and CODE = #table1.CODE)
	where RECORD_TYPE = 'CASE' ;

-- License Trak
update #table1 set PARENT_DESCRIPTION = 
	(select max(DESCRIPTION) from License2_Fees where LICENSE_NO = #table1.RECORD_NO
		and LICENSE_NO = @LICENSE_NO 
		and CODE = #table1.CODE)
	where RECORD_TYPE = 'LICENSE' ;

-- AEC Trak
update #table1 set PARENT_DESCRIPTION = 
	(select max(DESCRIPTION) from AEC_Fees where ST_LIC_NO = #table1.RECORD_NO
		and ST_LIC_NO = @ST_LIC_NO
		and CODE = #table1.CODE)
	where RECORD_TYPE = 'AEC' ;


--THIS SELECT NEEDS TO BE HERE FOR SSRS TO GET DATA------------------------------------------
select * from #table1 ;
---------------------------------------------------------------------------------------------
END










GO
