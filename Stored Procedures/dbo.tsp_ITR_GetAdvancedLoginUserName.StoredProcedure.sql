USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ITR_GetAdvancedLoginUserName]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_ITR_GetAdvancedLoginUserName]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ITR_GetAdvancedLoginUserName]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  
					  					  					  /*
tsp_ITR_GetAdvancedLoginUserName @T9UserID='mdp'

*/
-- =====================================================================
-- Revision History:
--	RR - 10/28/13
--	mdm - 11/5/13 - added revision history	
-- =====================================================================  
CREATE Procedure [dbo].[tsp_ITR_GetAdvancedLoginUserName](
@T9UserID Varchar(50)
)

As

Select UserID From Prmry_Users Where t9UserID = @T9UserID












GO
