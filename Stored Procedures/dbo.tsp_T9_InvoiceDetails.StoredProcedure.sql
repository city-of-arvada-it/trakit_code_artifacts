USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_InvoiceDetails]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_InvoiceDetails]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_InvoiceDetails]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================================================  

-- Author:		Rance Richardson
-- Create date: 12/19/2013
-- Description:	used to get records for Invoice Details window 
-- Change History:
--	Date:		Updated by:		Description of Change:
--	01/21/14	RRR				Added FEETYPE and DETAIL columns to InvoiceFees temp table
--  02/03/16	RRR				Changed parameter name to @InvoiceNumbers and made procedure dynamic
--								Fixed problem with Invoice_Main update, forced to use a cursor
--  07/12/2018  AG				Increased PAID_BY varchar(30) to 60 to be inline with the rest of TRAKiT
--  04/04/2019	AU				Increased ACTNO varchar(15) to 30 to be inline with the rest of Community Development. Changed @selectIn from NVCHAR(4000) to varchar(MAX). Changed @InvNo to 30
-- =====================================================================================================
CREATE procedure [dbo].[tsp_T9_InvoiceDetails]
@InvoiceNumbers as varchar(MAX)
as
BEGIN
DECLARE @SQL As varchar(MAX)
DECLARE @selectIn as varchar(MAX)

SET @InvoiceNumbers = CHAR(39) + REPLACE(@InvoiceNumbers, ',', ''',''') + CHAR(39)
SET @selectIn = 'select InvoiceNumber from Invoice_Main where InvoiceNumber IN (' + @InvoiceNumbers + ')'


-- RB CHeck before creating table
IF OBJECT_ID('tempdb..#InvoiceFees') IS NOT NULL  
	drop table #InvoiceFees

create table #InvoiceFees (INVOICE_NO varchar(300),MODULE varchar(10),ACTNO varchar(30),[DESCRIPTION] varchar(60),FEETYPE varchar(8),PAID_AMOUNT DECIMAL(14,2),
AMOUNT DECIMAL(14,2),DETAIL varchar(1), PAY_METHOD varchar(20),CHECK_NO varchar(20), PAID_DATE varchar(12), PAID_BY varchar(60),RECORDID varchar(30))


-- RB CHeck before creating table
IF OBJECT_ID('tempdb..#Invoices') IS NOT NULL  
	drop table #Invoices

create table #Invoices(InvoiceNumber varchar(30),InvoiceDate datetime,CreatedBy varchar(60),InvoiceTotal money, InvoicePaid money,InvoiceDue money)

-- RB CHeck before creating table
IF OBJECT_ID('tempdb..#InvoiceActivities') IS NOT NULL  
	drop table #InvoiceActivities

create table #InvoiceActivities(INVOICE_NO varchar(30), ACTNO varchar(30), MODULE varchar(10), AMOUNT DECIMAL(14,2))

SET @SQL = 'insert into #InvoiceFees
			select INVOICE_NO,''PROJECT'' as MODULE,PROJECT_NO as ACTNO,[DESCRIPTION],
			FEETYPE,PAID_AMOUNT,AMOUNT,DETAIL,PAY_METHOD,CHECK_NO,CONVERT(varchar(12),PAID_DATE,101) AS PAID_DATE,PAID_BY,RECORDID
			from project_fees
			where INVOICE_NO IN (' + @selectIn + ')

		insert into #InvoiceFees
		select INVOICE_NO,''PERMIT'' as MODULE,PERMIT_NO as ACTNO,[DESCRIPTION],
		FEETYPE,PAID_AMOUNT,AMOUNT,DETAIL,PAY_METHOD,CHECK_NO,CONVERT(varchar(12),PAID_DATE,101) AS PAID_DATE, PAID_BY,RECORDID
		from permit_fees
		where INVOICE_NO IN (' + @selectIn + ')

		insert into #InvoiceFees
		select INVOICE_NO,''CASE'' as MODULE,CASE_NO as ACTNO,[DESCRIPTION],
		FEETYPE,PAID_AMOUNT,AMOUNT,DETAIL,PAY_METHOD,CHECK_NO,CONVERT(varchar(12),PAID_DATE,101) AS PAID_DATE,PAID_BY,RECORDID
		from case_fees
		where INVOICE_NO IN (' + @selectIn + ')

		insert into #InvoiceFees
		select INVOICE_NO,''LICENSE2'' as MODULE,LICENSE_NO as ACTNO,[DESCRIPTION],
		FEETYPE,PAID_AMOUNT,AMOUNT,DETAIL,PAY_METHOD,CHECK_NO,CONVERT(varchar(12),PAID_DATE,101) AS PAID_DATE,PAID_BY,RECORDID
		from license2_fees
		where INVOICE_NO IN (' + @selectIn +')

		insert into #InvoiceFees
		select INVOICE_NO,''AEC'' as MODULE,ST_LIC_NO as ACTNO,[DESCRIPTION],
		FEETYPE,PAID_AMOUNT,AMOUNT,DETAIL,PAY_METHOD,CHECK_NO,CONVERT(varchar(12),PAID_DATE,101) AS PAID_DATE,PAID_BY,RECORDID
		from aec_fees
		where INVOICE_NO IN (' + @selectIn + ')

		DECLARE @InvNo varchar(30)
		DECLARE Update_Cursor CURSOR FOR
		SELECT InvoiceNumber FROM Invoice_Main WHERE InvoiceNumber IN (' + @SelectIn + ')
		OPEN Update_Cursor;
		FETCH NEXT FROM Update_Cursor INTO @InvNo;
		WHILE @@FETCH_STATUS= 0
		BEGIN
			update Invoice_Main set InvoiceDue = ISNULL((select SUM(AMOUNT) from #InvoiceFees where INVOICE_NO = @InvNo and PAID_AMOUNT = 0 
														 and (PAID_DATE IS NULL or PAID_DATE = '''')), 0.00) 
			where InvoiceNumber = @invNo
			FETCH NEXT FROM Update_Cursor INTO @InvNo
		END
		CLOSE Update_Cursor
		DEALLOCATE Update_Cursor

		insert into #Invoices select InvoiceNumber,InvoiceDate,CreatedBy,InvoiceTotal,InvoicePaid,InvoiceDue from Invoice_Main 
		where InvoiceNumber IN (' + @selectIn + ')

		insert into #InvoiceActivities
		select INVOICE_NO, ACTNO, MODULE, SUM(AMOUNT) as [AMOUNT] from #InvoiceFees group by INVOICE_NO, ACTNO, MODULE

		select * from #Invoices
		select * from #InvoiceActivities
		select * from #InvoiceFees'

--PRINT(@SQL)
EXEC(@SQL)

		drop table #InvoiceFees
		drop table #Invoices
		drop table #InvoiceActivities

END

GO
