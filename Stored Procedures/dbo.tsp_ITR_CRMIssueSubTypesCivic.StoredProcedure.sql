USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ITR_CRMIssueSubTypesCivic]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_ITR_CRMIssueSubTypesCivic]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ITR_CRMIssueSubTypesCivic]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  -- =============================================
-- Author:           Micah Neveu
-- Create date: 10/21/2014
-- Description:      Returns a list of sub types that
--                         have a parent type in the allowed
--                         types of eTRAKiT3_Preferences
--                         for CivicTRAK
-- =============================================
CREATE PROCEDURE [dbo].[tsp_ITR_CRMIssueSubTypesCivic]

AS
BEGIN
      SET NOCOUNT ON;

      DECLARE @issueTypesAllowed VARCHAR(300)
      DECLARE @sql VARCHAR(3000)
      SET @issueTypesAllowed =  (SELECT TOP 1 PREF_VALUE FROM Etrakit3_Preferences WHERE PREF_NAME = 'CRM_IssueTypesAllowed')

      IF LEN(@issueTypesAllowed) > 0
      BEGIN
            SET @sql = 'SELECT * FROM CRM_IssueTypeSubTypesInfo WHERE issue_type IN (' + (@issueTypesAllowed) + ')'
      END
      ELSE
            SET @sql = 'SELECT * FROM CRM_IssueTypeSubTypesInfo'

      EXEC(@sql)

END


GO
