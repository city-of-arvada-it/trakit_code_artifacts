USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GblCRMLinksTreeActivities]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_GblCRMLinksTreeActivities]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GblCRMLinksTreeActivities]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[tsp_GblCRMLinksTreeActivities](
      @ActivityNo varchar(30),
	  @Group	  varchar(15)
     
)

As
 BEGIN

 Declare @SqlStatement Varchar(550) 

 Set @SqlStatement = null

 if (@ActivityNo IS NOT null and @ActivityNo <> '') and (@Group is not null and @Group <> '')
	Begin 
		Select CI.ISSUE_ID,CI.ISSUE_LABEL, CI.RECORDID,ISNULL(CONVERT(VARCHAR,CI.DUE_DATE, 101),'') as DUE_DATE, ISNULL(CONVERT(VARCHAR,CI.COMPLETED_DATE, 101),'') as COMPLETED_DATE, 
		    CONVERT(VARCHAR(50), CI.created_datetime, 101) AS CREATED_DATE, CONVERT(VARCHAR(50), CI.last_update_date, 101) AS LAST_UPD_DATE,
		    cl1.list_text AS [TYPE], CI.ISSUE_SUBTYPE as [SUBTYPE], CI.title as [DESCRIPTION], cl2.list_text AS [STATUS],
			CI.issue_address AS SITE_ADDR
		 from CRM_Issues CI LEFT JOIN
	        dbo.geo_ownership ON dbo.geo_ownership.recordid = CI.issue_loc_recordid LEFT JOIN
            dbo.crm_lists cl1 ON CI.naturetype_list_id = cl1.list_id LEFT JOIN
            dbo.crm_lists cl2 ON CI.status_list_id = cl2.list_id	 
		 where CI.ISSUE_ID in (select ISSUE_ID from CRM_Links    where LINK_GROUP = @Group and LINK_NO = @ActivityNo ) order by CREATED_DATETIME DESC

	END
END




GO
