USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearch_ExecuteQuery]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_AdvSearch_ExecuteQuery]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearch_ExecuteQuery]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[tsp_T9_AdvSearch_ExecuteQuery] (
	@PageNumber int = 1,
	@PageSize int = 50,
	@SelectClause varchar(max) = null,
	@SelectAlias varchar(max) = '',
	@FromClause varchar(max) = null,
	@WhereClause varchar(max) = null,
	@GroupByClause varchar(max) = null,
	@OrderByClause varchar(max) = '["Key Field"]', -- specify [alias] here
	@OrderByInOver varchar(max) = '', -- specify [table].[column] here
	@ReturnHTML bit = 1,
	@IncludeHyperlinkColumns bit = 1,
	@Group varchar(50) = 'PERMIT'
)

AS
BEGIN

--print(convert(varchar,getdate(),109)) -- EWH: used for measuring the duration of this proc



if isnull(@WhereClause,'') = '' set @WhereClause = '1=1' -- do we need this?



-- --------------------------------------------------------------------------------------------------------
-- calculate the range of row numbers
-- --------------------------------------------------------------------------------------------------------
if @PageNumber < 1 set @PageNumber = 1
if @PageSize < 1 set @PageSize = 50

declare @fr bigint
declare @lr bigint
set @fr = @PageSize * (@PageNumber - 1) + 1
set @lr = @fr + @PageSize - 1

declare @firstrow varchar(10)
declare @lastrow varchar(10)
set @firstrow = cast(@fr as varchar)
set @lastrow = cast(@lr as varchar)
-- --------------------------------------------------------------------------------------------------------

--print @firstrow
--print @lastrow

-- --------------------------------------------------------------------------------------------------------
-- update the clauses as necessary
-- --------------------------------------------------------------------------------------------------------
if @WhereClause <> '' set @WhereClause = ' WHERE ' + @WhereClause
if @GroupByClause <> '' set @GroupByClause = ' GROUP BY ' + @GroupByClause
-- --------------------------------------------------------------------------------------------------------


-- =================================
--   compile and execute the query
-- =================================
declare @sql nvarchar(max)
set @sql=''

if @ReturnHTML = 0
begin
	-- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	-- compile query to return a table
	-- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	declare @HyperLinkColumnData varchar(500)
	set @HyperLinkColumnData = ''
	if (@IncludeHyperlinkColumns = 1) set @HyperLinkColumnData = '(select ' + '''' + @Group + '''' +  ') [GroupName], '

	set @sql = 'SELECT ' + @HyperLinkColumnData + @SelectAlias + ' FROM (SELECT ' + ' ROW_NUMBER() OVER(ORDER BY ' + @OrderByInOver + ') as ["row number"], ' + @SelectClause + ' FROM ' + @FromClause 
			+ @WhereClause + @GroupByClause + ') a WHERE ["row number"] between ' + @firstrow + ' and ' + @lastrow  -- + ' ORDER BY ' + @OrderByClause
	-- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
end
else
begin
	if @ReturnHTML = 1
	begin
		-- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		-- create select clause for use in html
		-- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		declare @SelectClauseAsTd varchar(max)
		if (@IncludeHyperlinkColumns = 1) 
			begin 

				 -- create the trakit item link column --> example: <img src="groupImg.ashx?s=16&amp;g=PROJECT" onclick="ti('PROJECT','PLUM1410-000028');" />
				 -- --------------------------------------------------------------------------------------------------------------------------------------------
				declare @s1 varchar(1000)
			
				set @s1 = '(select ' + '''groupImg.ashx?s=16&g='+ '' +  @Group + ''  +'''' + ' "@src", ' 
							+ '''ti('''''+ @Group +''''',''''' + ''' + ' + 'cast (["Key Field"] as nvarchar)' + ' + ''' + ''''');''' + ' "@onclick" for xml path(''img''), type) td, '
						-- TODO: title
					

				-- create the GIS link column --> example: <a id="PROJECT|PLUM1410-000028" /> 
				-- ---------------------------------------------------------------------------
				declare @s2 varchar(1000)
				set @s2 = '(select ''' + @Group +'|' + ''' + ' + 'cast(["Key Field"] as nvarchar) "@id" for xml path(''a''), type) td, ' 
		
		
				--images/permit_16.png
				print @s1
				print @s2

				set @SelectClauseAsTd = @s1 + @s2 + REPLACE(@SelectAlias, ',', ' td,') + ' td'
				print @SelectClauseAsTd


			end
		else
			begin
				-- just format the select alias string
				set @SelectClauseAsTd = REPLACE(@SelectAlias, ',', ' td,') + ' as td'
			end


			
		IF (@OrderByClause = '')
			set @sql = 'SELECT ' + @SelectClauseAsTd + ' FROM (SELECT ROW_NUMBER() OVER(ORDER BY ' + @OrderByInOver + ') as ["row number"], ' + @SelectClause + ' FROM ' + @FromClause 
					+ @WhereClause + @GroupByClause + ') a WHERE ["row number"] between ' + @firstrow + ' and ' + @lastrow + ' for xml raw(''tr''), elements xsinil, type'
		ELSE
			set @sql = 'SELECT ' + @SelectClauseAsTd + ' FROM (SELECT ROW_NUMBER() OVER(ORDER BY ' + @OrderByInOver + ') as ["row number"], ' + @SelectClause + ' FROM ' + @FromClause 
					+ @WhereClause + @GroupByClause + ') a WHERE ["row number"] between ' + @firstrow + ' and ' + @lastrow + ' ORDER BY ' + @OrderByClause + ' for xml raw(''tr''), elements xsinil, type'  

		-- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	end
end

-- ======================================
--   the magic
-- ======================================
print @sql
if @sql <> '' exec sp_executesql @sql 
-- ======================================


--print(convert(varchar,getdate(),109)) -- EWH: used for measuring the duration of this proc
END

GO
