USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_e3_LinkedActivities]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_e3_LinkedActivities]
GO
/****** Object:  StoredProcedure [dbo].[tsp_e3_LinkedActivities]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[tsp_e3_LinkedActivities]
	@activitygroup AS VARCHAR(30), 
	@activityno AS VARCHAR(30), 
	@loginascontractor AS int,
	@contractorStLicNo AS varchar(30),
	@activeonly AS int,
	@loginstate as int = 0,
	@trakitversion as int = 0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--should be removed-----------
--DECLARE @activitygroup AS VARCHAR(30) 
--DECLARE @activityno AS VARCHAR(30) 
--DECLARE @loginascontractor as int
--DECLARE @contractorStLicNo as varchar(30)
--SET @loginascontractor = 1
--set @activitygroup = 'AEC'
--set @contractorStLicNo = 'AEC-02020'
--set @activityno = 'AEC-02020'

------------------------------


DECLARE @Perm_ContractorFilter as VARCHAR(5) 
DECLARE @Perm_SearchNameTypeContractorOnly AS VARCHAR(5)
DECLARE @Proj_ContractorFilter as VARCHAR(5) 
DECLARE @Proj_SearchNameTypeContractorOnly AS VARCHAR(5)
DECLARE @Lic_ContractorFilter as VARCHAR(5) 
DECLARE @Lic2_ContractorFilter as VARCHAR(5) 
DECLARE @ShowPermitSearch AS VARCHAR(5)
DECLARE @ShowProjectSearch AS VARCHAR(5)
DECLARE @ShowCaseSearch AS VARCHAR(5)
DECLARE @ShowAECSearch AS VARCHAR(5)
DECLARE @ShowLicenseSearch AS VARCHAR(5)
DECLARE @Perm_RequireLogin AS  VARCHAR(5)
DECLARE @Perm_LoginSecurity AS VARCHAR(30)
DECLARE @Proj_RequireLogin AS  VARCHAR(5)
DECLARE @Proj_LoginSecurity AS VARCHAR(30)
DECLARE @Case_RequireLogin AS  VARCHAR(5)
DECLARE @Case_LoginSecurity AS VARCHAR(30)
DECLARE @AEC_RequireLogin AS  VARCHAR(5)
DECLARE @AEC_LoginSecurity AS VARCHAR(30)
DECLARE @Lic_RequireLogin AS  VARCHAR(5)
DECLARE @Lic_LoginSecurity AS VARCHAR(30)
DECLARE @Lic2_RequireLogin AS  VARCHAR(5)
DECLARE @Lic2_LoginSecurity AS VARCHAR(30)
DECLARE @contractorfilteringpermitquery as VARCHAR(MAX)

DECLARE @parentpermit AS VARCHAR(30)
DECLARE @parentproject AS VARCHAR(30)
DECLARE @parentlicense AS VARCHAR(30)
DECLARE @parentlicense2 AS VARCHAR(30)
DECLARE @linkedcase as VARCHAR(30)
DECLARE @parentaec AS VARCHAR(30)


DECLARE @Perm_Search_Filter AS VARCHAR(MAX)
DECLARE @Proj_Search_Filter AS VARCHAR(MAX)
DECLARE @Case_Search_Filter AS VARCHAR(MAX)
DECLARE @AEC_Search_Filter AS VARCHAR(MAX)
DECLARE @Lic_Search_Filter AS VARCHAR(MAX)
DECLARE @Lic2_Search_Filter AS VARCHAR(MAX)


DECLARE @SQLString AS  NVARCHAR(max)

--should be removed-----------
------------------------------

SET @contractorfilteringpermitquery = ''
SET @Perm_ContractorFilter = 'FALSE'
SET @Proj_ContractorFilter = 'FALSE'
SET @Lic_ContractorFilter = 'FALSE'
SET @Lic2_ContractorFilter = 'FALSE'

SET @Perm_Search_Filter = ISNULL((SELECT UPPER(ISNULL(pref_value, '')) FROM etrakit3_preferences WHERE pref_name = 'Perm_Search_Filter'), '')
IF @Perm_Search_Filter <> ''
	SET @Perm_Search_Filter  = ' AND ' + @Perm_Search_Filter
SET @Proj_Search_Filter = ISNULL((SELECT UPPER(ISNULL(pref_value, '')) FROM etrakit3_preferences WHERE pref_name = 'Proj_Search_Filter'), '')
IF @Proj_Search_Filter <> ''
	SET @Proj_Search_Filter = ' AND ' + @Proj_Search_Filter
SET @Case_Search_Filter = ISNULL((SELECT UPPER(ISNULL(pref_value, '')) FROM etrakit3_preferences WHERE pref_name = 'Case_Search_Filter'), '')
IF @Case_Search_Filter <> ''
	SET @Case_Search_Filter = ' AND ' + @Case_Search_Filter	
SET @AEC_Search_Filter = ISNULL((SELECT UPPER(ISNULL(pref_value, '')) FROM etrakit3_preferences WHERE pref_name = 'AEC_Search_Filter'), '')
IF @AEC_Search_Filter <> ''
	SET @AEC_Search_Filter = ' AND ' + @AEC_Search_Filter
SET @Lic_Search_Filter = ISNULL((SELECT UPPER(ISNULL(pref_value, '')) FROM etrakit3_preferences WHERE pref_name = 'Lic_Search_Filter'), '')
IF @Lic_Search_Filter <> ''
	SET @Lic_Search_Filter = ' AND ' + @Lic_Search_Filter
SET @Lic2_Search_Filter = ISNULL((SELECT UPPER(ISNULL(pref_value, '')) FROM etrakit3_preferences WHERE pref_name = 'Lic2_Search_Filter'), '')
if @Lic2_Search_Filter <> ''
	SET @Lic2_Search_Filter = ' AND ' + @Lic2_Search_Filter

-- CHECK IF ALLOW TO SEARCH FOR EACH MODULE
SET @ShowPermitSearch = ISNULL((SELECT UPPER(ISNULL(pref_value, 'FALSE')) FROM etrakit3_preferences WHERE pref_name = 'ShowPermitSearch'), 'FALSE')
SET @ShowProjectSearch = ISNULL((SELECT UPPER(ISNULL(pref_value, 'FALSE')) FROM etrakit3_preferences WHERE pref_name = 'ShowProjectSearch'), 'FALSE')
SET @ShowCaseSearch = ISNULL((SELECT UPPER(ISNULL(pref_value, 'FALSE')) FROM etrakit3_preferences WHERE pref_name = 'ShowCaseSearch'), 'FALSE')
SET @ShowAECSearch = ISNULL((SELECT UPPER(ISNULL(pref_value, 'FALSE')) FROM etrakit3_preferences WHERE pref_name = 'ShowAECSearch'), 'FALSE')
SET @ShowLicenseSearch = ISNULL((SELECT UPPER(ISNULL(pref_value, 'FALSE')) FROM etrakit3_preferences WHERE pref_name = 'ShowLicenseSearch'), 'FALSE')



-- CHECK LOGIN SECURITY
-- 0 = anonymous user
-- 1 = public user
-- 2 = contractor
-- 3 = trakituser

--- CHECK LOGIN SECURITY FOR PERMIT
IF @ShowPermitSearch = 'TRUE'
BEGIN
	SET @Perm_RequireLogin = ISNULL((SELECT UPPER(ISNULL(pref_value, 'FALSE')) FROM etrakit3_preferences WHERE pref_name = 'Perm_RequireLogin'), 'FALSE')

	IF @loginstate <> 0 AND @Perm_RequireLogin = 'TRUE'
	BEGIN
		SET @Perm_LoginSecurity = UPPER(ISNULL((SELECT UPPER(ISNULL(pref_value, 'FALSE')) FROM etrakit3_preferences WHERE pref_name = 'Perm_LoginSecurity'), ''))	

		IF @Perm_LoginSecurity <> 'EITHER' AND @Perm_LoginSecurity <> ''
		BEGIN
			IF @loginstate = 1 AND 	@Perm_LoginSecurity <> 'REQUIRE SELF-REGISTRATION'		
			BEGIN
				SET @ShowPermitSearch = 'FALSE'
			END 
			IF @loginstate = 2 AND 	@Perm_LoginSecurity <> 'REQUIRE CONTRACTOR LOGIN'		
			BEGIN
				SET @ShowPermitSearch = 'FALSE'
			END 
		END 
	END
	IF @loginstate = 0 AND @Perm_RequireLogin = 'TRUE'
	BEGIN
		SET @ShowPermitSearch = 'FALSE'
	END 
END

--- CHECK LOGIN SECURITY FOR PROJECT
IF @ShowProjectSearch = 'TRUE'
BEGIN
	SET @Proj_RequireLogin = ISNULL((SELECT UPPER(ISNULL(pref_value, 'FALSE')) FROM etrakit3_preferences WHERE pref_name = 'Proj_RequireLogin'), 'FALSE')

	IF @loginstate <> 0 AND @Proj_RequireLogin = 'TRUE'
	BEGIN
		SET @Proj_LoginSecurity = UPPER(ISNULL((SELECT UPPER(ISNULL(pref_value, 'FALSE')) FROM etrakit3_preferences WHERE pref_name = 'Proj_LoginSecurity'), ''))	

		IF @Proj_LoginSecurity <> 'EITHER' AND @Proj_LoginSecurity <> ''
		BEGIN
			IF @loginstate = 1 AND 	@Proj_LoginSecurity <> 'REQUIRE SELF-REGISTRATION'		
			BEGIN
				SET @ShowProjectSearch = 'FALSE'
			END 
			IF @loginstate = 2 AND 	@Proj_LoginSecurity <> 'REQUIRE CONTRACTOR LOGIN'		
			BEGIN
				SET @ShowProjectSearch = 'FALSE'
			END 
		END 
	END
	IF @loginstate = 0 AND @Proj_RequireLogin = 'TRUE'
	BEGIN
		SET @ShowProjectSearch = 'FALSE'
	END 
END

--- CHECK LOGIN SECURITY FOR CASE
IF @ShowCaseSearch = 'TRUE'
BEGIN
	SET @Case_RequireLogin = ISNULL((SELECT UPPER(ISNULL(pref_value, 'FALSE')) FROM etrakit3_preferences WHERE pref_name = 'Case_RequireLogin'), 'FALSE')

	IF @loginstate <> 0 AND @Case_RequireLogin = 'TRUE'
	BEGIN
		SET @Case_LoginSecurity = UPPER(ISNULL((SELECT UPPER(ISNULL(pref_value, 'FALSE')) FROM etrakit3_preferences WHERE pref_name = 'Case_LoginSecurity'), ''))	
		
		IF @Case_LoginSecurity <> 'EITHER' AND @Case_LoginSecurity <> ''
		BEGIN
			IF @loginstate = 1 AND 	@Case_LoginSecurity <> 'REQUIRE SELF-REGISTRATION'		
			BEGIN
				SET @ShowCaseSearch = 'FALSE'
			END 
			IF @loginstate = 2 AND 	@Case_LoginSecurity <> 'REQUIRE CONTRACTOR LOGIN'		
			BEGIN
				SET @ShowCaseSearch = 'FALSE'
			END 
		END 
	END
	IF @loginstate = 0 AND @Case_RequireLogin = 'TRUE'
	BEGIN
		SET @ShowCaseSearch = 'FALSE'
	END 
END

--- CHECK LOGIN AEC
IF @ShowAECSearch = 'TRUE'
BEGIN
	SET @AEC_RequireLogin = ISNULL((SELECT UPPER(ISNULL(pref_value, 'FALSE')) FROM etrakit3_preferences WHERE pref_name = 'AEC_RequireLogin'), 'FALSE')

	IF @loginstate <> 0 AND @AEC_RequireLogin = 'TRUE'
	BEGIN
		SET @AEC_LoginSecurity = UPPER(ISNULL((SELECT UPPER(ISNULL(pref_value, 'FALSE')) FROM etrakit3_preferences WHERE pref_name = 'AEC_LoginSecurity'), ''))	
		
		IF @AEC_LoginSecurity <> 'EITHER' AND @AEC_LoginSecurity <> ''
		BEGIN
			IF @loginstate = 1 AND 	@AEC_LoginSecurity <> 'REQUIRE SELF-REGISTRATION'		
			BEGIN
				SET @ShowAECSearch = 'FALSE'
			END 
			IF @loginstate = 2 AND 	@AEC_LoginSecurity <> 'REQUIRE CONTRACTOR LOGIN'		
			BEGIN
				SET @ShowAECSearch = 'FALSE'
			END 
		END 
	END
	IF @loginstate = 0 AND @AEC_RequireLogin = 'TRUE'
	BEGIN
		SET @ShowAECSearch = 'FALSE'
	END 
END

--- CHECK LOGIN LICENSE 
IF @ShowLicenseSearch = 'TRUE' AND @trakitversion = 0 
BEGIN
	SET @Lic_RequireLogin = ISNULL((SELECT UPPER(ISNULL(pref_value, 'FALSE')) FROM etrakit3_preferences WHERE pref_name = 'Lic_RequireLogin'), 'FALSE')

	IF @loginstate <> 0 AND @Lic_RequireLogin = 'TRUE'
	BEGIN
		SET @Lic_LoginSecurity = UPPER(ISNULL((SELECT UPPER(ISNULL(pref_value, 'FALSE')) FROM etrakit3_preferences WHERE pref_name = 'Lic_LoginSecurity'), ''))	
		
		IF @Lic_LoginSecurity <> 'EITHER' AND @Lic_LoginSecurity <> ''
		BEGIN
			IF @loginstate = 1 AND 	@Lic_LoginSecurity <> 'REQUIRE SELF-REGISTRATION'		
			BEGIN
				SET @ShowLicenseSearch = 'FALSE'
			END 
			IF @loginstate = 2 AND 	@Lic_LoginSecurity <> 'REQUIRE CONTRACTOR LOGIN'		
			BEGIN
				SET @ShowLicenseSearch = 'FALSE'
			END 
		END 
	END
	IF @loginstate = 0 AND @Lic_RequireLogin = 'TRUE'
	BEGIN
		SET @ShowLicenseSearch = 'FALSE'
	END 
END

--- CHECK LOGIN LICENSE2 
IF @ShowLicenseSearch = 'TRUE' AND @trakitversion = 9 
BEGIN
	SET @Lic2_RequireLogin = ISNULL((SELECT UPPER(ISNULL(pref_value, 'FALSE')) FROM etrakit3_preferences WHERE pref_name = 'Lic2_RequireLogin'), 'FALSE')

	IF @loginstate <> 0 AND @Lic_RequireLogin = 'TRUE'
	BEGIN
		SET @Lic2_LoginSecurity = UPPER(ISNULL((SELECT UPPER(ISNULL(pref_value, 'FALSE')) FROM etrakit3_preferences WHERE pref_name = 'Lic2_LoginSecurity'), ''))	
		
		IF @Lic2_LoginSecurity <> 'EITHER' AND @Lic2_LoginSecurity <> ''
		BEGIN
			IF @loginstate = 1 AND 	@Lic2_LoginSecurity <> 'REQUIRE SELF-REGISTRATION'		
			BEGIN
				SET @ShowLicenseSearch = 'FALSE'
			END 
			IF @loginstate = 2 AND 	@Lic2_LoginSecurity <> 'REQUIRE CONTRACTOR LOGIN'		
			BEGIN
				SET @ShowLicenseSearch = 'FALSE'
			END 
		END 
	END
	IF @loginstate = 0 AND @Lic2_RequireLogin = 'TRUE'
	BEGIN
		SET @ShowLicenseSearch = 'FALSE'
	END 
END


IF @loginascontractor = 1 
BEGIN
	SET @Perm_ContractorFilter = ISNULL((SELECT UPPER(ISNULL(pref_value, 'FALSE')) FROM etrakit3_preferences WHERE pref_name = 'Perm_ContractorFilter'), 'FALSE')
	SET @Perm_SearchNameTypeContractorOnly = ISNULL((SELECT UPPER(ISNULL(pref_value, 'FALSE')) FROM etrakit3_preferences WHERE pref_name = 'Perm_SearchNameTypeContractorOnly'), 'FALSE')

	SET @Proj_ContractorFilter = ISNULL((SELECT UPPER(ISNULL(pref_value, 'FALSE')) FROM etrakit3_preferences WHERE pref_name = 'Proj_ContractorFilter'), 'FALSE')
	SET @Proj_SearchNameTypeContractorOnly = ISNULL((SELECT UPPER(ISNULL(pref_value, 'FALSE')) FROM etrakit3_preferences WHERE pref_name = 'Proj_SearchNameTypeContractorOnly'), 'FALSE')

	SET @Lic_ContractorFilter = ISNULL((SELECT UPPER(ISNULL(pref_value, 'FALSE')) FROM etrakit3_preferences WHERE pref_name = 'Lic_ContractorFilter'), 'FALSE')
	SET @Lic2_ContractorFilter = ISNULL((SELECT UPPER(ISNULL(pref_value, 'FALSE')) FROM etrakit3_preferences WHERE pref_name = 'Lic2_ContractorFilter'), 'FALSE')
END  
ELSE
BEGIN
	SET @Perm_ContractorFilter = 'FALSE'
	SET @Perm_SearchNameTypeContractorOnly = 'FALSE'

	SET @Proj_ContractorFilter = 'FALSE'
	SET @Proj_SearchNameTypeContractorOnly = 'FALSE'

	SET @Lic_ContractorFilter = 'FALSE'
	SET @Lic2_ContractorFilter = 'FALSE'
END 


DECLARE @permitquery AS VARCHAR(max) 

IF @activitygroup = 'PERMIT' 
  BEGIN 

  SET @parentpermit = ISNULL((SELECT ISNULL(PARENT_PERMIT_NO, '') FROM permit_main WHERE permit_no = @activityno), '')
  SET @parentproject = ISNULL((SELECT ISNULL(PARENT_PROJECT_NO, '') FROM permit_main WHERE permit_no = @activityno), '')
  SET @parentlicense = ISNULL((SELECT ISNULL(PARENT_BUS_LIC_NO, '') FROM permit_main WHERE permit_no = @activityno), '')
    
  SET @SQLString = '
		  SELECT 1                  AS sortorder, 
				 ''PARENT-PROJECT'' AS category, 
				 pr.project_no      AS activityno, 
				 pr.projecttype     AS type, 
				 pr.status          AS status, 
				 pr.applied         AS applied
		  FROM   project_main pr
		  WHERE  pr.project_no = ''' + @parentproject + '''  AND (''' + @ShowProjectSearch + ''' = ''TRUE'' OR (''' + @ShowProjectSearch + ''' = ''FALSE'' AND 1=0))  AND (''' + @Proj_ContractorFilter + ''' = ''TRUE'' AND (pr.PROJECT_no IN (SELECT DISTINCT Project_People.project_no FROM Project_People WHERE (ID = ''C:' + @contractorStLicNo + ''') AND ((''' + @Proj_SearchNameTypeContractorOnly + ''' = ''TRUE'' AND NAMETYPE = ''CONTRACTOR'') OR ''' + @Proj_SearchNameTypeContractorOnly + ''' = ''FALSE''))) OR ''' + @Proj_ContractorFilter + ''' = ''FALSE'')' + @Proj_Search_Filter + '
		  UNION ALL 
		  SELECT 2                 AS sortorder, 
				 ''PARENT-PERMIT'' AS category, 
				 pm2.permit_no     AS activityno, 
				 pm2.permittype    AS type,  
				 pm2.status        AS status, 
				 pm2.applied       AS applied
		  FROM   permit_main pm2
		  WHERE  pm2.permit_no = ''' + @parentpermit + ''' AND (''' + @ShowPermitSearch + ''' = ''TRUE'' OR (''' + @ShowPermitSearch + ''' = ''FALSE'' AND 1=0)) AND (''' + @Perm_ContractorFilter + ''' = ''TRUE'' AND (pm2.PERMIT_NO IN (SELECT DISTINCT Permit_People.PERMIT_NO FROM Permit_People WHERE (ID = ''C:' + @contractorStLicNo + ''') AND ((''' + @Perm_SearchNameTypeContractorOnly + ''' = ''TRUE'' AND NAMETYPE = ''CONTRACTOR'') OR ''' + @Perm_SearchNameTypeContractorOnly + ''' = ''FALSE''))) OR  ''' + @Perm_ContractorFilter + ''' = ''FALSE'') ' + @Perm_Search_Filter + '
		  UNION ALL 
		  SELECT 3							AS sortorder, 
				 ''PARENT-LICENSE''			AS category, 
				 lm.bus_lic_no				AS activityno, 
				 lb.business_type			AS type, 
				 lb.LOC_ADDRESS1			AS status, 
				 lm.bus_lic_iss				AS applied
		  FROM ( SELECT * FROM license_main WHERE 1=1 ' + @Lic_Search_Filter + ' ) AS lm 
				 INNER JOIN license_business lb 
						 ON lb.business_no = lm.business_no 
		  WHERE  lm.bus_lic_no = ''' + @parentlicense + ''' AND (''' + @Lic_ContractorFilter + ''' = ''TRUE'' AND lb.business_no IN (SELECT DISTINCT license_People.business_no FROM license_People WHERE (ID = ''C:' + @contractorStLicNo + ''')) OR '''+ @Lic_ContractorFilter + ''' = ''FALSE'')
				 AND (''' + @ShowLicenseSearch + ''' = ''TRUE'' OR ('''+ @ShowLicenseSearch + ''' = ''FALSE'' AND 1=0)) 
		  UNION ALL 
		  SELECT 3                   AS sortorder, 
				 ''PARENT-LICENSE2'' AS category, 
				 lm.license_no       AS activityno, 
				 lm.license_type     AS type, 
				 lm.status           AS status, 
				 lm.applied          AS applied 
		  FROM   permit_main pm 
				 INNER JOIN ( SELECT * FROM license2_main WHERE 1=1 ' + @Lic2_Search_Filter + ' ) AS lm 
						 ON lm.license_no = pm.parent_bus_lic_no AND (''' + @Lic2_ContractorFilter + ''' = ''TRUE'' AND lm.license_no IN (SELECT DISTINCT license2_People.license_no FROM license2_People WHERE (ID = ''C:' + @contractorStLicNo + ''')) OR ''' + @Lic2_ContractorFilter + ''' = ''FALSE'')
		  WHERE  pm.permit_no = ''' + @activityno + '''
				 AND (''' + @ShowLicenseSearch + ''' = ''TRUE'' OR (''' + @ShowLicenseSearch + ''' = ''FALSE'' AND 1=0)) 
		  UNION ALL 
		  SELECT 5              AS sortorder, 
				 ''SUB-PERMIT'' AS category, 
				 permit_no      AS activityno, 
				 permittype     AS type, 
				 status         AS status, 
				 pm.applied     AS applied
		  FROM   permit_main pm 
		  WHERE  pm.parent_permit_no = ''' + @activityno + ''' AND (''' + @Perm_ContractorFilter + ''' = ''TRUE'' AND (pm.PERMIT_NO IN (SELECT DISTINCT Permit_People.PERMIT_NO FROM Permit_People WHERE (ID = ''C:' + @contractorStLicNo + ''') AND ((''' + @Perm_SearchNameTypeContractorOnly + ''' = ''TRUE'' AND NAMETYPE = ''CONTRACTOR'') OR ''' + @Perm_SearchNameTypeContractorOnly + ''' = ''FALSE''))) OR ''' + @Perm_ContractorFilter + ''' = ''FALSE'')
				 AND (''' + @ShowPermitSearch + ''' = ''TRUE'' OR (''' + @ShowPermitSearch + ''' = ''FALSE'' AND 1=0)) ' + @Perm_Search_Filter + '
		  UNION ALL 
		  SELECT 6            AS sortorder, 
				 ''SUB-CASE'' AS category, 
				 case_no      AS activityno, 
				 casetype     AS type, 
				 status       AS status, 
				 cm.started   AS applied 
		  FROM   case_main cm 
		  WHERE  cm.parent_permit_no = ''' + @activityno + ''' AND (''' + @ShowCaseSearch + ''' = ''TRUE'' OR (''' + @ShowCaseSearch + ''' = ''FALSE'' AND 1=0)) ' +  @Case_Search_Filter + '
		  ORDER  BY sortorder, 
					applied DESC, 
					activityno DESC' 

	EXECUTE sp_executesql @SQLString
  END 

IF @activitygroup = 'PROJECT' 
  BEGIN 

      SET @parentproject = ISNULL((SELECT ISNULL(PARENT_PROJECT_NO, '') FROM project_main WHERE project_no = @activityno), '')

	  SET @SQLString = '
		  SELECT 1                  AS sortorder, 
				 ''PARENT-PROJECT'' AS category, 
				 pr2.project_no     AS activityno, 
				 pr2.projecttype    AS type, 
				 pr2.status         AS status, 
				 pr2.applied        AS applied 
		  FROM   project_main pr2 
		  WHERE  pr2.project_no = ''' + @parentproject + ''' AND (''' + @ShowProjectSearch + ''' = ''TRUE'' OR (''' + @ShowProjectSearch + ''' = ''FALSE'' AND 1=0)) AND (''' + @Proj_ContractorFilter + ''' = ''TRUE'' AND (pr2.PROJECT_no IN (SELECT DISTINCT Project_People.project_no FROM Project_People WHERE (ID = ''C:' + @contractorStLicNo + ''') AND ((''' + @Proj_SearchNameTypeContractorOnly + ''' = ''TRUE'' AND NAMETYPE = ''CONTRACTOR'') OR ''' + @Proj_SearchNameTypeContractorOnly + ''' = ''FALSE''))) OR ''' + @Proj_ContractorFilter + ''' = ''FALSE'') ' + @Proj_Search_Filter + '
		  UNION ALL 
		  SELECT 2                 AS sortorder, 
				 ''SUB-PROJECT''   AS category, 
				 pr2.project_no    AS activityno, 
				 pr2.projecttype   AS type, 
				 pr2.status        AS status,  
				 pr2.applied       AS applied 
		  FROM   project_main pr2 
		  WHERE  pr2.parent_project_no = ''' + @activityno + ''' AND (''' + @Proj_ContractorFilter + ''' = ''TRUE'' AND (pr2.PROJECT_no IN (SELECT DISTINCT Project_People.project_no FROM Project_People WHERE (ID = ''C:' + @contractorStLicNo + ''') AND ((''' + @Proj_SearchNameTypeContractorOnly + ''' = ''TRUE'' AND NAMETYPE = ''CONTRACTOR'') OR ''' + @Proj_SearchNameTypeContractorOnly + ''' = ''FALSE''))) OR ''' + @Proj_ContractorFilter + ''' = ''FALSE'')
				 AND (''' + @ShowProjectSearch + ''' = ''TRUE'' OR (''' + @ShowProjectSearch + ''' = ''FALSE'' AND 1=0)) ' + @Proj_Search_Filter + '
		  UNION ALL 
		  SELECT 3               AS sortorder, 
				 ''SUB-PERMIT''  AS category, 
				 pm.permit_no    AS activityno, 
				 pm.permittype   AS type, 
				 pm.status       AS status, 
				 pm.applied      AS applied 
		  FROM   permit_main pm 
		  WHERE pm.parent_project_no = ''' + @activityno + ''' AND (''' + @Perm_ContractorFilter + ''' = ''TRUE'' AND (pm.PERMIT_NO IN (SELECT DISTINCT Permit_People.PERMIT_NO FROM Permit_People WHERE (ID = ''C:' + @contractorStLicNo + ''') AND ((''' + @Perm_SearchNameTypeContractorOnly + ''' = ''TRUE'' AND NAMETYPE = ''CONTRACTOR'') OR ''' + @Perm_SearchNameTypeContractorOnly + ''' = ''FALSE''))) OR ''' + @Perm_ContractorFilter + ''' = ''FALSE'')
				 AND (''' + @ShowPermitSearch + ''' = ''TRUE'' OR (''' + @ShowPermitSearch + ''' = ''FALSE'' AND 1=0)) ' + @Perm_Search_Filter + '
		  UNION ALL 
		  SELECT 4            AS sortorder, 
				 ''SUB-CASE'' AS category, 
				 case_no      AS activityno, 
				 casetype     AS type, 
				 status       AS status, 
				 cm.started   AS applied 
		  FROM   case_main cm 
		  WHERE  cm.parent_project_no = ''' + @activityno + ''' AND (''' + @ShowCaseSearch + ''' = ''TRUE'' OR (''' + @ShowCaseSearch + ''' = ''FALSE'' AND 1=0)) ' +  @Case_Search_Filter + '
		  ORDER  BY sortorder, 
					applied DESC, 
					activityno DESC '

	EXECUTE sp_executesql @SQLString
  END 
  
IF @activitygroup = 'CASE' 
  BEGIN 

	SET @parentpermit = ISNULL((SELECT ISNULL(PARENT_PERMIT_NO, '') FROM case_main WHERE case_no = @activityno), '')
	SET @parentproject = ISNULL((SELECT ISNULL(PARENT_PROJECT_NO, '') FROM case_main WHERE case_no = @activityno), '')
	SET @parentlicense = ISNULL((SELECT ISNULL(PARENT_BUS_LIC_NO, '') FROM case_main WHERE case_no = @activityno), '')

	SET @SQLString = '
		  SELECT 1                  AS sortorder, 
				 ''PARENT-PROJECT'' AS category, 
				 pr.project_no      AS activityno, 
				 pr.projecttype     AS type, 
				 pr.status          AS status, 
				 pr.applied         AS applied 
		  FROM   project_main pr 
		  WHERE  pr.project_no = ''' + @parentproject + ''' AND (''' + @Proj_ContractorFilter + ''' = ''TRUE'' AND (pr.PROJECT_no IN (SELECT DISTINCT Project_People.project_no FROM Project_People WHERE (ID = ''C:' + @contractorStLicNo + ''') AND ((''' + @Proj_SearchNameTypeContractorOnly + ''' = ''TRUE'' AND NAMETYPE = ''CONTRACTOR'') OR ''' + @Proj_SearchNameTypeContractorOnly + ''' = ''FALSE''))) OR ''' + @Proj_ContractorFilter + ''' = ''FALSE'')
				 AND (''' + @ShowProjectSearch + ''' = ''TRUE'' OR (''' + @ShowProjectSearch + ''' = ''FALSE'' AND 1=0))' + @Proj_Search_Filter + '
		  UNION ALL 
		  SELECT 2                 AS sortorder, 
				 ''PARENT-PERMIT'' AS category, 
				 permit_no         AS activityno, 
				 permittype        AS type, 
				 pm.status         AS status, 
				 pm.applied        AS applied
		  FROM   permit_main pm 
		  WHERE  pm.PERMIT_NO = ''' + @parentpermit + ''' AND (''' + @Perm_ContractorFilter + ''' = ''TRUE'' AND (pm.PERMIT_NO IN (SELECT DISTINCT Permit_People.PERMIT_NO FROM Permit_People WHERE (ID = ''C:' + @contractorStLicNo + ''') AND ((''' + @Perm_SearchNameTypeContractorOnly + ''' = ''TRUE'' AND NAMETYPE = ''CONTRACTOR'') OR ''' + @Perm_SearchNameTypeContractorOnly + ''' = ''FALSE''))) OR ''' + @Perm_ContractorFilter + ''' = ''FALSE'')
				 AND (''' + @ShowPermitSearch + ''' = ''TRUE'' OR (''' + @ShowPermitSearch + ''' = ''FALSE'' AND 1=0))' + @Perm_Search_Filter + '
		  UNION ALL 
		  SELECT 3                  AS sortorder, 
				 ''PARENT-LICENSE'' AS category, 
				 lm.bus_lic_no      AS activityno, 
				 lb.business_type   AS type, 
				 lb.loc_address1    AS status, 
				 lm.bus_lic_iss     AS applied 
		  FROM ( SELECT * FROM license_main WHERE 1=1 ' + @Lic_Search_Filter + ' ) AS lm 
				 INNER JOIN license_business lb 
						 ON lb.business_no = lm.business_no 
		  WHERE  lm.bus_lic_no = ''' + @parentlicense + ''' AND (''' + @Lic_ContractorFilter + ''' = ''TRUE'' AND lb.business_no IN (SELECT DISTINCT license_People.business_no FROM license_People WHERE (ID = ''C:' + @contractorStLicNo + ''')) OR ''' + @Lic_ContractorFilter + ''' = ''FALSE'') 
				 AND (''' + @ShowLicenseSearch + ''' = ''TRUE'' OR (''' + @ShowLicenseSearch + ''' = ''FALSE'' AND 1=0)) 
		  UNION ALL 
		  SELECT 4						AS sortorder, 
				 ''PARENT-LICENSE2''	AS category, 
				 lm.license_no			AS activityno, 
				 lm.license_type		AS type, 
				 lm.status	            AS status, 
				 lm.applied				AS applied
		  FROM   license2_main lm 
		  WHERE  lm.license_no = ''' + @parentlicense + ''' AND (''' + @Lic2_ContractorFilter + ''' = ''TRUE'' AND lm.license_no IN (SELECT DISTINCT license2_People.license_no FROM license2_People WHERE (ID = ''C:' + @contractorStLicNo + ''')) OR ''' + @Lic2_ContractorFilter + ''' = ''FALSE'')
				 AND (''' + @ShowLicenseSearch + ''' = ''TRUE'' OR (''' + @ShowLicenseSearch + ''' = ''FALSE'' AND 1=0)) ' + @Lic2_Search_Filter + '
		  UNION ALL 
		  SELECT 5                      AS sortoder, 
				 ''LINKED-CASE''        AS category, 
				 cm2.case_no            AS activityno, 
				 cm2.casetype           AS type, 
				 cm2.status             AS status, 
				 cm2.started            AS applied
		  FROM ( SELECT * FROM case_main WHERE 1=1 ' + @Case_Search_Filter + ' ) AS cm 
				 INNER JOIN case_links cl 
						 ON cl.case_no = cm.case_no 
				 INNER JOIN ( SELECT * FROM case_main WHERE 1=1 ' + @Case_Search_Filter + ' ) AS cm2 
						 ON cm2.case_no = cl.linked_case_no 
		  WHERE  cl.case_no = ''' + @activityno + ''' AND (''' + @ShowCaseSearch + ''' = ''TRUE'' OR (''' + @ShowCaseSearch + ''' = ''FALSE'' AND 1=0))
		  ORDER  BY sortorder, 
					applied DESC, 
					activityno DESC '

	  EXECUTE sp_executesql @SQLString
  END 

IF @activitygroup = 'AEC' 
  BEGIN 

      SET @parentaec = ISNULL((SELECT ISNULL(parent_aec_st_lic_no, '') FROM aec_main WHERE st_lic_no = @activityno), '')

      SET @SQLString = '
		  SELECT 1              AS sortorder, 
				 ''PARENT-AEC'' AS category, 
				 am.st_lic_no   AS activityno, 
				 am.aectype     AS type, 
				 am.ADDRESS1    AS status, 
				 am.applied     AS applied
		  FROM   aec_main am 
		  WHERE  am.st_lic_no = ''' + @parentaec + '''
				 AND (''' + @ShowAECSearch + ''' = ''TRUE'' OR (''' + @ShowAECSearch + ''' = ''FALSE'' AND 1=0))  ' + @AEC_Search_Filter + '
		  UNION ALL 
		  SELECT 2               AS sortorder, 
				 ''SUB-AEC''     AS category, 
				 am2.st_lic_no   AS activityno, 
				 am2.aectype     AS type, 
				 am2.ADDRESS1    AS status, 
				 am2.applied     AS applied
		  FROM   aec_main am2 
		  WHERE  am2.parent_aec_st_lic_no = ''' + @activityno + '''
				 AND (''' + @ShowAECSearch + ''' = ''TRUE'' OR (''' + @ShowAECSearch + ''' = ''FALSE'' AND 1=0)) ' + @AEC_Search_Filter + '
		  UNION ALL 
		  SELECT 3               AS sortorder, 
				 ''SUB-PERMIT''  AS category, 
				 pm.permit_no    AS activityno, 
				 pm.permittype   AS type, 
				 pm.status       AS status, 
				 pm.applied      AS applied 
		  FROM   permit_main pm 
		  WHERE (''' + @Perm_ContractorFilter + ''' = ''TRUE'' AND (pm.PERMIT_NO IN (SELECT DISTINCT Permit_People.PERMIT_NO FROM Permit_People WHERE (ID = ''C:' + @contractorStLicNo + ''') AND ((''' + @Perm_SearchNameTypeContractorOnly + ''' = ''TRUE'' AND NAMETYPE = ''CONTRACTOR'') OR ''' + @Perm_SearchNameTypeContractorOnly + ''' = ''FALSE''))) AND pm.permit_no IN (SELECT DISTINCT permit_no FROM permit_people pp WHERE  id = ''C:' + @activityno + ''') OR (''' + @Perm_ContractorFilter + ''' = ''FALSE'' AND pm.permit_no IN (SELECT DISTINCT permit_no FROM permit_people pp WHERE  id = ''C:' + @activityno + ''') ))
				AND (''' + @ShowPermitSearch + ''' = ''TRUE'' OR (''' + @ShowPermitSearch + ''' = ''FALSE'' AND 1=0)) ' + @Perm_Search_Filter + '
		  UNION ALL 
		  SELECT 4                AS sortorder, 
				 ''SUB-PROJECT''  AS category, 
				 pr.project_no    AS activityno, 
				 pr.projecttype   AS type, 
				 pr.status        AS status, 
				 pr.applied       AS applied
		  FROM   project_main pr 
		  WHERE  (''' + @Proj_ContractorFilter + ''' = ''TRUE'' AND (pr.PROJECT_no IN (SELECT DISTINCT Project_People.project_no FROM Project_People WHERE (ID = ''C:' + @contractorStLicNo + ''') AND ((''' + @Proj_SearchNameTypeContractorOnly + ''' = ''TRUE'' AND NAMETYPE = ''CONTRACTOR'') OR ''' + @Proj_SearchNameTypeContractorOnly + ''' = ''FALSE''))) AND pr.project_no IN (SELECT DISTINCT project_no FROM project_people pp WHERE  id = ''C:' + @activityno + ''') OR (''' + @Proj_ContractorFilter + ''' = ''FALSE'' AND pr.project_no IN (SELECT DISTINCT project_no FROM project_people pp WHERE  id = ''C:' + @activityno + ''')))
				 AND (''' + @ShowProjectSearch + ''' = ''TRUE'' OR (''' + @ShowProjectSearch + ''' = ''FALSE'' AND 1=0)) ' + @Proj_Search_Filter + '
		  UNION ALL 
		  SELECT 5            AS sortorder, 
				 ''SUB-CASE'' AS category, 
				 case_no      AS activityno, 
				 casetype     AS type, 
				 status       AS status, 
				 cm.started   AS applied 
		  FROM   case_main cm 
		  WHERE  CM.case_no IN (SELECT DISTINCT case_no 
								FROM   case_people cc 
								WHERE  id = ''C:' + @activityno + ''') 
				AND (''' + @ShowCaseSearch + ''' = ''TRUE'' OR (''' + @ShowCaseSearch + ''' = ''FALSE'' AND 1=0)) ' + @Case_Search_Filter + '
		  UNION ALL 
		  SELECT 6                  AS sortorder, 
				 ''SUB-LICENSE''    AS category, 
				 lm.bus_lic_no      AS activityno, 
				 lb.business_type   AS type, 
				 lb.loc_address1    AS status, 
				 lm.bus_lic_iss     AS applied 
		  FROM ( SELECT * FROM license_main WHERE 1=1 ' + @Lic_Search_Filter + ' ) AS lm 
				 INNER JOIN license_business lb 
						 ON lb.business_no = lm.business_no 
		  WHERE  (''' + @Lic_ContractorFilter + ''' = ''TRUE'' AND lb.business_no IN (SELECT DISTINCT license_People.business_no FROM license_People WHERE (ID = ''C:' + @contractorStLicNo + ''')) OR (''' + @Lic_ContractorFilter + ''' = ''FALSE'' AND lm.business_no IN (SELECT DISTINCT business_no  FROM license_people cc  WHERE  id = ''C:' + @activityno + ''')))
				 AND (''' + @ShowLicenseSearch + ''' = ''TRUE'' OR (''' + @ShowLicenseSearch + ''' = ''FALSE'' AND 1=0)) 
		  UNION ALL 
		  SELECT 7                  AS sortorder, 
				 ''SUB-LICENSE2''   AS category, 
				 lm.license_no      AS activityno, 
				 lm.license_type    AS type, 
				 lm.status          AS status, 
				 lm.applied         AS applied 
		  FROM   license2_main lm 
		  WHERE  (''' + @Lic2_ContractorFilter + ''' = ''TRUE'' AND lm.license_no IN (SELECT DISTINCT license2_People.license_no FROM license2_People WHERE (ID = ''C:' + @contractorStLicNo + ''') AND lm.license_no IN (SELECT DISTINCT license_no FROM   license2_people cc WHERE  id = ''C:' + @activityno + ''')) OR (''' + @Lic2_ContractorFilter + ''' = ''FALSE'' AND lm.license_no IN (SELECT DISTINCT license_no FROM   license2_people cc WHERE  id = ''C:' + @activityno + ''')))
				 AND (''' + @ShowLicenseSearch + ''' = ''TRUE'' OR (''' + @ShowLicenseSearch + ''' = ''FALSE'' AND 1=0)) ' + @Lic2_Search_Filter + '
		  ORDER  BY sortorder, 
					applied DESC, 
					activityno DESC '

	EXECUTE sp_executesql @SQLString

  END 

IF @activitygroup = 'LICENSE' 
    OR @activitygroup = 'LICENSE2' 
  BEGIN 
	  SET @SQLString = '	
		  SELECT 1              AS sortorder, 
				 ''SUB-PERMIT'' AS category, 
				 permit_no      AS activityno, 
				 permittype     AS type, 
				 status         AS status, 
				 pm.applied     AS applied 
		  FROM   permit_main pm 
		  WHERE  pm.parent_bus_lic_no = ''' + @activityno + ''' 
					AND (''' + @Perm_ContractorFilter + ''' = ''TRUE'' AND (pm.PERMIT_NO IN (SELECT DISTINCT Permit_People.PERMIT_NO FROM Permit_People WHERE (ID = ''C:' + @contractorStLicNo + ''') AND ((''' + @Perm_SearchNameTypeContractorOnly + ''' = ''TRUE'' AND NAMETYPE = ''CONTRACTOR'') OR ''' + @Perm_SearchNameTypeContractorOnly + ''' = ''FALSE''))) OR ''' + @Perm_ContractorFilter + ''' = ''FALSE'')
					AND (''' + @ShowPermitSearch + ''' = ''TRUE'' OR (''' + @ShowPermitSearch + ''' = ''FALSE'' AND 1=0)) ' + @Perm_Search_Filter + '
		  UNION ALL 
		  SELECT 2            AS sortorder, 
				 ''SUB-CASE'' AS category, 
				 case_no      AS activityno, 
				 casetype     AS type, 
				 status       AS status, 
				 cm.started   AS applied 
		  FROM   case_main cm 
		  WHERE  cm.parent_bus_lic_no = ''' + @activityno + ''' AND (''' + @ShowCaseSearch + ''' = ''TRUE'' OR (''' + @ShowCaseSearch + ''' = ''FALSE'' AND 1=0))' + @Case_Search_Filter
	  EXECUTE sp_executesql @SQLString
  END   
END
GO
