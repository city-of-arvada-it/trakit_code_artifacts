USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_InvoiceGenerateNumber]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_InvoiceGenerateNumber]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_InvoiceGenerateNumber]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =====================================================================
-- Revision history: 
--	mdm - 01/22/14 - added to VSS
--  rrr - 02/03/14 - removed FROM Invoice_Main on select @invoiceum
--  RB  - 03/22/16 - TKT_037 - Billing Address Moved Out Of Invoice_Main 
--  RB  - 05/10/16 - TKT_051 - Add RecordID + Group for contact to Invoice_Main table
-- ===================================================================== 


Create procedure [dbo].[tsp_T9_InvoiceGenerateNumber]
@autogenname as varchar(24),
@invoicetotal as money,
@invoicecontact as varchar(300),
@invoiceaddress1 as varchar(600),
@user as varchar(60), 
@InvoiceAddress2 as varchar(600),
@InvoiceCity as varchar(60), 
@InvoiceState as varchar(2), 
@InvoiceZipCode as varchar(10),
@InvoiceContactGroup as varchar(30),
@InvoiceContactRecID as varchar(20)

as
begin

	declare @prefix varchar(10)
	declare @lastnum int
	declare @inc int
	select @prefix = PREFIX, @lastnum = VALUE1,@inc = INCRMNT from Prmry_Autogen where NAME = @autogenname
	declare @invoicenum varchar(60)
	select @invoicenum = (@prefix + '-' + CAST((@lastnum + @inc) as varchar))
	if @inc < 1 set @inc = 1 
	if @prefix is null or @prefix = '' set @prefix = 'INV'
	insert into Invoice_Main(InvoiceNumber,InvoiceDate,InvoiceContact,InvoiceAddress,InvoiceTotal,
	InvoicePaid,InvoiceDue,CreatedBy, InvoiceAddress2, InvoiceCity, InvoiceState, InvoiceZip, ContactGroup, ContactRecordID)
	values(@invoicenum,getdate(),@invoicecontact,@invoiceaddress1,@invoicetotal,0,@invoicetotal,@user, @InvoiceAddress2, @InvoiceCity, @InvoiceState, @InvoiceZipCode, @InvoiceContactGroup, @InvoiceContactRecID)
	update Prmry_Autogen set VALUE1 = (@lastnum + @inc) where NAME = @autogenname
	select @invoicenum
end



GO
