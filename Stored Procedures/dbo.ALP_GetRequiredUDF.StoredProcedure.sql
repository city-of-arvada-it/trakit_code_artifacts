USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[ALP_GetRequiredUDF]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[ALP_GetRequiredUDF]
GO
/****** Object:  StoredProcedure [dbo].[ALP_GetRequiredUDF]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ALP_GetRequiredUDF]
	@activityid as VARCHAR(30)
AS
BEGIN
	--DECLARE @activityid AS VARCHAR(30) = 'GNR16-0084' 
	DECLARE @activitytypeid AS INT = 0 

	SELECT TOP 1 @activitytypeid = activitytypeid 
	FROM   activitytype 
	WHERE  Upper(activitytypename) = 'LICENSE2' 

	--select @activitytypeid 
	SELECT DISTINCT lic.license_no, 
					lic.license_type, 
					Isnull(typeudf.caption, udf.propertyname) AS Caption, 
					udf.propertyname, 
					udf.propertyid, 
					udf.propertytype, 
					udfvalue.stringvalue, 
					udfvalue.intvalue, 
					udfvalue.decimalvalue, 
					udfvalue.datevalue 
	FROM   license2_main lic 
		   INNER JOIN license2_fees fee 
				   ON fee.license_no = lic.license_no 
					  AND Isnull(paid, 0) = 0 
					  AND paid_date IS NULL 
		   INNER JOIN userdefinedproperty udf 
				   ON fee.formula LIKE '%' + udf.propertyname + '%' 
					  AND udf.activitytypeid = @activitytypeid 
		   LEFT JOIN userdefinedpropertyvalues udfvalue 
				  ON udfvalue.activityid = @activityid 
					 AND udfvalue.propertyid = udf.propertyid 
		   LEFT JOIN prmry_typesudf typeudf 
				  ON typeudf.groupname = 'LICENSE2' 
					 AND typeudf.typename = lic.license_type 
					 AND typeudf.datafield LIKE '%' + udf.propertyname 
	WHERE  lic.license_no = @activityid 
	UNION ALL 
	SELECT DISTINCT lic.license_no, 
					lic.license_type, 
					Isnull(typeudf.caption, udf.propertyname) AS Caption, 
					udf.propertyname, 
					udf.propertyid, 
					udf.propertytype, 
					udfvalue.stringvalue, 
					udfvalue.intvalue, 
					udfvalue.decimalvalue, 
					udfvalue.datevalue 
	FROM   license2_main lic 
		   INNER JOIN license2_fees fee 
				   ON fee.license_no = lic.license_no 
					  AND Isnull(paid, 0) = 0 
					  AND paid_date IS NULL 
		   INNER JOIN prmry_feetables feetable 
				   ON fee.formula LIKE '%' + feetable.tablename + '%' 
					  AND feetable.groupname = 'LICENSE2' 
		   INNER JOIN userdefinedproperty udf 
				   ON feetable.basis LIKE '%' + udf.propertyname + '%' 
					  AND udf.activitytypeid = @activitytypeid 
		   LEFT JOIN userdefinedpropertyvalues udfvalue 
				  ON udfvalue.activityid = @activityid 
					 AND udfvalue.propertyid = udf.propertyid 
		   LEFT JOIN prmry_typesudf typeudf 
				  ON typeudf.groupname = 'LICENSE2' 
					 AND typeudf.typename = lic.license_type 
					 AND typeudf.datafield LIKE '%' + udf.propertyname 
	WHERE  lic.license_no = @activityid   

    	
END

GO
