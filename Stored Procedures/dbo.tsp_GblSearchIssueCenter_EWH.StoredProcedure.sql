USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GblSearchIssueCenter_EWH]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_GblSearchIssueCenter_EWH]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GblSearchIssueCenter_EWH]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  
CREATE Procedure [dbo].[tsp_GblSearchIssueCenter_EWH](
@ContactID Varchar(1000) = Null,
@vStartDate Varchar(100) = Null,
@vEndDate Varchar(100) = Null,
@DateType Varchar(100) = Null,
@Type Varchar(500) = Null,
@ActivityNo Varchar(100) = Null,
@Status Varchar(100) = Null,
@ShowOverdueItems INT = Null,
@subTypes varchar(200) = null,
@UserID varchar(50) = null -- EWH Jan 13, 2015 issue #33974: restrict CRM records by user
)


As

Declare @SQL Varchar(8000)
Declare @WHERE Varchar(5000)
Declare @StartDate Datetime 
Declare @EndDate Datetime

Set @WHERE = ''
Set @StartDate  = Null
Set @EndDate  = Null
If @vStartDate = '' Set @vStartDate = Null
If @vStartDate = '1/1/0001 12:00:00 AM' Set @vStartDate = Null
If @vEndDate = '' Set @vEndDate = Null
If @vEndDate = '1/1/0001 12:00:00 AM' Set @vEndDate = Null

If @vStartDate Is Null Set @vStartDate = Convert(Varchar,GetDate(),101)
If @vEndDate Is Null Set @vEndDate = Convert(Varchar,GetDate(),101)

Set @vStartDate = CONVERT(Varchar,CONVERT(Datetime,@vStartDate),101)
Set @vEndDate = CONVERT(Varchar,CONVERT(Datetime,@vEndDate),101)

Print(@vStartDate)
Print(@vEndDate)

Set @StartDate = CONVERT(Datetime,@vStartDate)
Set @EndDate = CONVERT(Datetime,DATEADD(dd,1,@vEndDate))


If @ContactID = '' Set @ContactID = Null
If @DateType = '' Set @DateType = Null
If @vStartDate = '' Set @vStartDate = Null
If @vEndDate = '' Set @vEndDate = Null
If @Type = '' Set @Type = Null
If @subTypes = '' Set @subTypes = Null


/*
tsp_GblSearchIssueCenter @ContactID = '(unassigned)',  @DateType = 'Due_Date',  @vStartDate = '8/1/2012 12:00:00 AM',  @vEndDate = '8/14/2012 12:00:00 AM'

*/

If @ContactID = '(unassigned)'
      Set @WHERE = @WHERE + CHAR(10) + ' AND ASSIGNED_USER_ID Is Null'
ELSE
      If @ContactID Is Not Null
            Set @WHERE = @WHERE + CHAR(10) + ' AND ASSIGNED_USER_ID IN (''' + REPLACE(@ContactID,',',''',''') + ''')'


-- //mdn 7/11/2014 Issue 31830, to support multiple statuses, I've changed this line to the following line
--If @Status Is Not Null Set @WHERE = @WHERE + CHAR(10) + ' AND STATUS_LIST_ID = ''' + @Status + ''''
If @Status Is Not Null Set @WHERE = @WHERE + CHAR(10) + ' AND STATUS_LIST_ID IN (' + @Status + ')'

If @subTypes Is Not Null Set @WHERE = @WHERE + CHAR(10) + ' AND  Issue_SubType IN (''' + REPLACE(@subTypes,',',''',''') + ''') '


If @vStartDate Is Not Null Set @StartDate = CONVERT(Datetime,@vStartDate)
If @vEndDate Is Not Null Set @EndDate = CONVERT(Datetime,DATEADD(dd,1,@vEndDate))
Print(@StartDate)
Print(@EndDate)

If @DateType Is Not Null
Begin
      If @StartDate Is Not Null 
       Begin
            Set @WHERE = @WHERE + ' AND ( '
            Set @WHERE = @WHERE + CHAR(10) + ' ( ' + @DateType + ' >= Convert(Datetime,''' + Convert(Varchar,@StartDate) + ''')'
            If @EndDate Is Null Set @WHERE = @WHERE + ' ) '
      End
      
      If @EndDate Is Not Null 
       Begin
            If @StartDate Is Null Set @WHERE = @WHERE + ' AND ( ( ' ELSE Set @WHERE = @WHERE + ' AND '
            Set @WHERE = @WHERE + CHAR(10) + ' ' + @DateType + ' < Convert(Datetime,''' + Convert(Varchar,@EndDate) + ''') ) '
      End
      
       If @ShowOverdueItems Is Null Set @WHERE = @WHERE + ' ) '
End

If @ShowOverdueItems Is Not Null 
      Set @WHERE = @WHERE + CHAR(10) + ' OR ( DUE_DATE < CAST(FLOOR(CAST(getdate() AS float)) AS DATETIME) AND COMPLETED_DATE is null) )'


--RB Check if table exists before creating it
   IF OBJECT_ID('tempdb..#Results') IS NOT NULL  
	drop table #Results

Create Table #Results(ActivityNo Varchar(40),RecordID Varchar(40),Loc_RecordID Varchar(40),tGroup Varchar(40),AttachmentTotal INT Default 0, NotesTotal INT Default 0, RestrictionsTotal INT Default 0, FeesDue Money Default 0,
 RestrictionsSummary Varchar(5000) Default '',RestrictionsRecordIDUsed Varchar(40),RECORD_TYPE Varchar(60),RECORD_STATUS Varchar(60),
 SITE_ADDR Varchar(500),InspectorName Varchar(150),STATUS_LIST_ID INT,NATURETYPE_LIST_ID INT, ISSUE_ID INT,CREATEDVIA_LIST_ID INT,CATGRY_LIST_ID INT, 
 CreatedVia Varchar(50),Category Varchar(50), IssueSubType varchar(50))

Set @SQL = '
Insert Into #Results(ActivityNo,ISSUE_ID,RecordID,tGroup,Loc_RecordID,Site_Addr,STATUS_LIST_ID,NATURETYPE_LIST_ID,CREATEDVIA_LIST_ID,CATGRY_LIST_ID, IssueSubType)
Select Top 500 Issue_Label,Issue_ID,RecordID,''CRM'',
Loc_RecordID = Issue_Loc_RecordID,
SITE_ADDR = ISSUE_ADDRESS + IsNull('', '' + ISSUE_CITY,''''),
STATUS_LIST_ID,
NATURETYPE_LIST_ID,
CREATEDVIA_LIST_ID,
CATGRY_LIST_ID,
Issue_SubType
From CRM_Issues 
Where 1 = 1 
' + @WHERE
PRINT(@SQL)
EXEC(@SQL)


Update tr Set
Record_Type = TL.LIST_TEXT
From #Results tr
JOIN CRM_Lists TL
ON tr.NATURETYPE_LIST_ID = TL.LIST_ID
  
Update tr Set
Record_Status = SL.LIST_TEXT
From #Results tr
JOIN CRM_Lists SL
ON tr.STATUS_LIST_ID = SL.LIST_ID 

Update tr Set
CreatedVia = SL.LIST_TEXT
From #Results tr
JOIN CRM_Lists SL
ON tr.CREATEDVIA_LIST_ID = SL.LIST_ID 
 
Update tr Set
Category = SL.LIST_TEXT
From #Results tr
JOIN CRM_Lists SL
ON tr.CATGRY_LIST_ID = SL.LIST_ID 
 

Set @WHERE = '' 
If @Type Is Not Null 
 Begin
      Set @WHERE = @WHERE + CHAR(10) + ' AND Record_Type NOT IN (''' + REPLACE(@Type,',',''',''') + ''')'

      Set @SQL = '
      Delete tr
      From #Results tr
      WHERE 1 = 1 
      ' + @WHERE
      
      Print(@SQL)
      Exec(@SQL)
End



Print 'Note Count'
Update #Results Set
NotesTotal = CASE WHEN n.Total Is Null THEN 0 ELSE n.Total END
From #Results tr
LEFT JOIN (
      Select Total=Count(*),ActivityNo From Prmry_Notes
      WHERE ActivityGroup = 'CRM'
      Group By ActivityNo
) n
  ON tr.Issue_ID = n.ActivityNo

  
Print 'Restriction Count'
Update #Results Set
RestrictionsTotal = CASE WHEN r.Total Is Null THEN 0 ELSE r.Total END
From #Results tr
LEFT JOIN (
      Select Total=Count(*),Loc_RecordID From Geo_Restrictions2
      Where Date_Cleared Is Null AND Restriction_Notes Not Like 'VOIDED (%'
      Group By Loc_RecordID
) r
  ON tr.Loc_RecordID = r.Loc_RecordID



Print 'Table #Restrictions'

--RB Check if table exists before creating it
   IF OBJECT_ID('tempdb..#Restrictions') IS NOT NULL  
	drop table #Restrictions

Create Table #Restrictions(RecordID Varchar(30),Loc_RecordID Varchar(40),Summary Varchar(4000), FlagForDelete INT Default 0)
Insert Into #Restrictions(RecordID,Loc_RecordID,Summary)
Select r.RecordID,r.Loc_RecordID,
Summary = r.Restriction_Type + ' ('
+ Convert(Varchar,r.Date_Added,101) + ') ' + SubString(r.Restriction_Notes,1,50)
From #Results tr
JOIN Geo_RESTRICTIONS2 r
ON tr.Loc_RecordID = r.Loc_RecordID
Where r.Date_Cleared Is Null AND r.Restriction_Notes Not Like 'VOIDED (%'

While Exists ( Select * From #Restrictions )
Begin

      Print 'update #results'
      Update #Results Set
      RestrictionsSummary = RestrictionsSummary + r.Summary + '<br />',
      RestrictionsRecordIDUsed = r.RecordID
      From #Results tr
      JOIN #Restrictions r
      ON tr.Loc_RecordID = r.Loc_RecordID
      
      Print 'summary used'
      Update r Set
      FlagForDelete = 1
      From #Results tr
      JOIN #Restrictions r
      ON tr.RestrictionsRecordIDUsed = r.RecordID
      
      Print 'update #results 2'
      Update #Results Set
      RestrictionsRecordIDUsed = Null
      From #Results tr
      JOIN #Restrictions r
      ON tr.Loc_RecordID = r.Loc_RecordID
      
      Print 'delete flaggedfordelete records'
      Delete #Restrictions Where FlagForDelete = 1
      
 End 
 

Print 'CRM Attachments'
Update #Results Set
AttachmentTotal = CASE WHEN a.Total Is Null THEN 0 ELSE a.Total END
From #Results tr
LEFT JOIN (
      Select Total=Count(*),ISSUE_ID From CRM_Attachments
      Group By ISSUE_ID
) a
  ON tr.ISSUE_ID = a.ISSUE_ID
--WHERE tr.tGroup = 'CRM'


Select 
ActivityNo = tr.ActivityNo,
Created_Date_Datetime = c.CREATED_DATETIME,
Due_Date_Datetime = c.DUE_DATE,
Updated_Date_Datetime = c.Last_Update_Date,
Completed_Date_Datetime = c.COMPLETED_DATE,
CreatedDate = Convert(Varchar,c.CREATED_DATETIME, 101),
DueDate = Convert(Varchar,c.DUE_DATE, 101),
UpdatedDate = Convert(Varchar,c.Last_Update_Date, 101),
CompletedDate = Convert(Varchar,c.COMPLETED_DATE, 101), 
tGroup = tr.tGroup,
tr.CreatedVia,
tr.Category,
tr.AttachmentTotal,tr.NotesTotal,tr.RestrictionsTotal,tr.RestrictionsSummary,tr.Loc_RecordID,tr.Record_Type,tr.Record_Status,tr.Site_Addr, tr.IssueSubType,
c.*
From CRM_ISSUES c
JOIN #Results tr
  ON c.RecordID = tr.RecordID
-- EWH Jan 13, 2015 issue #33974: added where clause to restrict CRM records by user
where (c.NATURETYPE_LIST_ID IS NULL OR c.NATURETYPE_LIST_ID IN (select distinct CRM_Issues.NATURETYPE_LIST_ID from CRM_Issues inner join crm_lists on NATURETYPE_LIST_ID = list_id where list_text not in (SELECT distinct TYPENAME FROM PRMRY_CRM_RESTR_TYPE_ACCESS WHERE USERID <> @UserID)))

--Select tr.AttachmentTotal,tr.NotesTotal,tr.RestrictionsTotal,tr.FeesDue,tr.RestrictionsSummary,tr.Loc_RecordID,c.*
--From tvw_GblConditions c
--JOIN #Results tr
--  ON c.RecordID = tr.RecordID

/*

tsp_GblSearchIssueCenter @ContactID = '',  @DateType = 'Due_Date',  @vStartDate = '6/18/2012 12:00:00 AM',  @vEndDate = '6/18/2012 12:00:00 AM'

tsp_GblSearchIssueCenter @ContactID = '',  @DateType = 'Due_Date',  @vStartDate = '6/1/2012 12:00:00 AM',  @vEndDate = '6/30/2012 12:00:00 AM'

*/





GO
