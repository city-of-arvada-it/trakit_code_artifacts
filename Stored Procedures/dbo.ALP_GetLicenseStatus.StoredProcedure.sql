USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[ALP_GetLicenseStatus]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[ALP_GetLicenseStatus]
GO
/****** Object:  StoredProcedure [dbo].[ALP_GetLicenseStatus]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================================
-- Author:		Erick Hampton
-- Revised:		Dec 21, 2016
-- Description:	Gets the BatchID and most recent operation status of a specific license from the batch tracking table 
-- ====================================================================================================================
CREATE PROCEDURE [dbo].[ALP_GetLicenseStatus]

	@LicenseNumber varchar(30) = NULL
	
AS
BEGIN

	select top 1 
		  opstatus.[BatchID]
		, opstatus.[OperationStatusID]
	from [ALP_OperationStatusByBatchParameterSetLicenseNumber] opstatus
		inner join [ALP_Batches] b on opstatus.BatchID=b.Id
		
	where opstatus.LicenseNumber=@LicenseNumber
		and b.StatusID in (1, 2, 3) -- submitted, queued, wip (every status that indicates the batch has not finished processing)
		and opstatus.OperationStatusID in (1, 2) -- Pending, Processing (every status that indicates the license has not finished processing)
		and (opstatus.OperationStart is null or opstatus.OperationStart is not null and opstatus.OperationEnd is null)
		
	order by opstatus.[OperationSequence] desc

END

GO
