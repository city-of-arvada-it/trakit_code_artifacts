USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_InvoiceOutstandingFees]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_InvoiceOutstandingFees]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_InvoiceOutstandingFees]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  
-- =============================================
-- Author:		Michael Morton
-- Create date: 12/3/2013
-- Description:	Invoice Creation Tree
-- 7/20/15 - RGB - Added Fee Type Columns
-- VS - 2/4/2016 - For invoice creation
-- 7/14/16 - RRR - Exclude Fees of FeeType INFO (TKT_125)
-- 7/27/16 - RRR - Determine Deposit Type and return only deposits when 'COLLECT' and no records when 'NOFEE'
-- 5/17/18 - AMT - Added "INVOICED = '0'" to where clause of #outstandingfees insert statements
-- 8/3/18 - AU - Switched amount >= 0.01 to (amount >= 0.01 or amount <= -0.01) to allow credit fees to be invoiced
-- 8/7/18 - DR - Changed ActivityNo, all ActNo, and all parentactivity to varchar(30) to match ActivityNo across DB
-- =============================================
CREATE PROCEDURE [dbo].[tsp_T9_InvoiceOutstandingFees]
	
	@ActivityNo varchar(30),
	@Module varchar(15)

	
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @table as varchar(15)
	declare @actnofield as varchar(15)
	declare @sql as varchar(200)
	DECLARE @depositOnly varchar(15)  -- rrr 7/27/16: TKT_125

	DECLARE @default_merchant VARCHAR(60)
	SET @default_merchant = (SELECT ISNULL(value2, '') from Prmry_Preferences where item = 'ADVANCED_MERCHANT_ACCT' and value1 = 'YES')


    -- Insert statements for procedure here
    create table #outstandingfees (Module varchar(15),ActNo varchar(30),Code varchar(12),[description] varchar(60),
    parentactivity varchar(30),Amount float,RecordID varchar(30), FeeType varchar(8), merchant_account varchar(500))

    create table #outstandingfeesfinal (Module varchar(15),ActNo varchar(30),Code varchar(12),[description] varchar(60),
	parentactivity varchar(30),Amount float,RecordID varchar(30), FeeType varchar(8), merchant_account varchar(500))

	create table #outstandingsubfees (Module varchar(15),ActNo varchar(30),Code varchar(12),[description] varchar(60),
	Amount float,RecordID varchar(30),ParentID varchar(30),SubFeeType varchar(8))

	create table #outstandingsubfeesfinal (Module varchar(15),ActNo varchar(30),Code varchar(12),[description] varchar(60),
	Amount float,RecordID varchar(30),ParentID varchar(30),SubFeeType varchar(8))
		
	if @Module = 'PERMIT' 
		begin 
			set @table = 'permit_fees' 
			set @actnofield = 'PERMIT_NO'
			set @depositOnly = (select fee_type = case DepositType when 'COLLECT' then 'DEPOSIT'
								 when 'NOFEE' then 'XXZZXX' else '%%' end from permit_main where permit_no = @ActivityNo)  -- rrr 7/27/16: TKT_125
		end
	else if @Module = 'PROJECT' 
		begin 
			set @table = 'project_fees' 
			set @actnofield = 'PROJECT_NO'
			set @depositOnly = (select dep_type = case DepositType when 'COLLECT' then 'DEPOSIT'
								when 'NOFEE' then 'XXZZXX' else '%%' end from project_main where project_no = @ActivityNo)  -- rrr 7/27/16: TKT_125
		end 
	else if @Module = 'CASE' 
		begin 
			set @table = 'case_fees'
			set @actnofield = 'CASE_NO'
			set @depositOnly = (select dep_type = case DepositType when 'COLLECT' then 'DEPOSIT'
									when 'NOFEE' then 'XXZZXX' else '%%' end from case_main where case_no = @ActivityNo)  -- rrr 7/27/16: TKT_125
		end
	else if @Module = 'LICENSE2' 
		begin 
			set @table = 'license2_fees'
			set @actnofield = 'LICENSE_NO'
			set @depositOnly = (select dep_type = case DepositType when 'COLLECT' then 'DEPOSIT'
									when 'NOFEE' then 'XXZZXX' else '%%' end from license2_main where license_no = @ActivityNo)  -- rrr 7/27/16: TKT_125
		end
	else if @Module = 'AEC' 
		begin 
			set @table = 'aec_fees'
			set @actnofield = 'ST_LIC_NO'
			set @depositOnly = (select dep_type = case DepositType when 'COLLECT' then 'DEPOSIT'
									when 'NOFEE' then 'XXZZXX' else '%%' end from aec_main where st_lic_no = @ActivityNo)  -- rrr 7/27/16: TKT_125
		end

	
	if @Module = 'PERMIT' 
		begin 	
			insert into #outstandingfees select distinct 'PERMIT' as [Module],  @ActivityNo as [ActNo],Code, pf.description, @ActivityNo,AMOUNT,pf.RECORDID, feetype, ISNULL(pud.MerchantAccount, @default_merchant) as MERCHANT_ACCOUNT
		    from  PERMIT_FEES pf
			INNER JOIN permit_main pm ON pm.PERMIT_NO = pf.PERMIT_NO
			LEFT JOIN prmry_types pt ON pt.TypeName = pm.PermitType AND pt.GroupName = 'PERMITS'
			LEFT JOIN prmry_userdepts pud ON pud.UserDept = pt.OwnerDept		
			where pf.PERMIT_NO = @Activityno and  pf.Paid <> 1 and (pf.amount >= 0.01 or pf.amount <= -0.01) and pf.FEETYPE LIKE @depositOnly and pf.FEETYPE <> 'INFO'   -- rrr - exclude info fees (TKT_125)
				and (INVOICED is null or INVOICED = '' or INVOICED = '0')
		end
	else if @Module = 'PROJECT' 
		begin 			
			insert into #outstandingfees select distinct 'PROJECT' as [Module] ,@ActivityNo as [ActNo],Code, pf.description, @ActivityNo,AMOUNT,pf.RECORDID, FeeType, ISNULL(pud.MerchantAccount, @default_merchant) as MERCHANT_ACCOUNT
		    from PROJECT_FEES pf
			INNER JOIN projecT_main pm ON pm.PROJECT_NO = pf.PROJECT_NO
			LEFT JOIN prmry_types pt ON pt.TypeName = pm.PROJECTTYPE AND pt.GroupName = 'PROJECTS'
			LEFT JOIN prmry_userdepts pud ON pud.UserDept = pt.OwnerDept		
			where pf.PROJECT_NO = @Activityno and pf.Paid <> 1 and (pf.amount >= 0.01 or pf.amount <= -0.01) and pf.FEETYPE LIKE @depositOnly and pf.FEETYPE <> 'INFO'  -- rrr - exclude info fees (TKT_125)
				and (INVOICED is null or INVOICED = '' or INVOICED = '0')	
		end 
	else if @Module = 'CASE' 
		begin 			
			insert into #outstandingfees select distinct 'CASE' as [Module], @ActivityNo as [ActNo],Code,cf.description,@ActivityNo,AMOUNT,cf.RECORDID, FeeType, ISNULL(pud.MerchantAccount, @default_merchant) as MERCHANT_ACCOUNT
			from CASE_FEES cf
			INNER JOIN case_main cm ON cm.CASE_NO = cf.CASE_NO
			LEFT JOIN prmry_types pt ON pt.TypeName = cm.CaseType AND pt.GroupName = 'VIOLATIONS'
			LEFT JOIN prmry_userdepts pud ON pud.UserDept = pt.OwnerDept		
			where cf.CASE_NO = @ActivityNo and cf.Paid <> 1 and (cf.amount >= 0.01 or cf.amount <= -0.01) and cf.FEETYPE LIKE @depositOnly and cf.FEETYPE <> 'INFO'  -- rrr - exclude info fees (TKT_125)
				and (INVOICED is null or INVOICED = '' or INVOICED = '0')
		end
	else if @Module = 'LICENSE2' 
		begin 
			declare @parentlicno as varchar(20)
			
			insert into #outstandingfees select distinct 'LICENSE2' as [Module],@ActivityNo as [ActNo],Code,lf.description,@parentlicno,AMOUNT,lf.RECORDID, FeeType  , ISNULL(pud.MerchantAccount, @default_merchant) as MERCHANT_ACCOUNT
			from LICENSE2_FEES lf
			INNER JOIN license2_main lm ON lm.LICENSE_NO = lf.LICENSE_NO
			LEFT JOIN prmry_types pt ON pt.TypeName = lm.LICENSE_TYPE AND pt.GroupName = 'LICENSE2'
			LEFT JOIN prmry_userdepts pud ON pud.UserDept = pt.OwnerDept		
			where LF.LICENSE_NO = @ActivityNo and lf.Paid <> 1 and (lf.amount >= 0.01 or lf.amount <= -0.01) and lf.FEETYPE LIKE @depositOnly and lf.FEETYPE <> 'INFO'  -- rrr - exclude info fees (TKT_125)
				and (INVOICED is null or INVOICED = '' or INVOICED = '0')
		end
	else if @Module = 'AEC' 
		begin 			
			insert into #outstandingfees select distinct 'AEC' as [Module],@ActivityNo as [ActNo],Code, af.description,@ActivityNo,AMOUNT, af.RECORDID, FeeType  , ISNULL(pud.MerchantAccount, @default_merchant) as MERCHANT_ACCOUNT
			from AEC_FEES af
			INNER JOIN aec_main am ON am.ST_LIC_NO = af.ST_LIC_NO
			LEFT JOIN prmry_types pt ON pt.TypeName = am.AECTYPE AND pt.GroupName = 'AEC'
			LEFT JOIN prmry_userdepts pud ON pud.UserDept = pt.OwnerDept		
			where  af.ST_LIC_NO  = @ActivityNo  and af.Paid <> 1 and (af.amount >= 0.01 or af.amount <= -0.01) and af.FEETYPE LIKE @depositOnly and af.FEETYPE <> 'INFO'  -- rrr - exclude info fees (TKT_125)
				and (INVOICED is null or INVOICED = '' or INVOICED = '0')	
		end    
	insert into #outstandingfeesfinal select distinct Module,ActNo,Code,[description],parentactivity,AMOUNT,RecordID, FeeType, merchant_account 
	from #outstandingfees

	drop table #outstandingfees

	--- SUBFEES

	if @Module = 'PERMIT' 
		begin 	
			insert into #outstandingsubfees 
			select 'PERMIT' as Module,PERMIT_NO as ActNo, CODE,[description],AMOUNT,RECORDID,PARENTID,SubFeeType from Permit_SubFees where PERMIT_NO = @Activityno and SubFeeType <> 'INFO'  -- rrr - exclude info fees (TKT_125)
		end

	else if @Module = 'PROJECT' 
		begin 	

		print 'HERE'
		print @Activityno
			insert into #outstandingsubfees 	
		    select 'PROJECT' as Module,PROJECT_NO as ActNo, CODE,[description],AMOUNT,RECORDID,PARENTID,SubFeeType from Project_SubFees where PROJECT_NO = @Activityno and SubFeeType <> 'INFO'  -- rrr - exclude info fees (TKT_125)
		end	
				
	else if @Module = 'CASE' 
		begin 	
			insert into #outstandingsubfees 		
			select 'CASE' as Module,CASE_NO as ActNo, CODE,[description],AMOUNT,RECORDID,PARENTID,SubFeeType from Case_SubFees	 where CASE_NO = @Activityno and SubFeeType <> 'INFO'  -- rrr - exclude info fees (TKT_125)
		end

	else if @Module = 'LICENSE2' 
		begin 
		   insert into #outstandingsubfees 
		   select 'LICENSE2' as Module,LICENSE_NO as ActNo, CODE,[description],AMOUNT,RECORDID,PARENTID,SubFeeType from LICENSE2_SUBFEES where LICENSE_NO = @Activityno and SubFeeType <> 'INFO'  -- rrr - exclude info fees (TKT_125)
		end
	else if @Module = 'AEC' 
		begin 	
			insert into #outstandingsubfees 		
			select 'AEC' as Module,ST_LIC_NO as ActNo, CODE,[description],AMOUNT,RECORDID,PARENTID,SubFeeType from AEC_SubFees where ST_LIC_NO = @Activityno and SubFeeType <> 'INFO'  -- rrr - exclude info fees (TKT_125)
		end  
		
	select *
	from #outstandingfeesfinal

	
	insert into #outstandingsubfeesfinal
	select a.Module,a.ActNo,b.Code,b.[description],b.Amount,b.RecordID,b.ParentID, b.subfeetype
	from #outstandingfeesfinal a inner join #outstandingsubfees b
	on a.RecordID = b.ParentID and a.Module = b.Module
	select * from #outstandingsubfeesfinal

	drop table #outstandingsubfees
	drop table #outstandingsubfeesfinal
	drop table #outstandingfeesfinal

End	

GO
