USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_ProjectLetterFieldsAdministrative]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[usp_Arvada_ProjectLetterFieldsAdministrative]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_ProjectLetterFieldsAdministrative]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_Arvada_ProjectLetterFieldsAdministrative]
	@projectno varchar(30)
as
/**********************************************************************************************************************
* Creator:      	 
* Date:         	 
* Title:			 
* Description:  	 
* Examples:    
			EXECUTE dbo.[usp_Arvada_ProjectLetterFieldsAdministrative] 'DA2018-0006' 
			 
* Modifications:  	
  Developer Name		Date		Description
  ------------------	----------	----------------------------------------------------------- 
*
**********************************************************************************************************************/

set nocount on
 
 --------------------------
 --- Declare Variables   --
 --------------------------
 declare	
	@appliedDate datetime,
	@ProjectList varchar(max),
	@TextBottom nvarchar(max),
	@TextTop nvarchar(max),
	@plannerFull nvarchar(50),
	@plannerFirst nvarchar(50),
	@ComplDate varchar(10),
	@AccDate varchar(10),
	@appName nvarchar(50),
	@appType nvarchar(50)
 
 --------------------------
 --- Create Temp Tables  --
 --------------------------

create table #projectsList
(
	project_no varchar(30),
	levelValue int,
	applied datetime
)
	 
 --------------------------
 --- Body of Procedure   --
 --------------------------

 

select 
	@appliedDate = m.applied,
	@plannerFull = m.planner,
	@plannerFirst = case when charindex(' ', m.planner) > 0  then
			substring(m.planner,1,  charindex(' ', m.planner)-1)
			else isnull(m.planner,'')
			end,
	--@ComplDate = convert(Varchar(10),m.STATUS_DATE,101),
	@AccDate = convert(Varchar(10),m.APPLIED,101),
	@appName = app.NAME
from dbo.project_main m
left join dbo.Project_People app with (nolock)
	on app.PROJECT_NO = m.PROJECT_NO
	and app.NAMETYPE = 'applicant'
where m.PROJECT_NO = @projectno

select top 1 @ComplDate = convert(Varchar(10), action_date,101)
from dbo.Project_Actions a
where a.action_Type = 'finalized initial review'
and a.PROJECT_NO  = @projectno
order by 
	ACTION_DATE desc

 

--insert into #projectsList
--(
--	project_no ,
--	levelValue ,
--	applied 
--)
--select
--	project_no ,
--	levelValue ,
--	applied 
--from dbo.[tfn_Arvada_LinkedProjects](@projectno)
--where 
--	datediff(Day,applied,@appliedDate) = 0

--select @ProjectList =
-- stuff(
--			 (select ', '+ p.project_no 
--			  from #projectsList as p  
--			  for xml path(''), root('MyString'), type 
--			  ).value('/MyString[1]','nvarchar(max)') ,1,2,''
--		) 

declare @AccLetter varchar(10),
		@FirstReview varchar(10),
		@FirstResub varchar(10),
		@SecondReview varchar(10),
		@SecondResub varchar(10),
		@PlanningCommDate varchar(10),
		@trk_type varchar(60),
		@CommDevDirector varchar(10),
		@phone nvarchar(50),
		@fee decimal(36,2)


select top 1 @AccLetter = convert(Varchar(10), action_date,101)
from project_actions a
where 
	a.PROJECT_NO  = @projectno
and a.action_Type = 'Acceptance Letter'
order by 
	ACTION_DATE desc


select top 1 @FirstReview = convert(Varchar(10), action_date,101)
from project_actions a
where 
	a.PROJECT_NO  = @projectno
and a.action_Type = '1st Review Complete'
order by 
	ACTION_DATE desc

select top 1 @FirstResub = convert(Varchar(10), action_date,101)
from project_actions a
where 
	a.PROJECT_NO  = @projectno
and a.action_Type = 'Customer 1st Resubmittal'
order by 
	ACTION_DATE desc

select top 1 @SecondReview = convert(Varchar(10), action_date,101)
from project_actions a
where 
	a.PROJECT_NO  = @projectno
and a.action_Type = '2nd Review Complete'
order by 
	ACTION_DATE desc

select top 1 @SecondResub = convert(Varchar(10), action_date,101)
from project_actions a
where 
	a.PROJECT_NO  = @projectno
and a.action_Type = 'Customer 2nd Resubmittal'
order by 
	ACTION_DATE desc

select top 1 @PlanningCommDate = convert(Varchar(10), action_date,101)
from project_actions a
where 
	a.PROJECT_NO  = @projectno
and a.action_Type = 'Planning Commission Hearing'
order by 
	ACTION_DATE desc


select top 1 @CommDevDirector = convert(Varchar(10), action_date,101)
from project_actions a
where 
	a.PROJECT_NO  = @projectno
and a.action_Type = 'SEND TO COM. DEV. DIRECTOR'
order by 
	ACTION_DATE desc
	   
select 
	@trk_type = trk_type
from dbo.project_udf as a
where 
	a.PROJECT_NO  = @projectno
 
select @phone = isnull(u.phone, '720-898-7435')
from dbo.project_main m
left join dbo.prmry_users u
	on u.username = m.PLANNER
where
	m.project_no = @projectno


select @fee = sum(convert(decimal(36,2),amount))
from dbo.project_fees
where project_no = @projectno
and code in ('PLANAPP','MINORMOD')


select 
	@ProjectList as ProjectList,
	m.project_no,
	app.NAME, 
	m.project_name,
	ltrim(rtrim(
	app.NAME +
	case when app.ADDRESS1 is not null and app.ADDRESS1 != '' then  app.ADDRESS1 else '' end +  
	case when app.ADDRESS2 is not null and app.ADDRESS2 != '' then  case when isnull(app.ADDRESS1,'') != '' then char(10) + char(13) else '' end + app.ADDRESS2 else '' end +  
	case when app.CITY is not null and app.CITY != '' then char(10) + char(13) + app.CITY else '' end +
	case when app.[STATE] is not null and app.[STATE] != '' then  ', ' + app.[STATE] else '' end +
	case when app.ZIP is not null and app.ZIP != '' then  ', ' + app.ZIP else '' end))  as ApplicantFullAddress,
	m.planner,

	case when charindex(' ', m.planner) > 0  then
			substring(m.planner,1,  charindex(' ', m.planner)-1)
			else m.planner
			end as PlannerFirstName,
	m.SITE_ADDR,


	
	app.NAME as ApplicantName,

	ltrim(rtrim(
	app.NAME + char(10) + char(13) +
	case when app.ADDRESS1 is not null and app.ADDRESS1 != '' then  app.ADDRESS1 else '' end +  
	case when app.ADDRESS2 is not null and app.ADDRESS2 != '' then  case when isnull(app.ADDRESS1,'') != '' then char(10) + char(13) else '' end  + app.ADDRESS2 else '' end +  
	case when app.CITY is not null and app.CITY != '' then char(10) + char(13) + app.CITY else '' end +
	case when app.[STATE] is not null and app.[STATE] != '' then  ', ' + app.[STATE] else '' end +
	case when app.ZIP is not null and app.ZIP != '' then  '' + app.ZIP else '' end))  as ApplicantFullAddressWithName,

	m.project_no  as SiteFullAddress,
	--m.project_name + char(10) + char(13) +
	--case when m.SITE_ADDR is not null and m.SITE_ADDR != '' then  m.SITE_ADDR else '' end +  
	--case when m.SITE_CITY is not null and m.SITE_CITY != '' then char(10) + char(13) + m.SITE_CITY else '' end +
	--case when m.SITE_STATE is not null and m.SITE_STATE != '' then  ', ' + m.SITE_STATE else '' end +
	--case when m.SITE_ZIP is not null and m.SITE_ZIP != '' then  ', ' + m.SITE_ZIP else '' end  as SiteFullAddress,
	'Missing: ' +
		case when app.NAME is null then 'applicant name,' else '' end
		+  case when m.planner is null then 'planner,' else '' end
		+  case when @AccLetter is null then 'Acceptance Letter Date,' else '' end
		+  case when @trk_type is null then 'Track Type,' else '' end
		+  case when @FirstReview is null then 'First Review,' else '' end
		+  case when m.balance_Due is null then 'Balance Due,' else '' end
		+  case when @FirstResub is null then 'First Resubmittal Date,' else '' end
		+  case when @SecondResub is null then 'Second Resubmittal Date,' else '' end
		+  case when @CommDevDirector is null then 'SEND TO COM. DEV. DIRECTOR' else '' end
		+  case when @phone is null then 'Phone' else '' end
		+  case when @fee is null then 'Plan App Fee' else '' end
		+  case when @SecondReview is null then 'Second Review Date' else '' end 
		as MissingFields,
'Dear '+app.NAME +',

The City of Arvada Planning and Development Division has accepted your development application and has assigned it to '+m.planner+', who will be your project planner.  '+m.planner+' will be responsible for processing your application and guiding you through the development review process, and can be reached at '+@phone+'.  

The application was accepted on '+@AccLetter+'.  Your project will be placed on our '+lower(@trk_type)+' project review schedule.  The City''s initial review comments on your application will be provided to you on '+@FirstReview+'. The application fee of $'+convert(Varchar(10),@fee)+' is now due.  If the fee is not paid by the end of the initial review, no comments will be released. 

Your project planner will email you a notification of when the review comments from internal and external agencies are available for your project. You will be able to obtain them from our eTRAKiT system www.arvadapermits.org. Please note you will need to be logged in to review your comments. Once you have received and reviewed your comments, please feel free to contact your planner with any questions you may have or if you would like to schedule a meeting to discuss the comments. All calls and emails will be returned within 24 hours.

To remain on schedule, your resubmittal must be provided to your project planner through eTRAKiT by 4:00pm on '+@FirstResub+', Resubmittal documents will need to be attached by you through eTRAKiT.  The second review comments will be due to you on '+@SecondReview+'.   

If an additional review of your project is required following the second review comments, you will need to submit for a third review to your project planner by 4:00pm on '+@SecondResub+'. All resubmittal items are to be submitted electronically through www.arvadapermits.org.
 
If no additional review is necessary following the third submittal, a recommend for action will be forwarded to the Community Development Director on '+@CommDevDirector+'. 

Staff is committed to meeting our deadlines for comments. If you should miss a resubmittal date, your project will no longer be on the designated review schedule outlined in this letter.

Remember, all submissions will need to be complete and address each of the City review comments provided regarding the previous submittal. The City reserves the right to reject any submission that is determined to be incomplete or if significant comments are not adequately addressed. 
' as LetterText1,
'For additional information regarding your application or the development review process, please feel free to contact your project planner or visit www.arvada.org/develop. Additionally, you can follow the progress on your project through the City’s online permit and project tracking system arvadapermits.org. Instructions on creating an account and using the system are located at: https://www.arvada.org/permithelp. If at any time you have concerns about the timing or content of our reviews or have any other questions, you may also call me at '+@phone+'.

We look forward to working with you.


Sincerely,


Rob Smetana, AICP
Manager of City Planning and Development
' as LetterText2
from dbo.project_main m with (nolock)
left join dbo.Project_People app with (nolock)
	on app.PROJECT_NO = m.PROJECT_NO
	and app.NAMETYPE = 'applicant'
where
	m.project_no = @projectno


 
GO
