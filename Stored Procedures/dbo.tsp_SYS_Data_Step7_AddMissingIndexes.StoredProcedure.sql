USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_SYS_Data_Step7_AddMissingIndexes]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_SYS_Data_Step7_AddMissingIndexes]
GO
/****** Object:  StoredProcedure [dbo].[tsp_SYS_Data_Step7_AddMissingIndexes]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  					  
-- =====================================================================
-- Revision History:
--	mdm - 12/02/13 - added revision history	
--	mdm - 11/19/13 - added to source safe
-- =====================================================================   

CREATE PROCEDURE [dbo].[tsp_SYS_Data_Step7_AddMissingIndexes] (
@OutputPath Varchar(1000) = Null
)

AS
BEGIN
  SET NOCOUNT ON

  --mdm - 09/20/13; sample @OutputPath:  Step5_DS_AddMissingIndexes.sql

  IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[zIndexListForSP]') AND type in (N'U'))
		BEGIN
		   CREATE TABLE zIndexListForSP
			(
				TblName Varchar(255),
				IndexName Varchar(255),
				ColName Varchar(255), 
				Fragmentation float
			   )
		End

	truncate table  zIndexListForSP

	 INSERT INTO zIndexListForSP(TblName,IndexName,ColName)
		 SELECT  a.name AS TblName, b.name AS Indexname, c.name AS ColName
				FROM sys.objects a, sys.indexes b, sys.columns c, sys.index_columns d
				WHERE(b.object_id = a.object_id)
				AND b.object_id = c.object_id
				AND a.object_id = d.object_id
				AND b.index_id = d.index_id
				AND d.column_id = c.column_id
				AND (a.name not like 'sys%' and a.name not like 'queue_messages%' and a.name not like 'file%')
				ORDER BY a.name, c.name, b.name;

	--select * from #IndexList order by TblName,IndexName,ColName
	--select * from zIndexListForSP 	order by TblName, IndexName, ColName

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[zIndexTemp]') AND type in (N'U'))
BEGIN
create table zIndexTemp
(
SqlToExec varchar(2000)
)
END

truncate table zIndexTemp

insert into zIndexTemp
Select 'IF NOT EXISTS (SELECT * FROM SYS.INDEXES where object_id = object_id(''' + zIndexListForSP.TblName + ''' ) 
		AND NAME = ''' + zIndexListForSP.IndexName + ''')  CREATE INDEX ' + zIndexListForSP.IndexName + ' ON ' + zIndexListForSP.TblName + '(' +			zIndexListForSP.ColName + ') ;'
		from zIndexListForSP order by TblName, IndexName, ColName

declare @FileName as varchar(1000)
If @OutputPath Is Null OR @OutputPath = ''
	Set @FileName = 'C:\crw\TEMP\Step7_DS_AddMissingIndexes.sql'
ELSE
	Set @FileName = @OutputPath
--set @FileName = 'C:\Mayo\Step7_DS_AddMissingIndexes.sql'
--select @FileName

declare @dbName varchar(200)
set @dbName = db_name()
--select @dbName


declare @cmd varchar(2000)
set @cmd = 'bcp "select SqlToExec from '
set @cmd = @cmd + @dbname + '.dbo.zIndexTemp order by SqlToExec" queryout "'
set @cmd = @cmd + @FileName + '" -Utrakit -Ptrakit -c -S' + @@SERVERNAME

--select @cmd
exec xp_cmdshell @cmd

select SqlToExec from zIndexTemp order by SqlToExec

--drop table zIndexListForSP

END










GO
