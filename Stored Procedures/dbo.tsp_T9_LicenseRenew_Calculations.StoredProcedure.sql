USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_LicenseRenew_Calculations]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_LicenseRenew_Calculations]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_LicenseRenew_Calculations]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[tsp_T9_LicenseRenew_Calculations]    Script Date: 8/26/2014 2:36:58 PM ******/
-- ======================================================================================================================================================
-- Revisions:
-- EWH	Jan 28, 2016	Added {fee code} replacement in formula
--						Updated UDF replacement in formula
--						Moved QTY replacement to end and updated code
--
-- EWH	Jan 29, 2016	Changed UDF_NAME from varchar(20) to varchar(max)
--
-- mdm	Feb 2, 2016		Rewrote non-functional code for replacing udfs in formulas for both fees and subfees
--
-- EWH	Feb 4, 2016		Nullify formula_working_col values that are not numeric or contain mathematical operations before calculating formula value
--						Exclude QTY replacement in formula_working_col for rows where Quantity is null
--						Fixed non-functional code for replacing tiered udf formulas
-- =====================================================================================================================================================
create PROCEDURE [dbo].[tsp_T9_LicenseRenew_Calculations]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	
INSERT INTO LicenseRenew_EventLog (SourceMode, EventText, USERID) VALUES('CALC', 'Calculation routine invoked.','SQL')	

-- D9. Update Fees where no calculation or substitution necessary
UPDATE LicenseRenew_Review_Fees
SET LicenseRenew_Review_Fees.Calc_value = CAST(LicenseRenew_Review_Fees.FORMULA AS float)
WHERE ISNUMERIC( LicenseRenew_Review_Fees.FORMULA ) = 1

-- D10. Update SubFees where no calculation or substitution necessary 
UPDATE LicenseRenew_Review_SubFees
SET LicenseRenew_Review_SubFees.Calc_value = CAST(LicenseRenew_Review_SubFees.FORMULA As float)
WHERE ISNUMERIC( LicenseRenew_Review_SubFees.FORMULA) = 1

--D13.  Update formula for look up fields fields
IF OBJECT_ID(N'tempdb..#TempTable') IS NOT NULL
BEGIN
	DROP TABLE #TempTable
END

Select  name into  #TempTable
from  sys.columns 
WHERE object_id = (SELECT  object_id 
FROM [sys].[objects] 
WHERE type = 'U' and name = 'LICENSE2_UDF' )  


DECLARE @FieldName NVARCHAR(100)
declare @fieldValue int
DECLARE @getcounterID CURSOR


SET @getcounterID = CURSOR FOR

SELECT name
FROM #TempTable

OPEN @getcounterID
FETCH NEXT
FROM @getcounterID INTO @FieldName
WHILE @@FETCH_STATUS = 0
BEGIN

	-- update Formula_working_col
	IF EXISTS (SELECT distinct basis from LicenseRenew_Review_SubFees inner join [Prmry_FeeTables]
	on '['+[TABLENAME]+']'=LicenseRenew_Review_SubFees.FORMULA  WHERE basis=@FieldName) 

	if @FieldName <>'LICENSE_NO'
	begin
  
		IF  EXISTS( SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'formula_update')
		BEGIN
			drop table formula_update
		end
	
		DECLARE @sqlText nvarchar(max);
	  
		-- EWH Feb 4, 2016: formatted below for readability, and fixed errors with casting
		-- subfees
		-- TODO: test to make sure that the "+ +" part of the new_formula column will work when incr_step and incr_amount are both not null
		SET @sqlText = N'
			select
				license_no
				, item as code
				, formula
				, new_formula 
					= case	when isnull(incr_step, 0) <> 0  and isnull(incr_amount, 0) <> 0 
							then ''(('' + convert (varchar, ' + @FieldName + ') +''-('' + convert(varchar,min_range  ) +''-'' + convert(varchar,incr_step  ) + ''))'' + + ''/'' + convert(varchar, incr_step) + ''*'' + convert(varchar, incr_amount) + '') + '' + convert(varchar, base_amount)
							else cast(base_amount as varchar) 
							end
				, result
					= case	when isnull(incr_step, 0) <> 0 and isnull(incr_amount, 0) <> 0 
							then (( ' + @FieldName + ' - (min_range - incr_step)) / incr_step * incr_amount) + base_amount
							else base_amount 
							end
			into formula_update
			from
				(
				select sf.license_no, item, formula, basis, ' + @FieldName + ', [BASE_AMOUNT], MIN_RANGE, MAX_RANGE, isnull([INCR_STEP], 0) [INCR_STEP], isnull([INCR_AMOUNT], 0) [INCR_AMOUNT]
				from LicenseRenew_Review_SubFees sf 
					inner join LICENSE2_UDF t on sf.LICENSE_NO = t.LICENSE_NO
					inner join [Prmry_FeeTables] ft on ''[''+[TABLENAME]+'']'' = sf.FORMULA 
				where groupname=''LICENSE2'' 
					and cast(' + @FieldName + ' as float) between MIN_RANGE and MAX_RANGE
				) as tbl	   
		'
		exec (@sqlText)
	

		update sf set Calc_value=result,Formula_working_col=new_formula
		from LicenseRenew_Review_SubFees sf inner join formula_update as tbl on sf.license_no=tbl.license_no 
		and sf.item=tbl.code
	 
	
		IF  EXISTS( SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'formula_update')
		BEGIN
			drop table formula_update
		end
    
		set  @sqlText ='';
		SET @sqlText = N'update sf set 
		Formula_working_col=replace(formula,'''+@FieldName+''',t.'+ @FieldName +')
	   
		from LicenseRenew_Review_SubFees sf inner join LICENSE2_UDF t 
		on	sf.LICENSE_NO=t.LICENSE_NO
		where t.'+ @FieldName +' is not null and charindex('''+@FieldName+''',FORMULA)<>0'
		exec (@sqlText)
	  
	end


	FETCH NEXT
	FROM @getcounterID INTO @FieldName
END
CLOSE @getcounterID
DEALLOCATE @getcounterID



SET @getcounterID = CURSOR FOR

SELECT name
FROM #TempTable

OPEN @getcounterID
FETCH NEXT
FROM @getcounterID INTO @FieldName
WHILE @@FETCH_STATUS = 0
BEGIN

	-- update Formula_working_col
	IF EXISTS (SELECT distinct basis from LicenseRenew_Review_Fees inner join [Prmry_FeeTables]
	on '['+[TABLENAME]+']'=LicenseRenew_Review_Fees.FORMULA  WHERE basis=@FieldName) 


	if @FieldName <>'LICENSE_NO'
	begin
  
		IF  EXISTS( SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'formula_update')
		BEGIN
			drop table formula_update
		end

		-- EWH Feb 4, 2016: formatted below for readability, and fixed errors with casting
		-- fees
		-- TODO: test to make sure that the "+ +" part of the new_formula column will work when incr_step and incr_amount are both not null
		SET @sqlText = N'
			select 
					license_no
				, code
				, formula
				, new_formula
					= case	when isnull(incr_step, 0) <> 0 and isnull(incr_amount, 0) <> 0
							then ''((''+ convert(varchar, ' + @FieldName + ') +''-('' + convert(varchar, min_range) + ''-'' + convert(varchar, incr_step) + ''))'' + + ''/'' + convert(varchar, incr_step) + ''*'' + convert(varchar, incr_amount) + '') +'' + convert(varchar, base_amount)
							else cast(base_amount as varchar) 
							end
				, result 
					= case	when isnull(incr_step, 0) <> 0 and isnull(incr_amount, 0) <> 0 
							then (( ' + @FieldName + ' - (min_range - incr_step)) / incr_step * incr_amount) + base_amount
							else base_amount 
							end 					 					 
			into formula_update
			from
				(
				select f.license_no, code, formula, basis, ' + @FieldName + ', [BASE_AMOUNT], MIN_RANGE, MAX_RANGE, isnull([INCR_STEP], 0) [INCR_STEP], isnull([INCR_AMOUNT], 0) [INCR_AMOUNT]
				from LicenseRenew_Review_Fees f 
					inner join LICENSE2_UDF t on f.LICENSE_NO = t.LICENSE_NO
					inner join [Prmry_FeeTables] ft on ''[''+[TABLENAME]+'']'' = f.FORMULA 
				where groupname=''LICENSE2'' 
					and cast(' + @FieldName + ' as float) between MIN_RANGE and MAX_RANGE
				) as tbl
		'
		exec (@sqlText)
	

		update sf set Calc_value=result,Formula_working_col=new_formula
		from LicenseRenew_Review_Fees sf inner join formula_update as tbl on sf.license_no=tbl.license_no 
		and sf.code=tbl.code
    
	 
		SET @sqlText =''

		SET @sqlText = N'update sf set 
		Formula_working_col=replace(formula,'''+@FieldName+''',t.'+ @FieldName +')
		from LicenseRenew_Review_Fees sf inner join LICENSE2_UDF t 
		on	sf.LICENSE_NO=t.LICENSE_NO
		where t.'+ @FieldName +' is not null and charindex('''+@FieldName+''',FORMULA)<>0'
		exec (@sqlText)

	 end

	FETCH NEXT
	FROM @getcounterID INTO @FieldName
END
CLOSE @getcounterID
DEALLOCATE @getcounterID



------------------



declare @t2 as table(val varchar(20))
declare @exp varchar(20)
declare @code varchar (50)
declare @lic varchar (50)
declare @i int,@icount int
--calculate subfees
set @i=1;

 Select @icount=  COUNT(*) from dbo.LicenseRenew_Review_SubFees where 
  charindex('min',Formula_working_col)=0 and charindex('max',Formula_working_col)=0
 and Formula_working_col <>'' and Calc_value is null

while @i<@icount
begin
    select top(@i) @exp =Formula_working_col,@lic=license_no,@code=item from  dbo.LicenseRenew_Review_SubFees
	 where Calc_value is null and charindex('min',Formula_working_col)=0 and charindex('max',Formula_working_col)=0
	   and Formula_working_col <>'' order by Formula_working_col
     
    insert into @t2
    exec ('select '+@exp)  
  update  dbo.LicenseRenew_Review_SubFees set Calc_value=(select val from @t2) where license_no=@lic and item=@code
  
	delete  from @t2
    set @i=@i+1
end
-- calculate fees
set @i=1;

 Select @icount=  COUNT(*) from dbo.LicenseRenew_Review_Fees where 
 charindex('min',Formula_working_col)=0 and charindex('max',Formula_working_col)=0
 and Formula_working_col <>'' and Formula_working_col is not null  and Calc_value is null 

while @i<@icount
begin
    select top(@i) @exp =Formula_working_col,@lic=license_no,@code=code from  dbo.LicenseRenew_Review_Fees
	 where Calc_value is null and charindex('min',Formula_working_col)=0 and charindex('max',Formula_working_col)=0
	and Formula_working_col <>'' and  Formula_working_col is not null  order by Formula_working_col
     
    insert into @t2
    exec ('select '+@exp)  
  update  dbo.LicenseRenew_Review_Fees set Calc_value=(select val from @t2) where license_no=@lic and code=@code
  
	delete  from @t2
    set @i=@i+1
end








-- EWH Jan 28, 2016 [start]

-- initialize formula_working_col
update LicenseRenew_Review_Fees set Formula_working_col = FORMULA where Formula_working_col='' and formula <> '[ITEMIZE]' and formula <> '' and formula is not null and isnumeric(formula)=0
update LicenseRenew_Review_SubFees set Formula_working_col = FORMULA where Formula_working_col='' and formula <> '' and formula is not null and isnumeric(formula)=0



-- ==================================================================================================================================================================
--		REPLACE {FEE CODES} IN _FEES AND _SUBFEES TABLES WITH FORMULAS FROM PRMRY_FEESCHEDULE
-- ==================================================================================================================================================================

----------------------------------------------------------------------------------------------------------------------------------------------------------
-- stuff used by both fees and subfees 
declare @formulas table (parent_code varchar(12), code varchar(12), formula varchar(max))	-- contains place holder codes and values
declare @xml xml
declare @updates table (recordid varchar(30), formula varchar(2000))						-- contains updated formulas by recordid
----------------------------------------------------------------------------------------------------------------------------------------------------------


-- *****************************************************
-- ** REPLACE FEE CODES IN FORMULAS FOR SUBFEES TABLE **
-- *****************************************************

-- populate lookup table that contains place holder codes and values --> we can shorten the string here for {fee codes} by limiting to formulas like '%{%'
insert into @formulas
select parent_code, code, formula from Prmry_FeeSchedule
where charindex('{' + parent_code + '}',(select '' + Formula_working_col from LicenseRenew_Review_SubFees where Formula_working_col like '%{%' for xml path(''))) > 0
or charindex('{' + code + '}',(select '' + Formula_working_col from LicenseRenew_Review_SubFees where Formula_working_col like '%{%' for xml path(''))) > 0
--select * from @formulas

-- convert subfees recordid and formula columns into xml --> we can shorten the string here for {fee codes} by limiting to formulas like '%{%'
set @xml =(select recordid [r], Formula_working_col [f] from LicenseRenew_Review_SubFees where Formula_working_col like '%{%' for xml path('row'))
--set @xml =(select recordid, Formula_working_col [formula] from LicenseRenew_Review_SubFees where formula like '%{%' for xml path('row'))
--select @xml

-- replace the placeholders in the string with the formula, making sure to remove curly braces and encapsulate formula in parentheses
select @xml = replace(cast(@xml as nvarchar(max)), '{' + f.parent_code + '}', '(' + f.formula + ')') from @formulas f -- parent_code
select @xml = replace(cast(@xml as nvarchar(max)), '{' + f.code + '}', '(' + f.formula + ')') from @formulas f -- code
--select @xml

-- convert xml back into table
insert into @updates 
select xc.value('(r)[1]','varchar(30)') recordid, xc.value('(f)[1]','varchar(2000)') Formula_working_col
--select xc.value('(recordid)[1]','varchar(30)') recordid, xc.value('(formula)[1]','varchar(2000)') Formula_working_col
from @xml.nodes('/row') xt(xc)
--select * from @updates

-- update the Formula_working_col with the updated formulas
update LRRS
set Formula_working_col = U.formula 
from LicenseRenew_Review_SubFees LRRS inner join @updates U on LRRS.RECORDID = U.recordid



-- **************************************************
-- ** REPLACE FEE CODES IN FORMULAS FOR FEES TABLE **
-- **************************************************


-- populate lookup table that contains place holder codes and values --> we can shorten the string here for {fee codes} by limiting to formulas like '%{%'
delete @formulas -- clear table variable since we are reusing it
insert into @formulas
select parent_code, code, formula from Prmry_FeeSchedule
where charindex('{' + parent_code + '}',(select '' + Formula_working_col from LicenseRenew_Review_Fees where Formula_working_col like '%{%' for xml path(''))) > 0
or charindex('{' + code + '}',(select '' + Formula_working_col from LicenseRenew_Review_Fees where Formula_working_col like '%{%' for xml path(''))) > 0
--select * from @formulas

-- convert fees recordid and formula columns into xml --> we can shorten the string here for {fee codes} by limiting to formulas like '%{%'
set @xml =(select recordid [r], Formula_working_col [f] from LicenseRenew_Review_Fees where Formula_working_col like '%{%' for xml path('row'))
--select @xml

-- replace the placeholders in the string with the formula, making sure to remove curly braces and encapsulate formula in parentheses
select @xml = replace(cast(@xml as nvarchar(max)), '{' + f.parent_code + '}', '(' + f.formula + ')') from @formulas f -- parent_code
select @xml = replace(cast(@xml as nvarchar(max)), '{' + f.code + '}', '(' + f.formula + ')') from @formulas f -- code
--select @xml

-- convert xml back into table
delete @updates -- clear table variable since we are reusing it
insert into @updates 
select xc.value('(r)[1]','varchar(30)') recordid, xc.value('(f)[1]','varchar(2000)') Formula_working_col
--select xc.value('(recordid)[1]','varchar(30)') recordid, xc.value('(formula)[1]','varchar(2000)') Formula_working_col
from @xml.nodes('/row') xt(xc)
--select * from @updates

-- update the Formula_working_col with the updated formulas
update LRRF
set Formula_working_col = U.formula 
from LicenseRenew_Review_Fees LRRF inner join @updates U on LRRF.RECORDID = U.recordid
----------------------------------------------------------------------------------------------------------------------------------------------------------

-- ==================================================================================================================================================================
-- EWH Jan 28, 2016 [end]




-- TODO: check if there are any formulas that are not numeric - if not, we can skip this bit




-- EWH Jan 14, 2016 [start]
-- TODO: turn this into a stored proc so it can be used elsewhere
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- populate calc_value for formulas containing {UDF columns}
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- make list of columns declared as varchar(max)
declare @udf_list_varchar varchar(max)
set @udf_list_varchar = (SELECT '[' + column_name + '] varchar(max) null,' FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'License2_UDF' and column_name not in ('LICENSE_NO','RECORDID') for xml path(''))
if len(@udf_list_varchar) > 0 set @udf_list_varchar = left(@udf_list_varchar,len(@udf_list_varchar)-1) -- get rid of last comma

-- create temp table
IF OBJECT_ID('tempdb..#udf_varchar') IS NOT NULL DROP TABLE #udf_varchar
create table #udf_varchar (LICENSE_NO varchar(20) not null, RECORDID varchar(30) null)
declare @alter varchar(max)
set @alter = 'alter table #udf_varchar add ' + @udf_list_varchar
exec (@alter)

-- make list of columns with cast to varchar(max)
declare @udf_list_varchar_cast varchar(max)
set @udf_list_varchar_cast = (SELECT 'cast([' + column_name + '] as varchar) [' + column_name + '],' FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'License2_UDF' and column_name not in ('LICENSE_NO','RECORDID') for xml path(''))
set @udf_list_varchar_cast = 'select license_no,recordid,' + @udf_list_varchar_cast
set @udf_list_varchar_cast = left(@udf_list_varchar_cast,len(@udf_list_varchar_cast)-1) -- get rid of last comma

-- insert license2 records with varchar UDF values
declare @copy_udf_sql varchar(max) 
set @copy_udf_sql = 'insert into #udf_varchar ' + @udf_list_varchar_cast + ' from License2_UDF'
exec (@copy_udf_sql) 

-- put list of columns into text string
declare @udf_list varchar(max)
set @udf_list = (SELECT '[' + column_name + '],' FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'License2_UDF' and column_name not in ('LICENSE_NO','RECORDID') for xml path(''))
if len(@udf_list) > 0 set @udf_list = left(@udf_list,len(@udf_list)-1) -- get rid of last comma

--create table variable to hold unpivoted UDF values
declare @udf table (LICENSE_NO varchar(20) not null, RECORDID varchar(30) null, UDF_NAME varchar(max) not null, UDF_VALUE varchar(max) null)
declare @sql nvarchar(max)
set @sql=
'select LICENSE_NO,RECORDID, [UDF_NAME], [UDF_VALUE] from #udf_varchar
unpivot
(
	[UDF_VALUE]
	for [UDF_NAME] in (' + @udf_list + ')
) as a
where LICENSE_NO in (select LICENSE_NO from LicenseRenew_License_List)
'

insert into @udf
exec (@sql)
drop table #udf_varchar -- we should have everything in @udf now
-- EWH Jan 14, 2016 [end]

delete @formulas -- make sure there is no data in here in case of copy pasta


--***************************************************************************************************************************************************
-- mdm - 02/02/16 - replace udfs in formulas for both fees and subfees
--***************************************************************************************************************************************************

Select Identity(int, 1,1) AS myID, *
Into   #Temp
From   @udf

Declare @RowCount int;
Declare @CurrentRow int;
Declare @LicNo as varchar(255)
Declare @UDFName as varchar(255)
Declare @UDFValue as varchar(255)
declare @sqlSubFee as varchar(max)

Select @RowCount = Count(myID) From #Temp
set @CurrentRow = 1

WHILE (@currentRow <= @rowCount)
BEGIN
  SELECT TOP 1
     @LicNo = rt.[License_no]
    ,@UDFName = rt.[UDF_Name]
    ,@UDFValue = rt.[UDF_Value]
  FROM (
    SELECT ROW_NUMBER() OVER (
        ORDER BY t.[License_no], t.[UDF_Name], t.[UDF_Value]
       ) AS [RowNumber]
      ,t.[License_no]
      ,t.[UDF_Name]
      ,t.[UDF_Value]
    FROM #Temp t
  ) rt
  WHERE rt.[RowNumber] = @currentRow;

  
       set @sqlSubFee = 'update LicenseRenew_Review_SubFees'
              set @sqlSubFee = @sqlSubFee + ' set  Formula_working_col = replace(Formula_working_col,''' + @UDFName + ''','''  + @UDFValue + ''')' 
              set @sqlSubFee = @sqlSubFee + ' where LicenseRenew_Review_SubFees.LICENSE_NO = ''' + @LicNo + ''''  
              set @sqlSubFee = @sqlSubFee + ' and CHARINDEX(''' + @UDFName + ''',  Formula_working_col,0)>0'

              exec (@sqlSubFee)

              set @sqlSubFee = 'update LicenseRenew_Review_Fees'
              set @sqlSubFee = @sqlSubFee + ' set  Formula_working_col = replace(Formula_working_col,''' + @UDFName + ''','''  + @UDFValue + ''')' 
              set @sqlSubFee = @sqlSubFee + ' where LicenseRenew_Review_Fees.LICENSE_NO = ''' + @LicNo + ''''  
              set @sqlSubFee = @sqlSubFee + ' and CHARINDEX(''' + @UDFName + ''',  Formula_working_col,0)>0'

              exec (@sqlSubFee)

    SET @currentRow = @currentRow + 1;


End
--***************************************************************************************************************************************************



-- D12.  Update formula for QTY in SubFees
-- EWH Feb 4, 2016: updated QTY replacement to exlude when quantity column value is null
UPDATE LicenseRenew_Review_SubFees
SET Formula_working_col = REPLACE(Formula_working_col, 'QTY', CAST(LicenseRenew_Review_SubFees.QUANTITY as varchar(12)))
WHERE charindex('qty',Formula) > 0 and QUANTITY is not null
UPDATE LicenseRenew_Review_Fees
SET Formula_working_col = REPLACE(Formula_working_col, 'QTY', CAST(LicenseRenew_Review_Fees.QUANTITY as varchar(12)))
WHERE charindex('qty',Formula) > 0 and QUANTITY is not null


-- EWH Feb 4, 2016: nullify formula_working_col that contain something other than numbers or mathematical operations
update LicenseRenew_Review_Fees set formula_working_col=NULL WHERE formula_working_col LIKE '%[^0-9.*/+-()]%'
update LicenseRenew_Review_SubFees set formula_working_col=NULL WHERE formula_working_col LIKE '%[^0-9.*/+-()]%'


-- update the Calc_value with the math from Formula_working_col for fees
declare @math_fees varchar(max)
set @math_fees = (select '  update LicenseRenew_Review_Fees set Calc_value=try_cast(' + Formula_working_col + ' as float) where recordid=''' + recordid + ''''
from LicenseRenew_Review_Fees 
where amount is null and Calc_value is null and Formula_working_col <>''
for xml path(''))
exec (@math_fees)
set @math_fees=''


-- update the Calc_value with the math from Formula_working_col for subfees
declare @math_subfees varchar(max)
set @math_subfees = (select '  update LicenseRenew_Review_SubFees set Calc_value=try_cast(' + Formula_working_col + ' as float) where recordid=''' + recordid + ''''
from LicenseRenew_Review_SubFees 
where amount is null and Calc_value is null and Formula_working_col <>''
for xml path(''))
exec (@math_subfees)
set @math_subfees=''
-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- EWH Jan 28, 2016 [end]



----get all the fees finalized:

update [dbo].[LicenseRenew_Review_Fees] set amount=Calc_value
update [dbo].[LicenseRenew_Review_subFees] set amount=Calc_value
update [dbo].[LicenseRenew_Review_Fees] set amount=subfees_total from (select sum([Calc_value]) as subfees_total,[PARENTID] from [dbo].[LicenseRenew_Review_SubFees] group by [PARENTID]) tbl inner join [LicenseRenew_Review_Fees] lf on [PARENTID]=lf.RECORDID

UPDATE lm SET Balance_Due =total_due
from [dbo].[LicenseRenew_Review_Main] lm inner  join 
 (select license_no, sum(isnull(amount,0)) - sum(isnull(paid_amount,0)) as total_due FROM [dbo].[LicenseRenew_Review_Fees] lrf 
 group by lrf.license_no) as tbl
	on lm.LICENSE_NO=tbl.LICENSE_NO

END



GO
