USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ClearTRAKiT9Login]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_ClearTRAKiT9Login]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ClearTRAKiT9Login]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
	INSERTS OR UPDATES TRAKiT9_LOGIN Table value for the corresponding UID
*/
-- =====================================================================
-- Created By: 
--	HAN - 11/11/13
-- =====================================================================  
CREATE Procedure [dbo].[tsp_T9_ClearTRAKiT9Login](
	@UID VARCHAR(50)
)
AS
BEGIN
	UPDATE  TRAKIT9_LOGIN SET loginID = NULL, loginExpireDate = NULL where UID = @UID
END

GO
