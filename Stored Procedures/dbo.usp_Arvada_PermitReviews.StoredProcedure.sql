USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_PermitReviews]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[usp_Arvada_PermitReviews]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_PermitReviews]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_Arvada_PermitReviews]
	@Permit_No varchar(30)
as
/**********************************************************************************************************************
* Creator:      	 
* Date:         	 
* Title:			 
* Description:  	 
* Examples:    

EXECUTE dbo.[usp_Arvada_SpecialEventPermit]  'SPE15-00034'

update m
set issued = '10/01/2015'
from permit_Main m
where permit_no = 'SPE15-00034'

select * 
from permit_parcels
where permit_no = 'SPE15-00034'
					 
* Modifications:  	
  Developer Name		Date		Description
  ------------------	----------	----------------------------------------------------------- 
*
**********************************************************************************************************************/

set nocount on
 
 --------------------------
 --- Declare Variables   --
 --------------------------

 
 --------------------------
 --- Create Temp Tables  --
 --------------------------
 
 --------------------------
 --- Body of Procedure   --
 --------------------------


 select 
	ReviewType,
	Date_Sent,
	Contact,
	[Status],
	DATE_RECEIVED,
	NOTES,
	Remarks,
	Assigned_Date,
	stuff(
		(	select CHAR(13) + pn.notes 
			from dbo.Prmry_Notes AS pn  
			WHERE   pn.SubGroupRecordID = r.recordid
			for xml path(''), root('MyString'), type 
		).value('/MyString[1]','nvarchar(max)') ,1,1,'') as Notes
 from dbo.permit_reviews r
 where permit_no = @Permit_No
 order by 
	ReviewType

GO
