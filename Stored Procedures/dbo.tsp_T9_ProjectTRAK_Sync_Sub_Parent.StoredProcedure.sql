USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ProjectTRAK_Sync_Sub_Parent]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_ProjectTRAK_Sync_Sub_Parent]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ProjectTRAK_Sync_Sub_Parent]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

					  
CREATE PROCEDURE [dbo].[tsp_T9_ProjectTRAK_Sync_Sub_Parent](
	@ExeMode INT,
	@SyncStatus VARCHAR(50),
	@ParentProjectNO VARCHAR(50),
	@SubProjectNO VARCHAR(50) = NULL
	)
AS
BEGIN
	SET NOCOUNT ON;

	IF @ExeMode = 1 --On creation of sync sub project move all auto insert free, inspections,reviews to sync Parent			
	BEGIN
		UPDATE PROJECT_MAIN
		SET [STATUS] = @SyncStatus
		WHERE PROJECT_NO = @SubProjectNO
			AND len(PARENT_PROJECT_NO) > 0

		UPDATE PROJECT_INSPECTIONS
		SET PROJECT_NO = @ParentProjectNO
		WHERE PROJECT_NO = @SubProjectNO
		
		-- remove duplicate inspections
		;WITH CTE_Inspections (InspectionType, DuplicateCount)
		AS 
		(
			SELECT InspectionType, ROW_NUMBER() OVER (PARTITION BY InspectionType ORDER BY InspectionType) AS DuplicateCount
			FROM INSPECTIONS
			WHERE ActivityID = @ParentProjectNO
		)
		DELETE
		FROM CTE_Inspections
		WHERE DuplicateCount > 1

		UPDATE PROJECT_REVIEWS
		SET PROJECT_NO = @ParentProjectNO
		WHERE PROJECT_NO = @SubProjectNO
			
		-- remove duplicate reviews
		;WITH CTE_Reviews (ReviewType, DuplicateCount)
		AS 
		(
			SELECT ReviewType, ROW_NUMBER() OVER (PARTITION BY ReviewType ORDER BY ReviewType) AS DuplicateCount
			FROM REVIEWS
			WHERE ActivityID = @ParentProjectNO
		)
		DELETE
		FROM CTE_Reviews
		WHERE DuplicateCount > 1

		UPDATE PROJECT_FEES
		SET PROJECT_NO = @ParentProjectNO
		WHERE PROJECT_NO = @SubProjectNO

		UPDATE PROJECT_SUBFEES
		SET PROJECT_NO = @ParentProjectNO
		WHERE PROJECT_NO = @SubProjectNO
	
		UPDATE PROJECT_MAIN
		SET LOCKID = 'SYNC' + substring(Recordid, charindex(':', Recordid, 0), 50), fees_charged=0, BALANCE_DUE= 0 -- made balance_due and fees_charged null as subfee shudn't have any fee data SA 03302015
		WHERE PROJECT_NO = @SubProjectNO

		UPDATE Project_Main
		SET PROJECT_NAME = ppm.PROJECT_NAME,
			DESCRIPTION = ppm.DESCRIPTION,
			site_addr = ppm.SITE_ADDR,
			SITE_STREETID = ppm.SITE_STREETID,
			SITE_NUMBER = ppm.SITE_NUMBER,
			SITE_STREETNAME = ppm.SITE_STREETNAME,
			SITE_UNIT_NO = ppm.SITE_UNIT_NO,
			SITE_CITY = ppm.SITE_CITY,
			SITE_STATE = ppm.SITE_STATE,
			SITE_ZIP = ppm.SITE_ZIP,
			APPROVED = ppm.APPROVED,
			APPLIED = ppm.APPLIED,
			CLOSED = ppm.CLOSED,
			EXPIRED = ppm.EXPIRED,
			STATUS_DATE = ppm.STATUS_DATE,
			OTHER_DATE1 = ppm.OTHER_DATE1,
			APPLIED_BY = ppm.APPLIED_BY, -- Milestone dates dont have user id copied from parent #36624 Sa 040615
			APPROVED_BY = ppm.APPROVED_BY,
			CLOSED_BY = ppm.CLOSED_BY,
			EXPIRED_BY = ppm.EXPIRED_BY,
			STATUS_BY = ppm.STATUS_BY,
			OTHER_BY1 = ppm.OTHER_BY1
		FROM
		(
			SELECT *
			FROM project_main
			WHERE project_no = @ParentProjectNO
			) ppm
		WHERE Project_Main.parent_project_no = @ParentProjectNO
			AND Project_Main.STATUS = @SyncStatus
	END
	ELSE IF @ExeMode = 2 -- For syncing all sub projects of Parent projects in SYNC always
	BEGIN
		SELECT *
		FROM Project_Main
		WHERE project_no = @ParentProjectNO
			OR parent_project_no = @ParentProjectNO

		UPDATE Project_Main
		SET PROJECT_NAME = ppm.PROJECT_NAME,
			DESCRIPTION = ppm.DESCRIPTION,
			site_addr = ppm.SITE_ADDR,
			SITE_STREETID = ppm.SITE_STREETID,
			SITE_NUMBER = ppm.SITE_NUMBER,
			SITE_STREETNAME = ppm.SITE_STREETNAME,
			SITE_UNIT_NO = ppm.SITE_UNIT_NO,
			SITE_CITY = ppm.SITE_CITY,
			SITE_STATE = ppm.SITE_STATE,
			SITE_ZIP = ppm.SITE_ZIP,
			APPROVED = ppm.APPROVED,
			APPLIED = ppm.APPLIED,
			CLOSED = ppm.CLOSED,
			EXPIRED = ppm.EXPIRED,
			STATUS_DATE = ppm.STATUS_DATE,
			OTHER_DATE1 = ppm.OTHER_DATE1,
			APPLIED_BY = ppm.APPLIED_BY,        -- Milestone dates dont have user id copied from parent #36624 Sa 040615
			APPROVED_BY = ppm.APPROVED_BY,
			CLOSED_BY = ppm.CLOSED_BY,
			EXPIRED_BY = ppm.EXPIRED_BY,
			STATUS_BY = ppm.STATUS_BY,
			OTHER_BY1 = ppm.OTHER_BY1
		FROM (
			SELECT *
			FROM project_main
			WHERE project_no = @ParentProjectNO
			) ppm
		WHERE Project_Main.parent_project_no = @ParentProjectNO
			AND Project_Main.STATUS = @SyncStatus

		UPDATE PROJECT_MAIN
		SET LOCKID = 'SYNC' + substring(Recordid, charindex(':', Recordid, 0), 50)
		WHERE PARENT_PROJECT_NO = @ParentProjectNO
			AND STATUS = @SyncStatus
	END
	ELSE IF @ExeMode = 3
	BEGIN -- For sub projects that are already created as NON sysnc status and now changes to SYNC
		SELECT *
		FROM Project_Main
		WHERE project_no = @ParentProjectNO

		UPDATE Project_Main
		SET PROJECT_NAME = ppm.PROJECT_NAME,
			DESCRIPTION = ppm.DESCRIPTION,
			site_addr = ppm.SITE_ADDR,
			SITE_STREETID = ppm.SITE_STREETID,
			SITE_NUMBER = ppm.SITE_NUMBER,
			SITE_STREETNAME = ppm.SITE_STREETNAME,
			SITE_UNIT_NO = ppm.SITE_UNIT_NO,
			SITE_CITY = ppm.SITE_CITY,
			SITE_STATE = ppm.SITE_STATE,
			SITE_ZIP = ppm.SITE_ZIP,
			APPROVED = ppm.APPROVED,
			APPLIED = ppm.APPLIED,
			CLOSED = ppm.CLOSED,
			EXPIRED = ppm.EXPIRED,
			STATUS_DATE = ppm.STATUS_DATE,
			OTHER_DATE1 = ppm.OTHER_DATE1,
			APPLIED_BY = ppm.APPLIED_BY,           -- Milestone dates dont have user id copied from parent #36624 Sa 040615
			APPROVED_BY = ppm.APPROVED_BY,
			CLOSED_BY = ppm.CLOSED_BY,
			EXPIRED_BY = ppm.EXPIRED_BY,
			STATUS_BY = ppm.STATUS_BY,
			OTHER_BY1 = ppm.OTHER_BY1
		FROM (
			SELECT *
			FROM project_main
			WHERE project_no = (
					SELECT PARENT_PROJECT_NO
					FROM project_main
					WHERE PROJECT_NO = @ParentProjectNO
					)
			) ppm
		WHERE Project_Main.project_no = @ParentProjectNO

		UPDATE PROJECT_MAIN
		SET LOCKID = 'SYNC' + substring(Recordid, charindex(':', Recordid, 0), 50)
		WHERE PROJECT_NO = @ParentProjectNO
	END
	ELSE IF @ExeMode=4  -- update site address on locked sub projects if site address changed after creation .API unable to update as subprojects are locked for user #36626 
	BEGIN
	UPDATE Project_Main
		SET PROJECT_NAME = ppm.PROJECT_NAME,
			DESCRIPTION = ppm.DESCRIPTION,
			site_addr = ppm.SITE_ADDR,
			SITE_STREETID = ppm.SITE_STREETID,
			SITE_NUMBER = ppm.SITE_NUMBER,
			SITE_STREETNAME = ppm.SITE_STREETNAME,
			SITE_UNIT_NO = ppm.SITE_UNIT_NO,
			SITE_CITY = ppm.SITE_CITY,
			SITE_STATE = ppm.SITE_STATE,
			SITE_ZIP = ppm.SITE_ZIP,
			APPROVED = ppm.APPROVED,
			APPLIED = ppm.APPLIED,
			CLOSED = ppm.CLOSED,
			EXPIRED = ppm.EXPIRED,
			STATUS_DATE = ppm.STATUS_DATE,
			OTHER_DATE1 = ppm.OTHER_DATE1,
			APPLIED_BY = ppm.APPLIED_BY,   -- Milestone dates dont have user id copied from parent #36624 Sa 040615
			APPROVED_BY = ppm.APPROVED_BY,
			CLOSED_BY = ppm.CLOSED_BY,
			EXPIRED_BY = ppm.EXPIRED_BY,
			STATUS_BY = ppm.STATUS_BY,
			OTHER_BY1 = ppm.OTHER_BY1
		FROM
		(
			SELECT *
			FROM project_main
			WHERE project_no = @ParentProjectNO
			) ppm
		WHERE Project_Main.parent_project_no = @ParentProjectNO
			AND Project_Main.STATUS = @SyncStatus
	END
END

--**************Andrew Thomas 05/21/18 ****END**********
						
/******************************************************************************************************************************************************/	


GO
