USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_SYS_Data_AddMissingDefaultConstraints]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_SYS_Data_AddMissingDefaultConstraints]
GO
/****** Object:  StoredProcedure [dbo].[tsp_SYS_Data_AddMissingDefaultConstraints]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  
					  

CREATE PROCEDURE [dbo].[tsp_SYS_Data_AddMissingDefaultConstraints] (
@OutputPath Varchar(1000) = Null
)

AS
BEGIN
  SET NOCOUNT ON

  --mdm - 09/20/13; sample @OutputPath:  'c:\mayo\Step6_DS_AddMissingDefaultConstraints.sql'
  
	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[zAddDefaultConstraintsTemp]') AND type in (N'U'))
		BEGIN
		create table zAddDefaultConstraintsTemp
		(
		SqlToExec varchar(2000)
		)
		END

		truncate table zAddDefaultConstraintsTemp

		INSERT INTO zAddDefaultConstraintsTemp
		SELECT 'IF NOT EXISTS (SELECT * from sys.default_constraints sdc where name = ''' + sdc.name + ''')   
		ALTER TABLE [' + OBJECT_NAME(sdc.parent_object_id) + '] ADD CONSTRAINT [' + sdc.name + '] DEFAULT ' + sdc.definition + ' FOR [' + sc.name + '];'
from sys.default_constraints sdc inner join sys.columns sc on OBJECT_NAME(sdc.parent_object_id) = OBJECT_NAME(sc.object_id)  where sdc.parent_column_id =sc.column_id

		declare @FileName as varchar(1000)
		If @OutputPath Is Null OR @OutputPath = ''
			Set @FileName = 'C:\crw\TEMP\Step4_DS_AddMissingPrimaryKeys.sql'
		ELSE
			Set @FileName = @OutputPath
		--set @FileName = 'c:\mayo\Step6_DS_AddMissingDefaultConstraints.sql'
		--select @FileName

		declare @dbName varchar(200)
		set @dbName = db_name()
		--select @dbName


		declare @cmd varchar(2000)
		set @cmd = 'bcp "select SqlToExec from '
		set @cmd = @cmd + @dbname + '.dbo.zAddDefaultConstraintsTemp order by SqlToExec" queryout "'
		set @cmd = @cmd + @FileName + '" -Utrakit -Ptrakit -c -S' + @@SERVERNAME

		--select @cmd
		exec xp_cmdshell @cmd

		select SqlToExec from zAddDefaultConstraintsTemp order by SqlToExec

--drop table zAddDefaultConstraintsTemp

END


GO
