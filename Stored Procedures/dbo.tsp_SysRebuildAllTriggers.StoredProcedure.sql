USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_SysRebuildAllTriggers]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_SysRebuildAllTriggers]
GO
/****** Object:  StoredProcedure [dbo].[tsp_SysRebuildAllTriggers]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  					  
					  					  					  /*

tsp_SysRebuildAllTriggers
*/

CREATE Procedure [dbo].[tsp_SysRebuildAllTriggers]

As

declare @Pref varchar(20)
--declare @SQL varchar(8000)

select @Pref = value1 from Prmry_Preferences where Item like '%SHOW_AUDIT_HISTORY%'
Set @Pref = IsNull(@Pref,'NO')
Print(@Pref)
--RETURN

if @Pref = 'YES'
    exec dbo.tsp_SysRebuildAuditTriggers
else
    exec dbo.tsp_SysRebuildAuditTriggers @DropOnly=1


select @Pref = value1 from Prmry_Preferences where Item like '%LANDTRAK_SHOW_IMPORT_HISTORY%'
Set @Pref = IsNull(@Pref,'NO')
Print(@Pref)
--RETURN

if @Pref = 'YES'
    exec dbo.tsp_SysRebuildGeoChangeLogTriggers
else
    exec dbo.tsp_SysDropGeoChangeLogTriggers









GO
