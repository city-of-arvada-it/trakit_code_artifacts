USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearchDelete]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_AdvSearchDelete]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearchDelete]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  					  


                                    
-- ==========================================================================================
-- Revision History:
--       2014-03-03 - mdm - added to sourcesafe
-- ==========================================================================================   

CREATE PROCEDURE [dbo].[tsp_T9_AdvSearchDelete] (
       @RecordID as varchar(10)
)

AS
BEGIN

Declare @SQL Varchar(8000)


set @SQL = 'DELETE FROM Prmry_AdvSearchConfigItem where Recordid =' + @Recordid 

--print(@sql)
exec(@sql)

set @sql = 'DELETE FROM Prmry_AdvSearchConfigDetail where ParentRecordid =' + @Recordid
--print(@sql)
exec(@sql)

END







GO
