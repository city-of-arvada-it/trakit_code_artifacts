USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_FindpermitByAmount]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[usp_Arvada_FindpermitByAmount]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_FindpermitByAmount]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_Arvada_FindpermitByAmount]
	@Amount decimal(36,4),
	@TranDate datetime
as 
begin

/*
exec usp_Arvada_FindpermitByAmount '279.94', '07/31/2017'
*/

	select permit_no
	from dbo.permit_fees
	where
		datediff(day, @TranDate, assessed_date) in (-2,-1,0,1,2)
	and (paid_amount is null or paid_amount = 0)
	and ASSESSED_BY = 'ECON'
	group by permit_no
	having sum(amount) = @Amount



end
GO
