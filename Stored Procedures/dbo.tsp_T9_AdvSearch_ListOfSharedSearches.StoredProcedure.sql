USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearch_ListOfSharedSearches]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_AdvSearch_ListOfSharedSearches]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearch_ListOfSharedSearches]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[tsp_T9_AdvSearch_ListOfSharedSearches]

	@UserID varchar(50) = NULL,
	@ReturnHTML bit = 0,
	@FilterList bit = 1, -- filter results by DisplayInUserList = 1
	@PickListOrder int = -1 -- use this order instead of the one specified in user options [PickListOrder] column

AS
BEGIN

if @UserID is not null
begin
	-- get the [PickListOrder] value from user options
	if @PickListOrder = -1 select top 1 @PickListOrder = [PickListOrder] from [Prmry_AdvSearch_UserOptions] where UserID=@UserID
	
	-- identify the order by clause from user options
	declare @OrderBy varchar(max)
	select @OrderBy = case @PickListOrder 
								when 0 then 'DepartmentName, SearchName, RecordID'
								when 1 then 'Seq, RecordID'
								when 2 then 'LastViewedResults desc, SearchName, RecordID'
								when 3 then 'ViewCount desc, SearchName, RecordID'
							else 'DepartmentName, SearchName, RecordID'
						end

	-- we have to use dynamic sql now due to the complexity of the order by clause (dynamic sql also performs better with this anyway)
	declare @sql varchar(max)

	if @ReturnHTML = 1
	begin
		-- formatted as HTML
		set @sql ='select ''optgroupType'' [@class], 
							DepartmentName [@label], 
							(select ''shared_'' + cast(DepartmentID as nvarchar) + ''_'' + cast(RecordID as nvarchar) [@id], 
									Description [@title], 
									SearchName "*" 
									from 
										(
										select d.DepartmentName, d.DepartmentID, si.SearchName, si.RecordID [SearchID], si.Description, dmi.[DisplayInUserList], dmi.Seq, dmi.LastViewedResults, si.RecordID, dmi.ViewCount
										from  [Prmry_AdvSearch_SharedItem] si 
											inner join [Prmry_AdvSearch_Departments] d on si.DepartmentID = d.DepartmentID  
											inner join [Prmry_AdvSearch_DepartmentMembers] dm on si.DepartmentID = dm.DepartmentID  
											inner join [Prmry_AdvSearch_DepartmentMembersItems] dmi on si.RecordID = dmi.SharedSearchID and dm.UserID = dmi.UserID 
										where (dm.UserID=''' + @UserID + ''') and (dmi.UserID=''' + @UserID + ''') and ((' + cast(@FilterList as varchar) + '=0) or (dmi.DisplayInUserList=1)) 
										) a
									where a.DepartmentID = b.DepartmentID
									order by ' + @OrderBy + ' 
									for xml path(''option'') ,type) 
					from 
						(
						select d.DepartmentName, d.DepartmentID, si.SearchName, si.RecordID [SearchID], si.Description, dmi.[DisplayInUserList], dmi.Seq, dmi.LastViewedResults, si.RecordID, dmi.ViewCount
						from  [Prmry_AdvSearch_SharedItem] si 
							inner join [Prmry_AdvSearch_Departments] d on si.DepartmentID = d.DepartmentID  
							inner join [Prmry_AdvSearch_DepartmentMembers] dm on si.DepartmentID = dm.DepartmentID  
							inner join [Prmry_AdvSearch_DepartmentMembersItems] dmi on si.RecordID = dmi.SharedSearchID and dm.UserID = dmi.UserID 
						where (dm.UserID=''' + @UserID + ''') and (dmi.UserID=''' + @UserID + ''') and ((' + cast(@FilterList as varchar) + '=0) or (dmi.DisplayInUserList=1)) 
						) b 
					group by DepartmentID, DepartmentName
					for xml path(''optgroup''), type'
	end
	else
	begin
		-- formatted as a table
		set @sql = 'select DepartmentName, DepartmentID, SearchName, RecordID [SearchID], Description, [DisplayInUserList], Seq 
					from
					(
						select d.DepartmentName, d.DepartmentID, si.SearchName, si.RecordID [SearchID], si.Description, dmi.[DisplayInUserList], dmi.Seq, dmi.LastViewedResults, si.RecordID, dmi.ViewCount
						from [Prmry_AdvSearch_SharedItem] si inner join [Prmry_AdvSearch_Departments] d on si.DepartmentID = d.DepartmentID 
							inner join [Prmry_AdvSearch_DepartmentMembers] dm on si.DepartmentID = dm.DepartmentID 
							inner join [Prmry_AdvSearch_DepartmentMembersItems] dmi on si.RecordID = dmi.SharedSearchID and dm.UserID = dmi.UserID 
						where (dm.UserID=''' + @UserID + ''') and ((' + cast(@FilterList as varchar) + '=0) or (dmi.DisplayInUserList=1)) 
					) a
					order by DepartmentID, ' + @OrderBy
	end
	
	-- execute the sql statement set in one of the conditions above
	print @sql
	exec (@sql)

end

END





GO
