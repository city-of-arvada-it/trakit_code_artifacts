USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearch_PopulateCurrent]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_AdvSearch_PopulateCurrent]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearch_PopulateCurrent]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[tsp_T9_AdvSearch_PopulateCurrent] 

	@UserID varchar(50) = null,
	@Search int = 0, -- current = -2, last = -1, new = 0, 1 = saved, 2 = shared
	@SearchID int = -1,
	@GisTable varchar(50) = '',
	@GisRecordID int = -1
AS
BEGIN

-- TODO: need to check to make sure that the @SourceSearchID exists in @SourceTable

-- make sure we have a user id here!
if isnull(@UserID,'') <>''
begin

	-- determine if we should use the GIS values from the source table or the supplied parameters
	-- -------------------------------------------------------------------------------------------------
	declare @UseGisParams bit
	declare @MakeItDirty bit
	if @GisTable in ('temp','saved','shared') and @GisRecordID > 0 
	begin
		set @UseGisParams=1

		-- check if the parameter values are any different than the GIS info from source table
		declare @GisTableFromSource varchar(50)
		declare @GisRecordIDFromSource int
		declare @GeometryFilterEnabledFromSource bit

		if @Search = -2 select @GisTableFromSource=GisTable, @GisRecordIDFromSource=GisRecordID, @GeometryFilterEnabledFromSource=GeometryFilterEnabled from Prmry_AdvSearch_CurrentItem where UserID=@UserID
		else if @Search = -1 select @GisTableFromSource=GisTable, @GisRecordIDFromSource=GisRecordID, @GeometryFilterEnabledFromSource=GeometryFilterEnabled from Prmry_AdvSearch_LastItem where UserID=@UserID
		else if @Search = 0 select @GisTableFromSource=GisTable, @GisRecordIDFromSource=GisRecordID, @GeometryFilterEnabledFromSource=GeometryFilterEnabled from Prmry_AdvSearch_NewItem where UserID=@UserID
		else if @Search = 1 select @GisTableFromSource=GisTable, @GisRecordIDFromSource=GisRecordID, @GeometryFilterEnabledFromSource=GeometryFilterEnabled from [Prmry_AdvSearch_SavedItem] where RecordID = @SearchID
		else if @Search = 2 select @GisTableFromSource=GisTable, @GisRecordIDFromSource=GisRecordID, @GeometryFilterEnabledFromSource=GeometryFilterEnabled from [Prmry_AdvSearch_SharedItem] where RecordID = @SearchID

		if @GeometryFilterEnabledFromSource<>1 or @GisTable<>@GisTableFromSource or @GisRecordID<>@GisRecordIDFromSource set @MakeItDirty=1
		
	end
	else 
	begin
		set @UseGisParams=0
		set @MakeItDirty=0
	end
	-- -------------------------------------------------------------------------------------------------


	-- if @Search is empty then get the Search and SearchID from preferences
	if @Search is null 
	begin
		-- get the Search and SearchID from preferences
		select @Search = [EditorWindowDefaultSearch], @SearchID = [EditorWindowDefaultSearchID] from [Prmry_AdvSearch_UserOptions] where UserID = @UserID
	end

	-- only update current if we don't want to use the current table as-is
	if @Search = -2 
	begin
		-- can't replace a row with itself; we need this section in case we need to overwrite GisTable, GisRecordID, and GeometryFilterEnabled
		if @UseGisParams=1 
		begin
			-- update the current table with new values for GisTable, GisRecordID, and GeometryFilterEnabled
			-- if we are overwriting the GIS info then we must want to set GeometryFilterEnabled = true
			update Prmry_AdvSearch_CurrentItem set GisTable=@GisTable, GisRecordID=@GisRecordID, GeometryFilterEnabled=1 where UserID=@UserID
			if @MakeItDirty=1 update Prmry_AdvSearch_CurrentItem set IsDirty=1 where UserID=@UserID
		end
	end
	else
	begin
		-- copy records from last, new, saved, ahared tables to current

		-- remove all current records for user id
		delete from Prmry_AdvSearch_CurrentItem where UserID = @UserID -- detail and listItems records should be deleted as there is a cascade delete on those tables

		-- TODO: this would be easier to maintain if this was dynamic sql

		-- need new variables as @SearchID is not applicable to new and current tables
		declare @origItemID int 
		declare @newItemID int


		if @Search = -1 -- last
		begin
			
			-- item
			set @origItemID = (select RecordID from [Prmry_AdvSearch_LastItem] where UserID = @UserID) -- get item ref for detail and listItem tables
			insert into [Prmry_AdvSearch_CurrentItem]
			([UserID],[SearchName],[GroupName],[SearchType],[ReadOnly],[Description],[RefSearchID],[RefTable],[IsDirty],[GisTable],[GisRecordID],[GeometryFilterEnabled])
			select [UserID],[SearchName],[GroupName],[SearchType],[ReadOnly],[Description],[RefSearchID],[RefTable]
					,case when @MakeItDirty=1 then 1 else [IsDirty] end
					,case when @UseGisParams=1 then @GisTable else [GisTable] end, case when @UseGisParams=1 then @GisRecordID else [GisRecordID] end,[GeometryFilterEnabled]
			from [Prmry_AdvSearch_LastItem]
			where RecordID = @origItemID 

			select @newItemID = SCOPE_IDENTITY() -- get the new ItemID

			-- detail
			insert into [Prmry_AdvSearch_CurrentDetail]
			([ItemID],[TableName],[FieldName],[DisplayName],[Datatype],[Filter],[Value1],[Value2],[Seq])
			select @newItemID as [ItemID],[TableName],[FieldName],[DisplayName],[Datatype],[Filter],[Value1],[Value2],[Seq]
			from [Prmry_AdvSearch_LastDetail]
			where ItemID = @origItemID

		end
		else
		begin
			if @Search = 0 -- new
				begin
				
					-- item
					set @origItemID = (select RecordID from [Prmry_AdvSearch_NewItem] where UserID = @UserID) -- get item ref for detail and listItem tables
					insert into [Prmry_AdvSearch_CurrentItem]
					([UserID],[SearchName],[GroupName],[SearchType],[ReadOnly],[Description],[RefSearchID],[RefTable],[IsDirty],[GisTable],[GisRecordID],[GeometryFilterEnabled])
					select [UserID],[SearchName],[GroupName],[SearchType],[ReadOnly],[Description],0,'new',1
							,case when @UseGisParams=1 then @GisTable else [GisTable] end, case when @UseGisParams=1 then @GisRecordID else [GisRecordID] end,[GeometryFilterEnabled]
					from [Prmry_AdvSearch_NewItem]
					where RecordID = @origItemID

					select @newItemID = SCOPE_IDENTITY() -- get the new ItemID

					-- detail
					insert into [Prmry_AdvSearch_CurrentDetail]
					([ItemID],[TableName],[FieldName],[DisplayName],[Datatype],[Filter],[Value1],[Value2],[Seq])
					select @newItemID as [ItemID],[TableName],[FieldName],[DisplayName],[Datatype],[Filter],[Value1],[Value2],[Seq]
					from [Prmry_AdvSearch_NewDetail]
					where ItemID = @origItemID

			end
			else
			begin
				if @Search = 1 -- saved
				begin
					-- item
					insert into [Prmry_AdvSearch_CurrentItem]
					([UserID],[SearchName],[GroupName],[SearchType],[ReadOnly],[Description],[RefSearchID],[RefTable],[IsDirty],[GisTable],[GisRecordID],[GeometryFilterEnabled])
					select [UserID],[SearchName],[GroupName],[SearchType],[ReadOnly],[Description],@SearchID,'saved'
							,case when @MakeItDirty=1 then 1 else 0 end
							,case when @UseGisParams=1 then @GisTable else [GisTable] end, case when @UseGisParams=1 then @GisRecordID else [GisRecordID] end,[GeometryFilterEnabled]
					from [Prmry_AdvSearch_SavedItem]
					where RecordID = @SearchID

					select @newItemID = SCOPE_IDENTITY() -- get the new ItemID

					-- detail
					insert into [Prmry_AdvSearch_CurrentDetail]
					([ItemID],[TableName],[FieldName],[DisplayName],[Datatype],[Filter],[Value1],[Value2],[Seq])
					select @newItemID as [ItemID],[TableName],[FieldName],[DisplayName],[Datatype],[Filter],[Value1],[Value2],[Seq]
					from [Prmry_AdvSearch_SavedDetail]
					where ItemID=@SearchID
					
				end
				else
				begin
					if @Search = 2 -- shared
					begin
						-- item
						insert into [Prmry_AdvSearch_CurrentItem]
						([UserID],[SearchName],[GroupName],[SearchType],[ReadOnly],[Description],[RefSearchID],[RefTable],[IsDirty],[GisTable],[GisRecordID],[GeometryFilterEnabled])
						select @UserID,[SearchName],[GroupName],[SearchType],[ReadOnly],[Description],@SearchID,'shared'
								,case when @MakeItDirty=1 then 1 else 0 end
								,case when @UseGisParams=1 then @GisTable else [GisTable] end, case when @UseGisParams=1 then @GisRecordID else [GisRecordID] end,[GeometryFilterEnabled]
						from [Prmry_AdvSearch_SharedItem]
						where RecordID = @SearchID

						select @newItemID = SCOPE_IDENTITY() -- get the new ItemID

						-- detail
						insert into [Prmry_AdvSearch_CurrentDetail]
						([ItemID],[TableName],[FieldName],[DisplayName],[Datatype],[Filter],[Value1],[Value2],[Seq])
						select @newItemID as [ItemID],[TableName],[FieldName],[DisplayName],[Datatype],[Filter],[Value1],[Value2],[Seq]
						from [Prmry_AdvSearch_SharedDetail]
						where ItemID=@SearchID

					end
				end
			end
		end
	end
end

END
GO
