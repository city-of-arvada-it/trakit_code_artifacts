USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[lp_update_attachements_rvw]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[lp_update_attachements_rvw]
GO
/****** Object:  StoredProcedure [dbo].[lp_update_attachements_rvw]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* Update the custom table for documents that are uploaded to CRW
March 26, 2013 - Ychavez
Used by SSRS 
select * from l_permit_attachments_review
exec lp_update_attachements_rvw @RecordID = 'API::30215080700001', @MakeViewable = 'Y'
*/
create procedure [dbo].[lp_update_attachements_rvw]
(
	@RecordID varchar(50) = null,
	@MakeViewable varchar(10) = null
)
as
begin

if @RecordID is not null
 BEGIN
	update l_permit_attachments_review 
	set Make_Viewable = @MakeViewable
	, unlocked_by = USER_NAME(),
	unlocked_date = GETDATE()
	where RecordID = @RecordID
END
	select * from l_permit_attachments_review where recordID = isnull(@RecordID, recordid)

end
GO
