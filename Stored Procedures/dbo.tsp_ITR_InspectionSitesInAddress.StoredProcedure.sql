USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ITR_InspectionSitesInAddress]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_ITR_InspectionSitesInAddress]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ITR_InspectionSitesInAddress]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  -- =============================================
-- Author:        <Author,,Name>
-- Create date: <Create Date,,>
-- Description:   <Description,,>
-- =============================================
CREATE PROCEDURE  [dbo].[tsp_ITR_InspectionSitesInAddress] 
(
@siteAddress varchar(200),
@geoType varchar(200)
)
AS
BEGIN
      -- SET NOCOUNT ON added to prevent extra result sets from
      -- interfering with SELECT statements.
      SET NOCOUNT ON;
      
      SELECT DISTINCT loc_recordid
            INTO #tempActivities
      FROM tvw_ITR_Activities
      WHERE site_addr like '%' + @siteAddress + '%' and geotype= @geoType
      
      
      -- Cases
      SELECT vw.loc_recordid, vw.Tgroup, vw.recordid, ACTIVITYNO, vw.site_apn, vw.site_addr, vw.site_city, vw.site_state, vw.site_zip, 
            vw.geotype, LAT, LON, ISNULL(x,0)as x, ISNULL(y,0)as y, RESTRICTIONS.RECORDID as restriction_id, RESTRICTIONS.DATE_ADDED as restriction_add_date,
            RESTRICTIONS.RESTRICTION_TYPE as restriction_type, RESTRICTIONS.RESTRICTION_NOTES as restriction_notes, RESTRICTIONS.OTHER_NOTES as restriction_notes2, 
            DATE1, DATE2, DATE3, DATE4, DATE5, DATE6, DATE7, vw.DatePropertyList,
            vw.status AS [actStatus], ACTIVITYTYPE AS actType, ACTIVITYSUBTYPE as actSubType,
            actMain.CASE_NAME AS [DESCRIPTION], '' AS actJobValue, actMain.SITE_LOT_NO, actMain.SITE_BLOCK, actMain.SITE_SUBDIVISION, actMain.SITE_TRACT,
            actFees.BALANCE_DUE as feeBalance, actInsp.InspectionType, '' AS Company
      FROM tvw_ITR_Activities vw INNER JOIN #tempActivities temp ON (vw.loc_recordid = temp.loc_recordid)
            LEFT JOIN CASE_MAIN actMain ON actMain.CASE_NO = vw.ACTIVITYNO
            LEFT JOIN CASE_MAIN actFees ON actFees.CASE_NO = vw.ACTIVITYNO
            LEFT JOIN CASE_INSPECTIONS actInsp on vw.ACTIVITYNO = actInsp.CASE_NO
            LEFT JOIN (select RECORDID, DATE_ADDED, DATE_CLEARED, RESTRICTION_TYPE, RESTRICTION_NOTES, OTHER_NOTES, LOC_RECORDID
                              from Geo_RESTRICTIONS2
                              where DATE_CLEARED IS NULL AND RESTRICTION_NOTES NOT LIKE 'VOIDED (%'   
                           UNION
                           select RECORDID, '' AS DATE_ADDED, '' AS DATE_CLEARED, RESTRICTION_TYPE, RESTRICTION_NOTES, OTHER_NOTES, LOC_RECORDID
                            from Geo_RESTRICTIONS 
                            where RESTRICTION_TYPE <> 'NONE') AS RESTRICTIONS ON RESTRICTIONS.LOC_RECORDID = vw.loc_recordid
      WHERE Tgroup = 'CASE' and vw.site_addr like '%' + @siteAddress + '%' and geotype= @geoType
            AND (DATE5 IS NOT NULL AND DATE5 <> '' AND IsDate(DATE5) = 1)
            AND (DATE1 IS NULL OR DATE1 = '')
      UNION
      -- Permits
      SELECT vw.loc_recordid, vw.Tgroup, vw.recordid, ACTIVITYNO, vw.site_apn, vw.site_addr, vw.site_city, vw.site_state, vw.site_zip, 
            vw.geotype, LAT, LON, ISNULL(x,0)as x, ISNULL(y,0)as y, RESTRICTIONS.RECORDID as restriction_id, RESTRICTIONS.DATE_ADDED as restriction_add_date,
            RESTRICTIONS.RESTRICTION_TYPE as restriction_type, RESTRICTIONS.RESTRICTION_NOTES as restriction_notes, RESTRICTIONS.OTHER_NOTES as restriction_notes2,
            DATE1, DATE2, DATE3, DATE4, DATE5, DATE6, DATE7, vw.DatePropertyList,
            vw.status AS [actStatus], ACTIVITYTYPE AS actType, ACTIVITYSUBTYPE as actSubType,
            actMain.DESCRIPTION, actMain.JOBVALUE AS actJobValue, actMain.SITE_LOT_NO, actMain.SITE_BLOCK, actMain.SITE_SUBDIVISION, actMain.SITE_TRACT,
            actFees.BALANCE_DUE as feeBalance, actInsp.InspectionType, '' AS Company
      FROM tvw_ITR_Activities vw INNER JOIN #tempActivities temp ON (vw.loc_recordid = temp.loc_recordid)
            LEFT JOIN PERMIT_MAIN actMain ON actMain.PERMIT_NO = vw.ACTIVITYNO
            LEFT JOIN Permit_Main actFees ON actFees.PERMIT_NO = vw.ACTIVITYNO
            LEFT JOIN Permit_Inspections actInsp on vw.ACTIVITYNO = actInsp.PERMIT_NO
            LEFT JOIN (select RECORDID, DATE_ADDED, DATE_CLEARED, RESTRICTION_TYPE, RESTRICTION_NOTES, OTHER_NOTES, LOC_RECORDID
                              from Geo_RESTRICTIONS2
                              where DATE_CLEARED IS NULL AND RESTRICTION_NOTES NOT LIKE 'VOIDED (%'   
                           UNION
                           select RECORDID, '' AS DATE_ADDED, '' AS DATE_CLEARED, RESTRICTION_TYPE, RESTRICTION_NOTES, OTHER_NOTES, LOC_RECORDID
                            from Geo_RESTRICTIONS 
                            where RESTRICTION_TYPE <> 'NONE') AS RESTRICTIONS ON RESTRICTIONS.LOC_RECORDID = vw.loc_recordid
      WHERE Tgroup = 'PERMIT' and vw.site_addr like '%' + @siteAddress + '%' and geotype= @geoType
            --AND (DATE2 IS NULL OR DATE2 = '')
            -- rrr 6/5/13 - Adminitrack Issue No. 21386 - Include those that are issued, not those approved
            --AND (DATE2 IS NOT NULL AND DATE2 <> '' AND ISDATE(DATE2) = 1) 
            AND (DATE4 IS NULL OR DATE4 = '')
            AND (DATE3 IS NULL OR DATE3 = '' OR GETDATE() < Date3)
      UNION 
      -- Projects
      SELECT vw.loc_recordid, vw.Tgroup, vw.recordid, ACTIVITYNO, vw.site_apn, vw.site_addr, vw.site_city, vw.site_state, vw.site_zip, 
            vw.geotype, LAT, LON, ISNULL(x,0)as x, ISNULL(y,0)as y, RESTRICTIONS.RECORDID as restriction_id, RESTRICTIONS.DATE_ADDED as restriction_add_date,
            RESTRICTIONS.RESTRICTION_TYPE as restriction_type, RESTRICTIONS.RESTRICTION_NOTES as restriction_notes, RESTRICTIONS.OTHER_NOTES as restriction_notes2,
            DATE1, DATE2, DATE3, DATE4, DATE5, DATE6, DATE7, vw.DatePropertyList,
            vw.status AS [actStatus], ACTIVITYTYPE AS actType, ACTIVITYSUBTYPE as actSubType,
            actMain.PROJECT_NAME AS [DESCRIPTION], '' AS actJobValue, actMain.SITE_LOT_NO, actMain.SITE_BLOCK, actMain.SITE_SUBDIVISION, actMain.SITE_TRACT,
            actFees.BALANCE_DUE as feeBalance, actInsp.InspectionType, '' AS Company
      FROM tvw_ITR_Activities vw INNER JOIN #tempActivities temp ON (vw.loc_recordid = temp.loc_recordid)
            LEFT JOIN PROJECT_MAIN actMain ON actMain.PROJECT_NO = vw.ACTIVITYNO
            LEFT JOIN Project_Main actFees ON actFees.PROJECT_NO = vw.ACTIVITYNO
            LEFT JOIN PROJECT_INSPECTIONS actInsp on vw.ACTIVITYNO = actInsp.PROJECT_NO
            LEFT JOIN (select RECORDID, DATE_ADDED, DATE_CLEARED, RESTRICTION_TYPE, RESTRICTION_NOTES, OTHER_NOTES, LOC_RECORDID
                              from Geo_RESTRICTIONS2
                              where DATE_CLEARED IS NULL AND RESTRICTION_NOTES NOT LIKE 'VOIDED (%'   
                           UNION
                           select RECORDID, '' AS DATE_ADDED, '' AS DATE_CLEARED, RESTRICTION_TYPE, RESTRICTION_NOTES, OTHER_NOTES, LOC_RECORDID
                            from Geo_RESTRICTIONS 
                            where RESTRICTION_TYPE <> 'NONE') AS RESTRICTIONS ON RESTRICTIONS.LOC_RECORDID = vw.loc_recordid
      WHERE Tgroup = 'PROJECT' and vw.site_addr like '%' + @siteAddress + '%' and geotype= @geoType
            AND (DATE3 IS NULL OR DATE3 = '')
      UNION 
      -- License1
            SELECT vw.loc_recordid, vw.Tgroup, vw.recordid, ACTIVITYNO, vw.site_apn, vw.site_addr, vw.site_city, vw.site_state, vw.site_zip, 
            vw.geotype, LAT, LON, ISNULL(x,0)as x, ISNULL(y,0)as y, RESTRICTIONS.RECORDID as restriction_id, RESTRICTIONS.DATE_ADDED as restriction_add_date,
            RESTRICTIONS.RESTRICTION_TYPE as restriction_type, RESTRICTIONS.RESTRICTION_NOTES as restriction_notes, RESTRICTIONS.OTHER_NOTES as restriction_notes2,
            DATE1, DATE2, DATE3, DATE4, DATE5, DATE6, DATE7, vw.DatePropertyList,
            vw.status AS [actStatus], ACTIVITYTYPE AS actType, ACTIVITYSUBTYPE as actSubType,
            licBus.SITE_DESCRIPTION AS [DESCRIPTION], '' AS actJobValue, licBus.SITE_LOT_NO, licBus.SITE_BLOCK, licBus.SITE_SUBDIVISION, licBus.SITE_TRACT,
            (actFees.AMOUNT - actFees.PAID_AMOUNT) as feeBalance, actInsp.InspectionType, licBus.Company AS Company
      FROM tvw_ITR_Activities vw INNER JOIN #tempActivities temp ON (vw.loc_recordid = temp.loc_recordid)
            LEFT JOIN LICENSE_MAIN actMain ON actMain.BUS_LIC_NO = vw.ACTIVITYNO
            LEFT JOIN License_Business licBus ON licBus.BUSINESS_NO = actMain.BUSINESS_NO
            LEFT JOIN LICENSE_FEES actFees ON actFees.BUS_LIC_NO = vw.ACTIVITYNO
            LEFT JOIN LICENSE_INSPECTIONS actInsp on vw.ACTIVITYNO = actInsp.BUS_LIC_NO
            LEFT JOIN (select RECORDID, DATE_ADDED, DATE_CLEARED, RESTRICTION_TYPE, RESTRICTION_NOTES, OTHER_NOTES, LOC_RECORDID
                              from Geo_RESTRICTIONS2
                              where DATE_CLEARED IS NULL AND RESTRICTION_NOTES NOT LIKE 'VOIDED (%'   
                           UNION
                           select RECORDID, '' AS DATE_ADDED, '' AS DATE_CLEARED, RESTRICTION_TYPE, RESTRICTION_NOTES, OTHER_NOTES, LOC_RECORDID
                            from Geo_RESTRICTIONS 
                            where RESTRICTION_TYPE <> 'NONE') AS RESTRICTIONS ON RESTRICTIONS.LOC_RECORDID = vw.loc_recordid
      WHERE Tgroup = 'LICENSE'  and vw.site_addr like '%' + @siteAddress + '%' and geotype= @geoType
            AND (DATE2 IS NULL OR DATE2 = '' OR DATE2 > GETDATE())
      UNION
      --License2
      SELECT vw.loc_recordid, vw.Tgroup, vw.recordid, ACTIVITYNO, vw.site_apn, vw.site_addr, vw.site_city, vw.site_state, vw.site_zip, 
            vw.geotype, LAT, LON, ISNULL(x,0)as x, ISNULL(y,0)as y, RESTRICTIONS.RECORDID as restriction_id, RESTRICTIONS.DATE_ADDED as restriction_add_date,
            RESTRICTIONS.RESTRICTION_TYPE as restriction_type, RESTRICTIONS.RESTRICTION_NOTES as restriction_notes, RESTRICTIONS.OTHER_NOTES as restriction_notes2,
            DATE1, DATE2, DATE3, DATE4, DATE5, DATE6, DATE7, vw.DatePropertyList,
            vw.status AS [actStatus], ACTIVITYTYPE AS actType, ACTIVITYSUBTYPE as actSubType,
            actMain.SITE_DESCRIPTION AS [DESCRIPTION], '' AS actJobValue, actMain.SITE_LOT_NO, actMain.SITE_BLOCK, actMain.SITE_SUBDIVISION, actMain.SITE_TRACT,
            actFees.BALANCE_DUE as feeBalance, actInsp.InspectionType, vw.TITLE As Company
      FROM tvw_ITR_Activities vw INNER JOIN #tempActivities temp ON (vw.loc_recordid = temp.loc_recordid)
      LEFT JOIN LICENSE2_MAIN actMain ON actMain.LICENSE_NO = vw.ACTIVITYNO
            LEFT JOIN LICENSE2_MAIN actFees ON actFees.LICENSE_NO = vw.ACTIVITYNO
            LEFT JOIN LICENSE2_INSPECTIONS actInsp on vw.ACTIVITYNO = actInsp.LICENSE_NO
            LEFT JOIN (select RECORDID, DATE_ADDED, DATE_CLEARED, RESTRICTION_TYPE, RESTRICTION_NOTES, OTHER_NOTES, LOC_RECORDID
                              from Geo_RESTRICTIONS2
                              where DATE_CLEARED IS NULL AND RESTRICTION_NOTES NOT LIKE 'VOIDED (%'   
                           UNION
                           select RECORDID, '' AS DATE_ADDED, '' AS DATE_CLEARED, RESTRICTION_TYPE, RESTRICTION_NOTES, OTHER_NOTES, LOC_RECORDID
                            from Geo_RESTRICTIONS 
                            where RESTRICTION_TYPE <> 'NONE') AS RESTRICTIONS ON RESTRICTIONS.LOC_RECORDID = vw.loc_recordid
      WHERE Tgroup = 'LICENSE2'   and vw.site_addr like '%' + @siteAddress + '%' and geotype= @geoType
            AND (DATE2 IS NULL OR DATE2 = '' OR DATE2 > GETDATE())
            
      ORDER BY loc_recordid, Tgroup, ACTIVITYNO, restriction_id
      
END


GO
