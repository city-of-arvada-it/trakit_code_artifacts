USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_DevREviewClearpointReviews]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[usp_Arvada_DevREviewClearpointReviews]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_DevREviewClearpointReviews]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_Arvada_DevREviewClearpointReviews]
	@projectno varchar(50),
	@Step varchar(50)
as


select
	r.PROJECT_NO,
	r.REVIEWTYPE,
	r.DATE_DUE,
	r.DATE_RECEIVED,
	r.CONTACT,
	r.STATUS,
	r.REVIEWGROUP,
	case when r.DATE_RECEIVED is null and r.date_Due > convert(date, getdate()) then 'red'
		 when r.DATE_RECEIVED > r.date_due then 'red'
		 else 'black'
		 end as color
from dbo.project_reviews r
where
	r.REVIEWGROUP in ('1st Extended Review', '1st Standard' , '2nd Extended Review' , '2nd Standard Review', 'Completeness Check', 'Decision Review','Conditional Approval' )
and r.PROJECT_NO= @projectno
and (1 = case when @step = 'Completeness Check' and r.REVIEWGROUP = 'Completeness Check' then 1
			  when @step = 'First Review' and r.REVIEWGROUP in ('1st Extended Review', '1st Standard' ) then 1
			  when @step = 'Second Review' and r.REVIEWGROUP in ('2nd Extended Review' , '2nd Standard Review') then 1
			  when @step = 'Decision Review' and r.REVIEWGROUP in ('Decision Review' ) then 1
			  when @step = 'Conditional Approval' and r.REVIEWGROUP in ('Conditional Approval' ) then 1
			  else 0 end)
GO
