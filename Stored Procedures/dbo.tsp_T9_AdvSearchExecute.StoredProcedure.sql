USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearchExecute]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_AdvSearchExecute]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearchExecute]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
			

-- ===================================================================================================================================================
-- Revision History:
--       2014-02-26 - mdm - added to sourcesafe
--       2014-03-06 - mdm - added GIS Fields
--       2014-03-19 - mdm - add filter on the number of records to return
--       2014-04-03 - mdm - multiple fixes
--
--		 2014-DEC-08  EWH	Additions: 
--							1. made results table local instead of Global (zzAdvQueryResults)
--							2. added @OrderBy for order by clause
--							3. added handling for query that exceeds 8000 chars
--							4. return value = 1 (TRUE) if complete query or 0 (FALSE) if query is too long to process
--							5. added comments for future updates
--							6. select TOP in query with ORDER BY so appropriate results are returned
--							7. changed the way TOP is created in query string to prevent replacing 'select' in query string
--
--							Removed:
--							1. Disabled @DisplayAdditionalFields functionality - doesn't appear to do anything - left in place for future
--							2. Disabled @DefaultRow - is not used anywhere in procedure
--
--		2014-DEC-09	EWH		changed the position of the GIS_SITE_APN column to the first column (instead of adding at the end)
--
--		2014-DEC-11 EWH		added @UserID parameter, @CrmRestrictions variable, and code to restrict CRM data by user 
--							references:		issues #33974 and #26573
--											[tsp_T9_CRMRestrTypeAccess], [tsp_GblAdvancedSearch] line 222
--											C:\Development\webTRAKiT\webTRAKiT\UC\Default\ctrlWinAdvancedSearch.ascx.vb line 1792
--											C:\Development\webTRAKiT\webTRAKiT\classes\common.vb line 1827
--
--		2015-JAN-12 EWH		Fixed query for CRM restrictions
--											
-- ===================================================================================================================================================

/*
   ----------------------------------------------------------------
    TESTING:
   ----------------------------------------------------------------
	DECLARE	@return_value int

	EXEC	@return_value = [dbo].[tsp_T9_AdvSearchExecute]
			@Recordid = N'954',
			@ReturnRecords = 10,
			@OrderBy = NULL --N'order by [MECH_FURNACE]'

	SELECT	'Return Value' = @return_value
	----------------------------------------------------------------
*/ 

create PROCEDURE [dbo].[tsp_T9_AdvSearchExecute] (
	@Recordid Varchar(50) = Null,
	@ReturnRecords int = 0,
	@OrderBy varchar(500) = null, -- expect to receive complete ORDER BY clause from caller, including table name. ex: "ORDER BY AEC_MAIN.STATUS DESC"
	@UserID varchar(50) = null -- EWH Dec 11, 2014 issue #33974: restrict CRM records by user
)

AS
BEGIN
--print(convert(varchar,getdate(),109)) -- EWH: used for measuring the duration of this proc
Declare @Group as varchar(50)
Declare @JoinMainTable as varchar(255)
Declare @JoinField as varchar(50)
Declare @JoinMainTableDotField as varchar(255)
Declare @JoinTestTable as varchar(8000)
Declare @WhereClause varchar(8000) = ''
Declare @ReturnFieldList varchar(8000) = ''
Declare @JoinClause varchar(8000) = ''
Declare @SQL varchar(8000) = ''
--Declare @SQLAddFld varchar(8000) = '' -- EWH Dec 9, 2014: unused, not sure what this was intended for
Declare @TableName as varchar(255)
Declare @FieldName as varchar(255)
Declare @DisplayName as varchar(255)
Declare @DefaultOperator as varchar(255)
--Declare @DefaultField as varchar(255)
Declare @Datatype as varchar(255)
Declare @Value1 as varchar(255)
Declare @Value2 as varchar(255)
--Declare @DefaultRow as varchar(255)
Declare @DisplayAdditionalFields as varchar(1)
Declare @TblDotFld as varchar(255)
Declare @TblCount as int
Declare @return_value as int = 1
Declare @CrmRestrictions as varchar(500) = '' -- EWH Dec 11, 2014 issue #33974: restrict CRM records by user

-- ------------------------------------------------------------------------------------------------------------------------------------
--  Create SELECT clause
--						Notes:       
--						
-- ------------------------------------------------------------------------------------------------------------------------------------

set @Group = (Select upper([group]) from Prmry_AdvSearchConfigItem where recordid = @recordid )

-- EWH: do we use this for adv search 2 anywhere? - disabled for now
/*
set @DisplayAdditionalFields = (Select ShowAdditionalFields from Prmry_AdvSearchConfigItem where recordid = @recordid )

if @DisplayAdditionalFields <> 1 
       set @DisplayAdditionalFields = 0
*/

select @JoinField = dbo.tfn_ActivityNoField(@group)

--Set main table for Join
Begin
       Select @JoinMainTable = 
              case	when @Group = 'CRM' then 'CRM_ISSUES'
					When @Group = 'GEO' then  'GEO_OWNERSHIP'
					else @Group + '_MAIN'
              end    
End


-- EWH Dec 11, 2014 issue #33974: restrict CRM records by user - create where clause segment in @CrmRestrictions if @JoinMainTable = 'CRM_ISSUES'
--									note that if @UserID is not passed then we won't return any records
if @JoinMainTable = 'CRM_ISSUES' 
begin
	if isnull(@UserID,'') = '' --don't allow passing an empty string either
	begin
		set @CrmRestrictions=' AND 0=1' -- make sure we don't return any results at all if @UserID is not supplied
	end
	else
	begin
		-- EWH Jan 12, 2015: fixed query below for CRM restrictions
		set @CrmRestrictions=' AND (CRM_ISSUES.NATURETYPE_LIST_ID IS NULL OR CRM_ISSUES.NATURETYPE_LIST_ID IN (select distinct CRM_Issues.NATURETYPE_LIST_ID from CRM_Issues inner join crm_lists on NATURETYPE_LIST_ID = list_id where list_text not in (SELECT distinct TYPENAME FROM PRMRY_CRM_RESTR_TYPE_ACCESS WHERE USERID <> ''' + @UserID + ''')))' 
	end
end


--Begin build Joinclause
select @JoinMainTableDotField =  @JoinMainTable + '.' + @JoinField

-- Begin building query string. Set first field here
Begin
       Select @ReturnFieldList = ''''' as [GIS_SITE_APN], ' + -- EWH: empty string is faster than NULL
              case when @Group = 'CASE' then  @JoinMainTable + '.' + 'LOC_RECORDID'                     
                     when @Group = 'LICENSE2' then @JoinMainTable + '.' + 'LOC_RECORDID'
                     when @Group = 'PERMIT' then @JoinMainTable + '.' + 'LOC_RECORDID'
                     when @Group = 'PROJECT' then @JoinMainTable + '.' + 'LOC_RECORDID'
                     When @Group = 'GEO' then  @JoinMainTable + '.' + 'RECORDID'-- as LOC_RECORDID'
                     else ''''''  --' as LOC_RECORDID ' 
              end    
End

-- Set alias for first field and set second field here
SET    @ReturnFieldList = @ReturnFieldList + ' as [LOC_RECORDID], ' + @JoinMainTableDotField + ' as [KeyField]' 

--Adjust for sub GEO tables
if @Group = 'GEO'
       set @JoinField = 'LOC_RECORDID'

IF OBJECT_ID('tempdb..#FieldList') IS NOT NULL
    DROP TABLE #FieldList

--Select users fields into temp table --> compile into where clause
Select * into #FieldList from Prmry_AdvSearchConfigDetail where ParentRecordID = @Recordid

--------------------------
-- * test* 
--print @ReturnFieldList
--select * from #FieldList
--------------------------

-- create temp table where the results will be stored 
IF OBJECT_ID('tempdb..#AdvQueryResults') IS NOT NULL
begin
	DROP TABLE #AdvQueryResults
end

-- EWH: this may need review as we are declaring these columns as varchar instead of the actual data type of the column returned from dbo.tfn_ActivityNoField(@group)
--	GIS_SITE_APN goes first as it gets transformed into an html button for GIS navigation (the GIS globe)
create table #AdvQueryResults 
(
	  GIS_SITE_APN varchar(255) null
	, LOC_RECORDID varchar(255) null
	, KeyField varchar(255) null
)

Declare @Id int
Declare @AddColumn varchar(255) -- the sql statement to dynamically add a column

-- create column and append select query string for every record in table
While (Select Count(*) From #FieldList) > 0
Begin

    Select Top 1 @Id = recordid From #FieldList

       set @TableName =  (select upper(tablename) from #FieldList where recordid = @id)
       set @FieldName =  (select FieldName from #FieldList where recordid = @id)
       set @DisplayName =  (select DisplayName from #FieldList where recordid = @id)
       set @DefaultOperator =  (select DefaultOperator from #FieldList where recordid = @id)
       set @Datatype =  (select Datatype from #FieldList where recordid = @id)
       set @Value1 =  (select Value1 from #FieldList where recordid = @id)
       set @Value2 =  (select Value2 from #FieldList where recordid = @id)
		--set @DefaultRow =  (select DefaultRow from #FieldList where recordid = @id)
       
       set @ReturnFieldList = @ReturnFieldList + ', ' + @TableName + '.' + @Fieldname + ' as [' + @Displayname + ']' 

	   -- Add column to #AdvQueryResults
	   set @AddColumn = 'alter table #AdvQueryResults add [' + @DisplayName + '] ' + replace(@Datatype,'varchar','varchar(255)') 
		--print(@AddColumn)
	   exec(@AddColumn)

	   --  create the appropriate verbose sql condition to the where clause based on the provided indicator
       if @DefaultOperator <> '' 
	   		-- ---------------------------------------------------------------------------------------------------
			--  Create WHERE clause part 1:
			--									Notes:       
			-- ---------------------------------------------------------------------------------------------------
              Begin
                     set @WhereClause = @WhereClause + 'AND ('  + @TableName + '.' + @Fieldname 

                     if @DefaultOperator = 'contains' 
                           begin
                                  set @WhereClause = @WhereClause + ' like ''' + '%' + @value1 + '%''' + ') ' 
                           end

                     if @DefaultOperator = 'in' 
                           begin
                                  if @Datatype in ('float','int','money','numeric','real','smallint')
                                         begin
                                                set @Value1 = replace(@Value1,':',',')

                                                set @WhereClause = @WhereClause + ' in (' + @Value1 + ')) '
                                         end
                                  else
                                         begin
                                                set @Value1 = replace(@Value1,':',''',''')

                                                set @WhereClause = @WhereClause + ' in (''' + @Value1 + ''')) '
                                         end                               
                           end    

                     if @DefaultOperator = 'is'
                           begin         
                                  if @Datatype in ('float','int','money','numeric','real','smallint')
                                         begin
                                                set @WhereClause = @WhereClause + ' = ' + @value1 +  ') ' 
                                         end
                                  else
                                         begin
                                                set @WhereClause = @WhereClause + ' = ''' + @value1 +  ''') ' 
                                         end
                           end

                     if @DefaultOperator = 'is not'
                           begin         
                                  if @Datatype in ('float','int','money','numeric','real','smallint')
                                         begin
                                                set @WhereClause = @WhereClause + ' <> ' + @value1 +  ') ' 
                                         end
                                  else
                                         begin
                                                set @WhereClause = @WhereClause + ' <> ''' + @value1 +  ''') ' 
                                         end
                           end

                     if @DefaultOperator = 'Between' 
                           begin
                                  if @Datatype in ('float','int','money','numeric','real','smallint')
                                         begin
                                                set @WhereClause = @WhereClause + ' between ' + @value1 +  ' and ' + @value2 + ') ' 
                                         end
                                  else
                                         begin
                                                set @WhereClause = @WhereClause + ' between ''' + @value1 +  ''' and ''' + @Value2 + ''')' 
                                         end
                                  
                           end    
                           
                     if @DefaultOperator = 'Begins with' 
                           begin
                                  set @WhereClause = @WhereClause + ' like ''' + @value1 + '%''' + ') ' 
                           end
                     
                     if @DefaultOperator = 'At least'
                           begin         
                                  if @Datatype in ('float','int','money','numeric','real','smallint')
                                         begin
                                                set @WhereClause = @WhereClause + ' >= ' + @value1 +  ') ' 
                                         end
                                  else
                                         begin
                                                set @WhereClause = @WhereClause + ' >= ''' + @value1 +  ''') ' 
                                         end
                           end

                     if @DefaultOperator = 'Less than'
                           begin         
                                  if @Datatype in ('float','int','money','numeric','real','smallint')
                                         begin
                                                set @WhereClause = @WhereClause + ' < ' + @value1 +  ') ' 
                                         end
                                  else
                                         begin
                                                set @WhereClause = @WhereClause + ' < ''' + @value1 +  ''') ' 
                                         end
                           end
              end
			  -- ------------------END CREATE WHERE CLAUSE PART 1-------------------------------------------------


    Delete #FieldList Where recordid = @Id -- remove this record now that we have processed it

End
-- -------------------------------------------END OF CREATE SELECT CLAUSE--------------------------------------------------------------



-- ------------------------------------------------------------------------------------------------------------------------------------
--  Create JOIN clause
--						Notes:       
--						@JoinMainTable set at top of proc based on Group
--						@JoinField set at top of proc based on Function that identifies key field for a group
-- ------------------------------------------------------------------------------------------------------------------------------------
BEGIN

       IF OBJECT_ID('tempdb..#TableList') IS NOT NULL
              DROP TABLE #TableList



       Select Distinct TableName into #TableList from Prmry_AdvSearchConfigDetail where ParentRecordID = @Recordid
       Declare @TblId varchar(50)
	   set @TblCount = (Select Count(*) From #TableList)
	   select @JoinClause = 'FROM ' + @JoinMainTable 
       While (Select Count(*) From #TableList) > 0
       Begin
              Select Top 1 @TblId = TableName From #TableList
              set @TableName =  (select upper(tablename) from #TableList where TableName = @TblId)
              set @JoinTestTable =  @TableName +  '.' + @JoinField          
			  if @tblcount > 1 and (@JoinTestTable <> @JoinMainTableDotField) and  (@JoinMainTable <> @TableName)
              select @JoinClause =  @JoinClause + ' INNER JOIN ' + @TableName + ' ON ' + @JoinMainTableDotField + ' = ' +@JoinTestTable
              Delete #TableList Where TableName = @TblId
       END

end
-- -----------------------------------------END OF CREATE JOIN CLAUSE------------------------------------------------------------------




-- ------------------------------------------------------------------------------------------------------------------------------------
--  Create WHERE clause part 2: add additional fields
--
--				Notes:       
--				EWH: disabled for now; there doesn't appear to be a table named "zAdvSearchAdditionalFields" in the DB ?
--						per Mark Mayo, these fields would always be present in the customer's query if the ShowAdditionalFields flag was set to 1
---						ShowAdditionalFields always appears to be set = '0' in AdvancedSearchFormats2.ascx.vb
-- ------------------------------------------------------------------------------------------------------------------------------------
/*
if @DisplayAdditionalFields = 1
       Begin     
                           
              IF OBJECT_ID('tempdb..#AddFields') IS NOT NULL
                     DROP TABLE #AddFields

              --Select addition fields into temp table
              Select TblDotFld, Recordid into #AddFields from zAdvSearchAdditionalFields 
                     where [GROUP] = '' + @group + '' and zAdvSearchAdditionalFields.Fldname 
                     not in (select FieldName from Prmry_AdvSearchConfigDetail 
                     where parentrecordid = '' + @recordid + '' and TableName = '' + @JoinMainTable + '')

              Declare @AddId int

              While (Select Count(*) From #AddFields) > 0
                     Begin

                           Select Top 1 @AddId = recordid From #AddFields

                             set @TblDotFld =  (select upper(TblDotFld) from #AddFields where recordid = @AddId)
                             
                           SET    @ReturnFieldList = @ReturnFieldList + ', ' + @TblDotFld

                           Delete #AddFields Where recordid = @AddId

                     End

       END
*/
set @WhereClause = 'WHERE ' + substring(@WhereClause + @CrmRestrictions, 5, 8000)  -- EWH Dec 11, 2014 issue #33974: restrict CRM records by user - added @CrmRestrictions to end of WHERE clause

-- ----------------------------END OF CREATE WHERE CLAUSE PART 2-----------------------------------------------------------------------




-- ------------------------------------------------------------------------------------------------------------------------------------
--  Create ORDER BY clause
--						Notes:       
--						@OrderBy is passed as parameter to this procedure; expected format is "ORDER BY [DisplayName] [DESC]"
--																					ex1: "ORDER BY AEC_MAIN.STATUS DESC"
--																					ex2: "ORDER BY AEC_MAIN.STATUS"
--						Ordered by [KeyField] if @OrderBy is null       
-- ------------------------------------------------------------------------------------------------------------------------------------
if @OrderBy is null
begin
	set @OrderBy = 'ORDER BY [KeyField]'
end
-- -------------------------------------END OF CREATE ORDER BY CLAUSE------------------------------------------------------------------



-- ------------------------------------------------------------------------------------------------------------------------------------
--  Create TOP attribute in select clause
--
--						Notes:       
--						@ReturnRecords supplied by user
-- ------------------------------------------------------------------------------------------------------------------------------------
declare @Top varchar(20)
set @Top = ''
if @ReturnRecords > 0 and @ReturnRecords < 1001
    Begin
            set @Top = 'Top(' + cast(@ReturnRecords as varchar(4)) + ') '
    end
-- ----------------------------------END OF CREATE TOP ATTRIBUTE-----------------------------------------------------------------------



-- ------------------------------------------------------------------------------------------------------------------------------------
--  Validate string lengths of clauses to avoid errors due to truncated queries
--
--						Notes:       
--						Set @return_value = 0 if the query is truncated
--						Opportunity here for splitting @ReturnFieldList to have less columns to make a workable truncated query
-- ------------------------------------------------------------------------------------------------------------------------------------

-- check the overall length to make sure we won't exceed the maximum
set @return_value = 1 -- == true == complete query
if 20 + len(@ReturnFieldList) + len(@Top) + len(@JoinClause) + len(@WhereClause) + len(@OrderBy) > 8000 -- 20 is for the "SELECT DISTINCT " bit
begin
	set @return_value = 0 -- == false == partial / truncated query
	
	-- set @Top = NULL so we return NULL to user
	set @Top = ' NULL'

	-- set remaining clause values to empty strings 
	set @ReturnFieldList = ''
	set @JoinClause = ''
	set @WhereClause = ''
	set @OrderBy = ''
end
-- -----------------------------------------END OF CLAUSE LENGTH VALIDATION------------------------------------------------------------



-- ------------------------------------------------------------------------------------------------------------------------------------
--	Build and execute part 1 - GIS info not included
--
-- ------------------------------------------------------------------------------------------------------------------------------------
select @sql = 'SELECT DISTINCT ' + @Top + @ReturnFieldList + ' ' + @JoinClause + ' ' + @WhereClause + ' ' + ISNULL(@OrderBy,'')
insert #AdvQueryResults exec(@sql)
-- test stuff below:
--Select * from #AdvQueryResults
print(@sql)
--exec(@sql)
-- ----------------------------------END OF BUILD AND EXECUTE PART 1-------------------------------------------------------------------



-- ------------------------------------------------------------------------------------------------------------------------------------
--	Add GIS info
--						Notes:
--						We want these at the end to avoid having to add empty columns earlier in the statement
-- ------------------------------------------------------------------------------------------------------------------------------------
--Add GIS columns to table
alter table #AdvQueryResults add COORD_X float null;
alter table #AdvQueryResults add COORD_Y float null;
alter table #AdvQueryResults add LAT float null;
alter table #AdvQueryResults add LON float null;
alter table #AdvQueryResults add GIS_SITE_GEO_TYPE varchar(255) null;
alter table #AdvQueryResults add GIS_SITE_ALTERNATE_ID varchar(255) null;
--alter table #AdvQueryResults add GIS_SITE_APN varchar(255) null; -- EWH: moved to the top because this column is transformed into the GIS Globe button in the search results gridview

--Update table with the data for the GIS  columns
select @sql = 'Update #AdvQueryResults set COORD_X = GEO_OWNERSHIP.COORD_X, COORD_Y = GEO_OWNERSHIP.COORD_Y, LAT = GEO_OWNERSHIP.LAT , LON = GEO_OWNERSHIP.LON, GIS_SITE_GEO_TYPE = GEO_OWNERSHIP.GEOTYPE, GIS_SITE_ALTERNATE_ID = GEO_OWNERSHIP.SITE_ALTERNATE_ID, GIS_SITE_APN = GEO_OWNERSHIP.SITE_APN FROM GEO_OWNERSHIP WHERE RECORDID =  #AdvQueryResults.LOC_RECORDID'
--print(@sql)
EXEC(@sql)
-- ----------------------------------END OF ADD GIS INFO-------------------------------------------------------------------------------



-- ------------------------------------------------------------------------------------------------------------------------------------
--	Build and execute part 2 - GIS info included now
--
-- ------------------------------------------------------------------------------------------------------------------------------------
--set @sql = 'Select DISTINCT * from #AdvQueryResults ' + ISNULL(@OrderBy,'')
set @sql = 'Select * from #AdvQueryResults ' + ISNULL(@OrderBy,'')
--print(@sql)
exec(@sql)
--print(convert(varchar,getdate(),109)) -- EWH: used for measuring the duration of this proc
return @return_value
-- ----------------------------------END OF BUILD AND EXECUTE PART 2-------------------------------------------------------------------


END


GO
