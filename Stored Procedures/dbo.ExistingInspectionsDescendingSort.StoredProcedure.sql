USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[ExistingInspectionsDescendingSort]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[ExistingInspectionsDescendingSort]
GO
/****** Object:  StoredProcedure [dbo].[ExistingInspectionsDescendingSort]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE  [dbo].[ExistingInspectionsDescendingSort]

AS
BEGIN

Declare @SortedTable TABLE(PERMIT_NO varchar(50), SITE_ADDR varchar(50), InspectionType varchar(50), UserName varchar(50),
							SCHEDULED_DATE varchar(50), SCHEDULED_TIME varchar(50), SCHEDULED_SORTEDTIME time(0), RESULT varchar(50), COMPLETED_DATE varchar(50))


INSERT INTO @SortedTable
SELECT PI.PERMIT_NO,
PM.SITE_ADDR AS [Site Address],
PI.InspectionType,
	(SELECT [UserName] FROM [Prmry_Users]
	 WHERE [UserID] = PI.INSPECTOR) AS ASSIGNED_INSPECTOR,
	 PI.SCHEDULED_DATE,  pi.SCHEDULED_TIME,
	 (SELECT
			CASE
				WHEN
					PI.SCHEDULED_TIME = 'AM'
						THEN
							'00:00:00'
				WHEN
					PI.SCHEDULED_TIME = 'PM'
						THEN
							'12:00:00'
				WHEN
					PI.SCHEDULED_TIME = 'ANY'
						THEN
							'23:59:58'
				WHEN
					PI.SCHEDULED_TIME = NULL
						THEN
							'23:59:59'
				ELSE
					PI.SCHEDULED_TIME
			END AS SCHEDULED_TIME),
	 PI.RESULT,
	 Pi.COMPLETED_DATE
	 
FROM Permit_Main PM INNER JOIN Permit_Inspections PI ON PM.PERMIT_NO = PI.PERMIT_NO
WHERE CONVERT(VARCHAR, PI.SCHEDULED_DATE, 101) = CONVERT(VARCHAR, getdate(), 101)

SELECT * FROM @SortedTable ORDER BY SCHEDULED_SORTEDTIME DESC

END
GO
