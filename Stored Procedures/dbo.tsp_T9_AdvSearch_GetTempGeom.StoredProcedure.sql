USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearch_GetTempGeom]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_AdvSearch_GetTempGeom]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearch_GetTempGeom]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[tsp_T9_AdvSearch_GetTempGeom]
	-- Add the parameters for the stored procedure here
	@recordId integer = -1
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	
	DECLARE @g geometry;
	set @g = (select Boundary from Prmry_AdvSearch_GisTemp where RecordID = @recordId);
	
	select @g.ToString();
END

GO
