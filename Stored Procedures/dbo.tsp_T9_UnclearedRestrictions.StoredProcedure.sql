USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_UnclearedRestrictions]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_UnclearedRestrictions]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_UnclearedRestrictions]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  
CREATE Procedure [dbo].[tsp_T9_UnclearedRestrictions](     
	   @Loc_RecordID			varchar(80)
	   )
	   As
		Begin
	   Select * from geo_restrictions2 where Loc_RecordID =  @Loc_RecordID and (user_cleared is null  or user_cleared = '')
	   End



GO
