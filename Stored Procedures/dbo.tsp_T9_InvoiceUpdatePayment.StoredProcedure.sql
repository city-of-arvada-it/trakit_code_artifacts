USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_InvoiceUpdatePayment]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_InvoiceUpdatePayment]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_InvoiceUpdatePayment]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  					  
-- =====================================================================  
-- Author:		Rance Richardson
-- Create date: 01/24/2014
-- Description:	updates Invoice_Main records upon successful payment in T9 
-- Change History:
--	Date:		Updated by:		Description of Change:
--	01/30/2014	Rance R			Changed the Paid Amount is calculated
-- ===================================================================== 

CREATE procedure [dbo].[tsp_T9_InvoiceUpdatePayment]
	@InvoiceNumber as varchar(40)
	,@UpdateUser as varchar(40)

AS

DECLARE @IsPaid as bit 
SET @IsPaid = (select case when InvoiceTotal = InvoicePaid then 1 else 0 end as 'IsPaid' from Invoice_Main where InvoiceNumber = @InvoiceNumber)

IF @IsPaid = 0
BEGIN
	-- get sum of invoiced, paid fees for invoice
	DECLARE @PaidAmount as money
	SET @PaidAmount = (select IsNull(SUM(PAID_AMOUNT),0) from AEC_FEES where INVOICE_NO = @InvoiceNumber) +
					(select IsNull(SUM(PAID_AMOUNT),0) from CASE_FEES where INVOICE_NO = @InvoiceNumber) +
					(select IsNull(SUM(PAID_AMOUNT),0) from LICENSE2_FEES where INVOICE_NO = @InvoiceNumber) +
					(select IsNull(SUM(PAID_AMOUNT),0) from PERMIT_FEES where INVOICE_NO = @InvoiceNumber) +
					(select IsNull(SUM(PAID_AMOUNT),0) from PROJECT_FEES where INVOICE_NO = @InvoiceNumber)
					
	DECLARE @UPDATE as bit
	SET @UPDATE = (select case when InvoicePaid = @PaidAmount then 0 else 1 end as 'Updateable' from Invoice_Main where InvoiceNumber = @InvoiceNumber)

	IF @UPDATE = 1
	BEGIN
		UPDATE Invoice_Main
		SET InvoicePaid = @PaidAmount
			,InvoiceDue = InvoiceDue - @PaidAmount
			,PaidInFullDate = CASE WHEN InvoicePaid + @PaidAmount = InvoiceTotal THEN GETDATE() END
			,LastUpdated = GETDATE()
			,LastUpdatedBy = @UpdateUser
		WHERE InvoiceNumber = @InvoiceNumber
	END
END



GO
