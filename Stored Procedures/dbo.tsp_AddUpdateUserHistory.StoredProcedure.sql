USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_AddUpdateUserHistory]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_AddUpdateUserHistory]
GO
/****** Object:  StoredProcedure [dbo].[tsp_AddUpdateUserHistory]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create PROCEDURE [dbo].[tsp_AddUpdateUserHistory]
	@Ordinal int ,
	@UserId varchar(50),
    @Module varchar(50),
	@ActivityID varchar(50),
	@Title varchar(100),
	@ImageURL varchar(100)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @UID varchar(10), @RowCount int
	DECLARE @HistoryRows TABLE (UserHistoryID int, Ordinal int)

	IF EXISTS (SELECT TOP 1 1 FROM Prmry_Users WHERE UserID = @UserId)
		SELECT @UID = @UserId
	ELSE
		SELECT @UID = UserID FROM Prmry_Users WHERE UserName = @UserID

	-- Move all existing links down one
	UPDATE dbo.UserHistory SET
	Ordinal = Ordinal + 1
	WHERE UserID = @UID		
		and ActivityID != @ActivityID

	-- Add the most recent link
	IF EXISTS (
		select top 1 1 from dbo.UserHistory 
		where UserID = @UID
		and Module = @Module
		and ActivityID = @ActivityID
	)
	BEGIN
		UPDATE dbo.UserHistory
		SET
			Ordinal = @Ordinal
			,Title = @Title
			,ImageURL = @ImageURL
			,LastModifiedDate = getdate()
		WHERE UserID = @UID
		and Module = @Module
		and ActivityID = @ActivityID
	END
	ELSE IF @UID IS NOT NULL
	BEGIN
		INSERT INTO dbo.UserHistory (ordinal, userId, Module, ActivityID, Title, ImageURL, LastModifiedDate)
		SELECT @Ordinal, @UID, @Module, @ActivityID, @Title, @ImageURL, getdate()
	END

	INSERT INTO @HistoryRows 
	SELECT UserHistoryID, Ordinal
	FROM dbo.UserHistory
	WHERE UserID = @UID
	ORDER BY Ordinal desc

	SELECT @RowCount = @@ROWCOUNT - 20
	IF (@RowCount > 0)
	BEGIN
		;with Oldest AS
		(
			SELECT TOP (@RowCount) UserHistoryID, Ordinal FROM @HistoryRows ORDER BY Ordinal desc
		)
		DELETE uh
		FROM dbo.UserHistory as uh
		INNER JOIN Oldest as hr
			ON uh.UserHistoryID = hr.UserHistoryID		
	END
END

GO
