USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_BatchProcess_MainTable]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_BatchProcess_MainTable]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_BatchProcess_MainTable]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[tsp_T9_BatchProcess_MainTable] 
              @ListUpdateFields as UpdateFields readonly,
              @ListTableAndRecords as MainTableRecords readonly
              
AS
BEGIN

       Declare @SQL Varchar(8000)
       Declare @SQL2 Varchar(1000)
       Declare @SQL3 Varchar(1000)
       Declare @SQLeTrakit Varchar(1000)
       Declare @WhereClause varchar(255)
       Declare @UpdateStmt varchar(50) 
       Declare @TableName varchar(50) 
       Declare @AttachmentsViewableInEtrakit varchar(50)
       Declare @KeyField varchar(50) 
       Declare @ColumnName varchar(50) 
       Declare @ColumnValue varchar(255)
       Declare @RowNum int
       Declare @RowId Int
       declare @UserID as varchar(10)
       declare @TblName as varchar(100)
       declare @GroupName as varchar(255)
       declare @HasNotes Varchar(8000)

       IF object_id('tempdb..#ListUpdateFields') is not null drop table #ListUpdateFields
       IF object_id('tempdb..#MainTableRec') is not null  drop table #MainTableRec
       IF object_id('tempdb..#ForInsertIntoPrmry_Notes') is not null  drop table #ForInsertIntoPrmry_Notes
       IF object_id('tempdb..#ForInsertIntoPrmryAuditTrail') is not null drop table #ForInsertIntoPrmryAuditTrail
       IF object_id('tempdb..#ForInsertIntoPrmry_Notes') is not null drop table #ForInsertIntoPrmry_Notes


              --DECLARE       @ListUpdateFields as UpdateFields 
              --DECLARE       @ListTableAndRecords as MainTableRecords 
              --insert into @ListTableAndRecords(tblName, ActivityNo) values ('PERMIT_Main','bres2015-0233')
              --insert into @ListUpdateFields(RowID,ColumnName,Value) values (1,'UserID','mdm')
              --insert into @ListUpdateFields(RowID,ColumnName,Value) values (2,'groupname','PERMITS')
              --insert into @ListUpdateFields(RowID,ColumnName,Value) values (3,'tblName','Permit_Main')
              --insert into @ListUpdateFields(RowID,ColumnName,Value) values (4,'AttachmentViewableInEtrakit','Not Viewable in eTrakit')




       Create Table #ForInsertIntoPrmry_Notes(  
              ActivityGroup varchar(50),
              ActivityNo varchar(30), 
              ActivityRecordID varchar(50),
              SubGroup varchar(50) ,
              SubGroupRecordID varchar(30),
              UserID varchar(6) ,
              DateEntered datetime, 
              Notes varchar(7500) NULL)

       Create Table #ForInsertIntoPrmryAuditTrail(CURRENT_VALUE varchar(2000),FIELD_NAME varchar(50),
       ORIGINAL_VALUE varchar(2000),PRIMARY_KEY_VALUE varchar(50),SQL_ACTION varchar(1),TABLE_NAME varchar(50),USER_ID varchar(4), TRANSACTION_DATETIME datetime)

              set @HasNotes = ''

              select * into #ListUpdateFields from @ListUpdateFields

              select * into #MainTableRec from @ListTableAndRecords
                         
              set @TableName = (Select distinct tblName from @ListTableAndRecords)

              --use function to get key field
              select @KeyField = dbo.tfn_ActivityNoField(@TableName)

              set @WhereClause =   ' where ' + @KeyField + ' in (Select ActivityNo from #MainTableRec)'
      
              set @SQL  = 'Update ' + @TableName + ' set '

       select @RowId=MAX(RowID) FROM #ListUpdateFields     
       Select @RowNum = Count(*) From #ListUpdateFields      
       WHILE @RowNum > 0                          
       BEGIN   

              select @ColumnName = ColumnName from #ListUpdateFields where RowID= @RowId    
              select @ColumnValue = Value from #ListUpdateFields where RowID= @RowId  

                       if len(ltrim(rtrim(@ColumnValue))) > 0 and @ColumnName ='UserID'
                     Begin
                           set @UserID = @ColumnValue
                                  End

                       if len(ltrim(rtrim(@ColumnValue))) > 0 and @ColumnName ='GroupName'
                     Begin
                           set @GroupName = @ColumnValue
                     End 

                     if len(ltrim(rtrim(@ColumnValue))) > 0 and @ColumnName ='TblName'
                     Begin
                           set @TblName = @ColumnValue 
                     End 

                     if len(ltrim(rtrim(@ColumnValue))) > 0 and @ColumnName ='AttachmentViewableInEtrakit'
                     Begin
                           set @AttachmentsViewableInEtrakit = @ColumnValue 
                     End 

                     if len(ltrim(rtrim(@ColumnValue))) > 0 and @ColumnName ='Notes'
                     Begin
                          set @HasNotes =  @ColumnValue
                     End 

              if len(ltrim(rtrim(@ColumnValue))) > 0 and @ColumnName not in ('UserID', 'GroupName','TblName','AttachmentViewableInEtrakit')
                     Begin
                           set @SQL = @SQL + @ColumnName +  ' = ''' + @ColumnValue + ''','
                    End 

              select top 1 @RowId=RowID from #ListUpdateFields where RowID < @RowId order by RowID desc
              set @RowNum = @RowNum - 1                               
       
          END
         
          if ltrim(rtrim(@AttachmentsViewableInEtrakit)) = 'Viewable in eTrakit'
       Begin
                 set @SQLeTrakit = 'update ' +  @TblName + '  set ETRAKIT_ENABLED = 1 ' + @WhereClause + ';'
                 set @SQLeTrakit = replace(@SQLeTrakit,'_main','_attachments')


                     print(@SQLeTrakit)
                 exec(@SQLeTrakit)

          End

          if ltrim(rtrim(@AttachmentsViewableInEtrakit)) = 'Not Viewable in eTrakit'
       Begin
                 set @SQLeTrakit = 'update ' +  @TblName + '  set ETRAKIT_ENABLED = 0 ' + @WhereClause + ';'
                 set @SQLeTrakit = replace(@SQLeTrakit,'_main','_attachments')

                     print(@SQLeTrakit)
                 exec(@SQLeTrakit)

          End




          if len(ltrim(rtrim(@HasNotes))) > 0
          Begin
insert into #ForInsertIntoPrmry_Notes(ActivityNo) select ActivityNO from #MainTableRec  
                     Update #ForInsertIntoPrmry_Notes set ActivityGroup = (select Value from #ListUpdateFields where ColumnName = 'Groupname')
                     Update #ForInsertIntoPrmry_Notes set Notes = (select Value from #ListUpdateFields where ColumnName = 'Notes')
                     Update #ForInsertIntoPrmry_Notes set UserID = (select Value from #ListUpdateFields where ColumnName = 'UserID')
                     Update #ForInsertIntoPrmry_Notes set DateEntered = getdate()
                     Update #ForInsertIntoPrmry_Notes set activityrecordid = pm.recordid from permit_main pm inner join #ForInsertIntoPrmry_Notes fi on pm.PERMIT_NO  = fi.ActivityNo

                     --Finally
                     INSERT INTO prmry_notes (ActivityGroup, ACTIVITYNO, ActivityRecordID, Userid, DateEntered, Notes)
                     SELECT ActivityGroup, ActivityNo, ActivityRecordID, Userid, DateEntered, Notes
                     FROM #ForInsertIntoPrmry_Notes

          End
          
       set @SQL = left(@SQL, (len(@sql)-1))
       
       --For Audit
       set @SQL2 = @SQL

       set @SQL = @SQL + @WhereClause + ';'
              
       print(@SQL)
       exec(@SQL)

       insert into #ForInsertIntoPrmryAuditTrail(TABLE_NAME,PRIMARY_KEY_VALUE ) select tblName, ActivityNo from #MainTableRec

       insert into Prmry_AuditTrail (CURRENT_VALUE, PRIMARY_KEY_VALUE, SQL_ACTION, TABLE_NAME,TRANSACTION_DATETIME, USER_ID) select @SQL2, PRIMARY_KEY_VALUE, 'U', @TblName, getdate(), @UserID from #ForInsertIntoPrmryAuditTrail



END



GO
