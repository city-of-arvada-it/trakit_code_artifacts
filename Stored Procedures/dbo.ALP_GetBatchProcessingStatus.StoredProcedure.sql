USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[ALP_GetBatchProcessingStatus]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[ALP_GetBatchProcessingStatus]
GO
/****** Object:  StoredProcedure [dbo].[ALP_GetBatchProcessingStatus]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ====================================================================================================================
-- Author:		Erick Hampton
-- Create date: Apr 21, 2017
-- Description:	Gets the processing status of a batch.  
--				Returns:
--				1. Batch processing summary info, license counts and status
--				2. Distinct list of license numbers, license status, and the count of its operations by status
-- ====================================================================================================================
CREATE PROCEDURE [dbo].[ALP_GetBatchProcessingStatus]

	@BatchID int = -1 

AS
BEGIN

	declare @BatchStatus varchar(20) = NULL
	declare @Mode char(6) = NULL -- REVIEW or UPDATE
	declare @WhenSubmitted datetime = NULL
	declare @WhenCompleted datetime = NULL
	declare @PercentComplete float = NULL
	declare @LicensesTotal int = NULL
	declare @LicensesPending int = NULL
	declare @LicensesSuccessful int = NULL
	declare @LicensesFailed int = NULL


--  create table of aggregates, since we will reference this data more than once
	declare @info table (
			  LicenseNumber varchar(20)
			, OperationsTotal int
			, OperationsPending int
			, OperationsSuccessful int
			, OperationsFailed int
			, LicenseStatus as (case when OperationsFailed > 0 then 'FAILED' 
									when OperationsSuccessful = OperationsTotal then 'COMPLETED'
									when OperationsPending = OperationsTotal then 'PENDING'
									else convert(varchar, convert(int, (convert(float, OperationsPending) / convert(float, OperationsTotal)) * 100)) + '% COMPLETE'
									end))

--	populate the @info table
	insert into @info (LicenseNumber, OperationsTotal, OperationsPending, OperationsSuccessful, OperationsFailed)
	select LicenseNumber
		 , count(OperationID) [OperationsTotal]
		 , sum(case when OperationStatusID < 2 then 1 else 0 end) [OperationsPending]
		 , sum(case when OperationStatusID in (3,4) then 1 else 0 end) [OperationsSuccessful]
		 , sum(case when OperationStatusID in (2,5) then 1 else 0 end) [OperationsFailed]
	from [ALP_BatchParameterSetLicenseNumberOperationStatus] 
	where BatchID=@BatchID 
	group by BatchID, LicenseNumber 



--	1. Batch processing summary info, license counts and status
	select @BatchStatus = bs.Name, @WhenSubmitted = b.CreationDateTime, @Mode = case when IsUpdateMode = 1 then 'UPDATE' else 'REVIEW' end
	from ALP_Batches b inner join ALP_BatchStatuses bs on b.StatusID = bs.ID
	where b.Id=@BatchID

	if (select count(*) from [ALP_BatchParameterSetLicenseNumberOperationStatus] where BatchID=@BatchID and OperationEnd is null) = 0
	select @WhenCompleted = max(OperationEnd) from [ALP_BatchParameterSetLicenseNumberOperationStatus] where BatchID=@BatchID

	set @LicensesTotal = (select count(*) from @info)
	set @LicensesPending = (select count(*) from @info where OperationsPending > 0)
	set @LicensesSuccessful = (select count(*) from @info where OperationsSuccessful > 0)
	set @LicensesFailed = (select count(*) from @info where OperationsFailed > 0)
	set @PercentComplete = (100 - ((convert(float, @LicensesPending) / convert(float, @LicensesTotal)) * 100))

--	return the data	
	select @BatchStatus [BatchStatus]
		 , @PercentComplete [PercentComplete]
		 , @WhenSubmitted [WhenSubmitted]
		 , @WhenCompleted [WhenCompleted]
		 , @Mode [Mode]
		 , @LicensesTotal [LicensesTotal]
		 , @LicensesPending [LicensesPending]
		 , @LicensesSuccessful [LicensesSuccessful]
		 , @LicensesFailed [LicensesFailed]
		 

--  2. Distinct list of license numbers, license status, and the count of its operations by status
	select LicenseNumber, LicenseStatus, OperationsTotal, OperationsPending, OperationsSuccessful, OperationsFailed from @info
	

END

GO
