USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GetFollowHistoryDetails]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_GetFollowHistoryDetails]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GetFollowHistoryDetails]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



Create PROCEDURE [dbo].[tsp_GetFollowHistoryDetails]
	@ActivityNo VARCHAR(100),
	@DayCount int,
	@IsAllowClear bit,
	@userID varchar(100)

AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF @IsAllowClear = 1
	BEGIN 

		SELECT CONVERT(VARCHAR, updatedate, 101) + ' ' 
			   + Replace(Str(Datepart(hour, updatedate), 2), ' ', '0') 
			   + ':' 
			   + Replace(Str(Datepart(n, updatedate), 2), ' ', '0') [UpdateDate], 
			   audit.[group], 
			   tablename, 
			   Max(auditid)                                         [AuditID], 
			   Sum(CASE [type] 
					 WHEN 'i' THEN 1 
					 ELSE 0 
				   END)                                             [CountInserted], 
			   Sum(CASE [type] 
					 WHEN 'u' THEN 1 
					 ELSE 0 
				   END)                                             [CountUpdated], 
			   Sum(CASE [type] 
					 WHEN 'd' THEN 1 
					 ELSE 0 
				   END)                                             [CountDeleted] 
		FROM   audit (nolock) 
			   INNER JOIN follow (nolock) 
					   ON follow.activityno = audit.activityno 
		WHERE  audit.activityno = ( CASE 
									  WHEN audit.[group] = 'CRM' THEN 
									  CONVERT(VARCHAR(128), (SELECT TOP 1 issue_id 
															 FROM   crm_issues 
															 WHERE 
															 issue_label = @ActivityNo)) 
									  ELSE @ActivityNo 
									END ) 
			   AND Datediff(day, updatedate, Getdate()) <= @DayCount 
			   AND (audit.auditid > follow.lastauditidcleared  OR follow.lastauditidcleared IS NULL)
			   AND follow.UserID = @userID
		GROUP  BY CONVERT(VARCHAR, updatedate, 101) + ' ' 
				  + Replace(Str(Datepart(hour, updatedate), 2), ' ', '0') 
				  + ':' 
				  + Replace(Str(Datepart(n, updatedate), 2), ' ', '0'), 
				  audit.[group], 
				  tablename 
		ORDER  BY updatedate DESC 

	END

	ELSE 
	BEGIN 

		SELECT CONVERT(VARCHAR, updatedate, 101) + ' ' 
			   + Replace(Str(Datepart(hour, updatedate), 2), ' ', '0') 
			   + ':' 
			   + Replace(Str(Datepart(n, updatedate), 2), ' ', '0') [UpdateDate], 
			   [group], 
			   tablename, 
			   Max(auditid)                                         [AuditID], 
			   Sum(CASE [type] 
					 WHEN 'i' THEN 1 
					 ELSE 0 
				   END)                                             [CountInserted], 
			   Sum(CASE [type] 
					 WHEN 'u' THEN 1 
					 ELSE 0 
				   END)                                             [CountUpdated], 
			   Sum(CASE [type] 
					 WHEN 'd' THEN 1 
					 ELSE 0 
				   END)                                             [CountDeleted] 
		FROM   audit 
		WHERE  activityno = ( CASE 
								WHEN [group] = 'CRM' THEN CONVERT(VARCHAR(128), 
														  (SELECT TOP 1 issue_id 
														   FROM   crm_issues 
														   WHERE 
														   issue_label = @ActivityNo)) 
								ELSE @ActivityNo
							  END ) 
			   AND Datediff(day, updatedate, Getdate()) <= @DayCount 			
		GROUP  BY CONVERT(VARCHAR, updatedate, 101) + ' ' 
				  + Replace(Str(Datepart(hour, updatedate), 2), ' ', '0') 
				  + ':' 
				  + Replace(Str(Datepart(n, updatedate), 2), ' ', '0'), 
				  [group], 
				  tablename 
		ORDER  BY updatedate DESC 
	END

END


GO
