USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[DisableGeoTriggers]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[DisableGeoTriggers]
GO
/****** Object:  StoredProcedure [dbo].[DisableGeoTriggers]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create Procedure [dbo].[DisableGeoTriggers]

AS
 Declare @string varchar(8000)
 Declare @tableName nvarchar(500)
 Declare cur cursor
 for select name as tbname from sysobjects where id in(select parent_obj from sysobjects where xtype='tr') and name LIKE ('geo_%')
open cur
 fetch next from cur into @tableName
 while @@fetch_status = 0
 begin
 set @string ='Alter table '+ @tableName + ' Disable trigger all'
exec (@string)
 Fetch next from cur into @tableName
 end
 close cur
 deallocate cur


GO
