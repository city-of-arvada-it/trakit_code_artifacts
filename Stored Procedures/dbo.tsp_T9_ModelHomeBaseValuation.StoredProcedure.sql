USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ModelHomeBaseValuation]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_ModelHomeBaseValuation]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ModelHomeBaseValuation]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  
CREATE PROCEDURE [dbo].[tsp_T9_ModelHomeBaseValuation]
             @ModelRecordID varchar(10)
AS
BEGIN

       Declare @SQL Varchar(8000)

       if @ModelRecordID <> ''
              Begin

					SELECT  MBV.RecordID, ModelRecordID, ValuationCode, VS.[Description],  QUANTITY,  MBV.ORDERID FROM Prmry_ModelHomeBaseValuations MBV
					Join Prmry_ValuationSchedule  VS on  MBV.ValuationCode = VS.CODE AND VS.UNITS = 'EA' And VS.GroupName = 'PERMITS'
					where ModelRecordID = @ModelRecordID



              end

END






GO
