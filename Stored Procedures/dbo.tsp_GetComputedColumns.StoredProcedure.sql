USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GetComputedColumns]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_GetComputedColumns]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GetComputedColumns]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[tsp_GetComputedColumns]
	@TableName varchar(250)
As
Begin
	SELECT name FROM sys.columns
	WHERE is_computed = 1
	AND object_id = OBJECT_ID(@TableName)
End

GO
