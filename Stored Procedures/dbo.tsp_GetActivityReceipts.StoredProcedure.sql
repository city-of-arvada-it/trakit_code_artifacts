USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GetActivityReceipts]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_GetActivityReceipts]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GetActivityReceipts]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

					  					  					  
/*************************************************
-- Change History:
-- RRR	05/01/2014	Changed varchar column width for temp tables #Receipts and #Fees to accomodate increased column widths by clients
--
--
**************************************************/
					  
/*

tsp_GetActivityReceipts @Group='permit',@ActivityNo='A02-0002'

*/




CREATE Procedure [dbo].[tsp_GetActivityReceipts] (
@Group Varchar(40),
@ActivityNo Varchar(40),
@Bonds bit = NULL
)


As

Declare @SQL Varchar(8000)

	DECLARE @TableName varchar(40)
	
	IF @Bonds = 1
		select @TableName =  CASE
			when PATINDEX('PERMIT%',@Group) = 1 then 'PERMIT_Bonds'
			when PATINDEX('PROJECT%',@Group) = 1 then 'PROJECT_Bonds'
		End
	ELSE
		select @TableName =  CASE
			When PATINDEX('AEC%',@Group) = 1 then 'AEC_Fees'
			When PATINDEX('CASE%',@Group) = 1 then 'CASE_Fees'
			When PATINDEX('LICENSE2%',@Group) = 1 then 'LICENSE2_Fees'
			when PATINDEX('PERMIT%',@Group) = 1 then 'PERMIT_Fees'
			when PATINDEX('PROJECT%',@Group) = 1 then 'PROJECT_Fees'
		End
		
	Declare @KeyField Varchar(40)
	
	Select @KeyField = dbo.tfn_ActivityNoField(@TableName)
	
	Print(@TableName)
	Print(@KeyField)
	
	
	--RB Check if table exists before creating it
   IF OBJECT_ID('tempdb..#Receipts') IS NOT NULL  
	drop table #Receipts

	Create Table #Receipts(ReceiptNo Varchar(200),RecordIDUsed Varchar(200),PaidTotal Money Default 0,RecordIDList Varchar(5000) Default '',PayMethod Varchar(200),PaidBy Varchar(200),PaidDate Datetime,PARENT_RECORDID Varchar(30))
	
		--RB Check if table exists before creating it
   IF OBJECT_ID('tempdb..#Fees') IS NOT NULL  
	drop table #Fees
	
	Create Table #Fees(RecordID Varchar(200),PayMethod Varchar(200),PaidBy Varchar(200),PaidDate Datetime,PaidAmount Float,ReceiptNo Varchar(200),PARENT_RECORDID Varchar(30))
	
	IF @Bonds = 1
	Begin
		Set @SQL = '
		Insert Into #Fees(RecordID,PayMethod,PaidBy,PaidDate,PaidAmount,ReceiptNo,PARENT_RECORDID)
		Select RecordID,Pay_Method,Paid_By,Paid_Date,Paid_Amount,Receipt_No, CASE WHEN PARENT_RECORDID IS NULL THEN RecordID ELSE PARENT_RECORDID END AS PARENT_RECORDID
		From ' + @TableName + '
		Where ' + @KeyField + ' = ''' + @ActivityNo + '''
		AND Receipt_No Is Not Null AND Receipt_No <> '''' AND Pay_Method <> ''DEPOSIT'''
	
		Print(@SQL)
		Exec(@SQL)
	
		Set @SQL = '
		Insert Into #Receipts(ReceiptNo,PARENT_RECORDID)
		Select Distinct Receipt_No,CASE WHEN PARENT_RECORDID IS NULL THEN RecordID ELSE PARENT_RECORDID END AS PARENT_RECORDID From ' + @TableName + '
		Where ' + @KeyField + ' = ''' + @ActivityNo + '''
		AND Receipt_No Is Not Null AND Receipt_No <> '''' AND Pay_Method <> ''DEPOSIT'''

		Print(@SQL)
		Exec(@SQL)
	End
	ELSE
	Begin
		Set @SQL = '
		Insert Into #Fees(RecordID,PayMethod,PaidBy,PaidDate,PaidAmount,ReceiptNo)
		Select RecordID,Pay_Method,Paid_By,Paid_Date,Paid_Amount,Receipt_No 
		From ' + @TableName + '
		Where ' + @KeyField + ' = ''' + @ActivityNo + '''
		AND Receipt_No Is Not Null AND Receipt_No <> '''' AND Pay_Method <> ''DEPOSIT'''
	
		Print(@SQL)
		Exec(@SQL)
	
		Set @SQL = '
		Insert Into #Receipts(ReceiptNo)
		Select Distinct Receipt_No From ' + @TableName + '
		Where ' + @KeyField + ' = ''' + @ActivityNo + '''
		AND Receipt_No Is Not Null AND Receipt_No <> '''' AND Pay_Method <> ''DEPOSIT'''
	
		Print(@SQL)
		Exec(@SQL)
	End
	
	IF @Bonds = 1
	BEGIN
		Update tr Set
		PaidTotal = tf.Total
		From #Receipts tr
		JOIN (
			Select ReceiptNo,PARENT_RECORDID,Total=min(PaidAmount)
			From #Fees
			Group By ReceiptNo,PARENT_RECORDID
		) tf
		 ON tr.ReceiptNo = tf.ReceiptNo and tr.PARENT_RECORDID = tf.PARENT_RECORDID 
		  
		Update tr Set
		PaidTotal = tf.Total,PARENT_RECORDID=''
		From #Receipts tr
		JOIN (
			Select ReceiptNo,Total=sum(PaidTotal)
			From #Receipts
			Group By ReceiptNo
		) tf
		 ON tr.ReceiptNo = tf.ReceiptNo
	END
	ELSE
		Update tr Set
	PaidTotal = tf.Total
	From #Receipts tr
	JOIN (
		Select ReceiptNo,Total=SUM(PaidAmount)
		From #Fees
		Group By ReceiptNo
	) tf
	 ON tr.ReceiptNo = tf.ReceiptNo
	
	
	While Exists ( Select * From #Fees )
	 Begin
	
		Print 'Update'
		Update tr Set
		RecordIDUsed = tf.RecordID,
		PayMethod = tf.PayMethod,PaidBy = tf.PaidBy,PaidDate = tf.PaidDate,
		RecordIDList = RecordIDList + tf.RecordID + ','
		From #Receipts tr
		JOIN #Fees tf
		 ON tr.ReceiptNo = tf.ReceiptNo
		 
		Print 'Delete'
		Delete tf
		From #Receipts tr
		JOIN #Fees tf
		 ON tr.RecordIDUsed = tf.RecordID
		 
		Update #Receipts Set RecordIDUsed = Null
	 
	 End
	
	
	
	 
	 Update tr Set
	 RecordIDList = SubString(RecordIDList,1,Len(RecordIDList)-1)
	 From #Receipts tr
	 
	 
	 Select distinct * From #Receipts
	 
	 
/*


tsp_GetActivityReceipts @Group='permit',@ActivityNo='A02-0002'

*/





GO
