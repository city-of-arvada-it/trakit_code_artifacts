USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GblSearchReviewCenterE3_test]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_GblSearchReviewCenterE3_test]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GblSearchReviewCenterE3_test]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  


/*

tsp_GblSearchReviewCenter 
@DateType='Date_Sent'
,@vStartDate='6/1/12'
,@vEndDate='6/30/12'
,@ReviewGroup=''
,@Group='Permit,Geo,License2'
,@Type=''
,@ContactID='Mark Page,Robert Eyer'
,@ShowOverdueItems=1

select * from etrakit_user_accounts

select Distinct field01 = groupname + ' - ' + field01 from Prmry_ReviewControl where category = 'review_set'

select * from tvw_GblActivitiesE3 where tgroup = 'crm'

select * from Prmry_ReviewControl

Select Top 500 *
From tvw_GblReviewsE3 
Where 1 = 1 
AND (Remarks Is Null OR Remarks NOT LIKE 'VOIDED (%')

 --AND ( tGroup = 'Permits' AND ReviewGroup = 'All')  
 AND ( 
 ( Date_Due >= Convert(Datetime,'Jun  1 2012 12:00AM') AND 
 Date_Due < Convert(Datetime,'Jun 22 2012 12:00AM') )  ) 

select distinct field01 from Prmry_ReviewControl where category = 'review_type'

select Distinct Value = groupname + '|' + field01, Text = replace(groupname,'license2','LICENSES') + ' - ' + field01 from Prmry_ReviewControl where category = 'review_set'

tsp_GblSearchReviewCenter @ContactID = '',  @DateType = 'Date_Due',  @vStartDate = '6/1/2012 12:00:00 AM',  @vEndDate = '6/21/2012 12:00:00 AM',  @ReviewGroupName = 'All',  @ReviewGroupGroupName = 'Permits'

*/

CREATE Procedure [dbo].[tsp_GblSearchReviewCenterE3_test](
@ContactID Varchar(1000) = Null,
@vStartDate Varchar(100) = Null,
@vEndDate Varchar(100) = Null,
@DateType Varchar(100) = Null,
@ReviewGroupName Varchar(500) = Null,
@ReviewGroupGroupName Varchar(500) = Null,
@Group Varchar(500) = Null,
@Type Varchar(500) = Null,
@ShowOverdueItems INT = Null
)


As

Declare @SQL Varchar(8000)
Declare @WHERE Varchar(5000)
Declare @StartDate Datetime 
Declare @EndDate Datetime

Set @WHERE = ''
Set @StartDate  = Null
Set @EndDate  = Null
If @vStartDate = '' Set @vStartDate = Null
If @vStartDate = '1/1/0001 12:00:00 AM' Set @vStartDate = Null
If @vEndDate = '' Set @vEndDate = Null
If @vEndDate = '1/1/0001 12:00:00 AM' Set @vEndDate = Null

If @vStartDate Is Null Set @vStartDate = Convert(Varchar,GetDate(),101)
If @vEndDate Is Null Set @vEndDate = Convert(Varchar,GetDate(),101)

Set @StartDate = CONVERT(Datetime,@vStartDate)
Set @EndDate = CONVERT(Datetime,DATEADD(dd,1,@vEndDate))


If @ContactID = '' Set @ContactID = Null
If @ReviewGroupName = '' Set @ReviewGroupName = Null
If @ReviewGroupGroupName = '' Set @ReviewGroupGroupName = Null
If @DateType = '' Set @DateType = Null
If @vStartDate = '' Set @vStartDate = Null
If @vEndDate = '' Set @vEndDate = Null
If @Group = '' Set @Group = Null
If @Type = '' Set @Type = Null


If @ContactID Is Not Null Set @WHERE = @WHERE + CHAR(10) + ' AND CONTACT IN (''' + REPLACE(@ContactID,',',''',''') + ''')'
If @ReviewGroupGroupName Is Not Null Set @ReviewGroupGroupName = Replace(Replace(Replace(@ReviewGroupGroupName,'LICENSES','LICENSE2'),'PROJECTS','PROJECT'),'PERMITS','PERMIT')
If @ReviewGroupName Is Not Null Set @WHERE = @WHERE + CHAR(10) + ' AND ( tGroup = ''' + @ReviewGroupGroupName + ''' AND ReviewGroup = ''' + @ReviewGroupName + ''') '


If @Group Is Not Null Set @WHERE = @WHERE + CHAR(10) + ' AND tGroup IN (''' + REPLACE(@Group,',',''',''') + ''')'
If @Type Is Not Null Set @WHERE = @WHERE + CHAR(10) + ' AND ReviewType IN (''' + REPLACE(@Type,',',''',''') + ''')'

If @vStartDate Is Not Null Set @StartDate = CONVERT(Datetime,@vStartDate)
If @vEndDate Is Not Null Set @EndDate = CONVERT(Datetime,DATEADD(dd,1,@vEndDate))
Print(@StartDate)
Print(@EndDate)

 
If @DateType Is Not Null
 Begin
	If @StartDate Is Not Null 
	 Begin
		Set @WHERE = @WHERE + ' AND ( '
		Set @WHERE = @WHERE + CHAR(10) + ' ( ' + @DateType + ' >= Convert(Datetime,''' + Convert(Varchar,@StartDate) + ''')'
		If @EndDate Is Null Set @WHERE = @WHERE + ' ) '
	 End
	 
	If @EndDate Is Not Null 
	 Begin
		If @StartDate Is Null Set @WHERE = @WHERE + ' AND ( ( ' ELSE Set @WHERE = @WHERE + ' AND '
		Set @WHERE = @WHERE + CHAR(10) + ' ' + @DateType + ' < Convert(Datetime,''' + Convert(Varchar,@EndDate) + ''') ) '
	 End
	 
	 If @ShowOverdueItems Is Null Set @WHERE = @WHERE + ' ) '
 End

 If @ShowOverdueItems Is Not Null 
	Set @WHERE = @WHERE + CHAR(10) + ' OR ( DATE_DUE < CAST(FLOOR(CAST(getdate() AS float)) AS DATETIME) AND DATE_RECEIVED is null) )'


Create Table #Results(ActivityNo Varchar(40),RecordID Varchar(40),Loc_RecordID Varchar(40),tGroup Varchar(40),AttachmentTotal INT Default 0, ConditionsTotal INT Default 0, NotesTotal INT Default 0, RestrictionsTotal INT Default 0, FeesDue Money Default 0, RestrictionsSummary Varchar(5000) Default '',RestrictionsRecordIDUsed Varchar(40),RECORD_TYPE Varchar(30),RECORD_STATUS Varchar(30),SITE_ADDR Varchar(500),InspectorName Varchar(150))

Set @SQL = '
Insert Into #Results(ActivityNo,RecordID,tGroup)
Select Top 500 ActivityNo,RecordID,tGroup
From tvw_GblReviewsE3 
Where 1 = 1 
AND (Remarks Is Null OR Remarks NOT LIKE ''VOIDED (%'')
' + @WHERE
PRINT(@SQL)
EXEC(@SQL)

Update #Results Set
Loc_RecordID = a.Loc_RecordID,
Record_Type = a.ActivityType,
Record_Status = a.[Status],
SITE_ADDR = a.SITE_ADDR + IsNull(', ' + a.SITE_CITY,'')
From #Results tr
JOIN tvw_GblActivitiesE3 a
 ON tr.ActivityNo = a.ActivityNo
 AND tr.tGroup = a.TGroup
  
--Update #Results Set
--Loc_RecordID = a.Loc_RecordID
--From #Results tr
--JOIN tvw_GblActivitiesE3 a
-- ON tr.ActivityNo = a.ActivityNo
-- AND tr.tGroup = a.TGroup
 

Print 'Note Count'
Update #Results Set
NotesTotal = CASE WHEN n.Total Is Null THEN 0 ELSE n.Total END
From #Results tr
LEFT JOIN (
	Select Total=Count(*),SubGroupRecordID From Prmry_Notes
	WHERE SubGroup = 'REVIEW'
	Group By SubGroupRecordID
 ) n
  ON tr.RecordID = n.SubGroupRecordID

  
Print 'Restriction Count'
Update #Results Set
RestrictionsTotal = CASE WHEN r.Total Is Null THEN 0 ELSE r.Total END
From #Results tr
LEFT JOIN (
	Select Total=Count(*),Loc_RecordID From Geo_Restrictions2
	Where Date_Cleared Is Null AND Restriction_Notes Not Like 'VOIDED (%'
	Group By Loc_RecordID
 ) r
  ON tr.Loc_RecordID = r.Loc_RecordID
  

Print 'Table #Restrictions'
Create Table #Restrictions(RecordID Varchar(30),Loc_RecordID Varchar(40),Summary Varchar(4000), FlagForDelete INT Default 0)
Insert Into #Restrictions(RecordID,Loc_RecordID,Summary)
Select r.RecordID,r.Loc_RecordID,
Summary = r.Restriction_Type + ' ('
+ Convert(Varchar,r.Date_Added,101) + ') ' + SubString(r.Restriction_Notes,1,50)
From #Results tr
JOIN Geo_RESTRICTIONS2 r
 ON tr.Loc_RecordID = r.Loc_RecordID
Where r.Date_Cleared Is Null AND r.Restriction_Notes Not Like 'VOIDED (%'

While Exists ( Select * From #Restrictions )
 Begin

	Print 'update #results'
	Update #Results Set
	RestrictionsSummary = RestrictionsSummary + r.Summary + '<br />',
	RestrictionsRecordIDUsed = r.RecordID
	From #Results tr
	JOIN #Restrictions r
	 ON tr.Loc_RecordID = r.Loc_RecordID
	 
	Print 'summary used'
	Update r Set
	FlagForDelete = 1
	From #Results tr
	JOIN #Restrictions r
	 ON tr.RestrictionsRecordIDUsed = r.RecordID
	 
	Print 'update #results 2'
	Update #Results Set
	RestrictionsRecordIDUsed = Null
	From #Results tr
	JOIN #Restrictions r
	 ON tr.Loc_RecordID = r.Loc_RecordID
	 
	Print 'delete flaggedfordelete records'
	Delete #Restrictions Where FlagForDelete = 1
	 
 End 
  
Print 'Permit Attachments'
Update #Results Set
AttachmentTotal = CASE WHEN a.Total Is Null THEN 0 ELSE a.Total END
From #Results tr
LEFT JOIN (
	Select Total=Count(*),Permit_No From Permit_Attachments
	Group By Permit_No
 ) a
  ON tr.ActivityNo = a.Permit_No
WHERE tr.tGroup = 'PERMIT'
 
Print 'Project Attachments'
Update #Results Set
AttachmentTotal = CASE WHEN a.Total Is Null THEN 0 ELSE a.Total END
From #Results tr
LEFT JOIN (
	Select Total=Count(*),Project_No From Project_Attachments
	Group By Project_No
 ) a
  ON tr.ActivityNo = a.Project_No
WHERE tr.tGroup = 'PROJECT'

Print 'Case Attachments'
Update #Results Set
AttachmentTotal = CASE WHEN a.Total Is Null THEN 0 ELSE a.Total END
From #Results tr
LEFT JOIN (
	Select Total=Count(*),Case_No From Case_Attachments
	Group By Case_No
 ) a
  ON tr.ActivityNo = a.Case_No
WHERE tr.tGroup = 'CASE'

Print 'License2 Attachments'
Update #Results Set
AttachmentTotal = CASE WHEN a.Total Is Null THEN 0 ELSE a.Total END
From #Results tr
LEFT JOIN (
	Select Total=Count(*),License_No From License2_Attachments
	Group By License_No
 ) a
  ON tr.ActivityNo = a.License_No
WHERE tr.tGroup = 'LICENSE2'

Print 'AEC Attachments'
Update #Results Set
AttachmentTotal = CASE WHEN a.Total Is Null THEN 0 ELSE a.Total END
From #Results tr
LEFT JOIN (
	Select Total=Count(*),ST_LIC_NO From AEC_Attachments
	Group By ST_LIC_NO
 ) a
  ON tr.ActivityNo = a.ST_LIC_NO
WHERE tr.tGroup = 'AEC'

Print 'Geo Attachments'
Update #Results Set
AttachmentTotal = CASE WHEN a.Total Is Null THEN 0 ELSE a.Total END
From #Results tr
LEFT JOIN (
	Select Total=Count(*),LOC_RECORDID From Geo_Attachments
	Group By LOC_RECORDID
 ) a
  ON tr.ActivityNo = a.LOC_RECORDID
WHERE tr.tGroup = 'GEO'


Print 'Permit Fees Due'
Update #Results Set
FeesDue = CASE WHEN a.Total Is Null THEN 0 ELSE a.Total END
From #Results tr
LEFT JOIN (
	Select Total=Sum(Balance_Due),Permit_No From Permit_Main
	Group By Permit_No
 ) a
  ON tr.ActivityNo = a.Permit_No
WHERE tr.tGroup = 'PERMIT'

Print 'Project Fees Due'
Update #Results Set
FeesDue = CASE WHEN a.Total Is Null THEN 0 ELSE a.Total END
From #Results tr
LEFT JOIN (
	Select Total=Sum(Balance_Due),Project_No From Project_Main
	Group By Project_No
 ) a
  ON tr.ActivityNo = a.Project_No
WHERE tr.tGroup = 'PROJECT'

Print 'Case Fees Due'
Update #Results Set
FeesDue = CASE WHEN a.Total Is Null THEN 0 ELSE a.Total END
From #Results tr
LEFT JOIN (
	Select Total=Sum(Balance_Due),Case_No From Case_Main
	Group By Case_No
 ) a
  ON tr.ActivityNo = a.Case_No
WHERE tr.tGroup = 'CASE'

Print 'License2 Fees Due'
Update #Results Set
FeesDue = CASE WHEN a.Total Is Null THEN 0 ELSE a.Total END
From #Results tr
LEFT JOIN (
	Select Total=Sum(Balance_Due),License_No From License2_Main
	Group By License_No
 ) a
  ON tr.ActivityNo = a.License_No
WHERE tr.tGroup = 'LICENSE2'

Print 'AEC Fees Due'
Update #Results Set
FeesDue = CASE WHEN a.Total Is Null THEN 0 ELSE a.Total END
From #Results tr
LEFT JOIN (
	Select Total=Sum(Balance_Due),ST_LIC_NO From AEC_Main
	Group By ST_LIC_NO
 ) a
  ON tr.ActivityNo = a.ST_LIC_NO
WHERE tr.tGroup = 'AEC'


Print 'Conditions Count - License2'
Update #Results Set
ConditionsTotal = CASE WHEN c.Total Is Null THEN 0 ELSE c.Total END
From #Results tr
LEFT JOIN (
	Select Total=Count(*),LICENSE_NO From License2_Conditions2
	Group By LICENSE_NO
 ) c
  ON tr.ActivityNo = c.LICENSE_NO
WHERE tr.tGroup = 'LICENSE2'
  
Print 'Conditions Count - Project'
Update #Results Set
ConditionsTotal = CASE WHEN c.Total Is Null THEN 0 ELSE c.Total END
From #Results tr
LEFT JOIN (
	Select Total=Count(*),PROJECT_NO From Project_Conditions2
	Group By PROJECT_NO
 ) c
  ON tr.ActivityNo = c.PROJECT_NO
WHERE tr.tGroup = 'PROJECT'
  
Print 'Conditions Count - Permit'
Update #Results Set
ConditionsTotal = CASE WHEN c.Total Is Null THEN 0 ELSE c.Total END
From #Results tr
LEFT JOIN (
	Select Total=Count(*),PERMIT_NO From Permit_Conditions2
	Group By PERMIT_NO
 ) c
  ON tr.ActivityNo = c.PERMIT_NO
WHERE tr.tGroup = 'PERMIT'


Select tr.AttachmentTotal,tr.ConditionsTotal,tr.NotesTotal,tr.RestrictionsTotal,tr.FeesDue,tr.RestrictionsSummary,tr.Loc_RecordID,tr.Record_Type,tr.Record_Status,tr.Site_Addr,vr.*
From tvw_GblReviewsE3 vr
JOIN #Results tr
  ON vr.RecordID = tr.RecordID
  

--Select tr.AttachmentTotal,tr.NotesTotal,tr.RestrictionsTotal,tr.FeesDue,tr.RestrictionsSummary,tr.Loc_RecordID,r.*
--From tvw_GblReviewsE3 r
--JOIN #Results tr
--  ON r.RecordID = tr.RecordID

















GO
