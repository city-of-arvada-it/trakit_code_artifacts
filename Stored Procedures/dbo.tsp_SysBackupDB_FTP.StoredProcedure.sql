USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_SysBackupDB_FTP]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_SysBackupDB_FTP]
GO
/****** Object:  StoredProcedure [dbo].[tsp_SysBackupDB_FTP]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  					  
					  					  					  					  
/*
tsp_SysBackupDB_FTP 
@BackupDB='DEV',
@BackupDBFileName='DEV_NightlyBackup.bak',
@BackupDirectory='C:\SQL Backups\',
@FTPSite='cloud.crw.com',
@FTPScriptName='ftpscript'


tsp_SysBackupDB_FTP 
@BackupDB='CRW_DEMO',
@BackupDBFileName='CRW_DEMO_NightlyBackup.bak',
@BackupDirectory='C:\SQL Backups\',
@FTPSite='development.crw.com',
@FTPScriptName='ftpscript'

tsp_SysBackupDB_FTP 
@BackupDB='CRW',
@BackupDBFileName='CRW_NightlyBackup.bak',
@BackupDirectory='C:\SQL Backups\',
@FTPSite='development.crw.com',
@FTPScriptName='ftpscript'

*/


CREATE Procedure [dbo].[tsp_SysBackupDB_FTP](
@BackupDB Varchar(100),
@BackupDBFileName Varchar(300),
@BackupDirectory Varchar(300),
@FTPSite Varchar(300),
@FTPScriptName Varchar(100),
@ToggleXP_CMDShell INT = 1
)

As


Declare @sql varchar(8000)

Declare @FullPathAndFileName Varchar(1000)
Set @FullPathAndFileName = @BackupDirectory + @BackupDBFileName
Declare @FullZipPathAndFileName Varchar(1000)
Set @FullZipPathAndFileName = @BackupDirectory + REPLACE(@BackupDBFileName,'.bak','.zip')


Declare @BackupName Varchar(300)
Set @BackupName = @BackupDB + '-Backup'

Declare @FTPScriptBatchFile Varchar(100)
Declare @FTPScriptTextFile Varchar(100)
Set @FTPScriptBatchFile = '"' + @BackupDirectory + @FTPScriptName + '.bat"'
Set @FTPScriptTextFile = '"' + @BackupDirectory + @FTPScriptName + '.txt"'

If @ToggleXP_CMDShell Is Null Set @ToggleXP_CMDShell = 1

--backup db
--BACKUP DATABASE [CRW_DEMO] TO  DISK = N'C:\SQL Backups\CRW_DEMO_NightlyBackup.bak' WITH NOFORMAT, INIT,  NAME = N'CRW_DEMO-Full Database Backup', SKIP, NOREWIND, NOUNLOAD,  STATS = 10
BACKUP DATABASE @BackupDB TO  DISK = @FullPathAndFileName WITH NOFORMAT, INIT,  NAME = @BackupName, SKIP, NOREWIND, NOUNLOAD,  STATS = 10


--Create ftpscript.txt and upload to ftp site

--ftp -v -i -s:c:\ftpscript.txt
IF EXISTS (SELECT * FROM tempdb.sys.objects WHERE object_id = OBJECT_ID(N'tempdb..ftpscript') AND type in (N'U'))
	Drop Table tempdb..ftpscript
create table tempdb..ftpscript(linetext varchar(300))

insert into tempdb..ftpscript(linetext)
values('ftp -v -i -s:' + @FTPScriptTextFile)


If @ToggleXP_CMDShell = 1
 Begin
	--Enable xp_cmdshell
	Set @SQL = '
	EXEC sp_configure ''show advanced options'', 1
	RECONFIGURE
	EXEC sp_configure ''xp_cmdshell'', 1
	RECONFIGURE
	'
	Print(@SQL)
	Exec(@SQL)
 End
 
--set @sql = 'zip "' + @FullPathAndFileName + '" "' + @FullZipPathAndFileName + '"'
--print(@sql)
--exec master..xp_cmdshell @sql

 
select @sql = 'bcp "select * from tempdb..ftpscript" queryout ' + @FTPScriptBatchFile + ' -c -t, -T -S'
+ @@servername
print(@sql)
exec master..xp_cmdshell @sql


If @ToggleXP_CMDShell = 1
 Begin
	-- Disable xp_cmdshell
	Set @SQL = '
	EXEC sp_configure ''xp_cmdshell'', 0
	RECONFIGURE
	EXEC sp_configure ''show advanced options'', 0
	RECONFIGURE
	'
	Print(@SQL)
	Exec(@SQL)
 End

IF EXISTS (SELECT * FROM tempdb.sys.objects WHERE object_id = OBJECT_ID(N'tempdb..ftpscript') AND type in (N'U'))
	Drop Table tempdb..ftpscript
create table tempdb..ftpscript(linetext varchar(300))


insert into tempdb..ftpscript(linetext)
values('open ' + @FTPSite)
insert into tempdb..ftpscript(linetext)
values('trakitftpuser')
insert into tempdb..ftpscript(linetext)
values('password4ftp')
insert into tempdb..ftpscript(linetext)
values('put "' + @FullPathAndFileName + '"')
insert into tempdb..ftpscript(linetext)
values('dir')
insert into tempdb..ftpscript(linetext)
values('disconnect')
insert into tempdb..ftpscript(linetext)
values('bye')


If @ToggleXP_CMDShell = 1
 Begin
	--Enable xp_cmdshell
	Set @SQL = '
	EXEC sp_configure ''show advanced options'', 1
	RECONFIGURE
	EXEC sp_configure ''xp_cmdshell'', 1
	RECONFIGURE
	'
	Print(@SQL)
	Exec(@SQL)
 End
 
 
select @sql = 'bcp "select * from tempdb..ftpscript" queryout ' + @FTPScriptTextFile + ' -c -t, -T -S'
+ @@servername
print(@sql)
exec master..xp_cmdshell @sql


IF EXISTS (SELECT * FROM tempdb.sys.objects WHERE object_id = OBJECT_ID(N'tempdb..ftpscript') AND type in (N'U'))
	Drop Table tempdb..ftpscript


--check results of running ftpscript
CREATE TABLE #Tmp
(
fileattr nvarchar(255)
)

INSERT INTO #Tmp(fileattr)
exec master..xp_cmdshell @FTPScriptBatchFile 

SELECT * FROM #Tmp


If @ToggleXP_CMDShell = 1
 Begin
	-- Disable xp_cmdshell
	Set @SQL = '
	EXEC sp_configure ''xp_cmdshell'', 0
	RECONFIGURE
	EXEC sp_configure ''show advanced options'', 0
	RECONFIGURE
	'
	Print(@SQL)
	Exec(@SQL)
 End
 





















GO
