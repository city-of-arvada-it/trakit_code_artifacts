USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_LicenseRenew_Custom_Chronology]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_LicenseRenew_Custom_Chronology]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_LicenseRenew_Custom_Chronology]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  
					  -- =============================================
-- Author:		<Micah Neveu>
-- create date: <3/4/2015>
-- Description:	<Procedure to add chronology items beyond the GLR standard>
-- =============================================
CREATE PROCEDURE [dbo].[tsp_T9_LicenseRenew_Custom_Chronology]
	-- Add the parameters for the stored procedure here
	@ACTION_BY AS VARCHAR(50),
	@ACTION_TYPE AS VARCHAR(255),
	@ACTION_DESCRIPTION AS VARCHAR(2000),
	@RECORDPREFIX AS VARCHAR(4)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT LICENSE_NO,RECORDID = @RECORDPREFIX + right(('00' + str(DATEPART(yy, GETDATE()))),1)
 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)
 + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)
 + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)
 + right(('000000'+ CONVERT(varchar,ROW_NUMBER() OVER (ORDER BY (SELECT NULL)))),6),
 ACTION_BY = @ACTION_BY, ACTION_DATE = GETDATE(), ACTION_DESCRIPTION = @ACTION_DESCRIPTION, 
	ACTION_TYPE = @ACTION_TYPE, COMPLETED_DATE = GETDATE() INTO #ActiveLicenses FROM LicenseRenew_License_List



ALTER TABLE #ActiveLicenses ADD BUS_LIC_NO VARCHAR(15)
ALTER TABLE #ActiveLicenses ADD LOCKID VARCHAR(30)

-- insert these into the actions if it doesn't exist


;MERGE LICENSE2_ACTIONS AS T
USING #ActiveLicenses AS S
	ON s.LICENSE_NO = t.LICENSE_NO
		AND s.ACTION_TYPE = t.ACTION_TYPE
		AND datepart(year, s.ACTION_DATE) = datepart(year, t.ACTION_DATE)
WHEN NOT MATCHED BY target
	THEN
		INSERT (
			ACTION_BY,ACTION_DATE,ACTION_DESCRIPTION,ACTION_TYPE,COMPLETED_DATE,RECORDID,LICENSE_NO
			)
		VALUES (
			s.ACTION_BY,s.ACTION_DATE,s.ACTION_DESCRIPTION,s.ACTION_TYPE,s.COMPLETED_DATE
			,s.RECORDID,s.LICENSE_NO
			);

INSERT INTO LicenseRenew_EventLog (SourceMode,EventText,USERID) VALUES ('[custom action]', 'Custom chronology item added.','GLR');

-- finally add to prmry_notes if they do not exist
MERGE  Prmry_Notes  AS T
USING #ActiveLicenses AS S
	ON t.ActivityNo = s.LICENSE_NO
		AND t.SubGroupRecordID =s.recordid  
		and t.notes=s.ACTION_DESCRIPTION
		
WHEN NOT MATCHED BY target
	THEN
		INSERT 
 (ActivityGroup, ActivityNo, SubGroup, SubGroupRecordID, UserID,DateEntered,Notes)
values
('LICENSE2', s.license_no, 'ACTION', s.RECORDID, 'AUTO', GETDATE(), s.ACTION_DESCRIPTION
);
   
END





GO
