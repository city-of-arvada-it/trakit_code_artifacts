USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_SSRS_Multi_Receipt_Invoice]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_SSRS_Multi_Receipt_Invoice]
GO
/****** Object:  StoredProcedure [dbo].[tsp_SSRS_Multi_Receipt_Invoice]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[tsp_SSRS_Multi_Receipt_Invoice]
	@INVOICE_NO VARCHAR(300),
	@USERID VARCHAR(100) = NULL,
	@VAL1 VARCHAR(100) = NULL,
	@REPRINT VARCHAR(100)
	
AS
BEGIN

create table #table1
	(RECORD_NO varchar(2000),
	RECORD_TYPE varchar(2000),
	TYPE varchar(2000),
	CODE varchar(2000),
	ITEM varchar(2000),
	PARENT_DESCRIPTION varchar(2000),
	DESCRIPTION varchar(2000),
	ACCOUNT varchar(2000),
	QUANTITY float,
	PAID_AMOUNT float,
	PAID_DATE date,
	RECEIPT_NO varchar(2000),
	CHECK_NO varchar(2000),
	PAY_METHOD varchar(2000),
	PAID_BY varchar(2000),
	COLLECTED_BY varchar(2000),
	FEETYPE varchar(2000),
	DEPOSITID varchar(2000),
	SITE_ADDR varchar(2000),
	SITE_APN varchar (2000),
	STATUS varchar(2000),
	INVOICE_AMT float,
	PAID_ADDR1 varchar(2000),
	PAID_ADDR2 varchar(2000),
	PAID_CITY varchar(2000),
	PAID_STATE varchar(200),
	PAID_ZIP varchar(2000),
	INVOICE_DATE datetime,
	FLAG varchar(1)) ;

create index #table1_ix1 on #table1 (RECORD_NO);

--All-------------------------------------------------------------------------------------------------------------------------------------------

BEGIN
	insert into #table1 
		(RECORD_NO
		,RECORD_TYPE
		,CODE
		,ITEM
		,DESCRIPTION
		,PARENT_DESCRIPTION
		,ACCOUNT
		,QUANTITY
		,PAID_AMOUNT
		,PAID_DATE
		,RECEIPT_NO
		,CHECK_NO
		,SITE_ADDR
		,SITE_APN
		,PAY_METHOD
		,COLLECTED_BY
		,FEETYPE
		,DEPOSITID
		,PAID_BY
		,PAID_ADDR1
		,PAID_ADDR2
		,PAID_CITY
		,PAID_STATE
		,PAID_ZIP
		,INVOICE_AMT
		,TYPE
		,INVOICE_DATE)
	select Fees.ACTIVITYID
		,CASE WHEN Fees.ActivityTypeID = 1
			THEN 'PermitTRAK'
			WHEN Fees.ActivityTypeID = 2
			THEN 'ProjectTRAK'
			WHEN Fees.ActivityTypeID = 3
			THEN 'LicenseTRAK'
			WHEN Fees.ActivityTypeID = 4
			THEN 'CodeTRAK'
			WHEN Fees.ActivityTypeID = 6
			THEN 'AECTRAK'
			END
		,Fees.CODE
		,NULL
		,Fees.DESCRIPTION
		,NULL
		,Fees.ACCOUNT
		,Fees.QUANTITY
		,Fees.PAID_AMOUNT
		,Fees.PAID_DATE
		,Fees.RECEIPT_NO
		,Fees.CHECK_NO
		,CASE WHEN Fees.ActivityTypeID = 1
			THEN Permit_Main.SITE_ADDR
			WHEN Fees.ActivityTypeID = 2
			THEN Project_Main.SITE_ADDR
			WHEN Fees.ActivityTypeID = 3
			THEN License2_Main.SITE_ADDR
			WHEN Fees.ActivityTypeID = 4
			THEN Case_Main.SITE_ADDR
			WHEN Fees.ActivityTypeID = 6
			THEN AEC_Main.ADDRESS1
			END
		,CASE WHEN Fees.ActivityTypeID = 1
			THEN Permit_Main.SITE_APN
			WHEN Fees.ActivityTypeID = 2
			THEN Project_Main.SITE_APN
			WHEN Fees.ActivityTypeID = 3
			THEN License2_Main.SITE_APN
			WHEN Fees.ActivityTypeID = 4
			THEN Case_Main.SITE_APN
			WHEN Fees.ActivityTypeID = 6
			THEN NULL
			END
		,Fees.PAY_METHOD
		,Fees.COLLECTED_BY
		,Fees.FEETYPE
		,Fees.DEPOSITID
		,INVOICECONTACT
		,INVOICEADDRESS 
		,CASE WHEN INVOICEADDRESS2 = '' THEN NULL ELSE InvoiceAddress2 END
		,INVOICECITY
		,INVOICESTATE
		,CASE 
		WHEN INVOICEZIP NOT LIKE '%[^a-Z0-9]%' AND LEN(INVOICEZIP) > 5
        THEN SUBSTRING(INVOICEZIP,1,5) + '-' + SUBSTRING(INVOICEZIP,6,4)
        ELSE INVOICEZIP
        END
		,Fees.AMOUNT
		,CASE WHEN Fees.ActivityTypeID = 1
			THEN PERMITTYPE
			WHEN Fees.ActivityTypeID = 2
			THEN PROJECTTYPE
			WHEN Fees.ActivityTypeID = 3
			THEN LICENSE_TYPE
			WHEN Fees.ActivityTypeID = 4
			THEN CASETYPE
			WHEN Fees.ActivityTypeID = 6
			THEN AECTYPE
			END
		,Fees.INVOICE_DATE
	from Fees inner join Invoice_Main
	on Fees.INVOICE_NO = Invoice_Main.INVOICENUMBER
	left join Permit_Main 
	on Permit_Main.PERMIT_NO = Fees.ACTIVITYID
	and Fees.ActivityTypeID = Permit_Main.ActivityTypeID
	left join Project_Main
	on Project_Main.PROJECT_NO = Fees.ACTIVITYID
	and Fees.ActivityTypeID = Project_Main.ActivityTypeID
	left join License2_Main
	on License2_Main.LICENSE_NO = Fees.ACTIVITYID
	and Fees.ActivityTypeID = License2_Main.ActivityTypeID
	left join Case_Main
	on Case_Main.CASE_NO = Fees.ACTIVITYID
	and Fees.ActivityTypeID = Case_Main.ActivityTypeID
	left join AEC_Main
	on AEC_Main.ST_LIC_NO = Fees.ACTIVITYID
	and Fees.ActivityTypeID = AEC_Main.ActivityTypeID
	where DETAIL = 0 
	and INVOICE_NO = @INVOICE_NO 
	and AMOUNT > 0 
	and Fees.ActivityTypeID in (1,2,3,4,6)
	UNION ALL
	select SubFees.ACTIVITYID
		,CASE WHEN Fees.ActivityTypeID = 1
			THEN 'PermitTRAK'
			WHEN Fees.ActivityTypeID = 2
			THEN 'ProjectTRAK'
			WHEN Fees.ActivityTypeID = 3
			THEN 'LicenseTRAK'
			WHEN Fees.ActivityTypeID = 4
			THEN 'CodeTRAK'
			WHEN Fees.ActivityTypeID = 6
			THEN 'AECTRAK'
			END
		,SubFees.CODE
		,SubFees.ITEM
		,Fees.DESCRIPTION
		,SubFees.DESCRIPTION
		,SubFees.ACCOUNT
		,SubFees.QUANTITY
		,CASE WHEN SubFees.PAID = 1
		THEN SubFees.AMOUNT
		END
		,Fees.PAID_DATE 
		,Fees.RECEIPT_NO 
		,Fees.CHECK_NO 
		,CASE WHEN Fees.ActivityTypeID = 1
			THEN Permit_Main.SITE_ADDR
			WHEN Fees.ActivityTypeID = 2
			THEN Project_Main.SITE_ADDR
			WHEN Fees.ActivityTypeID = 3
			THEN License2_Main.SITE_ADDR
			WHEN Fees.ActivityTypeID = 4
			THEN Case_Main.SITE_ADDR
			WHEN Fees.ActivityTypeID = 6
			THEN AEC_Main.ADDRESS1
			END
		,CASE WHEN Fees.ActivityTypeID = 1
			THEN Permit_Main.SITE_APN
			WHEN Fees.ActivityTypeID = 2
			THEN Project_Main.SITE_APN
			WHEN Fees.ActivityTypeID = 3
			THEN License2_Main.SITE_APN
			WHEN Fees.ActivityTypeID = 4
			THEN Case_Main.SITE_APN
			WHEN Fees.ActivityTypeID = 6
			THEN NULL
			END
		,Fees.PAY_METHOD
		,Fees.COLLECTED_BY
		,SUBFEETYPE
		,Fees.DEPOSITID
		,INVOICECONTACT
		,INVOICEADDRESS 
		,CASE WHEN INVOICEADDRESS2 = '' THEN NULL ELSE InvoiceAddress2 END
		,INVOICECITY
		,INVOICESTATE
		,CASE 
		WHEN INVOICEZIP NOT LIKE '%[^a-Z0-9]%' AND LEN(INVOICEZIP) > 5
        THEN SUBSTRING(INVOICEZIP,1,5) + '-' + SUBSTRING(INVOICEZIP,6,4)
        ELSE INVOICEZIP
        END
		,SubFees.AMOUNT
		,CASE WHEN Fees.ActivityTypeID = 1
			THEN PERMITTYPE
			WHEN Fees.ActivityTypeID = 2
			THEN PROJECTTYPE
			WHEN Fees.ActivityTypeID = 3
			THEN LICENSE_TYPE
			WHEN Fees.ActivityTypeID = 4
			THEN CASETYPE
			WHEN Fees.ActivityTypeID = 6
			THEN AECTYPE
			END
		,Fees.INVOICE_DATE
	from SubFees inner join Fees 
	on Fees.RECORDID = SubFees.PARENTID
	inner join Invoice_Main
	on Fees.INVOICE_NO = Invoice_Main.INVOICENUMBER
	left join Permit_Main 
	on Permit_Main.PERMIT_NO = Fees.ACTIVITYID
	and Fees.ActivityTypeID = Permit_Main.ActivityTypeID
	left join Project_Main
	on Project_Main.PROJECT_NO = Fees.ACTIVITYID
	and Fees.ActivityTypeID = Project_Main.ActivityTypeID
	left join License2_Main
	on License2_Main.LICENSE_NO = Fees.ACTIVITYID
	and Fees.ActivityTypeID = License2_Main.ActivityTypeID
	left join Case_Main
	on Case_Main.CASE_NO = Fees.ACTIVITYID
	and Fees.ActivityTypeID = Case_Main.ActivityTypeID
	left join AEC_Main
	on AEC_Main.ST_LIC_NO = Fees.ACTIVITYID
	and Fees.ActivityTypeID = AEC_Main.ActivityTypeID 
	where DETAIL = 1 
	and Fees.INVOICE_NO = @INVOICE_NO
	and SubFees.AMOUNT > 0 
	and Fees.ActivityTypeID in (1,2,3,4,6)
	and SUBFEETYPE <> 'REFUND'
UNION ALL
	select SubFees.ACTIVITYID
		,CASE WHEN Fees.ActivityTypeID = 1
			THEN 'PermitTRAK'
			WHEN Fees.ActivityTypeID = 2
			THEN 'ProjectTRAK'
			WHEN Fees.ActivityTypeID = 3
			THEN 'LicenseTRAK'
			WHEN Fees.ActivityTypeID = 4
			THEN 'CodeTRAK'
			WHEN Fees.ActivityTypeID = 6
			THEN 'AECTRAK'
			END
		,SubFees.CODE
		,SubFees.ITEM
		,Fees.DESCRIPTION
		,SubFees.DESCRIPTION
		,SubFees.ACCOUNT
		,SubFees.QUANTITY
		,CASE WHEN SubFees.PAID = 1
		THEN SubFees.AMOUNT
		END
		,CONVERT(date, PaymentTransaction.Date) 
		,PaymentTransaction.ReceiptNumber 
		,PaymentTransactionFee.PaymentMethodDetail 
		,CASE WHEN Fees.ActivityTypeID = 1
			THEN Permit_Main.SITE_ADDR
			WHEN Fees.ActivityTypeID = 2
			THEN Project_Main.SITE_ADDR
			WHEN Fees.ActivityTypeID = 3
			THEN License2_Main.SITE_ADDR
			WHEN Fees.ActivityTypeID = 4
			THEN Case_Main.SITE_ADDR
			WHEN Fees.ActivityTypeID = 6
			THEN AEC_Main.ADDRESS1
			END
		,CASE WHEN Fees.ActivityTypeID = 1
			THEN Permit_Main.SITE_APN
			WHEN Fees.ActivityTypeID = 2
			THEN Project_Main.SITE_APN
			WHEN Fees.ActivityTypeID = 3
			THEN License2_Main.SITE_APN
			WHEN Fees.ActivityTypeID = 4
			THEN Case_Main.SITE_APN
			WHEN Fees.ActivityTypeID = 6
			THEN NULL
			END
		,PaymentTransactionFee.PaymentMethod 
		,SubFees.ASSESSED_BY 
		,SUBFEETYPE
		,Fees.DEPOSITID
		,INVOICECONTACT
		,INVOICEADDRESS 
		,CASE WHEN INVOICEADDRESS2 = '' THEN NULL ELSE InvoiceAddress2 END
		,INVOICECITY
		,INVOICESTATE
		,CASE 
		WHEN INVOICEZIP NOT LIKE '%[^a-Z0-9]%' AND LEN(INVOICEZIP) > 5
        THEN SUBSTRING(INVOICEZIP,1,5) + '-' + SUBSTRING(INVOICEZIP,6,4)
        ELSE INVOICEZIP
        END
		,SubFees.AMOUNT
		,CASE WHEN Fees.ActivityTypeID = 1
			THEN PERMITTYPE
			WHEN Fees.ActivityTypeID = 2
			THEN PROJECTTYPE
			WHEN Fees.ActivityTypeID = 3
			THEN LICENSE_TYPE
			WHEN Fees.ActivityTypeID = 4
			THEN CASETYPE
			WHEN Fees.ActivityTypeID = 6
			THEN AECTYPE
			END
		,Fees.INVOICE_DATE
	from SubFees inner join Fees 
	on Fees.RECORDID = SubFees.PARENTID
	inner join  PaymentTransactionFee
	on SubFees.ActivityID = PaymentTransactionFee.ActivityId
	and SubFees.PARENTID = PaymentTransactionFee.RecordId
	inner join PaymentTransactionSubfee
	on SubFees.RECORDID = PaymentTransactionSubFee.RECORDID
	and PaymentTransactionFee.ID = PaymentTransactionSubFee.TRANSACTIONFEEID
	inner join PaymentTransaction
	on PaymentTransaction.ID = PaymentTransactionFee.TransactionId
	and PaymentTransaction.TypeId = 2
	and PaymentTransaction.StatusId in (4,6)
	inner join Invoice_Main
	on Fees.INVOICE_NO = Invoice_Main.INVOICENUMBER
	left join Permit_Main 
	on Permit_Main.PERMIT_NO = Fees.ACTIVITYID
	and Fees.ActivityTypeID = Permit_Main.ActivityTypeID
	left join Project_Main
	on Project_Main.PROJECT_NO = Fees.ACTIVITYID
	and Fees.ActivityTypeID = Project_Main.ActivityTypeID
	left join License2_Main
	on License2_Main.LICENSE_NO = Fees.ACTIVITYID
	and Fees.ActivityTypeID = License2_Main.ActivityTypeID
	left join Case_Main
	on Case_Main.CASE_NO = Fees.ACTIVITYID
	and Fees.ActivityTypeID = Case_Main.ActivityTypeID
	left join AEC_Main
	on AEC_Main.ST_LIC_NO = Fees.ACTIVITYID
	and Fees.ActivityTypeID = AEC_Main.ActivityTypeID 
	where Fees.DETAIL = 1 
	and Fees.INVOICE_NO = @INVOICE_NO
	and SubFees.AMOUNT > 0 
	and Fees.ActivityTypeID in (1,2,3,4,6)
	and SUBFEETYPE = 'REFUND';
END

--Offset any Deposit Reductions
insert into #table1
	(RECORD_NO
	,RECORD_TYPE
	,ACCOUNT
	,CODE
	,PAID_AMOUNT
	,PAID_DATE
	,RECEIPT_NO
	,CHECK_NO
	,PAY_METHOD
	,FEETYPE
	,DEPOSITID
	,COLLECTED_BY)
select
	  RECORD_NO
	,RECORD_TYPE
	,(select ACCOUNT from Fees where Fees.RECORDID = #table1.DEPOSITID)
	,CODE
	,(PAID_AMOUNT*-1)
	,PAID_DATE
	,'OFFSET'
	,CHECK_NO
	,'OFFSET'
	,'OFFSET'
	,DEPOSITID
	,COLLECTED_BY
from #table1 
	where DEPOSITID in 
		(select RECORDID from Fees 
			where ACTIVITYID = #table1.RECORD_NO 
			and FEETYPE = 'DEPOSIT'
			and Fees.ActivityTypeID in (1,2,3,4,6))
	and FEETYPE <> 'REFUND' ;
	
--Update Credits and Refunds
update #table1 set #table1.PAID_AMOUNT = #table1.PAID_AMOUNT * -1 where (CODE = 'REFUND' or FEETYPE = 'REFUND') ;
update #table1 set #table1.PAID_AMOUNT = #table1.PAID_AMOUNT * -1 where (CODE = 'CREDIT' or ITEM = 'CREDIT') ;

update #table1 set #table1.INVOICE_AMT = #table1.INVOICE_AMT * -1 where (CODE = 'REFUND' or FEETYPE = 'REFUND') ;
update #table1 set #table1.INVOICE_AMT = #table1.INVOICE_AMT * -1 where (CODE = 'CREDIT' or ITEM = 'CREDIT') ;


--THIS SELECT NEEDS TO BE HERE FOR SSRS TO GET DATA------------------------------------------
select * from #table1 ;

END
GO
