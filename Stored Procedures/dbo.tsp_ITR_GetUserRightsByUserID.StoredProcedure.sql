USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ITR_GetUserRightsByUserID]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_ITR_GetUserRightsByUserID]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ITR_GetUserRightsByUserID]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  -- =====================================================================  

-- Author:		Rance Richardson
-- Create date: 6/12/2013
-- Description:	
-- Change History:
--	Date		Updated by:		Description of Change:
--	
--	mdm - 11/5/13 - added revision history	
-- =====================================================================  
CREATE PROCEDURE [dbo].[tsp_ITR_GetUserRightsByUserID] 
	@UserID varchar(30) 
	
AS
BEGIN
	SET NOCOUNT ON;

	SELECT UserAccess, '' AS 'TrakitModule',
		CASE WHEN UserAccess = 'ADMIN' THEN 'ADMIN' ELSE UserRights END AS 'UserRights',
		'' AS AccessDescription 
	INTO #temp1
	FROM Prmry_Users
	WHERE UserAccess IN ('ADMIN', 'USER')
		AND UserID = @UserID 
		
	DECLARE @UserRightsString as varchar(255)
	DECLARE @NumOfRights int
	DECLARE @UserAccess varchar(10)
	DECLARE @LoopsCounter int

	SET @LoopsCounter = 0
	SET @UserAccess = (SELECT UserAccess FROM #temp1)
	SET @NumOfRights = (SELECT (LEN(UserRights)/2) FROM #temp1)   
	SET @UserRightsString = (SELECT RTRIM(LTRIM(UserRights)) FROM #temp1)

	CREATE TABLE #UR (
		UserAccess varchar(10),
		TrakitModule varchar(20),
		AccessDescription varchar(255)
	)

	WHILE @LoopsCounter < @NumOfRights
	BEGIN
		INSERT INTO #UR (UserAccess,TrakitModule,AccessDescription)
		SELECT
		t1.UserAccess,
		t2.TrakitModule, 
		t2.AccessDescription
		FROM #temp1 t1 LEFT JOIN 
			(SELECT @UserAccess as UserAccess,'' as UserRights, TrakitModule, AccessDescription FROM PRMRY_ACCESSRIGHTS
				WHERE (Convert(binary(5),AccessCode) = Convert(binary(5),SUBSTRING(@UserRightsString, (@LoopsCounter * 2) +1, 2)))) 
			AS t2 ON t1.UserAccess = t2.UserAccess
		
		IF @UserAccess = 'ADMIN' SET @LoopsCounter = @NumOfRights
		ELSE SET @LoopsCounter = @LoopsCounter + 1
	END

	SELECT * FROM #UR

	DROP TABLE #temp1
	DROP TABLE #UR
	
	
	
END





GO
