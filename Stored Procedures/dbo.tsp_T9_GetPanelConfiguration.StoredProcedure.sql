USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_GetPanelConfiguration]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_GetPanelConfiguration]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_GetPanelConfiguration]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  CREATE PROCEDURE [dbo].[tsp_T9_GetPanelConfiguration] 
	@GroupName VARCHAR(12)
AS

SELECT [Control],
	Title,
	Collapsed,
	Caption,
	[Type],
	Height,
	LoadMethod,
	Script
FROM PRMRY_T9_ADMIN_MANAGEPANELS
WHERE GroupName = @GroupName
	AND Active = 1
ORDER BY OrderId




GO
