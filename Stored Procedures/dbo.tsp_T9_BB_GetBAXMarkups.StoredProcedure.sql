USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_BB_GetBAXMarkups]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_BB_GetBAXMarkups]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_BB_GetBAXMarkups]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  
-- =============================================
-- Author:		<Micah Neveu>
-- Create date: <4/7/2015>
-- Description:	<Simple read, returns the markups for the group and attachment id>
-- =============================================
CREATE PROCEDURE [dbo].[tsp_T9_BB_GetBAXMarkups]
	@GroupName VARCHAR(30),
	@AttachmentRecordID VARCHAR(30)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT * FROM Prmry_BAX_Markups WHERE GroupName = @GroupName AND AttachmentRecordID = @AttachmentRecordID
	
END


--***********************************************************************************************************



/****** Object:  StoredProcedure [dbo].[tsp_T9_BB_GetBAXMarkupsByVersion]    Script Date: 07/20/2015 13:38:33 ******/
SET ANSI_NULLS ON


GO
