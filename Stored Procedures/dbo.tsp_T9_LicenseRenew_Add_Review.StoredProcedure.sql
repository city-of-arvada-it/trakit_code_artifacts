USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_LicenseRenew_Add_Review]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_LicenseRenew_Add_Review]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_LicenseRenew_Add_Review]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  -- =============================================
-- Author:		<Micah Neveu>
-- Create date: <3/5/2015>
-- Description:	<adds the review type to ALL licenses in LicenseRenew_License_List>
-- =============================================
CREATE PROCEDURE [dbo].[tsp_T9_LicenseRenew_Add_Review]
	@REVIEWTYPE varchar(60),
	@REVIEWGROUP varchar(60),
	@RECORDPREFIX varchar(200)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @CONTACT varchar(200)
	SET @CONTACT = (SELECT TOP 1 Field02 FROM Prmry_ReviewControl WHERE GroupName = 'License2' AND Category = 'review_type')
	INSERT INTO LICENSE2_REVIEWS (RECORDID, CONTACT, REMARKS, REVIEWGROUP, REVIEWTYPE, LICENSE_NO)
		SELECT @RECORDPREFIX + right(('00' + str(DATEPART(yy, GETDATE()))),2) + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2) + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2) + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2) + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2) + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2) + right(('000000'+ CONVERT(varchar,ROW_NUMBER() OVER (ORDER BY (SELECT NULL))+111)),6)
			AS recordid, @CONTACT, 'Generated by GLR', @REVIEWGROUP, @REVIEWTYPE, tbl.License_no FROM (select License_no from LicenseRenew_License_List) as tbl
			
END



GO
