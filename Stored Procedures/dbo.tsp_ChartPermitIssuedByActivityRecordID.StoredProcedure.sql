USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ChartPermitIssuedByActivityRecordID]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_ChartPermitIssuedByActivityRecordID]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ChartPermitIssuedByActivityRecordID]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  					  
					  					  					  

CREATE Procedure [dbo].[tsp_ChartPermitIssuedByActivityRecordID](
	@RecordID1 varchar(8000) = Null,
	@RecordID2 varchar(8000) = Null,
	@RecordID3 varchar(8000) = Null,
	@RecordID4 varchar(8000) = Null
)

As

Declare @RecID table (RecordID varchar(2000)) 

insert @RecID
select r.recordid
from dbo.tfn_TblRecID(@recordid1,@recordid2,@recordid3,@recordid4) r

set nocount on

SELECT ISSUED, COUNT(ISSUED) AS cnt FROM PERMIT_MAIN where RECORDID in (select RECORDID from @recID)  GROUP BY ISSUED















GO
