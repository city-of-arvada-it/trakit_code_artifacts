USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_UpdateProjectNo]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[usp_Arvada_UpdateProjectNo]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_UpdateProjectNo]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_Arvada_UpdateProjectNo]
	@CurrentProjectNo varchar(50),
	@NewProjectNo varchar(50)

as

if not exists(
				select 1
				from dbo.project_main
				where PROJECT_NO = @CurrentProjectNo
				)
begin
	
	raiserror('Current Project Not found',16,1)
	return
end

if exists(
				select 1
				from dbo.project_main
				where PROJECT_NO = @NewProjectNo
				)
begin
	
	raiserror('New Project found',16,1)
	return
end

declare @currentprojectnorecid varchar(50)
select @currentprojectnorecid = recordid
from project_main
where PROJECT_NO = @CurrentProjectNo


declare @newprojectnorecid varchar(50)
select @newprojectnorecid = recordid
from project_main
where PROJECT_NO = @NewProjectNo

update a
set project_no = @NewProjectNo
from Project_UDF a
where project_no = @CurrentProjectNo

update a
set ActivityID = @NewProjectNo
from Attachments a
where activityid = @CurrentProjectNo
and a.ActivityTypeID = 2

update a
set activityid = @NewProjectNo
from  People a
where activityid = @CurrentProjectNo
and a.ActivityTypeID = 2

update a
set project_no = @NewProjectNo
from PROJECT_MAIN a
where project_no = @CurrentProjectNo

update a
set PRIMARY_KEY_VALUE = @NewProjectNo
from Prmry_AuditTrail a
where PRIMARY_KEY_VALUE = @CurrentProjectNo

update a
set activityid = @NewProjectNo
from  Actions a
where activityid = @CurrentProjectNo
and a.ActivityTypeID = 2

 
update a
set activityid = @NewProjectNo
from  Attachments  a
where activityid = @CurrentProjectNo
and a.ActivityTypeID = 2

update a
set project_no = @NewProjectNo
from Project_Bond_Document  a
where project_no = @CurrentProjectNo

update a
set ActivityID = @NewProjectNo
from  Bond_Inspections  a
where ActivityID = @CurrentProjectNo
and a.ActivityTypeID = 2 

 

update a
set ActivityID = @NewProjectNo
from Bonds  a
where ActivityID = @CurrentProjectNo
and a.ActivityTypeID = 2 

update t
set ActivityID = @NewProjectNo
FROM People as t 
join dbo.Activity as a 
	on a.ActivityID = t.ActivityID 
	and t.ActivityTypeID=a.ActivityTypeID
where 
	a.ActivityTypeID = 2 and BOND_RECORDID is not null
and t.ActivityID = @CurrentProjectNo

update a
set project_no = @NewProjectNo
from Project_Bonds_UDF  a
where project_no = @CurrentProjectNo


update a
set project_no = @NewProjectNo
from Project_Conditions  a
where project_no = @CurrentProjectNo

update a
set project_no = @NewProjectNo
from Conditions  a
where ActivityID = @CurrentProjectNo
and a.ActivityTypeID = 2 

update a
set project_no = @NewProjectNo
from Project_Conditions2_UDF  a
where project_no = @CurrentProjectNo

update a
set ActivityID = @NewProjectNo
from Fees a
where ActivityID = @CurrentProjectNo
and a.ActivityTypeID = 2 

update a
set project_no = @NewProjectNo
from PROJECT_FEES_AUDIT a
where project_no = @CurrentProjectNo

update a
set ActivityID = @NewProjectNo
from  INSPECTIONS a
where ActivityID = @CurrentProjectNo
and a.ActivityTypeID = 2 

update a
set project_no = @NewProjectNo
from PROJECT_INSPECTIONS_UDF a
where project_no = @CurrentProjectNo


update a
set project_no = @NewProjectNo
from PROJECT_MEMOS a
where project_no = @CurrentProjectNo

update a
set project_no = @NewProjectNo
from PROJECT_MULTI a
where project_no = @CurrentProjectNo

update a
set ActivityID = @NewProjectNo
from Parcels a
where ActivityID = @CurrentProjectNo
and a.ActivityTypeID = 2 


update a
set project_no = @NewProjectNo
from Project_ProfferBeneficiaries a
where project_no = @CurrentProjectNo

update a
set project_no = @NewProjectNo
from Project_Proffers a
where project_no = @CurrentProjectNo

update a
set project_no = @NewProjectNo
from Project_ProfferTransactionDetails a
where project_no = @CurrentProjectNo

update a
set project_no = @NewProjectNo
from Project_ProfferTransactions a
where project_no = @CurrentProjectNo

update a
set ActivityID = @NewProjectNo
from Reviews a
where ActivityID = @CurrentProjectNo
and a.ActivityTypeID = 2 

update a
set ActivityID = @NewProjectNo
from SubFees a
where ActivityID = @CurrentProjectNo
and a.ActivityTypeID = 2 

update a
set ActivityID = @NewProjectNo
from activity a
where ActivityID = @CurrentProjectNo
and a.ActivityTypeID = 2 

update a
set Activity_no = @NewProjectNo
 from PRMRY_PLANLOCATIONS  a
 where
	GROUPNAME = 'PROJECTS' 
	and ACTIVITY_NO = @CurrentProjectNo
GO
