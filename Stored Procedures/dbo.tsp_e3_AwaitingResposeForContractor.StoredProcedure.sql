USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_e3_AwaitingResposeForContractor]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_e3_AwaitingResposeForContractor]
GO
/****** Object:  StoredProcedure [dbo].[tsp_e3_AwaitingResposeForContractor]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

					  
-- =============================================
-- Author:		Victor Sinfuente
-- Create date: 10/10/2014
-- Description:	eNOtify
-- VS - 2/4/2016 - For invoice creation
-- =============================================

CREATE PROCEDURE [dbo].[tsp_e3_AwaitingResposeForContractor]
	@st_lic_no as varchar(300) = '',
	@getpermit as varchar(1) = '0',
	@getproject as varchar(1) = '0',
	@getlicense as varchar(1) = '0',
	@activityno as varchar(15) = '',
	@reviewrecordid as varchar(30) = ''
AS
BEGIN


--declare @st_lic_no as varchar(300) = '10'
--declare @getpermit as varchar(1) = '1'
--declare @getproject as varchar(1) = '1'
--declare @getlicense as varchar(1) = '1'

declare @sqlstring as nvarchar(max) = null
declare @etrakitcontractorusername as varchar(60) = ''
declare @activityfilter as varchar(500) = ''
declare @reviewrecordidfilter as varchar(500) = ''

select @etrakitcontractorusername = pref_value from etrakit3_preferences where  pref_name = 'LOGIN_CONTRACTOR_USERID'

IF @getpermit = '1'
BEGIN
	IF EXISTS (SELECT 1 
			   FROM INFORMATION_SCHEMA.TABLES 
			   WHERE TABLE_TYPE='BASE TABLE' 
			   AND TABLE_NAME='tmpPermitResponse') 
	   DROP TABLE tmpPermitResponse

	declare @permitAllowingResponse as varchar(max) = null

	select @permitAllowingResponse = isnull(pref_value, '') from Etrakit3_Preferences where pref_name = 'Perm_ReviewStatusAllowingResponse'

	if @activityno <> ''
	BEGIN
		SET @activityfilter = ' pm.permit_no = ''' +  @activityno + ''' and ' 
	END

	if @reviewrecordid <> ''
	BEGIN
		SET @reviewrecordidfilter = ' pr.recordid = ''' +  @reviewrecordid + ''' and ' 
	END 


	IF @permitAllowingResponse <> ''
	BEGIN
		set @sqlstring = 'SELECT  * INTO tmpPermitResponse FROM (select pm.recordid, ''PERMIT'' AS record_group,  pm.permit_no AS record_no, pm.SITE_ADDR as site_address, pm.PermitType as record_type, pr.REVIEWTYPE as review_type, ''RESPOND'' as respond, UPPER(pr.status) AS status, pm.loc_recordid as loc_recordid, pr.recordid as review_recordid from permit_main pm 
		inner join permit_reviews pr on ' + @reviewrecordidfilter + 'pr.PERMIT_NO = pm.PERMIT_NO and pr.STATUS in (' + @permitAllowingResponse + ')
		left join geo_ownership geo on geo.recordid = pm.loc_recordid 
		where ' + @activityfilter + ' (pm.EXPIRED is null or pm.EXPIRED > getdate()) and pm.permit_no in (select top 1 pp.permit_no from permit_people pp where pp.ID = ''C:' + @st_lic_no + ''' and pp.permit_no = pm.permit_no) and ''' + @etrakitcontractorusername + ''' not in (select top 1 USERID from prmry_notes where subgrouprecordid  = pr.recordid order by dateentered desc)
		) permits ORDER BY record_no'

		EXECUTE sp_executesql @sqlstring

		SELECT * FROM tmpPermitResponse
	END
END

IF @getproject = '1'
BEGIN
	IF EXISTS (SELECT 1 
			   FROM INFORMATION_SCHEMA.TABLES 
			   WHERE TABLE_TYPE='BASE TABLE' 
			   AND TABLE_NAME='tmpProjectResponse') 
	   DROP TABLE tmpProjectResponse

	declare @projectAllowingResponse as varchar(max) = null

	select @projectAllowingResponse = isnull(pref_value, '') from Etrakit3_Preferences where pref_name = 'Proj_ReviewStatusAllowingResponse'

	if @activityno <> ''
	BEGIN
		SET @activityfilter = ' pm.project_no = ''' +  @activityno + ''' and ' 
	END

	if @reviewrecordid <> ''
	BEGIN
		SET @reviewrecordidfilter = ' pr.recordid = ''' +  @reviewrecordid + ''' and ' 
	END 


	IF @projectAllowingResponse <> ''
	BEGIN
		set @sqlstring = 'SELECT  * INTO tmpProjectResponse FROM (select pm.recordid, ''PROJECT'' AS record_group,  pm.project_no AS record_no, pm.SITE_ADDR as site_address, pm.ProjectType as record_type, pr.REVIEWTYPE as review_type, ''RESPOND'' as respond, UPPER(pr.status) AS status, pm.loc_recordid as loc_recordid, pr.recordid as review_recordid from project_main pm 
		inner join project_reviews pr on ' + @reviewrecordidfilter + 'pr.project_no = pm.project_no and pr.STATUS in (' + @projectAllowingResponse + ')
		left join geo_ownership geo on geo.recordid = pm.loc_recordid 
		where ' + @activityfilter + '(pm.EXPIRED is null or pm.EXPIRED > getdate()) and pm.project_no in (select top 1 pp.project_no from project_people pp where pp.ID = ''C:' + @st_lic_no + ''' and pp.project_no = pm.project_no) and ''' + @etrakitcontractorusername + ''' not in (select top 1 USERID from prmry_notes where subgrouprecordid  = pr.recordid order by dateentered desc)
		) permits ORDER BY record_no'

		EXECUTE sp_executesql @sqlstring

		SELECT * FROM tmpProjectResponse
	END
END

IF @getlicense = '1'
BEGIN
	IF EXISTS (SELECT 1 
			   FROM INFORMATION_SCHEMA.TABLES 
			   WHERE TABLE_TYPE='BASE TABLE' 
			   AND TABLE_NAME='tmpLicenseResponse') 
	   DROP TABLE tmpLicenseResponse

	declare @licenseAllowingResponse as varchar(max) = null

	select @licenseAllowingResponse = isnull(pref_value, '') from Etrakit3_Preferences where pref_name = 'Lic2_ReviewStatusAllowingResponse'

	if @reviewrecordid <> ''
	BEGIN
		SET @reviewrecordidfilter = ' pr.recordid = ''' +  @reviewrecordid + ''' and ' 
	END 

	IF @licenseAllowingResponse <> ''
	BEGIN
		set @sqlstring = 'SELECT  * INTO tmpLicenseResponse FROM (select lm.recordid, ''LICENSE'' AS record_group,  lm.license_no AS record_no, lm.SITE_ADDR as site_address, lm.license_type as record_type, lr.REVIEWTYPE as review_type, ''RESPOND'' as respond, UPPER(lr.status) AS status, lm.loc_recordid as loc_recordid, lr.recordid as review_recordid from license2_main lm 
		inner join license2_reviews lr on ' + @reviewrecordidfilter + 'lr.license_no = lm.license_no and lr.STATUS in (' + @licenseAllowingResponse + ')
		left join geo_ownership geo on geo.recordid = lm.loc_recordid 
		where lm.license_no in (select top 1 lp.license_no from license2_people lp where lp.ID = ''C:' + @st_lic_no + ''' and lp.license_no = lm.license_no) and ''' + @etrakitcontractorusername + ''' not in (select top 1 USERID from prmry_notes where subgrouprecordid  = lr.recordid order by dateentered desc)
		) permits ORDER BY record_no'

		EXECUTE sp_executesql @sqlstring

		SELECT * FROM tmpLicenseResponse
	END
END

END


-- ============================================= 9.3.4.7 END
-- ============================================= 9.3.4.9 START
--commented tsp_T9_GblLinkActions in 9.3.4.7 build and adding updated SP from rafael
SET ANSI_NULLS ON
GO
