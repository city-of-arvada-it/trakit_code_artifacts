USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[ALP_AbortBatch]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[ALP_AbortBatch]
GO
/****** Object:  StoredProcedure [dbo].[ALP_AbortBatch]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================================================================
-- Author:		Erick Hampton
-- Create date: Oct 12, 2017
--
-- Oct 12, 2017:	* Delete licenses with a status of PENDING from batches with a status of SUBMITTED
--					* Delete batches with a status of SUBMITTED if all licenses have a status of PENDING
--
--			TODO:	* Delete licenses with a status of PENDING from batches with a status of QUEUED or WIP
--					* Delete batches with a status of DONE after archiving the license data changes
--					* Delete batches with a status of ERROR after archiving license data changes for COMPLETED licenses
--					* TBD: Delete batches with a status of ABORTED after rolling back license changes
-- ============================================================================================================================
CREATE PROCEDURE [dbo].[ALP_AbortBatch] 
(
	@BatchID int
)
AS
BEGIN
	SET NOCOUNT ON;
	
	-- Delete licenses with a status of PENDING from batches with a status of SUBMITTED (1)
	if exists(select * from ALP_Batches where ID=@BatchID and StatusID=1) 
	begin
		delete from [ALP_BatchParameterSetLicenseNumberOperationStatus] where BatchID=@BatchID and LicenseNumber in 
		(
			select LicenseNumber from 
			(
				select LicenseNumber, sum(case when OperationStart is null then 0 else 1 end) [started], sum(OperationStatusID) [status] -- license PENDING = 0
				from [ALP_BatchParameterSetLicenseNumberOperationStatus] 
				where BatchID=@BatchID
				group by LicenseNumber
			) a 
			where [started]=0 and [status]=0
		)

		-- delete the batch if there are no licenses in it
		if (select count(*) from [ALP_BatchParameterSetLicenseNumberOperationStatus] where BatchID=@BatchID) = 0
		begin
			delete from ALP_Batches where ID=@BatchID
		end
	end

END
GO
