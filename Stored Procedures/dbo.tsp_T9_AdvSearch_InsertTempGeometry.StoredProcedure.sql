USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearch_InsertTempGeometry]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_AdvSearch_InsertTempGeometry]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearch_InsertTempGeometry]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[tsp_T9_AdvSearch_InsertTempGeometry]

@UserID varchar(50) = null,
@SRID int = -1,
@WKT varchar(max) = null,
@ID int = -1 out,
@ErrMsg varchar(max) = '' out

AS
BEGIN
	if @UserID is not null and @UserID <>'' and @SRID is not null and @SRID > 0 and @WKT is not null and @WKT <> '' 
	begin
	begin try
		declare @geom geometry
		set @geom = geometry::STPolyFromText(@WKT, @SRID).MakeValid()
		INSERT INTO Prmry_AdvSearch_GisTemp ([UserID],[SRID],[Boundary]) VALUES (@UserID, @SRID, @geom)
		set @ID = SCOPE_IDENTITY()
		
		exec tsp_T9_AdvSearch_DeleteOrphanedGeometry @UserID, @ID -- EWH Oct 29, 2015: delete records in GisTemp table for user that are not in Current or Last tables

		--print @ID
		--select * from Prmry_AdvSearch_GisTemp where recordid = @ID

	end try
	begin catch
		set @ErrMsg = ERROR_MESSAGE()
	end catch
	end
	else
	begin
		set @ErrMsg = 'Not all parameter values received'
	end

	print @ErrMsg
END
GO
