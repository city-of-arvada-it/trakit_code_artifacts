USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_GetReceiptPaidAmount]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[usp_Arvada_GetReceiptPaidAmount]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_GetReceiptPaidAmount]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_Arvada_GetReceiptPaidAmount]
	@rcpNo varchar(50),
	@TranDate datetime
as
begin
/*
exec usp_Arvada_GetReceiptPaidAmount '06448G'

select * 
from dbo.permit_fees
where check_no = '06448G'


 select * 
 from permit_main
 where site_Addr like '8230 w 63%'


select * 
from permit_fees
where permit_no in
(
'ROOF17-03596'
)

select * 
from [dbo].[Etrakit_Cart]
where activity_no = 'ROOF17-03574'

select * 
from [dbo].[Etrakit_Cart_Fees]
where RECEIPT_NO = 'WEB6742'
or 
activity_no = 'ECON:170730092430244'

select * 
from [dbo].[Etrakit_SelectedFees]
where ACTIVITY_NO = 'RMIS17-01050'



select * 
from [dbo].[Etrakit_Cart_Activities]
where site_addr like '8603 Ing%'

select permit_no
from dbo.permit_fees
where
	datediff(day, '07/31/2017', assessed_date) in (-2,-1,0,1,2)
and (paid_amount is null or paid_amount = 0)
group by permit_no
having sum(amount) = 279.94

*/	

	declare @paidAmt float 

	select @paidAmt = isnull(sum(paid_Amount),0.00)  
	from dbo.permit_fees
	where check_no = @rcpNo
	and datediff(Day,@TranDate,paid_Date) = 0
	
	if (@paidAmt is null or @paidAmt = 0)
	begin
		select @paidAmt = isnull(sum(paid_Amount),0.00)  
		from dbo.project_Fees
		where check_no = @rcpNo
		and datediff(Day,@TranDate,paid_Date) = 0
	end
	 


	 select isnull(@paidAmt,0.00) as TrakItValue


end
 
GO
