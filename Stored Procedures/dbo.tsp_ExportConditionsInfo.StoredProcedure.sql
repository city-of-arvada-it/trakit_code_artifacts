USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ExportConditionsInfo]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_ExportConditionsInfo]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ExportConditionsInfo]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  
					  					  					  /*
tsp_ExportConditionsInfo @DBName='DEV', @FilePath='C:\CRW\iTrakitExports\', @FileName='ConditionsInfoExport'

*/

CREATE Procedure [dbo].[tsp_ExportConditionsInfo](
@DBName Varchar(50),
@FilePath Varchar(1000),
@FileName Varchar(150),
@ToggleXP_CMDShell INT = 1
)

As

SET NOCOUNT ON

Declare @SQL Varchar(8000)

If @ToggleXP_CMDShell Is Null Set @ToggleXP_CMDShell = 1

IF EXISTS (SELECT * FROM tempdb.sys.objects WHERE object_id = OBJECT_ID(N'tempdb..ConditionsInfo') AND type in (N'U'))
	Drop Table tempdb..ConditionsInfo

Create Table tempdb..ConditionsInfo([GROUPNAME] Varchar(200)
      ,[CATEGORY] Varchar(200)
      ,[TITLE] Varchar(200)
      ,[DESCRIPTION] Varchar(2000)
      ,[RECORDID] Varchar(200)
      ,[ORDERID] Varchar(200))


Insert Into tempdb..ConditionsInfo(GROUPNAME,CATEGORY,TITLE,[DESCRIPTION],RECORDID,ORDERID)
Values('GROUPNAME','CATEGORY','TITLE','DESCRIPTION','RECORDID','ORDERID')

Set @SQL = '
Insert Into tempdb..ConditionsInfo([GROUPNAME],[CATEGORY],[TITLE],[DESCRIPTION],[RECORDID],[ORDERID])
Select 
GROUPNAME=CASE WHEN GROUPNAME = '''' THEN Null ELSE GROUPNAME END,
CATEGORY=CASE WHEN CATEGORY = '''' THEN Null ELSE REPLACE(REPLACE(REPLACE(CATEGORY,CHAR(9),''''),CHAR(10),''''),CHAR(13),'''') END,
TITLE=CASE WHEN TITLE = '''' THEN Null ELSE REPLACE(REPLACE(REPLACE(TITLE,CHAR(9),''''),CHAR(10),''''),CHAR(13),'''') END,
[DESCRIPTION]=CASE WHEN [DESCRIPTION] = '''' THEN Null ELSE REPLACE(REPLACE(REPLACE([DESCRIPTION],CHAR(9),''''),CHAR(10),''''),CHAR(13),'''') END,
RECORDID=CASE WHEN RECORDID = '''' THEN Null ELSE RECORDID END,
ORDERID=CASE WHEN ORDERID = Null THEN ''999'' ELSE Convert(Varchar,ORDERID) END
From ' + @DBName + '..Prmry_ConditionsInfo
ORDER BY GroupName,ORDERID'
--Print(@SQL)
Exec(@SQL)

Select * From tempdb..ConditionsInfo


Declare @vDate Varchar(100)
Declare @OutputFileName Varchar(1000)

Set @vDate = Replace(Convert(Varchar,GetDate(),102),'.','_')
Print(@vDate)

--Set @OutputFileName = '"' + @FilePath + @FileName + '_' + @vDate + '.csv"'
Set @OutputFileName = '"' + @FilePath + @FileName + '.csv"'


If @ToggleXP_CMDShell = 1
 Begin
	--Enable xp_cmdshell
	Set @SQL = '
	EXEC sp_configure ''show advanced options'', 1
	RECONFIGURE
	EXEC sp_configure ''xp_cmdshell'', 1
	RECONFIGURE
	'
	Print(@SQL)
	Exec(@SQL)
 End

select @sql = 'bcp "select * from tempdb..ConditionsInfo" queryout ' + @OutputFileName + ' -c -t"|" -T -S'
+ @@servername
--Print(@SQL)
exec master..xp_cmdshell @sql

Drop Table tempdb..ConditionsInfo

Set @SQL = '
DELETE ' + @DBName + '.dbo.iTrakitFeeds Where [FileName] = ''' + @FileName + '.csv''

INSERT INTO ' + @DBName + '.dbo.iTrakitFeeds([filename],[data])
SELECT ''' + @FileName + '.csv'', * FROM OPENROWSET(
  BULK N''' + REPLACE(@OutputFileName,'"','') + ''', SINGLE_BLOB
) bt'
Print(@SQL)
Exec(@SQL)


If @ToggleXP_CMDShell = 1
 Begin
	-- Disable xp_cmdshell
	Set @SQL = '
	EXEC sp_configure ''xp_cmdshell'', 0
	RECONFIGURE
	EXEC sp_configure ''show advanced options'', 0
	RECONFIGURE
	'
	Print(@SQL)
	Exec(@SQL)
 End







GO
