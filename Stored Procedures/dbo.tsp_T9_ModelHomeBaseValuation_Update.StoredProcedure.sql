USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ModelHomeBaseValuation_Update]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_ModelHomeBaseValuation_Update]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ModelHomeBaseValuation_Update]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  
CREATE PROCEDURE [dbo].[tsp_T9_ModelHomeBaseValuation_Update] 
              @List as UpdateBaseValuation readonly
                       
AS
BEGIN
	
	update Prmry_ModelHomeBaseValuations 
		set QUANTITY = ttbl.Quantity from @List ttbl 
			join  Prmry_ModelHomeBaseValuations on ttbl.RecID = Prmry_ModelHomeBaseValuations.RecordID;
				

END





GO
