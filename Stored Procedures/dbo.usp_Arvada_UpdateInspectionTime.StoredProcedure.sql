USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_UpdateInspectionTime]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[usp_Arvada_UpdateInspectionTime]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_UpdateInspectionTime]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_Arvada_UpdateInspectionTime]
	@PermitNos nvarchar(max),
	@Inspectors nvarchar(max),
	@ScheduledDate datetime,
	@RunUpdate int = 0
as
/**********************************************************************************************************************
* Creator:      	 
* Date:         	 
* Title:			 
* Description:  	 
* Examples:    

exec usp_Arvada_UpdateInspectionTime @PermitNos=N'RMIS15-01807|ROW13-01538',@RunUpdate=0,@ScheduledDate='01/18/2016' 

select *

update i
set scheduled_time = null
from permit_inspections i
where recordid in ('SB:1501211118392836','EPRS:160101093103289')


select Field03 as Inspectors
from Prmry_InspectionControl
where Category = 'inspector'
and groupname = 'permits'
order by Field03 asc
					 
* Modifications:  	
  Developer Name		Date		Description
  ------------------	----------	----------------------------------------------------------- 
*
**********************************************************************************************************************/

set nocount on
 
 --------------------------
 --- Declare Variables   --
 --------------------------

 
 --------------------------
 --- Create Temp Tables  --
 --------------------------
create table #permits
(
	permitno varchar(50),
	recordid varchar(50)
)

create table #inspects
(
	inspector nvarchar(500)
)

	 
 --------------------------
 --- Body of Procedure   --
 --------------------------

insert into #permits
(
	permitno,
	recordid
)
select distinct
	permit_no,
	i.RECORDID
from dbo.Permit_Inspections i
inner join [dbo].[tfn_SSRS_String_To_Table](@PermitNos,'|',1) as b
	on b.val = i.PERMIT_NO

insert into #inspects
(
	inspector
)
select
	val
from [dbo].[tfn_SSRS_String_To_Table](@Inspectors,'|',1) as b

if @RunUpdate = 1
begin
	update i
	set SCHEDULED_TIME = 'PM'
	from dbo.inspections i
	inner join #permits p
		on p.permitno = i.ActivityID
		and p.RECORDID = i.RECORDID
	inner join #inspects ins
		on ins.inspector = i.INSPECTOR
	where 
		 DATEDIFF(day,i.scheduled_Date, @ScheduledDate) = 0
end

 
select 
	i.recordid,
	i.PERMIT_NO,
	i.InspectionType,
	u.username as inpector,
	i.SCHEDULED_DATE,
	i.SCHEDULED_TIME,
	m.site_addr as SiteAddr
from dbo.permit_inspections i
inner join #permits p
	on p.permitno = i.PERMIT_NO
inner join dbo.permit_main m
	on m.permit_no = i.permit_no
inner join dbo.prmry_users u
	on u.userid = i.inspector
inner join #inspects ins
		on ins.inspector = i.INSPECTOR
where
	DATEDIFF(day,i.scheduled_Date, @ScheduledDate) = 0
and (
		(@RunUpdate = 1 and i.SCHEDULED_TIME = 'AM') 
		or 
		@RunUpdate = 0
	)
order by 
	scheduled_date,
	i.inspector,
	i.permit_no
GO
