USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_LicenseRenew_UpdateExpirationDates]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_LicenseRenew_UpdateExpirationDates]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_LicenseRenew_UpdateExpirationDates]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  -- =============================================
-- Author:		<Micah Neveu>
-- Create date: <12/11/2014>
-- Description:	<Updates the expiration dates of licenses in the license renew license list in either LIVE or REVIEW tables>
-- @INLIVE: 1 is License2_Main, any other value is the Review_Main table
-- =============================================
CREATE PROCEDURE [dbo].[tsp_T9_LicenseRenew_UpdateExpirationDates] 
		@INLIVE INT,
		@UserID NVARCHAR(50) = 'GLR'
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @conditionCount INT
	SET @conditionCount = (SELECT COUNT (*) FROM LicenseRenew_ExpirationConditions)
	IF @conditionCount = 0
    BEGIN
	  	-- insert the DEFAULT action... any licensetype that doesn't exist uses DEFAULT
		-- mdm - 01/13/15 - added field list 
		INSERT INTO LicenseRenew_ExpirationConditions (License_Type, Condition, DefaultDate, ConditionDate) VALUES ('Default','1=2','CONVERT(VARCHAR(30), YEAR(GETDATE())) + ''/12/31''','CONVERT(VARCHAR(30), YEAR(GETDATE())) + ''/12/31''')
    END

	-- ENSURE ALL LICENSE TYPES ARE IN, THEY GET DEFAULTED NEXT
	INSERT INTO LicenseRenew_ExpirationConditions (License_Type) SELECT TypeName FROM Prmry_Types where GroupName = 'license2' AND typename not in (SELECT license_type FROM LicenseRenew_ExpirationConditions)

	-- UPDATE LICENSES USING EXPIRATION CONDITIONS TABLE
	-- OVERRIDE NULLS WITH DEFAULTS
	UPDATE LicenseRenew_ExpirationConditions SET Condition = (SELECT Condition FROM LicenseRenew_ExpirationConditions WHERE License_Type = 'Default') WHERE Condition IS NULL
	UPDATE LicenseRenew_ExpirationConditions SET DefaultDate = (SELECT DefaultDate FROM LicenseRenew_ExpirationConditions WHERE License_Type = 'Default') WHERE DefaultDate IS NULL
	UPDATE LicenseRenew_ExpirationConditions SET ConditionDate = (SELECT ConditionDate FROM LicenseRenew_ExpirationConditions WHERE License_Type = 'Default') WHERE ConditionDate IS NULL

	DECLARE @licenseType VARCHAR(2048)
	DECLARE @expirationCondition VARCHAR(2048)
	DECLARE @defaultDate VARCHAR(2048)
	DECLARE @conditionDate VARCHAR(2048)

	DECLARE @targetTable VARCHAR(2048)
			
	IF (@INLIVE = 1)
		BEGIN
			SET @targetTable = 'License2_Main'
		END
		ELSE
			SET @targetTable = 'LicenseRenew_Review_Main'

	DECLARE expirationCursor CURSOR
	LOCAL SCROLL STATIC

	FOR
		SELECT License_Type, condition, defaultDate, conditionDate FROM LicenseRenew_ExpirationConditions
		OPEN expirationCursor
		FETCH NEXT FROM expirationCursor INTO @licenseType, @expirationCondition, @defaultDate, @conditionDate
		WHILE @@FETCH_STATUS = 0
		BEGIN
	
		DECLARE @ExpireCommand VARCHAR(8000)

		SET @ExpireCommand = 'UPDATE ' + @targetTable + ' SET EXPIRED = ' + @conditionDate + ' WHERE License_Type = ''' 
			+ @licenseType + ''' AND ' + @targetTable + '.License_No IN (SELECT License_NO FROM LicenseRenew_License_List) AND '
			+ @expirationCondition
		EXEC (@ExpireCommand)
	
		FETCH NEXT FROM expirationCursor INTO @licenseType,@expirationCondition,@defaultDate,@conditionDate
	END
	
	CLOSE expirationCursor
	DEALLOCATE expirationCursor
		
	------------------------ Catch licenses that do not have a type in prmry_types -------------------------
	DECLARE @NoType INT = (SELECT COUNT(*) FROM LicenseRenew_Review_Main WHERE license_type NOT IN (SELECT typename FROM prmry_types WHERE groupname = 'license2'))

	IF @NoType > 0
		BEGIN
			-- licenses without a matching type are given a default expiration date (defaultDate of 'default' record in expiration conditions)
			INSERT INTO LicenseRenew_EventLog (SourceMode,EventText,USERID) VALUES ('UPDATE - warning', 'Expire date set to default for ' + CONVERT(NVARCHAR(20), @NoType) + ' licenses, which do not have a License Type in prmry_types.',@USERID)
			SET @defaultDate = (SELECT TOP 1 defaultDate FROM LicenseRenew_ExpirationConditions WHERE License_Type = 'default')
			SET @ExpireCommand = 'UPDATE ' + @targetTable + ' SET EXPIRED = ' + @defaultDate + ' WHERE ' + @targetTable + '.License_Type NOT IN (SELECT typename FROM prmry_types where GroupName = ''license2'') AND ' + @targetTable + '.License_No IN (SELECT License_NO FROM LicenseRenew_License_List)'
			EXEC (@ExpireCommand)
		END
END



GO
