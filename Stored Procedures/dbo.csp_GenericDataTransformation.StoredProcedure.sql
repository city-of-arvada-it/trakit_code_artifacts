USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[csp_GenericDataTransformation]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[csp_GenericDataTransformation]
GO
/****** Object:  StoredProcedure [dbo].[csp_GenericDataTransformation]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =================================================================================================================================================================
-- Author:		Micah Neveu
-- Create date: 2/14/2017
-- Description:	Applies the provided series of data transformations to the tables
--				This is a poor practice thing to do. We're doing for multiple reasons, including limitations on updating the datasets in the calling application.
--				Really, don't try this at home as it is, as a whole, a bad idea.
--				Also, if a different way to do this can be found in the client application, PLEASE DO IT.
--				So, it DELETES and entire row based on PRIMARY KEY, no where clauses, so be CAREFUL.
--	
-- Revisions:	EWH Aug 16, 2017: fixed issue with rows not deleting, added else conditions in loop, removed old comments, added new comments for clarity
--				ARG July 10 2017: fixed an issue with trying to update a TAX_ID column and adding fees
--				
-- =================================================================================================================================================================
CREATE PROCEDURE [dbo].[csp_GenericDataTransformation]

	@BatchID int = NULL,
	@LICENSE_NO varchar(30) = NULL,
	@Trans GenericDataTransformation READONLY

AS
BEGIN

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	declare @ErrMsg nvarchar(max) = ''
	
	if @BatchID is null or @LICENSE_NO is null 
	begin
		set @ErrMsg='Batch ID and License Number are required to update the database'
	end
	else
	begin

		-- create a working table since @Trans is read only
		declare @transformations table ([TransformationCommand] varchar(6) NOT NULL,
										[TransformationOrder] bigint,
										[TableName] varchar(200) NOT NULL,
										[PrimaryKeyColumnName] varchar(200) NOT NULL,
										[PrimaryKeyValue] varchar(200) NOT NULL,
										[ColumnName] varchar(200) NOT NULL,
										[ColumnValue] nvarchar(max),
										[Validated] bit,
										[IsIdentity] bit)

		--INSERT INTO @transformations SELECT * FROM @Trans ORDER BY TransformationOrder
		INSERT INTO @transformations ([TransformationCommand],[TransformationOrder],[TableName],[PrimaryKeyColumnName],[PrimaryKeyValue],[ColumnName],[ColumnValue],[Validated],[IsIdentity])
		SELECT						  [TransformationCommand],[TransformationOrder],[TableName],[PrimaryKeyColumnName],[PrimaryKeyValue],[ColumnName],[ColumnValue],0,0
		FROM @Trans
		WHERE ColumnName <> 'TAX_ID'
		ORDER BY TransformationOrder



		-- update the [IsIdentity] column for rows where the target ColumnName is the PrimaryKeyColumnName
		update t 
		set [IsIdentity] = isnull(is_identity, 0)
		from @transformations t
			inner join sys.columns c on Object_ID(t.TableName) = c.Object_ID and c.Name = t.PrimaryKeyColumnName 
		WHERE t.ColumnName = t.PrimaryKeyColumnName

		-- reset VALIDATED so no shennanigans!
		UPDATE @transformations set Validated = 0
	
		-- for delete commands, the column name and column value are not required, but they are required for AND clause.
		-- IF the column name and column value are supplied, we use those for creating the AND clause, othewerise, they
		-- get changed into primarykeycolumn and primarykeycolumnvalue (and still get AND'ed).
		UPDATE @transformations SET ColumnName = PrimaryKeyColumnName, ColumnValue = PrimaryKeyValue WHERE ColumnName = null
	
		-- Verify everything exists
		UPDATE @transformations SET Validated = (SELECT COUNT(*) FROM sys.columns WHERE Name=columnname AND Object_ID = Object_ID(tablename))
		DELETE FROM @transformations where Validated = 0
		-- Verify Primary Key
		UPDATE @transformations SET Validated = (SELECT COUNT(*) FROM sys.columns WHERE Name=primarykeycolumnname AND Object_ID = Object_ID(tablename))
		DELETE FROM @transformations WHERE validated = 0
		
		-- Scrub values
		-- strip out -- minus signs if there are TWO!
		UPDATE @transformations SET PrimaryKeyValue = REPLACE(PrimaryKeyValue,'--',''), ColumnValue = REPLACE(columnValue, '--','')
		-- triple up '
		UPDATE @transformations SET PrimaryKeyValue = REPLACE(PrimaryKeyValue,'''',''''''), ColumnValue = REPLACE(columnValue, '''','''''')
		
		DECLARE @executeThis nvarchar(max)

		-- testing update columnvalue and primarykeycolumnvalue to include '' (testing purposes only, we need to check the datatype)
		UPDATE @transformations SET PrimaryKeyValue = '''' + PrimaryKeyValue + '''' WHERE 0 <
			(SELECT count(*) FROM INFORMATION_SCHEMA.columns where table_name = TableName AND column_name = PrimaryKeyColumnName AND data_type in 
				('varchar', 'char','nchar','nvarchar','datetime')) 	
	
		-- pull the same tired trick for columnvalue	
		UPDATE @transformations SET ColumnValue = '''' + ColumnValue + '''' WHERE 0 <
			(SELECT count(*) FROM INFORMATION_SCHEMA.columns where table_name = TableName AND column_name = columnname AND data_type in 
				('varchar', 'char','nchar','nvarchar','datetime')) 
		
		-- nullify empty strings for values that are for non-textual data types (just float and int as of Mar 1, 2017)
		UPDATE @transformations SET ColumnValue = NULL
			WHERE 0 < (SELECT count(*) FROM INFORMATION_SCHEMA.columns where table_name = TableName AND column_name = columnname AND data_type in ('float', 'int')) 
			  and ColumnValue = ''


		-- convert BOOL to BIT if passed in as true/false
		UPDATE @transformations SET ColumnValue = 0 
			WHERE 0 < (SELECT count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = TableName AND COLUMN_NAME = columnname and data_type = 'bit' AND ColumnValue = 'false') --COLUMN_DEFAULT = '((0))')

		UPDATE @transformations SET ColumnValue = 1 
			WHERE 0 < (SELECT count(*) FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = TableName AND COLUMN_NAME = columnname and data_type = 'bit' AND ColumnValue = 'true') --COLUMN_DEFAULT = '((1))')



		-- TODO: stop writing terrible code
		delete from @transformations where ColumnValue is null and TransformationCommand <> 'DELETE' -- DELETE won't have any Column Values


		-- TODO: should we set ColumnValue = NULLIF('') for columns that have a default constraint?


		-- qualify the column names so they aren't confused with keywords in the SQL statement we construct
		UPDATE @transformations SET PrimaryKeyColumnName = '[' + PrimaryKeyColumnName + ']', ColumnName = '[' + ColumnName + ']' 

	


		-- ======================================================================================================================================================
		-- COMMAND CREATION 
		-- ======================================================================================================================================================

		-- create a checklist of all transformation orders, construct a statement for each one in order FIFO and delete after creating statement
		declare @tempo table(id int identity(1,1) not null, transformationOrder int )
		insert into @tempo 
		select transformationorder 
		from @transformations 
		group by transformationorder 
		order by TransformationOrder


		-- loop through temp transformations and build up super awesome
		declare @loopCounter int
		declare @count int
		declare @transformationID int
		declare @command varchar(6)
		declare @Statement nvarchar(MAX) = ''
		declare @columnNames nvarchar(MAX) 
		declare @columnValues nvarchar(MAX)

		select @count = count(*) from @tempo
		set @loopCounter = 1
	
		while @loopCounter <= @count
		BEGIN
					
			select top 1 @transformationID = transformationorder from @tempo WHERE id = @loopCounter
			select top 1 @command = transformationCommand from @transformations where TransformationOrder = @transformationID

			
			IF @command = 'INSERT'
			BEGIN
				----------------------------------------------------------------------------------------------------------------------------------------------------------
				-- INSERT
				----------------------------------------------------------------------------------------------------------------------------------------------------------
				set @Statement = @Statement + '  INSERT INTO ' + (select top(1) tablename from @transformations WHERE TransformationOrder = @transformationID) + ' ('

				set @columnNames = (
					select ColumnName + ', ' from @transformations where TransformationOrder = @transformationID AND IsIdentity != 1 for xml path(''))

				-- replace final , with a )
				set @columnNames = LEFT(@columnNames, LEN(@columnNames) - 1)
				set @columnNames = @columnNames + ') VALUES ('
		
				set @columnValues = (
					select isnull(ColumnValue,'') + ', ' from @transformations where TransformationOrder = @transformationID AND IsIdentity != 1 for xml path(''))

				-- replace final , with a )
				set @columnValues = LEFT(@columnValues, LEN(@columnValues) - 1)
				set @columnValues = @columnValues + ') '

				set @Statement = @Statement + @columnNames + @columnValues + ';'
				----------------------------------------------------------------------------------------------------------------------------------------------------------
			END
			ELSE
			BEGIN
				IF @command = 'UPDATE'
				BEGIN
					----------------------------------------------------------------------------------------------------------------------------------------------------------
					-- UPDATE
					----------------------------------------------------------------------------------------------------------------------------------------------------------
					DECLARE @whereClause nvarchar(max)
					set @whereClause = (select top(1) PrimaryKeyColumnName from @transformations where TransformationOrder = @transformationID) + '=' 
						+ (select top(1) PrimaryKeyValue from @transformations where TransformationOrder = @transformationID)
		
					set @Statement = @statement + '  UPDATE ' + (select top(1) tablename + ' SET ' from @transformations WHERE TransformationOrder = @transformationID) + ' '

					set @columnNames = (
						select ColumnName + '=' + isnull(ColumnValue,'') + ',' from @transformations where TransformationOrder = @transformationID AND IsIdentity != 1 for xml path(''))

					-- remove trailing ,
					set @columnNames = LEFT(@columnNames, LEN(@columnnames) - 1)

					set @statement = @Statement + @columnNames + ' WHERE ' + @whereClause + '; '
					----------------------------------------------------------------------------------------------------------------------------------------------------------
				END
				ELSE
				BEGIN
					IF @command = 'DELETE'
					BEGIN
						----------------------------------------------------------------------------------------------------------------------------------------------------------
						-- DELETE
						----------------------------------------------------------------------------------------------------------------------------------------------------------
						declare @delete nvarchar(max)
						-- note that we use top 1 because the primary key value should be the same for all rows for the TransformationOrder
						-- otherwise we get a duplicate delete statement for each row 
						set @delete = ( select top 1 '  DELETE FROM ' + tablename + ' WHERE ' + PrimaryKeyColumnName + ' = ' + PrimaryKeyValue 
										from @transformations where TransformationOrder = @transformationID for xml path('')) + ';'

						--print '@delete = ' + isnull(@delete, '')

						set @statement = @statement + ' ' + isnull(@delete,'')
						----------------------------------------------------------------------------------------------------------------------------------------------------------
					END
					ELSE
					BEGIN
						set @ErrMsg = 'Unknown command ' + '''' + isnull(@command,'NULL') + '''' + ' specified for license number ' + '''' + @LICENSE_NO + ''''
									+ ' in batch ' + '''' + cast(@BatchID as nvarchar) + '''' + ' TransformationOrder = ' + cast(@transformationID as nvarchar)
						break;
					END
				END
			END

			set @loopCounter = @loopCounter + 1
		END
		-- ======================================================================================================================================================



		if @ErrMsg = ''
		begin
			----------------------------------------------------------------------------------------------------------------------------------------------------------
			-- execute @statement
			----------------------------------------------------------------------------------------------------------------------------------------------------------
			-- free up memory?
			--delete from @transformations where TransformationOrder IN (SELECT TransformationOrder from @tempo)
	
	
			--print '@statement = ' + isnull(@statement, '')

			-- ADD TRANSACTIONNESS
			set @executeThis = 'BEGIN TRANSACTION [Transformations] BEGIN TRY  ' + isnull(@statement,'') + 
			' COMMIT TRANSACTION [Transformations] SELECT ''Success'' AS ''Result'' END TRY BEGIN CATCH set @sp_errmsg = error_message() ROLLBACK TRANSACTION [Transformations] SELECT ''Failed'' AS ''Result'' END CATCH'
			
			--print @executethis
	
			declare @def nvarchar(max) = N'@sp_errmsg nvarchar(max) out'
			execute sp_executesql @executeThis, @def, @sp_errmsg=@ErrMsg out
			----------------------------------------------------------------------------------------------------------------------------------------------------------
		end
	end


	set @ErrMsg = isnull(@ErrMsg, '')
	--print '@ErrMsg=' + @ErrMsg

	if @ErrMsg <> ''
	begin
		RAISERROR(@ErrMsg, 16, 1) with nowait -- let caller know there is a problem
	end

END

GO
