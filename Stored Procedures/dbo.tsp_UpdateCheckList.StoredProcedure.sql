USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_UpdateCheckList]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_UpdateCheckList]
GO
/****** Object:  StoredProcedure [dbo].[tsp_UpdateCheckList]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  

Create Procedure [dbo].[tsp_UpdateCheckList]
@List as CheckListType readonly
As
Begin
	/* update one at a time, read fo existing per row */
	
	--RB Check if table exists before creating it
   IF OBJECT_ID('tempdb..#tmpCheckList') IS NOT NULL  
	drop table #tmpCheckList
		
	Create Table #tmpCheckList(
		RecordID varchar(50) null,
	SubRecordID varchar(50) null,
	ItemValue int not null,
	CheckListID int not null,
	UserID varchar(50) null,
	Notes varchar(max),
	CheckDate datetime null, CheckListValueID int null)
	
	Declare @CheckValueTable table(CheckListValueID int, CheckListID int)

	--Insert into Temp table all the non existing records from the passed in table.
	Insert into #tmpCheckList 
		(recordID, SubRecordID, ItemValue, CheckListID, UserID, Notes, CheckDate, CheckListValueID)
		Select * from @List L where 		
		Not Exists (Select * from CheckListvalues where 
		RecordID = l.RecordID
		And SubRecordID = L.SubRecordID
		And CheckListID = L.CheckListID)

	-- Insert the new records.
	Insert Into CheckListValues (recordID, SubRecordID, ItemValue, CheckListID, userID, checkDate)
	output Inserted.CheckListValueID, inserted.CheckListID into @CheckValueTable
	Select RecordID, SubRecordID, ItemValue, CheckListID, userID, CheckDate from #tmpCheckList

		
	Insert into CheckListNotes (CheckListValueID, recordID, SubRecordID, Note, UserID, LastUpdate)
	Select CT.CheckListValueID, RecordID, SubRecordID, notes, UserID, CheckDate 
		from #tmpCheckList CL join @CheckValueTable CT 
		on  CL.CheckListID = CT.CheckListID
	 where len(notes) > 0

	
	
	Delete from #tmpCheckList 	
	Delete from @CheckValueTable		
	-- Get the Existing Records for an update.	


	Insert into #tmpCheckList 
	(recordID, SubRecordID, ItemValue, CheckListID, UserID, Notes, CheckDate, CheckListValueID)
	Select * from @List L where 		
		Exists (Select * from CheckListvalues where 
		RecordID = l.RecordID
		And SubRecordID = L.SubRecordID
		And CheckListID = L.CheckListID)



	-- Update Existing Records

	Update CheckListValues  
	Set ItemValue = #tmpCheckList.ItemValue,
	userID = #tmpCheckList.UserID, 
	CheckDate = #tmpCheckList.CheckDate
	From #tmpCheckList
	Where  Checklistvalues.CheckListValueID = #tmpCheckList.CheckListValueID

	-- We ALWAYS INSERT NOTES, never update.
	Insert into CheckListNotes (CheckListValueID, recordID, SubRecordID, Note,UserID, LastUpdate)
	Select CheckListValueID, RecordID, SubRecordID, notes, UserID, CheckDate from #tmpCheckList where len(notes) > 0 and CheckListValueID <> -99

	
	select * from #tmpCheckList
	drop table #tmpCheckList

End




GO
