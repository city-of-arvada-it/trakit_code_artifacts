USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearch_Rename]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_AdvSearch_Rename]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearch_Rename]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[tsp_T9_AdvSearch_Rename]
	@UserID varchar(50) = null,
	@SearchID int = -1,
	@NewName varchar(50) = null
AS
BEGIN
declare @ErrMsg varchar(50)
set @ErrMsg = ''

-- check to make sure that the user has a saved search with the given SearchID
if (select count(*) from [Prmry_AdvSearch_SavedItem] where userid=@UserID and RecordID=@SearchID) = 1
begin
	-- check if @NewName already exists in a list of search names that the user has saved currently
	if (select count(*) from [Prmry_AdvSearch_SavedItem] where userid=@UserID and SearchName=@NewName) > 0 
	begin
		set @ErrMsg = 'User already has a search saved as this name.'
	end
	else
	begin
		-- TODO: perform other validations?

		-- update search name
		update [Prmry_AdvSearch_SavedItem] set SearchName=@NewName where userid=@UserID and RecordID=@SearchID -- should be unnecessary, but include userid just in case
		update [Prmry_AdvSearch_currentItem] set SearchName=@NewName where userid=@UserID and RefSearchID=@SearchID -- should be unnecessary, but include userid just in case
		update [Prmry_AdvSearch_LastItem] set SearchName=@NewName where userid=@UserID and RefSearchID=@SearchID -- should be unnecessary, but include userid just in case
	end

end
else
begin
	set @ErrMsg = 'User does not have a saved search with that ID, or more than one saved search exists with that ID.' 
end

select @ErrMsg [ErrMsg]

END
GO
