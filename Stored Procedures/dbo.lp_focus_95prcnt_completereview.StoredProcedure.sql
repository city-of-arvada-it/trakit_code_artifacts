USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[lp_focus_95prcnt_completereview]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[lp_focus_95prcnt_completereview]
GO
/****** Object:  StoredProcedure [dbo].[lp_focus_95prcnt_completereview]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* 
--Ychavez 3/17/2014 - FOR FOCUS Measures report
-------------------------------------------------------------------------------------------
95% of submitted plans given an initial review of completeness within 3 business days.
--exec lp_focus_95prcnt_completereview @APPLIED_FROM = '01/01/2014' , @APPLIED_TO = '02/10/2014'
--finish, get results
-------------------------------------------------------------------------------------------
*/
CREATE procedure [dbo].[lp_focus_95prcnt_completereview]
		@APPLIED_FROM datetime = null,
		@APPLIED_TO datetime = null
as
begin


	--1st measure--
-------------------------------------------------------------------------------------------
--95% of submitted plans given an initial review of completeness within 3 business days.
-------------------------------------------------------------------------------------------
--DROP TABLE #tmp_measure
Select p.permit_no, p.applied, date_received,
case when DATEDIFF(day, applied, date_received) < 3 then 'less than 72 hours' else
'out of measure'
end as measure
,0 as count_less_72_hours, 0 as count_outofmeasure, 0 as total_permits--, *
into #tmp_measure
 from permit_main p
 JOIN PERMIT_REVIEWS R ON P.PERMIT_NO = R.pERMIT_NO

where permittype in ('RESIDENTIAL', 'new single family','COMMERCIAL' )
	and permitsubtype in('NEW DUPLEX', 'NEW SF ATTACHED', 'NEW SF DETACHED', 'NEW SFA')
 AND REVIEWTYPE IN ('DOCUMENTATION REVIEW',  'DOCUMENTATION REVIEW-COMMERCIAL')	
 	
AND applied  between  @APPLIED_FROM and @APPLIED_TO --'01/01/2014' and '03/20/2014' --
AND p.STATUS NOT IN ('WITHDRAWN', 'DENIED', 'VOID    ', 'VOID')

------------------------------------------------------------------------------------------
--CALCULATE TOTAL PERMITS / PERMITS IN MEASURE - OUT OF MEASURE / LESS THAN 72 MEASURE--
------------------------------------------------------------------------------------------

--drop table #tmp_count72
select COUNT(PERMIT_NO) as count_less_72_hours
into #tmp_count72
FROM #tmp_measure 
WHERE applied between @APPLIED_FROM and @APPLIED_TO
and measure = 'less than 72 hours'


--drop table #tmp_outofmeasure
select COUNT(PERMIT_NO) as count_outofmeasure
into #tmp_outofmeasure
FROM #tmp_measure 
WHERE applied between  @APPLIED_FROM and @APPLIED_TO
and measure = 'out of measure'

update #tmp_measure
set count_less_72_hours =  b.count_less_72_hours
from #tmp_count72 b


update #tmp_measure
set count_outofmeasure =  b.count_outofmeasure
from #tmp_outofmeasure b

select distinct sum(count_less_72_hours + count_outofmeasure) as total_permits--, count_less_72_hours
into #tmp_totpermits
 from #tmp_outofmeasure,  #tmp_count72


update #tmp_measure
set total_permits =  b.total_permits
from #tmp_totpermits b

update #tmp_totpermits
set total_permits =  0
where total_permits is null or len(total_permits) < 1
--need to combine all the results into one table
--USE THESE TWO NUMBERS TO CALCULATE IN REPORT--
--select (count_less_72_hours  /total_permits) as total
--select * from #tmp_measure


select
 SUM(cast(count_less_72_hours as decimal)) / Sum(cast(total_permits as decimal)) * 100 as percent_plans_cmplete_review, 
case when total_permits IS null then 0 else total_permits end as total_permits, count_less_72_hours
into #tmp_report
from #tmp_measure
group by total_permits, count_less_72_hours

select * from #tmp_report



--select
--* --SUM(cast(total_permits as decimal)) / Sum(cast(count_less_72_hours  as decimal)) as test
--from #tmp_measure
end
GO
