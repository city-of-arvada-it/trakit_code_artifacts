USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_SpecialEventPermit]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[usp_Arvada_SpecialEventPermit]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_SpecialEventPermit]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[usp_Arvada_SpecialEventPermit]
	@Permit_No varchar(30)
as
/**********************************************************************************************************************
* Creator:      	 
* Date:         	 
* Title:			 
* Description:  	 
* Examples:    

EXECUTE dbo.[usp_Arvada_SpecialEventPermit]  'SPE15-00034'

update m
set issued = '10/01/2015'
from permit_Main m
where permit_no = 'SPE15-00034'

select * 
from permit_udf
where spe_start_time is not null
					 
* Modifications:  	
  Developer Name		Date		Description
  ------------------	----------	----------------------------------------------------------- 
*
**********************************************************************************************************************/

set nocount on
 
 --------------------------
 --- Declare Variables   --
 --------------------------

 
 --------------------------
 --- Create Temp Tables  --
 --------------------------
 
 --------------------------
 --- Body of Procedure   --
 --------------------------

 
 select 
	m.permit_no,
	m.approved,
	m.[status],
	isnull(u2.username, m.approved_by) as approved_by,
	convert(Varchar(10), convert(date, m.issued),101) as issued,
	convert(Varchar(10), convert(date, m.expired),101) as expired,
	m.site_addr,
	m.site_subdivision,
 
		 stuff(
			 (select CHAR(13)+char(10)+ parc.site_Addr 
			  from [dbo].[Permit_Parcels] as parc
			  where parc.permit_no = m.permit_no  
			  for xml path(''), root('MyString'), type 
			  ).value('/MyString[1]','nvarchar(max)') ,1,2,'') as AdditionalSites,

	
	m.[description],
	
	app.name as applicantName,
	case when (LEN(app.phone) not in (10,7))
		 then  app.phone
	 when (LEN(app.phone) = 7)
		then SUBSTRING(app.phone, 4, 3) + '-' + SUBSTRING(app.phone, 7, 4)
	else
       LEFT(app.phone, 3) + '-' + SUBSTRING(app.phone, 4, 3) + '-' + SUBSTRING(app.phone, 7, 4)
	end  as applicantPhone,
	app.ADDRESS1 as applicantAdd1,
	app.CITY as applicantCity,
	app.[STATE] as applicantState,
	app.ZIP as applicantZip,
	app.EMAIL as applicantEmail,

	app.name + 
		case when app.ADDRESS1 is not null and app.ADDRESS1 != '' then char(10) + char(13) + app.ADDRESS1 else '' end +
		case when app.CITY is not null and app.CITY != '' then char(10) + char(13) + app.CITY else '' end +
		case when app.[STATE] is not null and app.[STATE] != '' then  ', ' + app.[STATE] else '' end +
		case when app.ZIP is not null and app.ZIP != '' then  ', ' + app.ZIP else '' end  as ApplicantFullAddress,

	eventContact.name + 
		case when eventContact.ADDRESS1 is not null and eventContact.ADDRESS1 != '' then char(10) + char(13) + eventContact.ADDRESS1 else '' end +
		case when eventContact.CITY is not null and eventContact.CITY != '' then char(10) + char(13) + eventContact.CITY else '' end +
		case when eventContact.[STATE] is not null and eventContact.[STATE] != '' then ', ' + eventContact.[STATE] else '' end +
		case when eventContact.ZIP is not null and eventContact.ZIP != '' then  ', ' + eventContact.ZIP else '' end  as eventContactFullAddress,


	eventContact.name as eventContactName,
	case when (LEN(eventContact.phone) not in (10,7))
		 then  eventContact.phone
	 when (LEN(eventContact.phone) = 7)
		then SUBSTRING(eventContact.phone, 4, 3) + '-' + SUBSTRING(eventContact.phone, 7, 4)
	else
       LEFT(eventContact.phone, 3) + '-' + SUBSTRING(eventContact.phone, 4, 3) + '-' + SUBSTRING(eventContact.phone, 7, 4)
	end as eventContactPhone,
	eventContact.ADDRESS1 as eventContactAdd1,
	eventContact.CITY as eventContactCity,
	eventContact.[STATE] as eventContactState,
	eventContact.ZIP as eventContactZip,
	eventContact.EMAIL as eventContactEmail,

	u.SED_HOUR_OP,
	u.SPE_ENTERTAIN as SPE_TYPE_ENTERTAIN, --u.SPE_TYPE_ENTERTAIN,
	u.SPE_ALCOHOL,
	convert(Varchar(10), convert(Date, u.SED_START),101) + ' ' +  isnull(u.spe_start_time,'') As StartDate,
	convert(Varchar(10), convert(Date,u.SED_END),101) + ' ' +  isnull(u.spe_end_time,'') as EndDate,
	null as SP_FT_ONSITE, --u.SP_FT_ONSITE,
	u.SPE_TENTS_STRUCT as SPE_TENT_COUNT, --u.SPE_TENT_COUNT,
	u.SPE_EVENT_DESC as spe_event_Desc, --u.spe_event_Desc
	--u.SPE_SET_REMO_TIME,
	u.SPE_SETUP,
	u.SPE_REMOVAL,
	u.SPE_VENDORS,
	u.SPE_TENTS_STRUCT,
	u.SED_START,
	u.SED_END
from dbo.permit_main m
left join 
	(select top 1 name, address1, address2, city, [state], zip, phone, email, permit_no
	 from dbo.permit_people
	 where permit_no = @permit_no
	 and nameType = 'APPLICANT'
	 ) as app
	 on app.permit_no = m.permit_no
left join 
	(select top 1 name, address1, address2, city, [state], zip, phone, email, permit_no
	 from dbo.permit_people
	 where permit_no = @permit_no
	 and nameType = 'ONSITE EVENT COORDINATOR'
	 ) as eventContact
	 on eventContact.permit_no = m.permit_no
left join dbo.permit_udf u
	on u.permit_no = m.permit_no
left join dbo.prmry_users u2 with (nolock)
	on u2.userid = m.approved_by
where m.permit_no = @permit_no



GO
