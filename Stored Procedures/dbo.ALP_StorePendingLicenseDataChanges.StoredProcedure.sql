USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[ALP_StorePendingLicenseDataChanges]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[ALP_StorePendingLicenseDataChanges]
GO
/****** Object:  StoredProcedure [dbo].[ALP_StorePendingLicenseDataChanges]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==============================================================================================================================================================================
-- Author:		Erick Hampton
-- Create date: Mar 31, 2016
-- Description:	Stores pending license data changes into ALP_LicenseDataChanges
-- ==============================================================================================================================================================================
CREATE PROCEDURE [dbo].[ALP_StorePendingLicenseDataChanges]

	@BatchID int = NULL,
	@LICENSE_NO varchar(30) = NULL,
	@Trans GenericDataTransformation READONLY

AS
BEGIN

	if (select count(*) from @Trans) > 0
	begin

		if @BatchID is not null and @BatchID > -1 and @LICENSE_NO is not null and @LICENSE_NO <> ''
		begin
			
			insert into [ALP_LicenseDataChanges] ([BatchID],[LICENSE_NO],[TransformationCommand],[TransformationOrder],[TableName],[PrimaryKeyColumnName],[PrimaryKeyValue],[ColumnName],[ColumnValue],[OldColumnValue],[ChangeCommitted])
			select @BatchID,@LICENSE_NO,[TransformationCommand],[TransformationOrder],[TableName],[PrimaryKeyColumnName],[PrimaryKeyValue],[ColumnName],[ColumnValue],[OldColumnValue],0
			from @Trans
			order by [TransformationOrder]

		end
	end

END
GO
