USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_BB_DeleteBAXMarkupsOnAttachment]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_BB_DeleteBAXMarkupsOnAttachment]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_BB_DeleteBAXMarkupsOnAttachment]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  
-- =============================================
-- Author:		<Micah Neveu>
-- Create date: <6/26/2015>
-- Description:	<Deletes all markups that exist on 
--				a list of comma delimited recordids>
-- =============================================
CREATE PROCEDURE [dbo].[tsp_T9_BB_DeleteBAXMarkupsOnAttachment]
	@AttachmentRecordID varchar(3100) -- turn this into a comma delimited list in the future (up to 30 records)
	AS
BEGIN
	-- remove padding between items
	SELECT @AttachmentRecordID = REPLACE(@AttachmentRecordID, ' ', '')
	DECLARE @Split char(1), @X xml
	SELECT @Split = ','
	SELECT @X = CONVERT(xml,'<root><s>' + REPLACE(@AttachmentRecordID, @Split, '</s><s>') + '</s></root>')

	--select [Value] = T.c.value('.','varchar(20)') FROM @X.nodes('/root/s') T(c)
	--select * from prmry_bax_markups where attachmentrecordid in (select [Value] = T.c.value('.','varchar(20)') FROM @X.nodes('/root/s') T(c))
		
	DELETE FROM prmry_bax_markups WHERE AttachmentRecordID IN (select [Value] = T.c.value('.','varchar(20)') FROM @X.nodes('/root/s') T(c))
END



--***********************************************************************************************************

/****** Object:  StoredProcedure [dbo].[tsp_T9_BB_GetBAXMarkups]    Script Date: 07/20/2015 13:38:29 ******/
SET ANSI_NULLS ON


GO
