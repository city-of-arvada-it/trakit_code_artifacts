USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_BB_AttachmentEligibleForCopyMarkups]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_BB_AttachmentEligibleForCopyMarkups]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_BB_AttachmentEligibleForCopyMarkups]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  
-- =============================================
-- Author:		<Micah Neveu>
-- Create date: <6/29/2015>
-- Description:	<determines if this record is eligible for copy markups>
-- =============================================
CREATE PROCEDURE [dbo].[tsp_T9_BB_AttachmentEligibleForCopyMarkups]
	@AttachmentRecordID varchar(30),
	@versionNumber int,
	@activityNumber varchar(30),
	@baseFileName varchar(300),
	@group varchar(20) = 'PERMIT',
	@eligible bit = 0 OUT
AS
BEGIN
	SET NOCOUNT ON;
	
--SELECT @versionNumber = VERSION, @activityNumber = permit_No, @attachmentDesc = [DESCRIPTION] FROM permit_attachments WHERE recordid = @AttachmentRecordID

-- get the count of markups for this
DECLARE @baxCount INT = (SELECT COUNT(*) FROM prmry_bax_markups WHERE AttachmentRecordID = @AttachmentRecordID)

IF (@baxCount = 0)
 BEGIN
	-- we need the correct table - AEC_attachments, case_attachments, crm_attachments, geo_attachments, license2_attachments, payment_attachments, permit_attachments, project_attachments
	DECLARE @precordid varchar(30) = 'xxx'


	

	-- and yeah, there is DEFINITELY a better way...
	IF (@group = 'AEC') BEGIN SELECT @precordid = (SELECT RECORDID FROM aec_attachments where ST_LIC_NO = @activityNumber AND VERSION = (@versionNumber - 1) AND [pathname] like @basefilename) END
	IF (@group = 'CASE') BEGIN SELECT @precordid = (SELECT RECORDID FROM case_attachments where CASE_NO = @activityNumber AND VERSION = (@versionNumber - 1) AND [pathname] like @basefilename) END
	IF (@group = 'CRM') BEGIN SELECT @precordid = (SELECT RECORDID FROM crm_attachments where ISSUE_ID = @activityNumber AND VERSION = (@versionNumber - 1) AND [pathname] like @basefilename) END
	IF (@group = 'GEO') BEGIN SELECT @precordid = (SELECT RECORDID FROM geo_attachments where SITE_APN = @activityNumber AND VERSION = (@versionNumber - 1) AND [pathname] like @basefilename) END
	IF (@group = 'LICENSE2') BEGIN SELECT @precordid = (SELECT RECORDID FROM license2_attachments where license_no = @activityNumber AND VERSION = (@versionNumber - 1) AND [pathname] like @basefilename) END
	IF (@group = 'PAYMENT') BEGIN SELECT @precordid = (SELECT RECORDID FROM payment_attachments where Activity_no = @activityNumber AND VERSION = (@versionNumber - 1) AND [pathname] like @basefilename) END
	IF (@group = 'PERMIT') BEGIN SELECT @precordid = (SELECT RECORDID FROM permit_attachments where permit_no = @activityNumber AND VERSION = (@versionNumber - 1) AND [pathname] like @basefilename) END
	IF (@group = 'PROJECT') BEGIN SELECT @precordid = (SELECT RECORDID FROM project_attachments where project_no = @activityNumber AND VERSION = (@versionNumber - 1) AND [pathname] like @basefilename) END
	-- ... to get this done cleanly

	IF (@precordid IS NOT NULL)
		BEGIN
			-- we have a valid previous version number
			DECLARE @pcount int = (SELECT COUNT(*) FROM prmry_bax_markups WHERE AttachmentRecordID = @precordid)
			-- if there are more than 0 markups, then eligible! otherwise, not so much
			SELECT @eligible = (SELECT(CAST(CASE @pcount WHEN 0 THEN 0 ELSE 1 END AS BIT)))
		END
 END

 
print 'eligibility ' + CAST(@eligible AS VARCHAR(9))

END



GO
