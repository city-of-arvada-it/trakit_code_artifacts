USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ITR_AETimesheets]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_ITR_AETimesheets]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ITR_AETimesheets]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  
					  					  					  
/*

select * from prmry_timesheets

insert into prmry_timetasks(userid,username,taskname,hourly_rate,account)
values('mp2','Mark Page 2','Action',30,null)
insert into prmry_timetasks(userid,username,taskname,hourly_rate,account)
values('mp2','Mark Page 2','Inspection',40,'Inspection Account NO')
insert into prmry_timetasks(userid,username,taskname,hourly_rate,account)
values('mp2','Mark Page 2','Review',50,'Review Account #')

select * from prmry_timetasks where hourly_rate > 0


tsp_GblAETimesheets @RecordID='STE:910221419000032',@Notes='test2'

tsp_GblAETimesheets @RecordID = '0',  @Notes = 'first test comment',  @Posted = 'False',  @UserID = 'MP2',  @UserName = 'Mark Page 2',  @vWorkDate = '4/8/2012',  @ActivityGroup = '',  @ActivityNo = 'AC05-2174',  @TaskName = 'Action',  @Account = '',  @LinkRecordID = '',  @LinkGroup = 'CASE',  @Hours = '5.75',  @HourlyRate = '0',  @Amount = '0'

*/
-- =====================================================================
-- Revision History:
--	RR - 10/28/13
--	mdm - 11/5/13 - added revision history	
-- =====================================================================  

CREATE Procedure [dbo].[tsp_ITR_AETimesheets] (
@RecordID Varchar(60),
@GeneratedRecordID Varchar(30) = Null,
@UserID Varchar(6) = Null,
@UserName Varchar(30) = Null,
@vWorkDate Varchar(50) = Null,
@ActivityGroup Varchar(60) = Null,
@ActivityNo Varchar(60) = Null,
@TaskName Varchar(60) = Null,
@Account Varchar(30) = Null,
@SubFeeRecordID Varchar(60) = Null,
@LinkRecordID Varchar(60) = Null,
@LinkGroup Varchar(15) = Null,
@Notes Varchar(2000) = Null,
@Hours Float = Null,
@HourlyRate Float = Null,
@Amount Float = Null,
@Posted Varchar(1) = Null
)


As

Declare @SQL Varchar(8000)
Declare @WHERE Varchar(5000)
Declare @WorkDate Datetime

Set @WHERE = ''
Set @WorkDate = Null

--If @UserID = '' Set @UserID = Null
--If @UserName = '' Set @UserName = Null
--If @vWorkDate = '' Set @vWorkDate = Null
--If @ActivityGroup = '' Set @ActivityGroup = Null
--If @ActivityNo = '' Set @ActivityNo = Null
--If @TaskName = '' Set @TaskName = Null
--If @Account = '' Set @Account = Null
--If @SubFeeRecordID = '' Set @SubFeeRecordID = Null
If @LinkRecordID = '' Set @LinkRecordID = Null
--If @LinkGroup = '' Set @LinkGroup = Null
--If @Notes = '' Set @Notes = Null

If @vWorkDate = '' Set @WorkDate = Null ELSE Set @WorkDate = Convert(Datetime,@vWorkDate)


If @RecordID = '0'
 Begin
	Print 'Insert New Record'
	Insert Into Prmry_Timesheets(RecordID,UserID,UserName,WorkDate,Activity_Group,Activity_No,TaskName,[Hours],Hourly_Rate,Amount,Posted,Notes,Account,SubFee_RecordID,Link_RecordID,Link_Group)
	Values(@GeneratedRecordID,@UserID,@UserName,@WorkDate,@ActivityGroup,@ActivityNo,@TaskName,@Hours,0,0,0,@Notes,Null,@SubFeeRecordID,@LinkRecordID,@LinkGroup)	

	Update pts Set
	Hourly_Rate = ptt.Hourly_Rate,
	Amount = pts.[Hours] * ptt.Hourly_Rate,
	Account = ptt.Account
	From Prmry_Timesheets pts
	JOIN Prmry_Timetasks ptt
	 ON (pts.UserID = ptt.UserID OR pts.UserName = ptt.UserName)
	 AND pts.TaskName = ptt.TaskName

 End
ELSE
 Begin
	 If @Posted Is Null
		  Begin
			Print 'Update Existing Record'
			
			Update t Set
			UserID = CASE WHEN @UserID Is Not Null THEN @UserID ELSE UserID END,
			UserName = CASE WHEN @UserName Is Not Null THEN @UserName ELSE UserName END,
			WorkDate = CASE WHEN @WorkDate Is Not Null THEN @WorkDate ELSE WorkDate END,
			Activity_Group = CASE WHEN @ActivityGroup Is Not Null THEN @ActivityGroup ELSE Activity_Group END,
			Activity_No = CASE WHEN @ActivityNo Is Not Null THEN @ActivityNo ELSE Activity_No END,
			TaskName = CASE WHEN @TaskName Is Not Null THEN @TaskName ELSE TaskName END,
			[Hours] = CASE WHEN @Hours Is Not Null THEN @Hours ELSE [Hours] END,
			Hourly_Rate = CASE WHEN @HourlyRate Is Not Null THEN @HourlyRate ELSE Hourly_Rate END,
			--Posted = CASE WHEN @Posted Is Not Null THEN @Posted ELSE Posted END,
			Account = CASE WHEN @Account Is Not Null THEN @Account ELSE Account END,
			--Amount = CASE WHEN @Amount Is Not Null THEN @Amount ELSE Amount END,
			--SubFee_RecordID = CASE WHEN @SubFeeRecordID Is Not Null THEN @SubFeeRecordID ELSE SubFee_RecordID END,
			Notes = CASE WHEN @Notes Is Not Null THEN @Notes ELSE Notes END,
			Link_RecordID = CASE WHEN @LinkRecordID Is Not Null THEN @LinkRecordID ELSE Link_RecordID END,
			Link_Group = CASE WHEN @LinkGroup Is Not Null THEN @LinkGroup ELSE Link_Group END
			From Prmry_Timesheets t
			WHERE t.RecordID = @RecordID
		End
	ELSE
	 Begin
			Print 'Post Timesheet Record'
			
			Update t Set
			Posted = @Posted,
			SubFee_RecordID = @SubFeeRecordID
			From Prmry_Timesheets t
			WHERE t.RecordID = @RecordID
		End
 End













GO
