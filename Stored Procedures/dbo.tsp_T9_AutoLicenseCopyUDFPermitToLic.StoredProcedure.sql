USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AutoLicenseCopyUDFPermitToLic]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_AutoLicenseCopyUDFPermitToLic]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AutoLicenseCopyUDFPermitToLic]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/****** Object:  StoredProcedure [dbo].[tsp_T9_AutoLicenseCopyUDFPermitToLic]    Script Date: 9/14/2015 9:43:37 AM ******/
					  

Create Procedure [dbo].[tsp_T9_AutoLicenseCopyUDFPermitToLic](
	@Permit_No as Varchar(30),
	@License_No as Varchar(30) 
)

As
BEGIN
		DECLARE @Count_UDF_MATCH as int = 0
	Declare @Count_PERMIT_UDF as int  = 0
	
	IF OBJECT_ID('tempdb..#TempCopyUdfPERToLIC') IS NOT NULL
		DROP TABLE #TempCopyUdfPERToLIC

	Begin Try	

			SELECT c.ActivityID, a.PropertyID as PERMIT_PROPID, a.PropertyName as PERMIT_PROPNAME, a.PropertyType as PERMIT_PROPTYPE, b.PropertyID as LICENSE_PROPID, b.PropertyName as LICENSE_PROPNAME, 
			   b.PropertyType as LICENSE_PROPTYPE, c.StringValue as PERMIT_STRINGVALUE, c.IntValue as PERMIT_INTVALUE, c.DecimalValue as PERMIT_DECIMALVALUE, c.DateValue as PERMIT_DATEVALUE
			into #TempCopyUdfPERToLIC FROM UserDefinedProperty a
			inner join userdefinedProperty b on a.PropertyName = b.PropertyName 
			inner join UserDefinedPropertyValues c on c.PropertyID = a.PropertyID and c.ActivityID = @Permit_No  and a.PropertyName <> 'PERMIT_NO' and a.PropertyName <> 'RECORDID' 
			WHERE (a.ActivityTypeID = 1 and a.SubActivityTypeID is NULL) and a.propertyname <> 'RECORDID' and a.propertyname <> 'PERMIT_NO' and
				   (b.ActivityTypeID = 3 and b.SubActivityTypeID is NULL) and a.propertyname <> 'RECORDID' and a.propertyname <> 'LICENSE_NO' 		   		   

			Set @Count_UDF_MATCH  = (select count(*) from #TempCopyUdfPERToLIC)

			if @Count_UDF_MATCH  > 0
				Begin
					UPDATE UserDefinedPropertyValues set StringValue = TEMP.PERMIT_STRINGVALUE , IntValue = TEMP.PERMIT_INTVALUE , DecimalValue = TEMP.PERMIT_DECIMALVALUE , DateValue = TEMP.PERMIT_DATEVALUE  
					from UserDefinedPropertyValues UD inner join #TempCopyUdfPERToLIC TEMP on uD.PropertyID = TEMP.LICENSE_PROPID 
					Where UD.ActivityID = @License_No
				End
			else
				Begin
					Select -1 as [ErrorNumber], 'UDF error or field mismatch between Permit and License. No UDFs copied' as [ErrorMessage]
				End

	End Try
	Begin Catch				
					Select error_number() as [ErrorNumber], error_message() as [ErrorMessage]						
	End Catch	 
	
 END
 

GO
