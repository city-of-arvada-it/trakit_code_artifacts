USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_SysDropGeoChangeLogTriggers]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_SysDropGeoChangeLogTriggers]
GO
/****** Object:  StoredProcedure [dbo].[tsp_SysDropGeoChangeLogTriggers]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  					  
					  					  					  

CREATE Procedure [dbo].[tsp_SysDropGeoChangeLogTriggers]

As

SET NOCOUNT ON

begin
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[Geo_Ownership_ChangeLogTracking]'))
DROP TRIGGER [dbo].[Geo_Ownership_ChangeLogTracking]
end

Begin
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[Geo_OwnershipUDF_ChangeLogTracking]'))
DROP TRIGGER [dbo].[Geo_OwnershipUDF_ChangeLogTracking]
End

Begin
IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N'[dbo].[Geo_UDF_ChangeLogTracking]'))
DROP TRIGGER [dbo].[Geo_UDF_ChangeLogTracking]
End



GO
