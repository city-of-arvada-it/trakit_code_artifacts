USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_UpdateFloodPlanCalcs]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[usp_Arvada_UpdateFloodPlanCalcs]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_UpdateFloodPlanCalcs]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--create table dbo.ArvadaFloodPlainPermits
--(
--	PermitType varchar(50) not null,
--	PermitSubType varchar(50) not null

--	constraint pkc_PermitType_PermitSubType
--		primary key clustered (permitType, permitSubType)
--)
--go
--insert into dbo.ArvadaFloodPlainPermits
--SELECT 'ADU','C'
--UNION ALL SELECT 'ADU','A'
--UNION ALL SELECT 'ADU','B'
--UNION ALL SELECT 'COMMERCIAL','REMODEL-ALTERATION'
--UNION ALL SELECT 'COMMERCIAL','NULL'
--UNION ALL SELECT 'COMMERCIAL','TENANT FINISH'
--UNION ALL SELECT 'COMMERCIAL','ADDITION'
--UNION ALL SELECT 'COMMERCIAL','MISCELLANEOUS'
--UNION ALL SELECT 'COMMERCIAL','PCS'
--UNION ALL SELECT 'COMMISC','MECH/PLUMB/ELEC'
--UNION ALL SELECT 'COMMISC','ELECTRICAL'
--UNION ALL SELECT 'COMMISC','PLUMBING'
--UNION ALL SELECT 'COMMMISC','PLUMBING'
--UNION ALL SELECT 'COMMMISC','MECHANICAL'
--UNION ALL SELECT 'COMMMISC','ELECTRICAL'
--UNION ALL SELECT 'DEMOLITION','MAJOR (BUILDING(S))'
--UNION ALL SELECT 'DEMOLITION','COMMERICAL'
--UNION ALL SELECT 'DEMOLITION','MINOR (INTERIOR)'
--UNION ALL SELECT 'DEMOLITION','NULL'
--UNION ALL SELECT 'DEMOLITION','RESIDENTIAL'
--UNION ALL SELECT 'ELEVATOR','SINGLE FAMILY'
--UNION ALL SELECT 'ELEVATOR','NULL'
--UNION ALL SELECT 'ELEVATOR','COMMERCIAL'
--UNION ALL SELECT 'MISC','DEMO SF'
--UNION ALL SELECT 'MISC','DEMOINT'
--UNION ALL SELECT 'MISC','WINDOWS'
--UNION ALL SELECT 'MISC','MISC'
--UNION ALL SELECT 'MISC','SIDING'
--UNION ALL SELECT 'NEW SINGLE FAMILY','NEW SF DETACHED'
--UNION ALL SELECT 'RESIDENTIAL','DETACHED GARAGE'
--UNION ALL SELECT 'RESIDENTIAL','ADDITION'
--UNION ALL SELECT 'RESIDENTIAL','NEW SFA'
--UNION ALL SELECT 'RESIDENTIAL','MISCELLANEOUS'
--UNION ALL SELECT 'RESIDENTIAL','MOVING'
--UNION ALL SELECT 'RESIDENTIAL','NEW DUPLEX'
--UNION ALL SELECT 'RESIDENTIAL','STRUCTURAL REPAIR'
--UNION ALL SELECT 'RESIDENTIAL','PATIO COVER'
--UNION ALL SELECT 'RESIDENTIAL','NEW SF ATTACHED'
--UNION ALL SELECT 'RESIDENTIAL','REMODEL'
--UNION ALL SELECT 'RESIDENTIAL','NEW SF DETACHED'
--UNION ALL SELECT 'RESIDENTIAL','NEW SINGLE FAMILY'
--UNION ALL SELECT 'RESIDENTIAL','SHED'
--UNION ALL SELECT 'RESIDENTIAL','FOUNDATION REPAIR'
--UNION ALL SELECT 'RESIDENTIAL','DECK'
--UNION ALL SELECT 'RESIDENTIAL','ADU'
--UNION ALL SELECT 'RESIDENTIAL','BASEMENT FINISH'
--UNION ALL SELECT 'RESMISC','ELECTRICAL'
--UNION ALL SELECT 'RESMISC','MECH/PLUMB/ELEC'
--UNION ALL SELECT 'RESMISC','2 OR MORE APPLIANCES'
--UNION ALL SELECT 'RESMISC','LAWN SPRINKLER'
--UNION ALL SELECT 'RESMISC','WATER HEATER'
--UNION ALL SELECT 'RESMISC','MECHANICAL'
--UNION ALL SELECT 'RESMISC','GAS LOG FIRE PLACE'
--UNION ALL SELECT 'RESMISC','AIR CONDITIONER'
--UNION ALL SELECT 'RESMISC','FURNACE'
--UNION ALL SELECT 'RESMISC','APP-SVC CHRG'
--UNION ALL SELECT 'RESMISC','PLUMBING'
--UNION ALL SELECT 'RESMISC','MUL. APP-SVC CHRG'
--UNION ALL SELECT 'RESMISC','EVAPORATIVE COOLER'
--UNION ALL SELECT 'ROOF','NULL'
--UNION ALL SELECT 'ROOF','LAMINATE'
--UNION ALL SELECT 'ROOF','MTL PANELS-ST. SEAM'
--UNION ALL SELECT 'ROOF','METAL-P'
--UNION ALL SELECT 'ROOF','90 ASPHALT ROLL'
--UNION ALL SELECT 'ROOF','WOOD SHAKES'
--UNION ALL SELECT 'ROOF','WOOD SHINGLES'
--UNION ALL SELECT 'ROOF','90LB ASPHALT ROLL'
--UNION ALL SELECT 'ROOF','LIGHT TILE =< 7PSF'
--UNION ALL SELECT 'ROOF','METAL SHINGLES'
--UNION ALL SELECT 'ROOF','SPECIAL ROOF'
--UNION ALL SELECT 'ROOF','HEAVY TILE > 7PSF'
--UNION ALL SELECT 'ROOF','SINGLE PLY ROOF'
--UNION ALL SELECT 'ROOF','COMROOF'
--UNION ALL SELECT 'ROOF','COMMERCIAL ROOF'
--UNION ALL SELECT 'ROOF','MODIFIED'
--UNION ALL SELECT 'ROOF','BUILT UP ROOF'
--UNION ALL SELECT 'ROOF','3-TAB'
--UNION ALL SELECT 'SOLAR','PV-RESIDENTIAL'
--UNION ALL SELECT 'SOLAR','PV-COMMERCIAL'
--UNION ALL SELECT 'SOLAR','HOT WATER-SINGLE'
--UNION ALL SELECT 'SOLAR','HOT WATER-COMMERCIAL'
--UNION ALL SELECT 'TELECOM','ALTERNATIVE TOWER'
--UNION ALL SELECT 'TELECOM','WALL'
--UNION ALL SELECT 'TELECOM','ROOF'

--go



create procedure [dbo].[usp_Arvada_UpdateFloodPlanCalcs]

as

/*
Purpose: There is a law from the state that requires us to add more to a permit in flood zone
The calculation requires us to know the job values for all prior permits (Where the job value was added properly (2014+)
In some cases, if the owner gets their propery value re-assessed we need to be able to override the county assessment
Then if it the sum(jobValue) > 80% of the property value, more things happen
The term for this is substantial improvement/substantial damage (SI/SD). That’s what FEMA refers to this principle as.

Only some permits (those affecting the property) are used in the calc, an audit report to see if we need to add any permits is
	sent out monthly.

exec usp_Arvada_UpdateFloodPlanCalcs
*/

set nocount on

begin
	
	if object_id('tempdb.dbo.#permitValues') is not null drop table #permitValues
	create table #permitValues
	(
		LocRecordID varchar(50),
		JobValueTotal float,
		ImprovedValue float,
		ModifiedPropertyValue float,
		--if we have put in a modified property value, and it is greater than what the county provided, use that
		FinalProperyValue as case when isnull(ModifiedPropertyValue,0) > ImprovedValue then isnull(ModifiedPropertyValue,0) else ImprovedValue end, 
		PctOfPropertyValue decimal(36,2),
		TextField nvarchar(500)
	)

	insert into #permitValues
	(
		LocRecordID,
		JobValueTotal,
		ImprovedValue,
		ModifiedPropertyValue
	)
	select
		o.RECORDID,
		b.JobValue,
		o.IMPROVED_VALUE,
		case when isnumeric(replace(replace(u.BLDGDATA02,'$',''),',','')) = 0 then null else replace(replace(u.BLDGDATA02,'$',''),',','') end
	from dbo.geo_ownership o
	left join dbo.geo_ownershipUDF u
		on u.LOC_RECORDID = o.RECORDID
	left join
	(
		select
			LOC_RECORDID,
			sum(JobValue) as JobValue
		from 
		(
			select 
				m.LOC_RECORDID,
				m.JOBVALUE
			from dbo.ArvadaFloodPlainPermits a
			inner join dbo.permit_main m
				on m.PermitType = a.PermitType
				and m.PermitSubType = a.PermitSubType
			where
				m.issued > '01/01/2014'
				and m.[status] not in ('DENIED','INCOMLT SUBMITTAL','INCOMPLT','NO INSPECTIONS','VOID','WITHDRAWN','APPLIED', 'APPROVED')
			union all
			select 
				m.LOC_RECORDID,
				m.JOBVALUE
			from dbo.ArvadaFloodPlainPermits a
			inner join dbo.permit_main m
				on m.PermitType = a.PermitType 
			where 
				a.PermitSubType = 'all'
				and m.[status] not in ('DENIED','INCOMLT SUBMITTAL','INCOMPLT','NO INSPECTIONS','VOID','WITHDRAWN','APPLIED', 'APPROVED')
				and m.issued > '01/01/2014' 
				
		) as b1
		group by 
			LOC_RECORDID
	) as b
		on b.LOC_RECORDID = o.RECORDID


	--Get total job value and % of property value
	update m
	set PctOfPropertyValue = convert(decimal(36,2), case when FinalProperyValue = 0 then null else JobValueTotal / FinalProperyValue end*100.00)
	from #permitValues m

 
	--set the text string we will show on the Geo Screen
	update m
	set TextField = convert(varchar(50), PctOfPropertyValue) + '%'
	from #permitValues m

	--update geo_ownershipUDF w\ the text string
	update u
	set BLDGDATA04 = TextField,
		BLDGDATA06 = '$'+convert(varchar(20), JobValueTotal)
	from #permitValues m
	inner join dbo.geo_ownershipUDF u
		on m.LocRecordID = u.LOC_RECORDID
	where
		m.TextField is not null

end



--select BLDGDATA02, BLDGDATA04
--from dbo.geo_ownershipUDF
--where LOC_RECORDID = 'CONV:130212080955330085'
GO
