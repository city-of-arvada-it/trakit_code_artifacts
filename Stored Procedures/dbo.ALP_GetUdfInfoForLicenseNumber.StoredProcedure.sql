USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[ALP_GetUdfInfoForLicenseNumber]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[ALP_GetUdfInfoForLicenseNumber]
GO
/****** Object:  StoredProcedure [dbo].[ALP_GetUdfInfoForLicenseNumber]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[ALP_GetUdfInfoForLicenseNumber]

	@LICENSE_NO varchar(30)

AS
BEGIN
 
	-- identify the columns to fetch data for based on the data type for the column
	declare @columnsandtypes table(col varchar(255), datatype varchar(40))
	insert into @columnsandtypes (col,datatype)
	select  sc.name, case when st.name in ('bigint','int','smallint','tinyint') then 'INTEGER' else 'FLOAT' END
	from sys.columns sc inner join sys.types st on sc.system_type_id = st.system_type_id
	where (object_id = object_id('LICENSE2_UDF','U') and sc.name <> 'LICENSE_NO' and sc.name <> 'RECORDID') 
		and st.name in ('bigint','int','smallint','tinyint','decimal','float','money','numeric','real','smallmoney')

	declare @columns table(col varchar(255))
	insert into @columns (col)
	select distinct col from @columnsandtypes 
	-- CREATE UDF LOOKUP TABLE
	declare @UDF_LIST table 
	(
		LICENSE_NO varchar(40) not null, 
		UDF_NAME varchar(255), 
		UDF_VALUE decimal(14,2)		-- cast any numeric value to decimal(14,2)
		
	)


	-- yaaaaaaaay!!  cursor time 
	--------------------------------------------------------------------------------------------------------------------------------
	DECLARE @FieldName NVARCHAR(255)
	DECLARE @cursor CURSOR
	DECLARE @sql nvarchar(500)=''

	SET @cursor = CURSOR FOR SELECT col FROM @columns
	OPEN @cursor
	FETCH NEXT FROM @cursor INTO @FieldName
	WHILE @@FETCH_STATUS = 0
	BEGIN

		set @sql = 'select t.[LICENSE_NO], ''' + @FieldName + ''', cast(t.[' + @FieldName + '] as decimal(14,2)) from LICENSE2_UDF t where LICENSE_NO=''' + @LICENSE_NO + ''''
		print @sql

		-- the insert-exec statement below is why we can't have nice things
		insert into @UDF_LIST (LICENSE_NO, UDF_NAME, UDF_VALUE)
		exec sp_executesql @sql

		FETCH NEXT FROM @cursor INTO @FieldName
	END
	CLOSE @cursor
	DEALLOCATE @cursor
	--------------------------------------------------------------------------------------------------------------------------------
	
	
	-- return the values to the caller
	select distinct 
		   lm.LICENSE_NO [LicenseNumber]
		 , isnull(ptu.Caption, list.[UDF_NAME]) [Caption]
		 , list.[UDF_NAME]
		 , isnull(ptu.DataType, (select datatype from @columnsandtypes where col = list.UDF_NAME) ) [DataType]
		 , list.[UDF_VALUE]

	from @UDF_LIST list 
		left join LICENSE2_MAIN lm on lm.LICENSE_NO=list.LICENSE_NO
		left join 
			(
				select Caption, TypeName, DataType, right(DataField, len(DataField)-2) [UDF_NAME]
				from Prmry_TypesUDF
				where UDF<>'TITL' and GroupName='LICENSE2' and DataType in ('FLOAT','INTEGER') and DataType in ('FLOAT','INTEGER')
			) ptu on (ptu.TypeName = lm.LICENSE_TYPE) and (list.UDF_NAME = ptu.UDF_NAME)

END

GO
