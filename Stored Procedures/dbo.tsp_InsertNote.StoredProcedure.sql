USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_InsertNote]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_InsertNote]
GO
/****** Object:  StoredProcedure [dbo].[tsp_InsertNote]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ======================================================================================
-- Author:		Erick Hampton
-- Create date: June 1, 2017
-- Description:	Inserts note record(s) into Prmry_Notes
--				Returns a pipe delimited string of NoteID values for the inserted row(s)
-- ======================================================================================
CREATE PROCEDURE [dbo].[tsp_InsertNote] (

	@UserID varchar(6) = NULL,
	@ActivityGroup varchar(50) = NULL,
	@ActivityNo varchar(60) = NULL,
	@ActivityRecordID varchar(40) = NULL,
	@SubGroup varchar(50) = NULL,
	@SubGroupRecordID varchar(30) = NULL,
	@Notes varchar(max) = NULL,
	@NoteIdValues varchar(max) = NULL out 
)

AS
BEGIN
SET NOCOUNT ON;

	if @Notes is not null and ltrim(rtrim(@Notes)) <> ''
	begin
		declare @DateEntered datetime = getdate() -- this needs to be the same value for notes that are split across rows so they can be rejoined
		set @NoteIdValues = ''

		if len(@Notes) <= 7500
		begin
			-- just need to insert one row
			insert into Prmry_Notes(UserID, DateEntered, ActivityGroup, ActivityRecordID, ActivityNo, SubGroup, SubGroupRecordID, Notes)
			values(@UserID, @DateEntered, @ActivityGroup, @ActivityRecordID, @ActivityNo, @SubGroup, @SubGroupRecordID,	@Notes)
			set @NoteIdValues = cast(@@IDENTITY as varchar)
		end
		else
		begin
			-- need to insert more than one row
			declare @segment varchar(7500)
			declare @there_are_rows_to_insert bit = 1
			while @there_are_rows_to_insert = 1
			begin
				if len(@Notes) > 7500
				begin
					set @there_are_rows_to_insert = 1				-- there will be another row to insert after this
					set @segment = left(@Notes, 7500)
					set @Notes = right(@Notes, len(@Notes) - 7500)	-- remove the segment from @Notes that we are about to insert 
				end
				else
				begin
					set @there_are_rows_to_insert = 0
					set @segment = @Notes
				end

				-- insert the notes segment
				insert into Prmry_Notes(UserID, DateEntered, ActivityGroup, ActivityRecordID, ActivityNo, SubGroup, SubGroupRecordID, Notes)
				values(@UserID, @DateEntered, @ActivityGroup, @ActivityRecordID, @ActivityNo, @SubGroup, @SubGroupRecordID,	@segment)
				if @NoteIdValues <> '' set @NoteIdValues = @NoteIdValues + '|'
				set @NoteIdValues = @NoteIdValues + cast(@@IDENTITY as varchar)

			end
		end
		--print '@NoteIdValues = ' + isnull(@NoteIdValues,'NULL')
	end

END
GO
