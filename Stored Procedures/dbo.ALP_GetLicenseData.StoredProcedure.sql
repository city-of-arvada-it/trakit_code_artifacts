USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[ALP_GetLicenseData]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[ALP_GetLicenseData]
GO
/****** Object:  StoredProcedure [dbo].[ALP_GetLicenseData]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =====================================================================================================
-- Author:		Erick Hampton
-- Create date: Dec 18, 2016
-- Description:	Get the data from a specified LICENSE2 table or view for a specified license number 
-- =====================================================================================================
CREATE PROCEDURE [dbo].[ALP_GetLicenseData]

		@TableOrViewName varchar(255) = NULL,
	@LicenseNumberColumnName varchar(255) = NULL,
	@LicenseNumber varchar(30) = NULL

AS
BEGIN
	-- DO NOT SET NOCOUNT ON! IT WILL THROW AN ERROR IN C# DATA ADAPTER

-- for security: confirm the desired table and column exist
if exists (select c.name from sys.columns c where c.name=@LicenseNumberColumnName and OBJECT_ID = (select top 1 object_id from sys.objects where name = @TableOrViewName and type in ('U','V')))
begin
	-- fetch
	declare @sql nvarchar(max) 
	
	-- filter tables/views that have a column for ActivityTypeID where ActivityTypeID = 3 (LICENSE2)
	if exists (select c.name from sys.columns c where c.name='ActivityTypeID' and OBJECT_ID = object_id(@TableOrViewName))
		set @sql = 'select * from ' + @TableOrViewName + ' where ' + @LicenseNumberColumnName + ' = '  + '''' + @LicenseNumber + '''' + ' and ActivityTypeID=3'
	else
		set @sql = 'select * from ' + @TableOrViewName + ' where ' + @LicenseNumberColumnName + ' = '  + '''' + @LicenseNumber + ''''
	
	exec sp_executesql @sql
end
else
begin
	-- raise error so caller knows the info is invalid
	raiserror('Invalid data specified',16,1) with nowait
end


END

GO
