USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[ALP_SetBatchStatus]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[ALP_SetBatchStatus]
GO
/****** Object:  StoredProcedure [dbo].[ALP_SetBatchStatus]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===================================================================
-- Author:		Erick Hampton
-- Create date: Dec 11, 2016
-- Description:	Sets the status info for a batch
-- ===================================================================
CREATE PROCEDURE [dbo].[ALP_SetBatchStatus]
	
	@BatchID int = -1,
	@StatusID int = -1

AS
BEGIN

	update [ALP_Batches] set StatusID=@StatusID where ID=@BatchID

END
GO
