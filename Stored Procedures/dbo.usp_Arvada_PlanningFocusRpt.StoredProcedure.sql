USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_PlanningFocusRpt]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[usp_Arvada_PlanningFocusRpt]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_PlanningFocusRpt]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--EXEC dbo.usp_Arvada_PlanningFocusRpt '05/01/2015','05/30/2015','',''

 
CREATE PROCEDURE [dbo].[usp_Arvada_PlanningFocusRpt]
(
	@FromDate DATETIME,
	@ToDate DATETIME,
	@ProjectType VARCHAR(MAX) = '',
	@Planner VARCHAR(MAX) = ''		
) 
AS

BEGIN
  
SET NOCOUNT ON

DECLARE @ProjectCount int,
		@PlannerParamCount INT,
		@ProjectParamCount INT
									
--Create temp tables for each parameter getting passed as "test|"
IF object_id('tempdb.dbo.#Planner')  IS NOT NULL DROP TABLE #Planner
CREATE TABLE #Planner
(
	plannerName VARCHAR(255)
)

IF object_id('tempdb.dbo.#ProjectType') IS NOT NULL DROP TABLE #ProjectType
CREATE TABLE #ProjectType
(
	ProjectType VARCHAR(250)
)

--this table lets us get distinct project counts while returning activity still
IF object_id('tempdb.dbo.#Project') IS NOT NULL DROP TABLE #Project
CREATE TABLE #Project
(
	ProjectNo VARCHAR(15)
)

 
--get params passed
INSERT INTO #ProjectType
( 
	ProjectType 
)
SELECT DISTINCT 
	val
FROM [dbo].[tfn_SSRS_String_To_Table](@ProjectType,'|',1)
 
SELECT @ProjectParamCount = COUNT(1)
FROM #projectType

--if nothing, return everything
IF @ProjectParamCount IS NULL
	SET @ProjectParamCount = 0


INSERT INTO #Planner
( 
	plannerName
)
SELECT DISTINCT 
	val
FROM [dbo].[tfn_SSRS_String_To_Table](@Planner,'|',1)



SELECT @PlannerParamCount = COUNT(1)
FROM #Planner

--if nothing, return everything
IF @PlannerParamCount IS NULL
	SET @PlannerParamCount = 0
IF EXISTS(	SELECT 1
			FROM #Planner p
			WHERE plannerName = 'All Including No Activity'
		)
SET @PlannerParamCount = 0 
 	
--get distinct num of projects
INSERT INTO #Project
( 
	ProjectNo 
)
SELECT
	m.PROJECT_NO
FROM dbo.PROJECT_MAIN AS m
LEFT JOIN #ProjectType pt
	ON pt.ProjectType = m.PROJECTTYPE 
						
WHERE
	m.APPLIED >= @FromDate 
AND m.APPLIED < dateadd(DAY,1,@ToDate)
AND (@PlannerParamCount = 0 
		OR EXISTS(SELECT 1
			 FROM dbo.Project_Actions AS a 
			 INNER JOIN #Planner pl
					ON pl.plannerName = a.ACTION_BY
			WHERE a.action_Type = 'Finalized Initial Review'
						AND a.PROJECT_NO = m.PROJECT_NO
			)
	)
AND (@ProjectParamCount = 0 OR pt.ProjectType IS NOT NULL)
	  	 

SELECT @ProjectParamCount = count(1)
FROM #Project

IF @ProjectParamCount IS NULL
	SET @ProjectParamCount = 0

  

--Return all data
SELECT
	m.PROJECT_NO,
	m.[PROJECT_NAME],
	m.SITE_ADDR,
	m.PROJECTTYPE,
	m.applied AS AppliedDate,
	m.PLANNER AS Planner,
	a.ACTION_TYPE,
	a.ACTION_BY,
	a.ACTION_DATE,
	a.COMPLETED_DATE,
	 stuff(
			 (select CHAR(13) + pn.notes 
			  from dbo.Prmry_Notes AS pn  
			  WHERE  pn.activityrecordid = m.RECORDID
					AND pn.SubGroupRecordID = a.recordid
			  for xml path(''), root('MyString'), type 
			  ).value('/MyString[1]','nvarchar(max)') ,1,1,'') AS ACTION_DESCRIPTION,
	DATEDIFF(DAY,m.APPLIED, a.COMPLETED_DATE) AS DayDifference,
	1 AS RowCountValue,
	(CASE WHEN DATEDIFF(DAY,m.APPLIED, a.COMPLETED_DATE) <= 28 THEN 1 ELSE 0 END) AS CompletedLessThan28Days
FROM dbo.PROJECT_MAIN AS m
INNER JOIN #Project p
	ON p.ProjectNo = m.PROJECT_NO
LEFT JOIN 
	(SELECT
			a.PROJECT_NO,
			a.ACTION_TYPE,
			a.ACTION_BY,
			a.ACTION_DATE,
			a.COMPLETED_DATE,
			a.ACTION_DESCRIPTION,
			a.RECORDID
	FROM dbo.Project_Actions AS a
	INNER JOIN #Project p
		ON p.ProjectNo = a.PROJECT_NO 
	LEFT JOIN #Planner pl
			ON pl.plannerName = a.ACTION_BY
	WHERE a.action_Type = 'Finalized Initial Review'
		AND (@PlannerParamCount = 0
				OR pl.plannerName IS NOT NULL)
	) AS a
	ON a.project_no= m.PROJECT_NO
--LEFT JOIN dbo.Prmry_Notes AS pn  
--	ON pn.activityrecordid = m.RECORDID
--		AND pn.SubGroupRecordID = a.recordid
ORDER BY 
	AppliedDate,
	project_no 
	 

 

 END
GO
