USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ModelHomeBuilder_Delete]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_ModelHomeBuilder_Delete]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ModelHomeBuilder_Delete]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  
CREATE PROCEDURE [dbo].[tsp_T9_ModelHomeBuilder_Delete] 
		@BuilderRecID varchar(10)  = '0'
AS
BEGIN

	Declare @SQL Varchar(8000)
	
	if @BuilderRecID <> '0'
		Begin
			--Delete any template detail records
			set @SQL = 'DELETE from Prmry_ModelHomeTemplateDetail WHERE TemplateRecordID in (select Recordid From			Prmry_ModelHomeTemplateItem where BuilderRecordid = ' + @BuilderRecID + ');'
					
			print(@sql)
			exec(@sql)

        end

		Begin
			--Delete any template item records
			set @SQL = 'DELETE from Prmry_ModelHomeTemplateItem WHERE (BuilderRecordID = ' + @BuilderRecID + ');'
					
			print(@sql)
			exec(@sql)

        end


		Begin

			set @SQL = 'DELETE from Prmry_ModelHomeBuilder WHERE (RecordID = ' + @BuilderRecID + ');'


			print(@sql)
			exec(@sql)

		end

END





GO
