USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_CRMRestrTypeAccess]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_CRMRestrTypeAccess]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_CRMRestrTypeAccess]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  					  
/*
	Fetches CRM types to which this UserID is having restriction
*/
-- =====================================================================
-- Created By: 
--	HAN - 11/22/13
-- =====================================================================  
CREATE Procedure [dbo].[tsp_T9_CRMRestrTypeAccess](
	@UserID VARCHAR(50)
)
AS
BEGIN
	SELECT DISTINCT TYPENAME  FROM PRMRY_CRM_RESTR_TYPE_ACCESS WHERE TYPENAME NOT IN (SELECT TYPENAME  FROM PRMRY_CRM_RESTR_TYPE_ACCESS WHERE USERID IN (@UserID))
END




GO
