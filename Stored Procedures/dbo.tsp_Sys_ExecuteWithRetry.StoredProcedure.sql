USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_Sys_ExecuteWithRetry]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_Sys_ExecuteWithRetry]
GO
/****** Object:  StoredProcedure [dbo].[tsp_Sys_ExecuteWithRetry]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  
CREATE procedure [dbo].[tsp_Sys_ExecuteWithRetry]
	@paramSQLString AS  NVARCHAR(max)
AS
BEGIN

DECLARE @SQLString AS  NVARCHAR(max)
DECLARE @retry int
DECLARE @success int 

SET @SQLString = @paramSQLString
set @retry = 0 
set @success = 0 


WHILE (@retry < 3)
BEGIN 
	SET @retry = @retry + 1

	SELECT 'TRY = ' + CONVERT(VARCHAR, @retry)
	BEGIN TRY
		EXECUTE sp_executesql @SQLString
		SET @success = 1
		BREAK
	END TRY
	BEGIN CATCH 
		WAITFOR DELAY '00:00:01'
	END CATCH 
END 

IF @success = 0
BEGIN
	RAISERROR('TRANSACTION INCOMPLETE', 16, 1)
END 

END



GO
