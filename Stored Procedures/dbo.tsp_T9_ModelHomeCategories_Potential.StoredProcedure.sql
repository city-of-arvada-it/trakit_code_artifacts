USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ModelHomeCategories_Potential]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_ModelHomeCategories_Potential]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ModelHomeCategories_Potential]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  
CREATE PROCEDURE [dbo].[tsp_T9_ModelHomeCategories_Potential]
       @code as varchar(255)                     
AS
BEGIN

		Declare @SQL Varchar(8000)
       

	if @code <> '' 
		begin
			 set @SQL = 'select description, code, UNITCOST, orderid from Prmry_ValuationSchedule where code not in (select valuationcode from Prmry_ModelHomeValuationCategory) and units = ''EA'' and levelno = 1 and code = ''' +  @code + ''';'
			 exec(@sql)
		end
	else

		select description, code, UNITCOST, orderid from Prmry_ValuationSchedule where code not in (select valuationcode from Prmry_ModelHomeValuationCategory) and units = 'EA' and levelno = 1


END







GO
