USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AttachmentIsHighestRevision]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_AttachmentIsHighestRevision]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AttachmentIsHighestRevision]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  

-- =============================================
-- Author:           <Micah Neveu>
-- Create date: <7/27/2015>
-- Description:      <Determines if the supplied attachmentid is the latest revision or not>
-- =============================================
CREATE PROCEDURE [dbo].[tsp_T9_AttachmentIsHighestRevision] 
       @GroupName varchar(20),
       @AttachmentRecordID varchar(30),
       @BaseFilename varchar(250),
       @ActivityNumber varchar(50), 
       @IsHighestVersion bit OUT

AS
BEGIN
       -- SET NOCOUNT ON added to prevent extra result sets from
       -- interfering with SELECT statements.
       SET NOCOUNT ON;

declare @myversionnumber int = 0 --= (select [version] from Permit_Attachments where RECORDID = @attachmentRecordID) -- local variable
declare @highestVersion int = 0 --= (select MAX([version])as VersionNumber from Permit_Attachments where PATHNAME like @basefilename and PERMIT_NO = @activityNumber)

-- default ishighest to 0
select @isHighestVersion = 0

IF @GroupName = 'AEC'
       BEGIN
              SELECT @myversionnumber = (select [version] from AEC_Attachments where RECORDID = @attachmentRecordID)
              SELECT @highestVersion = (select MAX([version])as VersionNumber from AEC_Attachments where PATHNAME like @basefilename and ST_LIC_NO = @activityNumber)  
       END

IF @GroupName = 'CASE'
       BEGIN
              SELECT @myversionnumber = (select [version] from Case_Attachments where RECORDID = @attachmentRecordID)
              SELECT @highestVersion = (select MAX([version])as VersionNumber from Case_Attachments where PATHNAME like @basefilename and CASE_NO = @activityNumber)    
       END

IF @GroupName = 'CRM'
       BEGIN
              -- CRM uses a NUMBER for issue_id, not stringiness
              DECLARE @ISSUE_ID INT = CAST(@activityNumber AS INT)
              SELECT @myversionnumber = (select [version] from CRM_Attachments where RECORDID = @attachmentRecordID)
              SELECT @highestVersion = (select MAX([version])as VersionNumber from CRM_Attachments where PATHNAME like @basefilename and ISSUE_ID = @activityNumber)   
       END

IF @GroupName = 'GEO'
       BEGIN
              SELECT @myversionnumber = (select [version] from GEO_Attachments where RECORDID = @attachmentRecordID)
              SELECT @highestVersion = (select MAX([version])as VersionNumber from GEO_Attachments where PATHNAME like @basefilename and LOC_RECORDID = @activityNumber)      
       END

IF @GroupName = 'LAND'
       BEGIN
              SELECT @myversionnumber = (select [version] from GEO_Attachments where RECORDID = @attachmentRecordID)
              SELECT @highestVersion = (select MAX([version])as VersionNumber from GEO_Attachments where PATHNAME like @basefilename and LOC_RECORDID = @activityNumber)      
       END
       
IF @GroupName = 'LICENSE2'
       BEGIN
              SELECT @myversionnumber = (select [version] from License2_Attachments where RECORDID = @attachmentRecordID)
              SELECT @highestVersion = (select MAX([version])as VersionNumber from License2_Attachments where PATHNAME like @basefilename and LICENSE_NO = @activityNumber)
       END

IF @GroupName = 'PAYMENT'
       BEGIN
              SELECT @myversionnumber = (select [version] from Payment_Attachments where RECORDID = @attachmentRecordID)
              SELECT @highestVersion = (select MAX([version])as VersionNumber from Payment_Attachments where PATHNAME like @basefilename and Activity_NO = @activityNumber) 
       END

IF @GroupName = 'PERMIT'
       BEGIN
              SELECT @myversionnumber = (select [version] from Permit_Attachments where RECORDID = @attachmentRecordID)
              SELECT @highestVersion = (select MAX([version])as VersionNumber from Permit_Attachments where PATHNAME like @basefilename and PERMIT_NO = @activityNumber)
       END

IF @GroupName = 'PROJECT'
       BEGIN
              SELECT @myversionnumber = (select [version] from Project_Attachments where RECORDID = @attachmentRecordID)
              SELECT @highestVersion = (select MAX([version])as VersionNumber from Project_Attachments where PATHNAME like @basefilename and PROJECT_NO = @activityNumber)
       END

if (@myversionnumber = @highestVersion)
       BEGIN
              select @isHighestVersion = 1      
       END

SELECT @isHighestVersion as 'Is Highest'

END




GO
