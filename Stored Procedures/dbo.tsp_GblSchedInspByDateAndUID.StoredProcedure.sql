USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GblSchedInspByDateAndUID]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_GblSchedInspByDateAndUID]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GblSchedInspByDateAndUID]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  					  
					  					  					  

/*
Revision History:
mdp - 11/5/13



tsp_GblSchedInspByDateAndUID 'MDP','4/2/2013'

*/



CREATE Procedure [dbo].[tsp_GblSchedInspByDateAndUID](
@InspectorID Varchar(6) = Null,
@StartDate varchar(30)= Null
)

As

set nocount on

--RB Check if table exists before creating it
   IF OBJECT_ID('tempdb..#CombinedInspections') IS NOT NULL  
	drop table #CombinedInspections

Create Table #CombinedInspections(
TGroup Varchar(15),
ActivityNo Varchar(60),
CAPOVERRIDE Varchar(4),
COMPLETED_DATE    varchar(50),
COMPLETED_TIME    varchar(10),
CREATED_BY  varchar(20),
CREATED_DATE      varchar(50),
CREATED_TIME      varchar(10),
DURATION    varchar(20),
DURATION_EST      varchar(20),
INSPECTIONTYPE    varchar(60),
INSPECTOR   varchar(20),
LOCKID      varchar(30),
NOTES varchar(2000),
RECORDID    varchar(30),
REMARKS     varchar(200),
RESULT      varchar(200),
SCHEDULED_DATE    varchar(50),
SCHEDULED_TIME    varchar(10),
SEQID varchar(10),
LOC_RECORDID varchar(30),
SITE_APN varchar(60),
GEOTYPE varchar(60),
SITE_ALTERNATE_ID varchar(60),
SITE_ADDR varchar(200),
SITE_CITY varchar(200),
SITE_STATE varchar(50),
SITE_ZIP varchar(10),
CONTACT_NAME varchar(200),
CONTACT_ADDRESS1 varchar(200),
CONTACT_CITY varchar(200),
CONTACT_STATE varchar(50),
CONTACT_ZIP varchar(10),
CONTACT_PHONE varchar(50),
PATHURL varchar(500),
PATHURLDATEADDED varchar(50),
ISVOIDED int ,
ISTODAY int ,
ISPASTDUE int ,
ATTACHMENTCOUNT int,
X Real,
Y Real,
LAT real,
LON REAL
)

Insert Into #CombinedInspections(
TGroup, 
ActivityNo,
CAPOVERRIDE,
COMPLETED_DATE,
COMPLETED_TIME,
CREATED_BY,
CREATED_DATE,
CREATED_TIME,
DURATION ,
DURATION_EST,
INSPECTIONTYPE,
INSPECTOR,
LOCKID,
NOTES,
RECORDID,
REMARKS,
RESULT,
SCHEDULED_DATE,
SCHEDULED_TIME,
SEQID,
LOC_RECORDID,
SITE_APN,
GEOTYPE,
SITE_ALTERNATE_ID,
SITE_ADDR,
SITE_CITY,
SITE_STATE,
SITE_ZIP,
CONTACT_NAME,
CONTACT_ADDRESS1,
CONTACT_CITY,
CONTACT_STATE,
CONTACT_ZIP,
CONTACT_PHONE,
PATHURL,
PATHURLDATEADDED,
ISVOIDED,
ISTODAY,
ISPASTDUE,
ATTACHMENTCOUNT,
X,
Y,
LAT,
LON

)
SELECT     'CASE' AS Tgroup, CI.CASE_NO AS ACTIVITYNO, CAST(CI.CAPOVERRIDE AS varchar(10)) AS CAPOVERRIDE, 
                      CONVERT(varchar(50), CI.COMPLETED_DATE, 101) AS COMPLETED_DATE, CI.COMPLETED_TIME, 
                      CI.CREATED_BY, CONVERT(varchar(50), CI.CREATED_DATE, 101) AS CREATED_DATE, CI.CREATED_TIME, 
                      CAST(CI.DURATION AS varchar(10)) AS DURATION, CAST(CI.DURATION_EST AS varchar(10)) AS DURATION_EST, 
                      CI.InspectionType AS INSPECTIONTYPE, CI.INSPECTOR, CI.LOCKID, CI.NOTES, 
                      CI.RECORDID, CI.REMARKS, CI.RESULT, CONVERT(varchar(50), CI.SCHEDULED_DATE, 101)
                       AS SCHEDULED_DATE, CI.SCHEDULED_TIME, CAST(CI.SEQID AS varchar(10)) AS SEQID, '' as LOC_RECORDID, '' AS SITE_APN, '' AS GEOTYPE, 
                      '' AS SITE_ALTERNATE_ID, '' AS SITE_ADDR, '' AS SITE_CITY, '' AS SITE_STATE, '' AS SITE_ZIP, '' AS CONTACT_NAME, '' AS CONTACT_ADDRESS1, 
                      '' AS CONTACT_CITY, '' AS CONTACT_STATE, '' AS CONTACT_ZIP, '' AS CONTACT_PHONE, 
                      CI.PATHURL AS PATHURL,
                      CONVERT(varchar(50), CI.PATHURLDATEADDED, 101) AS PATHURLDATEADDED,
                      0 AS ISVOIDED, 0 AS ISTODAY, 0 AS ISPASTDUE, 0 AS ATTACHMENTCOUNT, 0 as X, 0 as Y, 0 as LAT, 0 as LON
FROM         CASE_INSPECTIONS CI INNER JOIN CASE_MAIN CM ON (CI.CASE_NO = CM.CASE_NO)
WHERE     (isdate(CI.COMPLETED_DATE) = 0) AND (CI.INSPECTOR = @InspectorID) 
					AND CM.CLOSED Is Null AND
                      (CI.SCHEDULED_DATE <= CONVERT(DATETIME,@StartDate, 102))   

UNION
SELECT     'LICENSE2' AS Tgroup, L2I.LICENSE_NO AS ACTIVITYNO, CAST(L2I.CAPOVERRIDE AS varchar(10)) AS CAPOVERRIDE, 
                      CONVERT(varchar(50), L2I.COMPLETED_DATE, 101) AS COMPLETED_DATE, L2I.COMPLETED_TIME, 
                      L2I.CREATED_BY, CONVERT(varchar(50), L2I.CREATED_DATE, 101) AS CREATED_DATE, L2I.CREATED_TIME, 
                      CAST(L2I.DURATION AS varchar(10)) AS DURATION, CAST(L2I.DURATION_EST AS varchar(10)) AS DURATION_EST, 
                      L2I.InspectionType AS INSPECTIONTYPE, L2I.INSPECTOR, L2I.LOCKID, L2I.NOTES, 
                      L2I.RECORDID, L2I.REMARKS, L2I.RESULT, CONVERT(varchar(50), L2I.SCHEDULED_DATE, 101)
                       AS SCHEDULED_DATE, L2I.SCHEDULED_TIME, CAST(L2I.SEQID AS varchar(10)) AS SEQID, '' as LOC_RECORDID, '' AS SITE_APN, '' AS GEOTYPE, 
                      '' AS SITE_ALTERNATE_ID, '' AS SITE_ADDR, '' AS SITE_CITY, '' AS SITE_STATE, '' AS SITE_ZIP, '' AS CONTACT_NAME, '' AS CONTACT_ADDRESS1, 
                      '' AS CONTACT_CITY, '' AS CONTACT_STATE, '' AS CONTACT_ZIP, '' AS CONTACT_PHONE, 
                      L2I.PATHURL AS PATHURL,
                      CONVERT(varchar(50), L2I.PATHURLDATEADDED, 101) AS PATHURLDATEADDED,
                      0 AS ISVOIDED, 0 AS ISTODAY, 0 AS ISPASTDUE, 0 AS ATTACHMENTCOUNT, 0 as X, 0 as Y, 0 as LAT, 0 as LON
FROM         LICENSE2_INSPECTIONS L2I INNER JOIN LICENSE2_MAIN L2M ON (L2I.LICENSE_NO = L2M.LICENSE_NO)
WHERE     (isdate(L2I.COMPLETED_DATE) = 0) AND (L2I.INSPECTOR = @InspectorID) AND 
                      (L2I.SCHEDULED_DATE <= CONVERT(DATETIME,@StartDate, 102))  
                   
UNION
SELECT     'LICENSE' AS Tgroup, LI.BUS_LIC_NO AS ACTIVITYNO, CAST(LI.CAPOVERRIDE AS varchar(10)) AS CAPOVERRIDE, 
                      CONVERT(varchar(50), LI.COMPLETED_DATE, 101) AS COMPLETED_DATE, LI.COMPLETED_TIME, 
                      LI.CREATED_BY, CONVERT(varchar(50), LI.CREATED_DATE, 101) AS CREATED_DATE, 
                      LI.CREATED_TIME, CAST(LI.DURATION AS varchar(10)) AS DURATION, 
                      CAST(LI.DURATION_EST AS varchar(10)) AS DURATION_EST, LI.InspectionType AS INSPECTIONTYPE, 
                      LI.INSPECTOR, LI.LOCKID, LI.NOTES, LI.RECORDID, 
                      LI.REMARKS, LI.RESULT, CONVERT(varchar(50), LI.SCHEDULED_DATE, 101) 
                      AS SCHEDULED_DATE, LI.SCHEDULED_TIME, CAST(LI.SEQID AS varchar(10)) AS SEQID, '' as LOC_RECORDID, '' AS SITE_APN, '' AS GEOTYPE, 
                      '' AS SITE_ALTERNATE_ID, '' AS SITE_ADDR, '' AS SITE_CITY, '' AS SITE_STATE, '' AS SITE_ZIP, '' AS CONTACT_NAME, '' AS CONTACT_ADDRESS1, 
                      '' AS CONTACT_CITY, '' AS CONTACT_STATE, '' AS CONTACT_ZIP, '' AS CONTACT_PHONE, 
                      LI.PATHURL AS PATHURL,
                      CONVERT(varchar(50), LI.PATHURLDATEADDED, 101) AS PATHURLDATEADDED,
                      0 AS ISVOIDED, 0 AS ISTODAY, 0 AS ISPASTDUE, 0 AS ATTACHMENTCOUNT, 0 as X, 0 as Y, 0 as LAT, 0 as LON
FROM         LICENSE_INSPECTIONS LI INNER JOIN LICENSE_MAIN LM ON (LI.BUS_LIC_NO = LM.BUS_LIC_NO)
WHERE     (isdate(LI.COMPLETED_DATE) = 0) AND (LI.INSPECTOR = @InspectorID) AND 
                      (LI.SCHEDULED_DATE <= CONVERT(DATETIME, @StartDate, 102))
UNION
SELECT     'PERMIT' AS Tgroup, PI.PERMIT_NO AS ACTIVITYNO, CAST(CAPOVERRIDE AS varchar(10)) AS CAPOVERRIDE, CONVERT(varchar(50), COMPLETED_DATE, 101) 
                      AS COMPLETED_DATE, COMPLETED_TIME, CREATED_BY, CONVERT(varchar(50), CREATED_DATE, 101) AS CREATED_DATE, CREATED_TIME, 
                      CAST(DURATION AS varchar(10)) AS DURATION, CAST(DURATION_EST AS varchar(10)) AS DURATION_EST, InspectionType AS INSPECTIONTYPE, INSPECTOR, 
                      PI.LOCKID, PI.NOTES, PI.RECORDID, REMARKS, RESULT, CONVERT(varchar(50), SCHEDULED_DATE, 101) AS SCHEDULED_DATE, SCHEDULED_TIME, 
                      CAST(SEQID AS varchar(10)) AS SEQID, '' as LOC_RECORDID, '' AS SITE_APN, '' AS GEOTYPE, 
                      '' AS SITE_ALTERNATE_ID, '' AS SITE_ADDR, '' AS SITE_CITY, '' AS SITE_STATE, '' AS SITE_ZIP, '' AS CONTACT_NAME, '' AS CONTACT_ADDRESS1, 
                      '' AS CONTACT_CITY, '' AS CONTACT_STATE, '' AS CONTACT_ZIP, '' AS CONTACT_PHONE, 
                      PATHURL AS PATHURL,
                      CONVERT(varchar(50), PATHURLDATEADDED, 101) AS PATHURLDATEADDED,
                      0 AS ISVOIDED, 0 AS ISTODAY, 0 AS ISPASTDUE, 0 AS ATTACHMENTCOUNT, 0 as X, 0 as Y, 0 as LAT, 0 as LON
FROM         Permit_Inspections PI INNER JOIN Permit_Main PM ON (PI.PERMIT_NO = PM.PERMIT_NO)
WHERE     (isdate(PI.COMPLETED_DATE) = 0) AND (PI.INSPECTOR = @InspectorID) AND 
                      (PI.SCHEDULED_DATE <= CONVERT(DATETIME, @StartDate, 102))
UNION
SELECT     'PROJECT' AS Tgroup, PROJECT_INSPECTIONS.PROJECT_NO AS ACTIVITYNO, CAST(CAPOVERRIDE AS varchar(10)) AS CAPOVERRIDE, CONVERT(varchar(50), COMPLETED_DATE, 101) 
                      AS COMPLETED_DATE, COMPLETED_TIME, CREATED_BY, CONVERT(varchar(50), CREATED_DATE, 101) AS CREATED_DATE, CREATED_TIME, 
                      CAST(DURATION AS varchar(10)) AS DURATION, CAST(DURATION_EST AS varchar(10)) AS DURATION_EST, InspectionType AS INSPECTIONTYPE, INSPECTOR, 
                      PROJECT_INSPECTIONS.LOCKID, NOTES, PROJECT_INSPECTIONS.RECORDID, REMARKS, RESULT, CONVERT(varchar(50), SCHEDULED_DATE, 101) AS SCHEDULED_DATE, SCHEDULED_TIME, 
                      CAST(SEQID AS varchar(10)) AS SEQID, '' as LOC_RECORDID, '' AS SITE_APN, '' AS GEOTYPE, '' AS SITE_ALTERNATE_ID, '' AS SITE_ADDR, '' AS SITE_CITY, '' AS SITE_STATE, 
                      '' AS SITE_ZIP, '' AS CONTACT_NAME, '' AS CONTACT_ADDRESS1, '' AS CONTACT_CITY, '' AS CONTACT_STATE, '' AS CONTACT_ZIP, '' AS CONTACT_PHONE, 
                      PATHURL AS PATHURL,
                      CONVERT(varchar(50), PATHURLDATEADDED, 101) AS PATHURLDATEADDED,
                      0 AS ISVOIDED, 0 AS ISTODAY, 0 AS ISPASTDUE, 0 AS ATTACHMENTCOUNT, 0 as X, 0 as Y, 0 as LAT, 0 as LON
FROM         PROJECT_INSPECTIONS INNER JOIN Project_Main ON (PROJECT_INSPECTIONS.PROJECT_NO = Project_Main.PROJECT_NO)
WHERE     (isdate(PROJECT_INSPECTIONS.COMPLETED_DATE) = 0) AND (PROJECT_INSPECTIONS.INSPECTOR = @InspectorID) AND 
                      (PROJECT_INSPECTIONS.SCHEDULED_DATE <= CONVERT(DATETIME, @StartDate, 102))

UNION
SELECT     'GEO' AS Tgroup, GI.Loc_RecordID AS ACTIVITYNO, CAST(GI.CAPOVERRIDE AS varchar(10)) AS CAPOVERRIDE, CONVERT(varchar(50), GI.COMPLETED_DATE, 101) 
                      AS COMPLETED_DATE, GI.COMPLETED_TIME, GI.CREATED_BY, CONVERT(varchar(50), GI.CREATED_DATE, 101) AS CREATED_DATE, GI.CREATED_TIME, 
                      CAST(GI.DURATION AS varchar(10)) AS DURATION, CAST(GI.DURATION_EST AS varchar(10)) AS DURATION_EST, GI.InspectionType AS INSPECTIONTYPE, 
                      GI.INSPECTOR, GI.LOCKID, GI.NOTES, GI.RECORDID, GI.REMARKS, GI.RESULT, CONVERT(varchar(50), GI.SCHEDULED_DATE, 101) AS SCHEDULED_DATE, 
                      GI.SCHEDULED_TIME, CAST(GI.SEQID AS varchar(10)) AS SEQID, '' as LOC_RECORDID, '' AS SITE_APN, '' AS GEOTYPE, '' AS SITE_ALTERNATE_ID, '' AS SITE_ADDR, '' AS SITE_CITY, '' AS SITE_STATE, 
                      '' AS SITE_ZIP, '' AS CONTACT_NAME, '' AS CONTACT_ADDRESS1, '' AS CONTACT_CITY, '' AS CONTACT_STATE, '' AS CONTACT_ZIP, '' AS CONTACT_PHONE, 
                      GI.PATHURL AS PATHURL, CONVERT(varchar(50), GI.PATHURLDATEADDED, 101) AS PATHURLDATEADDED,
                      0 AS ISVOIDED, 0 AS ISTODAY, 0 AS ISPASTDUE, 0 AS ATTACHMENTCOUNT, 0 as X, 0 as Y, 0 as LAT, 0 as LON
FROM         GEO_INSPECTIONS GI INNER JOIN Geo_Ownership GEO ON (GI.LOC_RECORDID = GEO.RECORDID)
WHERE     (isdate(GI.COMPLETED_DATE) = 0) AND (GI.INSPECTOR = @InspectorID) AND 
                      (GI.SCHEDULED_DATE <= CONVERT(DATETIME, @StartDate, 102))
                      
                      

--Update isvoided
update #CombinedInspections set ISVOIDED = 1 where REMARKS like '%voided%'     
delete #CombinedInspections where ISVOIDED = 1

--Update istoday
update #CombinedInspections set ISTODAY = 1 where convert(datetime,SCHEDULED_DATE,101) = convert(datetime,GETDATE(),101)

--Update ispastdue
update #CombinedInspections set ISPASTDUE = 1 where convert(datetime,SCHEDULED_DATE,101) < convert(datetime,GETDATE(),101)                  

--Update Site_info from _Main tables
update #CombinedInspections set SITE_ADDR = Case_Main.SITE_ADDR, SITE_CITY= Case_Main.SITE_CITY, SITE_STATE= Case_Main.SITE_STATE, SITE_ZIP = Case_Main.SITE_ZIP from Case_Main where Case_Main.CASE_NO = #CombinedInspections.ActivityNo and #CombinedInspections.TGroup = 'CASE' 
update #CombinedInspections set SITE_ADDR = License_Business.LOC_ADDRESS1, SITE_CITY= License_Business.LOC_CITY, SITE_STATE= License_Business.LOC_STATE, SITE_ZIP = License_Business.LOC_ZIP FROM #CombinedInspections INNER JOIN
                      License_Main ON #CombinedInspections.ActivityNo = License_Main.BUS_LIC_NO INNER JOIN
                      License_Business ON License_Main.BUSINESS_NO = License_Business.BUSINESS_NO and #CombinedInspections.TGroup = 'LICENSE' 
update #CombinedInspections set SITE_ADDR = Permit_Main.SITE_ADDR, SITE_CITY= Permit_Main.SITE_CITY, SITE_STATE= Permit_Main.SITE_STATE, SITE_ZIP = Permit_Main.SITE_ZIP from Permit_Main where Permit_Main.PERMIT_NO = #CombinedInspections.ActivityNo and #CombinedInspections.TGroup = 'PERMIT' 
update #CombinedInspections set SITE_ADDR = Project_Main.SITE_ADDR, SITE_CITY= Project_Main.SITE_CITY, SITE_STATE= Project_Main.SITE_STATE, SITE_ZIP = Project_Main.SITE_ZIP from Project_Main where Project_Main.Project_NO = #CombinedInspections.ActivityNo and #CombinedInspections.TGroup = 'PROJECT' 
update #CombinedInspections set SITE_ADDR = License2_Main.SITE_ADDR, SITE_CITY= License2_Main.SITE_CITY, SITE_STATE= License2_Main.SITE_STATE, SITE_ZIP = License2_Main.SITE_ZIP from License2_Main where License2_Main.LICENSE_NO = #CombinedInspections.ActivityNo and #CombinedInspections.TGroup = 'LICENSE2' 

--Update Location Recordid from _Main tables
update #CombinedInspections set LOC_RECORDID = Case_Main.LOC_RECORDID from Case_Main where Case_Main.CASE_NO = #CombinedInspections.ActivityNo and #CombinedInspections.TGroup = 'CASE'
update #CombinedInspections set LOC_RECORDID = License_Business.LOC_RECORDID FROM #CombinedInspections INNER JOIN
                      License_Main ON #CombinedInspections.ActivityNo = License_Main.BUS_LIC_NO INNER JOIN
                      License_Business ON License_Main.BUSINESS_NO = License_Business.BUSINESS_NO and #CombinedInspections.TGroup = 'LICENSE'
update #CombinedInspections set LOC_RECORDID = Permit_Main.LOC_RECORDID from Permit_Main where Permit_Main.PERMIT_NO = #CombinedInspections.ActivityNo and #CombinedInspections.TGroup = 'PERMIT'
update #CombinedInspections set LOC_RECORDID = Project_Main.LOC_RECORDID from Project_Main where Project_Main.Project_NO = #CombinedInspections.ActivityNo and #CombinedInspections.TGroup = 'PROJECT'
update #CombinedInspections set LOC_RECORDID = Geo_Ownership.RECORDID from Geo_Ownership where Geo_Ownership.RecordID = #CombinedInspections.ActivityNo and #CombinedInspections.TGroup = 'GEO'
update #CombinedInspections set LOC_RECORDID = License2_Main.LOC_RECORDID from License2_Main where License2_Main.LICENSE_NO = #CombinedInspections.ActivityNo and #CombinedInspections.TGroup = 'LICENSE2'


                     
--Update site info from geo using populated Loc_Recordid
update #CombinedInspections set SITE_APN = GEO_OWNERSHIP.SITE_APN From GEO_OWNERSHIP where GEO_OWNERSHIP.RECORDID = #CombinedInspections.LOC_RECORDID and LEN(GEO_OWNERSHIP.SITE_APN) > 0
update #CombinedInspections set GEOTYPE = GEO_OWNERSHIP.GEOTYPE From GEO_OWNERSHIP where GEO_OWNERSHIP.RECORDID = #CombinedInspections.LOC_RECORDID and LEN(GEO_OWNERSHIP.GEOTYPE) > 0
update #CombinedInspections set SITE_ALTERNATE_ID = GEO_OWNERSHIP.SITE_ALTERNATE_ID From GEO_OWNERSHIP where GEO_OWNERSHIP.RECORDID = #CombinedInspections.LOC_RECORDID and LEN(GEO_OWNERSHIP.SITE_ALTERNATE_ID) > 0

--Changed to use Activity Address only per Nathan 4/1/2013 except for Geo Inspections
update #CombinedInspections set SITE_ADDR = GEO_OWNERSHIP.SITE_ADDR From GEO_OWNERSHIP where GEO_OWNERSHIP.RECORDID = #CombinedInspections.LOC_RECORDID and LEN(GEO_OWNERSHIP.SITE_ADDR) > 0 and #CombinedInspections.TGroup = 'GEO'
update #CombinedInspections set SITE_CITY = GEO_OWNERSHIP.SITE_CITY From GEO_OWNERSHIP where GEO_OWNERSHIP.RECORDID = #CombinedInspections.LOC_RECORDID and LEN(GEO_OWNERSHIP.SITE_CITY) > 0 and #CombinedInspections.TGroup = 'GEO'
update #CombinedInspections set SITE_STATE = GEO_OWNERSHIP.SITE_STATE From GEO_OWNERSHIP where GEO_OWNERSHIP.RECORDID = #CombinedInspections.LOC_RECORDID and LEN(GEO_OWNERSHIP.SITE_STATE) > 0 and #CombinedInspections.TGroup = 'GEO'
update #CombinedInspections set SITE_ZIP = GEO_OWNERSHIP.SITE_ZIP From GEO_OWNERSHIP where GEO_OWNERSHIP.RECORDID = #CombinedInspections.LOC_RECORDID and LEN(GEO_OWNERSHIP.SITE_ZIP) > 0 and #CombinedInspections.TGroup = 'GEO'

--added for xy & lat lons
update #CombinedInspections set X = GEO_OWNERSHIP.COORD_X From GEO_OWNERSHIP where GEO_OWNERSHIP.RECORDID = #CombinedInspections.LOC_RECORDID and GEO_OWNERSHIP.COORD_X IS NOT NULL
update #CombinedInspections set Y = GEO_OWNERSHIP.COORD_Y From GEO_OWNERSHIP where GEO_OWNERSHIP.RECORDID = #CombinedInspections.LOC_RECORDID and GEO_OWNERSHIP.COORD_Y IS NOT NULL
update #CombinedInspections set LAT = GEO_OWNERSHIP.LAT From GEO_OWNERSHIP where GEO_OWNERSHIP.RECORDID = #CombinedInspections.LOC_RECORDID and GEO_OWNERSHIP.LAT IS NOT NULL 
update #CombinedInspections set LON = GEO_OWNERSHIP.LON From GEO_OWNERSHIP where GEO_OWNERSHIP.RECORDID = #CombinedInspections.LOC_RECORDID and GEO_OWNERSHIP.LON IS NOT NULL

--Give one last attempt to give something for ipad if all other address info fails

Create Table #MaxZip(
SITE_ZIP varchar(10),
ZIPCOUNT int
)

Insert Into #MaxZip(
SITE_ZIP,
ZIPCOUNT
)
SELECT top 1(SITE_ZIP) as SITE_ZIP, COUNT(SITE_ZIP) AS ZIPCOUNT
FROM         Geo_Ownership
GROUP BY SITE_ZIP
ORDER BY ZIPCOUNT DESC

update #CombinedInspections set SITE_ZIP = #MaxZip.SITE_ZIP From #MaxZip where len(#CombinedInspections.SITE_ZIP) < 1


--Update Contacts 
update #CombinedInspections set
CONTACT_NAME = Case_People.NAME,
CONTACT_ADDRESS1 = Case_People.ADDRESS1,
CONTACT_CITY = Case_People.CITY,
CONTACT_STATE = Case_People.STATE,
CONTACT_ZIP = Case_People.ZIP,
CONTACT_PHONE = Case_People.PHONE
from Case_People where Case_People.CASE_NO = #CombinedInspections.ActivityNo
and Case_People.NAMETYPE = 'OWNER'
and #CombinedInspections.TGroup = 'CASE'

update #CombinedInspections set
CONTACT_NAME = License_People.NAME,
CONTACT_ADDRESS1 = License_People.ADDRESS1,
CONTACT_CITY = License_People.CITY,
CONTACT_STATE = License_People.STATE,
CONTACT_ZIP = License_People.ZIP,
CONTACT_PHONE = License_People.PHONE
from License_People where License_People.BUS_LIC_NO = #CombinedInspections.ActivityNo
and License_People.NAMETYPE = 'OWNER'
and #CombinedInspections.TGroup = 'LICENSE'

update #CombinedInspections set
CONTACT_NAME = License2_People.NAME,
CONTACT_ADDRESS1 = License2_People.ADDRESS1,
CONTACT_CITY = License2_People.CITY,
CONTACT_STATE = License2_People.STATE,
CONTACT_ZIP = License2_People.ZIP,
CONTACT_PHONE = License2_People.PHONE
from License2_People where License2_People.LICENSE_NO = #CombinedInspections.ActivityNo
and License2_People.NAMETYPE = 'OWNER'
and #CombinedInspections.TGroup = 'LICENSE2'

update #CombinedInspections set
CONTACT_NAME = Permit_People.NAME,
CONTACT_ADDRESS1 = Permit_People.ADDRESS1,
CONTACT_CITY = Permit_People.CITY,
CONTACT_STATE = Permit_People.STATE,
CONTACT_ZIP = Permit_People.ZIP,
CONTACT_PHONE = Permit_People.PHONE
from Permit_People where Permit_People.PERMIT_NO = #CombinedInspections.ActivityNo
and Permit_People.NAMETYPE = 'OWNER'
and #CombinedInspections.TGroup = 'PERMIT'

update #CombinedInspections set
CONTACT_NAME = Project_People.NAME,
CONTACT_ADDRESS1 = Project_People.ADDRESS1,
CONTACT_CITY = Project_People.CITY,
CONTACT_STATE = Project_People.STATE,
CONTACT_ZIP = Project_People.ZIP,
CONTACT_PHONE = Project_People.PHONE
from Project_People where Project_People.PROJECT_NO = #CombinedInspections.ActivityNo
and Project_People.NAMETYPE = 'OWNER'
and #CombinedInspections.TGroup = 'PROJECT'

update #CombinedInspections set
CONTACT_NAME = Geo_People.NAME,
CONTACT_ADDRESS1 = Geo_People.ADDRESS1,
CONTACT_CITY = Geo_People.CITY,
CONTACT_STATE = Geo_People.STATE,
CONTACT_ZIP = Geo_People.ZIP,
CONTACT_PHONE = Geo_People.PHONE
from Geo_People where Geo_People.Loc_RecordID = #CombinedInspections.ActivityNo
and Geo_People.NAMETYPE = 'OWNER'
and #CombinedInspections.TGroup = 'GEO'

----Update attachments (3 types: DOC,PHOTO, IMAGE; exclude DOC)
Create Table #InspectionImages(
TGroup Varchar(15),
ActivityNo Varchar(30),
ATTACHMENTCOUNT int
)
Insert Into #InspectionImages(TGroup,ActivityNo,ATTACHMENTCOUNT)
Select 'CASE' AS Tgroup, Case_Attachments.CASE_NO AS ACTIVITYNO, COUNT(recordid) from Case_Attachments where Case_Attachments.AttachmentType <> 'DOC' and Case_Attachments.CASE_NO in (select #CombinedInspections.ActivityNo from #CombinedInspections where #CombinedInspections.TGroup = 'CASE') group by Case_Attachments.CASE_NO
Union
Select 'LICENSE' AS Tgroup, LICENSE_Attachments.BUS_LIC_NO AS ACTIVITYNO, COUNT(recordid) from LICENSE_Attachments where LICENSE_Attachments.AttachmentType <> 'DOC' and LICENSE_Attachments.BUS_LIC_NO in (select #CombinedInspections.ActivityNo from #CombinedInspections where #CombinedInspections.TGroup = 'LICENSE') group by LICENSE_Attachments.BUS_LIC_NO
Union
Select 'LICENSE2' AS Tgroup, LICENSE2_Attachments.LICENSE_NO AS ACTIVITYNO, COUNT(recordid) from LICENSE2_Attachments where LICENSE2_Attachments.AttachmentType <> 'DOC' and LICENSE2_Attachments.LICENSE_NO in (select #CombinedInspections.ActivityNo from #CombinedInspections where #CombinedInspections.TGroup = 'LICENSE2') group by LICENSE2_Attachments.LICENSE_NO
Union
Select 'PERMIT' AS Tgroup, Permit_Attachments.permit_NO AS ACTIVITYNO, COUNT(recordid) from Permit_Attachments where Permit_Attachments.AttachmentType <> 'DOC' and Permit_Attachments.permit_NO in (select #CombinedInspections.ActivityNo from #CombinedInspections where #CombinedInspections.TGroup = 'PERMIT') group by Permit_Attachments.permit_NO
Union
Select 'PROJECT' AS Tgroup, Project_Attachments.project_NO AS ACTIVITYNO, COUNT(recordid) from Project_Attachments where Project_Attachments.AttachmentType <> 'DOC' and Project_Attachments.project_NO in (select #CombinedInspections.ActivityNo from #CombinedInspections where #CombinedInspections.TGroup = 'PROJECT') group by Project_Attachments.project_NO
Union
Select 'GEO' AS Tgroup, Geo_Attachments.Loc_RecordID AS ACTIVITYNO, COUNT(recordid) from Geo_Attachments where Geo_Attachments.AttachmentType <> 'DOC' and Geo_Attachments.Loc_RecordID in (select #CombinedInspections.ActivityNo from #CombinedInspections where #CombinedInspections.TGroup = 'GEO') group by Geo_Attachments.Loc_RecordID

update #CombinedInspections set #CombinedInspections.ATTACHMENTCOUNT = #InspectionImages.ATTACHMENTCOUNT from #InspectionImages inner join #CombinedInspections 
on #CombinedInspections.ActivityNo = #InspectionImages.ActivityNo and  #CombinedInspections.TGroup = #InspectionImages.TGroup


Select Top 50 * From #CombinedInspections Order By Scheduled_Date DESC,TGroup DESC, ActivityNo
















GO
