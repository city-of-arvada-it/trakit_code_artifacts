USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_SYS_Data_Step5_AddMissingPrimryKeys]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_SYS_Data_Step5_AddMissingPrimryKeys]
GO
/****** Object:  StoredProcedure [dbo].[tsp_SYS_Data_Step5_AddMissingPrimryKeys]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  					  
-- =====================================================================
-- Revision History:
--	mdm - 12/02/13 - added revision history	
--	mdm - 11/19/13 - added to source safe
-- =====================================================================   

CREATE PROCEDURE [dbo].[tsp_SYS_Data_Step5_AddMissingPrimryKeys] (
@OutputPath Varchar(1000) = Null
)

AS
BEGIN
  SET NOCOUNT ON

  --mdm - 09/20/13; sample @OutputPath:  'C:\Mayo\Step5_DS_AddMissingPrimaryKeys.sql'
  
	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[zAddPrmryKeysTemp]') AND type in (N'U'))
		BEGIN
		create table zAddPrmryKeysTemp
		(
		SqlToExec varchar(2000)
		)
		END

		truncate table zAddPrmryKeysTemp

		INSERT INTO zAddPrmryKeysTemp
		SELECT 'IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc JOIN 
		INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE ccu ON tc.CONSTRAINT_NAME = ccu.Constraint_name 
		WHERE tc.CONSTRAINT_TYPE = ''Primary Key'' 
		and ccu.table_name = ''' + ccu.TABLE_NAME + ''' and ccu.CONSTRAINT_NAME = ''' + ccu.CONSTRAINT_NAME + ''')     
		ALTER TABLE [' + ccu.TABLE_NAME + '] WITH NOCHECK ADD CONSTRAINT [' + ccu.CONSTRAINT_NAME + '] PRIMARY KEY CLUSTERED ([' + ccu.COLUMN_NAME + ']);'
		FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS tc
		JOIN INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE ccu ON tc.CONSTRAINT_NAME = ccu.Constraint_name
		WHERE tc.CONSTRAINT_TYPE = 'Primary Key'

		declare @FileName as varchar(1000)
		If @OutputPath Is Null OR @OutputPath = ''
			Set @FileName = 'C:\crw\TEMP\Step5_DS_AddMissingPrimaryKeys.sql'
		ELSE
			Set @FileName = @OutputPath
		--set @FileName = 'C:\Mayo\Step5_DS_AddMissingPrimaryKeys.sql'
		--select @FileName

		declare @dbName varchar(200)
		set @dbName = db_name()
		--select @dbName


		declare @cmd varchar(2000)
		set @cmd = 'bcp "select SqlToExec from '
		set @cmd = @cmd + @dbname + '.dbo.zAddPrmryKeysTemp order by SqlToExec" queryout "'
		set @cmd = @cmd + @FileName + '" -Utrakit -Ptrakit -c -S' + @@SERVERNAME

		--select @cmd
		exec xp_cmdshell @cmd

		select SqlToExec from zAddPrmryKeysTemp order by SqlToExec

--drop table zIndexListForSP

END







GO
