USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GblSearchMain_Parcel_SubSearch]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_GblSearchMain_Parcel_SubSearch]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GblSearchMain_Parcel_SubSearch]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[tsp_GblSearchMain_Parcel_SubSearch] 
(
		@searchString VARCHAR(500) = ''
	,@ExcludedGroups VARCHAR(250) = ''
)
AS
BEGIN
	SET NOCOUNT ON

	---- FOR DEBUG
	--SET NOCOUNT OFF

	--DECLARE @searchString VARCHAR(100) = 'BELL'
	--DECLARE @ExcludedGroups VARCHAR(250) = NULL

	--DROP TABLE [#Searches]
	--DROP TABLE [#TT]
	--DROP TABLE #ExcludeGroup
	---- FOR DEBUG

	CREATE TABLE [#Searches] (
		ColumnSearchAlias VARCHAR(256) NULL
		,[SearchTextFragment] [varchar](500) NULL
	)

	CREATE INDEX SEARCHESINDEX ON #SEARCHES (
		ColumnSearchAlias
		,[SearchTextFragment]
	)

	CREATE TABLE [#TT] (
		[recordid] [varchar](30) NULL
		,[SearchTextFragment] [varchar](256) NULL
	)

	CREATE TABLE #AllowedActivityTypes (ActivityTypeID INT)

	CREATE TABLE #ExcludeGroup (tgroup VARCHAR(100))
	SELECT @ExcludedGroups = COALESCE(@ExcludedGroups,'')

	-- Create rows from excluded groups string
	INSERT INTO #ExcludeGroup
	SELECT *
	FROM dbo.BreakStringIntoRows(@ExcludedGroups)

	/* QQHQ :: Used to constraint the type of records we are querying
		since we are now going directly against dbo.parcels instead
		of through tvw_GblActivityParcels' individual views (e.g. 
		CASE_Parcels, PERMIT_Parcels..) as before */ 
	INSERT INTO #AllowedActivityTypes (ActivityTypeID)
	VALUES 
		(1),	-- Permit Parcel
		(2),	-- Project Parcel
		(3),	-- License2 Parcels
		(4)		-- Case Parcels

	-- Will be using StartsWith instead of under-perf Contains
	IF RIGHT(@searchString, 1) <> '%'
		SET @searchString = @searchString + '%'

	INSERT INTO [#Searches]
	VALUES 
		 ('site_APN', @searchString)
		,('site_addr', @searchString)
		,('site_subdivision', @searchString)

	--Search for Parcels with Activity type id equal or between 1 and 4
	INSERT INTO #TT
	SELECT *
	FROM (
		SELECT dbo.Parcels.recordid
			,[TBL_Parcels_Fragments].[SearchTextFragment]
		FROM dbo.Parcels WITH (forceseek)
			JOIN dbo.Activity 
				ON Activity.ActivityID = Parcels.ActivityID
			INNER JOIN #AllowedActivityTypes 
				ON Activity.ActivityTypeID = #AllowedActivityTypes.ActivityTypeID
			INNER JOIN [dbo].[TBL_Parcels_Fragments] WITH (forceseek) 
				ON Parcels.RECORDID = [TBL_Parcels_Fragments].RECORDID
			INNER JOIN [#Searches] 
				ON [TBL_Parcels_Fragments].ColumnSearchAlias = #Searches.ColumnSearchAlias
				AND [TBL_Parcels_Fragments].[SearchTextFragment] LIKE #Searches.SearchTextFragment
	) AS tt
	OPTION (MAXDOP 2);


	INSERT INTO #TT
	SELECT *
	FROM (
		SELECT dbo.Parcels.recordid
			,[TBL_geo_ownership_Fragments].[SearchTextFragment]
		FROM dbo.Parcels
			JOIN dbo.Activity 
				ON Activity.ActivityID = Parcels.ActivityID
			INNER JOIN #AllowedActivityTypes 
				ON Activity.ActivityTypeID = #AllowedActivityTypes.ActivityTypeID
			INNER JOIN [TBL_geo_ownership_Fragments] 
				ON Parcels.LOC_RECORDID = [TBL_geo_ownership_Fragments].recordid
			INNER JOIN [#Searches] 
				ON [TBL_geo_ownership_Fragments].ColumnSearchAlias = #Searches.ColumnSearchAlias
				AND [TBL_geo_ownership_Fragments].[SearchTextFragment] LIKE #Searches.SearchTextFragment
		WHERE 
			[TBL_geo_ownership_Fragments].ColumnSearchAlias = 'site_subdivision'
	) AS tt;


	-- Get details and apply ExcludedGroup if any
	SELECT DISTINCT 
		 RecordID
		,Loc_RecordID
		,Tgroup
		,ActivityNo
		,Site_APN
		,Site_Addr
		,City AS Site_City
	FROM tvw_GblActivityParcels
	WHERE 
		RECORDID IN (SELECT DISTINCT recordid FROM #tt)
		AND tgroup NOT IN (SELECT * FROM #ExcludeGroup)
END
GO
