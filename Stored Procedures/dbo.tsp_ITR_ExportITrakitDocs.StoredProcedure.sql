USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ITR_ExportITrakitDocs]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_ITR_ExportITrakitDocs]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ITR_ExportITrakitDocs]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  
					  
					  					  					  /*
tsp_ExportITrakitDocs @DBName='DEV', @FilePath='C:\CRW\iTrakitExports\', @FileName='iTrakitDocsExport'

select * from itrakit_docs
where legalnotice like '%' + CHAR(13) + CHAR(10) + '%'


update itrakit_docs set
legalNotice = replace(legalnotice, CHAR(13) + CHAR(10), '^^/r^^')
where legalnotice like '%' + CHAR(13) + CHAR(10) + '%'

update itrakit_docs set
legalNotice = 'You have the right to remain Silent  
line 2 test'
where docid = 24

You have the right to remain Silent  line 2 test
*/

CREATE Procedure [dbo].[tsp_ITR_ExportITrakitDocs](
@DBName Varchar(50),
@FilePath Varchar(1000),
@FileName Varchar(150),
@ToggleXP_CMDShell INT = 1
)

As

SET NOCOUNT ON

Declare @SQL Varchar(8000)

If @ToggleXP_CMDShell Is Null Set @ToggleXP_CMDShell = 1

IF EXISTS (SELECT * FROM tempdb.sys.objects WHERE object_id = OBJECT_ID(N'tempdb..iTrakitDocs') AND type in (N'U'))
	Drop Table tempdb..iTrakitDocs

Create Table tempdb..iTrakitDocs([Group] Varchar(200)
      ,[SubGroup] Varchar(200)
      ,[DocName] Varchar(200)
      ,[DocDesc] Varchar(500)
      ,[DocTitle] Varchar(200)
      ,[DocTopText] Varchar(700)
      ,[DocSigText] Varchar(700)
      ,[DocBottomText] Varchar(700)
      ,[Active] Varchar(50)
      ,[ShowSignature] Varchar(50)
      ,[AllowPics] Varchar(50)
      ,[LegalNotice] Varchar(1024)
      ,[department] Varchar(150)
      ,[address1] Varchar(150)
      --,[address2] Varchar(150)
      --,[address3] Varchar(150)
      ,[city] Varchar(50)
      ,[state] Varchar(50)
      ,[zip] Varchar(50)
      ,[phone] Varchar(50)
      ,[fax] Varchar(50)
      ,[email] Varchar(150)
      ,[tbd] Varchar(150)
      )


Insert Into tempdb..iTrakitDocs([Group],SubGroup,DocName,DocDesc,DocTitle,DocTopText,DocSigText,DocBottomText,Active,ShowSignature,AllowPics,LegalNotice,department,address1,city,[state],zip,phone,fax,email,tbd)
Values('Group','SubGroup','DocName','DocDesc','DocTitle','DocTopText','DocSigText','DocBottomText','Active','ShowSignature','AllowPics','LegalNotice','department','address1','city','state','zip','phone','fax','email','tbd')

Set @SQL = '
Insert Into tempdb..iTrakitDocs([Group],[SubGroup],[DocName],[DocDesc],[DocTitle],[DocTopText],[DocSigText],[DocBottomText],[Active],[ShowSignature],[AllowPics],[LegalNotice],[department],[address1],[city],[state],[zip],[phone],[fax],[email],[tbd])
Select 
[Group]=CASE WHEN [Group] = '''' THEN Null ELSE [Group] END,
SubGroup=CASE WHEN SubGroup = '''' THEN Null ELSE SubGroup END,
DocName=CASE WHEN DocName = '''' THEN Null ELSE DocName END,
DocDesc=CASE WHEN DocDesc = '''' THEN Null ELSE Replace(DocDesc, CHAR(13) + CHAR(10), ''^^/r^^'') END,
DocTitle=CASE WHEN DocTitle = '''' THEN Null ELSE DocTitle END,
DocTopText=CASE WHEN DocTopText = '''' THEN Null ELSE Replace(DocTopText, CHAR(13) + CHAR(10), ''^^/r^^'') END,
DocSigText=CASE WHEN DocSigText = '''' THEN Null ELSE Replace(DocSigText, CHAR(13) + CHAR(10), ''^^/r^^'') END,
DocBottomText=CASE WHEN DocBottomText = '''' THEN Null ELSE Replace(DocBottomText, CHAR(13) + CHAR(10), ''^^/r^^'') END,
Active=CASE WHEN Active Is Null THEN ''0'' ELSE Convert(Varchar,Active) END,
ShowSignature=CASE WHEN ShowSignature Is Null THEN ''0'' ELSE Convert(Varchar,ShowSignature) END,
AllowPics=CASE WHEN AllowPics Is Null THEN ''0'' ELSE Convert(Varchar,AllowPics) END,
LegalNotice=CASE WHEN LegalNotice = '''' THEN Null ELSE Replace(LegalNotice, CHAR(13) + CHAR(10), ''^^/r^^'') END,
department=CASE WHEN department = '''' THEN Null ELSE department END,
address1=CASE WHEN address1 = '''' THEN Null WHEN address2 Is Not Null AND address2 <> '''' THEN address1 + '' '' + address2 ELSE address1 END,
city=CASE WHEN city = '''' THEN Null ELSE city END,
state=CASE WHEN state = '''' THEN Null ELSE state END,
zip=CASE WHEN zip = '''' THEN Null ELSE zip END,
phone=CASE WHEN phone = '''' THEN Null ELSE phone END,
fax=CASE WHEN fax = '''' THEN Null ELSE fax END,
email=CASE WHEN email = '''' THEN Null ELSE email END,
tbd=Null
From ' + @DBName + '..iTrakit_Docs
WHERE Active = 1
ORDER BY [Group],SubGroup,DocName'
--Print(@SQL)
Exec(@SQL)

Select * From tempdb..iTrakitDocs


Declare @vDate Varchar(100)
Declare @OutputFileName Varchar(1000)

Set @vDate = Replace(Convert(Varchar,GetDate(),102),'.','_')
Print(@vDate)

--Set @OutputFileName = '"' + @FilePath + @FileName + '_' + @vDate + '.csv"'
Set @OutputFileName = '"' + @FilePath + @FileName + '.csv"'


If @ToggleXP_CMDShell = 1
 Begin
	--Enable xp_cmdshell
	Set @SQL = '
	EXEC sp_configure ''show advanced options'', 1
	RECONFIGURE
	EXEC sp_configure ''xp_cmdshell'', 1
	RECONFIGURE
	'
	Print(@SQL)
	Exec(@SQL)
 End

select @sql = 'bcp "select * from tempdb..iTrakitDocs" queryout ' + @OutputFileName + ' -c -t"|" -T -S'
+ @@servername
--Print(@SQL)
exec master..xp_cmdshell @sql

Drop Table tempdb..iTrakitDocs


Set @SQL = '
DELETE ' + @DBName + '.dbo.iTrakitFeeds Where [FileName] = ''' + @FileName + '.csv''

INSERT INTO ' + @DBName + '.dbo.iTrakitFeeds([filename],[data])
SELECT ''' + @FileName + '.csv'', * FROM OPENROWSET(
  BULK N''' + REPLACE(@OutputFileName,'"','') + ''', SINGLE_BLOB
) bt'
Print(@SQL)
Exec(@SQL)

If @ToggleXP_CMDShell = 1
 Begin
	-- Disable xp_cmdshell
	Set @SQL = '
	EXEC sp_configure ''xp_cmdshell'', 0
	RECONFIGURE
	EXEC sp_configure ''show advanced options'', 0
	RECONFIGURE
	'
	Print(@SQL)
	Exec(@SQL)
 End










GO
