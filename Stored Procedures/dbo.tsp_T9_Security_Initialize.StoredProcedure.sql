USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_Security_Initialize]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_Security_Initialize]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_Security_Initialize]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================
-- Author:		Erick Hampton 
-- Create date: July 15, 2016
-- Description:	Creates default security preferences
-- ==================================================
CREATE PROCEDURE [dbo].[tsp_T9_Security_Initialize]
AS
BEGIN
	SET NOCOUNT ON;


	-- create table variable so we can calculate the recordid easily
	declare @prefs table (ID int identity(1,1), item varchar(100), value1 varchar(200), recordid AS ('SYS:' + right(('00' + str(DATEPART(yy, GETDATE()))),2) + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2) + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2) + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2) + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2) + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2) + right(('000000'+ CONVERT(varchar, ID)),6)) )

	insert into @prefs (Item,value1) values
	 ('USE_SSO','0')
	,('ENFORCE_COMPLEX_PASSWORDS','1')
	,('MAX_INVALID_PASSWORD_ATTEMPTS','3')
	,('LOCK_OUT_DURATION','0')
	,('MAX_PASSWORD_DURATION','90')
	,('INVITATION_LINK_DURATION','15')
	,('MIN_PASSWORD_LENGTH','6')
	,('MAX_PASSWORD_LENGTH','24')
	,('MIN_LOWERCASE_CHARS','1')
	,('MIN_UPPERCASE_CHARS','1')
	,('MIN_NUMERIC_CHARS','1')
	,('MIN_SPECIAL_CHARS','1')
	,('ELIGIBLE_SPECIAL_CHARS','!#$%&()*+,-./:;<=>?@[\]^_`{|}~ ')
	,('UNIQUE_PASSWORD_COUNT','0')
	,('UNIQUE_PASSWORD_DURATION','0')
	,('REQUIRE_EMAIL_ADDRESS','0')

	-- don't get rid of 'ACTIVE_DIRECTORY_ENABLED' if it already exists
	if (select count(*) from Prmry_Preferences where Item='ACTIVE_DIRECTORY_ENABLED')=0
	BEGIN
		insert into @prefs (Item,value1) values ('ACTIVE_DIRECTORY_ENABLED','0')
	END

	-- remove any old values so we don't duplicate them
	delete from Prmry_Preferences where GroupName='SYSTEM' and UserID='' and Item in ( 'USE_SSO'
																					 , 'ENFORCE_COMPLEX_PASSWORDS'
																					 , 'MAX_INVALID_PASSWORD_ATTEMPTS'
																					 , 'LOCK_OUT_DURATION'
																					 , 'MAX_PASSWORD_DURATION'
																					 , 'INVITATION_LINK_DURATION'
																					 , 'MIN_PASSWORD_LENGTH'
																					 , 'MAX_PASSWORD_LENGTH'
																					 , 'MIN_LOWERCASE_CHARS'
																					 , 'MIN_UPPERCASE_CHARS'
																					 , 'MIN_NUMERIC_CHARS'
																					 , 'MIN_SPECIAL_CHARS'
																					 , 'ELIGIBLE_SPECIAL_CHARS'
																					 , 'UNIQUE_PASSWORD_COUNT'
																					 , 'UNIQUE_PASSWORD_DURATION'
																					 , 'REQUIRE_EMAIL_ADDRESS'
																					  )



	-- popualate prmry_preferences with the default values
	insert into Prmry_Preferences (GroupName, UserID, Item, value1, Value2, RECORDID, LAST_MODIFIED, LAST_MODIFIED_BY) 
	select 'SYSTEM' [GroupName], '' [UserID], item, value1, '' [Value2], recordid, getdate() [LAST_MODIFIED], 'SYS' [LAST_MODIFIED_BY] from @prefs


	-- if the t9UserID column is null, set it to the value of the UserID colunm 
	update prmry_users set t9UserID=userid where t9UserID is null or ltrim(rtrim(t9UserID))=''


END
GO
