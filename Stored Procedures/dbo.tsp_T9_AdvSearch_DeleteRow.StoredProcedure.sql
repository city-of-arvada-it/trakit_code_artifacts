USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearch_DeleteRow]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_AdvSearch_DeleteRow]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearch_DeleteRow]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[tsp_T9_AdvSearch_DeleteRow]
	@DetailID int = -1,
	@UserID varchar(50) = null

AS
BEGIN


begin
		-- we want to delete row with below detailID and ItemID
		delete from Prmry_AdvSearch_CurrentDetail where RecordID = @DetailID  and itemid = (select recordid from Prmry_AdvSearch_CurrentItem where USERID =  @UserID)
end

END





GO
