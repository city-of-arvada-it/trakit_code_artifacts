USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[lp_focus_avg_workingdays_newcomm]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[lp_focus_avg_workingdays_newcomm]
GO
/****** Object:  StoredProcedure [dbo].[lp_focus_avg_workingdays_newcomm]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* 
--Ychavez 1/11/2015 - FOR FOCUS Measures report
*/
CREATE PROCEDURE [dbo].[lp_focus_avg_workingdays_newcomm]
		@APPLIED_FROM datetime = null,
		@APPLIED_TO datetime = null
as
begin


	--1st measure--
-------------------------------------------------------------------------------------------
--Average # of working days from initial application until permit issued (new single family)
--Permit Type: new Single Family
--Sub type: NEW SF DETACHED, NEW DUPLEX, NEW SF ATTACHED
--Measure: PERMIT APPLIED DATE TO ISSUED DATE-Average
--exec lp_focus_avg_workingdays_newcomm @APPLIED_FROM = '10/01/2014' , @APPLIED_TO = '12/31/2014'
-------------------------------------------------------------------------------------------
--SELECT * FROM #tmp_measure
--DROP TABLE #tmp_measure
SELECT 
DATEDIFF(day, applied, issued) AS num_of_days_applied_issued,PERMIT_NO,
 0 as total_permits, 0 as average_num_permits
into #tmp_measure
 from permit_main
where permittype in ('COMMERCIAL' )
AND PermitSubType IN ('NEW CONSTRUCTION')
		
AND ISSUED IS NOT NULL AND APPLIED IS NOT NULL
AND STATUS NOT IN ('WITHDRAWN', 'DENIED', 'VOID    ', 'VOID')
AND APPLIED > = @APPLIED_FROM 
AND ISSUED <= @APPLIED_TO

--AND issued > '1/01/2014'
------------------------------------------------------------------------------------------
--CALCULATE TOTAL PERMITS / PERMITS IN MEASURE - OUT OF MEASURE / LESS THAN 1 day MEASURE--
------------------------------------------------------------------------------------------


--use for testing-- 
SELECT sum(num_of_days_applied_issued ) AS tot_days_issued , COUNT(permit_no) AS num_of_permits
INTO #tmp_calc
 from #tmp_measure

 SELECT CAST(tot_days_issued /num_of_permits  AS  NUMERIC(18,2)) AS AVG_NO_DAYS_APP_TO_ISSUED, tot_days_issued, num_of_permits
  FROM #tmp_calc
--exec lp_focus_avg_workingdays_newcomm @APPLIED_FROM = '01/01/2014' , @APPLIED_TO = '10/31/2014'

END
GO
