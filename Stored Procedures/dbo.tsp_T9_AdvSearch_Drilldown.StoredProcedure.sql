USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearch_Drilldown]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_AdvSearch_Drilldown]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearch_Drilldown]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



create PROCEDURE [dbo].[tsp_T9_AdvSearch_Drilldown] (
	@UserID varchar(50) = null, -- EWH Dec 11, 2014 issue #33974: restrict CRM records by user
	@SearchTable varchar(50) = null, -- current / last / new / saved / shared
	@SearchID int = null,
	@FriendlyTableName varchar(255) = null, -- this is the friendly name for the table contains columns we want to display
	@KeyFieldFilterValue varchar(max) = null, -- this is the reference to the key field on the main table that we want to filter the results by
	@SelectAlias varchar(max) = null out,
	@SelectClause varchar(max) = null out,
	@FromClause varchar(max) = null out,
	@WhereClause varchar(max) = null out,
	@OrderByInOver varchar(max) = null out
)

AS
BEGIN
--print(convert(varchar,getdate(),109)) -- EWH: used for measuring the duration of this proc





declare @Group varchar(50)
declare @AdditionalTable varchar(50) -- this is table name that has the friendly table name = @FriendlyTableName
declare @MainTable varchar(50)
declare @UdfTable varchar(50) -- the custom fields table for a group

declare @details as [udtt_T9_AdvSearch_Detail] -- where the search details are stored


declare @KeyField_Main as varchar(50)
declare @MainTableDotKeyField as varchar(255)

declare @SQL varchar(max) = ''

declare @CrmRestrictions as varchar(max) = '' -- EWH Dec 11, 2014 issue #33974: restrict CRM records by user


	 
--print(convert(varchar,getdate(),109)) -- EWH: used for measuring the duration of this proc


-- make sure we have all of the input values 
if (@UserID is not null) and (@SearchTable is not null) and (@SearchID is not null) and (@FriendlyTableName is not null) and (@KeyFieldFilterValue is not null)
begin

-- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- FETCH ITEM DATA AND DETAILS
-- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- determine search id for current, new, or last search tables
if @SearchTable in ('current','new','last') set @SearchID = [dbo].[tfn_T9_AdvSearch_GetSearchID](@UserID, @SearchTable)

-- get search item info
declare	@Description varchar(250)
declare	@SearchName varchar(50) 
declare @RefSearchID int
declare	@RefTable varchar(50)
declare @SearchType varchar(50)
insert into @details exec [tsp_T9_AdvSearch_Get] @UserID, @SearchTable, @SearchID, @Group out, @SearchType out, @Description out, @SearchName out, @RefSearchID out, @RefTable out

if @Group is null set @Group = 'PERMIT' --  TODO: REPLACE THIS WITH...?
-- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------


-- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Look up table name from the given friendly table name
-- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------
set @AdditionalTable = dbo.tfn_T9_AdvSearch_GetTableNameFromFriendlyTableName(@Group,@FriendlyTableName)
-- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------


-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- set main table and UDF table for join
-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

set @UdfTable = ''
if @Group = 'CRM' or @Group = 'GEO'
begin
	 if @Group = 'CRM'
	 begin
		set @MainTable = 'TVW_CRMISSUES2' --> EWH Nov 8, 2017 #11004
		-- CRM has no UDF table
	 end
	 else
	 begin
		set @MainTable = 'GEO_OWNERSHIP'
		set @UdfTable = 'GEO_UDF'
	 end
end
else
begin
	set @MainTable =  @Group + '_MAIN' 
	set @UdfTable = @Group + '_UDF'
end

set @KeyField_Main = [dbo].[tfn_T9_AdvSearch_KeyField](@MainTable)

set @MainTableDotKeyField =  '[' + @MainTable + '].[' + @KeyField_Main + ']'

-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- EWH Dec 11, 2014 issue #33974: restrict CRM records by user - create where clause segment in @CrmRestrictions if @MainTable = 'tvw_CrmIssues2'
--									note that if @UserID is not passed then we won't return any records
-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
if @MainTable = 'TVW_CRMISSUES2' --> EWH Nov 8, 2017 #11004
begin
	if isnull(@UserID,'') = '' --don't allow passing an empty string either
	begin
		set @CrmRestrictions=' AND 0=1' -- make sure we don't return any results at all if @UserID is not supplied
	end
	else
	begin
		-- EWH Jan 12, 2015: fixed query below for CRM restrictions
		--> EWH Nov 8, 2017 #11004 replaced 'CRM_ISSUES' with 'tvw_CrmIssues2' in the query below
		set @CrmRestrictions=' AND (TVW_CRMISSUES2.NATURETYPE_LIST_ID IS NULL OR TVW_CRMISSUES2.NATURETYPE_LIST_ID IN (select distinct TVW_CRMISSUES2.NATURETYPE_LIST_ID from TVW_CRMISSUES2 inner join crm_lists on NATURETYPE_LIST_ID = list_id where list_text not in (SELECT distinct TYPENAME FROM PRMRY_CRM_RESTR_TYPE_ACCESS WHERE USERID <> ''' + @UserID + ''')))' 
		--set @CrmRestrictions=' AND (CRM_ISSUES.NATURETYPE_LIST_ID IS NULL OR CRM_ISSUES.NATURETYPE_LIST_ID IN (select distinct CRM_Issues.NATURETYPE_LIST_ID from CRM_Issues inner join crm_lists on NATURETYPE_LIST_ID = list_id where list_text not in (SELECT distinct TYPENAME FROM PRMRY_CRM_RESTR_TYPE_ACCESS WHERE USERID <> ''' + @UserID + ''')))' 
	end
end
-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------




-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Split details into 2 objects: outer for the columns in the main / udf / additional tables; inner for the columns that are consolidated to one column displaying counts
-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- columns that are in the main, main udf, or additional tables
declare @detailsOuter [udtt_T9_AdvSearch_Detail]
insert into @detailsOuter select * from @details where (TableName = @MainTable or TableName = @UdfTable or TableName = @AdditionalTable) order by Seq
--select * from @detailsOuter  -- testing


-- columns that are displayed as counts
declare @detailsInner [udtt_T9_AdvSearch_Detail]
insert into @detailsInner select * from @details where (TableName <> @MainTable and TableName <> @UdfTable and TableName <> @AdditionalTable) order by Seq
-- select * from @detailsInner  -- testing
-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Compile filter segments for @detailsOuter and @detailsInner; @filtersInner consolidates columns for the same table into one column and updates the table info accordingly
-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- compile filters for columns that are in the main, main udf, or additional tables
declare @filtersOuter udtt_T9_AdvSearch_DetailFilters
insert into @filtersOuter EXEC [tsp_T9_AdvSearch_CompileFilters] @detailsOuter, 0
--select * from @filtersOuter  -- testing


-- compile filters for columns that are displayed as counts
declare @filtersInner udtt_T9_AdvSearch_DetailFilters
insert into @filtersInner EXEC [tsp_T9_AdvSearch_CompileFilters] @detailsInner, 1
--select * from @filtersInner   -- testing
-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--  Create SELECT clause - includes sorting, but it is not very important here as it is addressed in the Aliases section after this
-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
set @SelectClause = ''
declare @s varchar(max) 
set @s = (
			select case when (tablename = @AdditionalTable) or (tablename = @MainTable) or (tablename = @UdfTable) then 
							-- format column data
							case Datatype	when 'float' then  ',str([' + tablename + '].[' + FieldName + ']) as [' + displayname + ']' 
											when 'date'	then ',convert(varchar(10),[' + tablename + '].[' + FieldName + '],101) as [' + displayname + ']' 
											when 'datetime' then ',convert(varchar(10),[' + tablename + '].[' + FieldName + '],101) as [' + displayname + ']' 
											when 'datetime2' then ',convert(varchar(10),[' + tablename + '].[' + FieldName + '],101) as [' + displayname + ']' 
											else ',[' + tablename + '].[' + FieldName + '] as [' + displayname + ']' 
											end
						-- don't include the columns with counts
						end 
			from  
				(
					select TableName, FieldName, DisplayName, Datatype, Seq from @detailsOuter -- the main table, udf table, and additional table columns
					union 

					-- the columns that we just want the counts for - use @filtersInner because the columns are consolidated there
					-- select the TableName as the DisplayName for the Table Columns" as we establish the alias in the JOIN clauses for these
					select [DisplayName] TableName, FieldName, DisplayName, 'int', 10000 from @filtersInner 
				) b
			order by seq 
			for xml path('')
		)
set @SelectClause =  @MainTableDotKeyField + ' as ["Key Field"]' + isnull(@s,'') -- put the key field value first, plus we don't have to strip off the leading comma :)
-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--  Create Column Aliases - the use of quotation marks and a space in ["Key Field"] is intentional to keep from duplicating an alias a user has specified.  
--		NOTE: @SelectAlias does not contain ["Key Field"] 
-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
set @s = '' -- reuse @s
set @s = (select '[' + displayname + '],' 
from  
(
	select DisplayName, Seq from @detailsOuter -- the main table, udf table, and additional table columns
) b
order by seq 
for xml path(''))
set @SelectAlias = (select case @s when null then '' else left(@s,(len(@s)-1)) end) -- remove trailing comma from the end
-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Create FROM clause 
-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- start with columns that will display data (not counts)
set @FromClause = @MainTable + isnull(
(
	select ' INNER JOIN ' + TableName + ' ON ' + @MainTableDotKeyField + ' = ' + TableName + '.' + [dbo].[tfn_T9_AdvSearch_KeyField](TableName) -- need to get the key field for the join table too, it is not always the same as the main table key field.
	from @detailsOuter
	where TableName <> @MainTable -- make sure we don't join the main table to itself
	group by TableName -- make sure we don't join a table more than once because it contains more than one column
	for xml path('')
),'')

--	-- assemble from clause 
set @FromClause = @FromClause + 
	isnull(
			(
				select	case isnull(CompiledFilter,'') when '' then ' LEFT ' else ' INNER ' end + 'JOIN (select ' + [dbo].[tfn_T9_AdvSearch_KeyField](TableName) + ' kf FROM [' + TableName + '] ' + 
						case isnull(CompiledFilter,'') when '' then '' else ' WHERE ' + isnull(CompiledFilter,'') end + 
						' GROUP BY ' + [dbo].[tfn_T9_AdvSearch_KeyField](TableName) + ') [' + DisplayName + '] ON ' + @MainTableDotKeyField + '=[' + DisplayName + '].kf' 
				from @filtersInner
				for xml path('')
			)
	,'')
-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------





-- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Create WHERE clause for columns that will display data (columns that display counts have where clauses in the joins)
-- ------------------------------------------------------------------------------------------------------------------------------------
-- assemble where clause with segments
set @WhereClause = @MainTableDotKeyField + '=''' + @KeyFieldFilterValue + '''' + isnull(
(
	select ' AND ' + CompiledFilter
	from @filtersOuter where CompiledFilter<>''
	for xml path('')
),'')
set @WhereClause = @WhereClause + @CrmRestrictions -- add in CRM restrictions
-- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


print '@UserID = ' + isnull(@UserID,'NULL')
print '@SearchTable = ' + isnull(@SearchTable,'NULL')
print '@SearchID (out) = ' + isnull(cast(@SearchID as varchar),'NULL')
print '@Group = ' + isnull(@Group,'NULL')
print '@MainTable = ' + isnull(@MainTable,'NULL')
print '@UdfTable = ' + isnull(@UdfTable,'NULL')
print '@AdditionalTable = ' + isnull(@AdditionalTable,'NULL')
print '@SelectAlias (out) = ' + isnull(@SelectAlias, 'NULL')
print '@SelectClause (out) = ' + isnull(@SelectClause, 'NULL')
print '@FromClause (out) = ' + isnull(@FromClause, 'NULL')
print '@WhereClause (out) = ' + isnull(@WhereClause, 'NULL')



--print(convert(varchar,getdate(),109)) -- EWH: used for measuring the duration of this proc


-- EWH Aug 17, 2015: can't call ExecuteQuery from here because there could be invalid characters in @FromClause (i.e. &lt;&gt: instead of <> )
-- return the data from execute query
/*
EXEC	[tsp_T9_AdvSearch_ExecuteQuery]
	@PageNumber = 1,
	@PageSize = 1000000000,		
	@SelectClause = @SelectClause,
	@SelectAlias = @SelectAlias,
	@FromClause = @FromClause,
	@WhereClause = @WhereClause,
	@GroupByClause = '',
	@OrderByClause = '',
	@OrderByInOver = @MainTableDotKeyField,
	@ReturnHTML = 1,
	@IncludeHyperlinkColumns = 0
*/
	
set @OrderByInOver = @MainTableDotKeyField

end


return

END

GO
