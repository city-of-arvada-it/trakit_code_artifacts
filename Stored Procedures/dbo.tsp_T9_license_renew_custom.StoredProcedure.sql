USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_license_renew_custom]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_license_renew_custom]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_license_renew_custom]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE  [dbo].[tsp_T9_license_renew_custom]
       
AS
BEGIN
       -- SET NOCOUNT ON added to prevent extra result sets from
       -- interfering with SELECT statements.
       SET NOCOUNT ON;

	 

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

DECLARE @SCHEDULE_EFFECTIVE_DATE as DATETIME
DECLARE @EFFECTIVE_FEE_SCHEDULE as varchar(200)
DECLARE @FEE_PARENT as nvarchar(30)
SET @SCHEDULE_EFFECTIVE_DATE = (SELECT MAX(EFFECTIVE_ON) FROM Prmry_FeeScheduleList WHERE Prmry_FeeScheduleList.EFFECTIVE_ON < GETDATE() AND TEST <> 1)
SET @EFFECTIVE_FEE_SCHEDULE = (SELECT TOP 1 Prmry_FeeScheduleList.SCHEDULE_NAME FROM Prmry_FeeScheduleList WHERE TEST <> 1 AND EFFECTIVE_ON = @SCHEDULE_EFFECTIVE_DATE or
	TEST <> 1 AND EFFECTIVE_ON is null )
IF @SCHEDULE_EFFECTIVE_DATE IS NULL
	BEGIN
		SET @SCHEDULE_EFFECTIVE_DATE = GETDATE()
		SET @EFFECTIVE_FEE_SCHEDULE = 'Default'
	END
	
SELECT @SCHEDULE_EFFECTIVE_DATE, @EFFECTIVE_FEE_SCHEDULE

--Create Parent Fee Temp Table


-- RB Check if temp table exists
IF OBJECT_ID('tempdb..#LicenseParentFees') IS NOT NULL  
	drop table #LicenseParentFees

Create Table #LicenseParentFees(
	RecordID varchar(30),
	License_No varchar(20),
	Code varchar(12),
	[Description] varchar(60),
	Formula varchar(2000),					--set to max for that table
	Detail varchar(1),						--its 1 / 0
	FeeType varchar(8),
	Amount float,
	Assessed_Date datetime,
	Paid varchar(1),
	Quantity float,
	Paid_Amount float,
	Assessed_by varchar(6)
	)

-- RB Check if temp table exists
IF OBJECT_ID('tempdb..#LicenseSubFees') IS NOT NULL  
	drop table #LicenseSubFees

Create Table #LicenseSubFees(
RecordID varchar(30),
Account varchar(30), 
Amount float,
Assessed_By varchar(6), 
Assessed_Date datetime,
Code varchar(12),
[Description] varchar(60),
formula varchar(2000),
item varchar(12),
license_no varchar(20),
paid varchar(1),
PARENTID varchar(30),
QUANTITY float,
subfeetype varchar(8)
)

 
--insert fees
insert into #LicenseParentFees 
( [RECORDID],license_no,[CODE],[DESCRIPTION],formula,[DETAIL],[FEETYPE],amount,[ASSESSED_DATE],[PAID],[QUANTITY], [paid_amount], assessed_by)

             SELECT 'CRW:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
                    + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)
                    + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
                    + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)
                    + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
                    + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)
                    + right(('000000'+ CONVERT(varchar,ROW_NUMBER() OVER (ORDER BY (SELECT NULL))+111)),6)       
                     as recid,tbl.* from
                    (select license_no, value1, value4, FORMULA, case when formula='[ITEMIZE]' then '1' else'0' end as detail, 'FEE' as feetype,
                           tamounts.amount, getdate() as assessed_date,'0' as paid,'0' as quantity, '0' as paid_amount,
                           'System' as assessed_by from LICENSE2_MAIN
                           inner join (select distinct typename,Value1, value2,value4 from Prmry_TypesInfo where groupname='license2'
                           and category='FEESAUTOMATIC') as t_feeList on LICENSE_TYPE=typename
                           inner join (select * from Prmry_FeeSchedule where groupname='license2' and SCHEDULE_NAME=@EFFECTIVE_FEE_SCHEDULE) as tFees
                                 on value1=code
                           inner join(select parent_code, sum(cast(formula as float)) as amount 
                           from Prmry_FeeSchedule where groupname='license2' AND ISNUMERIC(FORMULA)=1 and SCHEDULE_NAME=@EFFECTIVE_FEE_SCHEDULE group by parent_code) as tAmounts on Value1=tAmounts.PARENT_CODE
             where  CONVERT(date, expired)= CONVERT(date, DATEADD(day, 30, GETDATE())) and value2 is null) AS TBL

--insert fees for subtypes
                     insert into #LicenseParentFees 
( [RECORDID],license_no,[CODE],[DESCRIPTION],formula,[DETAIL],[FEETYPE],amount,[ASSESSED_DATE],[PAID],[QUANTITY], [paid_amount], assessed_by)

             SELECT 'CRW:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
                    + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)
                    + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
                    + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)
                    + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
                    + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)
                    + right(('000000'+ CONVERT(varchar,ROW_NUMBER() OVER (ORDER BY (SELECT NULL))+111)),6)       
                     as recid,tbl.* from
                    (SELECT LICENSE2_MAIN.LICENSE_NO, parent_fee.CODE, parent_fee.DESCRIPTION, parent_fee.FORMULA, 
                                 CASE WHEN parent_fee.FORMULA = '[ITEMIZE]' THEN '1' ELSE '0' END AS detail, 
                  'FEE' AS feetype, tAmounts.amount, GETDATE() AS assessed_date, '0' AS paid, '0' AS quantity, '0' AS paid_amount, 
                             'System' AS assessed_by
                                        FROM     (SELECT PARENT_CODE, SUM(CAST(FORMULA AS float)) AS amount
                  FROM      Prmry_FeeSchedule AS Prmry_FeeSchedule_1
                  WHERE   (GroupName = 'license2') AND (ISNUMERIC(FORMULA) = 1)
                  GROUP BY PARENT_CODE) AS tAmounts INNER JOIN
                      (SELECT CODE, DESCRIPTION,formula FROM      Prmry_FeeSchedule AS ps 
                       WHERE   (PARENT_CODE IS NULL) and  SCHEDULE_NAME=@EFFECTIVE_FEE_SCHEDULE) AS parent_fee ON tAmounts.PARENT_CODE = parent_fee.CODE INNER JOIN
                  LICENSE2_MAIN INNER JOIN
                      (SELECT TYPENAME, SUBTYPENAME, AUTO_DATA
                       FROM      Prmry_Auto_Insert
                       WHERE   (GROUPNAME = 'license2') AND (CATEGORY = 'fee')) AS t_feeList ON LICENSE2_MAIN.LICENSE_TYPE = t_feeList.TYPENAME AND 
                  LICENSE2_MAIN.LICENSE_SUBTYPE = t_feeList.SUBTYPENAME INNER JOIN
                      (SELECT * FROM      Prmry_FeeSchedule WHERE   (GroupName = 'license2') and SCHEDULE_NAME=@EFFECTIVE_FEE_SCHEDULE ) AS tFees ON 
                                   t_feeList.AUTO_DATA = tFees.CODE ON parent_fee.CODE = tFees.PARENT_CODE
                                 WHERE  (CONVERT(date, LICENSE2_MAIN.EXPIRED) = CONVERT(date, DATEADD(day, 30, GETDATE()))) ) AS TBL




/*
insert into license2_fees 
( [RECORDID],license_no,[CODE],[DESCRIPTION],formula,[DETAIL],[FEETYPE],amount,[ASSESSED_DATE],[PAID],[QUANTITY], [paid_amount], assessed_by)
select * from #LicenseParentFees

*/


--insert sub fees
insert into #LicenseSubFees
(recordid, account, amount,ASSESSED_BY, ASSESSED_DATE,code,description,formula,item,license_no,paid,PARENTID,QUANTITY,subfeetype)
             SELECT 'CRW:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
                    + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)
                    + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
                    + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)
                    + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
                    + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)
                    + right(('000000'+ CONVERT(varchar,ROW_NUMBER() OVER (ORDER BY (SELECT NULL)))),6)    
                     as recid,tbl.* from
             (SELECT tFees.ACCOUNT, tFees.FORMULA AS AMOUNT, 'System' AS ASSESSED_BY, GETDATE() AS ASSESSED_DATE, t_feeList.Value1 AS CODE, 
             t_feeList.Value4 AS DESCR, tFees.FORMULA, t_feeList.Value2, LMS.LICENSE_NO, '0' AS PAID, LMS.RECORDID, '0' AS QUANT, 
             'FEE' AS SUBFEETYPE
                           FROM     (SELECT LM.LICENSE_NO,EXPIRED, LM.LICENSE_TYPE, FEE.RECORDID, FEE.CODE FROM      
                                               LICENSE2_MAIN AS LM INNER JOIN
                            #LicenseParentFees AS FEE ON LM.LICENSE_NO = FEE.LICENSE_NO
                                               WHERE   (CONVERT(date, FEE.ASSESSED_DATE) = CONVERT(date, GETDATE()))) AS LMS 
                                 INNER JOIN
                      (SELECT DISTINCT TypeName, Value1, Value2, Value4
                       FROM      Prmry_TypesInfo
                       WHERE   (GroupName = 'license2') AND (Category = 'FEESAUTOMATIC')) AS t_feeList ON 
                                    LMS.LICENSE_TYPE = t_feeList.TypeName 
                               INNER JOIN
                      (SELECT * FROM      Prmry_FeeSchedule WHERE   (GroupName = 'license2') and SCHEDULE_NAME=@EFFECTIVE_FEE_SCHEDULE ) AS tFees 
                                   ON LMS.CODE = tFees.PARENT_CODE
                                        WHERE  (CONVERT(date, expired) = CONVERT(date, DATEADD(day, 30, GETDATE()))) 
										AND (t_feeList.Value2 IS not NULL)) as tbl

              insert into #LicenseSubFees
                                        (recordid, account, amount,ASSESSED_BY, ASSESSED_DATE,code,description,formula,item,license_no,paid,PARENTID,QUANTITY,subfeetype)
              SELECT 'CRW:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
                    + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)
                    + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
                    + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)
                    + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
                    + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)
                    + right(('000000'+ CONVERT(varchar,ROW_NUMBER() OVER (ORDER BY (SELECT NULL)))),6)    
                     as recid,tbl.* from
             (
                    SELECT tFees.ACCOUNT, tFees.FORMULA AS AMOUNT, 'System' AS ASSESSED_BY, GETDATE() AS ASSESSED_DATE, t_feeList.AUTO_DATA AS CODE, tFees.DESCRIPTION AS DESCR, 
                  tFees.FORMULA, t_feeList.AUTO_DATA, LMS.LICENSE_NO, '0' AS PAID, LMS.RECORDID, '0' AS QUANT, 'FEE' AS SUBFEETYPE
FROM     (SELECT DISTINCT LM.LICENSE_NO, LM.EXPIRED, LM.LICENSE_TYPE, LM.LICENSE_SUBTYPE, FEE.RECORDID, FEE.CODE
                  FROM      LICENSE2_MAIN AS LM INNER JOIN
                                    #LicenseParentFees AS FEE ON LM.LICENSE_NO = FEE.LICENSE_NO
                  WHERE   (CONVERT(date, FEE.ASSESSED_DATE) = CONVERT(date, GETDATE()))
				   AND (CONVERT(date, LM.EXPIRED) = CONVERT(date, DATEADD(day, 30, GETDATE())))) 
                  AS LMS INNER JOIN
                      (SELECT TYPENAME, SUBTYPENAME, AUTO_DATA
                       FROM      Prmry_Auto_Insert
                       WHERE   (GROUPNAME = 'license2') AND (CATEGORY = 'fee')) AS t_feeList 
					   ON LMS.LICENSE_TYPE = t_feeList.TYPENAME 
					   AND t_feeList.SUBTYPENAME = LMS.LICENSE_SUBTYPE 
					   INNER JOIN
                      (SELECT *
						   FROM      Prmry_FeeSchedule
						   WHERE   (GroupName = 'license2')
							 and SCHEDULE_NAME=@EFFECTIVE_FEE_SCHEDULE ) AS tFees 
					   ON LMS.CODE = tFees.PARENT_CODE AND t_feeList.AUTO_DATA = tFees.CODE
							WHERE  (CONVERT(date, LMS.EXPIRED) = CONVERT(date, DATEADD(day, 30, GETDATE()))) 
							AND (t_feeList.AUTO_DATA IS NOT NULL)) as tbl


/*
insert into LICENSE2_SUBFEES 
(recordid, account, amount,ASSESSED_BY, ASSESSED_DATE,code,description,formula,item,license_no,paid,PARENTID,QUANTITY,subfeetype)
select * from #LicenseSubFees

*/
--update total due in main table

update lm set lm.amount = tot_fee 
from #LicenseParentFees lm 
inner join(
	select license_no, sum(amount) as tot_fee, code, item, parentid from #LicenseSubFees
	where license_no in(select license_no from license2_main where
						expired = CONVERT(date, DATEADD(day, 30, GETDATE()))) 
group by license_no,code, item, parentid) as tbl 
on tbl.parentid=lm.recordid

select * from #LicenseParentFees
select * from #LicenseSubFees



insert into license2_fees 
( [RECORDID],license_no,[CODE],[DESCRIPTION],formula,[DETAIL],[FEETYPE],amount,[ASSESSED_DATE],[PAID],[QUANTITY], [paid_amount], assessed_by)
select * from #LicenseParentFees

insert into LICENSE2_SUBFEES 
(recordid, account, amount,ASSESSED_BY, ASSESSED_DATE,code,description,formula,item,license_no,paid,PARENTID,QUANTITY,subfeetype)
select * from #LicenseSubFees

--Everything after this comment Must be doen after insert back into regular tables. (it updates license2 main)

update lm set balance_due = total_due FROM license2_main lm 
inner join     
    (Select license_no, sum(amount)as total_due 
			from (SELECT LM.LICENSE_NO,EXPIRED, LM.LICENSE_TYPE, FEE.RECORDID, FEE.CODE, fee.amount 
				FROM   LICENSE2_MAIN AS LM 
				INNER JOIN  LICENSE2_FEES AS FEE 
				ON LM.LICENSE_NO = FEE.LICENSE_NO
             WHERE   (CONVERT(date, FEE.ASSESSED_DATE) = CONVERT(date, GETDATE()) and paid='0')) AS LMS 
		INNER JOIN
			(SELECT DISTINCT TypeName, Value1, Value2, Value4
			 FROM      Prmry_TypesInfo
		    WHERE   (GroupName = 'license2') AND (Category = 'FEESAUTOMATIC') and value2 is null) AS t_feeList 
				ON  LMS.LICENSE_TYPE = t_feeList.TypeName           
                    group by lms.license_no )as tbl 
	on lm.license_no=tbl.license_no

	


	

update lm set balance_due= total_due FROM license2_main lm inner join     
                    (Select license_no, sum(amount)  as total_due from (SELECT LM.LICENSE_NO,EXPIRED, LM.LICENSE_TYPE,lm.license_subtype,
                                  FEE.RECORDID, FEE.CODE, fee.amount FROM      
                                               LICENSE2_MAIN AS LM INNER JOIN
                            LICENSE2_FEES AS FEE ON LM.LICENSE_NO = FEE.LICENSE_NO
                                               WHERE   (CONVERT(date, FEE.ASSESSED_DATE) = CONVERT(date, GETDATE())
                                               and paid='0' and FEE.FEETYPE='FEE')) AS LMS 
                                 INNER JOIN
                      (SELECT [TYPENAME] ,[SUBTYPENAME] ,[AUTO_DATA]  
                                    FROM [dbo].[Prmry_Auto_Insert]  where groupname='license2'
                                  and CATEGORY='fee' ) AS t_feeList ON 
                                    LMS.LICENSE_TYPE = t_feeList.TypeName  and lms.license_subtype=  t_feeList.[SUBTYPENAME]       
                                    group by lms.license_no )as tbl on lm.license_no=tbl.license_no
--update status to delinquent 

update lm set status= 'DELINQUENT'  FROM license2_main lm inner join     
                    (select License_no from (Select lm.license_no,LICENSE_TYPE FROM      
                                               LICENSE2_MAIN AS LM INNER JOIN
                            LICENSE2_FEES AS FEE ON LM.LICENSE_NO = FEE.LICENSE_NO
                                               WHERE   (CONVERT(date, lm.expired) = CONVERT(date, GETDATE())
                                               and paid='0')) AS LMS 
                                 INNER JOIN
                      (SELECT DISTINCT TypeName, Value1, Value2, Value4
                       FROM      Prmry_TypesInfo
                       WHERE   (GroupName = 'license2') AND (Category = 'FEESAUTOMATIC') and value2 is null) AS t_feeList ON 
                                    LMS.LICENSE_TYPE = t_feeList.TypeName) as tbl on lm.license_no=tbl.license_no

update lm set status= 'DELINQUENT'  FROM license2_main lm inner join     
                    (select License_no from (Select lm.license_no,LICENSE_TYPE, license_subtype FROM      
                                               LICENSE2_MAIN AS LM INNER JOIN
                            LICENSE2_FEES AS FEE ON LM.LICENSE_NO = FEE.LICENSE_NO
                                               WHERE   (CONVERT(date, lm.expired) = CONVERT(date, GETDATE())
                                               and paid='0')) AS LMS 
                                 INNER JOIN
                      (SELECT [TYPENAME] ,[SUBTYPENAME] ,[AUTO_DATA]  
                                    FROM [dbo].[Prmry_Auto_Insert]  where groupname='license2'
                                  and CATEGORY='fee' and FEE_TYPE='fee') AS t_feeList ON 
                                    LMS.LICENSE_TYPE = t_feeList.TypeName and lms.license_subtype=  t_feeList.[SUBTYPENAME]) as tbl on lm.license_no=tbl.license_no

		
END

GO
