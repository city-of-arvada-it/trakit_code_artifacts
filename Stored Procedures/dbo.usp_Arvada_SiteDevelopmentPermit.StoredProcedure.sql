USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_SiteDevelopmentPermit]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[usp_Arvada_SiteDevelopmentPermit]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_SiteDevelopmentPermit]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_Arvada_SiteDevelopmentPermit]
	@Permit_No varchar(30)
as
/**********************************************************************************************************************
* Creator:      	 
* Date:         	 
* Title:			 
* Description:  	 
* Examples:    
			EXECUTE dbo.usp_Arvada_SiteDevelopmentPermit  '12-0310'
		exec usp_Arvada_SiteDevelopmentPermit 'site15-00125'
* Modifications:  	
  Developer Name		Date		Description
  ------------------	----------	----------------------------------------------------------- 
*
**********************************************************************************************************************/

set nocount on
 
 --------------------------
 --- Declare Variables   --
 --------------------------

 
 --------------------------
 --- Create Temp Tables  --
 --------------------------
 
 --------------------------
 --- Body of Procedure   --
 --------------------------

 
 select 
	m.permit_no,
	m.approved,
	isnull(u2.username, m.approved_by) as approved_by,
	convert(Varchar(10), convert(date, m.issued),101) as issued,
	m.site_addr,
	m.site_subdivision,
	--( 
	--	select site_Addr + ', '
	--	from [dbo].[Permit_Parcels] as parc
	--	where parc.permit_no = m.permit_no
	--	for xml path ('')
	--) as AdditionalSites,

	stuff(
	(	select ', ' + site_Addr 
		from [dbo].[Permit_Parcels] as parc
		where parc.permit_no = m.permit_no
		for xml path(''), root('MyString'), type 
	).value('/MyString[1]','nvarchar(max)') ,1,1,'') as AdditionalSites,


	m.[description],
	own.name as ownerName,
	case when (LEN(own.phone) not in (10,7))
		 then  own.phone
	 when (LEN(own.phone) = 7)
		then SUBSTRING(own.phone, 4, 3) + '-' + SUBSTRING(own.phone, 7, 4)
	else
       LEFT(own.phone, 3) + '-' + SUBSTRING(own.phone, 4, 3) + '-' + SUBSTRING(own.phone, 7, 4)
	end  as ownerPhone,
	perm.name as PermiteeName,
	case when (LEN(perm.phone) not in (10,7))
		 then  perm.phone
	 when (LEN(perm.phone) = 7)
		then SUBSTRING(perm.phone, 4, 3) + '-' + SUBSTRING(perm.phone, 7, 4)
	else
       LEFT(perm.phone, 3) + '-' + SUBSTRING(perm.phone, 4, 3) + '-' + SUBSTRING(perm.phone, 7, 4)
	end as PermiteePhone,
	u.sw_state_permit,
	u.SW_DEWATER,
	u.SW_ARMY_CORP,
	convert(Varchar(10), convert(Date, u.SW_EXP_START),101) As SW_EXP_START,
	convert(Varchar(10), convert(Date,u.SW_EXP_END),101) as SW_EXP_END,
	u.SW_ACRES2,
	u.SW_SPECIA_COND,
	u.SW_PREV_NO
from dbo.permit_main m
left join 
	(select top 1 name, phone, permit_no
	 from dbo.permit_people
	 where permit_no = @permit_no
	 and nameType = 'OWNER'
	 ) as own
	 on own.permit_no = m.permit_no
left join 
	(select top 1 name, phone, permit_no
	 from dbo.permit_people
	 where permit_no = @permit_no
	 and nameType = 'PERMITEE'
	 ) as perm
	 on perm.permit_no = m.permit_no
left join dbo.permit_udf u
	on u.permit_no = m.permit_no
left join dbo.prmry_users u2 with (nolock)
	on u2.userid = m.approved_by
where m.permit_no = @permit_no


 
GO
