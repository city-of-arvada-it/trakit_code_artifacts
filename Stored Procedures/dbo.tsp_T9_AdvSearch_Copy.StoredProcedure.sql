USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearch_Copy]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_AdvSearch_Copy]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearch_Copy]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



create PROCEDURE [dbo].[tsp_T9_AdvSearch_Copy]
	
	@SourceUserID varchar(50) = null,	-- UserID to get the search info with
	@SourceTable varchar(255) = null, -- where item is located
	@SourceSearchID int = null, -- what item to copy --> may be determined here if the source table is 'current'

	@DestinationUserID varchar(50) = null,	-- UserID to put in destination table
	@DestinationTable varchar(255) = null, -- where we want to copy the items	
	@DestinationSearchName varchar(255) = null out, -- this is the search name to store the copy as. If null it uses the source search name; returns the value that was used
	@DestinationDescription varchar(255) = null, -- this is the search description to store. If null it uses the source description. Applies to shared and saved searches only.
	@DestinationDepartmentID int = -1, -- if copying to shared, this is the associated department ID --> does NOT create a new department ID

	@RenameIfExists bit = 1, -- rename the search name to something unique for the user / dept if it already exists in the destination table. Takes priority over @OverwriteIfExists
	@OverwriteIfExists bit = 0, -- delete the existing records in the destination table for a user / dept if it already exists. example: set to 1 if user updating a saved search; set to 0 if copying to shared in search manager 

	@ErrMsg varchar(max) = '' out, -- error message to return (empty string if no errors)
	@NewItemID int = -1 out -- return the new item ID to the caller
AS
BEGIN

set @ErrMsg = '' -- initialize

declare @sqlItem nvarchar(max)
declare @sqlDetail nvarchar(max)

-- TODO: need to check to make sure that the @SourceSearchID exists in @SourceTable

if (@SourceUserID is not null) and (@SourceTable is not null) and (@DestinationUserID is not null) and (@DestinationTable is not null) -- make sure we have these parameters
begin

	-- make sure current table is not the destination table (need to use populate current for this)
	if @DestinationTable <> 'current'
	begin

	-- initialize @DestinationDescription
	if @DestinationDescription = '' set @DestinationDescription = null -- set to null if empty just in case we aren't converting empty strings to null when calling this proc
	else set @DestinationDescription = '''' + @DestinationDescription + '''' -- wrap in apostrophes

		-- get @SourceSearchID if source table is current, last or new
		if @SourceTable in ('current','last','new') set @SourceSearchID = [dbo].[tfn_T9_AdvSearch_GetSearchID](@SourceUserID, @SourceTable)

		-- make sure we have a @SourceSearchID
		if @SourceSearchID is not null
		begin

			-- if shared search, make sure the destination user has access to the department
			if @DestinationTable = 'shared'
			begin
			---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
			--    DESTINATION TABLE = SHARED SEARCH   
			---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				-- make sure we have the department ID to associate the search copy with
				if @DestinationDepartmentID is not null 
				begin
				  if @DestinationDepartmentID = 0

				   begin

					if (select count(*) from Prmry_AdvSearch_SharedItem (nolock) where  SearchName = @DestinationSearchName  ) > 0
				     begin
						declare @tempDeptID table (DepartmentID int)
						insert into @tempDeptID (DepartmentID ) select DepartmentID from Prmry_AdvSearch_SharedItem where SearchName = @DestinationSearchName
						
						delete from Prmry_AdvSearch_SharedItem where SearchName = @DestinationSearchName
						
						while ((select count(*) from @tempDeptID) > 0)
						  begin
				   			declare @deptId as int
							select top 1 @deptId= DepartmentID  from @tempDeptID 	

							-- check to see if user exists in department in Prmry_AdvSearch_DepartmentMembers
							if (select count(*) from Prmry_AdvSearch_DepartmentMembers (nolock) where UserID = @DestinationUserID) = 0
							 begin
								-- create record for user in Prmry_AdvSearch_DepartmentMembers
								insert into Prmry_AdvSearch_DepartmentMembers ([DepartmentID],[UserID],[CanGet],[CanShare],[CanRename],[CanDelete],[NotifyOfNewShare],[NotifyOfDeletedShare])
								values (@deptId,@DestinationUserID,1,0,0,0,1,1) -- default to being able to get and will be notified of changes
							end

										-- take care of the copy process here since shared uses department IDs instead of User IDs
							--item
							set @sqlItem = 'insert into Prmry_AdvSearch_SharedItem ([DepartmentID],[SearchName],[GroupName],[SearchType],[ReadOnly],[Description],[LastSearchDate],[SearchCount],[GisTable],[GisRecordID],[GeometryFilterEnabled]) 
											select ' + cast(@deptId as nvarchar) + ',''' + @DestinationSearchName + ''',GroupName,SearchType,1,' + isnull(@DestinationDescription, '[Description]') + ',null,0,[GisTable],[GisRecordID],[GeometryFilterEnabled]
											from Prmry_AdvSearch_' + @SourceTable + 'Item 
											where RecordID=' + cast(@SourceSearchID as nvarchar) + ' 
											select @NewItemID = SCOPE_IDENTITY()'
							
							print (@sqlItem)
							exec sp_executesql @sqlItem, N'@NewItemID int out', @NewItemID out

									--detail
							set @sqlDetail = 'insert into Prmry_AdvSearch_SharedDetail ([ItemID],[TableName],[FieldName],[DisplayName],[Datatype],[Filter],[Value1],[Value2],[Seq]) 
											  select ' + cast(@NewItemID as nvarchar) + ',[TableName],[FieldName],[DisplayName],[Datatype],[Filter],[Value1],[Value2],[Seq]
											  from Prmry_AdvSearch_' + @SourceTable + 'Detail 
											  where ItemID=' + cast(@SourceSearchID as nvarchar)
											  							-- check to see if a record exists already for user in Prmry_AdvSearch_DepartmentMembersItems (should never exist)
							if (select count(*) from Prmry_AdvSearch_DepartmentMembersItems (nolock) where UserID = @DestinationUserID and SharedSearchID = @NewItemID) = 0
							begin
								-- create record for user Prmry_AdvSearch_DepartmentMembersItems
								insert into Prmry_AdvSearch_DepartmentMembersItems ([DepartmentID],[UserID],[SharedSearchID],[DisplayInUserList],[LastViewedResults],[ViewCount],[Seq])
								values (@deptId,@DestinationUserID,@NewItemID,1,GETDATE(),0,0)
							end
							
							print (@sqlDetail)
							exec sp_executesql @sqlDetail
						delete from @tempDeptID where DepartmentID = @deptId
					end
				  end
				 end
				else
				begin
					-- make sure the department ID exists
					if (select count(*) from Prmry_AdvSearch_Departments (nolock) where DepartmentID = @DestinationDepartmentID) = 1 -- must be exactly 1 as it is primary key
					begin

						-- make sure the destination search name is unique
						if (select count(*) from Prmry_AdvSearch_SharedItem (nolock) where DepartmentID = @DestinationDepartmentID and SearchName = @DestinationSearchName) > 0
						begin
							-- name already exists; do we use a unique name, delete the existing search with the same name, or abort the copy?
							if @RenameIfExists = 1 set @DestinationSearchName = dbo.tfn_T9_AdvSearch_GetUniqueName(@DestinationUserID,@DestinationDepartmentID,'shared',@DestinationSearchName) -- get a unique name
							else
							begin
								if @OverwriteIfExists = 1 
								begin
									-- delete the existing search with the same name unless it is read only
									if (select count(*) from Prmry_AdvSearch_SharedItem (nolock) where DepartmentID = @DestinationDepartmentID and SearchName = @DestinationSearchName and [ReadOnly] = 1 ) > 0 set @ErrMsg = 'This search is read only.'
									else delete from Prmry_AdvSearch_SharedItem where DepartmentID = @DestinationDepartmentID and SearchName = @DestinationSearchName 
								end
								else set @ErrMsg = 'A shared search already exists with this name for this department.'
							end
						end

						if @ErrMsg = '' -- only continue if there are no errors
						begin
							-- check to see if user exists in department in Prmry_AdvSearch_DepartmentMembers
							if (select count(*) from Prmry_AdvSearch_DepartmentMembers (nolock) where UserID = @DestinationUserID) = 0
							begin
								-- create record for user in Prmry_AdvSearch_DepartmentMembers
								insert into Prmry_AdvSearch_DepartmentMembers ([DepartmentID],[UserID],[CanGet],[CanShare],[CanRename],[CanDelete],[NotifyOfNewShare],[NotifyOfDeletedShare])
								values (@DestinationDepartmentID,@DestinationUserID,1,0,0,0,1,1) -- default to being able to get and will be notified of changes
							end

							-- take care of the copy process here since shared uses department IDs instead of User IDs
							--item
							set @sqlItem = 'insert into Prmry_AdvSearch_SharedItem ([DepartmentID],[SearchName],[GroupName],[SearchType],[ReadOnly],[Description],[LastSearchDate],[SearchCount],[GisTable],[GisRecordID],[GeometryFilterEnabled]) 
											select ' + cast(@DestinationDepartmentID as nvarchar) + ',''' + @DestinationSearchName + ''',GroupName,SearchType,1,' + isnull(@DestinationDescription, '[Description]') + ',null,0,[GisTable],[GisRecordID],[GeometryFilterEnabled]
											from Prmry_AdvSearch_' + @SourceTable + 'Item 
											where RecordID=' + cast(@SourceSearchID as nvarchar) + ' 
											select @NewItemID = SCOPE_IDENTITY()'
							
							print (@sqlItem)
							exec sp_executesql @sqlItem, N'@NewItemID int out', @NewItemID out
							

							--detail
							set @sqlDetail = 'insert into Prmry_AdvSearch_SharedDetail ([ItemID],[TableName],[FieldName],[DisplayName],[Datatype],[Filter],[Value1],[Value2],[Seq]) 
											  select ' + cast(@NewItemID as nvarchar) + ',[TableName],[FieldName],[DisplayName],[Datatype],[Filter],[Value1],[Value2],[Seq]
											  from Prmry_AdvSearch_' + @SourceTable + 'Detail 
											  where ItemID=' + cast(@SourceSearchID as nvarchar)
							
							print (@sqlDetail)
							exec sp_executesql @sqlDetail
							
							-- check to see if a record exists already for user in Prmry_AdvSearch_DepartmentMembersItems (should never exist)
							if (select count(*) from Prmry_AdvSearch_DepartmentMembersItems (nolock) where UserID = @DestinationUserID and SharedSearchID = @NewItemID) = 0
							begin
								-- create record for user Prmry_AdvSearch_DepartmentMembersItems
								insert into Prmry_AdvSearch_DepartmentMembersItems ([DepartmentID],[UserID],[SharedSearchID],[DisplayInUserList],[LastViewedResults],[ViewCount],[Seq])
								values (@DestinationDepartmentID,@DestinationUserID,@NewItemID,1,GETDATE(),0,0)
							end
						end
					end
					else set @ErrMsg = 'DepartmentID must exist to receive a copy of a search'
				 end	
				end			
				else set @ErrMsg = '@DestinationDepartmentID parameter must be supplied to share a search'

			---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
			end
			else
			begin
			---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
			--    ALL OTHER DESTINATION TABLES: saved, last, new
			---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

				-- if destination table is last or new, delete out the existing records for the user
				if @DestinationTable = 'last' delete from Prmry_AdvSearch_LastItem where UserID = @DestinationUserID
				if @DestinationTable = 'new' delete from Prmry_AdvSearch_NewItem where UserID = @DestinationUserID

				-- we only have saved left, may as well hard-code the count stuff
				if @DestinationTable = 'saved'
				begin
					if (select count(*) from Prmry_AdvSearch_SavedItem (nolock) where UserID = @DestinationUserID and SearchName = @DestinationSearchName) > 0
						begin
						-- name already exists; do we use a unique name, delete the existing search with the same name, or abort the copy?
						if @RenameIfExists = 1 set @DestinationSearchName = dbo.tfn_T9_AdvSearch_GetUniqueName(@DestinationUserID,-1,'saved',@DestinationSearchName) -- get a unique name
						else
						begin
							if @OverwriteIfExists = 1
							begin
								-- delete the existing search with the same name unless it is read only
								if (select count(*) from Prmry_AdvSearch_SavedItem (nolock) where UserID = @DestinationUserID and SearchName = @DestinationSearchName and [ReadOnly] = 1 ) > 0 set @ErrMsg = 'This search is read only.'
								else delete from Prmry_AdvSearch_SavedItem where UserID = @DestinationUserID and SearchName = @DestinationSearchName
							end
							else set @ErrMsg = 'A saved search already exists with this name for this user.'
						end						
					end
				end

				if @ErrMsg = ''
				begin
					--item
					-- saved:	[UserID],[SearchName],[GroupName],[SearchType],[ReadOnly],[Description],[Seq],[CreationDate],[LastViewedResults],[ViewCount],[DisplayInUserList]
					-- last:	[UserID],[SearchName],[GroupName],[SearchType],[ReadOnly],[Description],[RefSearchID],[RefTable],[IsDirty]
					-- new:		[UserID],[SearchName],[GroupName],[SearchType],[ReadOnly],[Description],[RefSearchID],[RefTable]
					set @sqlItem = 'insert into Prmry_AdvSearch_' + @DestinationTable + 'Item '
					
					-- update column string based on columns in destination table
					-- EWH Jul 22, 2015: changed the way we set column names and values based on destination table and source table; need to get ref info from current when copying from current to last
					declare @columnNames nvarchar(max)
					declare @columnValues nvarchar(max)

					select @columnNames = case  when @DestinationTable='saved'	then '([UserID],[SearchName],[GroupName],[SearchType],[ReadOnly],[GisTable],[GisRecordID],[GeometryFilterEnabled],[Description],[Seq],[CreationDate],[LastViewedResults],[ViewCount],[DisplayInUserList])'
												when @DestinationTable='new'	then '([UserID],[SearchName],[GroupName],[SearchType],[ReadOnly],[GisTable],[GisRecordID],[GeometryFilterEnabled],[Description],[RefSearchID],[RefTable])' 
												when @DestinationTable='last'	then '([UserID],[SearchName],[GroupName],[SearchType],[ReadOnly],[GisTable],[GisRecordID],[GeometryFilterEnabled],[Description],[RefSearchID],[RefTable],[IsDirty])'
												end

					set @sqlItem = @sqlItem + @columnNames + ' select ''' + @DestinationUserID + ''','
					set @sqlItem = @sqlItem + case when @DestinationSearchName is null then '[SearchName]' else '''' + @DestinationSearchName + '''' end
					set @sqlItem = @sqlItem + ',GroupName,SearchType,0,[GisTable],[GisRecordID],[GeometryFilterEnabled],' -- default to NOT read only

					-- here @sqlItem is complete up until selecting [Description] from source table
					select @columnValues = case when @DestinationTable='saved'	then isnull(@DestinationDescription, '[Description]') + ',0,GETDATE(),null,0,1'
												when @DestinationTable='new'	then '[Description],' + cast(@SourceSearchID as nvarchar) + ',''' + @SourceTable + '''' -- use the source info as the ref info for the destination item
												when @DestinationTable='last'	then 
																					case @SourceTable when 'current' then '[Description],[RefSearchID],[RefTable],[IsDirty]' -- copy the ref info from the source as the ref info for the destination 
																													 else '[Description],' + cast(@SourceSearchID as nvarchar) + ',''' + @SourceTable + '''' -- use the source info as the ref info for the destination item
																													 end
												end
					-- add @columnValues to @sqlItem
					set @sqlItem = @sqlItem + @columnValues + ' from Prmry_AdvSearch_' + @SourceTable + 'Item 
																where RecordID=' + cast(@SourceSearchID as nvarchar) + ' 
																select @NewItemID = SCOPE_IDENTITY()'

					print (@sqlItem)
					exec sp_executesql @sqlItem, N'@NewItemID int out', @NewItemID out


					--detail
					set @sqlDetail = 'insert into Prmry_AdvSearch_' + @DestinationTable + 'Detail ([ItemID],[TableName],[FieldName],[DisplayName],[Datatype],[Filter],[Value1],[Value2],[Seq]) 
										select ' + cast(@NewItemID as nvarchar) + ',[TableName],[FieldName],[DisplayName],[Datatype],[Filter],[Value1],[Value2],[Seq]
										from Prmry_AdvSearch_' + @SourceTable + 'Detail 
										where ItemID=' + cast(@SourceSearchID as nvarchar) 
					print (@sqlDetail)
					exec sp_executesql @sqlDetail

				end
			end
		end
		else set @ErrMsg = '@SourceSearchID cannot be null'

	end
	else set @ErrMsg = '@DestinationTable cannot be the current table. Use the PopulateCurrent stored procedure to do this.'

end 
else 
begin
	set @ErrMsg = 'These parameters cannot be null:'
	if (@SourceUserID is null) set @ErrMsg = @ErrMsg + ' @SourceUserID'
	if (@SourceTable is null) set @ErrMsg = @ErrMsg + ' @SourceTable'
	if (@DestinationUserID is null) set @ErrMsg = @ErrMsg + ' @DestinationUserID'
	if (@DestinationTable is null) set @ErrMsg = @ErrMsg + ' @DestinationTable'
end

return
END
GO
