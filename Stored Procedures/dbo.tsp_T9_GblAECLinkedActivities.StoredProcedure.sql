USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_GblAECLinkedActivities]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_GblAECLinkedActivities]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_GblAECLinkedActivities]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


					  					  					  					  
-- =====================================================================
-- Revision History:
--	RB - 3/31/2014

-- =====================================================================   	
create Procedure [dbo].[tsp_T9_GblAECLinkedActivities](   
	   @ID			  varchar(20) 


)

As
 BEGIN

 Declare @SQLStatement varchar(1000) 
 Set @SQLStatement = null 
		
if @ID is not null and @ID <> ''
  Begin 
       	Set @SQLStatement = 
		   ' Select PERMIT_NO as ActivityNo, ' + CHAR(39) + 'PERMIT' + CHAR(39) + ' as ActivityGroup from Permit_Main where Permit_No in (select distinct PERMIT_NO from Permit_People where ID  = ' + CHAR(39) + 'C:' + @ID + CHAR(39) + ') UNION ' + 
		   ' Select PROJECT_NO as ActivityNo, ' + CHAR(39) + 'PROJECT' + CHAR(39) + ' as ActivityGroup from Project_Main where Project_No in (select distinct PROJECT_NO from Project_People where ID  = ' + CHAR(39) + 'C:' + @ID + CHAR(39) + ') UNION ' +		   
		   ' Select CASE_NO as ActivityNo, ' + CHAR(39) + 'CASE' + CHAR(39) + ' as ActivityGroup from Case_Main where Case_No in (select distinct CASE_NO from Case_People where ID  = ' + CHAR(39) + 'C:' + @ID + CHAR(39) + ') UNION' + 
		   ' Select LICENSE_NO as ActivityNo, ' + CHAR(39) + 'LICENSE2' + CHAR(39) + ' as ActivityGroup from License2_Main where License_No in (select distinct LICENSE_NO from License2_People where ID  = ' + CHAR(39) + 'C:' + @ID + CHAR(39) + ')'	
							 		
		if @SQLStatement is not null and @SQLStatement <> ''
		   Begin
		       EXEC(@SQLStatement)
		   End
  End  
 END

GO
