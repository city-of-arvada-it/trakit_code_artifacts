USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ModelHome_AddToPermitValuations]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_ModelHome_AddToPermitValuations]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ModelHome_AddToPermitValuations]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[tsp_T9_ModelHome_AddToPermitValuations] 
              @ModelRecordID varchar(10) = '',
              @PermitNo varchar(255) = '',
              @UserID varchar(255) = '',
                       @BuilderID varchar(255) = '',
                       @List as ModelHomeOptions readonly
              
AS
BEGIN

       Declare @SQL Varchar(8000)
       Declare @date varchar(255) = getdate()

          IF object_id('#PermitVal_Model') is not null drop table #PermitVal_Model


          Create Table #PermitVal_Model([PERMIT_NO] [varchar](30),
                     [CODE] [varchar](12),
                     [DESCRIPTION] [varchar](60),
                     [QUANTITY] [float] ,
                     [AMOUNT] [float],
                     [UNITCOST] [float],
                     [UNITS] [varchar](40) ,
                     [DETAIL] [varchar](1) ,
                     [RECORDID] [varchar](30), 
                     OptionName varchar(300))


				--RB Check if table exists before creating it
			IF object_id('#PermitVal_ModelSUM') is not null drop table #PermitVal_ModelSUM

            Create Table #PermitVal_ModelSUM([PERMIT_NO] [varchar](30),
                     [CODE] [varchar](12),
                     [DESCRIPTION] [varchar](60),
                     [QUANTITY] [float] ,
                     [AMOUNT] [float],
                     [UNITCOST] [float],
                     [UNITS] [varchar](40) ,
                     [DETAIL] [varchar](1) ,
                     [RECORDID] [varchar](30))

       if @ModelRecordID <> '' and @PermitNo <> ''
              begin
                     --Delete all previous model home valuations
                     set @SQL = 'Delete from Valuations where ActivityID = ''' + @PermitNo + ''' and code in (select code from Prmry_ModelHomeValuationCategory)'
                                  
                                   --Delete all previous model home valuations in history.
                     Delete from Permit_ModelHomeValuationHistory where permit_no = @PermitNo

                     --print(@sql)
                     exec(@sql)

                     --Add base valuations from template detail
                     set @SQL = 'Insert Into #PermitVal_Model([CODE], [UNITS],[QUANTITY], [DETAIL]  )
select ValuationCode, ''EA'', Quantity, ''0''  from Prmry_ModelHomeBaseValuations  Where ModelRecordID = ''' + @ModelRecordID + ''';'

                     --print(@sql)
                     exec(@sql)

                                  --Add options valuations from template detail
                                  Insert Into #PermitVal_Model([CODE], [UNITS],[QUANTITY], [DETAIL], OptionName  )
                                  select ValuationCode, 'EA', Quantity, '0', OptionName  from Prmry_ModelHomeOptionValuations 
                                  where  ModelRecordID = @ModelRecordID and OptionName in (Select distinct OptionName from @list)

                                  
                                  
                                  --Add permit_no
                    set @SQL = 'Update #PermitVal_Model set [PERMIT_NO] = ''' + @PermitNo+ ''';'
                                  --print(@sql)
                     exec(@sql)


                                  --Get data from valuation schedule
                                  set @SQL = 'Update #PermitVal_Model set [DESCRIPTION] = pvs.description, UNITCOST = pvs.UNITCOST from Prmry_ValuationSchedule pvs where pvs.CODE = #PermitVal_Model.CODE;'
                                  --print(@sql)
                     exec(@sql)

                                  --calc
                                  set @SQL = 'Update #PermitVal_Model set [Amount] = ([QUANTITY]*[UNITCOST]);'
                                  --print(@sql)
                     exec(@sql)


                                  

                                  
                                   Insert into #PermitVal_ModelSUM (Code, Description, Quantity, UNITCOST, Permit_NO, Units, Detail)
                                  Select  Code, Description, Sum(Quantity), UnitCost, Permit_No, Units, '0' 
                                   from #PermitVal_Model group by Code, Description, UnitCost, Permit_No, Units;

                                  
                                   
                                   Update #PermitVal_ModelSUM Set Amount = ([QUANTITY]*[UNITCOST]);

                                  
                                  --update recordid
                                  set @SQL = 'alter table #PermitVal_ModelSUM add AutoNum int identity;'
                                  --print(@sql)
                                  exec(@sql)


                                                              

                                  --set @SQL = 'alter table #PermitVal_Model add AutoNum int identity;'
                                  --print(@sql)
                                  --exec(@sql)

                                  set @UserID = @UserID + ':'
                                  set @SQL = 'Update #PermitVal_ModelSUM set [RECORDID] = ''' + @UserID + '''
                                  + right((''00'' + str(DATEPART(yy, GETDATE()))),2)
                                  + right((''00'' + ltrim(str(DATEPART(mm, GETDATE())))),2)
                                  + right((''00'' + ltrim(str(DATEPART(d, GETDATE())))),2)
                                  + right((''00'' + ltrim(str(DATEPART(hh, GETDATE())))),2)
                                  + right((''00'' + ltrim(str(DATEPART(n, GETDATE())))),2)
                                  + right((''00'' + ltrim(str(DATEPART(ss, GETDATE())))),2)
                                  + right((''0000000''+ ltrim(str([AutoNum]))),7);'

                                  --print(@sql)
                                  exec(@sql)
   
                                  set @SQL = 'alter table #PermitVal_ModelSUM drop column AutoNum;'
                                  --print(@sql)
                                  exec(@sql)

                                  --Add records to dbo.Valuations
                                  set @SQL = 'INSERT INTO dbo.Valuations
                                  (ActivityID, CODE, DESCRIPTION, QUANTITY, AMOUNT, UNITCOST, UNITS, DETAIL,RECORDID, ActivityTypeID)
                                  SELECT PERMIT_NO, CODE, DESCRIPTION, QUANTITY, AMOUNT, UNITCOST, UNITS, DETAIL, RECORDID, 1
                                  FROM #PermitVal_ModelSUM'

                                  --print(@sql)
                                  exec(@sql)

                                  insert into Permit_ModelHomeValuationHistory (Permit_no, ModelRecordID, BuilderID, OptionName, Code, [Description], Quantity, Amount, UnitCost) 
                                  select Permit_no, @ModelRecordID, @BuilderID, OptionName, Code, [Description], Quantity, Amount, UnitCost from #PermitVal_Model

                                  

              end

END

GO
