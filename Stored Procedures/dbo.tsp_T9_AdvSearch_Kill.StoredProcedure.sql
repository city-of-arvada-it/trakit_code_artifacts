USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearch_Kill]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_AdvSearch_Kill]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearch_Kill]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  -- =============================================================================================================
-- DESCRIPTION:
-- Used to delete a saved or shared item. 
-- This will delete both the item and detail because of the cascade delete constraint.
--
-- REVISION HISTORY: 
--		 2015 MAY 14 - EWH - Initial version
-- =============================================================================================================
create PROCEDURE [dbo].[tsp_T9_AdvSearch_Kill]
	@UserID varchar(50) = null,
	@SearchID int = null,
	@SearchTable varchar(50) = null, -- saved / shared
	@EnforceUserPrivileges bit = 1 -- make sure user has access to delete records

AS
BEGIN

declare @OkToDelete bit
set @OkToDelete = 0

	
if @EnforceUserPrivileges = 1
begin

	-- TODO: make sure user has access to these records
	if @SearchTable ='saved'
	begin
		

		set @OkToDelete = 1


	end
	else
	begin

		-- TODO: make sure user has access to delete shared records for this department
		if @SearchTable ='shared'
		begin
			

			set @OkToDelete = 1


		end
		else
		begin
			set @OkToDelete = 0 -- unexpected table value
		end

	end
end
else
begin
	set @OkToDelete = 1 -- we didn't want to enforce privileges
end


if @OkToDelete = 1
begin
	declare @sql varchar(300)
	set @sql = 'delete from Prmry_AdvSearch_' + @SearchTable + 'Item where RecordID=''' + @SearchID + ''
	exec @sql
end

END


GO
