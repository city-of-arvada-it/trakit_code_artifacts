USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_SetEtrakitAECExpired]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[usp_Arvada_SetEtrakitAECExpired]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_SetEtrakitAECExpired]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_Arvada_SetEtrakitAECExpired]
as

/*
08/12/2020 : Per emails w\ Josie, Brita and Sandy Bowman
Trakit allows a contractor who has an expired status with the City to schedule inspections
and that is frustrating for City employees.
We thought about adjusting the permit status but that has larger implications.
Instead we inactivate the etrakit account every hour via a sql agent job.
The job also activates the account if the status is active.

create table dbo.etrakitExpireLog
(
	EtrakitUserAccount varchar(50),
	StatusSet varchar(15),
	SetDate datetime not null constraint df_etrakitExpireLog_SetDate default getdate()
)
*/

insert into etrakitExpireLog
(
	EtrakitUserAccount,
	StatusSet
)
select
	a.EtrakIt_Name,
	'Inactivated'
from EtrakIt_User_Accounts a
inner join dbo.aec_main b
	on b.ST_LIC_NO = a.EtrakIt_Name
where b.[status] = 'Expired'
and etrakit_name like 'AEC%'
and isnull(a.active,0) = 1
union  
--or their insurance is not active
select 
	a.st_lic_no, 
--	nametype, 
	'Inactivated'
from AEC_INSURANCE a
inner join EtrakIt_User_Accounts b
	on a.ST_LIC_NO = b.EtrakIt_Name
where NAMETYPE in ('bond','liability','WORKMAN''S COMP.')
and isnull(b.active,0) = 1
group by 
	a.st_lic_no
--	nametype
having 
	max(isnull(a.policy_expire, getdate())) < convert(date,getdate())
 

--Expired AEC records, active etrakit acct
update a
set active = 0
from EtrakIt_User_Accounts a
inner join dbo.aec_main b
	on b.ST_LIC_NO = a.EtrakIt_Name
where b.[status] = 'Expired'
and etrakit_name like 'AEC%'
and isnull(a.active,0) = 1

--active AEC record, inactive etrakit account
insert into etrakitExpireLog
(
	EtrakitUserAccount,
	StatusSet
)
select
	a.EtrakIt_Name,
	'Activated'
from EtrakIt_User_Accounts a
inner join dbo.aec_main b
	on b.ST_LIC_NO = a.EtrakIt_Name
where b.[status] = 'Active'
and etrakit_name like 'AEC%'
and isnull(a.active,0) = 0
--cannot have expired insurances:
and not exists(
				select 
					a2.st_lic_no, 
				--	nametype, 
					max(isnull(a2.policy_expire, getdate())) as PolicyExpire
				from AEC_INSURANCE a2
				inner join EtrakIt_User_Accounts b2
					on a2.ST_LIC_NO = b2.EtrakIt_Name
				where a2.NAMETYPE in ('bond','liability','WORKMAN''S COMP.')
				and isnull(b2.active,0) = 1 
				and b2.EtrakIt_Name = a.EtrakIt_Name
				group by 
					a2.st_lic_no
				--	nametype
				having 
					max(isnull(a2.policy_expire, getdate())) < convert(date,getdate())
				)
 				

update a
set active = 1
from EtrakIt_User_Accounts a
inner join dbo.aec_main b
	on b.ST_LIC_NO = a.EtrakIt_Name
where b.[status] = 'Active'
and etrakit_name like 'AEC%'
and isnull(a.active,0) = 0

GO
