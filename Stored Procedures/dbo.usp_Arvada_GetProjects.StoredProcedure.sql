USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_GetProjects]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[usp_Arvada_GetProjects]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_GetProjects]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_Arvada_GetProjects]
	@planners nvarchar(max),
	@Status nvarchar(max) 

as
begin

/*
exec dbo.usp_Arvada_GetProjects 'Carol Ibanez','ACTIVE|1ST REVIEW|ON HOLD'
*/
set nocount on
 

select distinct 
	project_no
from dbo.project_main a
inner join dbo.tfn_Arvada_SSRS_String_To_Table2(@planners,'|',1) as p
	on p.Val = a.PLANNER
inner join dbo.tfn_Arvada_SSRS_String_To_Table2(@Status,'|',1) as s
	on s.Val = a.STATUS
where 
	a.PARENT_PROJECT_NO is null
	or a.PARENT_PROJECT_NO = ''
	or a.PARENT_PROJECT_NO not like 'DA%'
 

end
 
GO
