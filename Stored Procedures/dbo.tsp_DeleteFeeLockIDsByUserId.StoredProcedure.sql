USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_DeleteFeeLockIDsByUserId]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_DeleteFeeLockIDsByUserId]
GO
/****** Object:  StoredProcedure [dbo].[tsp_DeleteFeeLockIDsByUserId]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[tsp_DeleteFeeLockIDsByUserId]
	@userid Varchar(255) = Null
AS
BEGIN
	SET NOCOUNT ON;

	Update Fees set LOCKID = Null where LOCKID like @userid + ':%'
	Update SubFees set LOCKID = Null where LOCKID like @userid + ':%'

END

GO
