USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_SetSpecialEventPermitStatus]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[usp_Arvada_SetSpecialEventPermitStatus]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_SetSpecialEventPermitStatus]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_Arvada_SetSpecialEventPermitStatus]

as

begin

	set nocount on

	/*
	We use a field in the Permit_UDF table for the event end date, can you use that date to update the status to CLOSED.

	Event date is entered in permit_udf table as SED_END. Please enter the status of CLOSED if the SED_END date is less than today.

	If the status is currently in EXPIRED, DENIED or VOID do not update.

	If the SED_END field is null and the permit issuance date is older than today update the status to Closed.
	begin tran
	exec usp_Arvada_SetSpecialEventPermitStatus
	commit  
	*/


	declare 
			@s int,
			@e int,
			@permit varchar(25),
			@Status varchar(100)

	if object_id('tempdb.dbo.#createAudit') is not null drop table #createAudit
	create table #createAudit
	(
		PERMIT_NO varchar(25) not null,
		[status] varchar(100),
		rid int identity(1,1)
	)

	insert into #createAudit
	(
		permit_no,
		[status]
	)
	select 
		m.PERMIT_NO,
		[status]
	from dbo.permit_main m
	left join dbo.permit_udf u
		on u.permit_no = m.permit_no
	where PermitType in 
		('SPEC EVENT'
		)
	and isnull(u.SED_END, m.ISSUED) < convert(date, getdate())
	and ltrim(rtrim([status])) not in ('CLOSED', 'DENIED','EXPIRED','VOID','FINALED')




	select 
		@s = min(rid),
		@e = max(rid)
	from #createAudit

	while @s <= @e
	begin
	
		select @permit = permit_no,
				@Status = [status]
		from #createAudit
		where rid = @s

		update m
		set  
			Expired_by = 'CRW',
			[Status] = 'CLOSED'
		from dbo.permit_main m
		where permit_no = @permit

	 
		 Insert into Prmry_AuditTrail (TRANSACTION_DATETIME, USER_ID, TABLE_NAME, FIELD_NAME, PRIMARY_KEY_VALUE, SQL_ACTION, ORIGINAL_VALUE, CURRENT_VALUE) 
		 select getdate(), 'CRW', 'Permit_Main', 'STATUS', @permit, 'U', @Status, 'CLOSED'



		set @s = @s + 1
	end


end 
GO
