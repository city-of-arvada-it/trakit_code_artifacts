USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[lp_focus_80prct_12fam_30days_review]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[lp_focus_80prct_12fam_30days_review]
GO
/****** Object:  StoredProcedure [dbo].[lp_focus_80prct_12fam_30days_review]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 CREATE PROCEDURE [dbo].[lp_focus_80prct_12fam_30days_review]
		@APPLIED_FROM datetime = null, -- date sent
		@APPLIED_TO datetime = NULL --date returned
as
begin
/* 
--Ychavez 3/11/2014 - FOR FOCUS Measures report
-------------------------------------------------------------------------------------------
--80% 0% of new 1-2 family home plan reviews completed within 30 business days
--Measure 1--
--Permit Type: New Single Family
--Sub Types: NEW SF DETACHED, NEW DUPLEX, NEW SF ATTACHED
--Review Type: BUILDING PERMIT
--Measure from date sent to date returned
exec lp_focus_80prct_12fam_30days_review @APPLIED_FROM = '11/01/2014' , @APPLIED_TO = '11/30/2014'
-------------------------------------------------------------------------------------------
*/
--------------------------------------------------------------------------------
--80% of 1-2 family home plan reviews completed within 30 business days.--
--CALCULATE BY APPLIED - DATE_RECEIVED  select * from #TMP_CALC_FAM_PLAN_TOTALS  
-----------------------------------------------------------------------------
--DROP TABLE #TMP_CALC_FAM_PLAN_TOTALS
SELECT DATEDIFF(DAY, r.DATE_SENT, R.DATE_RECEIVED) AS DATE_DIFF, DATE_SENT,R.DATE_RECEIVED,  P.PERMIT_NO
, CASE WHEN DATEDIFF(DAY, r.DATE_SENT, DATE_RECEIVED) < 31  THEN 'WITHIN MEASURE' 
 WHEN  DATEDIFF(DAY, r.DATE_SENT, DATE_RECEIVED) >= 32 THEN 'OUT OF MEASURE'
 ELSE 'NOT CALCULATED'
 END AS MEASURE
INTO #TMP_CALC_FAM_PLAN_TOTALS
 FROM PERMIT_MAIN P
JOIN PERMIT_REVIEWS R ON P.PERMIT_NO = R.pERMIT_NO AND REVIEWTYPE = 'BUILDING PERMIT'
WHERE PERMITSUBTYPE IN ('NEW SF DETACHED', 'NEW SF ATTACHED', 'NEW DUPLEX')
--AND r.DATE_SENT >=  @APPLIED_FROM AND R.DATE_RECEIVED <= @APPLIED_TO 
AND  APPLIED BETWEEN  '10/01/2014' And '10/31/2014'

-----------------------------------------------------------------------------------------
--DROP TABLE #tmp_calc_measure_tot select * from #tmp_calc_measure_tot
-----------------------------------------------------------------------------------------
select SUM(case when MEASURE= 'WITHIN MEASURE' THEN 1 ELSE 0 END) IN_MEASURE_COUNT
 ,SUM(case when MEASURE = 'OUT OF MEASURE' THEN 1 ELSE 0 END) OUT_OF_MEASURE_COUNT
 ,SUM(CASE WHEN PERMIT_NO LIKE '%-%' THEN 1 END) TOTAL_PERMITS
 into #tmp_calc_measure_tot
  from #TMP_CALC_FAM_PLAN_TOTALS
  where MEASURE != 'NOT CALCULATED'
  
 -----------------------------------------------------------------------------------------
--
-----------------------------------------------------------------------------------------

select SUM(cast(IN_MEASURE_COUNT as decimal)) / Sum(cast(total_permits as decimal)) * 100 
as prcnt_plan_review15, total_permits, IN_MEASURE_COUNT
from #tmp_calc_measure_tot
group by total_permits, IN_MEASURE_COUNT


end
GO
