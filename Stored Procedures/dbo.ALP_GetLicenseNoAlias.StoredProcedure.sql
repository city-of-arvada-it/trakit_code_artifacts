USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[ALP_GetLicenseNoAlias]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[ALP_GetLicenseNoAlias]
GO
/****** Object:  StoredProcedure [dbo].[ALP_GetLicenseNoAlias]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- ========================================================================================
-- Author:		Erick Hampton
-- Create date: Dec 11, 2016
-- Description:	Get the alias for license_no for the specified shared search
-- ========================================================================================
CREATE PROCEDURE [dbo].[ALP_GetLicenseNoAlias]

	@SearchID int = -1

AS
BEGIN
	SET NOCOUNT ON;

	-- get the alias for license_no for the shared search
	declare @license_no_alias varchar(255) = ''
	select top 1 @license_no_alias=DisplayName from [Prmry_AdvSearch_SharedDetail] where ItemID=@SearchID and TableName='LICENSE2_MAIN' and FieldName='LICENSE_NO' 
	if @license_no_alias='' or @license_no_alias is null raiserror('LICENSE_NO is not a column in the specified shared search',16,1) with nowait

	-- return the alias
	select @license_no_alias

END


GO
