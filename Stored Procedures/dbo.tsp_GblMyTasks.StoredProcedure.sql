USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GblMyTasks]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_GblMyTasks]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GblMyTasks]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  					  
					  					  					  -- =============================================
-- Author:		Vance Bradshaw, CRW Systems, Inc
-- Create date: November 2011
-- Description:	My Tasks
--	Returns items due during the range selected
--	or overdue
--	from each of the modules and functional areas
-- 12/1/2011 - added NOTES
-- =============================================
CREATE PROCEDURE [dbo].[tsp_GblMyTasks]
	@USERID varchar(6) = 'CRW',
	@START_DATE datetime = '1/1/1900',
	@END_DATE datetime = '1/1/1900'

AS
BEGIN
	SET NOCOUNT ON;
	-----------------------
	-- INSPECTIONS
	-----------------------
	-- Permit Inspections
	select top 25 --temp logic by mdp to limit total results
		'PERMIT' as MODULE, 
		'INSPECTION' as CATEGORY,
		S.INSPECTIONTYPE as CAT_TYPE,
		S.PERMIT_NO as RECORD_NO,
		M.PERMITTYPE as RECORD_TYPE,
		M.STATUS as RECORD_STATUS,
		M.SITE_ADDR as SITE_ADDR,
		M.LOC_RECORDID as LOC_RECORDID,
		M.SITE_CITY as SITE_CITY,
		S.INSPECTOR as ASSIGNEE,
		S.SCHEDULED_DATE as DUE_DATE,
		S.NOTES as NOTES,
		S.RECORDID as RECORDID 
	from Permit_Inspections as S, Permit_Main as M
		where S.INSPECTOR = @USERID
		-- Due in the Range Provided
		and (S.SCHEDULED_DATE between @START_DATE and @END_DATE
		-- or Overdue
			or S.SCHEDULED_DATE < CAST(FLOOR(CAST(getdate() AS float)) AS DATETIME))
		and S.COMPLETED_DATE is null
		and S.PERMIT_NO = M.PERMIT_NO
UNION		
	-- Project Inspections
	select top 25 --temp logic by mdp to limit total results
		'PROJECT' as MODULE, 
		'INSPECTION' as CATEGORY,
		S.INSPECTIONTYPE as CAT_TYPE,
		S.PROJECT_NO as RECORD_NO,
		M.PROJECTTYPE as RECORD_TYPE,
		M.STATUS as RECORD_STATUS,
		M.SITE_ADDR as SITE_ADDR,
		M.LOC_RECORDID as LOC_RECORDID,
		M.SITE_CITY as SITE_CITY,
		S.INSPECTOR as ASSIGNEE,
		S.SCHEDULED_DATE as DUE_DATE,
		S.NOTES as NOTES,
		S.RECORDID as RECORDID 
	from Project_Inspections as S, Project_Main as M
		where S.INSPECTOR = @USERID
		-- Due in the Range Provided
		and (S.SCHEDULED_DATE between @START_DATE and @END_DATE
		-- or Overdue
			or S.SCHEDULED_DATE < CAST(FLOOR(CAST(getdate() AS float)) AS DATETIME))
		and S.COMPLETED_DATE is null
		and S.PROJECT_NO = M.PROJECT_NO
UNION		
	-- Case Inspections
	select top 25 --temp logic by mdp to limit total results
		'CASE' as MODULE, 
		'INSPECTION' as CATEGORY,
		S.INSPECTIONTYPE as CAT_TYPE,
		S.CASE_NO as RECORD_NO,
		M.CASETYPE as RECORD_TYPE,
		M.STATUS as RECORD_STATUS,
		M.SITE_ADDR as SITE_ADDR,
		M.LOC_RECORDID as LOC_RECORDID,
		M.SITE_CITY as SITE_CITY,
		S.INSPECTOR as ASSIGNEE,
		S.SCHEDULED_DATE as DUE_DATE,
		S.NOTES as NOTES,
		S.RECORDID as RECORDID 
	from Case_Inspections as S, Case_Main as M
		where S.INSPECTOR = @USERID		
		-- Due in the Range Provided
		and (S.SCHEDULED_DATE between @START_DATE and @END_DATE
		-- or Overdue
			or S.SCHEDULED_DATE < CAST(FLOOR(CAST(getdate() AS float)) AS DATETIME))
		and S.COMPLETED_DATE is null
		and S.CASE_NO = M.CASE_NO
--UNION		---commented out by mdp ---module not built yet
--	-- License Inspections
--	select top 25 --temp logic by mdp to limit total results
--		'LICENSE' as MODULE, 
--		'INSPECTION' as CATEGORY,
--		S.INSPECTIONTYPE as CAT_TYPE,
--		S.BUS_LIC_NO as RECORD_NO,
--		B.BUSINESS_TYPE as RECORD_TYPE,
--		M.STATUS as RECORD_STATUS,
--		B.LOC_ADDRESS1 as SITE_ADDR,
--		B.LOC_RECORDID as LOC_RECORDID,
--		B.SITE_CITY as SITE_CITY,
--		S.INSPECTOR as ASSIGNEE,
--		S.SCHEDULED_DATE as DUE_DATE,
--		S.NOTES as NOTES,
--		S.RECORDID as RECORDID 
--	from License_Inspections as S, License_Main as M, License_Business as B
--		where S.INSPECTOR = @USERID
--		-- Due in the Range Provided
--		and (S.SCHEDULED_DATE between @START_DATE and @END_DATE
--		-- or Ovedue
--			or S.SCHEDULED_DATE < CAST(FLOOR(CAST(getdate() AS float)) AS DATETIME))
--		and S.COMPLETED_DATE is null
--		and S.BUS_LIC_NO = M.BUS_LIC_NO
--		and M.BUSINESS_NO = B.BUSINESS_NO
UNION		
	-----------------------
	-- REVIEWS
	-----------------------
	-- Permit Reviews
	select top 25 --temp logic by mdp to limit total results
		'PERMIT' as MODULE, 
		'REVIEW' as CATEGORY,
		S.REVIEWTYPE as CAT_TYPE,
		S.PERMIT_NO as RECORD_NO,
		M.PERMITTYPE as RECORD_TYPE,
		M.STATUS as RECORD_STATUS,
		M.SITE_ADDR as SITE_ADDR,
		M.LOC_RECORDID as LOC_RECORDID,
		M.SITE_CITY as SITE_CITY,
		S.CONTACT as ASSIGNEE,
		S.DATE_DUE as DUE_DATE,
		S.NOTES as NOTES,
		S.RECORDID as RECORDID 
	from Permit_Reviews as S, Permit_Main as M
		where (S.CONTACT = @USERID
			or left(S.CONTACT,24) = (select max(left(USERNAME,24)) from Prmry_Users where USERID = @USERID))
		-- Due in the Range Provided
		and (S.DATE_DUE between @START_DATE and @END_DATE
		-- or Overdue
			or S.DATE_DUE < CAST(FLOOR(CAST(getdate() AS float)) AS DATETIME))
		and S.DATE_RECEIVED is null
		and S.PERMIT_NO = M.PERMIT_NO
UNION		
	-- Project Reviews
	select top 25 --temp logic by mdp to limit total results
		'PROJECT' as MODULE, 
		'REVIEW' as CATEGORY,
		S.REVIEWTYPE as CAT_TYPE,
		S.PROJECT_NO as RECORD_NO,
		M.PROJECTTYPE as RECORD_TYPE,
		M.STATUS as RECORD_STATUS,
		M.SITE_ADDR as SITE_ADDR,
		M.LOC_RECORDID as LOC_RECORDID,
		M.SITE_CITY as SITE_CITY,
		S.CONTACT as ASSIGNEE,
		S.DATE_DUE as DUE_DATE,
		S.NOTES as NOTES,
		S.RECORDID as RECORDID 
	from Project_Reviews as S, Project_Main as M
		where (S.CONTACT = @USERID
			or left(S.CONTACT,24) = (select max(left(USERNAME,24)) from Prmry_Users where USERID = @USERID))
		-- Due in the Range Provided
		and (S.DATE_DUE between @START_DATE and @END_DATE
		-- or Overdue
			or S.DATE_DUE < CAST(FLOOR(CAST(getdate() AS float)) AS DATETIME))
		and S.DATE_RECEIVED is null
		and S.PROJECT_NO = M.PROJECT_NO
--UNION		---commented out by mdp ---module not built yet
--	-- License Reviews
--	select top 25 --temp logic by mdp to limit total results
--		'LICENSE' as MODULE, 
--		'REVIEW' as CATEGORY,
--		S.REVIEWTYPE as CAT_TYPE,
--		S.BUS_LIC_NO as RECORD_NO,
--		B.BUSINESS_TYPE as RECORD_TYPE,
--		M.STATUS as RECORD_STATUS,
--		B.LOC_ADDRESS1 as SITE_ADDR,
--		B.LOC_RECORDID as LOC_RECORDID,
--		B.SITE_CITY as SITE_CITY,
--		S.CONTACT as ASSIGNEE,
--		S.DATE_DUE as DUE_DATE,
--		S.NOTES as NOTES,
--		S.RECORDID as RECORDID 
--	from License_Reviews as S, License_Main as M, License_Business as B
--		where (S.CONTACT = @USERID
--			or left(S.CONTACT,24) = (select max(left(USERNAME,24)) from Prmry_Users where USERID = @USERID))
--		-- Due in the Range Provided
--		and (S.DATE_DUE between @START_DATE and @END_DATE
--		-- or Overdue
--			or S.DATE_DUE < CAST(FLOOR(CAST(getdate() AS float)) AS DATETIME))
--		and S.DATE_RECEIVED is null
--		and S.BUS_LIC_NO = M.BUS_LIC_NO
--		and M.BUSINESS_NO = B.BUSINESS_NO
UNION		
	-----------------------
	-- CHRONOLOGY ACTIONS
	-----------------------
	-- Permit Actions
	select top 25 --temp logic by mdp to limit total results
		'PERMIT' as MODULE, 
		'ACTION' as CATEGORY,
		S.ACTION_TYPE as CAT_TYPE,
		S.PERMIT_NO as RECORD_NO,
		M.PERMITTYPE as RECORD_TYPE,
		M.STATUS as RECORD_STATUS,
		M.SITE_ADDR as SITE_ADDR,
		M.LOC_RECORDID as LOC_RECORDID,
		M.SITE_CITY as SITE_CITY,
		S.ACTION_BY as ASSIGNEE,
		S.ACTION_DATE as DUE_DATE,
		S.ACTION_DESCRIPTION as NOTES,
		S.RECORDID as RECORDID 
	from Permit_Actions as S, Permit_Main as M
		where (S.ACTION_BY = @USERID
			or left(S.ACTION_BY,30) = (select max(left(USERNAME,30)) from Prmry_Users where USERID = @USERID))
		-- Due in the Range Provided
		and (S.ACTION_DATE between @START_DATE and @END_DATE
		-- or Overdue
			or S.ACTION_DATE < CAST(FLOOR(CAST(getdate() AS float)) AS DATETIME))
		and S.COMPLETED_DATE is null
		and S.PERMIT_NO = M.PERMIT_NO
UNION		
	-- Project Actions
	select top 25 --temp logic by mdp to limit total results
		'PROJECT' as MODULE, 
		'ACTION' as CATEGORY,
		S.ACTION_TYPE as CAT_TYPE,
		S.PROJECT_NO as RECORD_NO,
		M.PROJECTTYPE as RECORD_TYPE,
		M.STATUS as RECORD_STATUS,
		M.SITE_ADDR as SITE_ADDR,
		M.LOC_RECORDID as LOC_RECORDID,
		M.SITE_CITY as SITE_CITY,
		S.ACTION_BY as ASSIGNEE,
		S.ACTION_DATE as DUE_DATE,
		S.ACTION_DESCRIPTION as NOTES,
		S.RECORDID as RECORDID 
	from Project_Actions as S, Project_Main as M
		where (S.ACTION_BY = @USERID
			or left(S.ACTION_BY,30) = (select max(left(USERNAME,30)) from Prmry_Users where USERID = @USERID))
		-- Due in the Range Provided
		and (S.ACTION_DATE between @START_DATE and @END_DATE
		-- or Overdue
			or S.ACTION_DATE < CAST(FLOOR(CAST(getdate() AS float)) AS DATETIME))
		and S.COMPLETED_DATE is null
		and S.PROJECT_NO = M.PROJECT_NO
UNION		
	-- Case Actions
	select top 25 --temp logic by mdp to limit total results
		'CASE' as MODULE, 
		'ACTION' as CATEGORY,
		S.ACTION_TYPE as CAT_TYPE,
		S.CASE_NO as RECORD_NO,
		M.CASETYPE as RECORD_TYPE,
		M.STATUS as RECORD_STATUS,
		M.SITE_ADDR as SITE_ADDR,
		M.LOC_RECORDID as LOC_RECORDID,
		M.SITE_CITY as SITE_CITY,
		S.ACTION_BY as ASSIGNEE,
		S.ACTION_DATE as DUE_DATE,
		S.ACTION_DESCRIPTION as NOTES,
		S.RECORDID as RECORDID 
	from Case_Actions as S, Case_Main as M
		where (S.ACTION_BY = @USERID
			or left(S.ACTION_BY,30) = (select max(left(USERNAME,30)) from Prmry_Users where USERID = @USERID))
		-- Due in the Range Provided
		and (S.ACTION_DATE between @START_DATE and @END_DATE
		-- or Overdue
			or S.ACTION_DATE < CAST(FLOOR(CAST(getdate() AS float)) AS DATETIME))
		and S.COMPLETED_DATE is null
		and S.CASE_NO = M.CASE_NO 
--UNION				---commented out by mdp ---module not built yet
--	-- AEC Actions
--	select top 25 --temp logic by mdp to limit total results
--		'AEC' as MODULE, 
--		'ACTION' as CATEGORY,
--		S.ACTION_TYPE as CAT_TYPE,
--		S.ST_LIC_NO as RECORD_NO,
--		M.AECTYPE as RECORD_TYPE,
--		M.STATUS as RECORD_STATUS,
--		M.ADDRESS1 as SITE_ADDR,
--		'' as LOC_RECORDID,
--		M.CITY as SITE_CITY,
--		S.ACTION_BY as ASSIGNEE,
--		S.ACTION_DATE as DUE_DATE,
--		S.ACTION_DESCRIPTION as NOTES,
--		S.RECORDID as RECORDID 
--	from AEC_Actions as S, AEC_Main as M
--		where (S.ACTION_BY = @USERID
--			or left(S.ACTION_BY,30) = (select max(left(USERNAME,30)) from Prmry_Users where USERID = @USERID))
--		-- Due in the Range Provided
--		and (S.ACTION_DATE between @START_DATE and @END_DATE
--		-- or Overdue
--			or S.ACTION_DATE < CAST(FLOOR(CAST(getdate() AS float)) AS DATETIME))
--		and S.COMPLETED_DATE is null
--		and S.ST_LIC_NO = M.ST_LIC_NO
--UNION		---commented out by mdp ---module not built yet
--	-- LICENSE Actions
--	select top 25 --temp logic by mdp to limit total results
--		'LICENSE' as MODULE, 
--		'ACTION' as CATEGORY,
--		S.ACTION_TYPE as CAT_TYPE,
--		S.BUS_LIC_NO as RECORD_NO,
--		B.BUSINESS_TYPE as RECORD_TYPE,
--		M.STATUS as RECORD_STATUS,
--		B.LOC_ADDRESS1 as SITE_ADDR,
--		B.LOC_RECORDID as LOC_RECORDID,
--		B.SITE_CITY as SITE_CITY,
--		S.ACTION_BY as ASSIGNEE,
--		S.ACTION_DATE as DUE_DATE,
--		S.ACTION_DESCRIPTION as NOTES,
--		S.RECORDID as RECORDID 
--	from License_Actions as S, License_Main as M, License_Business as B
--		where (S.ACTION_BY = @USERID
--			or left(S.ACTION_BY,30) = (select max(left(USERNAME,30)) from Prmry_Users where USERID = @USERID))
--		-- Due in the Range Provided
--		and (S.ACTION_DATE between @START_DATE and @END_DATE
--		-- or Overdue
--			or S.ACTION_DATE < CAST(FLOOR(CAST(getdate() AS float)) AS DATETIME))
--		and S.COMPLETED_DATE is null
--		and S.BUS_LIC_NO = M.BUS_LIC_NO
--		and M.BUSINESS_NO = B.BUSINESS_NO
UNION		
	-----------------------
	-- CodeTRAK Cases by Followup Date
	-----------------------
	select top 25 --temp logic by mdp to limit total results
		'CASE' as MODULE, 
		'FOLLOWUP' as CATEGORY,
		CASETYPE as CAT_TYPE,
		CASE_NO as RECORD_NO,
		CASETYPE as RECORD_TYPE,
		STATUS as RECORD_STATUS,
		SITE_ADDR as SITE_ADDR,
		LOC_RECORDID as LOC_RECORDID,
		SITE_CITY as SITE_CITY,
		ASSIGNED_TO as ASSIGNEE,
		FOLLOWUP as DUE_DATE,
		DESCRIPTION as NOTES,
		RECORDID as RECORDID 
	from Case_Main
		where left(ASSIGNED_TO,30) = (select max(left(USERNAME,30)) from Prmry_Users where USERID = @USERID)
		and FOLLOWUP between @START_DATE and @END_DATE
--UNION		---commented out by mdp ---module not built yet
--	-----------------------
--	-- CRM Issues by Due Date Date
--	-----------------------
--	select top 25 --temp logic by mdp to limit total results
--		'CRM' as MODULE, 
--		'ISSUE' as CATEGORY,
--		TL.LIST_TEXT as CAT_TYPE,
--		M.ISSUE_LABEL as RECORD_NO,
--		TL.LIST_TEXT as RECORD_TYPE,
--		SL.LIST_TEXT as RECORD_STATUS,
--		M.ISSUE_ADDRESS as SITE_ADDR,
--		M.ISSUE_LOC_RECORDID as LOC_RECORDID,
--		M.ISSUE_CITY as SITE_CITY,
--		M.ASSIGNED_USER_ID as ASSIGNEE,
--		M.DUE_DATE as DUE_DATE,
--		M.NOTES as NOTES,
--		M.RECORDID as RECORDID 
--	from CRM_Issues as M, CRM_Lists as TL, CRM_Lists as SL
--		where M.ASSIGNED_USER_ID = @USERID
--		-- Due in the Range Provided
--		and (M.DUE_DATE between @START_DATE and @END_DATE
--		-- or Overdue
--			or M.DUE_DATE < CAST(FLOOR(CAST(getdate() AS float)) AS DATETIME))
--		and M.COMPLETED_DATE is null
--		and TL.LIST_CATEGORY = '2'
--		and TL.LIST_ID = M.NATURETYPE_LIST_ID
--		and SL.LIST_CATEGORY = '5'
--		and SL.LIST_ID = M.STATUS_LIST_ID	
UNION		
	-----------------------
	-- CONDITIONS
	-----------------------
	-- PERMIT Conditions
	select top 25 --temp logic by mdp to limit total results
		'PERMIT' as MODULE, 
		'CONDITION' as CATEGORY,
		S.CONDITION_TYPE as CAT_TYPE,
		S.PERMIT_NO as RECORD_NO,
		M.PERMITTYPE as RECORD_TYPE,
		M.STATUS as RECORD_STATUS,
		M.SITE_ADDR as SITE_ADDR,
		M.LOC_RECORDID as LOC_RECORDID,
		M.SITE_CITY as SITE_CITY,
		S.CONTACT as ASSIGNEE,
		S.DATE_REQUIRED as DUE_DATE,
		S.CONDITION_NOTES as NOTES,
		S.RECORDID as RECORDID 
	from Permit_Conditions2 as S, Permit_Main as M
		where (S.CONTACT = @USERID
			or left(S.CONTACT,80) = (select max(left(USERNAME,80)) from Prmry_Users where USERID = @USERID))
		-- Due in the Range Provided
		and (S.DATE_REQUIRED between @START_DATE and @END_DATE
		-- or Overdue
			or S.DATE_REQUIRED < CAST(FLOOR(CAST(getdate() AS float)) AS DATETIME))
		and S.DATE_SATISFIED is null
		and S.PERMIT_NO = M.PERMIT_NO
UNION		
	-- Project Conditions
	select top 25 --temp logic by mdp to limit total results
		'PROJECT' as MODULE, 
		'CONDITION' as CATEGORY,
		S.CONDITION_TYPE as CAT_TYPE,
		S.PROJECT_NO as RECORD_NO,
		M.PROJECTTYPE as RECORD_TYPE,
		M.STATUS as RECORD_STATUS,
		M.SITE_ADDR as SITE_ADDR,
		M.LOC_RECORDID as LOC_RECORDID,
		M.SITE_CITY as SITE_CITY,
		S.CONTACT as ASSIGNEE,
		S.DATE_REQUIRED as DUE_DATE,
		S.CONDITION_NOTES as NOTES,
		S.RECORDID as RECORDID 
	from Project_Conditions2 as S, Project_Main as M
		where (S.CONTACT = @USERID
			or left(S.CONTACT,80) = (select max(left(USERNAME,80)) from Prmry_Users where USERID = @USERID))
		-- Due in the Range Provided
		and (S.DATE_REQUIRED between @START_DATE and @END_DATE
		-- or Overdue
			or S.DATE_REQUIRED < CAST(FLOOR(CAST(getdate() AS float)) AS DATETIME))
		and S.DATE_SATISFIED is null
		and S.PROJECT_NO = M.PROJECT_NO		
		
ORDER BY DUE_DATE ;
		
END











GO
