USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearchByGroup]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_AdvSearchByGroup]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearchByGroup]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					                                     
-- ==========================================================================================
-- Revision History:
--       2014-03-03 - mdm - added to sourcesafe
--		 2014 NOV 24 - EWH - added filter for tables/columns in Prmry_AdvSearchExclusions 
--		 2014 DEC 09 - EWH - Updated column widths in temp table to 255 from 30
-- ==========================================================================================   

/*
GO

DECLARE	@return_value int

EXEC	@return_value = [dbo].[tsp_T9_AdvSearchByGroup]
		@Group = N'permit'

SELECT	'Return Value' = @return_value

GO
*/

CREATE PROCEDURE [dbo].[tsp_T9_AdvSearchByGroup] (
       @Group Varchar(50) = Null  
)

AS
BEGIN

Declare @SQL Varchar(8000)
Declare @WhereClause varchar(8000)

Select @whereclause =
CASE WHEN @Group = 'Geo' THEN 
              'Geo_Inspections''' + ',' + '''GEO_INSPECTIONS_UDF''' + ',' + '''Geo_Ownership''' + ',' + '''Geo_OwnershipUDF''' +          ',' + '''Geo_People''' + ',' + '''Geo_RESTRICTIONS2''' + ',' + '''GEO_UDF'
       WHEN @Group =  'CRM' THEN 
              'CRM_Issues'
    else 
               @group + '_Actions''' + ',''' + @group + '_Conditions2''' + ',''' + @group + '_Conditions2_UDF''' + ',''' + @group + '_INSPECTIONS''' + ',''' + @group + '_INSPECTIONS_UDF''' + ',''' + @group + '_Main''' + ',''' + @group + '_People''' + ',''' + @group + '_Reviews''' + ',''' + @group + '_UDF''' + ',''' + @group + '_Violations_UDF''' + ',''' + @group + '_Violations2'

END
              
--print(@whereclause)


if exists (select  * from tempdb.dbo.sysobjects o where o.xtype in ('U') 
   and o.id = object_id(N'tempdb.. #AdvSearchByGroupResults')
)
DROP TABLE #AdvSearchByGroupResults;

Create Table  #AdvSearchByGroupResults
(TableName Varchar(255),FieldName Varchar(255),DataType Varchar(255))

set @SQL = 'Insert into #AdvSearchByGroupResults (TableName, FieldName, Datatype)
       select OBJECT_NAME(object_id) as tblName, name as fldName, 
       type_name(system_type_id) as fldType 
       from sys.columns 
       where OBJECT_NAME(object_id) in 
       (''' + @whereclause + ''') 
       order by  OBJECT_NAME(object_id), name'

--print(convert(varchar,getdate(),109))
exec(@sql)

/*
-- the delete is slightly slower
delete t from #AdvSearchByGroupResults t
join [Prmry_AdvSearchExclusions] p on t.TableName = p.TableName and t.FieldName = p.[columnname]
*/

select TableName, FieldName, Datatype from #AdvSearchByGroupResults
where [#AdvSearchByGroupResults].[TableName]+'|'+[#AdvSearchByGroupResults].[FieldName] not in ( select [Prmry_AdvSearchExclusions].[tableName]+'|'+[Prmry_AdvSearchExclusions].[columnname] from Prmry_AdvSearchExclusions ) 

--print(convert(varchar,getdate(),109))

--print(@sql)
--exec(@sql)

END





GO
