USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ModelHomeTemplateItem_Add]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_ModelHomeTemplateItem_Add]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ModelHomeTemplateItem_Add]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  
CREATE PROCEDURE [dbo].[tsp_T9_ModelHomeTemplateItem_Add] 
              @TemplateName varchar(255) = '',
			  @BuilderRecordID varchar(10),
              @UserID varchar(255) = '',
              @OrderId varchar(255) = '0'
AS
BEGIN

       
       Declare @date varchar(255) = getdate()

    if @OrderId = '0'
            begin
                set  @OrderId = (select count(RecordID) +1  from Prmry_ModelHomeTemplateItem)
            end


       if @TemplateName <> ''
              Begin
				Insert into Prmry_ModelHomeTemplateItem (BuilderRecordID, ORDERID, TemplateName, LAST_MODIFIED_BY, LAST_MODIFIED) 
					Values (@BuilderRecordID, @OrderID, @TemplateName, @UserID, @date)
					
					--set @SQL = 'INSERT INTO Prmry_ModelHomeTemplateItem (BuilderRecordID, ORDERID, TemplateName, LAST_MODIFIED_BY, LAST_MODIFIED)
							--VALUES (' + @BuilderRecordID + ',' + @OrderId + ',''' + @TemplateName + ''',''' + @UserID + ''', ''' + @date + ''');'

                     

              end

END



GO
