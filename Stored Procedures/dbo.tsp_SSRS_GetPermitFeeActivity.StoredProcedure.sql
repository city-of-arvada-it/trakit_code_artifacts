USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_SSRS_GetPermitFeeActivity]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_SSRS_GetPermitFeeActivity]
GO
/****** Object:  StoredProcedure [dbo].[tsp_SSRS_GetPermitFeeActivity]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  					  
					  					  					  
CREATE PROCEDURE [dbo].[tsp_SSRS_GetPermitFeeActivity] 
	@FromDate DATETIME,
	@ToDate DATETIME,
	@Account VARCHAR(2000),
	@Module VARCHAR(2000)
	
AS
BEGIN

--RB Check if table exists before creating it
   IF OBJECT_ID('tempdb..#table1') IS NOT NULL  
	drop table #table1

create table #table1
	(RECORD_NO varchar(20),
	RECORD_TYPE varchar(20),
	CODE varchar(30),
	DESCRIPTION varchar(40),
	ACCOUNT varchar(30),
	PAID_AMOUNT float,
	PAID_DATE date,
	RECEIPT_NO varchar(30),
	CHECK_NO varchar(30),
	PAY_METHOD varchar(30),
	COLLECTED_BY varchar(50),
	FEETYPE varchar(10),
	DEPOSITID varchar(30),
	SITE_ADDR varchar(70),
	SITE_APN varchar (50)) ;

create index #table1_ix1 on #table1 (RECORD_NO);

--SET BLANK ACCOUNT NUMBER TO NULL
update Permit_Fees set ACCOUNT = NULL where ACCOUNT = '' and DETAIL = 0 and PAID = 1 and PAID_DATE between @FromDate and @ToDate  ;
update Permit_SubFees set ACCOUNT = NULL where ACCOUNT = '' and PARENTID in (Select RECORDID from Permit_Fees where DETAIL = 1 and PAID_DATE between @FromDate and @ToDate) ;

--GET FEES--
DECLARE @PERMITS varchar(50)
DECLARE @PROJECTS varchar(50)
DECLARE @CASES varchar(50)
DECLARE @LICENSES varchar(50)
DECLARE @AEC varchar(50)

SET @PERMITS = 'PERMITS'
SET @PROJECTS = 'PROJECTS'
SET @CASES = 'CASES'
SET @LICENSES = 'LICENSES'
SET @AEC = 'AEC'

--PERMITS
IF @PERMITS in (SELECT * FROM [dbo].[fn_String_To_Table](@Module,',',1))

BEGIN
	insert into #table1 (RECORD_NO, RECORD_TYPE, CODE, DESCRIPTION, ACCOUNT, PAID_AMOUNT, PAID_DATE, RECEIPT_NO, CHECK_NO, SITE_ADDR, SITE_APN, PAY_METHOD, COLLECTED_BY, FEETYPE, DEPOSITID)
	select Permit_Fees.PERMIT_NO, 'PERMIT TRAK', Permit_Fees.CODE, Permit_Fees.DESCRIPTION, Permit_Fees.ACCOUNT, Permit_Fees.PAID_AMOUNT, 
		Permit_Fees.PAID_DATE, Permit_Fees.RECEIPT_NO, Permit_Fees.CHECK_NO, Permit_Main.SITE_ADDR, Permit_Main.SITE_APN,
		Permit_Fees.PAY_METHOD, Permit_Fees.COLLECTED_BY, Permit_Fees.FEETYPE, Permit_Fees.DEPOSITID
	from Permit_Fees inner join Permit_Main on Permit_Main.PERMIT_NO = Permit_Fees.PERMIT_NO
	where DETAIL = 0 and PAID = 1 and PAID_DATE between @FromDate and @ToDate 
	and (ACCOUNT in (SELECT * FROM [dbo].[fn_String_To_Table](@Account,',',1)) or ACCOUNT is NULL)
	UNION
	select Permit_SubFees.PERMIT_NO, 'PERMIT TRAK', ITEM as CODE, Permit_SubFees.DESCRIPTION, Permit_SubFees.ACCOUNT, Permit_SubFees.AMOUNT as PAID_AMOUNT, 
		Permit_Fees.PAID_DATE as PAID_DATE, Permit_Fees.RECEIPT_NO as RECEIPT_NO, Permit_Fees.CHECK_NO as CHECK_NO, Permit_Main.SITE_ADDR, Permit_Main.SITE_APN,
		Permit_Fees.PAY_METHOD, Permit_Fees.COLLECTED_BY, Permit_Fees.FEETYPE, Permit_Fees.DEPOSITID
	from Permit_SubFees inner join Permit_Fees on Permit_Fees.RECORDID = Permit_SubFees.PARENTID
	inner join Permit_Main on Permit_Main.PERMIT_NO = Permit_SubFees.PERMIT_NO
	where PARENTID in (Select RECORDID from Permit_Fees where DETAIL = 1 and PAID_DATE between @FromDate and @ToDate)
	and Permit_SubFees.PAID = 1 and (Permit_SubFees.ACCOUNT in (SELECT * FROM [dbo].[fn_String_To_Table](@Account,',',1)) or Permit_SubFees.ACCOUNT is NULL)
	order by ACCOUNT, PERMIT_NO ;
END

--PROJECTS
IF @PROJECTS in (SELECT * FROM [dbo].[fn_String_To_Table](@Module,',',1))

BEGIN
	insert into #table1 (RECORD_NO, RECORD_TYPE, CODE, DESCRIPTION, ACCOUNT, PAID_AMOUNT, PAID_DATE, RECEIPT_NO, CHECK_NO, SITE_ADDR, SITE_APN, PAY_METHOD, COLLECTED_BY, FEETYPE, DEPOSITID)
	select Project_Fees.PROJECT_NO, 'PROJECT TRAK', Project_Fees.CODE, Project_Fees.DESCRIPTION, Project_Fees.ACCOUNT, Project_Fees.PAID_AMOUNT, 
		Project_Fees.PAID_DATE, Project_Fees.RECEIPT_NO, Project_Fees.CHECK_NO, Project_Main.SITE_ADDR, Project_Main.SITE_APN,
		Project_Fees.PAY_METHOD, Project_Fees.COLLECTED_BY, Project_Fees.FEETYPE, Project_Fees.DEPOSITID
	from Project_Fees inner join Project_Main on Project_Main.PROJECT_NO = Project_Fees.PROJECT_NO
	where DETAIL = 0 and PAID = 1 and PAID_DATE between @FromDate and @ToDate 
	and (ACCOUNT in (SELECT * FROM [dbo].[fn_String_To_Table](@Account,',',1)) or ACCOUNT is NULL)
	UNION
	select Project_SubFees.PROJECT_NO, 'PROJECT TRAK', ITEM as CODE, Project_SubFees.DESCRIPTION, Project_SubFees.ACCOUNT, Project_SubFees.AMOUNT as PAID_AMOUNT, 
		Project_Fees.PAID_DATE as PAID_DATE, Project_Fees.RECEIPT_NO as RECEIPT_NO, Project_Fees.CHECK_NO as CHECK_NO, Project_Main.SITE_ADDR, Project_Main.SITE_APN,
		Project_Fees.PAY_METHOD, Project_Fees.COLLECTED_BY, Project_Fees.FEETYPE, Project_Fees.DEPOSITID
	from Project_SubFees inner join Project_Fees on Project_Fees.RECORDID = Project_SubFees.PARENTID
	inner join Project_Main on Project_Main.PROJECT_NO = Project_SubFees.PROJECT_NO
	where PARENTID in (Select RECORDID from Project_Fees where DETAIL = 1 and PAID_DATE between @FromDate and @ToDate)
	and Project_SubFees.PAID = 1 and (Project_SubFees.ACCOUNT in (SELECT * FROM [dbo].[fn_String_To_Table](@Account,',',1)) or Project_SubFees.ACCOUNT is NULL)
	order by ACCOUNT, PROJECT_NO ;
END

--CASES
IF @CASES in (SELECT * FROM [dbo].[fn_String_To_Table](@Module,',',1))

BEGIN
	insert into #table1 (RECORD_NO, RECORD_TYPE, CODE, DESCRIPTION, ACCOUNT, PAID_AMOUNT, PAID_DATE, RECEIPT_NO, CHECK_NO, SITE_ADDR, SITE_APN, PAY_METHOD, COLLECTED_BY, FEETYPE, DEPOSITID)
	select Case_Fees.CASE_NO, 'CODE TRAK', Case_Fees.CODE, Case_Fees.DESCRIPTION, Case_Fees.ACCOUNT, Case_Fees.PAID_AMOUNT, 
		Case_Fees.PAID_DATE, Case_Fees.RECEIPT_NO, Case_Fees.CHECK_NO, Case_Main.SITE_ADDR, Case_Main.SITE_APN,
		Case_Fees.PAY_METHOD, Case_Fees.COLLECTED_BY, Case_Fees.FEETYPE, Case_Fees.DEPOSITID
	from Case_Fees inner join Case_Main on Case_Main.CASE_NO = Case_Fees.CASE_NO
	where DETAIL = 0 and PAID = 1 and PAID_DATE between @FromDate and @ToDate 
	and (ACCOUNT in (SELECT * FROM [dbo].[fn_String_To_Table](@Account,',',1)) or ACCOUNT is NULL)
	UNION
	select Case_SubFees.CASE_NO, 'CODE TRAK', ITEM as CODE, Case_SubFees.DESCRIPTION, Case_SubFees.ACCOUNT, Case_SubFees.AMOUNT as PAID_AMOUNT, 
		Case_Fees.PAID_DATE as PAID_DATE, Case_Fees.RECEIPT_NO as RECEIPT_NO, Case_Fees.CHECK_NO as CHECK_NO, Case_Main.SITE_ADDR, Case_Main.SITE_APN,
		Case_Fees.PAY_METHOD, Case_Fees.COLLECTED_BY, Case_Fees.FEETYPE, Case_Fees.DEPOSITID
	from Case_SubFees inner join Case_Fees on Case_Fees.RECORDID = Case_SubFees.PARENTID
	inner join Case_Main on Case_Main.CASE_NO = Case_SubFees.CASE_NO
	where PARENTID in (Select RECORDID from Case_Fees where DETAIL = 1 and PAID_DATE between @FromDate and @ToDate)
	and Case_SubFees.PAID = 1 and (Case_SubFees.ACCOUNT in (SELECT * FROM [dbo].[fn_String_To_Table](@Account,',',1)) or Case_SubFees.ACCOUNT is NULL)
	order by ACCOUNT, CASE_NO ;
END


update #table1 set #table1.PAID_AMOUNT = #table1.PAID_AMOUNT * -1 where CODE = 'REFUND' ;
update #table1 set #table1.RECEIPT_NO = 'REFUND' where #table1.CODE = 'REFUND' ;
update #table1 set #table1.ACCOUNT = '*NONE*' where #table1.ACCOUNT is NULL ;

select * from #table1 ;

END











GO
