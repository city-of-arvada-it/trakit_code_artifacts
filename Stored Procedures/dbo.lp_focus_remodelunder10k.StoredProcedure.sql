USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[lp_focus_remodelunder10k]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[lp_focus_remodelunder10k]
GO
/****** Object:  StoredProcedure [dbo].[lp_focus_remodelunder10k]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* 
--Ychavez 3/11/2014 - FOR FOCUS Measures report
*/
CREATE procedure [dbo].[lp_focus_remodelunder10k]
		@APPLIED_FROM datetime = null,
		@APPLIED_TO datetime = null
as
begin


	--1st measure--
-------------------------------------------------------------------------------------------
--90% of remodeling plan reviews for projects under $10,000 completed within 1 business day.
--Criteria:
--Permit Type: Residential
--Sub Types: ADDITION, BASEMENT FINISH, DECK, DETACHED GARAGE, FOUNDATION REPAIR, MISCELLANEOUS, MOVING, REMODEL, STRUCUTAL REPAIR
--Measure  Dates: Permit Applied date to Issued Date if more than 1 business day out of measure
--Job Valuation: <= 10,000.00
--yc accounted for Weekdays in code
--exec lp_focus_remodelunder10k @APPLIED_FROM = '10/01/2014' , @APPLIED_TO = '10/31/2014'
-------------------------------------------------------------------------------------------

--DROP TABLE #tmp_measure
SELECT 
case when DATEDIFF(day, applied, issued) < 2 then 'less than 1 business day'
when DATEDIFF(day, applied, issued) < 4 AND DATENAME(WEEKDAY,applied) = 'FRIDAY' then 'FRIDAY less than 1 business day' 
ELSE
'out of measure'
end as measure
,0 as count_less_1_day, 0 as count_outofmeasure, 0 as total_permits, *
into #tmp_measure
 from permit_main
where permittype in ('RESIDENTIAL' )
		and permitsubtype in('REMODEL', 'BASEMENT FINISH', 'ADDITION', 'DECK', 'DETACHED GARAGE', 
			'FOUNDATION REPAIR', 'MISCELLANEOUS', 'MOVING', 'STRUCTURAL REPAIR')
AND ISSUED IS NOT NULL AND APPLIED IS NOT NULL
AND STATUS NOT IN ('WITHDRAWN', 'DENIED', 'VOID    ', 'VOID')
AND JOBVALUE  <= 10000
--AND issued > '1/01/2014'

------------------------------------------------------------------------------------------
--CALCULATE TOTAL PERMITS / PERMITS IN MEASURE - OUT OF MEASURE / LESS THAN 1 day MEASURE--
------------------------------------------------------------------------------------------

--drop table #tmp_count72
select COUNT(PERMIT_NO) as count_less_1_day
into #tmp_count72
FROM #tmp_measure 
WHERE applied between @APPLIED_FROM and @APPLIED_TO
and measure = 'less than 1 business day'


--drop table #tmp_outofmeasure
select COUNT(PERMIT_NO) as count_outofmeasure
into #tmp_outofmeasure
FROM #tmp_measure 
WHERE applied between  @APPLIED_FROM and @APPLIED_TO
and measure = 'out of measure'

update #tmp_measure
set count_less_1_day =  b.count_less_1_day
from #tmp_count72 b


update #tmp_measure
set count_outofmeasure =  b.count_outofmeasure
from #tmp_outofmeasure b

select distinct sum(count_less_1_day + count_outofmeasure) as total_permits--, 
into #tmp_totpermits
 from #tmp_outofmeasure,  #tmp_count72


update #tmp_measure
set total_permits =  b.total_permits
from #tmp_totpermits b

--need to combine all the results into one table
--USE THESE TWO NUMBERS TO CALCULATE IN REPORT--
--select (count_less_72_hours  /total_permits) as total
--select * from #tmp_measure


select
SUM(cast(count_less_1_day as decimal)) / Sum(cast(total_permits as decimal)) * 100 as percent_1_day, total_permits, count_less_1_day
from #tmp_measure
group by total_permits, count_less_1_day


--select
--* --SUM(cast(total_permits as decimal)) / Sum(cast(count_less_1_day  as decimal)) as test
--from #tmp_measure
end
GO
