USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ReleaseFeeLocks]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_ReleaseFeeLocks]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ReleaseFeeLocks]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[tsp_T9_ReleaseFeeLocks]
AS
BEGIN
	SET NOCOUNT ON

	CREATE TABLE #InactiveUsers  ([UID] VARCHAR(50))
	CREATE INDEX idx ON #InactiveUsers(UID)

	INSERT INTO #InactiveUsers ([UID])
	SELECT UserID from prmry_Users (NOLOCK) AS u
	LEFT OUTER JOIN TRAKIT9_login (NOLOCK) AS t ON u.UserID = t.[UID]
	WHERE loginExpireDate IS NULL 
		OR loginExpireDate < GETDATE()

	UPDATE F 
	SET LOCKID = NULL
	FROM Fees AS F
	INNER JOIN tvw_Fees_LockIDUser VF ON F.RECORDID = VF.RECORDID
	INNER JOIN #InactiveUsers 
		ON VF.[LockIDUser] = #InactiveUsers.UID;
	
	--REPLACED TO AVOID TABLE SCAN DUE TO SUBSTRING FUNCTION
	--UPDATE F 
	--SET LOCKID = NULL
	--FROM Fees AS F
	--INNER JOIN #InactiveUsers 
	--	ON SUBSTRING(F.LockId, 0, CHARINDEX(':', F.LOCKID)) = #InactiveUsers.UID

	UPDATE SF 
	SET LOCKID = NULL
	FROM SubFees AS SF
	INNER JOIN #InactiveUsers 
		ON SUBSTRING(SF.LockId, 0, CHARINDEX(':', SF.LOCKID)) = #InactiveUsers.UID

END
GO
