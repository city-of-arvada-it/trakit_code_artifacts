USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearch_DeleteSearch]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_AdvSearch_DeleteSearch]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearch_DeleteSearch]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  
CREATE procedure [dbo].[tsp_T9_AdvSearch_DeleteSearch]
@UserID varchar(50) = null,
@RecordID [int] = 0,
@DepartmentID [int] = -1
as
begin
	if @departmentID = -1
		Begin
			Delete from Prmry_AdvSearch_SavedItem where RecordID = @RecordID
		End
	else
		Begin
			delete from Prmry_AdvSearch_SharedItem 			
			where DepartmentID = @DepartmentID and RecordID = @RecordID 
		End

			Delete from [Prmry_AdvSearch_currentItem] where RefSearchID = @RecordID;
			Delete from [Prmry_AdvSearch_LastItem] where RefSearchID = @RecordID;

end
GO
