USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_ROWExpiredContractors]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[usp_Arvada_ROWExpiredContractors]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_ROWExpiredContractors]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_Arvada_ROWExpiredContractors]
	@startdate datetime,
	@enddate datetime	
as

/**********************************************************************************************************************
* Creator:      	 
* Date:         	 
* Title:			 
* Description:  	 
* Examples:    
	EXECUTE dbo.usp_Arvada_ROWExpiredContractors '06/01/2016','07/01/2016'
					 
* Modifications:  	
  Developer Name		Date		Description
  ------------------	----------	----------------------------------------------------------- 
*
**********************************************************************************************************************/

set nocount on
 
 --------------------------
 --- Declare Variables   --
 --------------------------

 
 --------------------------
 --- Create Temp Tables  --
 --------------------------


  --------------------------
 --- Body of Procedure   --
 --------------------------


 
select
	m.company,
	m.Phone_1,
	m.email,
	m.st_lic_no,
	m.st_lic_expire,
	i.nameType,
	i.policy_expire
from dbo.aec_main m
inner join dbo.AEC_INSURANCE i
	on i.ST_LIC_NO = m.ST_LIC_NO
where 
	m.aecsubtype = 'RIGHT OF WAY'
and i.POLICY_EXPIRE < dateadd(Day,1,@enddate)
and i.POLICY_EXPIRE >= @startdate
and exists(
			select 1
			from dbo.permit_main m
			where m.[status] = 'ISSUED'
				or m.FINALED is null
			)
order by
	m.company,
	i.policy_expire,
	i.nameType
	
--add order, need to know intent of report
GO
