USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_SSRS_LengthOfClosedCases]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_SSRS_LengthOfClosedCases]
GO
/****** Object:  StoredProcedure [dbo].[tsp_SSRS_LengthOfClosedCases]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

					  					  					  
-- =============================================
-- Author:	Dan Haynes
-- Create date: 1/10/2012
-- Description:	Gets ALL Closed Cases and Calculates Days and Groups Length Case has been Open
-- =============================================
CREATE PROCEDURE [dbo].[tsp_SSRS_LengthOfClosedCases] 
AS
BEGIN

--truncate table #table1
--select * from #table1 order by DAYS_OPEN


-- RB Check if temp table exists
IF OBJECT_ID('tempdb..#table1') IS NOT NULL  
	drop table #table1

create table #table1(
	CASE_NO varchar(200) NULL,
	CASETYPE varchar(200) NULL,
	CASESUBTYPE varchar(200) NULL,
	ASSIGNED_TO varchar(200) NULL,
	[DESCRIPTION] varchar(8000) NULL,
	CASE_LOCATION varchar(2000) NULL,
	[STATUS] varchar(200) NULL,
	[STARTED] datetime NULL,
	CLOSED datetime NULL,
	SITE_APN varchar(2000) NULL,
	SITE_ALTERNATE_ID varchar(2000) NULL,
	SITE_ADDR varchar(2000) NULL,
	DAYS_OPEN int,
	OPEN_GROUP varchar(50)) ;

insert into #table1 (CASE_NO, CASETYPE, CASESUBTYPE, ASSIGNED_TO, [DESCRIPTION], CASE_LOCATION, [STATUS], [STARTED], CLOSED, SITE_APN, SITE_ALTERNATE_ID, SITE_ADDR)
select CASE_NO, CASETYPE, CASESUBTYPE, ASSIGNED_TO, [DESCRIPTION], CASE_LOCATION, [STATUS], [STARTED], CLOSED, SITE_APN, SITE_ALTERNATE_ID, SITE_ADDR
from Case_Main where (CLOSED is not NULL or CLOSED <> '') and STATUS = 'CLOSED' and STARTED is not NULL;

update #table1 set DAYS_OPEN = CONVERT(bigint,CONVERT(datetime,CLOSED))- CONVERT(bigint,CONVERT(datetime,[STARTED])) ;
update #table1 set OPEN_GROUP = CASE 
								WHEN (DAYS_OPEN < 30) THEN 'Group 1 [Less Than 30 Days]'
								WHEN (DAYS_OPEN >= 30  and  DAYS_OPEN < 90 and DAYS_OPEN is not NULL) THEN 'Group 2 [30 to 90 Days]'
								WHEN (DAYS_OPEN >= 90  and  DAYS_OPEN < 180 and DAYS_OPEN is not NULL) THEN 'Group 3 [90 to 180 Days]'
								WHEN (DAYS_OPEN >= 180  and  DAYS_OPEN < 365 and DAYS_OPEN is not NULL) THEN 'Group 4 [180 Days to 1 Year]'
								WHEN (DAYS_OPEN >= 365  and  DAYS_OPEN < 1095 and DAYS_OPEN is not NULL) THEN 'Group 5 [1 to 3 Years]'
								WHEN (DAYS_OPEN >= 1095 and DAYS_OPEN is not NULL) THEN 'Group 6 [More Than 3 Years]' END ;
								
select * from #table1 order by OPEN_GROUP, CASETYPE;


END






GO
