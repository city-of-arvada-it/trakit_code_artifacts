USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_LicenseRenew_Add_Fee]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_LicenseRenew_Add_Fee]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_LicenseRenew_Add_Fee]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  
CREATE PROCEDURE [dbo].[tsp_T9_LicenseRenew_Add_Fee]
	@FEECODE_TO_ADD AS varchar(30),
	@ASSESSED_BY AS varchar(6),
	@RECORDPREFIX AS varchar(6),
	@REVIEW_TABLES AS INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

DECLARE @SCHEDULE_EFFECTIVE_DATE as DATETIME
DECLARE @EFFECTIVE_FEE_SCHEDULE as varchar(200)
DECLARE @FEE_PARENT as nvarchar(30)
DECLARE @FEE_AMOUNT as float

SET @SCHEDULE_EFFECTIVE_DATE = (SELECT MAX(EFFECTIVE_ON) FROM Prmry_FeeScheduleList WHERE Prmry_FeeScheduleList.EFFECTIVE_ON < GETDATE() AND TEST <> 1)
SET @EFFECTIVE_FEE_SCHEDULE = (SELECT TOP 1 Prmry_FeeScheduleList.SCHEDULE_NAME FROM Prmry_FeeScheduleList WHERE TEST <> 1 AND EFFECTIVE_ON = @SCHEDULE_EFFECTIVE_DATE or TEST <> 1 AND EFFECTIVE_ON is null )
set @FEE_PARENT=(select top 1 nullif(Prmry_FeeSchedule.PARENT_CODE,'')  
FROM Prmry_FeeSchedule WHERE Prmry_FeeSchedule.CODE=@FEECODE_TO_ADD)

IF @SCHEDULE_EFFECTIVE_DATE IS NULL
	BEGIN
		SET @SCHEDULE_EFFECTIVE_DATE = GETDATE()
		SET @EFFECTIVE_FEE_SCHEDULE = 'Default'
	END
	
SELECT @SCHEDULE_EFFECTIVE_DATE, @EFFECTIVE_FEE_SCHEDULE
 
 SELECT TOP(1) @FEE_AMOUNT = CONVERT(float,formula) FROM Prmry_FeeSchedule WHERE SCHEDULE_NAME = @EFFECTIVE_FEE_SCHEDULE AND GroupName = 'LICENSE2' AND CODE = @FEECODE_TO_ADD AND ISNUMERIC(FORMULA) = 1
 

IF @FEE_AMOUNT IS NOT NULL
	BEGIN
		IF @REVIEW_TABLES = 1
			BEGIN
				INSERT INTO LicenseRenew_Review_Fees([recordid],ASSESSED_BY, ASSESSED_DATE, AMOUNT, CODE, 
				DESCRIPTION, COMMENTS,DETAIL, FEETYPE,FORMULA, PAID, ACCOUNT, LICENSE_NO) 
				SELECT 'GLR:' + right(('00' + str(DATEPART(yy, GETDATE()))),2) + 
				right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2) + 
				right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2) + 
				right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2) + 
				right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2) + 
				right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2) + 
				right(('000000'+ CONVERT(varchar,ROW_NUMBER() OVER (ORDER BY (SELECT NULL)))),6)as recid, 
				@ASSESSED_BY as ASSESSED_BY, 
				GETDATE() as ASSESSED_DATE, CONVERT(float,formula),		
				case when @FEECODE_TO_ADD<>@FEE_PARENT then @FEE_PARENT else @FEECODE_TO_ADD end as codes,		
				DESCRIPTION, CAST (DATEPART(yy,GETDATE()) AS VARCHAR(20))  + ' ' + DESCRIPTION as comments,
				case when @FEECODE_TO_ADD<>@FEE_PARENT  then 1 else 0 end as detail, 'FEE' as feetype,
				formula,0 as paid, ACCOUNT, tbl.LICENSE_NO FROM (select LICENSE_NO FROM LicenseRenew_License_List) as tbl,
				Prmry_FeeSchedule WHERE SCHEDULE_NAME = @EFFECTIVE_FEE_SCHEDULE
				AND GroupName = 'LICENSE2' AND CODE = @FEECODE_TO_ADD AND ISNUMERIC(FORMULA) = 1
--add subfee if needed
			IF @FEECODE_TO_ADD<>@FEE_PARENT
			BEGIN
				INSERT INTO LicenseRenew_Review_SubFees
				([RECORDID],[LICENSE_NO],[CODE],[ITEM]  ,[DESCRIPTION],[FORMULA] ,[AMOUNT],[ACCOUNT],[PAID],
				[COMMENTS],[PARENTID],[ASSESSED_DATE]  ,[ASSESSED_BY]  ,[SUBFEETYPE])
				SELECT 
				'GLR:' + right(('00' + str(DATEPART(yy, GETDATE()))),2) + 
				right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2) + 
				right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2) + 
				right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2) + 
				right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2) + 
				right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2) + 
				right(('000000'+ CONVERT(varchar,ROW_NUMBER() OVER (ORDER BY (SELECT NULL)))),6)as recid, tbl.LICENSE_NO,
				PARENT_CODE,code,DESCRIPTION, FORMULA,CONVERT(float,formula) as amount,account,'0' as paid,
				CAST (DATEPART(yy,GETDATE()) AS VARCHAR(20))  + ' ' + DESCRIPTION as comments,parentid,
				GETDATE() as ASSESSED_DATE,@ASSESSED_BY as ASSESSED_BY, 'FEE'
				FROM (select l.LICENSE_NO,f.recordid as parentid FROM LicenseRenew_License_List l 
				inner join LicenseRenew_Review_Fees f 
				on l.LICENSE_NO= f.LICENSE_NO where code=@FEE_PARENT and f.recordid not in
				 (select PARENTID from LicenseRenew_Review_SubFees)) as tbl,
				Prmry_FeeSchedule WHERE SCHEDULE_NAME = @EFFECTIVE_FEE_SCHEDULE
				AND GroupName = 'LICENSE2' AND code = @FEECODE_TO_ADD AND ISNUMERIC(FORMULA) = 1
			end
		END
	
	else
	BEGIN
		INSERT INTO License2_Fees([recordid],ASSESSED_BY, ASSESSED_DATE, AMOUNT, CODE, 
		DESCRIPTION, COMMENTS,DETAIL, FEETYPE,FORMULA, PAID, ACCOUNT, LICENSE_NO) 
		SELECT 'GLR:' + right(('00' + str(DATEPART(yy, GETDATE()))),2) + 
		right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2) + 
		right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2) + 
		right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2) + 
		right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2) + 
		right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2) + 
		right(('000000'+ CONVERT(varchar,ROW_NUMBER() OVER (ORDER BY (SELECT NULL)))),6)as recid, 
		@ASSESSED_BY as ASSESSED_BY, 
		GETDATE() as ASSESSED_DATE, CONVERT(float,formula),		PARENT_CODE,		
		 DESCRIPTION, CAST (DATEPART(yy,GETDATE()) AS VARCHAR(20))  + ' ' + DESCRIPTION as comments,
		case when nullif(PARENT_CODE,'') is null then 0 else 1 end as detail, 'FEE' as feetype,
		 formula,0 as paid, ACCOUNT, tbl.LICENSE_NO FROM (select LICENSE_NO FROM LicenseRenew_License_List) as tbl,
		 Prmry_FeeSchedule WHERE SCHEDULE_NAME = @EFFECTIVE_FEE_SCHEDULE
		  AND GroupName = 'LICENSE2' AND CODE = @FEECODE_TO_ADD AND ISNUMERIC(FORMULA) = 1
--add subfee if needed
		 IF @FEECODE_TO_ADD<>@FEE_PARENT
			BEGIN
				INSERT INTO License2_SubFees
		([RECORDID],[LICENSE_NO],[CODE],[ITEM]  ,[DESCRIPTION],[FORMULA] ,[AMOUNT],[ACCOUNT],[PAID],
		[COMMENTS],[PARENTID],[ASSESSED_DATE]  ,[ASSESSED_BY]  ,[SUBFEETYPE])
		SELECT 
		'GLR:' + right(('00' + str(DATEPART(yy, GETDATE()))),2) + 
		right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2) + 
		right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2) + 
		right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2) + 
		right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2) + 
		right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2) + 
		right(('000000'+ CONVERT(varchar,ROW_NUMBER() OVER (ORDER BY (SELECT NULL)))),6)as recid, tbl.LICENSE_NO,
		PARENT_CODE,code,DESCRIPTION, FORMULA,CONVERT(float,formula) as amount,account,'0' as paid,
		CAST (DATEPART(yy,GETDATE()) AS VARCHAR(20))  + ' ' + DESCRIPTION as comments,parentid,
		GETDATE() as ASSESSED_DATE,@ASSESSED_BY as ASSESSED_BY, 'FEE'
		 FROM (select l.LICENSE_NO,f.recordid as parentid FROM LicenseRenew_License_List l inner 
		 join License2_Fees f 
		 on l.LICENSE_NO= f.LICENSE_NO where code=@FEE_PARENT and f.recordid not in
				 (select PARENTID from License2_SubFees)) as tbl,
		 Prmry_FeeSchedule WHERE SCHEDULE_NAME = @EFFECTIVE_FEE_SCHEDULE
		  AND GroupName = 'LICENSE2' AND CODE = @FEECODE_TO_ADD AND ISNUMERIC(FORMULA) = 1
		    end
		END
	end
	ELSE
	-- insert FAILURE!! to the license log
	INSERT INTO LicenseRenew_Process_Exceptions SELECT getdate() as PROCESSING_DATE, 'FEE ERROR', @FEECODE_TO_ADD + ' formula must be single value' AS EXCEPTION_DESC FROM LicenseRenew_Review_Main WHERE license_type NOT IN (SELECT typename FROM prmry_types WHERE groupname = 'license2')
END
---------------------


GO
