USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ExportActivityStatuses]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_ExportActivityStatuses]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ExportActivityStatuses]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  
-- =====================================================================
-- Revision History:
--	mdm - 12/02/13 - added revision history	
--  mdp - 11/26/13 - 
-- =====================================================================  				  				  
					  					  					  
/*
tsp_ExportActivityStatuses @DBName='DEV', @FilePath='C:\CRW\iTrakitExports\', @FileName='ActivityStatusExport'

select * from prmry_standardnotes
select * from prmry_standardtexts

*/

CREATE Procedure [dbo].[tsp_ExportActivityStatuses](
@DBName Varchar(50),
@FilePath Varchar(1000),
@FileName Varchar(150),
@ToggleXP_CMDShell INT = 1
)

As

SET NOCOUNT ON

Declare @SQL Varchar(8000)

If @ToggleXP_CMDShell Is Null Set @ToggleXP_CMDShell = 1

IF EXISTS (SELECT * FROM tempdb.sys.objects WHERE object_id = OBJECT_ID(N'tempdb..ActivityStatuses') AND type in (N'U'))
	Drop Table tempdb..ActivityStatuses

Create Table #ActivityTypesInfo(GroupName Varchar(200),TypeName Varchar(200),Value1 Varchar(200),[Description] Varchar(200),Account Varchar(200),ORDERID Varchar(200))
Create Table #ActivityTypesInfoNoMatch(GroupName Varchar(200),TypeName Varchar(200))
Create Table #ActivityStatus(GroupName Varchar(200), [Status] Varchar(200),Parcel_Lock Varchar(1),SetClosedDate Varchar(1), CRM_Issue_Link Varchar(200), ORDERID Varchar(200))
Create Table #ActivityTypes(GroupName Varchar(200),TypeName Varchar(200),ORDERID Varchar(200))


Create Table tempdb..ActivityStatuses(GroupName Varchar(200),TypeName Varchar(200),Value1 Varchar(200),Parcel_Lock Varchar(200),SetClosedDate Varchar(200), CRM_Issue_Link Varchar(200),ORDERID Varchar(200))

Insert Into tempdb..ActivityStatuses(GroupName,TypeName,Value1,Parcel_Lock,SetClosedDate,CRM_Issue_Link,ORDERID)
Values('GroupName','TypeName','Value1','Parcel_Lock','SetClosedDate','CRM_Issue_Link','ORDERID')


Set @SQL = '
Insert Into #ActivityTypes([GroupName],[TypeName],[ORDERID])
Select 
GroupName=CASE WHEN GroupName = '''' THEN Null ELSE GroupName END,
TypeName=CASE WHEN TypeName = '''' THEN Null ELSE REPLACE(REPLACE(REPLACE(TypeName,CHAR(9),''''),CHAR(10),''''),CHAR(13),'''') END,
ORDERID=CASE WHEN ORDERID Is Null THEN ''999'' ELSE Convert(Varchar,ORDERID) END
From ' + @DBName + '..Prmry_Types
ORDER BY GroupName,ORDERID'
--Print(@SQL)
Exec(@SQL)

Set @SQL = '
Insert Into #ActivityTypesInfo([GroupName],[TypeName],[Value1],[ORDERID])
Select 
GroupName=CASE WHEN GroupName = '''' THEN Null ELSE GroupName END,
TypeName=CASE WHEN TypeName = '''' THEN Null ELSE REPLACE(REPLACE(REPLACE(TypeName,CHAR(9),''''),CHAR(10),''''),CHAR(13),'''') END,
Value1=CASE WHEN Value1 = '''' THEN Null ELSE REPLACE(REPLACE(REPLACE(Value1,CHAR(9),''''),CHAR(10),''''),CHAR(13),'''') END,
ORDERID=CASE WHEN ORDERID Is Null THEN ''999'' ELSE Convert(Varchar,ORDERID) END
From ' + @DBName + '..Prmry_TypesInfo
WHERE Category = ''Status''
ORDER BY GroupName,ORDERID'
--Print(@SQL)
Exec(@SQL)

Set @SQL = '
Insert Into #ActivityStatus([GroupName],[Status],[Parcel_Lock],[SetClosedDate],[CRM_ISSUE_LINK],[ORDERID])
Select 
GroupName=CASE WHEN GroupName = '''' THEN Null ELSE GroupName END,
Status=CASE WHEN Status = '''' THEN Null ELSE REPLACE(REPLACE(REPLACE(Status,CHAR(9),''''),CHAR(10),''''),CHAR(13),'''') END,
Parcel_Lock=CASE WHEN Parcel_Lock = '''' Or Parcel_Lock Is Null THEN ''0'' ELSE Parcel_Lock END,
SetClosedDate=CASE WHEN SetClosedDate = '''' Or SetClosedDate Is Null THEN ''0'' ELSE SetClosedDate END,
CRM_ISSUE_LINK=CASE WHEN CRM_ISSUE_LINK = '''' THEN Null ELSE REPLACE(REPLACE(REPLACE(CRM_ISSUE_LINK,CHAR(9),''''),CHAR(10),''''),CHAR(13),'''') END,
ORDERID=CASE WHEN ORDERID Is Null THEN ''999'' ELSE Convert(Varchar,ORDERID) END
From ' + @DBName + '..Prmry_Status
ORDER BY GroupName,ORDERID'
--Print(@SQL)
Exec(@SQL)

Print('Insert All where no match into #ActivityTypesInfoNoMatch')
Insert Into #ActivityTypesInfoNoMatch([GroupName],[TypeName])
Select 
t.GroupName,t.TypeName
From #ActivityTypes t
LEFT JOIN #ActivityTypesInfo ti
 ON t.GroupName = ti.GroupName AND t.TypeName = ti.TypeName
WHERE ti.TypeName is null

Print('Insert All no match Types with Statuses into tempdb..ActivityStatuses')
Insert Into tempdb..ActivityStatuses(GroupName,TypeName,Value1)
Select 
tinm.GroupName,
tinm.TypeName,
st.[Status]
From #ActivityTypesInfoNoMatch tinm
JOIN #ActivityStatus st
 ON tinm.GroupName = st.GroupName 
 
Print('Update values from #ActivityStatus')
Update tati Set
Parcel_Lock = tst.Parcel_Lock
,SetClosedDate = tst.SetClosedDate
,OrderID = tst.OrderID
,CRM_ISSUE_LINK = tst.CRM_ISSUE_LINK
From tempdb..ActivityStatuses tati
JOIN #ActivityStatus tst
 ON tati.GroupName = tst.GroupName AND tati.Value1 = tst.Status
 
Insert Into tempdb..ActivityStatuses([GroupName],[TypeName],[Value1],[OrderID])
Select 
ti.GroupName,ti.TypeName,ti.Value1,ti.OrderID
From #ActivityTypes t
JOIN #ActivityTypesInfo ti
 ON t.GroupName = ti.GroupName AND t.TypeName = ti.TypeName
Order By ti.GroupName,ti.TypeName,Convert(INT,ti.OrderID)

 

--Select * From #ActivityTypes


Select * From tempdb..ActivityStatuses


/*
tsp_ExportActivityStatuses @DBName='DEV', @FilePath='C:\CRW\TRAKiT9_services\Exports\', @FileName='ActivityStatusExport'

*/


Declare @vDate Varchar(100)
Declare @OutputFileName Varchar(1000)

Set @vDate = Replace(Convert(Varchar,GetDate(),102),'.','_')
Print(@vDate)

--Set @OutputFileName = '"' + @FilePath + @FileName + '_' + @vDate + '.csv"'
Set @OutputFileName = '"' + @FilePath + @FileName + '.csv"'


If @ToggleXP_CMDShell = 1
 Begin
	--Enable xp_cmdshell
	Set @SQL = '
	EXEC sp_configure ''show advanced options'', 1
	RECONFIGURE
	EXEC sp_configure ''xp_cmdshell'', 1
	RECONFIGURE
	'
	Print(@SQL)
	Exec(@SQL)
 End

select @sql = 'bcp "select * from tempdb..ActivityStatuses" queryout ' + @OutputFileName + ' -c -t"|" -T -S'
+ @@servername
--Print(@SQL)
exec master..xp_cmdshell @sql


--IF EXISTS (SELECT * FROM tempdb.sys.objects WHERE object_id = OBJECT_ID(N'tempdb..ActivityStatuses') AND type in (N'U'))
	--Drop Table tempdb..ActivityStatuses


Set @SQL = '
DELETE ' + @DBName + '.dbo.iTrakitFeeds Where [FileName] = ''' + @FileName + '.csv''

INSERT INTO ' + @DBName + '.dbo.iTrakitFeeds([filename],[data])
SELECT ''' + @FileName + '.csv'', * FROM OPENROWSET(
  BULK N''' + REPLACE(@OutputFileName,'"','') + ''', SINGLE_BLOB
) bt'
Print(@SQL)
Exec(@SQL)

If @ToggleXP_CMDShell = 1
 Begin
	-- Disable xp_cmdshell
	Set @SQL = '
	EXEC sp_configure ''xp_cmdshell'', 0
	RECONFIGURE
	EXEC sp_configure ''show advanced options'', 0
	RECONFIGURE
	'
	Print(@SQL)
	Exec(@SQL)
 End








GO
