USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_WaivedUseTax]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[usp_Arvada_WaivedUseTax]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_WaivedUseTax]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_Arvada_WaivedUseTax]
	@FromDate DATETIME,
	@ToDate DATETIME
as
/**********************************************************************************************************************
* Creator:      	 
* Date:         	 
* Title:			 
* Description:  	 
* Examples:    
			EXECUTE dbo.usp_Arvada_WaivedUseTax 
					 
* Modifications:  	
  Developer Name		Date		Description
  ------------------	----------	----------------------------------------------------------- 
*
**********************************************************************************************************************/

set nocount on
 
 --------------------------
 --- Declare Variables   --
 --------------------------

 
 --------------------------
 --- Create Temp Tables  --
 --------------------------
 IF object_id('tempdb.dbo.#final') IS NOT NULL DROP TABLE #final
CREATE TABLE #final
(
	PERMIT_NO varchar(25),
	SITE_ADDR VARCHAR(40),
	JOBVALUE float,
	AMOUNT float,
	COMMENTS varchar(200),
	ASSESSED_DATE datetime,
	CONTRACTOR_NAME VARCHAR(50) ,
	CONTRACTOR_ADDRESS VARCHAR(4000),
	CONTACTOR_PHONE VARCHAR(50),
	PersonTypeMatch VARCHAR(50)
)

	 
 --------------------------
 --- Body of Procedure   --
 --------------------------
 

--everything but items for permit_people
INSERT INTO #final
( 
	PERMIT_NO ,
    SITE_ADDR ,
    JOBVALUE ,
    AMOUNT ,
    COMMENTS ,
    ASSESSED_DATE ,
    CONTRACTOR_NAME
)

SELECT 
	s.PERMIT_NO,
	m.SITE_ADDR,
	m.JOBVALUE,
	s.AMOUNT,
	NULLIF(s.COMMENTS,'') AS comments,
	s.ASSESSED_DATE,
	ISNULL(m.CONTRACTOR_NAME,m.APPLICANT_NAME)
FROM dbo.Permit_SubFees s
INNER JOIN dbo.Permit_Main m
	ON s.PERMIT_NO = m.PERMIT_NO
WHERE
	s.CODE = 'USETAX' -- '%USETAX%'
--AND s.ITEM = 'USETAX4' --Tax Exempt
and ISNULL(s.AMOUNT,0) = 0
and s.ASSESSED_DATE >= @FromDate
AND s.ASSESSED_DATE < dateadd(DAY,1,@ToDate)
and (m.[status] not in ('applied', 'approved', 'void', 'withdrawn'))

UNION

SELECT 
	s.PERMIT_NO,
	m.SITE_ADDR,
	m.JOBVALUE,
	s.AMOUNT,
	NULLIF(s.COMMENTS,'') AS comments,
	s.ASSESSED_DATE,
	ISNULL(m.CONTRACTOR_NAME,m.APPLICANT_NAME)
FROM dbo.Permit_Fees s
INNER JOIN dbo.Permit_Main m
	ON s.PERMIT_NO = m.PERMIT_NO
WHERE
	s.CODE = 'USETAX' -- '%USETAX%'
and ISNULL(s.AMOUNT,0) = 0
and s.ASSESSED_DATE >= @FromDate
AND s.ASSESSED_DATE < dateadd(DAY,1,@ToDate)
and (m.[status] not in ('applied', 'approved', 'void', 'withdrawn'))
AND NOT EXISTS(SELECT 1 --just remove what would be removed from the UNION distincting things but this should be a bit faster
				FROM dbo.Permit_SubFees s2
				WHERE 
					s2.code = 'USETAX'
					AND ISNULL(s2.AMOUNT,0) = 0
					AND s2.PERMIT_NO = s.PERMIT_NO
				 )

--sometimes comments are on the permit_fees record, rather than the sub fees, so see if we get anything:
UPDATE f 
SET 
	COMMENTS = s.COMMENTS
FROM #final f
INNER JOIN dbo.Permit_Fees s
	ON s.PERMIT_NO = f.PERMIT_NO
WHERE	
	f.comments IS NULL
AND s.CODE = 'USETAX' 
AND s.COMMENTS IS NOT NULL 

--permit_people has 1 to many with permit_no, so do our best to get the best info here:    
; 
WITH cte AS 
(
	SELECT
		p.permit_no,
		p.ADDRESS1,
		p.CITY,
		p.PHONE,
		p.nameType,
		ROW_NUMBER() OVER (PARTITION BY p.PERMIT_NO 
								ORDER BY 
									CASE WHEN nameType = 'owner' AND p.phone IS NOT NULL AND p.address1 IS NOT NULL THEN 1  
										  WHEN nameType = 'owner' AND p.phone IS NOT NULL  THEN 2 
										  WHEN nameType = 'owner' AND p.address1 IS NOT NULL  THEN 3
										 WHEN nameType != 'PREV OWNER' AND p.phone IS NOT NULL AND p.address1 IS NOT NULL THEN 4
										 WHEN nameType != 'PREV OWNER' AND p.phone IS NOT NULL THEN 5
										 WHEN nameType = 'owner' THEN 6
										 WHEN nameType = 'owner 1' THEN 7
										 WHEN nameType = 'owner 2' THEN 8
										 WHEN nameType != 'PREV OWNER' THEN 9
										 ELSE 10
										 END
							) AS rid
	FROM dbo.Permit_People p
	INNER JOIN #final f
		ON f.PERMIT_NO = p.PERMIT_NO
)
UPDATE f 
SET
	CONTRACTOR_ADDRESS  = c.ADDRESS1,
    CONTACTOR_PHONE = c.PHONE,
	PersonTypeMatch = c.nameType
FROM #final f
INNER JOIN cte c
	ON c.PERMIT_NO = f.	PERMIT_NO
WHERE	c.rid = 1

SELECT 
	PERMIT_NO ,
	SITE_ADDR ,
	JOBVALUE ,
	AMOUNT ,
	COMMENTS ,
	ASSESSED_DATE ,
	CONTRACTOR_NAME  ,
	CONTRACTOR_ADDRESS ,
	CONTACTOR_PHONE ,
	PersonTypeMatch
FROM #final
ORDER BY
		CONTRACTOR_NAME,
		PERMIT_NO,
		ASSESSED_DATE
GO
