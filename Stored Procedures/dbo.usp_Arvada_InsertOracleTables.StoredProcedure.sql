USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_InsertOracleTables]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[usp_Arvada_InsertOracleTables]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_InsertOracleTables]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_Arvada_InsertOracleTables]
	@l_batch_date datetime,
	@FindUnassigned int = 0
as

begin

set nocount on

truncate table dbo.ArvadaOracledata

/*
--as called from Oracle
--exec usp_Arvada_InsertOracleTables '02/14/2020', 0

--as called from report
exec usp_Arvada_InsertOracleTables '03/19/2020', 0

select * 
from OracleFinalOutput

*/


--This was taken from Pam's original logic in Oracle, moved to t-sql
--It gathers necessary financial data for Trakit and puts it into the planning, engineering, building buckets by pay method to make journal entries in Oracle
--This part does all the pre-work, another procedure returns the data (limitations on procedure calls from Oracle to SQL Server) . The calls look like this:
--exec dbo.usp_Arvada_InsertOracleTables '05/02/2018'
--exec dbo.usp_Arvada_OracleTrakitBatch  

		--adjust a fee if it was applied some time last year, not paid yet
		--the fees all adjusted in 2019 and cases where the fee was on the books prior to the fee change
		--not paid yet, then it got paid it needs to go to the right account at the time it got paid.
		update f
		set account = fs.ACCOUNT 
		from Permit_SubFees f
		inner join Permit_Fees fe
			on f.ParentID = fe.RECORDID
		inner join permit_main m
			on m.PERMIT_NO = f.PERMIT_NO
		inner join Prmry_FeeSchedule fs
			on fs.CODE = f.item
			and fs.SCHEDULE_NAME = '2019 FEE SCHEDULE'
			and fs.GroupName = 'PERMITS'
		where  APPLIED <= dateadd(month, -6, '01/01/2019')
		 and PAID_DATE >= '01/01/2019'
		and isnull(f.account,'') != isnull(fs.ACCOUNT,'')
		

		update f
		set account = fs.ACCOUNT
		from Permit_Fees f
		inner join permit_main m
			on m.PERMIT_NO = f.PERMIT_NO
		inner join Prmry_FeeSchedule fs
			on fs.CODE = f.CODE
			and fs.SCHEDULE_NAME = '2019 FEE SCHEDULE'
			and fs.GroupName = 'PERMITS'
		where APPLIED <= dateadd(month, -6, '01/01/2019')
		and PAID_DATE >= '01/01/2019'
		and isnull(f.account,'') != isnull(fs.ACCOUNT,'')

 
 

      /*********** adding Permit transactions below ***********/
      INSERT INTO ArvadaOracledata (COLLECTED_BY, CODE, PERMIT_TYPE, TRANSACTION_NO, TRANSACTION_TYPE, 
			TRANSACTION_DATE, CREDIT_AMOUNT, DEBIT_AMOUNT, DESCRIPTION, GL_NUMBER, TAX_CODE, RECEIPT_NO, PAY_METHOD)
      SELECT p.collected_by, isnull(s.code,p.code) code, PermitType as PERMIT_TYPE,
      isnull(s.permit_no, p.permit_no) permit_no, 'Permit' transaction_type,
      P.PAID_DATE,
      isnull(S.AMOUNT, p.PAID_AMOUNT) amount,
      0,
      NULL,
      isnull(s.ACCOUNT, p.ACCOUNT) GL_NUMBER,
	  case when isnull(s.code, p.code) in ('USE TAX',  'USETAX') then 'USE TAX'
		   else 'PERMITS'
		   end as TAX_CODE,
      RECEIPT_NO, pay_method
      FROM PERMIT_FEES P
      JOIN PERMIT_MAIN M2 ON P.PERMIT_NO = M2.PERMIT_NO
      left outer JOIN PERMIT_SUBFEES S ON P.RECORDID = S.PARENTID
--      LEFT OUTER JOIN PERMIT_UDF U ON S.PERMIT_NO  = U.PERMIT_NO
      WHERE datediff(day, p.PAID_DATE, @l_batch_date) = 0
      AND isnull(S.PAID, p.paid)      = 1
      AND p.pay_method != 'VOID'
      and isnull(p.code,'no code') != 'REFUND'
      AND isnull(P.INVOICED,0) <> 1


 
      /*********** adding Project transactions below ***********/
      INSERT INTO ArvadaOracledata (COLLECTED_BY, CODE, PERMIT_TYPE, TRANSACTION_NO, TRANSACTION_TYPE, TRANSACTION_DATE, CREDIT_AMOUNT, DEBIT_AMOUNT, DESCRIPTION, GL_NUMBER, TAX_CODE, RECEIPT_NO, PAY_METHOD)
      SELECT p.collected_by, isnull(s.code, p.code) code, PROJECTTYPE as  permit_type,
      p.project_no,'Project',
      P.PAID_DATE,
      isnull(S.AMOUNT, p.PAID_AMOUNT) amount,
      0,
      NULL,
      isnull(s.ACCOUNT, p.account) GL_NUMBER,
      	  case when isnull(s.code, p.code) in ('USE TAX',  'USETAX') then 'USE TAX'
		   else 'PERMITS'
		   end as TAX_CODE,
      RECEIPT_NO, pay_method
      FROM PROJECT_FEES P
      JOIN PROJECT_MAIN M2 ON P.PROJECT_NO = M2.PROJECT_NO
      LEFT OUTER JOIN PROJECT_SUBFEES S ON P.RECORDID = S.PARENTID
--      JOIN PROJECT_UDF U2 ON S.PROJECT_NO= U2.PROJECT_NO
      WHERE  datediff(day, p.PAID_DATE, @l_batch_date) = 0
      AND isnull(S.PAID, p.paid) = 1
      AND p.pay_method != 'VOID'
      and isnull(p.code,'no code') != 'REFUND'
      AND isnull(p.INVOICED,0) <> 1
      

      /*************** adding AEC Transactions ************************/

      INSERT INTO ArvadaOracledata (COLLECTED_BY, CODE, PERMIT_TYPE, TRANSACTION_NO, TRANSACTION_TYPE, TRANSACTION_DATE, CREDIT_AMOUNT, DEBIT_AMOUNT, DESCRIPTION, GL_NUMBER, TAX_CODE, RECEIPT_NO, PAY_METHOD)
      SELECT collected_by, isnull(s.code, f.code) code, AECTYPE as  permit_type,
      f.st_lic_no,'AEC',
      f.PAID_DATE transaction_date, 
      isnull(s.AMOUNT, f.PAID_AMOUNT) credit_amount,
      0 debit_amount,
      NULL description,
      isnull(s.ACCOUNT, f.ACCOUNT) GL_NUMBER,
      	  case when isnull(s.code, s.code) in ('USE TAX',  'USETAX') then 'USE TAX'
		   else 'LICENSING' --5/30/19 changed from permits to licensing
		   end as TAX_CODE,
      RECEIPT_NO, f.pay_method
      FROM AEC_FEES f
      JOIN AEC_MAIN M2 ON f.ST_LIC_NO = M2.ST_LIC_NO
      LEFT OUTER JOIN AEC_SUBFEES S ON f.RECORDID = S.PARENTID
--      JOIN AEC_UDF U2 ON S.ST_LIC_NO= U2.ST_LIC_NO
      WHERE   datediff(day, f.PAID_DATE, @l_batch_date) = 0
      AND isnull(s.PAID, f.PAID)      = 1
      AND f.pay_method != 'VOID'
      and isnull(f.code,'no code') != 'REFUND'
      AND isnull(f.INVOICED,0) <> 1

	  select * 
	  from Prmry_Users
	  where username like 'def%'

--assign batch type or register drawers:
--everything except Brita goes to etrakit during COVID times:
--DMCP added 04/02/2020
if @l_batch_date >= '03/19/2020'
begin 
	update a
	set batch_type =
			case 
			when isnull(COLLECTED_BY,'') not in ('BVH','DMCP','DE','DECO') then 'ETRAKIT'
			when CODE in ('ESCROW1','DEV','DEV1','DEV2','ESCR01','DEVAPP','ROWLAND','ROWLAN2','DEVPUB','PLAN10','PLAN11','ZONEV','APP43','PLANAPP42','PLANAPP42','ZONEB') 
				then 'Planning'
           when CODE = 'ESCROW' 
				then 'Engineering'
           when CODE = 'ROW' 
				then 'Engineering'
		   when CODE IN 
		   ('COM29','ROW61','ROW62','ROW63','TPLAN2','TPLN','TPLN1','TPLN3','TPLN4','TRAFF2','TRAFF3','TRAFF4','TRAFF5','TRAFF6','TRAFF7','TRFDIS','TRFDIS1','TRFDIS2',	'TRFDIS3')
				then 'Engineering'
           when PERMIT_TYPE in ('FLOODPL DEV', 'RIGHT OF WAY', 'YARDXX','ENG RCPT', 'ROWYARD','DRY Utility'
                               -- PROJECT TRANSACTION TYPES(PROJECTTYPE) BELOW
                               ,'future use engineering'
                                -- AEC TRANSACTION TYPES('AECTYPE') BELOW
                               ,'ROW CONTRACTOR', 'SITE DEV', 'SMALL CELL') 
				then 'Engineering'
           when CODE in ('LIMITED', 'BLDINSP', 'BCAB' ) and PERMIT_TYPE = 'CONTRACTOR' 
				then 'Building'
		   when code in ('COMBO10','COMBO11','COMBO12','COMBO13','COMBO9','DEMOL3','DEMOL4','SOLR3') 
			    then 'Building'
           when PERMIT_TYPE in ('ADU', 'COMBINATION', 'COMMERCIAL', 'COMMMISC'
                               , 'DEMOLITION', 'ELEVATOR', 'ELEVATORS'
                               , 'MISC', 'MOVING', 'PROPTY MAINT', 'Receipt'
                               , 'RESIDENTIAL', 'RESMISC', 'Retain Wall', 'ROOF'
                               , 'SIGN', 'SOLAR','BLDG RCPT','TELECOM'
                               , 'FIRE PRO','RETAIN WALL',
                               -- PROJECT TRANSACTION TYPES(PROJECTTYPE) BELOW
                               'future use building','RADIO AMP',
                                -- AEC TRANSACTION TYPES('AECTYPE') BELOW
                              'BID CONTRACTOR', 'NEW SINGLE FAMILY','RESIDENTIAL MECHANICAL',
                              'RESIDENTIAL PLUMBING','RESIDENTIAL WTR HTR','COMMERCIAL ELECTRICAL','RESIDENTIAL ELECTRICAL', 'BACKFLOW DEVICE') 
				then 'Building'
           when PERMIT_TYPE in ('FOOD TRUCK VENDING', 'SPEC EVENT'
                               , 'TEMP REVOCABLE', 'TEMP SALES', 'TRANS MERCH','PLN RCPT','TEMP USE'
                               -- PROJECT TRANSACTION TYPES(PROJECTTYPE) BELOW
                               ,'ALTERN SIGN PROGRAM', 'ANNEXATION', 'CERT OF COMPLIANCE'
                               , 'CERTIFICATE OF COMPLIANCE', 'COMP PLAN AMENDMENT'
                               , 'COND USE PERMIT', 'CUP', 'DPB', 'FINAL DEV PLAN'
                               , 'FINAL PLAT', 'FLOODPLAIN MODIFICATION', 'HEIGHT EXCEPTION'
                               , 'MINOR MODIFICATION', 'MINOR SUBDIVISION', 'NULL'
                               , 'OUT OF CITY UTILITY', 'OUTLINE DEV PLAN', 'PRELIM DEV PLAN'
                               , 'PRELIMINARY PLAT', 'REFERRAL', 'SITE PLAN', 'SKETCH PLANS'
                               , 'SPECIAL DISTRICT', 'STREET NAME CHANGE', 'TELECOMMUNICATIONS'
                               , 'URBAN RENEWAL PLAN', 'VACATION', 'VARIANCE', 'VESTED RIGHTS'
                               , 'ZONING','CONS','REVOCABLE PERMIT','CONCEPT PLAN', 'FENCE', 'DEVELOPMENT APPLICATION', 'ADU C') 
				then 'Planning'
           else PERMIT_TYPE end 
		   from ArvadaOracledata a
end
else
begin
	update a
	set batch_type = 
		case when COLLECTED_BY in ('ECON', 'EPR', 'EPRS', 'EU', 'WEB') 
				then 'ETRAKIT'
           when CODE in ('ESCROW1','DEV','DEV1','DEV2','ESCR01','DEVAPP','ROWLAND','ROWLAN2','DEVPUB','PLAN10','PLAN11','ZONEV','APP43','PLANAPP42','PLANAPP42','ZONEB') 
				then 'Planning'
           when CODE = 'ESCROW' 
				then 'Engineering'
           when CODE = 'ROW' 
				then 'Engineering'
		   when CODE IN 
		   ('COM29','ROW61','ROW62','ROW63','TPLAN2','TPLN','TPLN1','TPLN3','TPLN4','TRAFF2','TRAFF3','TRAFF4','TRAFF5','TRAFF6','TRAFF7','TRFDIS','TRFDIS1','TRFDIS2',	'TRFDIS3')
				then 'Engineering'
           when PERMIT_TYPE in ('FLOODPL DEV', 'RIGHT OF WAY', 'YARDXX','ENG RCPT', 'ROWYARD','DRY Utility'
                               -- PROJECT TRANSACTION TYPES(PROJECTTYPE) BELOW
                               ,'future use engineering'
                                -- AEC TRANSACTION TYPES('AECTYPE') BELOW
                               ,'ROW CONTRACTOR', 'SITE DEV', 'SMALL CELL') 
				then 'Engineering'
           when CODE in ('LIMITED', 'BLDINSP', 'BCAB' ) and PERMIT_TYPE = 'CONTRACTOR' 
				then 'Building'
		   when code in ('COMBO10','COMBO11','COMBO12','COMBO13','COMBO9','DEMOL3','DEMOL4','SOLR3') 
			    then 'Building'
           when PERMIT_TYPE in ('ADU', 'COMBINATION', 'COMMERCIAL', 'COMMMISC'
                               , 'DEMOLITION', 'ELEVATOR', 'ELEVATORS'
                               , 'MISC', 'MOVING', 'PROPTY MAINT', 'Receipt'
                               , 'RESIDENTIAL', 'RESMISC', 'Retain Wall', 'ROOF'
                               , 'SIGN', 'SOLAR','BLDG RCPT','TELECOM'
                               , 'FIRE PRO','RETAIN WALL',
                               -- PROJECT TRANSACTION TYPES(PROJECTTYPE) BELOW
                               'future use building','RADIO AMP',
                                -- AEC TRANSACTION TYPES('AECTYPE') BELOW
                              'BID CONTRACTOR', 'NEW SINGLE FAMILY','RESIDENTIAL MECHANICAL',
                              'RESIDENTIAL PLUMBING','RESIDENTIAL WTR HTR','COMMERCIAL ELECTRICAL','RESIDENTIAL ELECTRICAL', 'BACKFLOW DEVICE') 
				then 'Building'
           when PERMIT_TYPE in ('FOOD TRUCK VENDING', 'SPEC EVENT'
                               , 'TEMP REVOCABLE', 'TEMP SALES', 'TRANS MERCH','PLN RCPT','TEMP USE'
                               -- PROJECT TRANSACTION TYPES(PROJECTTYPE) BELOW
                               ,'ALTERN SIGN PROGRAM', 'ANNEXATION', 'CERT OF COMPLIANCE'
                               , 'CERTIFICATE OF COMPLIANCE', 'COMP PLAN AMENDMENT'
                               , 'COND USE PERMIT', 'CUP', 'DPB', 'FINAL DEV PLAN'
                               , 'FINAL PLAT', 'FLOODPLAIN MODIFICATION', 'HEIGHT EXCEPTION'
                               , 'MINOR MODIFICATION', 'MINOR SUBDIVISION', 'NULL'
                               , 'OUT OF CITY UTILITY', 'OUTLINE DEV PLAN', 'PRELIM DEV PLAN'
                               , 'PRELIMINARY PLAT', 'REFERRAL', 'SITE PLAN', 'SKETCH PLANS'
                               , 'SPECIAL DISTRICT', 'STREET NAME CHANGE', 'TELECOMMUNICATIONS'
                               , 'URBAN RENEWAL PLAN', 'VACATION', 'VARIANCE', 'VESTED RIGHTS'
                               , 'ZONING','CONS','REVOCABLE PERMIT','CONCEPT PLAN', 'FENCE', 'DEVELOPMENT APPLICATION', 'ADU C') 
				then 'Planning'
           else PERMIT_TYPE end 
	from ArvadaOracledata a
end
  
if @FindUnassigned = 1 and @FindUnassigned is not null
begin

	select 
		COLLECTED_BY, 
		CODE, 
		PERMIT_TYPE, 
		TRANSACTION_NO, 
		TRANSACTION_TYPE,
		TRANSACTION_DATE,
		CREDIT_AMOUNT,
		DEBIT_AMOUNT,
		DESCRIPTION,
		GL_NUMBER,
		TAX_CODE, 
		RECEIPT_NO, 
		PAY_METHOD 
	from ArvadaOracledata
	where batch_type is null
	or batch_type not in ('Planning','Building','Engineering','ETRAKIT')
end



--load the final table we return to Oracle via the procedure usp_Arvada_OracleTrakitBatch
--calling a procedure from Oracle did not let it do work and then return a result so we call this one to do work
--the other to return it 
truncate table dbo.OracleFinalOutput
insert into dbo.OracleFinalOutput
(
	batch_type,
	transaction_date,
	debit_amount,
	credit_amount,
	[description],
	GL_NUMBER,
	tax_code,
	PAY_METHOD
)	  
SELECT 
	 batch_type,
      transaction_date, 
	  sum(debit_amount) debit_amount, 
	  sum(credit_amount) credit_amount,
      [description],
       GL_NUMBER,
      tax_code,
	  PAY_METHOD
FROM ArvadaOracledata
      GROUP BY 
		batch_type ,
		transaction_date, 
		[description],
        GL_NUMBER,
		tax_code,
		PAY_METHOD
      ORDER BY batch_type

end
GO
