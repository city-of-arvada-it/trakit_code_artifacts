USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_EtrakitSearchUpdate]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[usp_Arvada_EtrakitSearchUpdate]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_EtrakitSearchUpdate]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_Arvada_EtrakitSearchUpdate]
 @etrakitUsername varchar(50),
 @email varchar(80),
 @Active int,
 @FirstName varchar(50),
 @LastName varchar(50),
 @Address varchar(50),
 @City varchar(50),
 @CompanyName varchar(50)
as
begin

/*
This proc is used in an internally built winForms app
lets users search the etrakit user table and adjust data w\o access to
the admin website which does everything and can break way too much way too easily 
*/
set nocount on


update a
set
	email = case when @email is not null and @email != '' then @email else email end,
	active = case when @active in (0,1) then @active else active end,
	First_Name = @FirstName,
	Last_Name = @LastName,
	[Address] = @Address,
	City = @City,
	Company_Name = @CompanyName
from dbo.EtrakIt_User_Accounts a
where
	a.EtrakIt_Name = @etrakitUsername


end
GO
