USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ModelHomeTemplateItem_Delete]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_ModelHomeTemplateItem_Delete]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ModelHomeTemplateItem_Delete]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  
CREATE PROCEDURE [dbo].[tsp_T9_ModelHomeTemplateItem_Delete] 
              @RecID varchar(10)  = '0'
AS
BEGIN

       Declare @SQL Varchar(8000)
	          
       if @RecID <> '0'
	          Begin
				--Delete any detail records
					set @SQL = 'DELETE from Prmry_ModelHomeTemplateDetail WHERE (TemplateRecordID = ' + @RecID + ');'
					
					print(@sql)
					exec(@sql)

              end

              Begin

                     set @SQL = 'DELETE from Prmry_ModelHomeTemplateItem WHERE (RecordID = ' + @RecID + ');'


                     print(@sql)
                     exec(@sql)

              end

END





GO
