USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[ALP_GetLicenseDataChanges]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[ALP_GetLicenseDataChanges]
GO
/****** Object:  StoredProcedure [dbo].[ALP_GetLicenseDataChanges]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Author:		Erick Hampton
-- Create date: May 10, 2017
-- Description:	Returns data for the supplied license number before / after it has been processed
--				Optionally returns only data about a specific operation for a license number
--
--	Revisions:	EWH Aug 17, 2017: updated references to old table names to the new table names
--
-- ================================================================================================
CREATE PROCEDURE [dbo].[ALP_GetLicenseDataChanges]

	@BatchID int = -1,
	@LICENSE_NO varchar(20) = NULL,
	@OperationID int = NULL

AS
BEGIN

	declare @ErrMsg varchar(255) = ''
	
	if (select count(*) from [ALP_BatchParameterSetLicenseNumberOperationStatus] where BatchID=@BatchID and LicenseNumber=@LICENSE_NO) = 0
	begin
		set @ErrMsg = 'Invalid Batch ID (' + cast(@BatchID as varchar) + ') and License Number (' + @LICENSE_NO + ') combination.'
	end
	else
	begin
		
		declare @BatchStatusID int = -1
		select @BatchStatusID=StatusID from ALP_Batches where ID=@BatchID

		if @BatchStatusID > 2 -- some amount of processing has begun for the batch
		begin
			
			declare @OperationStatusID int = -1

			if @OperationID is not null and @OperationID > 0
			begin
				
				-- TODO: we need to add an OperationID column to dragon fang 

				-- get license data changes for just the specified operation ID
				select @OperationStatusID=OperationStatusID from [ALP_BatchParameterSetLicenseNumberOperationStatus] where BatchID=@BatchID and LicenseNumber=@LICENSE_NO and OperationID=@OperationID
				if @OperationStatusID <= 2 
				begin -- operation status is Unknown (-1), Pending (0), Processing (1), or Out of Scope (2)
					set @ErrMsg = 'This license number has not been processed.'
				end
				else
				begin -- operation status is Ready to Update (3), Completed (4), or Failed (5)
					select TableName, ColumnName, OldColumnValue [OldValue], ColumnValue [NewValue] 
					from [ALP_LicenseDataChanges] 
					where BatchID=@BatchID and LICENSE_NO=@LICENSE_NO --and OperationID=@OperationID 
					order by ID
				end
			end
			else
			begin
				-- get license data changes for all the operations for the license number
				select @OperationStatusID=min(OperationStatusID) from [ALP_BatchParameterSetLicenseNumberOperationStatus] where BatchID=@BatchID and LicenseNumber=@LICENSE_NO 

				if @OperationStatusID <= 2 
				begin -- lowest operation status is Unknown (-1), Pending (0), Processing (1), or Out of Scope (2)
					set @ErrMsg = 'This license number has not been processed.'
				end
				else
				begin -- lowest operation status is Ready to Update (3), Completed (4), or Failed (5)

					-- construct a table to house license data changes so there is only one row per [Table].[Column] change 
					-- for each unique row with changes and only the original and final column values are identified
					declare @data table (TableName varchar(200), PrimaryKeyValue varchar(200), ColumnName varchar(200), OldValue varchar(max), NewValue varchar(max))
					insert into @data (TableName, PrimaryKeyValue, ColumnName, OldValue, NewValue)
					select old.TableName, old.PrimaryKeyValue, old.ColumnName, old.OldValue, new.NewValue
					from (		select TableName, PrimaryKeyValue, ColumnName, min(ID) [oldID], min(OldColumnValue) [OldValue]
								from [ALP_LicenseDataChanges] ldc
								where BatchID=@BatchID and LICENSE_NO=@LICENSE_NO
								group by TableName, PrimaryKeyValue, ColumnName) old
					inner join (select TableName, PrimaryKeyValue, ColumnName, max(ID) [newID], max(ColumnValue) [NewValue]
								from [ALP_LicenseDataChanges] ldc
								where BatchID=@BatchID and LICENSE_NO=@LICENSE_NO
								group by TableName, PrimaryKeyValue, ColumnName) new
					on old.TableName=new.TableName and old.PrimaryKeyValue=new.PrimaryKeyValue and old.ColumnName=new.ColumnName 
					order by old.oldID
					--where ldc.TableName not in ('Fees','SubFees') -- we still need to group stuff for fees and subfees
					--and ldc.OldColumnValue <> ldc.ColumnValue -- we want to display the old and new values even if they are the same



					-- construct a table to house license data changes to the Fees tables -- Version = 'Old' or 'New'
					declare @fees table([Version] char(3), RECORDID varchar(30), CODE varchar(12), [DESCRIPTION] varchar(60), AMOUNT float, QUANTITY float, COMMENTS varchar(200), FORMULA varchar(2000), ACCOUNT varchar(30), FEETYPE varchar(8), ASSESSED_DATE datetime, ASSESSED_BY varchar(6), DETAIL varchar(1), INVOICED varchar(1), PAID varchar(1))
									
					-- construct a table to house license data changes to the SubFees tables -- Version = 'Old' or 'New'
					declare @subfees table([Version] char(3), RECORDID varchar(30), PARENTID varchar(30), CODE varchar(12), ITEM varchar(12), [DESCRIPTION] varchar(60), AMOUNT float, QUANTITY float, COMMENTS varchar(200), FORMULA varchar(2000), ACCOUNT varchar(30), SUBFEETYPE varchar(8), ASSESSED_DATE datetime, ASSESSED_BY varchar(6), INVOICED varchar(1), PAID varchar(1))

					-- populate the Fees table only if there are changes 
					if (select count(*) from [ALP_LicenseDataChanges] where BatchID=@BatchID and LICENSE_NO=@LICENSE_NO and TableName='Fees') > 0
					begin

						-- compile and insert data into @fees based on 1 pkv = 1 row to insert
						declare @PrimaryKeyValues table(PKV varchar(200))
						insert into @PrimaryKeyValues (PKV) select distinct PrimaryKeyValue from @data where TableName='Fees' -- get a distinct list of primary key values to iterate through;

						declare @PKV varchar(200)
						declare @cursor_fees cursor
						SET @cursor_fees = CURSOR FOR SELECT PKV FROM @PrimaryKeyValues
						OPEN @cursor_fees
						FETCH NEXT FROM @cursor_fees INTO @PKV
						WHILE @@FETCH_STATUS = 0
						BEGIN
							-- OldValue: don't include inserts here (if RECORDID is null all column values will be null)
							if (select OldValue from @data where PrimaryKeyValue=@PKV and TableName='Fees' and ColumnName='RECORDID') is not null
							begin
								insert into @fees ([Version],RECORDID,ACCOUNT,AMOUNT,ASSESSED_BY,ASSESSED_DATE,CODE,COMMENTS,[DESCRIPTION],DETAIL,FEETYPE,FORMULA,INVOICED,PAID,QUANTITY)
								select 'Old',
								(select OldValue from @data where PrimaryKeyValue=@PKV and TableName='Fees' and ColumnName='RECORDID'),
								(select OldValue from @data where PrimaryKeyValue=@PKV and TableName='Fees' and ColumnName='ACCOUNT'),
								(select OldValue from @data where PrimaryKeyValue=@PKV and TableName='Fees' and ColumnName='AMOUNT'),
								(select OldValue from @data where PrimaryKeyValue=@PKV and TableName='Fees' and ColumnName='ASSESSED_BY'),
								(select OldValue from @data where PrimaryKeyValue=@PKV and TableName='Fees' and ColumnName='ASSESSED_DATE'),
								(select OldValue from @data where PrimaryKeyValue=@PKV and TableName='Fees' and ColumnName='CODE'),
								(select OldValue from @data where PrimaryKeyValue=@PKV and TableName='Fees' and ColumnName='COMMENTS'),
								(select OldValue from @data where PrimaryKeyValue=@PKV and TableName='Fees' and ColumnName='DESCRIPTION'),
								(select OldValue from @data where PrimaryKeyValue=@PKV and TableName='Fees' and ColumnName='DETAIL'),
								(select OldValue from @data where PrimaryKeyValue=@PKV and TableName='Fees' and ColumnName='FEETYPE'),
								(select OldValue from @data where PrimaryKeyValue=@PKV and TableName='Fees' and ColumnName='FORMULA'),
								(select OldValue from @data where PrimaryKeyValue=@PKV and TableName='Fees' and ColumnName='INVOICED'),
								(select OldValue from @data where PrimaryKeyValue=@PKV and TableName='Fees' and ColumnName='PAID'),
								(select OldValue from @data where PrimaryKeyValue=@PKV and TableName='Fees' and ColumnName='QUANTITY')
							end

							-- NewValue: don't include deletes here (if RECORDID is null all column values will be null)
							if (select NewValue from @data where PrimaryKeyValue=@PKV and TableName='Fees' and ColumnName='RECORDID') is not null
							begin
								insert into @fees ([Version],RECORDID,ACCOUNT,AMOUNT,ASSESSED_BY,ASSESSED_DATE,CODE,COMMENTS,[DESCRIPTION],DETAIL,FEETYPE,FORMULA,INVOICED,PAID,QUANTITY)
								select 'New',
								(select NewValue from @data where PrimaryKeyValue=@PKV and TableName='Fees' and ColumnName='RECORDID'),
								(select NewValue from @data where PrimaryKeyValue=@PKV and TableName='Fees' and ColumnName='ACCOUNT'),
								(select NewValue from @data where PrimaryKeyValue=@PKV and TableName='Fees' and ColumnName='AMOUNT'),
								(select NewValue from @data where PrimaryKeyValue=@PKV and TableName='Fees' and ColumnName='ASSESSED_BY'),
								(select NewValue from @data where PrimaryKeyValue=@PKV and TableName='Fees' and ColumnName='ASSESSED_DATE'),
								(select NewValue from @data where PrimaryKeyValue=@PKV and TableName='Fees' and ColumnName='CODE'),
								(select NewValue from @data where PrimaryKeyValue=@PKV and TableName='Fees' and ColumnName='COMMENTS'),
								(select NewValue from @data where PrimaryKeyValue=@PKV and TableName='Fees' and ColumnName='DESCRIPTION'),
								(select NewValue from @data where PrimaryKeyValue=@PKV and TableName='Fees' and ColumnName='DETAIL'),
								(select NewValue from @data where PrimaryKeyValue=@PKV and TableName='Fees' and ColumnName='FEETYPE'),
								(select NewValue from @data where PrimaryKeyValue=@PKV and TableName='Fees' and ColumnName='FORMULA'),
								(select NewValue from @data where PrimaryKeyValue=@PKV and TableName='Fees' and ColumnName='INVOICED'),
								(select NewValue from @data where PrimaryKeyValue=@PKV and TableName='Fees' and ColumnName='PAID'),
								(select NewValue from @data where PrimaryKeyValue=@PKV and TableName='Fees' and ColumnName='QUANTITY')
							end

							FETCH NEXT FROM @cursor_fees INTO @PKV
						END
						CLOSE @cursor_fees
						DEALLOCATE @cursor_fees

					end 

					-- populate the SubFees table only if there are changes 
					if (select count(*) from [ALP_LicenseDataChanges] where BatchID=@BatchID and LICENSE_NO=@LICENSE_NO and TableName='SubFees') > 0
					begin

						-- compile and insert data into @subfees based on 1 pkv = 1 row to insert
						delete from @PrimaryKeyValues 
						insert into @PrimaryKeyValues (PKV) select distinct PrimaryKeyValue from @data where TableName='Subfees' -- get a distinct list of primary key values to iterate through;

						declare @cursor_subfees cursor
						SET @cursor_subfees = CURSOR FOR SELECT PKV FROM @PrimaryKeyValues
						OPEN @cursor_subfees
						FETCH NEXT FROM @cursor_subfees INTO @PKV
						WHILE @@FETCH_STATUS = 0
						BEGIN
							-- OldValue
							if (select OldValue from @data where PrimaryKeyValue=@PKV and TableName='Subfees' and ColumnName='RECORDID') is not null
							begin
								insert into @subfees ([Version],RECORDID,PARENTID,ACCOUNT,AMOUNT,ASSESSED_BY,ASSESSED_DATE,CODE,ITEM,COMMENTS,[DESCRIPTION],SUBFEETYPE,FORMULA,INVOICED,PAID,QUANTITY)
								select 'Old',
								(select OldValue from @data where PrimaryKeyValue=@PKV and TableName='Subfees' and ColumnName='RECORDID'),
								(select OldValue from @data where PrimaryKeyValue=@PKV and TableName='Subfees' and ColumnName='PARENTID'),
								(select OldValue from @data where PrimaryKeyValue=@PKV and TableName='Subfees' and ColumnName='ACCOUNT'),
								(select OldValue from @data where PrimaryKeyValue=@PKV and TableName='Subfees' and ColumnName='AMOUNT'),
								(select OldValue from @data where PrimaryKeyValue=@PKV and TableName='Subfees' and ColumnName='ASSESSED_BY'),
								(select OldValue from @data where PrimaryKeyValue=@PKV and TableName='Subfees' and ColumnName='ASSESSED_DATE'),
								(select OldValue from @data where PrimaryKeyValue=@PKV and TableName='Subfees' and ColumnName='CODE'),
								(select OldValue from @data where PrimaryKeyValue=@PKV and TableName='Subfees' and ColumnName='ITEM'),
								(select OldValue from @data where PrimaryKeyValue=@PKV and TableName='Subfees' and ColumnName='COMMENTS'),
								(select OldValue from @data where PrimaryKeyValue=@PKV and TableName='Subfees' and ColumnName='DESCRIPTION'),
								(select OldValue from @data where PrimaryKeyValue=@PKV and TableName='Subfees' and ColumnName='SUBFEETYPE'),
								(select OldValue from @data where PrimaryKeyValue=@PKV and TableName='Subfees' and ColumnName='FORMULA'),
								(select OldValue from @data where PrimaryKeyValue=@PKV and TableName='Subfees' and ColumnName='INVOICED'),
								(select OldValue from @data where PrimaryKeyValue=@PKV and TableName='Subfees' and ColumnName='PAID'),
								(select OldValue from @data where PrimaryKeyValue=@PKV and TableName='Subfees' and ColumnName='QUANTITY')
							end

							-- NewValue
							if (select NewValue from @data where PrimaryKeyValue=@PKV and TableName='Subfees' and ColumnName='RECORDID') is not null
							begin
								insert into @subfees ([Version],RECORDID,PARENTID,ACCOUNT,AMOUNT,ASSESSED_BY,ASSESSED_DATE,CODE,ITEM,COMMENTS,[DESCRIPTION],SUBFEETYPE,FORMULA,INVOICED,PAID,QUANTITY)
								select 'New',
								(select NewValue from @data where PrimaryKeyValue=@PKV and TableName='Subfees' and ColumnName='RECORDID'),
								(select NewValue from @data where PrimaryKeyValue=@PKV and TableName='Subfees' and ColumnName='PARENTID'),
								(select NewValue from @data where PrimaryKeyValue=@PKV and TableName='Subfees' and ColumnName='ACCOUNT'),
								(select NewValue from @data where PrimaryKeyValue=@PKV and TableName='Subfees' and ColumnName='AMOUNT'),
								(select NewValue from @data where PrimaryKeyValue=@PKV and TableName='Subfees' and ColumnName='ASSESSED_BY'),
								(select NewValue from @data where PrimaryKeyValue=@PKV and TableName='Subfees' and ColumnName='ASSESSED_DATE'),
								(select NewValue from @data where PrimaryKeyValue=@PKV and TableName='Subfees' and ColumnName='CODE'),
								(select NewValue from @data where PrimaryKeyValue=@PKV and TableName='Subfees' and ColumnName='ITEM'),
								(select NewValue from @data where PrimaryKeyValue=@PKV and TableName='Subfees' and ColumnName='COMMENTS'),
								(select NewValue from @data where PrimaryKeyValue=@PKV and TableName='Subfees' and ColumnName='DESCRIPTION'),
								(select NewValue from @data where PrimaryKeyValue=@PKV and TableName='Subfees' and ColumnName='SUBFEETYPE'),
								(select NewValue from @data where PrimaryKeyValue=@PKV and TableName='Subfees' and ColumnName='FORMULA'),
								(select NewValue from @data where PrimaryKeyValue=@PKV and TableName='Subfees' and ColumnName='INVOICED'),
								(select NewValue from @data where PrimaryKeyValue=@PKV and TableName='Subfees' and ColumnName='PAID'),
								(select NewValue from @data where PrimaryKeyValue=@PKV and TableName='Subfees' and ColumnName='QUANTITY')
							end

							FETCH NEXT FROM @cursor_subfees INTO @PKV
						END
						CLOSE @cursor_subfees
						DEALLOCATE @cursor_subfees
								
					end 

					-- select the return values

					-- 1. License Information --> maintain content of top and bottom of the panel 
					select TableName, ColumnName, OldValue, NewValue from 
					(
					select '1' [seq], TableName, ColumnName, OldValue, NewValue from @data where TableName='LICENSE2_MAIN' and ColumnName in ('COMPANY','APPLIED','APPLIED_BY','EXPIRED','EXPIRED_BY','ISSUED','ISSUED_BY','LICENSE_SUBTYPE','LICENSE_TYPE','STATUS','OWNERSHIP_TYPE','RESALE_ID','TAX_ID','MAINTEXTFIELD1','MAINTEXTFIELD2','MAINTEXTFIELD3','MAINTEXTFIELD4','MAINTEXTFIELD5','MAINTEXTFIELD6','MAINTEXTFIELD7','MAINTEXTFIELD8','STATUS_BY','OWNER_NAME') 
					union
					select '2' [seq], TableName, ColumnName, OldValue, NewValue from @data where TableName='LICENSE2_MAIN' and ColumnName in ('SITE_APN','SITE_GEOTYPE','SITE_ADDR','SITE_CITY','SITE_STATE','SITE_ZIP','SITE_TRACT','SITE_BLOCK','SITE_LOT_NO','SITE_SUBDIVISION','SITE_NUMBER','SITE_UNIT_NO','SITE_STREETNAME','SITE_ALTERNATE_ID','SITE_DESCRIPTION','SITE_STREETID','HISTORICAL_APN') 
					) a
					order by seq, ColumnName

					-- 2. Additional License Information --> maintain content of top and bottom of the panel 
					select TableName, ColumnName, OldValue, NewValue from 
					(				
					select '1' [seq], TableName, ColumnName, OldValue, NewValue from @data where TableName='LICENSE2_MAIN' and ColumnName in ('COMPANY_PRINT_AS','MAIL_ADDRESS1','MAIL_ADDRESS2','MAIL_CITY','MAIL_STATE','MAIL_ZIP','PHONE','PHONE_EXT','FAX','EMERGENCY','EMAIL') 
					union
					select '2' [seq], TableName, ColumnName, OldValue, NewValue from @data where TableName='LICENSE2_MAIN' and ColumnName in ('CHECKBOX1','CHECKBOX2','CHECKBOX3','CHECKBOX4','CHECKBOX5','CHECKBOX6','CHECKBOX7','CHECKBOX8','TEXTFIELD1','TEXTFIELD2','TEXTFIELD3','TEXTFIELD4','TEXTFIELD5','TEXTFIELD6','TEXTFIELD7','TEXTFIELD8') 
					) a
					order by seq, ColumnName

					-- 3. Contacts
					select TableName, ColumnName, OldValue, NewValue from @data where TableName='People' order by ColumnName

					-- 4. Inspections
					select TableName, ColumnName, OldValue, NewValue from @data where TableName='Inspections' or (TableName='LICENSE2_MAIN' and ColumnName='DEFAULT_INSPECTOR') order by TableName, ColumnName

					-- 5. Conditions
					select TableName, ColumnName, OldValue, NewValue from @data where TableName='Conditions' order by ColumnName

					-- Financial Information
					------------------------------------------------------------------------------------------------------------------------------------------------------------------------
					-- 6. LICENSE2_MAIN: FEES_CHARGED,FEES_PAID,BALANCE_DUE
					select TableName, ColumnName, OldValue, NewValue from @data where TableName='LICENSE2_MAIN' and ColumnName in ('FEES_CHARGED','FEES_PAID','BALANCE_DUE') order by ColumnName

					-- 7. Fees
					select * from @fees order by RECORDID

					-- 8. Subfees
					select * from @subfees order by RECORDID
					------------------------------------------------------------------------------------------------------------------------------------------------------------------------

					-- 9. Reviews
					select TableName, ColumnName, OldValue, NewValue from @data where TableName='Reviews' order by ColumnName

					-- 10. Chronology
					select TableName, ColumnName, OldValue, NewValue from @data  where TableName='Actions' order by ColumnName

					-- 11. Custom Screens
					select TableName, ColumnName, OldValue, NewValue from @data where TableName='LICENSE2_UDF' or (TableName='LICENSE2_MAIN' and ColumnName in ('LIAB_CARRIER','LIAB_EXP','LIAB_ISS','LIAB_NO','W_COMP_EXP','W_COMP_ISS','W_COMP_NO','WRKR_COMP')) order by TableName, ColumnName

					-- 12. Other
					select TableName, ColumnName, OldValue, NewValue from @data where TableName='LICENSE2_MAIN' and ColumnName in ('NOTES','SIC_1','SIC_2','SIC_3','ST_LIC_EXP','ST_LIC_ISS','REFERENCE_NO','TAG','YRMO','TSstatus') order by TableName, ColumnName
				end

			end


		end
		else
		begin
			set @ErrMsg = 'This batch has not yet begun processing.'
			print @ErrMsg
		end
	end


	-- communication!
	if @ErrMsg <> '' RAISERROR(@ErrMsg, 13,1) 

END

GO
