USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_PlanReviewAEDA]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[usp_Arvada_PlanReviewAEDA]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_PlanReviewAEDA]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_Arvada_PlanReviewAEDA]
	@Status nvarchar(max) = null,
	@ReportGroup nvarchar(50) = 'Public Hearing'
as
begin

/*
exec usp_Arvada_PlanReviewAEDA @status = '1ST REVIEW|2ND REVIEW|DECISION REVIEW|COMPLETENESS REVIEW|UNDER REVIEW|RESPONSE PEND|APPLIED|ON HOLD|CUSTOMER RESP. PEND|CONDITIONAL APPROVAL',
		@ReportGroup = 'Other'

exec usp_Arvada_PlanReviewAEDA @status = null,
		@ReportGroup = 'Public Hearing'
exec usp_Arvada_PlanReviewAEDA @status = null,
		@ReportGroup = 'Extended'
exec usp_Arvada_PlanReviewAEDA @status = null,
		@ReportGroup = 'No Public Hearing'
exec usp_Arvada_PlanReviewAEDA @status = null,
		@ReportGroup = 'Standard'

('da2017-0048', 'da2017-0049', 'da2017-0050', 'da2017-0051', 'da2017-0060' , 'da2017-0105', 'da2017-0108', 'da2017-0116')

Add planner to the report
Extended and Standard Track types are now Public Hearing
Include minor mods (Admin) and variances (PH) --> Attached to a DA as parent
Do not show CRW-PROJECT
*/

create table #status
(
	statusValue nvarchar(50)
)



create table #final
(
	project_no nvarchar(50),
	ProjectStatus nvarchar(50),
	ProjectDesc nvarchar(200),
	ActionName nvarchar(230),
	ActionDate datetime,
	CompletedDate datetime,
	ActionOrder int,
	ProjectTrack nvarchar(50),
	maxVal int,
	udfFields varchar(max),
	planner nvarchar(500)
)

insert into #status
(
	statusValue
)
select
	Val
from dbo.tfn_Arvada_SSRS_String_To_Table2(@status,'|',1)


 

create table #ProjTypes
(
	ProjectType varchar(50),
	RollupType varchar(50)
)

insert into #ProjTypes
(ProjectType, rolluptype)
select 'ADMIN 2ND SUBMITTAL', 'Administrative Track'
union all select 'ADMIN DECISION REVIEW','Administrative Track'
union all select 'ADMIN SUBMITAL','Administrative Track'
union all select 'Administrative Track','Administrative Track'
union all select 'PH 2ND SUBMITTAL','Public Hearing'
union all select 'PH DECISION REVIEW','Public Hearing'
union all select 'PH SUBMITTAL','Public Hearing'

 

insert into #final
(
	project_no,
	projectstatus,
	ProjectDesc,
	projectTrack,
	ActionName,
	ActionOrder,
	udfFields,
	planner
)
select
	m.PROJECT_NO,
	m.[STATUS],
	m.PROJECT_NAME,
	case 
		when u.trk_type = 'Extended' then 'EXTENDED TRACK'
		when u.trk_type = 'Administrative' then 'Administrative Track'
		when u.trk_type = 'Standard' then 'STANDARD TRACK'
		when u.TRK_TYPE = 'Public Hearing' then 'Public Hearing'
		ELSE 'NO TRACK DEFINED'
	END as projTrack,
	b.Hearing_Action,
	b.rid,
	(case when nullif(DA_TYPE_1,'') is not null then DA_TYPE_1 else '' end +
				case when nullif(DA_TYPE_2,'') is not null then ', ' + DA_TYPE_2 else '' end +
				case when nullif(DA_TYPE_3,'') is not null then ', ' + DA_TYPE_3 else '' end +
				case when nullif(DA_TYPE_4,'') is not null then ', ' + DA_TYPE_4 else '' end +
				case when nullif(DA_TYPE_5,'') is not null then ', ' + DA_TYPE_5 else '' end +
				case when nullif(DA_TYPE_6,'') is not null then ', ' + DA_TYPE_6 else '' end 						
				),
	m.PLANNER
from dbo.project_main m
left join dbo.project_udf u
	on m.PROJECT_NO = u.PROJECT_NO
left join 
	(
		Select 
			p.RollupType as Hearing_Type,
			t.Hearing_Action ,
			p.RollupType,
			row_number() over (partition by t.hearing_type order by t.no_days) as rid
		from Prmry_TimeClock t
		inner join #ProjTypes p
			on p.ProjectType = t.Hearing_Type
		WHERE t.GROUPNAME = 'PROJECT' 
		and p.RollupType in ('STANDARD TRACK', 'EXTENDED TRACK','Administrative Track')
		and Hearing_Action != 'Payment Due'
	) as b
	on b.Hearing_Type = case 
							when u.trk_type = 'Extended' then 'Public Hearing'
							when u.trk_type = 'Administrative' then 'Administrative Track'
							when u.trk_type = 'Standard' then 'Public Hearing'
							when u.TRK_TYPE = 'Public Hearing' then 'Public Hearing'
							ELSE 'NO TRACK DEFINED'
						END 
WHERE 
	(m.[STATUS] in (select statusValue from #status) or @status is null) --('COMPLETENESS REVIEW', 'UNDER REVIEW', 'RESPONSE PEND',  'APPLIED','ON HOLD'))
and 
	(
		@ReportGroup = 'Public Hearing' and u.trk_type in ('Extended', 'Standard','Public Hearing')
		or
		@ReportGroup = 'No Public Hearing' and u.trk_type in ('Administrative')
		or
		@ReportGroup = 'Other' and isnull(u.trk_type,'') not in ('Extended', 'Standard','Administrative','Public Hearing')
	)
and (nullif(m.PARENT_PROJECT_NO,'') is null 
		or (m.PROJECTTYPE in ('MINOR MODIFICATION','VARIANCE') and nullif(m.PARENT_PROJECT_NO,'') is null   ) --sub projects not included in dev. review
	)
and m.PROJECTTYPE not in ('PREAPPLICATION','OTHER','MINOR MODIFICATION')

and m.PROJECT_NO != 'CRW-PROJECT'


--union all

--select
--	m.PROJECT_NO,
--	m.[STATUS],
--	m.PROJECT_NAME,
--	case 
--		when u.trk_type = 'Extended' then 'EXTENDED TRACK'
--		when u.trk_type = 'Administrative' then 'Administrative Track'
--		when u.trk_type = 'Standard' then 'STANDARD TRACK'
--		ELSE 'NO TRACK DEFINED'
--	END as projTrack,
--	b.Hearing_Action,
--	b.rid,
--	(case when nullif(DA_TYPE_1,'') is not null then DA_TYPE_1 else '' end +
--				case when nullif(DA_TYPE_2,'') is not null then ', ' + DA_TYPE_2 else '' end +
--				case when nullif(DA_TYPE_3,'') is not null then ', ' + DA_TYPE_3 else '' end +
--				case when nullif(DA_TYPE_4,'') is not null then ', ' + DA_TYPE_4 else '' end +
--				case when nullif(DA_TYPE_5,'') is not null then ', ' + DA_TYPE_5 else '' end +
--				case when nullif(DA_TYPE_6,'') is not null then ', ' + DA_TYPE_6 else '' end 						
--				)
--from dbo.project_main m
--left join dbo.project_udf u
--	on m.PROJECT_NO = u.PROJECT_NO
--cross join 
--	(
--		Select 
--			Hearing_Type,
--			Hearing_Action ,
--			row_number() over (partition by hearing_type order by no_days) as rid
--		from Prmry_TimeClock t
--		WHERE t.GROUPNAME = 'PROJECT'  
--		and Hearing_Type = 'Conditional Approval'
--	) as b
--WHERE 
--   (m.[STATUS] in (select statusValue from #status) or @status is null) --('COMPLETENESS REVIEW', 'UNDER REVIEW', 'RESPONSE PEND',  'APPLIED','ON HOLD'))
--and m.PARENT_PROJECT_NO is null --sub projects not included in dev. review
--and m.PROJECTTYPE not in ('PREAPPLICATION','OTHER','MINOR MODIFICATION')
--and ( m.PROJECT_NO like 'da%' --for now, to cut down on clutter during development
--		--or m.project_no = 'CRW-PROJECT'
--				)
--and exists(
--			Select 1 
--			from dbo.Project_Actions a  
--			where a.ACTION_TYPE in ('Manager Review')
--				and a.PROJECT_NO = m.PROJECT_NO
--			)

update f
set 
	udfFields = ltrim(rtrim(case when substring(udfFields,1,1) = ',' then substring(udffields,2,len(udffields)) else udffields end))
from #final f


--get the highest sort val so we can add to it later
update f
set maxVal = b.maxVal
from #final f
inner join
	(
		select project_no, max(ActionOrder) as maxval
		from #final f
		group by project_no
	) as b
	on b.project_no = f.project_no

--plop in more dates that are not so auto matic
insert into #final
(
	project_no,
	projectstatus,
	ProjectDesc,
	projectTrack,
	ActionName,
	ActionOrder,
	planner,
	udfFields
)
select distinct
	project_no,
	projectstatus,
	ProjectDesc,
	projectTrack,
	b.Hearing_Action,
	case	
					when b.Hearing_Action = 'BOA Hearing' then f.maxVal + 1
					when b.Hearing_Action = 'Planning Commission Hearing' then f.maxVal + 2
					when b.Hearing_Action = 'City Council Public Hearing' then f.maxVal + 3
			end,
	f.planner,
	f.udfFields
from #final f
left join 
	(
		Select 
			Hearing_Type,
			Hearing_Action 
		from Prmry_TimeClock t
		WHERE t.GROUPNAME = 'PROJECT' 
		and t.Hearing_Action in ('BOA Hearing','Planning Commission Hearing','City Council Public Hearing')
	) as b
	on 1=1
where projectTrack in ('STANDARD TRACK', 'EXTENDED TRACK','Public Hearing')

--get the highest sort val so we can add to it later
update f
set maxVal = b.maxVal
from #final f
inner join
	(
		select project_no, max(ActionOrder) as maxval
		from #final f
		group by project_no
	) as b
	on b.project_no = f.project_no

insert into #final
(
	project_no,
	projectstatus,
	ProjectDesc,
	projectTrack,
	ActionName,
	ActionOrder,
	planner,
	udfFields
)
select distinct
	project_no,
	projectstatus,
	ProjectDesc,
	projectTrack,
	b.Hearing_Action,
	case	
					when b.Hearing_Action = 'Manager Review' then f.maxVal + 1
					when b.Hearing_Action = 'Customer Response Due' then f.maxVal + 2
					when b.Hearing_Action = 'Staff Review Due' then f.maxVal +3
			end,
	f.planner,
	f.udfFields
from #final f
left join 
	(
		Select 
			Hearing_Type,
			Hearing_Action 
		from Prmry_TimeClock t
		WHERE t.GROUPNAME = 'PROJECT' 
		and t.Hearing_type in ('Conditional Approval')
	) as b
	on 1=1
where  
  exists(
			Select 1 
			from dbo.Project_Actions a  
			where a.ACTION_TYPE in ('Manager Review')
				and a.PROJECT_NO = f.PROJECT_NO
			)


update f
set 
	ActionDate = a.ACTION_DATE,
	CompletedDate = a.COMPLETED_DATE
from #final f
inner join dbo.Project_Actions a
	on a.PROJECT_NO = f.Project_No
	and a.ACTION_TYPE = f.ActionName


declare @url nvarchar(1000)
select @url =  [dbo].[ReturnTrakitURL]('project')	 



select
	project_no,
	ProjectStatus ,
	ProjectDesc,
	ActionName ,
	ActionDate ,
	CompletedDate,
	ActionOrder,
	case when ProjectTrack in ('STANDARD TRACK', 'EXTENDED TRACK', 'Public Hearing') then 'Cases going to Public Hearing'
		 when ProjectTrack = 'Administrative Track' then 'Cases not going to Public Hearing or After Public Hearing'
		 else 'No Track Selected Yet'
	end as ProjectGroup	,
	case when ProjectTrack in ('STANDARD TRACK', 'EXTENDED TRACK', 'Public Hearing') then 1
		 when ProjectTrack = 'Administrative Track' then 2
		 else 3
	end as ProjectGroupNum,
	@url+project_no+'&action=undefined&category=undefined' as GoToProject,
	1 as SumThis,
	udfFields,
	planner
from #final
order by 
	ProjectGroupNum,
	project_no,
	actionorder





end
GO
