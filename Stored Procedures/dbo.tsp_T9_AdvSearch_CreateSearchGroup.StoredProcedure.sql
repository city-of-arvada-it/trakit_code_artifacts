USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearch_CreateSearchGroup]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_AdvSearch_CreateSearchGroup]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearch_CreateSearchGroup]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create Procedure [dbo].[tsp_T9_AdvSearch_CreateSearchGroup]

@DepartmentName varchar(50) = null,
@UserID varchar(20) =null,
@DepartmentID int =  -1 out,
@ErrorMessage varchar(250)= null out
As 
begin	
	If @DepartmentName is not null or @userID is not null
		begin			
			if not exists  (Select * from [Prmry_AdvSearch_Departments] where DepartmentName = @DepartmentName)
			begin
				Insert into [Prmry_AdvSearch_Departments] (DepartmentName, DepartmentLeader) Values  (@DepartmentName, @UserID)
				set @DepartmentID = SCOPE_IDENTITY()
				Insert Into [Prmry_AdvSearch_DepartmentMembers] (DepartmentID, UserID, CanGet, CanShare, CAnRename, CanDelete,NotifyofNewShare, NotifyofDeletedShare)
				Values (@DepartmentID, @UserID, 1,1,1,1,0,0) -- since user created this department, should have all access to this department.
			end
			else
			begin
				Set @DepartmentID = -1
				Set @ErrorMessage = 'This Search Group already exists'				
			end
		end
end
return
GO
