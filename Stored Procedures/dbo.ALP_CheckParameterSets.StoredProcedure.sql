USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[ALP_CheckParameterSets]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[ALP_CheckParameterSets]
GO
/****** Object:  StoredProcedure [dbo].[ALP_CheckParameterSets]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- ============================================================================================================================
-- Author:		Erick Hampton
-- Create date: Jul 6, 2017
-- Description:	Validates ALP configuration settings. This was created previously but did not contain these header comments.
-- ============================================================================================================================
CREATE PROCEDURE [dbo].[ALP_CheckParameterSets]

	@ParameterSetID int = -1,
	@CheckPassed bit out,
	@ErrorType varchar(255) = '' out
AS
BEGIN
	set @CheckPassed = 0


	-- parameter set exists?
	if (select count(*) from ALP_ParameterSets ps where ps.Id=@ParameterSetID) = 0
	begin
		set @ErrorType= 'InvalidParameterSetId' 
		return
	end


	-- parameter set type is valid?
	if (select count(*) from ALP_ParameterSets ps inner join ALP_ParameterSetTypes pst on ps.ParameterSetTypeId=pst.Id where ps.Id=@ParameterSetID) = 0 
	begin
		set @ErrorType= 'InvalidParameterSetTypeId' 
		return
	end
	

	-- parameter set has operations assigned?
	if (select count(*) from ALP_ParameterSetOperations where ParameterSetId=@ParameterSetID) = 0 
	begin
		set @ErrorType= 'NoOperationsAssigned' 
		return
	end

	
	-- are there any required ValueNames for the operations assigned to parameter set that have not been assigned values?
	if (select count(*) from [ALP_OperationValuesByParameterSet] where ParameterSetId=@ParameterSetID and IsValueOptional=0 and Value is null) > 0 
	begin
		set @ErrorType= 'RequiredOperationValuesMissing' 
		return
	end


	-- this is a system error (unless the operation does not require access to the database), but will need to be caught and corrected --> this one cannot be corrected by the user
	-- check if rows exist for the operations in the required columns table
	if (select count(*) from [ALP_OperationRequiredColumns] orc inner join ALP_ParameterSetOperations pso on orc.OperationID=pso.OperationID and pso.ParameterSetId=@ParameterSetID) = 0 
	begin
		set @ErrorType= 'OperationRequiredColumnsNotSpecified' 
		return
	end

	

	-- operations assigned to parameter set has ValueNames assigned?
	-- no value names assigned to an operation --> this is not necessarily an error!  --> pass back a @severity level maybe?
	if (select count(*) from ALP_OperationValueNames ovn inner join ALP_ParameterSetOperations pso on ovn.OperationId=pso.OperationId and pso.ParameterSetId=@ParameterSetId) = 0 
	begin
		set @ErrorType= 'NoOperationValueNamesAssigned' 
		return
	end


	
	-- if the parameter set type is a renewal, does it have at least one renewal period assigned to it?
	-- NOTE: this is done LAST so all other validations are performed first in case we are running a renewal parameter set as ad-hoc
	if (select count(*) from [ALP_ParameterSets] ps inner join [ALP_ParameterSetTypes] pst on ps.ParameterSetTypeId=pst.Id where ps.Id=@ParameterSetID and pst.TypeName='Renewal') > 0
	begin
		if (select count(*) from [ALP_RenewalParameterSetAssignment] rpsa inner join [ALP_RenewalPeriods] rp on rpsa.RenewalPeriodId=rp.Id where rpsa.ParameterSetId=@ParameterSetID) = 0
		begin
			set @ErrorType= 'NoRenewalPeriodAssigned' 
			return
		end
	end



	-- TODO: 
	--		 we need a global (not specific to a ParameterSetID) validation that identies 
	--		 if any renewal periods overlap in a fashion where exactly one renewal ParameterSetID cannot be 
	--		 identified for any given date within all applicable renewal periods 





	set @CheckPassed = 1

END
GO
