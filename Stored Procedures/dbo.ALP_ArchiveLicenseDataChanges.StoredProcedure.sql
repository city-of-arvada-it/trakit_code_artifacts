USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[ALP_ArchiveLicenseDataChanges]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[ALP_ArchiveLicenseDataChanges]
GO
/****** Object:  StoredProcedure [dbo].[ALP_ArchiveLicenseDataChanges]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==============================================================================================================================================================================
-- Author:		Erick Hampton
-- Create date: Apr 3, 2017
-- Description:	Archives license data changes to ALP_LicenseDataChangesArchive
--				Deletes associated rows from ALP_Batches, ALP_LicenseDataChanges and ALP_BatchParameterSetLicenseNumberOperationStatus
--				Does NOT delete rows from ALP_LicensesRunPerRenewal
-- ==============================================================================================================================================================================
CREATE PROCEDURE [dbo].[ALP_ArchiveLicenseDataChanges]

	@BatchID int = NULL,
	@LICENSE_NO varchar(30) = NULL
AS
BEGIN

	if @BatchID is not null and @BatchID > -1 and @LICENSE_NO is not null and @LICENSE_NO <> ''
	begin

		BEGIN TRY
			begin tran

			-- populate [ALP_LicenseDataChangesArchive] table
			insert into [ALP_LicenseDataChangesArchive] ([LicenseDataChangeID]
														,[LICENSE_NO]
														,[TransformationCommand]
														,[TransformationOrder]
														,[TableName]
														,[PrimaryKeyColumnName]
														,[PrimaryKeyValue]
														,[ColumnName]
														,[ColumnValue]
														,[OldColumnValue]
														,[BatchID]
														,[BatchCreationDateTime]
														,[SharedSearchID]
														,[ParameterSetID]
														,[ParameterSetType]
														,[CallingSystem]
														)

			select ldc.[ID] [LicenseDataChangeID],ldc.[LICENSE_NO],ldc.[TransformationCommand],ldc.[TransformationOrder],ldc.[TableName],ldc.[PrimaryKeyColumnName],ldc.[PrimaryKeyValue],ldc.[ColumnName],ldc.[ColumnValue],ldc.[OldColumnValue],ldc.[BatchID]
				  ,b.[CreationDateTime] [BatchCreationDateTime],b.[SharedSearchID]
				  ,bpslnos.[ParameterSetID]
				  ,pst.TypeName
				  ,c.[System]

			from [ALP_LicenseDataChanges] ldc
				inner join [ALP_Batches] b on ldc.BatchID=b.Id
				inner join [ALP_BatchParameterSetLicenseNumberOperationStatus] bpslnos on (ldc.BatchID=bpslnos.BatchID and ldc.LICENSE_NO=bpslnos.LicenseNumber)
				inner join [ALP_ParameterSets] ps on ps.Id=bpslnos.ParameterSetID
				left  join [ALP_ParameterSetTypes] pst on pst.Id=ps.ParameterSetTypeId
				left  join [ALP_Callers] c on c.ID=b.CallerID

			where ldc.BatchID=@BatchID and ldc.LICENSE_NO=@LICENSE_NO

			-- TBD: store operation durations for estimating processing times?
			-- TODO

			-- delete corresponding data from ALP_Batches, ALP_LicenseDataChanges, ALP_BatchParameterSetLicenseNumberOperationStatus
			delete from [ALP_BatchParameterSetLicenseNumberOperationStatus] where BatchID=@BatchID and LicenseNumber=@LICENSE_NO
			delete from [ALP_LicenseDataChangesArchive] where BatchID=@BatchID and LICENSE_NO=@LICENSE_NO
			delete from [ALP_Batches] where ID=@BatchID 

			commit
		END TRY
		BEGIN CATCH
			rollback
			declare @ErrMsg nvarchar(4000)
			raiserror(@ErrMsg, 16, 1) with nowait
		END CATCH

	end

END
GO
