USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_GetPermitNames]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[usp_Arvada_GetPermitNames]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_GetPermitNames]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		Jesse Hutchens
-- Create date: 06/23/2016
-- Description:	Used in printPermit_TH.aspx
-- =============================================
exec  dbo.usp_Arvada_GetEtrakItUDFFields 'FENC16-00172'
*/
create PROCEDURE [dbo].[usp_Arvada_GetPermitNames]
	@permitno varchar(50)
AS
BEGIN 

SET NOCOUNT ON;
 --------------------------
 --- Declare Variables   --
 --------------------------
 
 
 --------------------------
 --- Create Temp Tables  --
 --------------------------

	 
 --------------------------
 --- Body of Procedure   --
 --------------------------

 select		
	NameType,
	Name,
	Address1, 
	City,
	State,
	Zip,
	Phone,
	 ltrim(rtrim(case when Address1 is null then '' else Address1 + ' ' end + 
	 case when City is null then '' else City + ' ' end + 
	 case when [State] is null then '' else [State] + ' ' end + 
	 case when Zip is null then '' else Zip + ' ' end )) as FullAddress
 from dbo.permit_people
 where PERMIT_NO = @permitno
 Order by
	NameType



END
GO
