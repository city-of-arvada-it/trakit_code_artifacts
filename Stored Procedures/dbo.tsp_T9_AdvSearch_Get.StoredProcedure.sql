USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearch_Get]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_AdvSearch_Get]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearch_Get]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  

-- ===================================================================================================================================================
-- DESCRIPTION:
--		For a specified @UserID, @SearchTable, and @SearchID (optional),
--			* returns the search item data as output parameters 
--			* returns the search detail as the output
--
-- NOTES:
--		The searches are all the same but I didn't want to use dynamic SQL to sacrifice speed for duplicated code.
--		[tsp_T9_AdvSearch_InitializeUser] is called to initialize user info if we can't get the SearchID from [tfn_T9_AdvSearch_GetSearchID]
--
--		@SearchTable has the following possible values:
--			current	- SearchID will be output - gets data from Current tables for UserID
--			last	- SearchID will be output - gets data from Last tables for UserID
--			new		- SearchID will be output - gets data from New tables for UserID
--			shared	- SearchID required to retrieve - gets data from Shared tables for SearchID
--			saved	- SearchID required to retrieve - gets data from Saved tables for SearchID
--
-- REVISION HISTORY: 
--		 2015 May 14 - EWH - Initial version
-- ===================================================================================================================================================
-- ---------
/*
TEST:

DECLARE @RC int
DECLARE @UserID varchar(50)
DECLARE @SearchTable varchar(10)
DECLARE @SearchID int
DECLARE @Group varchar(50)
DECLARE @SearchType varchar(50)
DECLARE @Description varchar(250)

set @UserID='EH'
set @SearchTable='shared'
set @SearchID='2'

EXECUTE @RC = [dbo].[tsp_T9_AdvSearch_Get] 
   @UserID
  ,@SearchTable
  ,@SearchID OUTPUT
  ,@Group OUTPUT
  ,@SearchType OUTPUT
  ,@Description OUTPUT
GO

*/
-- ---------

CREATE PROCEDURE [dbo].[tsp_T9_AdvSearch_Get]
	@UserID varchar(50) = null,
	@SearchTable varchar(10) = 'current',
	@SearchID int = -1 out,					
	@Group varchar(50) = 'PERMIT' out,		
	@SearchType varchar(50) = 'PERMIT_MAIN' out,
	@Description varchar(250) = null out,
	@SearchName varchar(50) = null out,
	@RefSearchID int = null out,
	@RefTable varchar(50) = null out,
	@IsDirty bit = 0 out,
	@GisTable varchar(50) = null out,
	@GisRecordID int = -1 out,
	@GeometryFilterEnabled bit = 0 out
AS
BEGIN

if @UserID is not null
begin
	
	set @IsDirty = 0; -- default to not dirty
		if @SearchTable in ('current','last','new')
	begin
		-- get the @SearchID
		set @SearchID = dbo.tfn_T9_AdvSearch_GetSearchID(@UserID, @SearchTable)

		if @SearchID is null 
		begin
			-- initialize user and try to get SearchID again...
			exec [tsp_T9_AdvSearch_InitializeUser] @UserID
			set @SearchID = dbo.tfn_T9_AdvSearch_GetSearchID(@UserID, @SearchTable)
		end
	end

	-- fetch the criteria now that we surely (fingers crossed!) have a @SearchID
	if @SearchID > 0 
	begin

		if @SearchTable='current'
		begin
			-- item (output parameters)
			SELECT top 1 @SearchID = RecordID, @SearchName = SearchName, @Group = GroupName, @SearchType = SearchType, @Description = [Description], @RefSearchID = RefSearchID, @RefTable = RefTable, @IsDirty = IsDirty, @GisTable = GisTable, @GisRecordID = GisRecordID, @GeometryFilterEnabled = GeometryFilterEnabled 
			from [Prmry_AdvSearch_CurrentItem] (nolock)
			where RecordID = @SearchID

			-- get the search criteria detail
			select [RecordID], [ItemID], [TableName], [FieldName], [DisplayName], [Datatype], [Filter], [Value1], [Value2], [Seq]
			from [Prmry_AdvSearch_CurrentDetail] (nolock)
			where ItemID = @SearchID
			order by Seq
		end
		else
		begin
			if @SearchTable='last'
			begin
				-- item (output parameters)
				SELECT top 1 @SearchID = RecordID, @SearchName = SearchName, @Group = GroupName, @SearchType = SearchType, @Description = [Description], @RefSearchID = RefSearchID, @RefTable = RefTable, @IsDirty = IsDirty, @GisTable = GisTable, @GisRecordID = GisRecordID, @GeometryFilterEnabled = GeometryFilterEnabled 
				from [Prmry_AdvSearch_LastItem] (nolock)
				where RecordID = @SearchID

				-- get the search criteria detail
				select [RecordID], [ItemID], [TableName], [FieldName], [DisplayName], [Datatype], [Filter], [Value1], [Value2], [Seq]
				from [Prmry_AdvSearch_LastDetail] (nolock)
				where ItemID = @SearchID
				order by Seq
			end
			else
			begin
				if @SearchTable='new'
				begin
					-- item (output parameters)
					SELECT top 1 @SearchID = RecordID, @SearchName = SearchName, @Group = GroupName, @SearchType = SearchType, @Description = [Description], @RefSearchID = RefSearchID, @RefTable = RefTable, @GisTable = GisTable, @GisRecordID = GisRecordID, @GeometryFilterEnabled = GeometryFilterEnabled 
					from [Prmry_AdvSearch_NewItem] (nolock)
					where RecordID = @SearchID

					-- get the search criteria detail
					select [RecordID], [ItemID], [TableName], [FieldName], [DisplayName], [Datatype], [Filter], [Value1], [Value2], [Seq]
					from [Prmry_AdvSearch_NewDetail] (nolock)
					where ItemID = @SearchID
					order by Seq
				end
				else
				begin
					if @SearchTable='saved'
					begin
						-- item (output parameters)
						SELECT top 1 @SearchID = RecordID, @SearchName = SearchName, @Group = GroupName, @SearchType = SearchType, @Description = [Description], @GisTable = GisTable, @GisRecordID = GisRecordID, @GeometryFilterEnabled = GeometryFilterEnabled 
						from [Prmry_AdvSearch_SavedItem] (nolock)
						where RecordID = @SearchID

						-- get the search criteria detail
						select [RecordID], [ItemID], [TableName], [FieldName], [DisplayName], [Datatype], [Filter], [Value1], [Value2], [Seq]
						from [Prmry_AdvSearch_SavedDetail] (nolock)
						where ItemID = @SearchID
						order by Seq
					end
					else
					begin
						if @SearchTable='shared'
						begin
							-- item (output parameters)
							SELECT top 1 @SearchID = RecordID, @SearchName = SearchName, @Group = GroupName, @SearchType = SearchType, @Description = [Description], @GisTable = GisTable, @GisRecordID = GisRecordID, @GeometryFilterEnabled = GeometryFilterEnabled 
							from [Prmry_AdvSearch_SharedItem] (nolock)
							where RecordID = @SearchID

							-- get the search criteria detail
							select [RecordID], [ItemID], [TableName], [FieldName], [DisplayName], [Datatype], [Filter], [Value1], [Value2], [Seq]
							from [Prmry_AdvSearch_SharedDetail] (nolock)
							where ItemID = @SearchID
							order by Seq
						end
					end
				end
			end
		end

	end
end

--print '@UserID = ' + isnull(@UserID,'NULL')
--print '@SearchTable = ' + isnull(@SearchTable,'NULL')
--print '@SearchID (out) = ' + isnull(cast(@SearchID as varchar),'NULL')
--print '@Group (out) = ' + isnull(@Group,'NULL')
--print '@SearchType (out) = ' + isnull(@SearchType,'NULL')
--print '@Description (out) = ' + isnull(@Description,'NULL')
--print '@SearchName (out) = ' + isnull(@SearchName,'NULL')
--print '@RefTable (out) = ' + isnull(@RefTable,'NULL')
--print '@RefSearchID (out) = ' + isnull(cast(@RefSearchID as varchar),'NULL')

return
END
GO
