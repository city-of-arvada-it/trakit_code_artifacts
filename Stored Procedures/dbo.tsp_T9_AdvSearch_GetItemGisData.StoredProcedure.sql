USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearch_GetItemGisData]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_AdvSearch_GetItemGisData]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearch_GetItemGisData]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[tsp_T9_AdvSearch_GetItemGisData]

	@GroupName varchar(20) = 'permit',
	@KeyFieldValue varchar(30) = '',
	--@COORD_X float = null out,
	--@COORD_Y float = null out,
	--@LAT float = null out,
	--@LON float = null out,
	@GIS_SITE_GEO_TYPE varchar(255) = null out,
	@GIS_SITE_ALTERNATE_ID varchar(255) = null out,
	@GIS_SITE_APN varchar(255) = null out,
	@LOC_RECORDID varchar(255) = null out

AS
BEGIN

if @GroupName <> 'AEC' -- AEC doesn't use GIS
begin
	if @GroupName = 'GEO' set @LOC_RECORDID = @KeyFieldValue -- LOC_RECORDID = RECORDID for GIS
	else
	begin

		declare @KeyField varchar(40)
		select @KeyField = dbo.tfn_ActivityNoField(@GroupName)

		declare @LridField varchar(40)
		declare @MainTable varchar(255)

		-- set main table and column name that contains LOC_RECORDID value
		if @GroupName = 'CRM' -- CRM is a special snowflake
		begin
			set @MainTable = 'CRM_ISSUES'
			set @LridField = 'ISSUE_LOC_RECORDID'
		end
		else
		begin
			set @MainTable = @GroupName + '_MAIN'
			set @LridField = 'LOC_RECORDID'
		end


		-- create select statement to get the LOC_RECORDID for the key field value
		declare @sql nvarchar(max)
		set @sql = 'select top 1 @lrid = ' + @LridField + ' from ' + @MainTable + ' where ' + @KeyField + ' = ' + '''' + @KeyFieldValue + ''''
		--print @sql


		exec sp_executesql @sql, N'@lrid varchar(255) out', @lrid = @LOC_RECORDID out

	end

	-- only continue if we have a @LOC_RECORDID
	if @LOC_RECORDID is not null and @LOC_RECORDID <> ''
	begin
		-- update the output parameters
		select	
				--  @COORD_X = GEO_OWNERSHIP.COORD_X
				--, @COORD_Y = GEO_OWNERSHIP.COORD_Y
				--, @LAT = GEO_OWNERSHIP.LAT
				--, @LON = GEO_OWNERSHIP.LON
				--, 
					@GIS_SITE_GEO_TYPE = GEO_OWNERSHIP.GEOTYPE
				, @GIS_SITE_ALTERNATE_ID = GEO_OWNERSHIP.SITE_ALTERNATE_ID
				, @GIS_SITE_APN = GEO_OWNERSHIP.SITE_APN 

		FROM GEO_OWNERSHIP 
		WHERE RECORDID = @LOC_RECORDID
	end

end



-- check output
print '@GroupName = ' + isnull(@GroupName,'NULL')
print '@KeyFieldValue = ' + isnull(@KeyFieldValue,'NULL')
--print '@COORD_X (out) = ' + isnull(cast(@COORD_X as varchar),'NULL')
--print '@COORD_Y (out) = ' + isnull(cast(@COORD_Y as varchar),'NULL')
--print '@LAT (out) = ' + isnull(cast(@LAT as varchar),'NULL')
--print '@LON (out) = ' + isnull(cast(@LON as varchar),'NULL')
print '@GIS_SITE_GEO_TYPE (out) = ' + isnull(@GIS_SITE_GEO_TYPE,'NULL')
print '@GIS_SITE_ALTERNATE_ID (out) = ' + isnull(@GIS_SITE_ALTERNATE_ID,'NULL')
print '@GIS_SITE_APN (out) = ' + isnull(@GIS_SITE_APN,'NULL')
print '@LOC_RECORDID (out) = ' + isnull(@LOC_RECORDID,'NULL')


/*
SAMPLE OUTPUT

select top 1 @lrid = LOC_RECORDID from permit_MAIN where PERMIT_NO = 'PLUM2012-00007'
@GroupName = permit
@KeyFieldValue = PLUM2012-00007
@COORD_X (out) = 6.12126e+006
@COORD_Y (out) = 1.81373e+006
@LAT (out) = 36.9662
@LON (out) = -122.007
@GIS_SITE_GEO_TYPE (out) = ADDRESS
@GIS_SITE_ALTERNATE_ID (out) = 3E1C7C13-A839-476D-8C93-180835178378
@GIS_SITE_APN (out) = 01025102
@LOC_RECORDID (out) = ADD:10033

*/

/*
TEST DATA

PERMIT_NO
PLUM2012-00007
PLUM2012-00008
PLUM2012-00010
PLUM2012-00011
PLUM2012-00012
PLUM2013-00001

LOC_RECORDID
ADD:10033
APN:00923447
APN:00922148
APN:00258244
APN:00922148
APN:00258226


*/


END
GO
