USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_decrypt_data]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_decrypt_data]
GO
/****** Object:  StoredProcedure [dbo].[tsp_decrypt_data]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[tsp_decrypt_data] 
	@recordid AS VARCHAR(50),
	@field AS VARCHAR(20), 
	@table AS VARCHAR(30) = NULL
AS
BEGIN

	OPEN SYMMETRIC KEY SymKey
	DECRYPTION BY CERTIFICATE EncryptCert;
	IF @table IS NULL 
		BEGIN
			EXEC ('	SELECT CONVERT(VARCHAR(50), DECRYPTBYKEY(' + @field + '))
					FROM protected_data
					WHERE license_recordid = ''' + @recordid + '''' )
		END 
	ELSE
		BEGIN
			IF @table = 'license2_main'
				SELECT CONVERT(VARCHAR(20), DECRYPTBYKEY(TAX_ID))
				FROM LICENSE2_MAIN
				WHERE RECORDID = @recordid
			IF @table = 'aec_main'
				SELECT CONVERT(VARCHAR(20), DECRYPTBYKEY(a.TAX_ID))
				FROM AEC_MAIN AS a
				WHERE RECORDID = @recordid
		END 
	CLOSE SYMMETRIC KEY symkey
END 
GO
