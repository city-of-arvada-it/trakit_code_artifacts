USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_BasicFocusMeasures]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[usp_Arvada_BasicFocusMeasures]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_BasicFocusMeasures]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec dbo.usp_Arvada_BasicFocusMeasures '06/01/2015','06/30/2015'

CREATE PROCEDURE [dbo].[usp_Arvada_BasicFocusMeasures]
(
	@FromDate datetime,
	@ToDate DATETIME ,
	@ReportType VARCHAR(50) = null
)

AS 

BEGIN

SET NOCOUNT ON

/*
exec dbo.usp_Arvada_BasicFocusMeasures '06/01/2015','06/30/2015'

*/

 --------------------------
 --- Declare Variables   --
 --------------------------

 
 --------------------------
 --- Create Temp Tables  --
 --------------------------
 IF OBJECT_ID('tempdb.dbo.#Audit') IS NOT NULL DROP TABLE #Audit
CREATE TABLE #Audit
(
	PRIMARY_KEY_VALUE VARCHAR(50), 
	CompletedDateTime DATETIME,
	CompleteByDateTime DATETIME,
	CompleteByTime VARCHAR(50)
)
	 
 --------------------------
 --- Body of Procedure   --
 --------------------------

--Used in Permitting FOCUS Measures below (Group 2):
--Go to the audit trail to get when something was completed with the time it was completed to compare completedby date and time
--to scheduled to be completed by date and time
INSERT INTO #Audit
( 
	PRIMARY_KEY_VALUE , 
	CompletedDateTime 
)
SELECT
	pat.PRIMARY_KEY_VALUE,  
	MIN(pat.TRANSACTION_DATETIME) AS FirstCompletedDateAfter
FROM dbo.permit_inspections pin WITH (NOLOCK) 
INNER JOIN dbo.Prmry_AuditTrail pat WITH (NOLOCK)
	ON pin.RECORDID = pat.PRIMARY_KEY_VALUE
	AND pat.FIELD_NAME = 'COMPLETED_DATE'
	AND pat.TRANSACTION_DATETIME >= pin.SCHEDULED_DATE --the action happened after it was received last
WHERE 
	pin.SCHEDULED_DATE >= @FromDate 
	AND pin.SCHEDULED_DATE < dateadd(DAY,1,@ToDate)
	AND (CHARINDEX('AM',Scheduled_Time) > 0 
			OR CHARINDEX('PM',Scheduled_Time) > 0
		)
GROUP BY
	pat.PRIMARY_KEY_VALUE 

--Set the Complete BY time to either 2pm or 7pm.  Get the time value for troubleshooting as needed
UPDATE a
SET 
	CompleteByDateTime = CASE 
						WHEN CHARINDEX('AM',pin.Scheduled_Time) > 0 THEN  CONVERT(VARCHAR(10),pin.Scheduled_date,101) +   ' 14:00' 
						WHEN CHARINDEX('PM',pin.Scheduled_Time) > 0 THEN   CONVERT(VARCHAR(10),pin.Scheduled_date,101) +   ' 19:00' 
						ELSE NULL 
					END,
	CompleteByTime = pin.SCHEDULED_TIME
FROM dbo.permit_inspections AS pin
INNER JOIN #Audit a
	ON a.PRIMARY_KEY_VALUE = pin.RECORDID





 

		SELECT
			'Building Plan Review' AS ReportName,
			'80% of New 1-2 Family home plan reviews completed within 30 days' AS MetricName,
			'Total Plan Reviews Within Measure' AS SubMetricName,
			 SUM(CASE WHEN DATEDIFF(DAY,date_sent, DATE_RECEIVED) <= 30 THEN 1 ELSE 0 END) AS DataPoint,
			1 AS OB,
			1 AS GroupOrder
		FROM dbo.permit_reviews
		INNER JOIN dbo.PERMIT_MAIN
			ON DBO.PERMIT_REVIEWS.PERMIT_NO = DBO.PERMIT_MAIN.PERMIT_NO
		WHERE
			 permit_reviews.reviewtype = 'BUILDING PERMIT'
		AND (PERMIT_MAIN.PermitType = 'NEW SINGLE FAMILY' )
		AND permit_reviews.DATE_RECEIVED >= @FromDate
		AND permit_reviews.DATE_RECEIVED < dateadd(DAY,1,@ToDate)

		UNION ALL

		SELECT
			'Building Plan Review' AS ReportName,
			'80% of New 1-2 Family home plan reviews completed within 30 days' AS MetricName,
			'Total plan reviews completed' AS SubMetricName,
			 SUM(CASE WHEN 	DATE_RECEIVED IS NOT NULL THEN 1 ELSE 0 END)  AS DataPoint,
			2 AS OB,
			1 AS GroupOrder
		FROM dbo.permit_reviews
		INNER JOIN dbo.PERMIT_MAIN
			ON DBO.PERMIT_REVIEWS.PERMIT_NO = DBO.PERMIT_MAIN.PERMIT_NO
		WHERE
			 permit_reviews.reviewtype = 'BUILDING PERMIT'
		AND (PERMIT_MAIN.PermitType = 'NEW SINGLE FAMILY' )
		AND permit_reviews.DATE_RECEIVED >= @FromDate
		AND permit_reviews.DATE_RECEIVED < dateadd(DAY,1,@ToDate)

		UNION ALL

		SELECT 
			'Building Plan Review' AS ReportName,
			'90% of remodeling plan reviews for projects under $10,000 completed within 1 business day' AS MetricName,
			'Total Plan Reviews Within Measure' AS SubMetricName,		
			SUM(CASE WHEN DATEDIFF(DAY, Applied, Issued) <= 1 THEN 1 ELSE 0 END) AS DataPoint ,
			3 AS OB,
			1 AS GroupOrder
		FROM dbo.PERMIT_MAIN 
		WHERE   DBO.PERMIT_MAIN.PermitType IN ('RESIDENTIAL')
			AND PERMIT_MAIN.Applied >= @FromDate
			AND PERMIT_MAIN.Applied < dateadd(DAY,1,@ToDate)
			AND PERMIT_MAIN.JOBVALUE <= 10000.00
	
		UNION ALL

		SELECT 
			'Building Plan Review' AS ReportName,
			'90% of remodeling plan reviews for projects under $10,000 completed within 1 business day' AS MetricName,
			'Total Plan reviews completed' AS SubMetricName,		
			COUNT(1) AS DataPoint ,
			4 AS OB,
			1 AS GroupOrder
		FROM dbo.PERMIT_MAIN 
		WHERE   DBO.PERMIT_MAIN.PermitType IN ('RESIDENTIAL')
			AND PERMIT_MAIN.Applied >= @FromDate
			AND PERMIT_MAIN.Applied < dateadd(DAY,1,@ToDate)
			AND PERMIT_MAIN.JOBVALUE <= 10000.00

		UNION ALL

		SELECT 
			'Building Plan Review' AS ReportName,
			'1-2 Family Home plan reviews provided' AS MetricName,
			'Total 1-2 Family Plan Reviews' AS SubMetricName,		
			 SUM(CASE WHEN 	DATE_RECEIVED IS NOT NULL THEN 1 ELSE 0 END)  AS DataPoint,
			5 AS OB,
			1 AS GroupOrder
		FROM dbo.permit_reviews
		INNER JOIN dbo.PERMIT_MAIN
			ON DBO.PERMIT_REVIEWS.PERMIT_NO = DBO.PERMIT_MAIN.PERMIT_NO
		WHERE
			 permit_reviews.reviewtype = 'BUILDING PERMIT'
		AND (PERMIT_MAIN.PermitType = 'NEW SINGLE FAMILY' )
		AND permit_reviews.DATE_RECEIVED >= @FromDate
		AND permit_reviews.DATE_RECEIVED < dateadd(DAY,1,@ToDate)

		UNION ALL

		SELECT 
			'Building Plan Review' AS ReportName,
			'Average 30 total days from initial application until permit issued (new single family)' AS MetricName,
			'Average days plan review completed' AS SubMetricName,		
			AVG(DATEDIFF(DAY, APPLIED, ISSUED)) AS DataPoint,
			6 AS OB,
			1 AS GroupOrder
		FROM dbo.PERMIT_MAIN
		WHERE 
			 ISSUED >= @FromDate
		AND ISSUED < dateadd(DAY,1,@ToDate)
		AND (PermitSubType IN ('NEW DUPLEX', 'NEW SF ATTACHED', 'NEW SF DETACHED'))

		UNION ALL

		SELECT 
			'Building Plan Review' AS ReportName,
			'Average 30 days from initial application until permit issued (new commercial)' AS MetricName,
			'Average days plan review completed' AS SubMetricName,		
			AVG(DATEDIFF(DAY, APPLIED, ISSUED)) AS DAYS_TO_COMPLETED,
			7 AS OB,
			1 AS GroupOrder
		FROM dbo.PERMIT_MAIN
		WHERE 
			 ISSUED >= @FromDate
		AND ISSUED < dateadd(DAY,1,@ToDate)
		AND (PermitType IN ('COMMERCIAL'))

 

UNION ALL

		SELECT 
			'Permitting And Contractor Licenses' AS ReportName,
			'The City’s permit status records are updated within 2 hours of the inspection 95% of the time' AS MetricName,
			'Total Inspections Completed' AS SubMetricName,		
			COUNT(1)  AS DataPoint,
			1 AS OB,
			2 AS GroupOrder
		FROM #Audit a 

		UNION ALL
		
		SELECT 
			'Permitting And Contractor Licenses' AS ReportName,
			'The City’s permit status records are updated within 2 hours of the inspection 95% of the time' AS MetricName,
			'Total Inspections within Measure' AS SubMetricName,		
			SUM(CASE WHEN  a.CompletedDateTime <= a.CompleteByDateTime  THEN 1 ELSE 0 END) AS DataPoint,
			2 AS OB,
			2 AS GroupOrder
		FROM #Audit a 

		UNION ALL
		
		SELECT 
			'Permitting And Contractor Licenses' AS ReportName,
			'Number of Building Permits issued' AS MetricName,
			'Total Building Permits Issued' AS SubMetricName,		
			COUNT(1) AS DataPoint,
			3 AS OB,
			2 AS GroupOrder
		 FROM dbo.PERMIT_MAIN
		WHERE 
				ISSUED >= @FromDate
		AND ISSUED < dateadd(DAY,1,@ToDate)
		AND permittype not in ('RIGHT OF WAY', 'rowyard', 'TEMP,REVOCABLE','SPEC EVENT')

		UNION ALL
		
		SELECT 
			'Permitting And Contractor Licenses' AS ReportName,
			'Contractor licenses issued' AS MetricName,
			'Total Contractor Licenses Issued' AS SubMetricName,		
			COUNT(1) AS DataPoint,
			4 AS OB,
			2 AS GroupOrder
		FROM dbo.aec_main
		WHERE 
			ST_LIC_EXPIRE >= DATEADD(YEAR,1,@FromDate)
		AND ST_LIC_EXPIRE < DATEADD(YEAR,1,DATEADD(DAY,1,@ToDate))
		and aectype != 'ROW CONTRACTOR'

 
	UNION ALL
 

		SELECT 
			'Inspections' AS ReportName,
			'98% of inspection conducted on the day scheduled' AS MetricName,
			'Total number of Inspections in Measure' AS SubMetricName,		
			COUNT(1) AS DataPoint,
			1 AS OB,
			3 AS GroupOrder
		FROM dbo.Permit_Inspections
		WHERE 
			COMPLETED_DATE >= @FromDate
		AND COMPLETED_DATE < dateadd(DAY,1,@ToDate) 
		AND INSPECTOR IN ('CARS', 'AQ', 'TA', 'NR', 'KN', 'DR', 'DC', 'DMCP','CCI', 'RN', 'AS', 'D1', 'c1', 't1', 'ccie', 'dstr', 'bvh', 'jj')
		AND DateDiff(DAY,SCHEDULED_DATE, COMPLETED_DATE) = 0

		UNION ALL
		
		SELECT 
			'Inspections' AS ReportName,
			'98% of inspection conducted on the day scheduled'AS MetricName,
			'Total number of Inspections completed' AS SubMetricName,		
			COUNT(1) AS DataPoint,
			2 AS OB,
			3 AS GroupOrder
		FROM dbo.Permit_Inspections
		WHERE 
			COMPLETED_DATE >= @FromDate
		AND COMPLETED_DATE < dateadd(DAY,1,@ToDate) 
		AND INSPECTOR IN ('CARS', 'AQ', 'TA', 'NR', 'KN', 'DR', 'DC', 'DMCP','CCI', 'RN', 'AS', 'D1', 'c1', 't1', 'ccie', 'dstr', 'bvh', 'jj')

		ORDER BY 
			GroupOrder,
			OB
			 

 


END -- PROC END

 
GO
