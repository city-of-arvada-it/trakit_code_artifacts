USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_SysSQLSearch]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_SysSQLSearch]
GO
/****** Object:  StoredProcedure [dbo].[tsp_SysSQLSearch]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  					  
					  					  					  

/*
tsp_SysSQLSearch 'site_city'

*/



Create Procedure [dbo].[tsp_SysSQLSearch](
@SearchText Varchar(1000)
)

As

Declare @Search as varchar(1000)
Set @Search =  '%' + @SearchText + '%'
  
SELECT Name, 'StProc' as ObjType
FROM sys.procedures
WHERE OBJECT_DEFINITION(OBJECT_ID) LIKE @Search
union  SELECT Name, 'Trigger' as ObjType
FROM sys.triggers
WHERE OBJECT_DEFINITION(OBJECT_ID) LIKE @Search
union  select name, 'Function' as ObjType  FROM sys.objects
WHERE OBJECT_DEFINITION(OBJECT_ID) LIKE @Search and type ='FN'
union   select name, 'View' as ObjType  FROM sys.objects
WHERE OBJECT_DEFINITION(OBJECT_ID) LIKE @Search and type ='V'












GO
