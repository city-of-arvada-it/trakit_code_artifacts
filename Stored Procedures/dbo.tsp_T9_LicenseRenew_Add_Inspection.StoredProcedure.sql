USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_LicenseRenew_Add_Inspection]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_LicenseRenew_Add_Inspection]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_LicenseRenew_Add_Inspection]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  -- =============================================
-- Author:		<Micah Neveu>
-- Create date: <3/5/2015>
-- Description:	<Helper to create an inspection>
-- =============================================
CREATE PROCEDURE [dbo].[tsp_T9_LicenseRenew_Add_Inspection] 
	@RECORDPREFIX AS varchar(6),
	@INSPECTION AS varchar(255),
	@CREATED_BY AS varchar(6)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Inspector varchar(60)
	DECLARE @DefaultDuration int = 0
	DECLARE @DefaultDate int = 2 -- where is this in the DB??
	DECLARE @USERID AS varchar(6)

	-- get the default inspector for the inspector type and duration, and convert to USERID
	SELECT TOP(1) @Inspector = field05, @DefaultDuration = field08 FROM Prmry_InspectionControl WHERE Category = 'INSPECTION_TYPE' AND Field01 = @INSPECTION

	SET @USERID = (select userid from PRMRY_USERS where UserName = SUBSTRING(@Inspector, 0, CHARINDEX('(' ,@Inspector) - 1))

	INSERT INTO LICENSE2_INSPECTIONS([recordid],INSPECTOR,LICENSE_NO,InspectionType, CREATED_BY, CREATED_DATE, DURATION, SCHEDULED_DATE) 
		SELECT @RECORDPREFIX + right(('00' + str(DATEPART(yy, GETDATE()))),2) + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2) + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2) + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2) + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2) + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2) + right(('000000'+ CONVERT(varchar,ROW_NUMBER() OVER (ORDER BY (SELECT NULL))+111)),6) 
		AS recid,
		@USERID,
		tbl.*, 
		GETDATE() AS CREATED_DATE,
		@DefaultDuration AS DURATION,
		DATEADD(d,@DefaultDate,GETDATE()) AS SCHEDULED_DATE
		FROM (SELECT license_no, @INSPECTION AS InspectionType, @CREATED_BY AS CREATED_BY FROM LicenseRenew_License_List) AS tbl
	END



GO
