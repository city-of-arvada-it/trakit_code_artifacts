USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_CLR_UpdateLatLon]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_CLR_UpdateLatLon]
GO
/****** Object:  StoredProcedure [dbo].[tsp_CLR_UpdateLatLon]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  
create procedure [dbo].[tsp_CLR_UpdateLatLon]
      @Type varchar(8),
      @LayerURL varchar(5000) = null,
      @GeometryURL varchar(5000) = null,
      @TrakitField varchar(50) = null,
      @LayerField varchar(50) = null
as

BEGIN

if exists (select * from sys.objects where object_id = object_id(N'[dbo].[zResults]'))
      drop table [dbo].[zResults]
      
if exists (select * from sys.objects where object_id = object_id(N'[dbo].[zResults2]'))
      drop table [dbo].[zResults2]

if not exists(select * from sys.objects where name = N'CheckedLatLon')
      begin
            create table CheckedLatLon(
            [loc_recordid] varchar(30),
            [checked] [bit] default 0 not null,
            [date_checked] datetime,
            constraint [PK_CheckedLatLon] primary key clustered
                  (
                        [loc_recordid] ASC
                  )
            )
      end

create table zResults2 (recid2 varchar(30),lat float,lon float)

--Reset Checked to 0 in CheckedLatLon when Date_Checked older than 90 days
update CheckedLatLon set checked = 0, date_checked = NULL
where datediff(day, date_checked, GETDATE()) >7 ;

--Run Loop to Update Results2 table
declare @recordid varchar(30)
declare @tvalue varchar(1000)
declare @tval varchar(1000)
declare @ctr int = 0
declare @id varchar(1000)
create table zResults (recid varchar(30),trakitvalue varchar(1000),lat float,lon float,checked bit)
if(@Type = 'esri')
      begin
            declare @sql varchar(5000)
            set @sql = 'insert into zResults 
            (recid,trakitvalue,lat,lon,checked)
            select recordid,' + @TrakitField + ' as trakitvalue,lat,lon,      
            checked from geo_ownership left outer join CheckedLatLon on 
            geo_ownership.recordid = CheckedLatLon.loc_recordid 
            where lat is null and lon is null and 
            (CheckedLatLon.checked = 0 or CheckedLatLon.checked is null)
            declare record_cursor CURSOR FOR
            select recid,trakitvalue from zResults'
            execute(@sql);
      end
else if(@Type = 'mapquest')
      begin
            insert into zResults (recid,trakitvalue,lat,lon,checked)
            select recordid,site_addr + ',' + site_city + ',' + site_state + 
            ',' + site_zip as trakitvalue,lat,lon,          
            checked from geo_ownership left outer join CheckedLatLon on 
            geo_ownership.recordid = CheckedLatLon.loc_recordid 
            where lat is null and lon is null and (site_addr is 
            not null or site_city is not null or 
            site_state is not null or site_zip is not null) and 
            (CheckedLatLon.checked = 0 or CheckedLatLon.checked is null)
            declare record_cursor CURSOR FOR
            select top 5000 recid,trakitvalue from zResults
      end
else
      begin
            return
      end

open record_cursor;
fetch next from record_cursor into
@recordid,@tvalue;
while @@fetch_status = 0
      begin
            set @tval = '''' + ltrim(rtrim(@tvalue)) + ''''
            if(@Type = 'esri')
                  begin
                        declare @sql2 varchar(5000)
                        set @sql2 = 'insert into zResults2(lat,lon)
                        exec dbo.tsp_CLR_GetESRIxy ''' + @LayerURL +''',''' + 
                        @GeometryURL + ''',''' + @LayerField + ''',''''' + 
                        @tval + ''''''
                        exec(@sql2);
                  end
            else
                  begin
                        insert into zResults2
                        (lat,lon)
                        exec dbo.tsp_CLR_GetMapQuestxy @tval
                  end
            if(select count(*) from zResults2 where recid2 is null) = 0
                  begin
                        print 'No xy coordinates found for record ' + @tval
                  end
            else
                  begin
                        update zResults2 set recid2 = @recordid
                        where recid2 is null
                        print 'Found xy coordinates for record ' + @tval
                  end
            if(select count(*) from CheckedLatLon where loc_recordid = 
            @recordid) = 0
                  begin
                        insert into CheckedLatLon(loc_recordid,checked) 
                        values(@recordid,1)
                  end
            --if(select checked from CheckedLatLon where loc_recordid = @recordid) = 0
            else
                  begin
                        update CheckedLatLon
                        set checked = 1, date_checked = GETDATE() where loc_recordid = @recordid
                  end
            set @ctr = @ctr + 1
          print @ctr
            fetch next from record_cursor into
            @recordid,@tvalue;
      end;
close record_cursor;
deallocate record_cursor;

if(select count(*) from zResults2) >=1
      Begin
            update dbo.geo_ownership set dbo.geo_ownership.lat = zResults2.lat,dbo.geo_ownership.lon = zResults2.lon 
            from zResults2 where dbo.geo_ownership.recordid = zResults2.recid2
      End
else
      Begin
            print 'NO New LAT LON Records'
      End
END

 



GO
