USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AddSubActivityCondition]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_AddSubActivityCondition]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AddSubActivityCondition]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  					  



CREATE PROCEDURE [dbo].[tsp_T9_AddSubActivityCondition] 
      @ParentGroup varchar(8) = Null
      ,@SubGroup varchar(8) = Null
      ,@ParentConditionID varchar(30) = null
      ,@ActivityNo varchar(15) = null
      ,@UserID varchar(4) = null
      
AS

BEGIN
      -- SET NOCOUNT ON added to prevent extra result sets from
      -- interfering with SELECT statements.
      SET NOCOUNT ON;
      
      declare @subconditionstable varchar(25)
      declare @parentconditionstable varchar(25)
      declare @activitynofield varchar(20)
      declare @SQL varchar(2000)
      declare @SQLExecute varchar(1000)
      declare @recid varchar(30)
      declare @recidcount int
      
      select @recid = dbo.tfn_RecordidRowNum()
	  select @recidcount = count(recordid)from project_conditions2 where recordid = @recid 
	  while @recidcount > 0
	  begin
		select @recid = dbo.tfn_RecordidRowNum()
		select @recidcount = count(recordid)from project_conditions2 where recordid = @recid
	  end
      
      IF @SubGroup = 'PERMIT'
      BEGIN
		set @subconditionstable = 'Permit_Conditions2'
		set @activitynofield = 'PERMIT_NO'
      END
      IF @SubGroup = 'PROJECT'
      BEGIN
		set @subconditionstable = 'Project_Conditions2'
		set @activitynofield = 'PROJECT_NO'
      END
      IF @ParentGroup = 'PROJECT'
      BEGIN
		set @parentconditionstable = 'Project_Conditions2'
      END
      IF @ParentGroup = 'PERMIT'
      BEGIN
		set @parentconditionstable = 'Permit_Conditions2'
      END
      --PRINT 'table ' + @conditionstable
      set @SQL = 'insert into ' + @subconditionstable + ' (CONDITION_NOTES,CONDITION_TYPE,CONTACT,DATE_ADDED,DATE_REQUIRED,DATE_SATISFIED,DEPARTMENT,LOCATION,' + @activitynofield + ',RECORDID,REMARKS,SEQ_NO,[STATUS],USERID,PARENTGROUP,PARENTID)
select
ISNULL(CONDITION_NOTES,'''') , 
CASE WHEN CONDITION_TYPE IS NOT NULL THEN CONDITION_TYPE END ,
CASE WHEN CONTACT IS NOT NULL THEN CONTACT END , 
CASE WHEN DATE_ADDED IS NOT NULL THEN CONVERT(varchar(25),DATE_ADDED,121) END, 
CASE WHEN DATE_REQUIRED IS NOT NULL THEN CONVERT(varchar(25),DATE_REQUIRED,121) END, 
CASE WHEN DATE_SATISFIED IS NOT NULL THEN CONVERT(varchar(25),DATE_SATISFIED,121) END, 
CASE WHEN DEPARTMENT IS NOT NULL THEN DEPARTMENT END,
CASE WHEN LOCATION IS NOT NULL THEN LOCATION END,
''' + @ActivityNo + ''',
''' + @recid + ''',
CASE WHEN REMARKS IS NOT NULL THEN REMARKS END, 
CONVERT(varchar, SEQ_NO) , 
CASE WHEN [STATUS] IS NOT NULL THEN [STATUS] END,
''' + @UserID + ''',
''' + @ParentGroup + ''',
''' + @ParentConditionID + ''' 
from ' + @parentconditionstable + ' where recordid = ''' + @ParentConditionID + ''''
	  print @SQL
	  EXEC(@SQL)
            
END








GO
