USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ProjectStatusLockRule]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_ProjectStatusLockRule]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ProjectStatusLockRule]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  

					  

-- =====================================================================
-- Revision History:
--	RB - 9/25/2014 - Added to Source Safe

-- =====================================================================   	
CREATE Procedure [dbo].[tsp_T9_ProjectStatusLockRule](     
	   @ConditionRecordID			varchar(25),
	   @ProcedureAction				varchar(25),
	   @StatusLock					bit, 
	   @ActivityNo                  Varchar(25),
	   @TypeName					Varchar(60)

)

As
 BEGIN

-- TEST ONLY
--Declare 
--	   @ConditionRecordID			varchar(25) = 'CRW:1404080910510007',
--	   @ProcedureAction				varchar(25) = 'Test',
--	   @StatusLock					Varchar(5),
--       @ActivityNo                  Varchar(25) = 'CRW-PROJECT'


 Declare @SQLStatement     varchar(500)	
 Declare @ConditionCount   int = 0

  
 if @ProcedureAction is not null and @ProcedureAction <> ''
    Begin    

		Set @ProcedureAction = UPPER(@ProcedureAction)
		
		if @ProcedureAction = 'SELECT' and (@ConditionRecordID is not null and @ConditionRecordID <> '')
		   Begin
		        Set @SQLStatement = 'Select STATUS_LOCK from project_conditions2 where recordid = ' + Char(39) + @ConditionRecordID + CHAR(39)
		   End
		Else if @ProcedureAction = 'UPDATE' and (@ConditionRecordID is not null and @ConditionRecordID <> '')
		   Begin				
				if @StatusLock = 1
				  Begin
					Set @SQLStatement = 'Update project_conditions2 set STATUS_LOCK = 1 where recordid = ' + Char(39) + @ConditionRecordID + CHAR(39)
				  End
				Else
				  Begin
					Set @SQLStatement = 'Update project_conditions2 set STATUS_LOCK = 0 where recordid = ' + Char(39) + @ConditionRecordID + CHAR(39)
				  End
		   End
		 Else if @ProcedureAction = 'TESTRULE' and (@ActivityNo is not null and @ActivityNo <> '') and (@TypeName  is not null and @TypeName <> '')
		   Begin	
		   
				Set @ConditionCount = (Select count(*) from Project_Conditions2 where project_no = @ActivityNo and (status_lock = 1 and isdate(date_satisfied)= 0))	   					
		
				if @ConditionCount > 0
				   Begin
						Set @SQLStatement = 'select Status as StatusName from prmry_status where GroupName = ' + Char(39) + 'PROJECTS' + Char(39) + 
				                    ' and (project_status_lock = 1)' + 
				                    ' and Status in(select value1 from Prmry_TypesInfo where GroupName = ' + Char(39) + 'PROJECTS' + Char(39) + 
				                    ' and Category = ' + Char(39) + 'STATUS' + Char(39) + 	
				                    ' and Typename = ' + Char(39) + @TypeName  + Char(39) + ')'					   
				   End							 
		   End
    End
 
 if @SQLStatement <> '' 
   Begin
		EXEC(@SQLStatement)
		--print @SQLStatement
   End
		
 END




GO
