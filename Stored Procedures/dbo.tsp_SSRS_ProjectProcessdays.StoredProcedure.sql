USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_SSRS_ProjectProcessdays]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_SSRS_ProjectProcessdays]
GO
/****** Object:  StoredProcedure [dbo].[tsp_SSRS_ProjectProcessdays]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  					  
					  					  					  
-- =============================================
-- Author:	Dan Haynes
-- Create date: 1/31/2012
-- Description:	Process Data for Project Details Report
-- =============================================
CREATE PROCEDURE [dbo].[tsp_SSRS_ProjectProcessdays] 
		@PROJECT_NO varchar(15)
AS
BEGIN

--Create Temp Table


--RB Check if table exists before creating it
   IF OBJECT_ID('tempdb..#table1') IS NOT NULL  
	drop table #table1

create table #table1
	(GROUPNAME varchar(30)
	,SORT_ORDER int
	,APPLIED datetime
	,APPROVED datetime
	,CLOSED datetime
	,EXPIRED datetime
	,[DAYS] int
	,TOTAL int
	,OPENED int
	,COMPLETED int);

--Add Groups
insert into #table1 (GROUPNAME, SORT_ORDER) values('DAYS', '1');
insert into #table1 (GROUPNAME, SORT_ORDER) values('REVIEWS', '2');
insert into #table1 (GROUPNAME, SORT_ORDER) values('ACTIONS', '3');
insert into #table1 (GROUPNAME, SORT_ORDER) values('INSPECTIONS', '4');
insert into #table1 (GROUPNAME, SORT_ORDER) values('CONDITIONS', '5');

--Get STARTED, CLOSED Dates
update #table1 set APPLIED = (select APPLIED from Project_Main where PROJECT_NO = @PROJECT_NO and GROUPNAME = 'DAYS') ;
update #table1 set APPROVED = (select APPROVED from Project_Main where PROJECT_NO = @PROJECT_NO and GROUPNAME = 'DAYS') ;
update #table1 set CLOSED = (select CLOSED from Project_Main where PROJECT_NO = @PROJECT_NO and GROUPNAME = 'DAYS') ;
update #table1 set EXPIRED = (select EXPIRED from Project_Main where PROJECT_NO = @PROJECT_NO and GROUPNAME = 'DAYS') ;
	
--Get TOTALDAYS
update #table1 set [DAYS] = CASE
	WHEN CLOSED is NULL THEN CONVERT(bigint,CONVERT(datetime,GETDATE()))- CONVERT(bigint,CONVERT(datetime,APPLIED))
	WHEN CLOSED is NULL and EXPIRED is not NULL THEN CONVERT(bigint,CONVERT(datetime,EXPIRED))- CONVERT(bigint,CONVERT(datetime,APPLIED))
	WHEN CLOSED is not NULL THEN CONVERT(bigint,CONVERT(datetime,CLOSED))- CONVERT(bigint,CONVERT(datetime,APPLIED)) END 
	WHERE GROUPNAME = 'DAYS';

--Get Number of Valid Reviews-------------------------------------------------------------------------------
update #table1 set TOTAL = (select COUNT(PROJECT_NO) from Project_Reviews where PROJECT_NO = @PROJECT_NO
	and (REMARKS not like '%VOIDED%' or REMARKS is NULL or REMARKS = '')) 
	where GROUPNAME = 'REVIEWS' ;
	
--Get Number of Opened Valid Reviews	
update #table1 set OPENED = (select COUNT(PROJECT_NO) from Project_Reviews where PROJECT_NO = @PROJECT_NO 
	and (REMARKS not like '%VOIDED%' or REMARKS is NULL or REMARKS = '')
	and DATE_RECEIVED is NULL)
	where GROUPNAME = 'REVIEWS' ;

--Get Number of Completed Valid Reviews	
update #table1 set COMPLETED = (select COUNT(PROJECT_NO) from Project_Reviews where PROJECT_NO = @PROJECT_NO 
	and (REMARKS not like '%VOIDED%' or REMARKS is NULL or REMARKS = '')
	and DATE_RECEIVED is not NULL)
	where GROUPNAME = 'REVIEWS' ;
	
--Get Number of Valid Actions-------------------------------------------------------------------------------
update #table1 set TOTAL = (select COUNT(PROJECT_NO) from Project_Actions where PROJECT_NO = @PROJECT_NO
	and (ACTION_DESCRIPTION not like '%VOIDED%' or ACTION_DESCRIPTION is NULL or ACTION_DESCRIPTION = '')) 
	where GROUPNAME = 'ACTIONS' ;
	
--Get Number of Opened Valid Actions	
update #table1 set OPENED = (select COUNT(PROJECT_NO) from Project_Actions where PROJECT_NO = @PROJECT_NO 
	and (ACTION_DESCRIPTION not like '%VOIDED%' or ACTION_DESCRIPTION is NULL or ACTION_DESCRIPTION = '')
	and COMPLETED_DATE is NULL)
	where GROUPNAME = 'ACTIONS' ;

--Get Number of Completed Valid Actions	
update #table1 set COMPLETED = (select COUNT(PROJECT_NO) from Project_Actions where PROJECT_NO = @PROJECT_NO 
	and (ACTION_DESCRIPTION not like '%VOIDED%' or ACTION_DESCRIPTION is NULL or ACTION_DESCRIPTION = '')
	and COMPLETED_DATE is not NULL)
	where GROUPNAME = 'ACTIONS' ;
	
--Get Number of Valid Inspections---------------------------------------------------------------------------
update #table1 set TOTAL = (select COUNT(PROJECT_NO) from Project_Inspections where PROJECT_NO = @PROJECT_NO 
	and (REMARKS not like '%VOIDED%' or REMARKS is NULL or REMARKS = ''))
	where GROUPNAME = 'INSPECTIONS' ;
	
--Get Number of Open Valid Inspections
update #table1 set OPENED = (select COUNT(PROJECT_NO) from Project_Inspections where PROJECT_NO = @PROJECT_NO 
	and (REMARKS not like '%VOIDED%' or REMARKS is NULL or REMARKS = '')
	and COMPLETED_DATE is NULL) 
	where GROUPNAME = 'INSPECTIONS' ;

--Get Number of Completed Valid Inspections	
update #table1 set COMPLETED = (select COUNT(PROJECT_NO) from Project_Inspections where PROJECT_NO = @PROJECT_NO 
	and (REMARKS not like '%VOIDED%' or REMARKS is NULL or REMARKS = '')
	and COMPLETED_DATE is not NULL) 
	where GROUPNAME = 'INSPECTIONS' ;

--Get Number of Valid Conditions----------------------------------------------------------------------------
update #table1 set TOTAL = (select COUNT(PROJECT_NO) from Project_Conditions2 where PROJECT_NO = @PROJECT_NO 
	and (CONDITION_NOTES not like '%VOIDED%' or CONDITION_NOTES is NULL or CONDITION_NOTES = ''))
	where GROUPNAME = 'CONDITIONS' ;
	
--Get Number of Opened Valid Violations
update #table1 set OPENED = (select COUNT(PROJECT_NO) from Project_Conditions2 where PROJECT_NO = @PROJECT_NO 
	and (CONDITION_NOTES not like '%VOIDED%' or CONDITION_NOTES is NULL or CONDITION_NOTES = '')
	and DATE_SATISFIED is NULL) 
	where GROUPNAME = 'CONDITIONS' ;

--Get Number of Completed Valid Violations	
update #table1 set COMPLETED = (select COUNT(PROJECT_NO) from Project_Conditions2 where PROJECT_NO = @PROJECT_NO 
	and (CONDITION_NOTES not like '%VOIDED%' or CONDITION_NOTES is NULL or CONDITION_NOTES = '')
	and DATE_SATISFIED is not NULL) 
	where GROUPNAME = 'CONDITIONS' ;

--Clean Up Numbers-------------------------------------------------------------------------------------------
update #table1 set TOTAL = 0 where TOTAL < 0 or TOTAL is NULL;
update #table1 set OPENED = 0 where OPENED < 0 or OPENED is NULL;
update #table1 set COMPLETED = 0 where COMPLETED < 0 or COMPLETED is NULL;

--Send Data to Report----------------------------------------------------------------------------------------
select * from #table1 order by SORT_ORDER;

END











GO
