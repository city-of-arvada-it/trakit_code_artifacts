USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[usp_arvada_TrackingBoardCommissions]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[usp_arvada_TrackingBoardCommissions]
GO
/****** Object:  StoredProcedure [dbo].[usp_arvada_TrackingBoardCommissions]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_arvada_TrackingBoardCommissions]

	@Grouping varchar(150) = null

as

/*
exec [usp_arvada_TrackingBoardCommissions] @Grouping = 'Planning Commission Hearing'

Can you please create a new report titled Tracking Boards and Commissions. 
I have attached an example for the data that I need and how the report should look. 
There is another report Arvada Development Tracking Report that is the same data that you can added to, 
I need to keep that report existing and that I am trying to get into this report but I need to add more information to it and keep the Development Tracking Report as it is today.
 I need to have the data group by the Planning Commission Hearing date which is an action, 
 also I want to have the cases come off the report once all of the actions have a completed date. 
 And I want to show anything that is June 2019 forward.

This report should have a parameter that allows Josie to choose whether to look at Planning Commission Hearing Date (Planning Commission Hearing) 
	or by Board of Adjustments (BOA Hearing).

*/

create table #actionType
(
	ActionType varchar(60)
)


create table #final
(
	ActionType varchar(150),
	ActionDate datetime,
	Project_No varchar(max),
	ApplicationName nvarchar(max),
	PLANNER nvarchar(500),
	StatusValue varchar(50),
	Project_Name nvarchar(max),  
	CompletedDate datetime,
	orderby int,
	ReportActionDate datetime,
	ReportActionDateCount int,
	ActionBy nvarchar(500)
)

create table #actionMap
(
	ActionDateType varchar(100),
	ActionType varchar(max)
)

--need a mapping of this action type, we want to see these actions listed on the report:
insert into #actionMap
(
	ActionDateType,
	ActionType
)
select
	'Planning Commission Hearing',
	'Applicant pick up signs|CC annex res acceptance|CC PH Title due|Call applicant re PC signs|City Council Public Hearing|CC PH Packet due|Post PC Meeting Notice|PC Meeting Minutes Due|Prepare CC signs|Submit PC Publication|Write PC Staff Report|Prepare final PC packet|Planning Commission Hearing|Prepare PC written notice|Prepare PC Signs|Written notice by applicant|PC Packet to Admin|PC Presentation to KATV|PC Staff Report to Manager|PC Packet to City Attorney|PC Packet delivery|PC Posting Log/Mail aff submtd|'


declare @actionTypes varchar(max)

select @actionTypes = actionType
from #actionMap
where ActionDateType = @Grouping

insert into #actionType
(
	ActionType
)
select distinct
	val
from [dbo].[tfn_Arvada_SSRS_String_To_Table](@actionTypes,'|',1)
  
   

insert into #final
(
	Project_No,
	ActionType,
	ApplicationName,
	PLANNER,
	StatusValue,
	Project_Name, 
	ActionDate,
	CompletedDate ,
	ActionBy
)
SELECT
	m.PROJECT_NO,
	ac.ACTION_TYPE,
	p.NAME + char(10) + char(13) + p.PHONE as ApplicationName,
	m.PLANNER,
	m.STATUS,
	M.PROJECT_NAME, 
	ac.ACTION_DATE,
	ac.COMPLETED_DATE,
	ac.ACTION_BY
from dbo.project_main m
inner join project_actions as ac
	on ac.PROJECT_NO = m.PROJECT_NO 
inner join #actionType am
	on am.ActionType = ac.ACTION_TYPE
left join (select project_no, name, '('+substring(phone,1,3)+') '+substring(phone,4,3)+'-'+right(phone,4) as phone
	   from Project_People
	   where NAMETYPE = 'applicant') as p
	   on m.PROJECT_NO = p.project_no 
--WHERE ([STATUS] in ('COMPLETENESS REVIEW', '1ST REVIEW', '2ND REVIEW', '3RD REVIEW', 'DECISION REVIEW', 'CUSTOMER RESP. PEND', 'APPLIED', 'ON HOLD','PH PENDING', 'FULL CUSTOMER RESP','APPEAL RECVD'))
where
	exists
		(select 1
			from Project_Actions a2
			where a2.PROJECT_NO = m.PROJECT_NO
			and a2.ACTION_TYPE = am.ActionType
			and a2.COMPLETED_DATE is null
		) 
 and 	exists
		(select 1
			from Project_Actions a3
			where a3.PROJECT_NO = m.PROJECT_NO
			and a3.ACTION_TYPE = @Grouping
			and a3.COMPLETED_DATE is null
		) 
 and ac.ACTION_DESCRIPTION not like '%voided%'

  

  
--get the report action date, just easier to see it this way (for all rows it is showing the date we ultimately care about:
	-- planning commission date, BOA Hearing date...):
update f
set ReportActionDate = b.ActionDate
from #final f
inner join
	(select
			ActionDate,
			Project_No
	 from #final
	 where 
		ActionType = @Grouping 
	) as b
	on b.Project_No = f.Project_No




--the number of projects in that date group
--using the date above, how many projects have that same date?
update f
set ReportActionDateCount = b.CountValue
from #final f
inner join
	(select
			ReportActionDate,
			count(distinct project_no) as CountValue
	 from #final
	 group by 
		ReportActionDate
	) as b
	on b.ReportActionDate = f.ReportActionDate


--hold over from a prior report.  Removing this for now...
--get the max date for cc pub hearing
----apparently there can be a few :)
--update f
--set ActionDate = b.ActionDate
--from #final f
--inner join	
--	(select
--		project_no,
--		ActionDate,
--		row_number() over (order by ActionDate desc, project_no) as rid
--	 from #final f
--	 where ACtionType = 'city council public hearing'
--	 ) as b
--	 on b.project_no = f.project_no
--where
--	b.rid = 1

	 
 

--if the project has no dates in any of the actions we specified, just remove it
delete f
from #final f
where not exists(select 1 from #final f2 where f2.Project_No = f.Project_No and f2.actiondate is not null)

----remove projects where the max cc project date is in the past
delete f
from #final f
where exists(select 1 from #final f2 where f2.Project_No = f.Project_No and f2.actiondate < convert(Date, getdate()) and f2.ACtionType = 'city council public hearing' and f2.ActionDate is not null)
 
--remove older projects with strange data
delete f
from #final f
where ReportActionDate < '06/01/2019'
	

--set their order by across the projects using the date of the action type the report is being asked to use:
update f
set OrderBy = b.rid
from #final f
inner join	
	(select
		project_no,
		row_number() over (order by ActionDate desc, project_no) as rid
	 from #final f
	 where ActionType = @Grouping
	 ) as b
	 on b.project_no = f.project_no


select
	@Grouping as GroupName,
	ActionType ,
	ActionDate ,
	CompletedDate,
	ActionBy,
	Project_No ,
	ApplicationName ,
	PLANNER,
	StatusValue,
	Project_Name,
	ReportActionDate,
	ReportActionDateCount ,
	OrderBy as RowOrderBy, 
		case 
			ltrim(rtrim(ActionType)) 
				when 'CC annex res acceptance' then 'Accept Annexation Petition'
				when 'Submit PC Publication' then 'Publication to Clerks Ofc.'
				when 'Post PC Meeting Notice' then 'Post Sign for Planning Commission'
				when 'PC Staff Report to Manager' then 'Staff Report to Planning Manager'
				when 'Planning Commission Hearing' then 'Planning Commission Meeting'
				when 'Post CC Meeting Notice' then 'Post Sign for City Council'
				when 'Legal Description Due' then 'Legal Description Due'
				when 'CC PH Title due' then 'Civic Clerk Coversheets'
				when 'cc ph packet due' then 'Council Packet Due'
				when 'First Reading' then 'First Reading'
				when 'City Council Public Hearing' then 'City Council Public Hearing'
				else null end as ActionTypeDescription,
		case 
			ltrim(rtrim(ActionType)) 
				when 'CC annex res acceptance' then 1
				when 'Submit PC Publication' then 2
				when 'Post PC Meeting Notice' then 3
				when 'PC Staff Report to Manager' then 4
				when 'Planning Commission Hearing' then 5
				when 'Prepare CC signs' then 6
				when 'Legal Description Due' then 7
				when 'CC PH Title due' then 8
				when 'cc ph packet due' then 9
				when 'First Reading' then 10
				when 'City Council Public Hearing' then 11
				else 5 end as columnorderby

from #final
order by 
	project_NO, 
	ReportActionDate,
	ActionDate asc

	 
GO
