USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[lp_sensitive_data_notify]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[lp_sensitive_data_notify]
GO
/****** Object:  StoredProcedure [dbo].[lp_sensitive_data_notify]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* 
August -19, 20-13 - Ychavez
Created to catch any sensitive data entered online in the notes, to prevent data such as lock box #s, garage codes etc.
--There is a job that will run daily that will execute this stored procedure
exec lp_sensitive_data_notify
*/
CREATE procedure [dbo].[lp_sensitive_data_notify]

as
begin

	create table  #tmp_hii(
	[NoteID] [int] NULL,
	[ActivityGroup] [varchar](50) NOT NULL,
	[ActivityNo] [varchar](30) NOT NULL,
	[ActivityRecordID] [varchar](30) NULL,
	[SubGroup] [varchar](50) NULL,
	[SubGroupRecordID] [varchar](30) NULL,
	[UserID] [varchar](6) NULL,
	[DateEntered] [datetime] NULL,
	[Notes] [varchar](7500) NULL,
	[attachmentRecID] [varchar](50) NULL,
	[markUpID] [varchar](50) NULL,
	[markupEXT] [varchar](250) NULL,
	[markupName] [varchar](50) NULL,
	[markupStatus] [int] NULL,
	tsannoID int null
)

----------------------------------------------------
--DROP TABLE THAT WILL HOLD DATA TO BE EMAILED--  select * from  LT_SENSITVE_DATA_HOLD
----------------------------------------------------
truncate table LT_SENSITVE_DATA_HOLD
--IF EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LT_SENSITVE_DATA_HOLD]') AND TYPE IN (N'U'))
--DROP TABLE [dbo].[LT_SENSITVE_DATA_HOLD]

----------------------------------------------------
--LOOK FOR DATA THAT IS NOT SUPPOSED TO BE ENTERED
-----------------------------------------------------

insert into #tmp_hii
(
	[NoteID] ,
	[ActivityGroup] ,
	[ActivityNo] ,
	[ActivityRecordID] ,
	[SubGroup] ,
	[SubGroupRecordID] ,
	[UserID] ,
	[DateEntered] ,
	[Notes] ,
	[attachmentRecID] ,
	[markUpID] ,
	[markupEXT] ,
	[markupName] ,
	[markupStatus] ,
	tsannoID
)
select 
	[NoteID] ,
	[ActivityGroup] ,
	[ActivityNo] ,
	[ActivityRecordID] ,
	[SubGroup] ,
	[SubGroupRecordID] ,
	[UserID] ,
	[DateEntered] ,
	[Notes] ,
	[attachmentRecID] ,
	[markUpID] ,
	[markupEXT] ,
	[markupName] ,
	[markupStatus] ,
	tsannoID
From dbo.Prmry_Notes 
where (Notes like '%lock box%' OR notes like '%lockbox%' )
and DateEntered > GETDATE() -1


insert into #tmp_hii
(
	[NoteID] ,
	[ActivityGroup] ,
	[ActivityNo] ,
	[ActivityRecordID] ,
	[SubGroup] ,
	[SubGroupRecordID] ,
	[UserID] ,
	[DateEntered] ,
	[Notes] ,
	[attachmentRecID] ,
	[markUpID] ,
	[markupEXT] ,
	[markupName] ,
	[markupStatus] ,
	tsannoID
)
select 
	[NoteID] ,
	[ActivityGroup] ,
	[ActivityNo] ,
	[ActivityRecordID] ,
	[SubGroup] ,
	[SubGroupRecordID] ,
	[UserID] ,
	[DateEntered] ,
	[Notes] ,
	[attachmentRecID] ,
	[markUpID] ,
	[markupEXT] ,
	[markupName] ,
	[markupStatus] ,
	tsannoID 
From dbo.Prmry_Notes 
where Notes like '%garage code%'
and DateEntered > GETDATE() -1

 
insert into #tmp_hii
(
	[NoteID] ,
	[ActivityGroup] ,
	[ActivityNo] ,
	[ActivityRecordID] ,
	[SubGroup] ,
	[SubGroupRecordID] ,
	[UserID] ,
	[DateEntered] ,
	[Notes] ,
	[attachmentRecID] ,
	[markUpID] ,
	[markupEXT] ,
	[markupName] ,
	[markupStatus] ,
	tsannoID
)
select 
	[NoteID] ,
	[ActivityGroup] ,
	[ActivityNo] ,
	[ActivityRecordID] ,
	[SubGroup] ,
	[SubGroupRecordID] ,
	[UserID] ,
	[DateEntered] ,
	[Notes] ,
	[attachmentRecID] ,
	[markUpID] ,
	[markupEXT] ,
	[markupName] ,
	[markupStatus] ,
	tsannoID
From dbo.Prmry_Notes 
where Notes like '%combo%' and Notes like '%[0-9]%' and Notes not like '%combo fence%'
and notes not like '%combo detector%'
and DateEntered > GETDATE() -1


insert into #tmp_hii
(
	[NoteID] ,
	[ActivityGroup] ,
	[ActivityNo] ,
	[ActivityRecordID] ,
	[SubGroup] ,
	[SubGroupRecordID] ,
	[UserID] ,
	[DateEntered] ,
	[Notes] ,
	[attachmentRecID] ,
	[markUpID] ,
	[markupEXT] ,
	[markupName] ,
	[markupStatus] ,
	tsannoID
)
select 
	[NoteID] ,
	[ActivityGroup] ,
	[ActivityNo] ,
	[ActivityRecordID] ,
	[SubGroup] ,
	[SubGroupRecordID] ,
	[UserID] ,
	[DateEntered] ,
	[Notes] ,
	[attachmentRecID] ,
	[markUpID] ,
	[markupEXT] ,
	[markupName] ,
	[markupStatus] ,
	tsannoID
From dbo.Prmry_Notes 
where Notes like '% LB c%'
and DateEntered > GETDATE() -1


insert into #tmp_hii
(
	[NoteID] ,
	[ActivityGroup] ,
	[ActivityNo] ,
	[ActivityRecordID] ,
	[SubGroup] ,
	[SubGroupRecordID] ,
	[UserID] ,
	[DateEntered] ,
	[Notes] ,
	[attachmentRecID] ,
	[markUpID] ,
	[markupEXT] ,
	[markupName] ,
	[markupStatus] ,
	tsannoID
)
select 
	[NoteID] ,
	[ActivityGroup] ,
	[ActivityNo] ,
	[ActivityRecordID] ,
	[SubGroup] ,
	[SubGroupRecordID] ,
	[UserID] ,
	[DateEntered] ,
	[Notes] ,
	[attachmentRecID] ,
	[markUpID] ,
	[markupEXT] ,
	[markupName] ,
	[markupStatus] ,
	tsannoID
From dbo.Prmry_Notes 
where Notes like '%LB:%'
and DateEntered > GETDATE() -1


insert into #tmp_hii
(
	[NoteID] ,
	[ActivityGroup] ,
	[ActivityNo] ,
	[ActivityRecordID] ,
	[SubGroup] ,
	[SubGroupRecordID] ,
	[UserID] ,
	[DateEntered] ,
	[Notes] ,
	[attachmentRecID] ,
	[markUpID] ,
	[markupEXT] ,
	[markupName] ,
	[markupStatus] ,
	tsannoID
)
select 
	[NoteID] ,
	[ActivityGroup] ,
	[ActivityNo] ,
	[ActivityRecordID] ,
	[SubGroup] ,
	[SubGroupRecordID] ,
	[UserID] ,
	[DateEntered] ,
	[Notes] ,
	[attachmentRecID] ,
	[markUpID] ,
	[markupEXT] ,
	[markupName] ,
	[markupStatus] ,
	tsannoID
From dbo.Prmry_Notes 
where Notes like '%Combo%'
and DateEntered > GETDATE() -1
and Notes not like '%combo fence%'
and notes not like '%combo detector%'

insert into #tmp_hii
(
	[NoteID] ,
	[ActivityGroup] ,
	[ActivityNo] ,
	[ActivityRecordID] ,
	[SubGroup] ,
	[SubGroupRecordID] ,
	[UserID] ,
	[DateEntered] ,
	[Notes] ,
	[attachmentRecID] ,
	[markUpID] ,
	[markupEXT] ,
	[markupName] ,
	[markupStatus] ,
	tsannoID
)
select 
	[NoteID] ,
	[ActivityGroup] ,
	[ActivityNo] ,
	[ActivityRecordID] ,
	[SubGroup] ,
	[SubGroupRecordID] ,
	[UserID] ,
	[DateEntered] ,
	[Notes] ,
	[attachmentRecID] ,
	[markUpID] ,
	[markupEXT] ,
	[markupName] ,
	[markupStatus] ,
	tsannoID
From dbo.Prmry_Notes 
where Notes like '%L.Box%'
and DateEntered > GETDATE() -1


insert into #tmp_hii
(
	[NoteID] ,
	[ActivityGroup] ,
	[ActivityNo] ,
	[ActivityRecordID] ,
	[SubGroup] ,
	[SubGroupRecordID] ,
	[UserID] ,
	[DateEntered] ,
	[Notes] ,
	[attachmentRecID] ,
	[markUpID] ,
	[markupEXT] ,
	[markupName] ,
	[markupStatus] ,
	tsannoID
)
select 
	[NoteID] ,
	[ActivityGroup] ,
	[ActivityNo] ,
	[ActivityRecordID] ,
	[SubGroup] ,
	[SubGroupRecordID] ,
	[UserID] ,
	[DateEntered] ,
	[Notes] ,
	[attachmentRecID] ,
	[markUpID] ,
	[markupEXT] ,
	[markupName] ,
	[markupStatus] ,
	tsannoID
From dbo.Prmry_Notes 
where Notes like '%LockBox%'
and DateEntered > GETDATE() -1

insert into #tmp_hii
(
	[NoteID] ,
	[ActivityGroup] ,
	[ActivityNo] ,
	[ActivityRecordID] ,
	[SubGroup] ,
	[SubGroupRecordID] ,
	[UserID] ,
	[DateEntered] ,
	[Notes] ,
	[attachmentRecID] ,
	[markUpID] ,
	[markupEXT] ,
	[markupName] ,
	[markupStatus] ,
	tsannoID
)
select 
	[NoteID] ,
	[ActivityGroup] ,
	[ActivityNo] ,
	[ActivityRecordID] ,
	[SubGroup] ,
	[SubGroupRecordID] ,
	[UserID] ,
	[DateEntered] ,
	[Notes] ,
	[attachmentRecID] ,
	[markUpID] ,
	[markupEXT] ,
	[markupName] ,
	[markupStatus] ,
	tsannoID 
From dbo.Prmry_Notes 
where Notes like '%LocBox%'
and DateEntered > GETDATE() -1

insert into #tmp_hii
(
	[NoteID] ,
	[ActivityGroup] ,
	[ActivityNo] ,
	[ActivityRecordID] ,
	[SubGroup] ,
	[SubGroupRecordID] ,
	[UserID] ,
	[DateEntered] ,
	[Notes] ,
	[attachmentRecID] ,
	[markUpID] ,
	[markupEXT] ,
	[markupName] ,
	[markupStatus] ,
	tsannoID
)
select 
	[NoteID] ,
	[ActivityGroup] ,
	[ActivityNo] ,
	[ActivityRecordID] ,
	[SubGroup] ,
	[SubGroupRecordID] ,
	[UserID] ,
	[DateEntered] ,
	[Notes] ,
	[attachmentRecID] ,
	[markUpID] ,
	[markupEXT] ,
	[markupName] ,
	[markupStatus] ,
	tsannoID
From dbo.Prmry_Notes 
where Notes like '%Box%' and Notes like '%code%'
and DateEntered > GETDATE() -1

insert into #tmp_hii
(
	[NoteID] ,
	[ActivityGroup] ,
	[ActivityNo] ,
	[ActivityRecordID] ,
	[SubGroup] ,
	[SubGroupRecordID] ,
	[UserID] ,
	[DateEntered] ,
	[Notes] ,
	[attachmentRecID] ,
	[markUpID] ,
	[markupEXT] ,
	[markupName] ,
	[markupStatus] ,
	tsannoID
)
select 
	[NoteID] ,
	[ActivityGroup] ,
	[ActivityNo] ,
	[ActivityRecordID] ,
	[SubGroup] ,
	[SubGroupRecordID] ,
	[UserID] ,
	[DateEntered] ,
	[Notes] ,
	[attachmentRecID] ,
	[markUpID] ,
	[markupEXT] ,
	[markupName] ,
	[markupStatus] ,
	tsannoID
 From dbo.Prmry_Notes 
 where Notes like '%key pad%'
 and DateEntered > GETDATE() -1

----

  
	

 
--select * from #tmp_hii
-- where Notes not like '%custom screeen%'
--PUT TEMP RESULTS INTO TABLE SO THAT THEY CAN BE EMAILED TODAY--
DECLARE @COUNT AS INT
SELECT @COUNT =  COUNT(*) FROM #tmp_hii
IF @COUNT > 0 
BEGIN
	insert into DBO.LT_SENSITVE_DATA_HOLD
	select * FROM #tmp_hii

	
	--so the the front counter can edit these notes, switch the user:
	update b
	set userid = 'DECO'
	from dbo.LT_SENSITVE_DATA_HOLD a
	inner join dbo.Prmry_Notes b
		on a.NoteID = b.NoteID
	where
		b.userid in ('ECON','EPRS')    
 



 --------------------------------------------------------------------------
 --send email to bldg staff to let them know of possible sensitive data-
 --------------------------------------------------------------------------
--IF OBJECT_ID('CRW_PROD.DBO.LT_SENSITVE_DATA_HOLD') IS NOT NULL
--BEGIN
----email address [jhutchens@arvada.org;] removed on 5/12/2021 by P.Downing per Josie request

   EXEC MSDB.DBO.sp_send_dbmail
			@profile_name =  N'BldgStaffSensitiveData',
			@recipients =    N'jsuk@arvada.org;buildingpermits@arvada.org',
			@subject = 'TRAKIT/ETRAKIT  Possible ',
			@body = 'The following permit was found with possible sensitive data in the notes field.   Please review and follow proper procedures',
			@execute_query_database = 'msdb',
			@query_no_truncate = -1 ,
			@query_result_header = 1, 
			@query = 'SELECT DISTINCT  ''NoteID - '' + cast(NOTEID as varchar(12))   , ''ACTIVITYNO -  '' + CAST(ACTIVITYNO AS VARCHAR(20))  from  crw_prod.dbo.LT_SENSITVE_DATA_HOLD'  --        @query = 'SELECT top (3)  ''NoteID - '' + cast(NOTEID as varchar(-12))   , ''ACTIVITYNO - '' + CAST(ACTIVITYNO AS VARCHAR(20))  from tempdb..#tmp_hii'

		-- [dbo].[lp_sensitive_data_notify]

 END --END TO BEGIN 
-- End T-SQL --
END


GO
