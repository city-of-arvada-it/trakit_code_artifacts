USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GblSearch3]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_GblSearch3]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GblSearch3]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[tsp_GblSearch3] 
	-- Add the parameters for the stored procedure here
	@searchString Varchar(500) = Null
 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT top 1000 * from tvw_GblActivities where
	(activityNo like @searchString 
	or site_APN like @searchString
	or site_addr like @searchString
	or owner_name like @searchString
	or site_alternate_id like @searchString)
	and (tgroup in ('PERMIT','CASE','PROJECT','LICENSE2'))
	order by ACTIVITYNO asc
END






GO
