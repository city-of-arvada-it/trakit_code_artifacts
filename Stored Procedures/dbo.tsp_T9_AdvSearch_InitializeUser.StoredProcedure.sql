USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearch_InitializeUser]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_AdvSearch_InitializeUser]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearch_InitializeUser]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  -- ===================================================================================================================
-- DESCRIPTION:
--		Call this when user logs in to T9 to ensure the user has
--			* Records for New, Last, and Current special searches
--			* A record in the user options table
--			* A record in the GroupMembers table with membership to the �System Default� Group
--
-- NOTES:
--		Called by [tsp_T9_AdvSearch_Get] if New or Last special searches are not found in the Item table for @UserID
--
-- REVISION HISTORY: 
--		JUL 02, 2015 - EWH - Initial version
--		JUL 31, 2015 - EWH - Set default for SearchType ='' instead of 'MAIN'
--		JUL 31, 2015 - EWH - added 'output' after @DeptID when calling tsp_T9_AdvSearch_InitializeDefaultDepartment
--		AUG 03, 2015 - EWH - added IsDirty value of 0 when creating new CurrentItem or LastItem record
-- ===================================================================================================================
-- ------------------------------------------------------------------------------------------------
-- TEST
-- ------------------------------------------------------------------------------------------------
/*
	DECLARE @return_value int
	DECLARE @UserID varchar(50)
	set		@UserID = 'EH'

	EXECUTE @return_value = [dbo].[tsp_T9_AdvSearch_InitializeUser] 
			@UserID
	GO
*/
-- ------------------------------------------------------------------------------------------------
create PROCEDURE [dbo].[tsp_T9_AdvSearch_InitializeUser]

	@UserID varchar(50) = null

AS
BEGIN

declare @ErrMsg varchar(50)
set @ErrMsg = ''

if @UserID is not null
begin
	-- ensure a user option record exists for @UserID
	-- defaults: 
	--   display editor and results in TrakitMain 
	--   Editor default = Last search
	--   Results default = Last search
	--   Show editor when opening Adv Search
	--   prompt before overwriting
	--   order search list alphabetically
	if (select count(*) from [Prmry_AdvSearch_UserOptions] where UserID=@UserID) = 0 
	begin
		insert into Prmry_AdvSearch_UserOptions 
		([UserID], DisplayStyle, EditorWindowDefaultSearch, EditorWindowDefaultSearchID, ResultsWindowDefaultSearch, ResultsWindowDefaultSearchID, EditorWindowTakesPriorityOverResultsWindow, PromptBeforeOverwritingSavedSearchCriteria, PickListOrder) 
		values (@UserID, 1, -1, -1, -1, -1, 1, 1, 0)
	end

	-- get DepartmentID for Default DepartmentID
	declare @AddUserToDefaultDept bit
	set @AddUserToDefaultDept = 0 -- false
	declare @DeptID int
	set @DeptID = (select top 1 [DepartmentID] from [Prmry_AdvSearch_Departments] (nolock) where [DepartmentName] = 'Default')

	if @DeptID is null
	begin
		-- Add searches to Default DepartmentID since they don't exist
		exec tsp_T9_AdvSearch_InitializeDefaultDepartment @DeptID output -- @DeptID is out parameter for tsp_T9_AdvSearch_InitializeDefaultDepartment
		set @AddUserToDefaultDept = 1 -- we already know we need to add user to this DeptID
	end
	
	-- ensure there are existing shared searches
	if (select count(*) from Prmry_AdvSearch_SharedItem (nolock) where DepartmentID=@DeptID) = 0
	begin
		-- Add searches to Default DepartmentID since they don't exist
		exec tsp_T9_AdvSearch_InitializeDefaultDepartment @DeptID output -- @DeptID is out parameter for tsp_T9_AdvSearch_InitializeDefaultDepartment
	end


	-- ensure @UserID is a member of the default department
	if @AddUserToDefaultDept = 0
	begin
		if (select count(*) from [Prmry_AdvSearch_DepartmentMembers] (nolock) where UserID=@UserID and DepartmentID=@DeptID ) = 0
		begin
			set @AddUserToDefaultDept = 1
		end
	end

	-- add @UserID to @DeptID 
	if @AddUserToDefaultDept = 1
	begin
		insert into [Prmry_AdvSearch_DepartmentMembers] ([DepartmentID], [UserID], [CanGet], [CanShare], [CanRename], [CanDelete], [NotifyOfNewShare], [NotifyOfDeletedShare]) values (@DeptID, @UserID, 1, 0, 0, 0, 1, 1)
	end
	
	-- make sure user has user level records for shared searches s/he can access --> make sure each search is only in there one time!
	insert into [Prmry_AdvSearch_DepartmentMembersItems] ([DepartmentID],[UserID],[SharedSearchID],[DisplayInUserList],[LastViewedResults],[ViewCount],[Seq])
	select distinct si.DepartmentID, dm.UserID, si.RecordID, 1, GETDATE(), 0, 0 
	from Prmry_AdvSearch_SharedItem si inner join Prmry_AdvSearch_DepartmentMembers dm on si.DepartmentID = dm.DepartmentID
	where dm.UserID = @UserID and dm.CanGet = 1 and si.RecordID not in ( select SharedSearchID from [Prmry_AdvSearch_DepartmentMembersItems] where UserID = @UserID )

	-- ensure new, last, and current search items exist
	-- ------------------------------------------------------------------------------------------------------------------------------------
	
	-- check to see if we are missing anything
	declare @InsertNew int
	declare @InsertLast int
	declare @InsertCurrent int

	set @InsertNew = case when (select top 1 RecordID from [Prmry_AdvSearch_NewItem] (nolock) where UserID=@UserID) is null then 1 else 0 end
	set @InsertLast = case when (select top 1 RecordID from [Prmry_AdvSearch_LastItem] (nolock) where UserID=@UserID) is null then 1 else 0 end
	set @InsertCurrent = case when (select top 1 RecordID from [Prmry_AdvSearch_CurrentItem] (nolock) where UserID=@UserID) is null then 1 else 0 end

	print @InsertNew
	print @InsertLast
	print @InsertCurrent

	--	only proceed if we are missing something so we don't waste time declaring and setting variables if it is unnecessary
	if (@InsertNew + @InsertLast + @InsertCurrent) > 0
	begin
		-- default values
		-- TBD: Keep these values or do something else?
		-- ---------------------------------------------
	
		-- search name - get "New Search" value from function
		declare @SearchName varchar(50)
		select @SearchName = dbo.tfn_T9_AdvSearch_NewSearchName()

		-- item values
		declare @GroupName varchar(50)
		declare @SearchType varchar(50)

		declare @ReadOnly bit

		set @GroupName = 'PERMIT'
		set @SearchType = 'PERMIT_MAIN'
		set @ReadOnly = 1


		-- detail values
		declare @ItemID int
		declare @TableName varchar(50)
		declare @FieldName varchar(50)
		declare @DisplayName varchar(50)
		declare @DataType varchar(50)
	
		set @TableName = 'Permit_Main'
		set @FieldName = 'PERMIT_NO'
		set @DisplayName = 'Permit Number'
		set @DataType = 'varchar'


		-- insert record into New if necessary
		if @InsertNew = 1
		begin
			-- create New search item
			insert into [Prmry_AdvSearch_NewItem] ([UserID], [SearchName], [GroupName], [SearchType], [ReadOnly]) 
			values (@UserID, @SearchName, @GroupName, @SearchType, @ReadOnly)
			set @ItemID = SCOPE_IDENTITY()

			-- create New special search detail 
			insert into [Prmry_AdvSearch_NewDetail] ([ItemID], [TableName], [FieldName], [DisplayName], [Datatype])
			values (@ItemID, @TableName, @FieldName, @DisplayName, @DataType)
		end

		-- insert record into Last if necessary
		if @InsertLast = 1
		begin
			-- create Last search item 
			insert into [Prmry_AdvSearch_LastItem] ([UserID], [SearchName], [GroupName], [SearchType], [ReadOnly], [IsDirty]) 
			values (@UserID, @SearchName, @GroupName, @SearchType, @ReadOnly, 0)
			set @ItemID = SCOPE_IDENTITY()

			-- create Last search detail
			insert into [Prmry_AdvSearch_LastDetail] ([ItemID], [TableName], [FieldName], [DisplayName], [Datatype])
			values (@ItemID, @TableName, @FieldName, @DisplayName, @DataType)
		end

		-- insert record into Current if necessary
		if @InsertCurrent = 1
		begin
			-- create Current search item 
			insert into [Prmry_AdvSearch_CurrentItem] ([UserID], [SearchName], [GroupName], [SearchType], [ReadOnly], [IsDirty]) 
			values (@UserID, @SearchName, @GroupName, @SearchType, @ReadOnly, 0)
			set @ItemID = SCOPE_IDENTITY()

			-- create Current search detail
			insert into [Prmry_AdvSearch_CurrentDetail] ([ItemID], [TableName], [FieldName], [DisplayName], [Datatype])
			values (@ItemID, @TableName, @FieldName, @DisplayName, @DataType)
		end
	end
	-- ------------------------------------------------------------------------------------------------------------------------------------
end
else
begin
	set @ErrMsg = 'UserID required'
end

select @ErrMsg [ErrMsg]

END
GO
