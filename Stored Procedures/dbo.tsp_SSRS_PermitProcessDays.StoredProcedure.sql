USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_SSRS_PermitProcessDays]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_SSRS_PermitProcessDays]
GO
/****** Object:  StoredProcedure [dbo].[tsp_SSRS_PermitProcessDays]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  					  
					  					  					  
-- =============================================
-- Author:	Dan Haynes
-- Create date: 12/01/2011
-- Description:	Calculates Days Between Dates in Permit Process
-- =============================================
CREATE PROCEDURE [dbo].[tsp_SSRS_PermitProcessDays] 
		@PERMIT_NO varchar(15)
AS
BEGIN


--RB Check if table exists before creating it
   IF OBJECT_ID('tempdb..#table1') IS NOT NULL  
	drop table #table1

create table #table1
	(APPLIEDTOAPPROVED int
	,APPROVEDTOISSUED int
	,ISSUEDTOFINALED int);
	
insert into #table1 (APPLIEDTOAPPROVED, APPROVEDTOISSUED, ISSUEDTOFINALED)
select
	APPLIEDTOAPPROVED = CONVERT(bigint,CONVERT(datetime,APPROVED))- CONVERT(bigint,CONVERT(datetime,APPLIED)),
	APPROVEDTOISSUED = CONVERT(bigint,CONVERT(datetime,ISSUED))- CONVERT(bigint,CONVERT(datetime,APPROVED)),
	ISSUEDTOFINALED = CONVERT(bigint,CONVERT(datetime,FINALED))- CONVERT(bigint,CONVERT(datetime,ISSUED))
	from Permit_Main
	where PERMIT_NO = @PERMIT_NO;

update #table1 set APPLIEDTOAPPROVED = 0 where APPLIEDTOAPPROVED < 0 ;
update #table1 set APPROVEDTOISSUED = 0 where APPROVEDTOISSUED < 0 ;
update #table1 set ISSUEDTOFINALED = 0 where ISSUEDTOFINALED < 0 ;

select * from #table1;
END











GO
