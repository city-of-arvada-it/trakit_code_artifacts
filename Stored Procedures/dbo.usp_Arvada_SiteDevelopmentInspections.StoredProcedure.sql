USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_SiteDevelopmentInspections]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[usp_Arvada_SiteDevelopmentInspections]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_SiteDevelopmentInspections]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_Arvada_SiteDevelopmentInspections]
	@PermitNo varchar(30),
	@RecID varchar(30) = null
as
/**********************************************************************************************************************
* Creator:      	 
* Date:         	 
* Title:			 
* Description:  	 
* Examples:    

EXECUTE dbo.usp_Arvada_SiteDevelopmentInspections @PermitNo = 'SITE15-00088', @RecID = 'SGO:1602170906350835'
select *
from dbo.permit_inspections
where permit_no like 'SITE15-00088'

'SITE15-00088'

	 
* Modifications:  	
  Developer Name		Date		Description
  ------------------	----------	----------------------------------------------------------- 
*
**********************************************************************************************************************/

set nocount on
 
 --------------------------
 --- Declare Variables   --
 --------------------------

 
 --------------------------
 --- Create Temp Tables  --
 --------------------------

	 
 --------------------------
 --- Body of Procedure   --
 --------------------------

 SELECT 
	m.[DESCRIPTION] AS ProjectName,
	m.PERMIT_NO ,
	u.UserName,
	u.PHONE,
	i.SCHEDULED_DATE,
	i.SCHEDULED_TIME,
	i.INSPECTOR,
	i.InspectionType,
	i.RESULT,
	i.RECORDID as UniqueInspection,
		stuff(
		(	select CHAR(13) + pn.notes 
			from dbo.Prmry_Notes AS pn  
			WHERE   pn.SubGroupRecordID = i.recordid
			for xml path(''), root('MyString'), type 
		).value('/MyString[1]','nvarchar(max)') ,1,1,'') as notes,
	i.COMPLETED_DATE,
	i.COMPLETED_TIME
into #a
FROM dbo.permit_main m
INNER JOIN dbo.Permit_Inspections i
	ON i.PERMIT_NO = m.PERMIT_NO
INNER JOIN dbo.Prmry_Users u
	on i.INSPECTOR = u.UserID
WHERE
	m.PERMIT_NO = @PermitNo
and (i.recordid =  @RecID or @RecID is null)
and i.inspectionType in ('ROUTINE','FOLLOW UP','Other-Site Dev','Final Site develop**','INITIAL SITE DEV','SITE DEV. COMPLAINT','OTHER- SITE DEV')
ORDER BY
	i.scheduled_date,
	i.RecordID

IF @@Rowcount > 0
begin

	 SELECT 
		ProjectName,
		PERMIT_NO ,
		UserName,
		PHONE,
		SCHEDULED_DATE,
		SCHEDULED_TIME,
		INSPECTOR,
		InspectionType,
		RESULT,
		UniqueInspection,
		notes,
		COMPLETED_DATE,
		COMPLETED_TIME
	from #a
	order by 
		SCHEDULED_DATE,
		UniqueInspection
end
else 
begin
	 SELECT 
		a.[DESCRIPTION] AS ProjectName,
		@PermitNo  as PERMIT_NO ,
		null as SCHEDULED_DATE,
		null as SCHEDULED_TIME,
		'' as INSPECTOR,
		'' as InspectionType,
		'' as RESULT,
		@RecID as UniqueInspection,
		'' as notes ,
		null as COMPLETED_DATE,
		null as COMPLETED_TIME
	FROM dbo.permit_main a
	WHERE
		a.PERMIT_NO = @PermitNo 
end
	 
 
GO
