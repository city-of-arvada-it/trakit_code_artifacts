USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ModelHomeOptionValuation_Delete]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_ModelHomeOptionValuation_Delete]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ModelHomeOptionValuation_Delete]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  
CREATE PROCEDURE [dbo].[tsp_T9_ModelHomeOptionValuation_Delete] 
              @RecID varchar(10)  = '0'
AS
BEGIN

       
       if @RecID <> '0'
              Begin

                    DELETE from Prmry_ModelHomeOptionValuations WHERE RecordID = @RecID



              end

END





GO
