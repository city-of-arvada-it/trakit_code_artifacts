USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_t9_InsertUnlinkedCheckListValue]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_t9_InsertUnlinkedCheckListValue]
GO
/****** Object:  StoredProcedure [dbo].[tsp_t9_InsertUnlinkedCheckListValue]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  

	CREATE procedure [dbo].[tsp_t9_InsertUnlinkedCheckListValue]
	@itemText varchar (50),
	@notes varchar(max),
	@status int,
	@ActivityNo varchar(50),
	@ParentRecordID varchar(50),
	@userID varchar(50)	
	as
	Begin

		
		
		/* 
		All written from the point of view of the checklist item
		Major Parent record = is Permit Record ID, ParentREcord ID would be Inspection */
		insert into checklistvalues (recordid, subrecordid, itemvalue, oneTimeItemText, checklistid, userid,  checkdate) values(
									@ActivityNo, @ParentRecordID, @status, @itemText, -1, @userID,  getdate())

		if len(@notes) > 0
			begin
		Insert Into checkListNotes (CheckListValueID, recordID, SubRecordID, Note,UserID, LastUpdate) Values	(
									(Select Scope_Identity() as CheckListValueID), @ActivityNo, @ParentRecordID, @notes, @userID, getdate())
		
			end

		

	End





GO
