USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[ALP_ParameterSetOperationValueSearch]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[ALP_ParameterSetOperationValueSearch]
GO
/****** Object:  StoredProcedure [dbo].[ALP_ParameterSetOperationValueSearch]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ALP_ParameterSetOperationValueSearch] (
		@InputParameterSet int = Null,
		@InputOperationId int = Null
 )
 
AS
BEGIN

		Declare @iRwCnt int,  
				@ParameterSetId int, 
				@OperationId int, 	
				@OperationValueNameId int,
				@OperationValueId int, 
				@ScopeIdentityValue int 	

		-- Test Data
		--Declare @InputParameterSet int = 12
		--Declare @InputOperationId int = 1



		Begin Try

		IF OBJECT_ID('tempdb..#TempOperationValues ') IS NOT NULL
		  DROP TABLE #TempOperationValues 

		if (@InputParameterSet is null or @InputOperationId is null) or (@InputOperationId < 0 or @InputParameterSet < 0)
			Begin
					Set @iRwCnt = 0	
					
					Select ParameterSetId, OperationId, OperationSequence, OperationValueNameId, ValueName, OperationValueId, Value , IsValueOptional 
					from ALP_OperationValuesByParameterSet where parametersetid = @InputParameterSet  and operationid = @InputOperationId 
						--and IsValueOptional = 0 
						order by OperationValueNameId 					

			End
		Else
			Begin

					-- Put Requested Data into temp Table
					select ParameterSetId, OperationId, OperationSequence, OperationValueNameId, ValueName, IsValueOptional, OperationValueId, Value  
					into #TempOperationValues from ALP_OperationValuesByParameterSet 
					where parametersetid =  @InputParameterSet and operationid = @InputOperationId --and IsValueOptional = 0 
						and OperationValueId is null order by OperationValueNameId 

					-- Get number of records returned
					set @iRwCnt = @@ROWCOUNT			
	
					if @iRwCnt = 0
						Begin
							Select ParameterSetId, OperationId, OperationSequence, OperationValueNameId, ValueName, OperationValueId, Value , IsValueOptional
							from ALP_OperationValuesByParameterSet where parametersetid =  @InputParameterSet and operationid = @InputOperationId 
							--	and IsValueOptional = 0 
								order by OperationValueNameId 
								
							Return
						End	
	
						-- Start Loop to go thru No Operation Values records
					While @iRwCnt > 0
						Begin

							-- Select one Record
							Select top 1 @ParameterSetId = ParameterSetId, @OperationId = OperationId, @OperationValueNameId = OperationValueNameId, 
										 @OperationValueId = OperationValueId from #TempOperationValues 

							-- Get Record Count
							set @iRwCnt = @@ROWCOUNT 

							-- If Record Count > 0 then there are parameter sets / Operations with no assigned Operation Values

							if @iRwCnt > 0
								Begin
				
									Set @ScopeIdentityValue = -1

									-- Insert new Operation values records + get Scope Identity to insert into ParameterSetOperationValues Tables

									Insert Into ALP_OperationValues (OperationId, OperationValueNameId, Value) 
									Values (@OperationId, @OperationValueNameId, '')

									Set @ScopeIdentityValue = SCOPE_IDENTITY()				
					
									-- insert into ParameterSetOperationValues Tables
									if @ScopeIdentityValue > -1
										Begin
											Insert Into ALP_ParameterSetOperationValues (ParameterSetId, OperationId, OperationValueId) 
											Values (@ParameterSetId, @OperationId, @ScopeIdentityValue)
										End 
					
									-- Delete from Temporary Table so that we can process next record if any
									delete from #TempOperationValues where ParameterSetId = @ParameterSetId  and OperationId = @OperationId 
										and OperationValueNameId = @OperationValueNameId  

								End
							else
								Begin
									-- If no more records to process then Just return relevant records		
									Set @iRwCnt = 0	
					
									Select ParameterSetId, OperationId, OperationSequence, OperationValueNameId, ValueName, OperationValueId, Value , IsValueOptional
									from ALP_OperationValuesByParameterSet where parametersetid =  @InputParameterSet and operationid = @InputOperationId 
										--and IsValueOptional = 0 
										order by OperationValueNameId 
								
									Return
								End
						End
				End

		End Try
		Begin Catch
			-- If error then reset @iRwCnt to 0 and return an empty dataset
			Set @iRwCnt = 0	

			select Top 0 ParameterSetId, OperationId, OperationSequence, OperationValueNameId, ValueName, OperationValueId, Value, IsValueOptional
			from ALP_OperationValuesByParameterSet 	
	
			Return
		End Catch
End



GO
