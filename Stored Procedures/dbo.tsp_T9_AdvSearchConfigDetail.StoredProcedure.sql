USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearchConfigDetail]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_AdvSearchConfigDetail]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearchConfigDetail]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  
                                    
-- ==========================================================================================
-- Revision History:
--       2014-03-03 - mdm - added to sourcesafe
--            2014 NOV 24 - EWH - added filter to not include data in [Prmry_AdvSearchExclusions]
-- ==========================================================================================   

CREATE PROCEDURE [dbo].[tsp_T9_AdvSearchConfigDetail] (
       @ParentRecordID varchar(10) 
)

AS
BEGIN


select * from Prmry_AdvSearchConfigDetail 
where ParentRecordID = @ParentRecordID 
and [Prmry_AdvSearchConfigDetail].[TableName]+'|'+[Prmry_AdvSearchConfigDetail].[FieldName] not in ( select [Prmry_AdvSearchExclusions].[tableName]+'|'+[Prmry_AdvSearchExclusions].[columnname] from Prmry_AdvSearchExclusions )
order by  recordid


END






GO
