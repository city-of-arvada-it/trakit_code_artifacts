USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_e3_Security_SetPassword_Contractor]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_e3_Security_SetPassword_Contractor]
GO
/****** Object:  StoredProcedure [dbo].[tsp_e3_Security_SetPassword_Contractor]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[tsp_e3_Security_SetPassword_Contractor]
  @st_lic_no varchar(50)
, @Salt varchar(128)
, @HashedPassword varchar(128)
, @HashedSecretAnswer varchar(128)
, @Creator varchar(10)
, @CreationReason varchar(20)

AS
BEGIN
SET NOCOUNT ON;
	If Exists(select 1 from Prmry_Security_eTRAKiTContractorUserData WHERE st_lic_no = @st_lic_no)
	Begin
		DELETE FROM [Prmry_Security_eTRAKiTContractorUserData] WHERE st_lic_no = @st_lic_no
	End
INSERT INTO [Prmry_Security_eTRAKiTContractorUserData]  (st_lic_no, Salt, HashedPassword, HashedSecretAnswer,  Creator, CreationReason, CreationDate) values (@st_lic_no, @Salt, @HashedPassword, @HashedSecretAnswer, @Creator, @CreationReason, getdate())

END
GO
