USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_export_CustomObj_mdm]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_export_CustomObj_mdm]
GO
/****** Object:  StoredProcedure [dbo].[tsp_export_CustomObj_mdm]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  
					  					  					  					  




CREATE PROCEDURE [dbo].[tsp_export_CustomObj_mdm] (
@OutputPath Varchar(1000) = Null
)

AS
BEGIN
  SET NOCOUNT ON
      
  CREATE TABLE #MyObjectHierarchy 
   (
    HID int identity(1,1) not null primary key,
    ObjectId int,
    TYPE int,OBJECTTYPE AS CASE 
                             WHEN TYPE =  1 THEN 'FUNCTION' 
                             WHEN TYPE =  4 THEN 'VIEW' 
                             WHEN TYPE = 16 THEN 'PROCEDURE'
                             WHEN TYPE = 256 THEN 'TRIGGER'
                             ELSE ''
                           END,
   ONAME varchar(255), 
   OOWNER varchar(255), 
   SEQ int
   )
  --results table
  CREATE TABLE #Results(ResultsID int identity(1,1) not null,ResultsText varchar(max) )
  --list of objects in dependancy order
  INSERT #MyObjectHierarchy (TYPE,ONAME,OOWNER,SEQ)
    EXEC sp_msdependencies @intrans = 1 

Update #MyObjectHierarchy SET ObjectId = object_id(OOWNER + '.' + ONAME)

   DELETE FROM #MyObjectHierarchy WHERE objectid in(
    SELECT [object_id] FROM sys.synonyms UNION ALL
    SELECT [object_id] FROM master.sys.synonyms)
    
DELETE FROM #MyObjectHierarchy WHERE OBJECTTYPE = ''

DELETE FROM #MyObjectHierarchy WHERE  (
	(LEFT(ONAME,3) <> 'tfn' ) 
	and (LEFT(ONAME,3) <> 'tvw' ) 
	and (LEFT(ONAME,3) <> 'tsp' )
	and (ONAME not like 'tgr_Net_Pref%')
	and (ONAME not like 'tgr_T9_Pref%')
	--and (ONAME not like '%AuditTable%')
	--and (ONAME not like 'Pref_Geo%')
	--and (ONAME not like '%ChangeTracking')
	)
	
--Temp only
--DELETE FROM #MyObjectHierarchy WHERE ONAME like 'tsp_SSRS%' OR ONAME like 'tvw_SSRS%' OR ONAME like 'tfn_SSRS%'
	
	
	--Select * From #MyObjectHierarchy
	--Return
  
  DECLARE
    @schemaname     varchar(255),
    @objname        varchar(255),
    @objecttype     varchar(20),
    @ModDate            Datetime,
    @FullObjectName varchar(510),
    @FullText           varchar(max),
    @sp                       varchar(max)
    set @FullText = ''


  DECLARE cur1 CURSOR FOR 
	SELECT OOWNER,ONAME,OBJECTTYPE FROM #MyObjectHierarchy ORDER BY HID
  OPEN cur1
  FETCH NEXT FROM cur1 INTO @schemaname,@objname,@objecttype
  WHILE @@fetch_status <> -1
	BEGIN		
	If  @@fetch_status <> -2
	 BEGIN
		SET @FullObjectName = QUOTENAME(@schemaname) + '.' + QUOTENAME(@objname)
		PRINT @objecttype + ' - ' + @FullObjectName
              
			IF @objecttype = 'FUNCTION'                  
                  BEGIN  
                             
                      select @FullText = @FullText + '
                      IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''' + @FullObjectName + '''))'
                      --select @FullText = @FullText + CHAR(10) + CHAR(13)  
                      select @FullText = @FullText +  '
                      DROP FUNCTION ' + @FullObjectName
                      --select @FullText = @FullText + CHAR(10) + CHAR(13)  
                      select @FullText = @FullText + '
                      GO
                      '
                      --select @FullText = @FullText + CHAR(10) + CHAR(13)  
                      select @FullText = @FullText +  ''        
                                    
                      select @sp =  definition from [sys].[sql_modules] WHERE object_id = OBJECT_ID(N'' + @FullObjectName + '')
                      
                      select @FullText = @FullText + @sp
                      --select @FullText = @FullText + CHAR(10) + CHAR(13)  
                      select @FullText = @FullText + '
                      GO
                      '
                      --select @FullText = @FullText + CHAR(10) + CHAR(13)  
                        
				END		
				IF @objecttype = 'VIEW'
                  BEGIN
                  
                      select @FullText = @FullText + '
                      IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N''' + @FullObjectName + '''))'   
                      --select @FullText = @FullText + CHAR(10) + CHAR(13)              
                      select @FullText = @FullText +  '
                      DROP VIEW ' + @FullObjectName
                      --select @FullText = @FullText + CHAR(10) + CHAR(13)  
                      select @FullText = @FullText + '
                      GO
                      '
                      --select @FullText = @FullText + CHAR(10) + CHAR(13)  
                      
                      select @sp =  definition from [sys].[sql_modules] WHERE object_id = OBJECT_ID(N'' + @FullObjectName + '')
                      
                      select @FullText = @FullText + @sp
                      --select @FullText = @FullText + CHAR(10) + CHAR(13)  
                      select @FullText = @FullText + '
                      GO
                      '
                      --select @FullText = @FullText + CHAR(10) + CHAR(13)  

                  END
            IF @objecttype = 'PROCEDURE'                 
                  BEGIN  
                                                                                   
					  select @FullText = @FullText + '
					  IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''' + @FullObjectName + '''))'
					  --select @FullText = @FullText + CHAR(10) + CHAR(13)  
					  select @FullText = @FullText +  '
					  DROP PROCEDURE ' + @FullObjectName
					  --select @FullText = @FullText + CHAR(10) + CHAR(13)  
					  select @FullText = @FullText + '
					  GO
					  '
					  --select @FullText = @FullText + CHAR(10) + CHAR(13)  
					  select @FullText = @FullText +  ''
                      
                      select @sp =  definition from [sys].[sql_modules] WHERE object_id = OBJECT_ID(N'' + @FullObjectName + '')
                          
                      select @FullText = @FullText + @sp
                      --select @FullText = @FullText + CHAR(10) + CHAR(13)  
                      select @FullText = @FullText + '
                      GO
                      '
                      --select @FullText = @FullText + CHAR(10) + CHAR(13)  

				END
				IF @objecttype = 'TRIGGER'                 
                  BEGIN  
                                                                                   
					  select @FullText = @FullText + '
					  IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''' + @FullObjectName + '''))'
					  --select @FullText = @FullText + CHAR(10) + CHAR(13)  
					  select @FullText = @FullText +  '
					  DROP TRIGGER ' + @FullObjectName
					  --select @FullText = @FullText + CHAR(10) + CHAR(13)  
					  select @FullText = @FullText + '
					  GO
					  '
					  --select @FullText = @FullText + CHAR(10) + CHAR(13)  
					  select @FullText = @FullText +  ''
                      
                      select @sp =  definition from [sys].[sql_modules] WHERE object_id = OBJECT_ID(N'' + @FullObjectName + '')
                          
                      select @FullText = @FullText + @sp
                      --select @FullText = @FullText + CHAR(10) + CHAR(13)  
                      select @FullText = @FullText + '
                      GO
                      '
                      --select @FullText = @FullText + CHAR(10) + CHAR(13)  

				END
				
			 END
            FETCH NEXT FROM cur1 INTO @schemaname,@objname,@objecttype
            
            
	END
  CLOSE cur1
  DEALLOCATE cur1
  
	Select @FullText = @FullText + '
	IF  EXISTS (SELECT * FROM sys.triggers WHERE object_id = OBJECT_ID(N''[dbo].[Prmry_AuditTrail_LandTRAK_Site_Change_Log]''))
	DROP TRIGGER [dbo].[Prmry_AuditTrail_LandTRAK_Site_Change_Log]
	GO
	'
    
    Select @FullText = @FullText + '
    EXEC tsp_SysRebuildAllTriggers
	GO
	'
    
    
  SELECT @FullText AS [processing-instruction(x)] FOR XML PATH('')
  
  
  

--Declare @SQL Varchar(8000)
--Declare @OutputFileName Varchar(1000)
--Declare @DBName Varchar(100)
--Set @DBName = 'CRW_SysMaster'

--Declare @VersionName Varchar(100)
--Set @VersionName = CONVERT(Varchar,GetDate(),112) + '_' + REPLACE(CONVERT(VARCHAR, GETDATE(), 108),':','.') + '.sql'

----If @OutputPath Is Null OR @OutputPath = ''
----	Set @OutputFileName = 'C:\crw\TEMP\Trakit9_DBUpdates.sql'
----ELSE
----	Set @OutputFileName = @OutputPath

----Set @SQL = '
----INSERT INTO ' + @DBName + '.dbo.VersionControl_SQL([VersionFileName],[VersionSQL])
----SELECT ''' + @VersionName + '.sql'', ' + @FullText + ''
----Print(@SQL)
----Exec(@SQL)

--INSERT INTO CRW_SysMaster.dbo.VersionControl_SQL([VersionFileName],[VersionSQL])
--Values(@VersionName,Convert(Varbinary(max),@FullText))


--RETURN
  


----Enable xp_cmdshell
--Set @SQL = '
--EXEC sp_configure ''show advanced options'', 1
--RECONFIGURE
--EXEC sp_configure ''xp_cmdshell'', 1
--RECONFIGURE
--'
----Print(@SQL)
--Exec(@SQL)



--Create Table tempdb..ExportCustomObj(data Varchar(max))
--Insert Into tempdb..ExportCustomObj(data)
--Values(@FullText)

----Select * from tempdb..ExportCustomObj

--select @sql = 'bcp "tempdb..ExportCustomObj" out ' + @OutputFileName + ' -c -T -S'
--+ @@servername
--Print(@SQL)
--exec master..xp_cmdshell @sql

--Drop Table tempdb..ExportCustomObj

--Set @SQL = '
----DELETE ' + @DBName + '.dbo.VersionControl_SQL Where [VersionFileName] = ''' + @VersionName + '''

--INSERT INTO ' + @DBName + '.dbo.VersionControl_SQL([VersionFileName],[VersionSQL])
--SELECT ''' + @VersionName + ''', * FROM OPENROWSET(
--  BULK N''' + REPLACE(@OutputFileName,'"','') + ''', SINGLE_BLOB
--) bt'
--Print(@SQL)
--Exec(@SQL)


---- Disable xp_cmdshell
--Set @SQL = '
--EXEC sp_configure ''xp_cmdshell'', 0
--RECONFIGURE
--EXEC sp_configure ''show advanced options'', 0
--RECONFIGURE
--'
----Print(@SQL)
--Exec(@SQL)


END




/*
tsp_export_CustomObj

Select * From CRW_SysMaster.dbo.VersionControl_SQL Order By LastModifiedDate DESC

Truncate Table CRW_SysMaster.dbo.VersionControl_SQL

CRW_SysMaster.dbo.tsp_ExportVersionControl_SQL

CRW_SysMaster.dbo.tsp_ExportVersionControl_SQL @LastActive=1

*/















GO
