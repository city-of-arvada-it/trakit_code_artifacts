USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GblAENotes]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_GblAENotes]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GblAENotes]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  					  
					  					  					  
/*
select * from Prmry_Notes

Insert Into Prmry_Notes(UserID,ActivityNo,SubGroupRecordID,DateEntered,ActivityGroup,SubGroup,Notes)
Select UserID,ActivityNo,SubGroupRecordID,DateEntered,ActivityGroup,SubGroup,Notes
From Notes

tsp_GblAENotes @NoteID=2,@UserID='mdp',@Notes='TEST - MDP(4/21/2011 10:30 AM OTG)  Kroger Store paid $60.00 renewal fee in 2010 for permit to expire in 2012. $30.00 fee for permit to expire in 2011 and $30.00 fee for permit to expire in 2012.'

tsp_GblAENotes @NoteID=0,@UserID='mdp',@ActivityGroup='PERMIT',@ActivityNo='SWTR2012-0001',@SubGroup='',@SubGroupRecordID='',
@Notes='TEST2 - MDP(4/21/2011 10:30 AM OTG)  Kroger Store paid $60.00 renewal fee in 2010 for permit to expire in 2012. $30.00 fee for permit to expire in 2011 and $30.00 fee for permit to expire in 2012.'

*/


CREATE Procedure [dbo].[tsp_GblAENotes](
@NoteID INT,
@UserID Varchar(6),
@ActivityGroup Varchar(50) = Null,
@ActivityNo Varchar(60) = Null,
@SubGroup Varchar(50) = Null,
@SubGroupRecordID Varchar(30) = Null,
@Notes Varchar(7500)
)

As

Declare @SQL Varchar(8000)

If @ActivityGroup = '' Set @ActivityGroup = Null
If @ActivityNo = '' Set @ActivityNo = Null
If @SubGroup = '' Set @SubGroup = Null
If @SubGroup = 'CHRONOLOGY' Set @SubGroup = 'ACTION'
If @SubGroupRecordID = '' Set @SubGroupRecordID = Null

Declare @ActivityRecordID Varchar(40)
Select @ActivityRecordID = RecordID From tvw_GblActivities Where ActivityNo = @ActivityNo
If @ActivityRecordID Is Null
	Select @ActivityRecordID = RecordID From Geo_Ownership Where RecordID = @ActivityNo
If @ActivityRecordID Is Null
	Select @ActivityRecordID = RecordID From CRM_Issues Where Convert(Varchar,Issue_ID) = @ActivityNo


If @NoteID = 0
 Begin
	Insert Into Prmry_Notes(UserID,DateEntered,ActivityGroup,ActivityRecordID,ActivityNo,SubGroup,SubGroupRecordID,Notes)
	Values(
	@UserID,
	GetDate(),
	@ActivityGroup,
	@ActivityRecordID,
	@ActivityNo,
	@SubGroup,
	@SubGroupRecordID,
	@Notes
	)
 End

If @NoteID > 0
 Begin
	Update Prmry_Notes
	Set 
	UserID = @UserID,
	DateEntered = GetDate(),
	Notes = @Notes
	WHERE NoteID = @NoteID
 End
 
 
 
 
 












GO
