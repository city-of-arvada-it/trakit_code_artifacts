USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[ALP_GetBatchInitInfo]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[ALP_GetBatchInitInfo]
GO
/****** Object:  StoredProcedure [dbo].[ALP_GetBatchInitInfo]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- ==============================================================================
-- Author:		Erick Hampton
-- Revised:		Dec 23, 2016
-- Description:	Gets the Mode, ParameterSetID, and SharedSearchID for a batch
-- ==============================================================================
CREATE PROCEDURE [dbo].[ALP_GetBatchInitInfo]

	@BatchID int = -1,
	@IsUpdateMode bit = 0 out,
	@ParameterSetID int = -1 out,
	@SharedSearchID int = -1 out

AS
BEGIN

	select @IsUpdateMode=IsUpdateMode, @ParameterSetID=ParameterSetID, @SharedSearchID=SharedSearchID
	from [ALP_Batches]
	where ID=@BatchID

END

GO
