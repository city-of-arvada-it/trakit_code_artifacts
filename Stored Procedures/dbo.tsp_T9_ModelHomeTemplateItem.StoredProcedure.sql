USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ModelHomeTemplateItem]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_ModelHomeTemplateItem]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ModelHomeTemplateItem]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  
CREATE PROCEDURE [dbo].[tsp_T9_ModelHomeTemplateItem] (
       @BuilderRecID varchar(10)  = '1', 
	   @TemplateRecID varchar(10)  = ''

)

AS
BEGIN

Declare @SQL Varchar(8000)

	if @TemplateRecID <> ''
		Begin
			
			SELECT RecordID, BuilderRecordID, TemplateName, ORDERID FROM Prmry_ModelHomeTemplateItem WHERE	
			(BuilderRecordID =  @BuilderRecID  and recordid = @TemplateRecID ) order by TemplateName , orderid

			/*set @SQL = 'SELECT RecordID, BuilderRecordID, TemplateName, ORDERID FROM Prmry_ModelHomeTemplateItem WHERE	
										(BuilderRecordID = ' + @BuilderRecID + ' and recordid = ' + @TemplateRecID + ') order by TemplateName , orderid'

			print(@sql)
			exec(@sql)*/

		end

	else
		
		Begin
			
			SELECT RecordID, BuilderRecordID, TemplateName, ORDERID FROM Prmry_ModelHomeTemplateItem WHERE
										(BuilderRecordID =  @BuilderRecID ) order by TemplateName , orderid

			/*set @SQL = 'SELECT RecordID, BuilderRecordID, TemplateName, ORDERID FROM Prmry_ModelHomeTemplateItem WHERE
										(BuilderRecordID = ' + @BuilderRecID + ') order by TemplateName , orderid'

			print(@sql)
			exec(@sql)*/

		End


END





GO
