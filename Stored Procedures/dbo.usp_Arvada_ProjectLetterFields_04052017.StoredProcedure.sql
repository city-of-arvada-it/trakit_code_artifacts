USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_ProjectLetterFields_04052017]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[usp_Arvada_ProjectLetterFields_04052017]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_ProjectLetterFields_04052017]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[usp_Arvada_ProjectLetterFields_04052017]
	@projectno varchar(30)
as
/**********************************************************************************************************************
* Creator:      	 
* Date:         	 
* Title:			 
* Description:  	 
* Examples:    
			EXECUTE dbo.usp_Arvada_ProjectLetterFields 'DG2016-0008' 
			 
* Modifications:  	
  Developer Name		Date		Description
  ------------------	----------	----------------------------------------------------------- 
*
**********************************************************************************************************************/

set nocount on
 
 --------------------------
 --- Declare Variables   --
 --------------------------
 declare	
	@appliedDate datetime,
	@ProjectList varchar(max),
	@TextBottom nvarchar(max),
	@TextTop nvarchar(max),
	@plannerFull nvarchar(50),
	@plannerFirst nvarchar(50),
	@ComplDate varchar(10),
	@AccDate varchar(10),
	@appName nvarchar(50)
 
 --------------------------
 --- Create Temp Tables  --
 --------------------------

create table #projectsList
(
	project_no varchar(30),
	levelValue int,
	applied datetime
)
	 
 --------------------------
 --- Body of Procedure   --
 --------------------------

 

select 
	@appliedDate = m.applied,
	@plannerFull = m.planner,
	@plannerFirst = case when charindex(' ', m.planner) > 0  then
			substring(m.planner,1,  charindex(' ', m.planner)-1)
			else isnull(m.planner,'')
			end,
	--@ComplDate = convert(Varchar(10),m.STATUS_DATE,101),
	@AccDate = convert(Varchar(10),m.APPLIED,101),
	@appName = app.NAME
from dbo.project_main m
left join dbo.Project_People app with (nolock)
	on app.PROJECT_NO = m.PROJECT_NO
	and app.NAMETYPE = 'applicant'
where m.PROJECT_NO = @projectno

select top 1 @ComplDate = convert(Varchar(10), action_date,101)
from dbo.Project_Actions a
where a.action_Type = 'finalized initial review'
and a.PROJECT_NO  = @projectno
order by 
	ACTION_DATE desc

 

insert into #projectsList
(
	project_no ,
	levelValue ,
	applied 
)
select
	project_no ,
	levelValue ,
	applied 
from dbo.[tfn_Arvada_LinkedProjects](@projectno)
where 
	datediff(Day,applied,@appliedDate) = 0

select @ProjectList =
 stuff(
			 (select ', '+ p.project_no 
			  from #projectsList as p  
			  for xml path(''), root('MyString'), type 
			  ).value('/MyString[1]','nvarchar(max)') ,1,2,''
		) 


set @TextBottom =
N'for this application.  However, larger or more complex projects may exceed this timeframe.  <br></br>
<br></br>
Your project planner will consolidate review comments from all internal departments and external review agencies and provide a review letter to you via the email address provided as part of your application.<br></br>
<br></br>
Once you have received review comments, please feel free to contact your project planner with any questions you may have or if you would like to schedule a meeting to discuss the comments.  All calls and emails will be returned within 24 hours.   <br></br>
<br></br>
Typically, more than one review is required prior to scheduling of public hearings or administrative approval of an application.  However, we do strive to complete our review of projects within two submittals and have a City strategic goal of achieving this result on 60 percent of all applications.  Your role in achieving this goal is vital.<br></br>
<br></br>
Each City review will be completed, and comments provided to you, approximately four weeks after your subsequent submittal.  Any required public hearings regarding your application will be scheduled following the review of your final submission.  All submissions will need to be complete and address each of the City review comments provided regarding the previous submittal.  The City reserves the right to reject any submission that is determined to be incomplete or if significant comments are not adequately addressed.<br></br>
<br></br>
Please remember that all written notice of public hearings must be sent to all property owners within 400 feet of the subject property at least 12 days prior to the hearing date. The site will also need to be posted 15 days prior to the hearing date.  Additionally, at least 30 days prior to the public hearing, written notice of the application must be mailed to any owner of mineral rights associated with the subject property, pursuant to Colorado Revised Statute 24-65.5-103.   These notifications are your responsibility and the lack of proper notification will cause the public hearing date to be postponed.  It is important that you obtain an updated list of adjacent property owners from the county before the notices are sent out.  The posting sign(s) will be provided to you by the City, but it is your responsibility to verify that the sign(s) remain in place prior to the hearing in accordance with the Land Development Code requirements.<br></br>
<br></br>
For additional information regarding your application or the development review process, please feel free to contact your project planner.   Additionally, you can follow the progress on your project through the City’s online permit and project tracking system “eTRAKiT”.  Instructions on creating an account and using the system are located at: <a href="https://arvada.org/business/development/development-projects"> https://arvada.org/business.development/development-projects</a>. If at any time you have concerns about the timing or content of our reviews or any other questions, you may also call me at 720-898-7345.<br></br>
<br></br>
We look forward to working with you.<br></br>
<br></br>
<br></br>
Sincerely,<br></br>
<br></br>
<br></br>
<br></br>
Rob Smetana, AICP<br></br>
Manager of City Planning and Development
'

set @TextTop = 
N'
Dear Click here to enter applicant name,<br></br>
<br></br>
The Arvada City Planning and Development Division has accepted your development application and has assigned it to Click here to enter planner full name, who will be your project planner.  Click here to enter planner first name will be responsible for processing your application and guiding you through the development review process, and can be reached at 720-898-7435.  <br></br>
<br></br>
The application was accepted on Click here to enter acceptance date.  The City’s initial review comments on your application are typically made available to you four weeks after your submittal, or Click here to enter anticipated comment completion date '


set @TextTop = replace(@TextTop,'Click here to enter applicant name',isnull(@appName,''))
set @TextTop = replace(@TextTop,'Click here to enter planner full name',isnull(@plannerFull,''))
set @TextTop = replace(@TextTop,'Click here to enter planner first name',isnull(@plannerFirst,''))
set @TextTop = replace(@TextTop,'Click here to enter acceptance date',isnull(@AccDate,''))
set @TextTop = replace(@TextTop,'Click here to enter anticipated comment completion date',isnull(@ComplDate,''))

select 
	@ProjectList as ProjectList,
	m.project_no,
	app.NAME, 
	m.project_name,
	ltrim(rtrim(
	case when app.ADDRESS1 is not null and app.ADDRESS1 != '' then  app.ADDRESS1 else '' end +  
	case when app.ADDRESS2 is not null and app.ADDRESS2 != '' then  case when isnull(app.ADDRESS1,'') != '' then char(10) + char(13) else '' end + app.ADDRESS2 else '' end +  
	case when app.CITY is not null and app.CITY != '' then char(10) + char(13) + app.CITY else '' end +
	case when app.[STATE] is not null and app.[STATE] != '' then  ', ' + app.[STATE] else '' end +
	case when app.ZIP is not null and app.ZIP != '' then  ', ' + app.ZIP else '' end))  as ApplicantFullAddress,
	m.planner,

	case when charindex(' ', m.planner) > 0  then
			substring(m.planner,1,  charindex(' ', m.planner)-1)
			else m.planner
			end as PlannerFirstName,
	getdate() as TodaysDate,
	@TextBottom as TextBottom,
	@TextTop as TextTop,
	@TextTop + @TextBottom as FullTextValue,
	m.SITE_ADDR,


	
	app.NAME + char(10) + char(13) +
	ltrim(rtrim(
	case when app.ADDRESS1 is not null and app.ADDRESS1 != '' then  app.ADDRESS1 else '' end +  
	case when app.ADDRESS2 is not null and app.ADDRESS2 != '' then  case when isnull(app.ADDRESS1,'') != '' then char(10) + char(13) else '' end  + app.ADDRESS2 else '' end +  
	case when app.CITY is not null and app.CITY != '' then char(10) + char(13) + app.CITY else '' end +
	case when app.[STATE] is not null and app.[STATE] != '' then  ', ' + app.[STATE] else '' end +
	case when app.ZIP is not null and app.ZIP != '' then  ', ' + app.ZIP else '' end))  as ApplicantFullAddressWithName,

	@ProjectList + char(10) + char(13) +
	m.project_name + char(10) + char(13) +
	case when m.SITE_ADDR is not null and m.SITE_ADDR != '' then  m.SITE_ADDR else '' end +  
	case when m.SITE_CITY is not null and m.SITE_CITY != '' then char(10) + char(13) + m.SITE_CITY else '' end +
	case when m.SITE_STATE is not null and m.SITE_STATE != '' then  ', ' + m.SITE_STATE else '' end +
	case when m.SITE_ZIP is not null and m.SITE_ZIP != '' then  ', ' + m.SITE_ZIP else '' end  as SiteFullAddress


from dbo.project_main m with (nolock)
left join dbo.Project_People app with (nolock)
	on app.PROJECT_NO = m.PROJECT_NO
	and app.NAMETYPE = 'applicant'
where
	m.project_no = @projectno


 
GO
