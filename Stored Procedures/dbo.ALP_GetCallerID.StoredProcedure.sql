USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[ALP_GetCallerID]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[ALP_GetCallerID]
GO
/****** Object:  StoredProcedure [dbo].[ALP_GetCallerID]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================================================================================================
-- Author:		Erick Hampton
-- Create date: Dec 27, 2016
-- Description:	Returns the ID from the Callers table that matches the input parameters
--				If there is no match, a new row is inserted for the criteria before returning its ID
-- ==============================================================================================================================================================================
CREATE PROCEDURE [dbo].[ALP_GetCallerID]

	@System varchar(255) = '',
	@Method varchar(2000) = '',
	@CallerID int = -1 out

AS
BEGIN

	-- fail if all data not supplied
	if @System is null or @System = '' or @Method is null or @Method = ''
	begin
		-- we need both @System and @Method
		raiserror('Not all info was supplied to ALP_GetCallerID',16,1) with nowait
	end
	else
	begin
		-- we have what we need to proceed
		select top 1 @CallerID = [ID] from [ALP_Callers] where [System]=@System and [Method]=@Method
		if @CallerID is null or @CallerID = -1
		begin
			-- need to add this one
			insert into [ALP_Callers] ([System], [Method]) values (@System, @Method)
			set @CallerID = SCOPE_IDENTITY()
		end
	end

END

GO
