USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_Sys_Data_Step2_MissingColumns]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_Sys_Data_Step2_MissingColumns]
GO
/****** Object:  StoredProcedure [dbo].[tsp_Sys_Data_Step2_MissingColumns]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  
					  

-- =====================================================================
-- Revision History:
--	mdm - 12/02/13 - added revision history	
--	mdm - 11/19/13 - added to source safe
-- =====================================================================   

CREATE PROCEDURE [dbo].[tsp_Sys_Data_Step2_MissingColumns] (
@OutputPath Varchar(1000) = Null
)

AS
BEGIN
  SET NOCOUNT ON

--mdm - 08/15/13; sample @OutputPath:  c:\mayo\Step2_DS_AddMissingColumns.sql


----mdm - 08/14/13 - May need to enable xp_cmdshell
---- To allow advanced options to be changed.
--EXEC sp_configure 'show advanced options', 1
--GO
---- To update the currently configured value for advanced options.
--RECONFIGURE
--GO
---- To enable the feature.
--EXEC sp_configure 'xp_cmdshell', 1
--GO
---- To update the currently configured value for this feature.
--RECONFIGURE
--GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[zColumnTemp]') AND type in (N'U'))
BEGIN
create table zColumnTemp
(
SqlToExec varchar(2000)
)
END

truncate table zColumnTemp

insert into zColumnTemp
Select 'IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_NAME = ''' + isc.TABLE_NAME + ''') and NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_NAME = ''' + isc.TABLE_NAME + '''
AND COLUMN_NAME = ''' + isc.COLUMN_NAME + ''')   
  ALTER TABLE ' + isc.TABLE_NAME + ' ADD [' + isc.COLUMN_NAME + '] ' + isc.data_type + '' + 
  (case isc.data_type
       when 'char' then ' ('
              when 'nchar' then ' ('
                     when 'varbinary' then ' ('
                           when 'varchar' then ' ('
                           else '' end)
                           +
(case isc.data_type
       when 'char' then cast(isc.CHARACTER_MAXIMUM_LENGTH as varchar)
              when 'nchar' then cast(isc.CHARACTER_MAXIMUM_LENGTH as varchar)
                    when 'varbinary' then case when isc.CHARACTER_MAXIMUM_LENGTH =  -1 then 'MAX' else cast(isc.CHARACTER_MAXIMUM_LENGTH as varchar) end
                           when 'varchar' then case when isc.CHARACTER_MAXIMUM_LENGTH =  -1 then 'MAX' else cast(isc.CHARACTER_MAXIMUM_LENGTH as varchar) end
                           else '' end)
                                                       +
  (case isc.data_type
       when 'char' then ')'
              when 'nchar' then ')'
                     when 'varbinary' then ')'
                           when 'varchar' then ')'
                           else '' end)                                                                             
                           +
         (case when isc.IS_NULLABLE = 'No' then 
		 
		 (case when isc.data_type = 'int' and sc.is_identity = 1 then ' identity (1,1) NOT' else '' end) else '' end ) + ' NULL;' 
        + ' '
from INFORMATION_SCHEMA.COLUMNS isc left join sys.columns sc
 on  isc.TABLE_NAME = object_name(sc.object_id) and isc.COLUMN_NAME = sc.name
left join
    information_schema.table_constraints tc
on  tc.Table_name               =  isc.TABLE_NAME
AND tc.Constraint_Type  = 'PRIMARY KEY'
cross apply

    (select '[' + Column_Name + '], '
     FROM       information_schema.key_column_usage kcu
     WHERE      kcu.Constraint_Name     = tc.Constraint_Name
     ORDER BY
        ORDINAL_POSITION
     FOR XML PATH('')) j (list)
       where isc.table_name not like 'z%' and isc.table_name not like 'tvw%'
order by  isc.TABLE_NAME, isc.COLUMN_NAME


declare @FileName as varchar(1000)
If @OutputPath Is Null OR @OutputPath = ''
	Set @FileName = 'C:\crw\TEMP\DataStructure_Columns.sql'
ELSE
	Set @FileName = @OutputPath
--set @FileName = 'c:\mayo\Step2_DS_AddMissingColumns.sql'
--select @FileName

declare @dbName varchar(200)
set @dbName = db_name()
--select @dbName


declare @cmd varchar(2000)
--set @cmd = 'bcp "select SqlToExec from SanClemente2012.dbo.zColumnTemp order by SqlToExec" queryout "'
set @cmd = 'bcp "select SqlToExec from '
set @cmd = @cmd + @dbname + '.dbo.zColumnTemp order by SqlToExec" queryout "'
set @cmd = @cmd + @FileName + '" -Utrakit -Ptrakit -c -S' + @@SERVERNAME

--select @cmd
exec xp_cmdshell @cmd

select SqlToExec from zColumnTemp order by SqlToExec


--drop table zColumnTemp

---- Disable xp_cmdshell
--Set @SQL = '
--EXEC sp_configure ''xp_cmdshell'', 0
--RECONFIGURE
--EXEC sp_configure ''show advanced options'', 0
--RECONFIGURE
--GO


END










GO
