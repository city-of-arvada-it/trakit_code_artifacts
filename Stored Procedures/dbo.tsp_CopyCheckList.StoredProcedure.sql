USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_CopyCheckList]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_CopyCheckList]
GO
/****** Object:  StoredProcedure [dbo].[tsp_CopyCheckList]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  
create procedure [dbo].[tsp_CopyCheckList]
	@FromParentRecordID varchar (50),
	@ToParentRecordID varchar(50),
	@FromSubRecordID varchar(50),
	@ToSubRecordID varchar(50),
	@TypeOfCopy varchar(1)
	as
	Begin

		-- RB Drop table if it exists
		IF OBJECT_ID('tempdb..#tmpCopy') IS NOT NULL  
			drop table #tmpCopy

		Create Table #tmpCopy (
			RecordID varchar(50) null,
			SubRecordID varchar(50) null,
			ItemValue int not null,
			CheckListID int not null,
			UserID varchar(50) null,
			CheckDate datetime null,
			onetimeItemText varchar(100) null,
			OldCheckValueID int)

		-- RB Drop table if it exists
		IF OBJECT_ID('tempdb..#tmpCopyNotes') IS NOT NULL  
			drop table #tmpCopyNotes

		Create Table #tmpCopyNotes(
			NoteID int identity(1,1),
			CheckListValueID int,
			recordID varchar(50), 
			subRecordID varchar(50),
			Note varchar(maX),
			userID varchar(50),
			lastUpdate datetime)
			
		Declare @CheckListValue table(NewCheckListValueID int null)
		

			Insert into #tmpCopy Select @ToParentRecordID, @ToSubRecordID, itemValue, 
			CheckListID, userID,  checkDate ,onetimeItemText, checkListValueID
			from CheckListValues C
			Where  C.RecordID = @FromParentRecordID And C.subRecordID = @FromSubRecordID 

		

			Insert Into CheckListValues (recordID, SubRecordID, ItemValue, CheckListID, userID,  checkDate, onetimeItemText, TmpCopyCheckValueID)
			output Inserted.CheckListValueID  as NewCheckListValueID into @CheckListValue			
			Select RecordID, SubRecordID, ItemValue, CheckListID, userID,  CheckDate, onetimeItemText, OldCheckValueID  from #tmpCopy


			/*Insert into #tmpCopyNotes (CheckListValueID, recordID, SubRecordID, Note, UserID, LastUpdate)
			Select (Select CV.CheckListValueID from CheckListValues CV Where oneTimeItemText = C.CheckListValueID)  ,
			@ToParentRecordID, @ToSubRecordID, Note, C.UserID, LastUpdate from CheckListNotes C 
			join #tmpCopy TC on TC.OldCheckValueID = C.checkListValueID*/
			



			Insert into #tmpCopyNotes (CheckListValueID, recordID, SubRecordID, Note, UserID, LastUpdate)
			select CV.ChecklistValueID, @ToParentRecordID, @ToSubRecordID, CN.Note, CN.UserID, CN.LastUpdate
			from #tmpCopy TC join CheckListValues CV on CV.TmpCopyCheckValueID = TC.OldCheckValueID			
			join CheckListNotes CN on TC.OldCheckValueID = CN.checkListValueID

			
			insert into CheckListNotes (CheckListValueID, RecordID, SubRecordID, Note, UserID, LastUpdate)
			Select CheckListValueID, recordID, SubRecordID, Note, UserID, LastUpdate From #tmpCopyNotes

				
			Drop Table #tmpCopy
			Drop Table #tmpCopyNotes
			




	End



GO
