USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[ALP_GetOperationValuesConditionalColumns]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[ALP_GetOperationValuesConditionalColumns]
GO
/****** Object:  StoredProcedure [dbo].[ALP_GetOperationValuesConditionalColumns]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===============================================================================================
-- Author:		Rafael Buelna
-- Revised:		Apr 23, 2017
-- Description:	Get License2 UDF Columns
-- ===============================================================================================
CREATE PROCEDURE [dbo].[ALP_GetOperationValuesConditionalColumns]

	@SelectedSource Varchar(30)

AS
BEGIN
	if @SelectedSource = 'LICENSE DATA'
		Begin			
			select table_name + '.' + COLUMN_NAME as [column_name] from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='LICENSE2_MAIN' and column_name in ('STATUS', 'EXPIRED', 'ISSUED') order by COLUMN_NAME
		End
	Else if @SelectedSource = 'USER DEFINED FIELDS'
		Begin
			select table_name + '.' + COLUMN_NAME as [column_name] from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='LICENSE2_UDF' and column_name not in ('LICENSE_NO', 'RECORDID', 'TYPE') order by COLUMN_NAME
		End
END
GO
