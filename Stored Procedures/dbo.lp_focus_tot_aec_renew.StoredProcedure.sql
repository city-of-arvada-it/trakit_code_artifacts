USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[lp_focus_tot_aec_renew]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[lp_focus_tot_aec_renew]
GO
/****** Object:  StoredProcedure [dbo].[lp_focus_tot_aec_renew]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* 
--Ychavez 1/11/2014 - FOR FOCUS Measures report
-------------------------------------------------------------------------------------------
Total AEC issued (renewed)
Based on Issued date
AEC records : BID CONTRACTOR
exec lp_focus_tot_aec_renew @APPLIED_FROM = '02/01/2014' , @APPLIED_TO = '03/01/2014'
-------------------------------------------------------------------------------------------
*/
CREATE procedure [dbo].[lp_focus_tot_aec_renew]
		@APPLIED_FROM datetime = null, --issued
		@APPLIED_TO datetime = NULL ---issued
as
begin
---NOT finished000-----

SELECT  DISTINCT COUNT(ST_LIC_NO) AS num_of_permits 
FROM dbo.AEC_MAIN
WHERE ST_LIC_ISSUE BETWEEN @APPLIED_FROM AND @APPLIED_TO


end
GO
