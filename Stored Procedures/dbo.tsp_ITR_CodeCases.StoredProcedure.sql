USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ITR_CodeCases]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_ITR_CodeCases]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ITR_CodeCases]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  
-- =============================================
-- Author:		<Gilberto hubner>
-- Create date: <08/20/2014>
-- Description:	<Provide cases per page>
-- =============================================
CREATE PROCEDURE [dbo].[tsp_ITR_CodeCases]
@UserID Varchar(6),
@TotalRecords  INTEGER,
@RecordsPerPage INT,
@PageNum  INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.


select Top (@TotalRecords)  ROW_NUMBER() OVER(ORDER BY followup asc,started desc) AS RowNum,  [PREFIX]
      ,[YRMO]
      ,[SEQ_NO]
      ,[CASE_NO]
      ,[CASE_NAME]
      ,[STARTED]
      ,[STARTED_BY]
      ,[CLOSED]
      ,[CLOSED_BY]
      ,[LASTACTION]
      ,[LASTACTION_BY]
      ,[FOLLOWUP]
      ,[FOLLOWUP_BY]
      ,[RECEIVED_BY]
      ,[HOW_RECEIVED]
      ,[CaseType]
      ,[CaseSubType]
      ,[CASE_LOCATION]
      ,[SITE_APN]
      ,[SITE_STREETID]
      ,[SITE_NUMBER]
      ,[SITE_STREETNAME]
      ,[SITE_UNIT_NO]
      ,[SITE_CITY]
      ,[SITE_STATE]
      ,[SITE_ZIP]
      ,[SITE_LOT_NO]
      ,[SITE_BLOCK]
      ,[SITE_TRACT]
      ,[SITE_SUBDIVISION]
      ,[SITE_DESCRIPTION]
      ,[SITE_ADDR]
      ,[CODE_SECTION]
      ,[DESCRIPTION]
      ,[ASSIGNED_TO]
      ,[REFERRED_TO]
      ,[STATUS]
      ,[FEES_CHARGED]
      ,[FEES_PAID]
      ,[BALANCE_DUE]
      ,[PARENT_PROJECT_NO]
      ,[OTHER_DATE1]
      ,[OTHER_BY1]
      ,[OWNER_NAME]
      ,[RESIDENT_NAME]
      ,[COMPLAINANT_NAME]
      ,[LOCKID]
      ,[LOC_RECORDID]
      ,[DEPOSITTYPE]
      ,[HISTORICAL_APN]
      ,[PARENT_PERMIT_NO]
      ,[PARENT_BUS_LIC_NO]
      ,[REFERENCE_NO]
      ,[SITE_ALTERNATE_ID]
      ,[SITE_GEOTYPE]
      ,[TSstatus] INTO #tempCodeCases from case_main join dbo.Prmry_Users on UserName = ASSIGNED_TO where UserID = @UserID  and closed is NULL   order by followup asc,started desc
SELECT * 
  FROM #tempCodeCases
WHERE RowNum BETWEEN (@PageNum - 1) * @RecordsPerPage + 1 
                  AND @PageNum * @RecordsPerPage
                   order by followup asc,started desc;

END



GO
