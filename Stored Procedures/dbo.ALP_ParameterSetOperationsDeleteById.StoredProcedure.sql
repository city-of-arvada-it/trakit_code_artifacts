USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[ALP_ParameterSetOperationsDeleteById]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[ALP_ParameterSetOperationsDeleteById]
GO
/****** Object:  StoredProcedure [dbo].[ALP_ParameterSetOperationsDeleteById]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- ==========================================================================================
-- Revision History:
--	  2016-12-11 - RGB Initial
-- ==========================================================================================			


CREATE PROCEDURE [dbo].[ALP_ParameterSetOperationsDeleteById] (
		@InputParameterSetOperationsId int = Null
	
 )
 
AS
BEGIN

		Declare @iRwCnt int, 
				@ParameterSetOperationId int,
				@ParameterSetOperationsValuesId int,
				@OperationValueId int,	
				@Id int	 		

		-- Test Data
		--Declare @InputParameterSetOperationsId int =107
	
	
		Begin Try

		IF OBJECT_ID('tempdb..#TempOperations') IS NOT NULL
		  DROP TABLE #TempOperations
		  
		if @InputParameterSetOperationsId is not null 
			Begin
				If @InputParameterSetOperationsId <=0
					Begin
						Return
					End
			End	   	
	
		-- Put Requested Data into temp Table
		Select Id=IDENTITY(INT,1,1), APSO.id as ParameterSetOperationId, APSOV.id as ParameterSetOperationsValuesId, AOV.id as OperationValueId into #TempOperations from ALP_ParameterSetOperations APSO
		left join ALP_ParameterSetOperationValues APSOV on APSO.ParameterSetId = APSOV.ParameterSetId  and APSO.OperationId = APSOV.OperationId 
		left Join ALP_OperationValues AOV on APSOV.OperationValueId = AOV.id 		
		where APSO.id = @InputParameterSetOperationsId 

		set @iRwCnt = 0

		-- Get number of records returned
		set @iRwCnt = @@ROWCOUNT	

			-- Start Loop to go thru No Operation Values records
		While @iRwCnt > 0
			Begin

				-- Select one Record
				Select top 1 @Id = Id, @ParameterSetOperationId = ParameterSetOperationId, @ParameterSetOperationsValuesId = ParameterSetOperationsValuesId, @OperationValueId = OperationValueId 
				from #TempOperations

				-- Get Record Count
				set @iRwCnt = @@ROWCOUNT 

				-- If Record Count > 0 then there are parameter sets / Operations with no assigned Operation Values

				if @iRwCnt > 0
					Begin
					
						

					if @ParameterSetOperationsValuesId is not null 
						Begin							
							if  @ParameterSetOperationsValuesId > 0
								Begin
									delete from ALP_ParameterSetOperationValues where id = @ParameterSetOperationsValuesId  
								End
						End

					if @OperationValueId is not null
						Begin
							if @OperationValueId > 0 
								Begin
									delete from ALP_OperationValues where id = @OperationValueId
								End
						End																											
					
						delete from #TempOperations where id = @Id 
					End				
			End
							
			delete from ALP_ParameterSetOperations where Id = @InputParameterSetOperationsId		

		End Try
		Begin Catch
			-- If error then reset @iRwCnt to 0 and return 
			Set @iRwCnt = 0				
			Return
		End Catch
End


GO
