USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_SysRestoreDB_FTP]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_SysRestoreDB_FTP]
GO
/****** Object:  StoredProcedure [dbo].[tsp_SysRestoreDB_FTP]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  					  
					  					  					  
/*

tsp_SysRestoreDB_FTP 
@DBName='CRW_DayOld',
@DBFileName='CRW_NightlyBackup.bak',
@DBFileDirectory='C:\Databases\',
@FTPDirectory='c:\ftp\',
@BackupDirectory='C:\SQL Backups\Archives\',
@MDFFileName='SantaCruz',
@LDFFileName='SantaCruz_log'

tsp_SysRestoreDB_FTP 
@DBName='CRW_DEMO_DayOld',
@DBFileName='CRW_DEMO_NightlyBackup.bak',
@DBFileDirectory='C:\Databases\',
@FTPDirectory='c:\ftp\',
@BackupDirectory='C:\SQL Backups\Archives\',
@MDFFileName='SantaCruz',
@LDFFileName='SantaCruz_log'

tsp_SysRestoreDB_FTP 
@DBName='CRW_DEMO_DayOld',
@DBFileName='CRW_DEMO_NightlyBackup.bak',
@DBFileDirectory='C:\Databases\',
@FTPDirectory='c:\ftp\',
@BackupDirectory='C:\SQL Backups\Archives\',
@MDFFileName='SantaCruz',
@LDFFileName='SantaCruz_log'

Select * 
FROM   Master..SysProcesses
WHERE DB_NAME(DBId) = 'CRW_DEMO_DayOld'

ALTER DATABASE CRW_DEMO_DayOld SET OFFLINE WITH
ROLLBACK IMMEDIATE
GO

ALTER DATABASE CRW_DEMO_DayOld SET ONLINE


*/




CREATE Procedure [dbo].[tsp_SysRestoreDB_FTP](
@DBName Varchar(100),
@DBFileName Varchar(100),
@DBFileDirectory Varchar(500),
@FTPDirectory Varchar(300),
@BackupDirectory Varchar(300),
@MDFFileName Varchar(100),
@LDFFileName Varchar(100),
@ToggleXP_CMDShell INT = 1,
@CopyToArchivesOnly INT = 0
)

As

DECLARE @SQL Varchar(8000)
DECLARE @SPId VARCHAR(7000)

If @ToggleXP_CMDShell Is Null Set @ToggleXP_CMDShell = 1


--SELECT  @SPId = COALESCE(@SPId,'')+'KILL '+CAST(SPID AS VARCHAR)+'; '
--FROM   Master..SysProcesses
--WHERE DB_NAME(DBId) = @DBName
--and SPID <> @@SPID

--Set @SQL = 'Use Master 
--GO 
--' + @SPId + '
--GO 
--'
                
--PRINT(@SPId)
--EXEC(@SPId)

--RETURN

If @CopyToArchivesOnly = 0
 Begin
	BEGIN TRY
		Set @SQL = '
		Use Master
		ALTER DATABASE ' + @DBName + ' SET OFFLINE WITH
		ROLLBACK IMMEDIATE
		'
		Print(@SQL)
		Exec(@SQL)

		Print(@SQL)
		Exec(@SQL)
	END TRY

	BEGIN CATCH
		Print('DB Not Found to take offline')
	END CATCH
 End

declare @date varchar(30)
declare @fullfilename varchar(500)
declare @fullfilepath varchar(1000)
declare @cmd varchar(3000)
Set @date = convert(char(8),getdate(),112)
Set @fullfilename = @DBName + '_' + @date + '.bak'
Set @fullfilepath = '"' + @BackupDirectory + @fullfilename + '"'
Print(@fullfilename)
Print(@fullfilepath)


If @ToggleXP_CMDShell = 1
 Begin
	--Enable xp_cmdshell
	Set @SQL = '
	EXEC sp_configure ''show advanced options'', 1
	RECONFIGURE
	EXEC sp_configure ''xp_cmdshell'', 1
	RECONFIGURE
	'
	Print(@SQL)
	Exec(@SQL)
 End


set @cmd = 'Del ' + @fullfilepath 
print @cmd
Exec master..xp_cmdshell @cmd, NO_OUTPUT

set @cmd = 'Copy "' + @FTPDirectory + @DBFileName + '" ' + @fullfilepath 
print @cmd
Exec master..xp_cmdshell @cmd, NO_OUTPUT


If @ToggleXP_CMDShell = 1
 Begin
	-- Disable xp_cmdshell
	Set @SQL = '
	EXEC sp_configure ''xp_cmdshell'', 0
	RECONFIGURE
	EXEC sp_configure ''show advanced options'', 0
	RECONFIGURE
	'
	Print(@SQL)
	Exec(@SQL)
 End
 
 
If @CopyToArchivesOnly = 1
	RETURN


Declare @MDFFileDestination Varchar(500)
Declare @LDFFileDestination Varchar(500)

Set @MDFFileDestination = @DBFileDirectory + @DBName + '\' + @DBName + '.mdf'
Set @LDFFileDestination = @DBFileDirectory + @DBName + '\' + @DBName + '.ldf'

Set @fullfilepath = REPLACE(@fullfilepath,'"','')

RESTORE DATABASE @DBName FROM  DISK = @fullfilepath WITH  FILE = 1,  
MOVE @MDFFileName TO @MDFFileDestination,  
MOVE @LDFFileName TO @LDFFileDestination, NOUNLOAD,  REPLACE,  STATS = 10



Set @SQL = '
Use Master
ALTER DATABASE ' + @DBName + ' 
SET ONLINE
'
Print(@SQL)
Exec(@SQL)

Set @SQL = @DBName + '..sp_changedbowner ''trakit'''
Print(@SQL)
Exec(@SQL)












GO
