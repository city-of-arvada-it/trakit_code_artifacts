USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_GblCRMDirectLinkedActivities]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_GblCRMDirectLinkedActivities]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_GblCRMDirectLinkedActivities]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  					  
-- =====================================================================
-- Revision History:
--	RB - 3/31/2014

-- =====================================================================   	
CREATE Procedure [dbo].[tsp_T9_GblCRMDirectLinkedActivities](     
	   @IssueLabel_id	varchar(20)

)

As
 BEGIN

 Declare @SQLStatement varchar(500)
 Declare @NSQLStatement nvarchar(500)
 Declare @IssueId      int	
 Declare @IssueIdStr   varchar(30)
 Set @SQLStatement = null 
		
			 set @NSQLStatement = 'Select @IssueId = ISSUE_ID from CRM_Issues where ISSUE_LABEL = ' + CHar(39) + @IssueLabel_id + Char(39)		

			 execute sp_executesql @NSQLStatement, @params =  N'@IssueId int OUTPUT', @IssueId = @IssueId OUTPUT	
 
	         if @IssueId >  0
			    Begin

				   set @IssueIdStr = convert(varchar(30), @IssueId) 			  	
				 
				   set @SQLStatement=  'Select CL.issue_id,  CL.LINK_ID, CL.LINK_NO, CL.LINK_GROUP, CI.ISSUE_LABEL from CRM_Links CL left join CRM_ISSUES CI on CL.link_id = CI.issue_id where (CL.ISSUE_ID = ' + @IssueIdStr + ' or CL.LINK_ID = ' + @IssueIdStr + ') '
				   				   
				   EXEC(@SQLStatement)				
			  End	
 END




GO
