USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[lp_focus_80prct_fam_15days_review]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[lp_focus_80prct_fam_15days_review]
GO
/****** Object:  StoredProcedure [dbo].[lp_focus_80prct_fam_15days_review]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* 
--Ychavez 3/11/2014 - FOR FOCUS Measures report
-------------------------------------------------------------------------------------------
--80% of 1-2 family home plan reviews completed within 15 business days.--
exec lp_focus_80prct_fam_15days_review @APPLIED_FROM = '02/01/2014' , @APPLIED_TO = '03/01/2014'
-------------------------------------------------------------------------------------------
*/
CREATE procedure [dbo].[lp_focus_80prct_fam_15days_review]
		@APPLIED_FROM datetime = null,
		@APPLIED_TO datetime = null
as
begin

--------------------------------------------------------------------------------
--80% of 1-2 family home plan reviews completed within 15 business days.--
--CALCULATE BY APPLIED - DATE_RECEIVED
-----------------------------------------------------------------------------
--DROP TABLE #TMP_CALC_FAM_PLAN_TOTALS
SELECT DATEDIFF(DAY, APPLIED, DATE_RECEIVED) AS DATE_DIFF, APPLIED, DATE_RECEIVED, P.PERMIT_NO
, CASE WHEN DATEDIFF(DAY, APPLIED, DATE_RECEIVED) < 16  THEN 'WITHIN MEASURE' 
 WHEN  DATEDIFF(DAY, APPLIED, DATE_RECEIVED) >= 16 THEN 'OUT OF MEASURE'
 ELSE 'NOT CALCULATED'
 END AS MEASURE
INTO #TMP_CALC_FAM_PLAN_TOTALS
 FROM PERMIT_MAIN P
JOIN PERMIT_REVIEWS R ON P.PERMIT_NO = R.pERMIT_NO AND REVIEWTYPE = 'BUILDING PERMIT'
WHERE APPLIED BETWEEN  @APPLIED_FROM and @APPLIED_TO --SAME AS APPLIED DATE
AND PERMITSUBTYPE IN ('NEW SF DETACHED', 'NEW SF ATTACHED', 'NEW DUPLEX')
ORDER BY APPLIED
-----------------------------------------------------------------------------------------
--DROP TABLE #tmp_calc_measure_tot
-----------------------------------------------------------------------------------------
select SUM(case when MEASURE= 'WITHIN MEASURE' THEN 1 ELSE 0 END) IN_MEASURE_COUNT
 ,SUM(case when MEASURE = 'OUT OF MEASURE' THEN 1 ELSE 0 END) OUT_OF_MEASURE_COUNT
 ,SUM(CASE WHEN PERMIT_NO LIKE '%-%' THEN 1 END) TOTAL_PERMITS
 into #tmp_calc_measure_tot
  from #TMP_CALC_FAM_PLAN_TOTALS
  where MEASURE != 'NOT CALCULATED'
  
 -----------------------------------------------------------------------------------------
--
-----------------------------------------------------------------------------------------

select SUM(cast(IN_MEASURE_COUNT as decimal)) / Sum(cast(total_permits as decimal)) * 100 
as prcnt_plan_review15, total_permits, IN_MEASURE_COUNT
from #tmp_calc_measure_tot
group by total_permits, IN_MEASURE_COUNT


end
GO
