USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearchSave]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_AdvSearchSave]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearchSave]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  

CREATE PROCEDURE [dbo].[tsp_T9_AdvSearchSave] (
       @RecordID as varchar(10) = null,
          @Group as varchar(50),
          @FilterName as varchar(50),
          @UserName as varchar(50),
          @SearchField as varchar(50),
          @ShowAdditionalFields as varchar(5),
              @IsTemplate varchar(1),
              @IsDefault varchar(1),
              @UserID varchar(50),
          @Detail as varchar(8000) 
          )

AS
BEGIN

Declare @SQL Varchar(8000)
Declare @ValuesClause varchar(8000)
Declare @WhereClause varchar(8000)
Declare @ParentRecordID varchar(50)

      -- determine if the recordid is 0 AND the filter exists for this user and group
      IF CAST(@RecordID as int) = 0
      BEGIN
            SET @RecordID = ISNULL((SELECT top 1 recordid from Prmry_AdvSearchConfigItem where [GROUP] = @Group AND FilterName = @FilterName and UserName = @UserName),0)
      END

       --Upate if exists
       IF cast(@RecordID as int) > 0 

              begin

                     ----error trapping example
                     --IF (ISNULL(@Group, 0) = 0)
                     --BEGIN
                     --    RAISERROR('Invalid parameter: Group cannot be NULL or zero', 18, 0)
                     --    RETURN
                     --END

                     --IF (ISNULL(@Group, '') = '')
                     --BEGIN
                     --    RAISERROR('Invalid parameter: Group cannot be NULL or empty', 18, 0)
                     --    RETURN
                     --END

                              
                     set @SQL = 'Update Prmry_AdvSearchConfigItem 
                           set 
                           [Group] = ''' + @group 
                           + ''', FilterName = ''' + @FilterName 
                           + ''', UserName= ''' + @UserName 
                           + ''', SearchField= ''' + @SearchField 
                           + ''', ShowAdditionalFields= ''' + @ShowAdditionalFields
                                       + ''', IsTemplate= ''' + @IsTemplate
                                       + ''', IsDefault= ''' + @IsDefault
                           + ''' where recordid = ' + @RecordID 
                     

                     exec(@sql)

                     -- Update detail 
                               exec tsp_T9_AdvSearchSaveDetail @recordid, @Detail
                                                    
              end

       else

              begin
                    
                               --create value clause
                     set @ValuesClause  = '''' + @Group + ''',''' + @FilterName + ''',''' + @UserName + ''',''' + @SearchField + ''',''' + @ShowAdditionalFields + ''',''' + @IsTemplate + ''''

                              --Add new record
                     set @SQL = 'INSERT INTO Prmry_AdvSearchConfigItem
                                                ([Group], FilterName, UserName, SearchField, ShowAdditionalFields, IsTemplate)
                     VALUES
                     (' + @ValuesClause + ')'

                     exec(@sql)

                              --Get new parentrecordid
                                                     
                              --if exists (select  * from tempdb.dbo.sysobjects o where o.xtype in ('U') 
                              --   and o.id = object_id(N'tempdb.. #RecID')
                              --)
                              --DROP TABLE #RecID;


							  --RB Check if table exists before creating it
								IF OBJECT_ID('tempdb..#RecID') IS NOT NULL  
									drop table #RecID

                              Create Table  #RecID
                              (Recordid Varchar(10))

                              set @whereclause = 'where [Group] = ''' + @Group + ''' and Filtername = ''' + @FilterName + ''' and Username = ''' + @UserName + ''''

                              set @SQL = 'Insert into #RecID (Recordid)
                                       select Recordid from Prmry_AdvSearchConfigItem ' + @whereclause + '' 
     
                        exec(@sql)

                        set @ParentRecordID = (select * from #RecID)

                        select @ParentRecordID

                        -- Update detail 
                        exec tsp_T9_AdvSearchSaveDetail @ParentRecordID, @Detail

              end

            begin
                  if @IsDefault = 1
                        set @SQL = 'Update Prmry_AdvSearchConfigItem set IsDefault = ''0'' where (UserName = ''' + @UserName + ''' and [Group] = ''' + @group + ''' and RecordID <> ''' + @RecordID + ''')'

                        --print(@sql)    
                exec(@sql)

                        if @UserName = '<SYSTEM DEFAULT>'
                        set @SQL = 'Update Prmry_AdvSearchConfigItem set IsDefault = ''0'' where (UserName = ''' + @Userid + ''' and [Group] = ''' + @group + ''' and RecordID <> ''' + @RecordID + ''')'

                        --print(@sql)    
                exec(@sql)


            end

            set @SQL = 'Update Prmry_AdvSearchConfigItem set IsDefault = ''0'' where UserName = ''<SYSTEM DEFAULT>'''

            --print(@sql)    
        exec(@sql)

End





--**************************************************************************************************************************

--**************************************************************************************************************************
--*** mdm - 03/25/15 - For MJ
SET ANSI_NULLS ON


GO
