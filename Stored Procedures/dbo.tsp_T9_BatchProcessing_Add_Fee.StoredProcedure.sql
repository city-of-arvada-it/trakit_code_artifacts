USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_BatchProcessing_Add_Fee]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_BatchProcessing_Add_Fee]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_BatchProcessing_Add_Fee]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  




CREATE PROCEDURE [dbo].[tsp_T9_BatchProcessing_Add_Fee]
       
       @ListFeeCodes as UpdateFields readonly,
       @ListUpdateFields as UpdateFields readonly,
       @ListTableAndRecords as MainTableRecords readonly
       
       --@FEECODE_TO_ADD AS varchar(30),
       --@ASSESSED_BY AS varchar(6),
       --@RECORDPREFIX AS varchar(6),
       --@REVIEW_TABLES AS INT


AS
BEGIN
       -- SET NOCOUNT ON added to prevent extra result sets from
       -- interfering with SELECT statements.
       SET NOCOUNT ON;


DECLARE @SCHEDULE_EFFECTIVE_DATE as DATETIME
DECLARE @EFFECTIVE_FEE_SCHEDULE as varchar(200)
DECLARE @FEE_PARENT as nvarchar(30)
DECLARE @FEE_AMOUNT as float
       
Declare @SQL Varchar(8000)
Declare @TableName varchar(50) 
Declare @UDFTableName varchar(50) 
Declare @ColumnName varchar(50) 
Declare @ColumnValue varchar(255)
Declare @RowNum int
Declare @RowId Int
Declare @UserID as varchar(10)
Declare @TblName as varchar(100)
Declare @KeyField varchar(50)  
Declare @Contact as varchar(100)
Declare @GroupName as varchar(255)

--Drop Tables

IF object_id('tempdb..#UDFFields') is not null drop table #UDFFields
IF object_id('tempdb..#ListFeeCodes') is not null drop table #ListFeeCodes
IF object_id('tempdb..#ForInsertFees') is not null drop table #ForInsertFees
IF object_id('tempdb..#ForInsertSubFees') is not null drop table #ForInsertSubFees
IF object_id('tempdb..#ForInsertIntoPrmryAuditTrail') is not null drop table #ForInsertIntoPrmryAuditTrail
IF object_id('tempdb..#ListFeeTypes') is not null drop table #ListFeeTypes
IF object_id('tempdb..#ListUpdateFields') is not null drop table #ListUpdateFields
IF object_id('tempdb..#MainTableRec') is not null  drop table #MainTableRec

--Create tables
Create Table #ForInsertFees(
        ACTIVITY_NO varchar(15),     
        CODE varchar(12),
        DESCRIPTION varchar(60),
        ACCOUNT varchar(30),
        FORMULA varchar(2000),
        QUANTITY float,
        AMOUNT float,
        DETAIL varchar(1),
        PAID varchar(1),
        COMMENTS varchar(200),
        ASSESSED_DATE datetime,
        ASSESSED_BY varchar(6),
        FEETYPE varchar(8),
        DEPOSITID varchar(30),
        PAY_BY_DEPOSIT varchar(1),
        RECORDID varchar(30),
        Formula_working_col varchar(1024),
        Calc_value float,
        ForRecID int identity)

CREATE TABLE #ForInsertSubFees(
        ACTIVITY_NO varchar(15),   
        CODE varchar(12),
        ITEM varchar(12),
        DESCRIPTION varchar(60),
        QUANTITY float,
        FORMULA varchar(2000),
        AMOUNT float,
        ACCOUNT varchar(30),
        COMMENTS varchar(200),
        PARENTID varchar(30),
        ASSESSED_DATE datetime,
        ASSESSED_BY varchar(6),
        SUBFEETYPE varchar(8),
        RECORDID varchar(30),
        Formula_working_col varchar(1024),
        Calc_value float, 
        ForRecID int identity)
                     
--insert into @ListTableAndRecords(tblName, ActivityNo) values ('PERMIT_Main','BELC2015-0373')
--insert into @ListTableAndRecords(tblName, ActivityNo) values ('PERMIT_Main','BELC2015-0372')
--insert into @ListTableAndRecords(tblName, ActivityNo) values ('PERMIT_Main','BELC2015-0371')
--insert into @ListTableAndRecords(tblName, ActivityNo) values ('PERMIT_Main','BELC2015-0370')

--insert into @ListUpdateFields(RowID,ColumnName,Value) values (1,'UserID','mdm')
--insert into @ListUpdateFields(RowID,ColumnName,Value) values (2,'groupname','PERMITS')
--insert into @ListUpdateFields(RowID,ColumnName,Value) values (3,'tblName','Permit_FEES')
--insert into @ListUpdateFields(RowID,ColumnName,Value) values (4,'ASSESSED_DATE','07/22/58')
--insert into @ListUpdateFields(RowID,ColumnName,Value) values (5,'ASSESSED_BY','CRW')
--insert into @ListUpdateFields(RowID,ColumnName,Value) values (6,'UDFtblName','Permit_UDF')
       
--insert into @ListFeeCodes(RowID,ColumnName,Value) values (0,'CODE','STRM0001')
--insert into @ListFeeCodes(RowID,ColumnName,Value) values (1,'CODE','BLD018')
--insert into @ListFeeCodes(RowID,ColumnName,Value) values (2,'CODE','BLD005')
--insert into @ListFeeCodes(RowID,ColumnName,Value) values (3,'CODE','STRM0002')


--Populate table variables
select * into #ListFeeCodes from  @ListFeeCodes

select * into #ListUpdateFields from @ListUpdateFields

select * into #MainTableRec from @ListTableAndRecords

-- Start populating table (this should create all combinations for insert)
set @TableName = (select distinct tblName from #MainTableRec)
             
--use function to get key field
select @KeyField = dbo.tfn_ActivityNoField(@TableName)
          

SET @SCHEDULE_EFFECTIVE_DATE = (SELECT MAX(EFFECTIVE_ON) FROM Prmry_FeeScheduleList WHERE Prmry_FeeScheduleList.EFFECTIVE_ON < GETDATE() AND TEST <> 1)
              
SET @EFFECTIVE_FEE_SCHEDULE = (SELECT TOP 1 Prmry_FeeScheduleList.SCHEDULE_NAME FROM Prmry_FeeScheduleList WHERE TEST <> 1 AND EFFECTIVE_ON = @SCHEDULE_EFFECTIVE_DATE or TEST <> 1 AND EFFECTIVE_ON is null )
              
IF @SCHEDULE_EFFECTIVE_DATE IS NULL
        BEGIN
            SET @SCHEDULE_EFFECTIVE_DATE = GETDATE()
            SET @EFFECTIVE_FEE_SCHEDULE = 'Default'
        END
       
SELECT @SCHEDULE_EFFECTIVE_DATE, @EFFECTIVE_FEE_SCHEDULE


-- Start populating table (this should create all combinations for insert)
insert into #ForInsertFees(ACTIVITY_NO,CODE,DESCRIPTION, FORMULA, FEETYPE, ACCOUNT, DETAIL) 
select MTR.ActivityNo, lfc.Value, pfs.DESCRIPTION, pfs.FORMULA , pfs.feetype, pfs.account, '0'
from #ListFeeCodes lfc, #MainTableRec MTR, Prmry_FeeSchedule pfs 
WHERE pfs.SCHEDULE_NAME = @EFFECTIVE_FEE_SCHEDULE AND pfs.GroupName = 'Permits' AND CODE = pfs.CODE
And lfc.Value = pfs.CODE and pfs.LevelNo = 1

insert into #ForInsertFees(ACTIVITY_NO,CODE,DESCRIPTION, FORMULA, FEETYPE, ACCOUNT, DETAIL) 
select MTR.ActivityNo, pfs.parent_code, pfs.DESCRIPTION, pfs.FORMULA , pfs.feetype, pfs.account, '1'
from #ListFeeCodes lfc, #MainTableRec MTR, Prmry_FeeSchedule pfs 
WHERE pfs.SCHEDULE_NAME = @EFFECTIVE_FEE_SCHEDULE AND pfs.GroupName = 'Permits' AND CODE = lfc.Value
And lfc.Value = pfs.CODE and pfs.LevelNo = 2

-- Start populating table (this should create all combinations for insert)
insert into #ForInsertSubFees(ACTIVITY_NO,CODE, ITEM, DESCRIPTION, FORMULA, subFEETYPE, ACCOUNT) 
select MTR.ActivityNo,pfs.PARENT_CODE , lfc.Value, pfs.DESCRIPTION, pfs.FORMULA, pfs.feetype, pfs.account
from #ListFeeCodes lfc, #MainTableRec MTR, Prmry_FeeSchedule pfs 
WHERE pfs.SCHEDULE_NAME = @EFFECTIVE_FEE_SCHEDULE AND pfs.GroupName = 'Permits' AND CODE =pfs.CODE
And lfc.Value = pfs.CODE and pfs.LevelNo = 2

        
--Update temp table based
select @RowId=MAX(RowID) FROM #ListUpdateFields     
Select @RowNum = Count(*) From #ListUpdateFields      
WHILE @RowNum > 0                          
BEGIN   

       select @ColumnName = ColumnName from #ListUpdateFields where RowID= @RowId    
       select @ColumnValue = Value from #ListUpdateFields where RowID= @RowId  

    if len(ltrim(rtrim(@ColumnValue))) > 0 and @ColumnName ='UserID'
            Begin
                set @UserID = @ColumnValue
            End

    if len(ltrim(rtrim(@ColumnValue))) > 0 and @ColumnName ='GroupName'
            Begin
                set @GroupName = @ColumnValue
            End 

       if len(ltrim(rtrim(@ColumnValue))) > 0 and @ColumnName ='TblName'
        Begin
            set @TblName = @ColumnValue 
        End 

       if len(ltrim(rtrim(@ColumnValue))) > 0 and @ColumnName ='UDFtblName'
              Begin
                     set @UDFTableName = @ColumnValue 
              End 

    if len(ltrim(rtrim(@ColumnValue))) > 0 and @ColumnName not in ('UserID', 'GroupName','TblName', 'UDFtblName')
        Begin
            set @SQL = 'Update #ForInsertFees set ' + @ColumnName +  ' = ''' + @ColumnValue + ''''
            set @SQL = 'Update #ForInsertSubFees set ' + @ColumnName +  ' = ''' + @ColumnValue + ''''
            exec(@sql)
        End           
                                  
    select top 1 @RowId=RowID from #ListUpdateFields where RowID < @RowId order by RowID desc
    set @RowNum = @RowNum - 1                             
       
END

update #ForInsertFees set DETAIL = '0',AMOUNT = 0,QUANTITY = 0, PAID = '0', RECORDID ='BPRO:' + right(('00' + str(DATEPART(yy, GETDATE()))),1)
+ right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)
+ right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
+ right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)
+ right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
+ right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)
+ right(('000000'+ ltrim(str(ForRecID))),6);

update #ForInsertSubFees set AMOUNT = 0, QUANTITY = 0, RECORDID ='BPRO:' + right(('00' + str(DATEPART(yy, GETDATE()))),1)
+ right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)
+ right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
+ right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)
+ right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
+ right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)
+ right(('000000'+ ltrim(str(ForRecID))),6);

update #ForInsertSubFees set PARENTID = fif.RECORDID
from #ForInsertFees fif
where fif.ACTIVITY_NO = #ForInsertSubFees.ACTIVITY_NO
and fif.code = #ForInsertSubFees.CODE

update #ForInsertFees set Detail = '1', Formula = '[ITEMIZE]'
from #ForInsertSubFees fisf
where fisf.ACTIVITY_NO = #ForInsertFees.ACTIVITY_NO
and fisf.CODE = #ForInsertFees.CODE

-- D9. Update Fees where no calculation or substitution necessary
UPDATE #ForInsertFees
SET #ForInsertFees.Calc_value = CAST(#ForInsertFees.FORMULA AS float)
WHERE ISNUMERIC( #ForInsertFees.FORMULA ) = 1

-- D10. Update SubFees where no calculation or substitution necessary 
UPDATE #ForInsertSubFees
SET #ForInsertSubFees.Calc_value = CAST(#ForInsertSubFees.FORMULA As float)
WHERE ISNUMERIC( #ForInsertSubFees.FORMULA) = 1

-- D12.  Update formula for QTY in SubFees
UPDATE #ForInsertSubFees
SET Formula_working_col = REPLACE(Formula, 'QTY', CAST(#ForInsertSubFees.QUANTITY as varchar(12)))
WHERE charindex('qty',Formula)<>0

UPDATE #ForInsertFees
SET Formula_working_col = REPLACE(Formula, 'QTY', CAST(#ForInsertFees.QUANTITY as varchar(12)))
WHERE charindex('qty',Formula)<>0


print('D13')

--D13.  Update formula for look up fields fields
IF OBJECT_ID(N'tempdb..#TempTable') IS NOT NULL
BEGIN
        DROP TABLE #TempTable
END

Select name into  #TempTable from  sys.columns WHERE object_id = (SELECT  object_id FROM [sys].[objects] WHERE type = 'U' and name = '' +  @UDFTableName  + '' and system_type_id in (56,62)) order by name

--print (@sql)
exec(@sql)

SELECT name FROM #TempTable

DECLARE @FieldName NVARCHAR(100)
DECLARE @fieldValue int
DECLARE @getcounterID CURSOR

SET @getcounterID = CURSOR FOR

SELECT name FROM #TempTable

OPEN @getcounterID
FETCH NEXT
FROM @getcounterID INTO @FieldName
WHILE @@FETCH_STATUS = 0
BEGIN
       -- update Formula_working_col
       IF EXISTS (SELECT distinct basis from #ForInsertSubFees inner join [Prmry_FeeTables]
       on '['+[TABLENAME]+']'=#ForInsertSubFees.FORMULA  WHERE basis=@FieldName) 

       --select * from #ForInsertFees
       --select * from #ForInsertSubFees

       --print('before 1')
       if @FieldName <> @KeyField
              begin
  
                 IF  EXISTS( SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'formula_update')
                        BEGIN
                                    drop table formula_update
                        end
       
                     DECLARE @sqlText nvarchar(max);
         
                     SET @sqlText = N'select Activity_NO, item as code,  formula, new_formula=
                     case when 
                     isnull(incr_step,0)<>0  and isnull(incr_amount,0)<>0 then
                     ''((''+ convert (varchar, '+@FieldName+')
                     +''-('' + convert(varchar,min_range  ) +''-'' + convert(varchar,incr_step  ) +''))''+
                     +''/'' +convert(varchar, incr_step  )+''*'' +convert(varchar,incr_amount) 
                     +'') +''+  convert(varchar,base_amount  )
                     else cast(base_amount as varchar) end
                     ,result=case when  
                     isnull(incr_step,0)<>0  and isnull(incr_amount,0)<>0 then
                     (( '+@FieldName+' - (min_range-incr_step ))/incr_step *incr_amount)+base_amount
                     else base_amount  end                                   
                     into formula_update
                     from(
                     select sf.Activity_NO, item, formula,basis,'+@FieldName+',[BASE_AMOUNT], MIN_RANGE,MAX_RANGE, [INCR_STEP] ,[INCR_AMOUNT]
                     from #ForInsertSubFees sf inner join ' + @UDFTableName  + ' t 
                     on     sf.Activity_NO=t.' + @KeyField + ' inner join [Prmry_FeeTables] ft
                     on ft.TABLENAME =replace( replace(sf.formula,''['',''''),'']'','''') where  groupname=''' + @GroupName + ''' 
                     and cast('+@FieldName+' as float) between   MIN_RANGE and          MAX_RANGE  ) as mdm'
    
                     --print('after 1')
                     --print(@sqlText)
                     exec (@sqlText)

                     set  @sqlText ='';    
                     SET @sqlText = N'update sf set Calc_value=result,Formula_working_col=new_formula'
                     SET @sqlText = @sqlText + ' from #ForInsertSubFees sf inner join formula_update as tbl on sf.Activity_NO'
                     SET @sqlText = @sqlText + '=tbl.Activity_NO and sf.item=tbl.code'
                     exec (@sqlText)
     
                 IF  EXISTS( SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'formula_update')
                 BEGIN
                             drop table formula_update
                 end
     
                     set  @sqlText ='';
                     SET @sqlText = N'update sf set Formula_working_col=replace(sf.formula,'
                     SET @sqlText = @sqlText + '''' + @FieldName 
                     SET @sqlText = @sqlText + ''''
                     SET @sqlText = @sqlText + ',t.'
                     SET @sqlText = @sqlText + '' + @FieldName
                     SET @sqlText = @sqlText + ''
                     SET @sqlText = @sqlText + ') from #ForInsertSubFees sf inner join '
                     SET @sqlText = @sqlText + @UDFTableName  
                     SET @sqlText = @sqlText + ' t on sf.Activity_NO=t.'
                     SET @sqlText = @sqlText + @KeyField 
                     SET @sqlText = @sqlText + ' where t.'
                     SET @sqlText = @sqlText + @FieldName 
                     SET @sqlText = @sqlText +' is not null and charindex('
                     SET @sqlText = @sqlText + ''''+ @FieldName +''''
                     SET @sqlText = @sqlText + ',sf.FORMULA)<>0'
                     --print('dowah')
                     --print(@FieldName)
                     exec (@sqlText)
         
              END

              FETCH NEXT
              FROM @getcounterID INTO @FieldName
       END

CLOSE @getcounterID
DEALLOCATE @getcounterID

--print('dowah')

--Select * into zmdmForInsertSubFees from #ForInsertSubFees
--Select * into zmdmForInsertFees from #ForInsertFees


SET @getcounterID = CURSOR FOR

SELECT name
FROM #TempTable

OPEN @getcounterID
FETCH NEXT
FROM @getcounterID INTO @FieldName
WHILE @@FETCH_STATUS = 0
BEGIN

-- update Formula_working_col
IF EXISTS (SELECT distinct basis from #ForInsertFees fif inner join [Prmry_FeeTables]ft
on ft.TABLENAME = replace(replace(fif.formula,'[',''),']','')   WHERE basis=@FieldName) 
  
  print('before 2')  
if @FieldName <>@KeyField
  begin
  
       IF  EXISTS( SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'formula_update')
       BEGIN
              drop table formula_update
       end

       set @sqlText ='';
              SET @sqlText = N'select  Activity_no, code,  formula, new_formula=
              case when 
              isnull(incr_step,0)<>0  and isnull(incr_amount,0)<>0 then
              ''((''+ convert (varchar, '+@FieldName+')
              +''-('' + convert(varchar,min_range  ) +''-'' + convert(varchar,incr_step  ) +''))''+
              +''/'' +convert(varchar, incr_step  )+''*'' +convert(varchar,incr_amount) 
              +'') +''+  convert(varchar,base_amount  )
              else cast(base_amount as varchar) end
              ,result=case when  
              isnull(cast(incr_step as varchar),''0'')<>''0''  and isnull(cast(incr_amount as varchar),''0'')<>''0'' then
              (( '+@FieldName+' - (min_range-incr_step ))/incr_step *incr_amount)+base_amount
              else base_amount  end                                                                     
              into formula_update
              from(
              select Activity_NO, code, formula,basis,'+@FieldName+',[BASE_AMOUNT], MIN_RANGE,MAX_RANGE, [INCR_STEP] ,[INCR_AMOUNT]
              from #ForInsertFees fif inner join  ' + @UDFTableName  + ' udft
              on     fif.Activity_NO = udft.' + @KeyField + '
              inner join [Prmry_FeeTables] ft
              on ft.TABLENAME =replace( replace(fif.formula,''['',''''),'']'','''') where  groupname=''' + @GroupName + ''' 
              and (' +@FieldName+ ' between  MIN_RANGE and MAX_RANGE)
              and ' +@FieldName+ ' <> 0 ) as bla'
              --print(@sqlText)
              --print('uno')
              exec (@sqlText)
              --print('dos')
       

set  @sqlText ='';    
SET @sqlText = N'update fif set Calc_value=result, Formula_working_col=new_formula'
SET @sqlText = @sqlText + ' from #ForInsertFees fif inner join formula_update as tbl on fif.Activity_NO=tbl.Activity_NO'
SET @sqlText = @sqlText + ' and fif.code=tbl.code'
--print('hmmmmm')
--print(@sqlText)
exec (@sqlText)
        
SET @sqlText =''

SET @sqlText = N'update fif set 
Formula_working_col=replace(formula,'''+@FieldName+''',t.'+ @FieldName +')
from #ForInsertFees fif inner join  ' + @UDFTableName  + ' t 
on    fif.Activity_no=t.' + @KeyField + ' where
t.'+ @FieldName +' is not null and charindex('''+@FieldName+''',FORMULA)<>0'
--print('aarrrrggggghhh')
--print(@sqlText)
exec (@sqlText)


end


FETCH NEXT
FROM @getcounterID INTO @FieldName
END
CLOSE @getcounterID
DEALLOCATE @getcounterID

declare @t2 as table(val varchar(20))
declare @exp varchar(20)
declare @code varchar (50)
declare @lic varchar (50)
declare @i int,@icount int
--calculate subfees
set @i=1;

Select @icount=  COUNT(*) from #ForInsertSubFees where 
  charindex('min',Formula_working_col)=0 and charindex('max',Formula_working_col)=0
and Formula_working_col <>'' and Calc_value is null

while @i<@icount
begin
    select top(@i) @exp =Formula_working_col,@lic=Activity_no,@code=item from  #ForInsertSubFees
       where Calc_value is null and charindex('min',Formula_working_col)=0 and charindex('max',Formula_working_col)=0
          and Formula_working_col <>'' order by Formula_working_col
     
    insert into @t2
    exec ('select '+@exp)  
  update  #ForInsertSubFees set Calc_value=(select val from @t2) where Activity_no=@lic and item=@code
  
       delete  from @t2
    set @i=@i+1
end
-- calculate fees
set @i=1;

Select @icount=  COUNT(*) from #ForInsertFees where 
 charindex('min',Formula_working_col)=0 and charindex('max',Formula_working_col)=0
and Formula_working_col <>'' and Formula_working_col is not null  and Calc_value is null 

while @i<@icount
begin
    select top(@i) @exp =Formula_working_col,@lic=Activity_no,@code=code from  #ForInsertFees
       where Calc_value is null and charindex('min',Formula_working_col)=0 and charindex('max',Formula_working_col)=0
       and Formula_working_col <>'' and  Formula_working_col is not null  order by Formula_working_col
     
    insert into @t2
    exec ('select '+@exp)  
  update  #ForInsertFees set Calc_value=(select val from @t2) where Activity_no=@lic and code=@code
  
       delete  from @t2
    set @i=@i+1
end

--***************************************************************
--***************************************************************
--***************************************************************
--***************************************************************
--***************************************************************
--***************************************************************


--check main record 


--print('MAYO stuff')

--select * into  zmdmForInsertSubFees  from #ForInsertSubFees
--select * into  zmdmForInsertFees  from #ForInsertFees


--.  Update formula for look up fields fields
IF OBJECT_ID(N'tempdb..#MainTempTable') IS NOT NULL
BEGIN
        DROP TABLE #MainTempTable
END

Select name into  #MainTempTable from  sys.columns WHERE object_id = (SELECT  object_id FROM [sys].[objects] WHERE type = 'U' and name = '' +  @TableName  + '' and system_type_id in (56,62)) order by name

--print (@sql)
exec(@sql)

SELECT name FROM #MainTempTable

SET @getcounterID = CURSOR FOR
SELECT name FROM #MainTempTable
OPEN @getcounterID
FETCH NEXT
FROM @getcounterID INTO @FieldName
WHILE @@FETCH_STATUS = 0
BEGIN
       -- update Formula_working_col
       IF EXISTS (SELECT distinct basis from #ForInsertSubFees inner join [Prmry_FeeTables]
       on '['+[TABLENAME]+']'=#ForInsertSubFees.FORMULA  WHERE basis=@FieldName) 

       --select * from #ForInsertFees
       --select * from #ForInsertSubFees

       --print('before 1')
       if @FieldName <> @KeyField
              begin
  
                 IF  EXISTS( SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'formula_update')
                        BEGIN
                                    drop table formula_update
                        end
      
         
SET @sqlText = N'select Activity_NO, item as code,  formula, new_formula=
case when 
isnull(incr_step,0)<>0  and isnull(incr_amount,0)<>0 then
''((''+ convert (varchar, '+@FieldName+')
+''-('' + convert(varchar,min_range  ) +''-'' + convert(varchar,incr_step  ) +''))''+
+''/'' +convert(varchar, incr_step  )+''*'' +convert(varchar,incr_amount) 
+'') +''+  convert(varchar,base_amount  )
else cast(base_amount as varchar) end
,result=case when  
isnull(incr_step,0)<>0  and isnull(incr_amount,0)<>0 then
(( '+@FieldName+' - (min_range-incr_step ))/incr_step *incr_amount)+base_amount
else base_amount  end                                   
into formula_update
from(
select sf.Activity_NO, item, formula,basis,'+@FieldName+',[BASE_AMOUNT], MIN_RANGE,MAX_RANGE, [INCR_STEP] ,[INCR_AMOUNT]
from #ForInsertSubFees sf inner join ' + @TableName  + ' t 
on     sf.Activity_NO=t.' + @KeyField + ' inner join [Prmry_FeeTables] ft
on ft.TABLENAME =replace( replace(sf.formula,''['',''''),'']'','''') where  groupname=''' + @GroupName + ''' 
and cast('+@FieldName+' as float) between   MIN_RANGE and          MAX_RANGE  ) as mdm'

    
                     print('after 1')
                     print(@sqlText)
                     exec (@sqlText)

                     set  @sqlText ='';    
                     SET @sqlText = N'update sf set Calc_value=result,Formula_working_col=new_formula'
                     SET @sqlText = @sqlText + ' from #ForInsertSubFees sf inner join formula_update as tbl on sf.Activity_NO'
                     SET @sqlText = @sqlText + '=tbl.Activity_NO and sf.item=tbl.code'
                     exec (@sqlText)
     
                 IF  EXISTS( SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'formula_update')
                 BEGIN
                             drop table formula_update
                 end
     
                     set  @sqlText ='';
                     SET @sqlText = N'update sf set Formula_working_col=replace(sf.formula,'
                     SET @sqlText = @sqlText + '''' + @FieldName 
                     SET @sqlText = @sqlText + ''''
                     SET @sqlText = @sqlText + ',t.'
                     SET @sqlText = @sqlText + '' + @FieldName
                     SET @sqlText = @sqlText + ''
                     SET @sqlText = @sqlText + ') from #ForInsertSubFees sf inner join '
                     SET @sqlText = @sqlText + @TableName  
                     SET @sqlText = @sqlText + ' t on sf.Activity_NO=t.'
                     SET @sqlText = @sqlText + @KeyField 
                     SET @sqlText = @sqlText + ' where t.'
                     SET @sqlText = @sqlText + @FieldName 
                     SET @sqlText = @sqlText +' is not null and charindex('
                     SET @sqlText = @sqlText + ''''+ @FieldName +''''
                     SET @sqlText = @sqlText + ',sf.FORMULA)<>0 and charindex('
                     SET @sqlText = @sqlText + '''['''
                     SET @sqlText = @sqlText + ',sf.FORMULA)=0'
                     --print('dowah')
                     --print(@sqlText)
                     exec (@sqlText)
         
              END

              FETCH NEXT
              FROM @getcounterID INTO @FieldName
       END

CLOSE @getcounterID
DEALLOCATE @getcounterID

--print('dowah')

--Select * into zmdmForInsertSubFees from #ForInsertSubFees
--Select * into zmdmForInsertFees from #ForInsertFees


SET @getcounterID = CURSOR FOR

SELECT name
FROM #MainTempTable

OPEN @getcounterID
FETCH NEXT
FROM @getcounterID INTO @FieldName
WHILE @@FETCH_STATUS = 0
BEGIN

-- update Formula_working_col
IF EXISTS (SELECT distinct basis from #ForInsertFees fif inner join [Prmry_FeeTables]ft
on '['+[TABLENAME]+']'= fif.formula  WHERE basis=@FieldName) 
  
  print('before 2')  
if @FieldName <>@KeyField
  begin
  
       IF  EXISTS( SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'formula_update')
       BEGIN
              drop table formula_update
       end

       set @sqlText ='';
              SET @sqlText = N'select  Activity_no, code,  formula, new_formula=
              case when 
              isnull(incr_step,0)<>0  and isnull(incr_amount,0)<>0 then
              ''((''+ convert (varchar, '+@FieldName+')
              +''-('' + convert(varchar,min_range  ) +''-'' + convert(varchar,incr_step  ) +''))''+
              +''/'' +convert(varchar, incr_step  )+''*'' +convert(varchar,incr_amount) 
              +'') +''+  convert(varchar,base_amount  )
              else cast(base_amount as varchar) end
              ,result=case when  
              isnull(cast(incr_step as varchar),''0'')<>''0''  and isnull(cast(incr_amount as varchar),''0'')<>''0'' then
              (( '+@FieldName+' - (min_range-incr_step ))/incr_step *incr_amount)+base_amount
              else base_amount  end                                                                     
              into formula_update
              from(
              select Activity_NO, code, formula,basis,'+@FieldName+',[BASE_AMOUNT], MIN_RANGE,MAX_RANGE, [INCR_STEP] ,[INCR_AMOUNT]
              from #ForInsertFees fif inner join  ' + @TableName  + ' udft
              on     fif.Activity_NO = udft.' + @KeyField + '
              inner join [Prmry_FeeTables] ft
              on ft.TABLENAME =replace( replace(fif.formula,''['',''''),'']'','''') where  groupname=''' + @GroupName + ''' 
              and (' +@FieldName+ ' between  MIN_RANGE and MAX_RANGE)
              and ' +@FieldName+ ' <> 0 ) as bla'

              --print(@sqlText)
              print('uno')
              exec (@sqlText)
              --print('dos')
       

set  @sqlText ='';    
SET @sqlText = N'update fif set Calc_value=result, Formula_working_col=new_formula'
SET @sqlText = @sqlText + ' from #ForInsertFees fif inner join formula_update as tbl on fif.Activity_NO=tbl.Activity_NO'
SET @sqlText = @sqlText + ' and fif.code=tbl.code'
--print('hmmmmm')
--print(@sqlText)
exec (@sqlText)
        
SET @sqlText =''

SET @sqlText = N'update fif set 
Formula_working_col=replace(formula,'''+@FieldName+''',t.'+ @FieldName +')
from #ForInsertFees fif inner join  ' + @TableName  + ' t 
on    fif.Activity_no=t.' + @KeyField + ' where
t.'+ @FieldName +' is not null and charindex('''+@FieldName+''',FORMULA)<>0'
print('aarrrrggggghhh')
print(@sqlText)
exec (@sqlText)


end

--select * into  zmdmForInsertSubFees2  from #ForInsertSubFees
--select * into  zmdmForInsertFees2  from #ForInsertFees


--FETCH NEXT
--FROM @getcounterID INTO @FieldName
--END
--CLOSE @getcounterID
--DEALLOCATE @getcounterID

----calculate subfees
--set @i=1;

--Select @icount=  COUNT(*) from #ForInsertSubFees where 
--  charindex('min',Formula_working_col)=0 and charindex('max',Formula_working_col)=0
--and Formula_working_col <>'' and Calc_value is null

--while @i<@icount
--begin
--    select top(@i) @exp =Formula_working_col,@lic=Activity_no,@code=item from  #ForInsertSubFees
--       where Calc_value is null and charindex('min',Formula_working_col)=0 and charindex('max',Formula_working_col)=0
--          and Formula_working_col <>'' order by Formula_working_col
     
--    insert into @t2
--    exec ('select '+@exp)  
--  update  #ForInsertSubFees set Calc_value=(select val from @t2) where Activity_no=@lic and item=@code
  
--       delete  from @t2
--    set @i=@i+1
--end
---- calculate fees
--set @i=1;

--Select @icount=  COUNT(*) from #ForInsertFees where 
-- charindex('min',Formula_working_col)=0 and charindex('max',Formula_working_col)=0
--and Formula_working_col <>'' and Formula_working_col is not null  and Calc_value is null 

--while @i<@icount
--begin
--    select top(@i) @exp =Formula_working_col,@lic=Activity_no,@code=code from  #ForInsertFees
--       where Calc_value is null and charindex('min',Formula_working_col)=0 and charindex('max',Formula_working_col)=0
--       and Formula_working_col <>'' and  Formula_working_col is not null  order by Formula_working_col
     
--    insert into @t2
--    exec ('select '+@exp)  
--  update  #ForInsertFees set Calc_value=(select val from @t2) where Activity_no=@lic and code=@code
  
--       delete  from @t2
--    set @i=@i+1
--end



----get all the fees finalized:

update #ForInsertFees set amount=Calc_value
update #ForInsertSubFees set amount=Calc_value
update #ForInsertFees set amount=subfees_total from (select sum([Calc_value]) as subfees_total,[PARENTID] from #ForInsertSubFees group by [PARENTID]) tbl inner join #ForInsertFees lf on [PARENTID]=lf.RECORDID

--UPDATE lm SET Balance_Due =total_due
--from [dbo].[BatchProcessing_Review_Main] lm inner  join 
-- (select Permit_no, sum(isnull(amount,0)) - sum(isnull(paid_amount,0)) as total_due FROM [dbo].[BatchProcessing_Review_Fees] lrf 
-- group by lrf.Permit_no) as tbl
--       on lm.Permit_no=tbl.Permit_no

INSERT INTO Permit_Fees
                         (PERMIT_NO, CODE, DESCRIPTION, ACCOUNT, FORMULA, QUANTITY, AMOUNT, DETAIL,  PAID, RECORDID,ASSESSED_DATE, ASSESSED_BY, FEETYPE)
SELECT        ACTIVITY_NO, CODE, DESCRIPTION, ACCOUNT, FORMULA, QUANTITY, AMOUNT, DETAIL, PAID,  RECORDID, ASSESSED_DATE, ASSESSED_BY,FEETYPE
FROM  #ForInsertFees

INSERT INTO Permit_SubFees
                         (PERMIT_NO, CODE, ITEM, DESCRIPTION, QUANTITY, FORMULA, AMOUNT, ACCOUNT, RECORDID, PARENTID, ASSESSED_DATE, ASSESSED_BY, SUBFEETYPE)
SELECT        ACTIVITY_NO, CODE, ITEM, DESCRIPTION, QUANTITY, FORMULA, AMOUNT, ACCOUNT, RECORDID, PARENTID, ASSESSED_DATE,   ASSESSED_BY, SUBFEETYPE
FROM            #ForInsertSubFees


--select * into  zmdmForInsertSubFees3  from #ForInsertSubFees
--select * into  zmdmForInsertFees3  from #ForInsertFees

end

END






GO
