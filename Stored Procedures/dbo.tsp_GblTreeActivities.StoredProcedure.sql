USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GblTreeActivities]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_GblTreeActivities]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GblTreeActivities]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE Procedure [dbo].[tsp_GblTreeActivities](
	   @LocRecordID			varchar(50),
	   @Activity_no	varchar(50),
	   @Group		varchar(20),
	   @IsParentSearch		bit, 
	   @IsLocationSearch	bit,
	   @maxRecords int = -1
)

As
BEGIN
 Declare @SQLStatement varchar(MAX)

 Set @SQLStatement = null 


 if @LocRecordID Is not Null and @LocRecordID <> '' and @isLocationSearch = 1
  Begin

		Set @SQLStatement = 'Select * from tvw_GblTreeActivities where RECORDID = ' + CHAR(39) + @LocRecordID + CHAR(39) + 
							' OR LOC_RECORDID = ' + CHAR(39) + @LocRecordID + CHAR(39) + 
							' OR PARENT_RECORDID = ' + CHAR(39) + @LocRecordID  + CHAR(39) + 							
							' ORDER BY SORT_CRITERIA1 DESC, SORT_CRITERIA2 ASC'
		EXEC(@SQLStatement)
					
		
  End 
Else if @IsParentSearch = 0 and @isLocationSearch = 0 and (@Activity_no is not null and @Activity_no <> '') and (@Group is not null and @group <> '') and (@LocRecordID is not null and @LocRecordID <> '')
  Begin
		Set @SQLStatement = 'Select * from tvw_GblTreeActivities where ACTIVITYNO = ' + CHAR(39) + @Activity_no + CHAR(39) + ' OR PARENT_RECORDID = ' + CHAR(39) + @LocRecordID  + CHAR(39) + 'ORDER BY sort_criteria1 desc, sort_criteria2  asc'
			EXEC(@SQLStatement)

  End

Else if (@IsParentSearch = 0 and @isLocationSearch = 0  and (@Activity_no is not null and @Activity_no <> '') and (@Group is not null and @group <> '') and 
		 (@LocRecordID is null or @LocRecordID = ''))
  Begin
		Set @SQLStatement = 'Select * from tvw_GblTreeActivities where ACTIVITYNO = ' + CHAR(39) + @Activity_no + CHAR(39) +  ' AND TGROUP = '  + CHAR(39) + @Group + CHAR(39) + ' ORDER BY sort_criteria1 desc, sort_criteria2  asc'
			EXEC(@SQLStatement)
  End

Else if (@IsParentSearch = 1 and @isLocationSearch = 0 and (@Activity_no is not null and @Activity_no <> '') and (@Group is not null and @group <> '')) 
		
  Begin 
		Set @SQLStatement = ' * from tvw_GblTreeActivities where ((ACTIVITYNO = ' + CHAR(39) + @Activity_no + CHAR(39) +  ' AND TGROUP = '  + CHAR(39) + @Group + CHAR(39) + ')'
		Set @SQLStatement = @SQLStatement + 
				Case @Group
					When 'PERMIT' Then ' or PARENT_PERMIT_NO = ' + CHAR(39) + @Activity_no + CHAR(39) + ') '
					When 'PROJECT' Then ' or PARENT_PROJECT_NO = ' + CHAR(39) + @Activity_no + CHAR(39) + ') '
					When 'PAYMENT' Then ' or PARENT_ACTIVITY_NO = ' + CHAR(39) + @Activity_no + CHAR(39) + ') '					
					When 'GEO' Then ' or PARENT_SITE_APN = ' + CHAR(39) + @Activity_no + CHAR(39) + ') '
					When 'LICENSE' Then ' or PARENT_BUS_LIC_NO = ' + CHAR(39) + @Activity_no + CHAR(39) + ') '
					When 'AEC' Then ' or PARENT_AEC_ST_LIC_NO = ' + CHAR(39) + @Activity_no + CHAR(39) + ') '
					When 'LICENSE2' Then ' or PARENT_BUS_LIC_NO = ' + CHAR(39) + @Activity_no + CHAR(39) + ') '
					When 'CASE' Then ' or PARENT_BUS_LIC_NO = ' + CHAR(39) + @Activity_no + CHAR(39) + ') '
				End
		if @maxRecords <> -1
			Begin 
				DECLARE @permitMax int = 0;
				if @Group = 'PERMIT' SET @permitMax = @maxRecords + 1;
				else SET @permitMax = @maxRecords;
				DECLARE @PermitStatement varchar(500) = 'select top ' + cast(@permitMax As varchar(500)) +  @SQLStatement + ' AND Tgroup = ''PERMIT'''

				DECLARE @projectMax int = 0;
				if @Group = 'PROJECT' SET @projectMax = @maxRecords + 1;
				else SET @projectMax = @maxRecords;
				DECLARE @ProjectStatement varchar(500) = 'select top ' + cast(@projectMax As varchar(500)) +  @SQLStatement + ' AND Tgroup = ''PROJECT''';

				DECLARE @caseMax int = 0;
				if @Group = 'CASE' SET @caseMax = @maxRecords + 1;
				else SET @caseMax = @maxRecords;
				DECLARE @CaseStatement varchar(500) = 'select top ' + cast(@caseMax As varchar(500)) +  @SQLStatement + ' AND Tgroup = ''CASE''';

				DECLARE @licenseMax int = 0;
				if @Group = 'LICENSE2' SET @licenseMax = @maxRecords + 1;
				else SET @licenseMax = @maxRecords;
				DECLARE @LicenseStatement varchar(500) = 'select top ' + cast(@licenseMax As varchar(500)) +  @SQLStatement + ' AND Tgroup = ''LICENSE2''';

				DECLARE @crmMax int = 0;
				if @Group = 'CRM' SET @crmMax = @maxRecords + 1;
				else SET @crmMax = @maxRecords;
				DECLARE @CRMStatement varchar(500) = 'select top ' + cast(@crmMax As varchar(500)) +  @SQLStatement + ' AND Tgroup = ''CRM''';

			
				DECLARE @AECMax int = 0;
				if @Group = 'AEC' SET @AECMax = @maxRecords + 1;
				else SET @AECMax = @maxRecords;
				DECLARE @AECStatement varchar(500) = 'select top ' + cast(@AECMax As varchar(500)) +  @SQLStatement + ' AND Tgroup = ''AEC''';

				SET @SQLStatement = @PermitStatement + ' UNION ' + @ProjectStatement + ' UNION ' + @CaseStatement + ' UNION ' + @LicenseStatement + ' UNION ' + @CRMStatement + ' UNION ' + @AECStatement + ' ORDER BY sort_criteria1 desc, sort_criteria2  asc'

			End
		else 
			Begin
				
				SET @SQLStatement = 'select ' + @SQLStatement + ' ORDER BY sort_criteria1 desc, sort_criteria2  asc';
			End
			print @SQLStatement;
			EXEC(@SQLStatement);

		
  End

 END

GO
