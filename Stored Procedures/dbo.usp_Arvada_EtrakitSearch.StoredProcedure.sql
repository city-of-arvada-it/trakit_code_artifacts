USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_EtrakitSearch]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[usp_Arvada_EtrakitSearch]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_EtrakitSearch]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
CREATE procedure [dbo].[usp_Arvada_EtrakitSearch]
@searchCriteria as nvarchar(500) 
--declare @searchCriteria as nvarchar(100)
--declare @activityId int
--set @searchCriteria = '200227971'
--set @searchCriteria = 'ne'
--set @searchCriteria = '4455'
--set @searchCriteria = 'glad'
--set @searchCriteria = 'be'
--set @activityId = 1

/*

exec [usp_Arvada_EtrakitSearch] @searchCriteria = 'gillis'

*/

as 
set nocount on

declare @spError nvarchar(1000)

create table #search
( 
	[EtrakIt_Name] [varchar](50) NOT NULL,	
	[Company_Name] [varchar](50) NULL,
	[First_Name] [varchar](50) NULL,
	[Middle_Initial] [varchar](50) NULL,
	[Last_Name] [varchar](50) NULL,
	[Address] [varchar](50) NULL,
	[City] [varchar](50) NULL,
	[State] [varchar](50) NULL,
	[Zip_Code] [varchar](50) NULL,
	[Zip4] [varchar](50) NULL,
	[Phone_Area] [varchar](10) NULL,
	[Phone_First_Three] [varchar](10) NULL,
	[Phone_Last_Four] [varchar](10) NULL,
	[Phone_Ext] [varchar](50) NULL,
	[Email] [varchar](80) NULL,
	[Active] [varchar](1) NULL
)

insert into #search
(
	[EtrakIt_Name] ,
	[Company_Name] ,
	[First_Name],
	[Middle_Initial] ,
	[Last_Name] ,
	[Address],
	[City],
	[State] ,
	[Zip_Code] ,
	[Zip4] ,
	[Phone_Area] ,
	[Phone_First_Three] ,
	[Phone_Last_Four],
	[Phone_Ext] ,
	[Email] ,
	[Active] 
)
select
	[EtrakIt_Name] ,
	[Company_Name] ,
	[First_Name],
	[Middle_Initial] ,
	[Last_Name] ,
	[Address],
	[City],
	[State] ,
	[Zip_Code] ,
	[Zip4] ,
	[Phone_Area] ,
	[Phone_First_Three] ,
	[Phone_Last_Four],
	[Phone_Ext] ,
	[Email] ,
	[Active] 
from [dbo].[EtrakIt_User_Accounts] a
where
	[EtrakIt_Name] like '%'+@searchCriteria+'%'
or
	[Company_Name] like '%'+@searchCriteria+'%'
or
	[First_Name] like '%'+@searchCriteria+'%'
or
	[Last_Name] like '%'+@searchCriteria+'%'
or
	[Address] like '%'+@searchCriteria+'%'
or
	[Phone_Last_Four] like '%'+@searchCriteria+'%'
or
	[Email] like '%'+@searchCriteria+'%'
 
if (
	select count(*)
	from #search t
	) > 1000 
begin
	set @spError = 'There were too many results generated with that search criteria, please enter more information'
	raiserror(@spError,11,1) 
	return
end

select
	[EtrakIt_Name] ,
	isnull([Company_Name] ,'') as [Company_Name],
	isnull([First_Name],'') as [First_Name],
	isnull([Last_Name] ,'') as [Last_Name],
	isnull([Address],'') as [Address],
	isnull([City],'') as [City],
	isnull([Email] ,'') as [Email],
	isnull([Active] , '') as [Active]
from #search t
order by 
	t.[EtrakIt_Name]



GO
