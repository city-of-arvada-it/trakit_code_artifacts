USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_PlanReviewAEDAPermits]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[usp_Arvada_PlanReviewAEDAPermits]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_PlanReviewAEDAPermits]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_Arvada_PlanReviewAEDAPermits]
	@ProjectNo nvarchar(100)
as
begin

/*
exec usp_Arvada_PlanReviewAEDAPermits 'DA2017-0002'
exec usp_Arvada_PlanReviewAEDAPermits 'DA2017-0002'

*/
	
	select
		 permit_no,
		 APPLIED,
		 APPROVED,
		 ISSUED
	from dbo.permit_main
	where PARENT_PROJECT_NO = @ProjectNo


end
GO
