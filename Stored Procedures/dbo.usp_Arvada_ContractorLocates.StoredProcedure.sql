USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_ContractorLocates]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[usp_Arvada_ContractorLocates]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_ContractorLocates]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_Arvada_ContractorLocates]
	--@contractors nvarchar(max),
	@startdate datetime,
	@enddate datetime
as 

/*
exec [usp_Arvada_ContractorLocates] 
	@contractors = 'ADVANCED HARDSCAPE LLC|CABLECOM, LLC|Master drilling|JR telecom|ON TIME COMMUNICATIONS LLC|INTEGRATED COMMUNICATION SERVICE INC|CPI COMMUNICATIONS LLC|RAPID WIRE',
	@startdate = '02/01/2017',
	@enddate = '03/03/2017'


select applicant_name, contractor_name, permit_no, site_addr, applied, ISSUED
from Permit_Main
where ISSUED > '2017-02-28'
and PermitType = 'right of way'
AND (CONTRACTOR_NAME IN ('ADVANCED HARDSCAPE LLC', 'CABLECOM, LLC', 'Master drilling', 'JR telecom', 'ON TIME COMMUNICATIONS, LLC', 'INTEGRATED COMMUNICATION SERVICE, INC', 'CPI COMMUNICATIONS LLC', 'RAPID WIRE'))


*/

--create table #contractors
--(
--	contractorname nvarchar(250)
--)
 
-- insert into #contractors
--(	
--	contractorname
--)
--select
--	[Val]
--from [dbo].tfn_Arvada_SSRS_String_To_Table(@contractors,'|',1)



SELECT 
	m.issued AS issuedDate, 
	m.site_addr, 
	m.PERMIT_NO, 
	m.CONTRACTOR_NAME, 
	m.PermitSubType, 
	m.[DESCRIPTION]
FROM Permit_Main m
--inner join #contractors c
--	on c.contractorname = m.CONTRACTOR_NAME
WHERE issued >= @startdate
and issued < dateadd(Day,1,@enddate)
AND (CONTRACTOR_NAME IN ('ADVANCED HARDSCAPE LLC', 'CABLECOM, LLC', 'Master drilling', 'JR telecom', 'ON TIME COMMUNICATIONS, LLC', 'INTEGRATED COMMUNICATION SERVICE, INC', 'CPI COMMUNICATIONS LLC', 'RAPID WIRE'))
ORDER BY 
	APPLIED ASC

GO
