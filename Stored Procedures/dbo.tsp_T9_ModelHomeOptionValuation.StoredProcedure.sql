USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ModelHomeOptionValuation]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_ModelHomeOptionValuation]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ModelHomeOptionValuation]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  
CREATE PROCEDURE [dbo].[tsp_T9_ModelHomeOptionValuation]
             @ModelRecordID varchar(10),
			 @OptionName varchar(300)
AS
BEGIN


       if @ModelRecordID <> '' and @OptionName <> ''
              Begin

					SELECT  MBV.RecordID, ModelRecordID, MBV.OptionName, ValuationCode, VS.[Description],  QUANTITY,  MBV.ORDERID FROM Prmry_ModelHomeOptionValuations MBV
					Join Prmry_ValuationSchedule  VS on  MBV.ValuationCode = VS.CODE AND VS.UNITS = 'EA' And VS.GroupName = 'PERMITS'
					where ModelRecordID = @ModelRecordID and MBV.OptionName = @OptionName



              end
	else if @ModelRecordID <> ''
		begin
		SELECT  MBV.RecordID, ModelRecordID, MBV.OptionName, ValuationCode, VS.[Description],  QUANTITY,  MBV.ORDERID FROM Prmry_ModelHomeOptionValuations MBV
					Join Prmry_ValuationSchedule  VS on  MBV.ValuationCode = VS.CODE AND VS.UNITS = 'EA'  And VS.GroupName = 'PERMITS'
					where ModelRecordID = @ModelRecordID 
		end

END





GO
