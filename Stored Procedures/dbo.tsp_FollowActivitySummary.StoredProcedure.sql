USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_FollowActivitySummary]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_FollowActivitySummary]
GO
/****** Object:  StoredProcedure [dbo].[tsp_FollowActivitySummary]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  					  
					  					  					  /*
tsp_FollowActivitySummary @Group='PERMIT', @ActivityNo='BRES2012-00040', @vLastUpdate='2012-08-02 12:28:47.933'

*/


CREATE Procedure [dbo].[tsp_FollowActivitySummary] (
@Group Varchar(200),
@ActivityNo Varchar(6000),
@vLastUpdate Varchar(200)
)

As

Declare @SQL Varchar(8000)
Declare @LastUpdate Datetime
Declare @TempID INT
Declare @LoopDate Datetime

Set @LastUpdate = Convert(Datetime,@vLastUpdate)
Print @LastUpdate


--RB Check if table exists before creating it
   IF OBJECT_ID('tempdb..#Audit') IS NOT NULL  
	drop table #Audit

Create Table #Audit(TempID INT Identity, [Type] Varchar(1), [Group] Varchar(100), ActivityNo Varchar(200), TableName Varchar(200), PrimaryKeyField Varchar(2000), PrimaryKeyValue Varchar(2000), UpdateDate Datetime, Ignore INT Default 0)

Set @SQL = '
Insert Into #Audit([Type],[Group],ActivityNo,TableName,PrimaryKeyField,PrimaryKeyValue,UpdateDate)
Select
--*
[Type]
,[Group]
,ActivityNo
,TableName
,PrimaryKeyField
,PrimaryKeyValue
,UpdateDate
From Audit
WHERE UpdateDate > Convert(Datetime,''' + @vLastUpdate + ''')
AND [Group] = ''' + @Group + '''
AND ActivityNo IN (''' + @ActivityNo + ''') 
Group By [Type],[Group],ActivityNo,TableName,PrimaryKeyField,PrimaryKeyValue,UpdateDate
Order By UpdateDate DESC
'
Print(@SQL)
Exec(@SQL)

--If Exists (Select * From #Audit WHERE Ignore = 0)
-- Begin
--	Set @TempID = Max(TempID) From #Audit
 
-- End

--Select * From #Audit WHERE Ignore = 0 Order By TempID


Select
--*
[Type]
,[Group]
,ActivityNo
,TableName
From #Audit
Group By [Type],[Group],ActivityNo,TableName
Order By TableName,[Group],ActivityNo


/*
tsp_FollowActivitySummary @Group='PERMIT', @ActivityNo='BRES2012-00040', @vLastUpdate='2012-08-02 12:28:47.933'

*/













GO
