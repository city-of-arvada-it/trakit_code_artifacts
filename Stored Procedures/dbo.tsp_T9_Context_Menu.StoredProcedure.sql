USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_Context_Menu]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_Context_Menu]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_Context_Menu]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		rafael Buelna
-- Create date: 5/19/2016
-- Description:	Gets Context Menu Items
-- Revision History:
-- =============================================

Create PROCEDURE [dbo].[tsp_T9_Context_Menu](
	@GroupName as varchar(20), 
	@TypeName as varchar(20), 
	@Status as varchar(20)
	)
	
AS
BEGIN
	
		select top 1 * from prmry_Context_menu where ((Groupname = @GroupName or Groupname = 'ALL')
		and  (@TypeName = TypeName or TypeName = 'ALL')
		and  (@Status = [Status] or [Status] = 'ALL'))
			
END
-- =============================================
-- ============================================= changes from Dipti on 05/17

/****** Object:  StoredProcedure [dbo].[tsp_GblSearchMain]    Script Date: 02/04/2014 13:56:07 ******/
SET ANSI_NULLS ON
GO
