USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearchSaveDetail]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_AdvSearchSaveDetail]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearchSaveDetail]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  					  

                                    
-- ==========================================================================================
-- Revision History:
--       2014-03-03 - mdm - added to sourcesafe
-- ==========================================================================================   

CREATE PROCEDURE [dbo].[tsp_T9_AdvSearchSaveDetail] (
       @ParentRecordID as varchar(10),
	   @Detail as varchar(8000) 
	   )

AS
BEGIN

	Declare @SQL Varchar(8000)
	Declare @ValuesClause varchar(8000)

	

	-- clean detail
	set @sql = 'DELETE FROM Prmry_AdvSearchConfigDetail where ParentRecordid =' + @ParentRecordID
			
	--print(@sql)
	exec(@sql)

	--repopulate detail 

	--RB Check if table exists before creating it
   IF OBJECT_ID('tempdb..#Temp') IS NOT NULL  
	drop table #Temp

	Create Table #Temp(
		FullString varchar(8000),
		ParseString varchar(8000),
		TableName varchar(50),
		FieldName varchar(50),
		DisplayName varchar(50),
		DefaultOperator varchar(50),
		ParentRecordID int,
		DefaultRow varchar(5),
		Datatype varchar(50) ,
		Value1 varchar(255) ,
		Value2 varchar(255) 
	)

	--Check and prefix Detail with ';'
	 set @detail = ';' + @Detail
 
	;WITH MyRows AS
	(
		SELECT LEFT(@Detail, CHARINDEX(';', @Detail) -1) AS MyRow, RIGHT(@Detail, LEN(@Detail) - CHARINDEX(';', @Detail)) AS Remainder
		UNION ALL
		SELECT LEFT(Remainder, CHARINDEX(';', Remainder) -1) AS MyRow, RIGHT(Remainder, LEN(Remainder) - CHARINDEX(';', Remainder)) AS Remainder
		FROM MyRows
		WHERE CHARINDEX(';', Remainder)>0
		UNION ALL
		SELECT Remainder AS MyRow, NULL AS Remainder
		FROM MyRows
		WHERE CHARINDEX(';', Remainder)=0
	)
	
	INSERT INTO #Temp (FullString, ParseString)
	SELECT MyRow AS FullString, MyRow as parsestring
	FROM MyRows Where len(ltrim(rtrim(MyRow))) > 0

	update #Temp set parentrecordid = @ParentRecordID
 
	update #Temp set tablename = substring(ParseString,0,charindex('|',ParseString))
	update #Temp set ParseString = substring(ParseString,(charindex('|',ParseString) + 1),8000)

	update #Temp set fieldname = substring(ParseString,0,charindex('|',ParseString))
	update #Temp set ParseString = substring(ParseString,(charindex('|',ParseString) + 1),8000)

	update #Temp set DisplayName = substring(ParseString,0,charindex('|',ParseString))
	update #Temp set ParseString = substring(ParseString,(charindex('|',ParseString) + 1),8000)

	update #Temp set DefaultOperator = substring(ParseString,0,charindex('|',ParseString))
	update #Temp set ParseString = substring(ParseString,(charindex('|',ParseString) + 1),8000)

	update #Temp set DataType = substring(ParseString,0,charindex('|',ParseString))
	update #Temp set ParseString = substring(ParseString,(charindex('|',ParseString) + 1),8000)

	update #Temp set Value1 = substring(ParseString,0,charindex('|',ParseString))
	update #Temp set ParseString = substring(ParseString,(charindex('|',ParseString) + 1),8000)

	update #Temp set Value2 = substring(ParseString,0,charindex('|',ParseString))
	update #Temp set ParseString = substring(ParseString,(charindex('|',ParseString) + 1),8000)

	update #Temp set DefaultRow =ParseString

	set @sql = 'Select * from #Temp'

	print(@sql)

	--Insert detail
	set @sql ='INSERT INTO Prmry_AdvSearchConfigDetail
        (TableName, FieldName, DisplayName, DefaultOperator, ParentRecordID, DefaultRow, Datatype, Value1, Value2)
		SELECT TableName, FieldName, DisplayName, DefaultOperator, ParentRecordID, DefaultRow, Datatype, Value1, Value2 FROM #Temp'
	
	print(@sql)

	exec(@sql)


End









GO
