USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GblSearchRouteInspections]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_GblSearchRouteInspections]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GblSearchRouteInspections]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  					  
					  					  					  



/*

tsp_GblSearchInspectionCenter @InspectorID='mh',@InspectionType='ROOFING Final',@DateType='Scheduled_Date',
@vStartDate='8/1/92',@vEndDate='9/1/92',@Group='Permit'

tsp_GblSearchInspectionCenter @InspectorID='AB',@InspectionType='SPRINKLER CLEARANCE',@DateType='Scheduled_Date',
@vStartDate='1/1/2008',@vEndDate='10/1/2012',@Group='Permit'


tsp_GblSearchInspectionCenter @InspectorID='AXR'

*/

CREATE Procedure [dbo].[tsp_GblSearchRouteInspections](
@where Varchar(5000) = Null
--@InspectionType Varchar(800) = Null,
--@vStartDate Varchar(100) = Null,
--@vEndDate Varchar(100) = Null,
--@DateType Varchar(100) = Null,
--@Group Varchar(100) = Null
)


As

Declare @SQL Varchar(8000)
--Declare @WHERE Varchar(5000)
--Declare @StartDate Datetime = Null
--Declare @EndDate Datetime = Null

--Set @WHERE = ''


--If @InspectorID = '' Set @InspectorID = Null
--If @InspectionType = '' Set @InspectionType = Null
--If @DateType = '' Set @DateType = Null
--If @vStartDate = '' Set @vStartDate = Null
--If @vEndDate = '' Set @vEndDate = Null
--If @Group = '' Set @Group = Null


--If @InspectorID Is Not Null Set @WHERE = @WHERE + CHAR(10) + ' AND INSPECTOR = ''' + @InspectorID + ''''
--If @InspectionType Is Not Null Set @WHERE = @WHERE + CHAR(10) + ' AND InspectionType = ''' + @InspectionType + ''''
--If @Group Is Not Null Set @WHERE = @WHERE + CHAR(10) + ' AND tGroup = ''' + @Group + ''''
--If @vStartDate Is Not Null Set @StartDate = CONVERT(Datetime,@vStartDate)
--If @vEndDate Is Not Null Set @EndDate = CONVERT(Datetime,DATEADD(dd,1,@vEndDate))


--Print(@StartDate)
--Print(@EndDate)

--If @DateType Is Not Null
-- Begin
--	If @StartDate Is Not Null Set @WHERE = @WHERE + CHAR(10) + ' AND ' + @DateType + ' >= ''' + Convert(Varchar,@StartDate) + ''''
--	If @EndDate Is Not Null Set @WHERE = @WHERE + CHAR(10) + ' AND ' + @DateType + ' < ''' + Convert(Varchar,@EndDate) + ''''
-- End


Set @SQL = '
Select vi.*, InspectorName = CASE WHEN ic.Field03 IS NULL THEN vi.Inspector ELSE ic.Field03 END From tvw_GblInspections vi
LEFT JOIN Prmry_InspectionControl ic
ON vi.INSPECTOR = ic.Field01 AND ic.GroupName = (REPLACE(vi.tGroup,''CODE'',''VIOLATION'') + ''s'')
Where 1 = 1 ' + @WHERE

PRINT(@SQL)
EXEC(@SQL)























GO
