USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GblSearchMain_Activity_SubSearch]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_GblSearchMain_Activity_SubSearch]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GblSearchMain_Activity_SubSearch]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[tsp_GblSearchMain_Activity_SubSearch] 
	 @searchString VARCHAR(500) = ''
	,@ExcludedGroups VARCHAR(250) = ''
AS
BEGIN

	SET NOCOUNT ON

	---- FOR DEBUG
	--SET NOCOUNT OFF

	--DECLARE @searchString VARCHAR(100) = 'BELL'
	--DECLARE @ExcludedGroups VARCHAR(250) = NULL

	--DROP TABLE [#Searches]
	--DROP TABLE [#TT]
	--DROP TABLE #ExcludeGroup
	---- FOR DEBUG


	CREATE TABLE [#Searches] (
		ColumnSearchAlias VARCHAR(256) NULL
		,[SearchTextFragment] [varchar](500) NULL
	)

	CREATE INDEX SEARCHESINDEX ON #SEARCHES (
		ColumnSearchAlias
		,[SearchTextFragment]
	)

	CREATE TABLE [#TT] (
		[recordid] [varchar](30) NULL
		,[SearchTextFragment] [varchar](256) NULL
	)

	CREATE TABLE #ExcludeGroup (tgroup VARCHAR(100))
	SELECT @ExcludedGroups = COALESCE(@ExcludedGroups,'')

	-- Create rows from excluded groups string
	INSERT INTO #ExcludeGroup
	SELECT *
	FROM dbo.BreakStringIntoRows(@ExcludedGroups)

	-- Will be using StartsWith instead of under-perf Contains
	IF RIGHT(@searchString, 1) <> '%'
		SET @searchString = @searchString + '%'

	SET @searchString = REPLACE(@searchString, '''', '''''')

	INSERT INTO [#Searches]
	VALUES 
		('activityNo',@searchString),
		('site_APN',@searchString),
		('site_addr' ,@searchString),
		('owner_name',@searchString),
		('title',@searchString),
		('site_alternate_id',@searchString),
		('site_subdivision',@searchString),
		('case_no',@searchString),
		('permit_no',@searchString),	--activityNo alias for table permit_main
		('project_no',@searchString),	--activityNo alias for table project_main
		('license_no',@searchString),	--activityNo alias for table license2_main
		('issue_label',@searchString),	--activityNo alias for table crm_issues
		('recordid',@searchString),		--activityNo alias for table geo_ownership
		('st_lic_no',@searchString),	--activityNo alias for table aec_main
		('company',@searchString),		--TITLE alias for table aec_main
		('case_name',@searchString),	--TITLE alias for table case_main
		('description',@searchString),	--TITLE alias for table permit_main
		('project_name',@searchString),	--TITLE alias for table project_main
		('address1',@searchString)		--SITE_ADDR alias for table aec_main

	--Search for Case_main
	INSERT INTO #TT
	SELECT *
	FROM (
		SELECT dbo.case_main.recordid
			,[TBL_case_main_Fragments].[SearchTextFragment]
		FROM dbo.case_main
			INNER JOIN [dbo].[TBL_case_main_Fragments] WITH (forceseek) 
				ON CASE_MAIN.RECORDID = [TBL_case_main_Fragments].RECORDID
			INNER JOIN [#Searches] 
				ON [TBL_case_main_Fragments].ColumnSearchAlias = #Searches.ColumnSearchAlias
				AND [TBL_case_main_Fragments].[SearchTextFragment] LIKE #Searches.SearchTextFragment
	) AS tt

	INSERT INTO #TT
	SELECT *
	FROM (
		SELECT dbo.case_main.recordid
			,[TBL_geo_ownership_Fragments].[SearchTextFragment]
		FROM [#Searches]
			INNER LOOP JOIN [dbo].[TBL_geo_ownership_Fragments] 
				ON TBL_geo_ownership_Fragments.ColumnSearchAlias = #Searches.ColumnSearchAlias
				AND TBL_geo_ownership_Fragments.[SearchTextFragment] LIKE #Searches.SearchTextFragment
			INNER JOIN dbo.case_main 
				ON case_main.loc_recordid = [TBL_geo_ownership_Fragments].recordid
		WHERE 
			TBL_geo_ownership_Fragments.ColumnSearchAlias = 'site_alternate_id' 
	) AS tt
	OPTION (MAXDOP 1)

	--Search for permit_main
	INSERT INTO #TT
	SELECT *
	FROM (
		SELECT dbo.permit_main.recordid
			,[TBL_permit_main_Fragments].[SearchTextFragment]
		FROM [#Searches]
			INNER JOIN [TBL_permit_main_Fragments] 
				ON [TBL_permit_main_Fragments].ColumnSearchAlias = #Searches.ColumnSearchAlias
				AND [TBL_permit_main_Fragments].[SearchTextFragment] LIKE #Searches.SearchTextFragment
			INNER JOIN permit_main WITH (forceseek) 
				ON permit_main.RECORDID = [TBL_permit_main_Fragments].RECORDID
		) AS tt
	OPTION (FORCE ORDER)

	INSERT INTO #TT
	SELECT *
	FROM (
		SELECT dbo.permit_main.recordid
			,[TBL_geo_ownership_Fragments].[SearchTextFragment]
		FROM [#Searches]
			INNER LOOP JOIN [dbo].[TBL_geo_ownership_Fragments] 
				ON TBL_geo_ownership_Fragments.ColumnSearchAlias = #Searches.ColumnSearchAlias
				AND TBL_geo_ownership_Fragments.[SearchTextFragment] LIKE #Searches.SearchTextFragment
			INNER JOIN dbo.permit_main ON permit_main.loc_recordid = [TBL_geo_ownership_Fragments].recordid
		WHERE 
			TBL_geo_ownership_Fragments.ColumnSearchAlias = 'site_alternate_id'
		) AS tt
	OPTION (MAXDOP 1)

	----Search for project_main
	INSERT INTO #TT
	SELECT *
	FROM (
	SELECT dbo.project_main.recordid
		,[TBL_project_main_Fragments].[SearchTextFragment]
	FROM [#Searches]
		INNER JOIN [TBL_project_main_Fragments] 
			ON [TBL_project_main_Fragments].ColumnSearchAlias = #Searches.ColumnSearchAlias
			AND [TBL_project_main_Fragments].[SearchTextFragment] LIKE #Searches.SearchTextFragment
		INNER JOIN project_main 
			ON project_main.RECORDID = [TBL_project_main_Fragments].RECORDID
	) AS tt
	OPTION (force order, MAXDOP 1)

	INSERT INTO #TT
	SELECT *
	FROM (
		SELECT dbo.project_main.recordid
			,[TBL_geo_ownership_Fragments].[SearchTextFragment]
		FROM [#Searches]
			INNER LOOP JOIN [dbo].[TBL_geo_ownership_Fragments] 
				ON TBL_geo_ownership_Fragments.ColumnSearchAlias = #Searches.ColumnSearchAlias
				AND TBL_geo_ownership_Fragments.[SearchTextFragment] LIKE #Searches.SearchTextFragment
			INNER JOIN dbo.project_main 
				ON project_main.loc_recordid = [TBL_geo_ownership_Fragments].recordid
		WHERE 
			TBL_geo_ownership_Fragments.ColumnSearchAlias = 'site_alternate_id'
	) AS tt
	OPTION (MAXDOP 1)

	----Search for license2_main
	INSERT INTO #TT
	SELECT *
	FROM (
		SELECT dbo.license2_main.recordid
			,[TBL_license2_main_Fragments].[SearchTextFragment]
		FROM dbo.license2_main
			INNER JOIN [dbo].[TBL_license2_main_Fragments] WITH (forceseek) 
				ON license2_main.RECORDID = [TBL_license2_main_Fragments].RECORDID
			INNER JOIN [#Searches] 
				ON [TBL_license2_main_Fragments].ColumnSearchAlias = #Searches.ColumnSearchAlias
				AND [TBL_license2_main_Fragments].[SearchTextFragment] LIKE #Searches.SearchTextFragment
	) AS tt

	INSERT INTO #TT
	SELECT *
	FROM (
		SELECT dbo.license2_main.recordid
			,[TBL_geo_ownership_Fragments].[SearchTextFragment]
		FROM [#Searches]
			INNER LOOP JOIN [dbo].[TBL_geo_ownership_Fragments] 
				ON TBL_geo_ownership_Fragments.ColumnSearchAlias = #Searches.ColumnSearchAlias
				AND TBL_geo_ownership_Fragments.[SearchTextFragment] LIKE #Searches.SearchTextFragment
			INNER JOIN dbo.license2_main 
				ON license2_main.loc_recordid = [TBL_geo_ownership_Fragments].recordid
		WHERE 
			TBL_geo_ownership_Fragments.ColumnSearchAlias = 'site_alternate_id' 
	) AS tt
	OPTION (MAXDOP 1)

	----Search for crm_issues
	INSERT INTO #TT
	SELECT *
	FROM (
		SELECT dbo.crm_issues.recordid
			,[TBL_crm_issues_Fragments].[SearchTextFragment]
		FROM dbo.crm_issues
			INNER JOIN [dbo].[TBL_crm_issues_Fragments] WITH (forceseek) 
				ON crm_issues.RECORDID = [TBL_crm_issues_Fragments].RECORDID
			INNER JOIN [#Searches] 
				ON [TBL_crm_issues_Fragments].ColumnSearchAlias = #Searches.ColumnSearchAlias
				AND [TBL_crm_issues_Fragments].[SearchTextFragment] LIKE #Searches.SearchTextFragment
	) AS tt
		

	INSERT INTO #TT
	SELECT *
	FROM (
		SELECT dbo.crm_issues.recordid
			,[TBL_geo_ownership_Fragments].[SearchTextFragment]
		FROM [#Searches]
			INNER LOOP JOIN [dbo].[TBL_geo_ownership_Fragments] 
				ON TBL_geo_ownership_Fragments.ColumnSearchAlias = #Searches.ColumnSearchAlias
				AND TBL_geo_ownership_Fragments.[SearchTextFragment] LIKE #Searches.SearchTextFragment
			INNER JOIN dbo.crm_issues
				ON crm_issues.ISSUE_LOC_RECORDID = [TBL_geo_ownership_Fragments].recordid
		WHERE 
			TBL_geo_ownership_Fragments.ColumnSearchAlias = 'site_alternate_id'
			OR TBL_geo_ownership_Fragments.ColumnSearchAlias = 'owner_name'
			OR TBL_geo_ownership_Fragments.ColumnSearchAlias = 'site_subdivision'
	) AS tt

	--Search for geo
	INSERT INTO #TT
	SELECT *
	FROM (
		SELECT dbo.geo_ownership.recordid
			,[TBL_geo_ownership_Fragments].[SearchTextFragment]
		FROM [#Searches]
			INNER JOIN [TBL_geo_ownership_Fragments] WITH (FORCESEEK)
				ON [TBL_geo_ownership_Fragments].ColumnSearchAlias = #Searches.ColumnSearchAlias
				AND [TBL_geo_ownership_Fragments].[SearchTextFragment] LIKE #Searches.SearchTextFragment
			INNER JOIN  [dbo].geo_ownership
				ON [TBL_geo_ownership_Fragments].RECORDID = geo_ownership.RECORDID 
	) AS tt

	--Search for aec_main
	INSERT INTO #TT
	SELECT *
	FROM (
		SELECT dbo.aec_main.recordid
			,[TBL_aec_main_Fragments].[SearchTextFragment]
		FROM [dbo].[TBL_aec_main_Fragments] WITH (forceseek)
			INNER JOIN dbo.aec_main 
				ON [TBL_aec_main_Fragments].RECORDID = aec_main.RECORDID
			INNER JOIN [#Searches] 
				ON [TBL_aec_main_Fragments].ColumnSearchAlias = #Searches.ColumnSearchAlias
				AND [TBL_aec_main_Fragments].[SearchTextFragment] LIKE #Searches.SearchTextFragment
	) AS tt

	-- Get details and apply ExcludedGroup if any
	SELECT DISTINCT 
		 RecordID
		,Loc_RecordID
		,Tgroup
		,ActivityNo
		,Site_APN
		,Site_Addr
		,site_city
	FROM tvw_GblActivities
	WHERE 
		RECORDID IN (SELECT DISTINCT recordid FROM #tt)
		AND tgroup NOT IN (SELECT * FROM #ExcludeGroup)
		
END
GO
