USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_SSRS_GetCaseFees]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_SSRS_GetCaseFees]
GO
/****** Object:  StoredProcedure [dbo].[tsp_SSRS_GetCaseFees]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  					  
					  					  					  
CREATE PROCEDURE [dbo].[tsp_SSRS_GetCaseFees]
	@CASE_NO VARCHAR(30)
		
AS
BEGIN

--RB Check if table exists before creating it
   IF OBJECT_ID('tempdb..#table1') IS NOT NULL  
	drop table #table1

create table #table1
	(RECORD_NO varchar(20),
	RECORD_TYPE varchar(20),
	CODE varchar(30),
	ITEM varchar(30),
	PARENT_DESCRIPTION varchar(100),
	DESCRIPTION varchar(100),
	ACCOUNT varchar(30),
	QUANTITY float,
	AMOUNT float,
	PAID_AMOUNT float,
	PAID_DATE date,
	RECEIPT_NO varchar(30),
	CHECK_NO varchar(30),
	PAY_METHOD varchar(30),
	PAID_BY varchar(100),
	COLLECTED_BY varchar(100),
	FEETYPE varchar(10),
	DEPOSITID varchar(30),
	SITE_ADDR varchar(100),
	SITE_APN varchar (100)) ;

create index #table1_ix1 on #table1 (RECORD_NO);

--TESTING ONLY---------------------------------------------------------
--Truncate Table #table1
--DECLARE @RECEIPT_NO varchar(30)
--SET @RECEIPT_NO = '1029415-01'
--Select * from #Table1 order by RECORD_NO, CODE, ITEM
--TESTING ONLY---------------------------------------------------------


--CASES--------------------------------------------------------------------------------------------------------------------------------------------
BEGIN
	insert into #table1 (RECORD_NO, RECORD_TYPE, CODE, ITEM, DESCRIPTION, ACCOUNT, QUANTITY, AMOUNT, PAID_AMOUNT, PAID_DATE, RECEIPT_NO, CHECK_NO, SITE_ADDR, SITE_APN, PAY_METHOD, COLLECTED_BY, FEETYPE, DEPOSITID, PAID_BY)
	select Case_Fees.CASE_NO, 'CodeTRAK', Case_Fees.CODE, NULL, Case_Fees.DESCRIPTION, Case_Fees.ACCOUNT, Case_Fees.QUANTITY, Case_Fees.AMOUNT, Case_Fees.PAID_AMOUNT, 
		Case_Fees.PAID_DATE, Case_Fees.RECEIPT_NO, Case_Fees.CHECK_NO, Case_Main.SITE_ADDR, Case_Main.SITE_APN,
		Case_Fees.PAY_METHOD, Case_Fees.COLLECTED_BY, Case_Fees.FEETYPE, Case_Fees.DEPOSITID, Case_Fees.PAID_BY
	from Case_Fees inner join Case_Main on Case_Main.CASE_NO = Case_Fees.CASE_NO
	where DETAIL = 0 and Case_Main.CASE_NO = @CASE_NO and AMOUNT > 0
	UNION ALL
	select Case_SubFees.CASE_NO, 'CodeTRAK', Case_SubFees.CODE, Case_SubFees.ITEM, Case_SubFees.DESCRIPTION, Case_SubFees.ACCOUNT, Case_SubFees.QUANTITY, Case_SubFees.AMOUNT, NULL, 
		Case_Fees.PAID_DATE as PAID_DATE, Case_Fees.RECEIPT_NO as RECEIPT_NO, Case_Fees.CHECK_NO as CHECK_NO, Case_Main.SITE_ADDR, Case_Main.SITE_APN,
		Case_Fees.PAY_METHOD, Case_Fees.COLLECTED_BY, Case_Fees.FEETYPE, Case_Fees.DEPOSITID, Case_Fees.PAID_BY
	from Case_SubFees inner join Case_Fees on Case_Fees.RECORDID = Case_SubFees.PARENTID
	inner join Case_Main on Case_Main.CASE_NO = Case_SubFees.CASE_NO
	where PARENTID in (Select RECORDID from Case_Fees where DETAIL = 1 and Case_Fees.CASE_NO = @CASE_NO)
	and Case_SubFees.AMOUNT > 0 ;
END



--Offset any Code Deposit Reductions	
insert into #table1
	( RECORD_NO, RECORD_TYPE, ACCOUNT, CODE, AMOUNT, PAID_AMOUNT, PAID_DATE, RECEIPT_NO, CHECK_NO, PAY_METHOD, FEETYPE, DEPOSITID, COLLECTED_BY)
select
	  RECORD_NO, RECORD_TYPE, (select ACCOUNT from Case_Fees where Case_Fees.RECORDID = #table1.DEPOSITID), CODE, (AMOUNT*-1), (PAID_AMOUNT*-1),  PAID_DATE, 'OFFSET', CHECK_NO, 'OFFSET', 'OFFSET', DEPOSITID, COLLECTED_BY
from #table1 
	where RECORD_TYPE = 'CodeTrak'
	and DEPOSITID in 
		(select RECORDID from Case_Fees 
			where CASE_NO = #table1.RECORD_NO 
			and FEETYPE = 'DEPOSIT')
	and FEETYPE <> 'REFUND' ;
	

-- Code Trak
update #table1 set PARENT_DESCRIPTION = 
	(select max(DESCRIPTION) from Case_Fees where CASE_NO = #table1.RECORD_NO
		and CASE_NO = @CASE_NO 
		and CODE = #table1.CODE)
	where RECORD_TYPE = 'CodeTRAK' ;


--Update Credits and Refunds
update #table1 set #table1.AMOUNT = #table1.AMOUNT * -1 where (CODE = 'REFUND' or FEETYPE = 'REFUND') ;
update #table1 set #table1.AMOUNT = #table1.AMOUNT * -1 where (CODE = 'CREDIT' or ITEM = 'CREDIT') ;
update #table1 set #table1.PAID_AMOUNT = #table1.PAID_AMOUNT * -1 where (CODE = 'REFUND' or FEETYPE = 'REFUND') ;
update #table1 set #table1.PAID_AMOUNT = #table1.PAID_AMOUNT * -1 where (CODE = 'CREDIT' or ITEM = 'CREDIT') ;
update #table1 set #table1.ACCOUNT = '*NONE*' where (#table1.ACCOUNT is NULL or #table1.ACCOUNT = '') ;

--THIS SELECT NEEDS TO BE HERE FOR SSRS TO GET DATA------------------------------------------
select * from #table1 ;
---------------------------------------------------------------------------------------------
END





GO
