USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GblSearchTasks]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_GblSearchTasks]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GblSearchTasks]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*

tsp_GblSearchTasks 
@UserID = 'RJE',
@vStartDate = '',
@vEndDate = '',
@Top = 0

*/



CREATE Procedure [dbo].[tsp_GblSearchTasks](
@UserID Varchar(30),
@vStartDate Varchar(100) = Null,
@vEndDate Varchar(100) = Null,
@Top INT = Null
)


As

Declare @SQL Varchar(8000)
Declare @SQLTop Varchar(800)
Declare @InspectionsWHERE Varchar(1000)
Declare @ReviewsWHERE Varchar(1000)
Declare @ActionsWHERE Varchar(1000)
Declare @ConditionsWHERE Varchar(1000)
Declare @StartDate Datetime 
Declare @EndDate Datetime
Declare @vTop Varchar(25)


Set @InspectionsWHERE = ''
Set @ReviewsWHERE = ''
Set @ActionsWHERE = ''
Set @ConditionsWHERE = ''


If @Top = 0 Set @Top = Null
--temp logic by mdp to limit total 
If @Top Is Null Set @Top = 25
If @Top Is Null Set @vTop = '' ELSE Set @vTop = ' Top ' + Convert(Varchar,@Top)


Set @StartDate  = Null
Set @EndDate  = Null
If @vStartDate = '' Set @vStartDate = Null
If @vEndDate = '' Set @vEndDate = Null

If @vStartDate Is Not Null Set @StartDate = CONVERT(Datetime,@vStartDate)
If @vEndDate Is Not Null Set @EndDate = CONVERT(Datetime,DATEADD(dd,1,@vEndDate))

Print(@StartDate)
Print(@EndDate)


Create Table #Tasks(
MODULE Varchar(30), 
CATEGORY Varchar(30),
CAT_TYPE Varchar(30),
RECORD_NO Varchar(30),
RECORD_TYPE Varchar(30),
RECORD_STATUS Varchar(30),
SITE_ADDR Varchar(50),
LOC_RECORDID Varchar(30),
SITE_CITY Varchar(30),
ASSIGNEE Varchar(30),
DUE_DATE Datetime,
RECORDID Varchar(30),
SITE_APN Varchar(50),
SITE_ALTERNATE_ID Varchar(50),
SITE_GEOTYPE Varchar(50)
)

Set @SQLTop = '
Insert Into #Tasks(Module,Category,Cat_Type,Record_No,Record_Type,Record_Status,Site_Addr,Loc_RecordID,Site_City,Assignee,Due_Date,RecordID,Site_APN,Site_Alternate_ID,Site_GeoType)
Select ' + @vTop + ' ' 




--Begin: Inspections
Set @InspectionsWHERE = @InspectionsWHERE + '
WHERE S.INSPECTOR = ''' + @USERID + '''
AND S.COMPLETED_DATE Is Null 
AND 
(S.SCHEDULED_DATE < Convert(Varchar,GetDate()) 
OR 
(S.SCHEDULED_DATE >= ' + Convert(Varchar,@STARTDATE) + ' 
AND S.SCHEDULED_DATE < ' + Convert(Varchar,@ENDDATE) + '))'


--Permit_Inspections
Set @SQL = @SQLTop + '
	''PERMIT'' as MODULE, 
	''INSPECTION'' as CATEGORY,
	S.INSPECTIONTYPE as CAT_TYPE,
	S.PERMIT_NO as RECORD_NO,
	M.PERMITTYPE as RECORD_TYPE,
	M.STATUS as RECORD_STATUS,
	M.SITE_ADDR as SITE_ADDR,
	M.LOC_RECORDID as LOC_RECORDID,
	M.SITE_CITY as SITE_CITY,
	S.INSPECTOR as ASSIGNEE,
	S.SCHEDULED_DATE as DUE_DATE,
	S.RECORDID as RECORDID,
	M.SITE_APN as SITE_APN,
	M.SITE_ALTERNATE_ID as SITE_ALTERNATE_ID,
	M.SITE_GEOTYPE as SITE_GEOTYPE
From Permit_Inspections S
JOIN Permit_Main M
 ON S.PERMIT_NO = M.PERMIT_NO
' + @InspectionsWHERE
Print(@SQL)
Exec(@SQL)
		
		
		





Select * From #Tasks





GO
