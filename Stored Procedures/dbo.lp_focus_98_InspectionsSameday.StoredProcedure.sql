USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[lp_focus_98_InspectionsSameday]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[lp_focus_98_InspectionsSameday]
GO
/****** Object:  StoredProcedure [dbo].[lp_focus_98_InspectionsSameday]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* 
--Ychavez 3/11/2014 - FOR FOCUS Measures report
-------------------------------------------------------------------------------------------
--98% of inspections conducted on the day scheduled.
exec lp_focus_98_InspectionsSameday @APPLIED_FROM = '01/01/2014' , @APPLIED_TO = '03/01/2014'
-------------------------------------------------------------------------------------------
*/
CREATE procedure [dbo].[lp_focus_98_InspectionsSameday]
		@APPLIED_FROM datetime = null,
		@APPLIED_TO datetime = null
as
begin

------------------------------------------------------------------------
--98% of inspections conducted on the day scheduled.
--TOTAL NUMBER OF INSPECTIONS IN TIMEFRAME, HOW MANY DONE WITIN ONE DAY OF SCHEDULED DATE
--drop table #TMP_COUNT
--------------------------------------------------------------------------

select DATEDIFF(DAY, SCHEDULED_DATE, COMPLETED_DATE) AS DATE_DIFF, SCHEDULED_DATE, COMPLETED_DATE, PERMIT_MAIN.PERMIT_NO
, CASE WHEN DATEDIFF(DAY, SCHEDULED_DATE, COMPLETED_DATE) < 2  THEN 'WITHIN MEASURE' 
 WHEN  DATEDIFF(DAY, SCHEDULED_DATE, COMPLETED_DATE) >= 2 THEN 'OUT OF MEASURE'

ELSE 'NOT CALCULATED' END AS MEASUREMENT
 INTO #TMP_COUNT
from Permit_Inspections inner join Permit_Main
on Permit_Inspections.PERMIT_NO = Permit_Main.PERMIT_NO
where SCHEDULED_DATE between @APPLIED_FROM and @APPLIED_TO --SAME AS APPLIED DATE
--where SCHEDULED_DATE between '01/01/2014' and '03/01/2014' --SAME AS APPLIED DATE

and INSPECTOR NOT in ('DP', 'EDM', 'JCL', 'JFR', 'JG', 'JS', 'KJ', 'MARK', 'MJ', 'MT', 'PN', 'SMCD') --TAKE OUT ENGINEERING INSPECTORS
and ISNULL(REMARKS,'') not like '%VOIDED%'
and STATUS not IN ('CANCELED', 'VOID')
AND COMPLETED_DATE IS NOT NULL
---------------------------------------------------------------
--***this throws off count if the inspection was canceled. i remove 'not caculated' below.
--josie wants to add in geo_inspections using scheduled date and completed date in this measure
------------------------------------------------------------------------

insert into #TMP_COUNT
select 
DATEDIFF(DAY, SCHEDULED_DATE, COMPLETED_DATE) AS DATE_DIFF, 
SCHEDULED_DATE, COMPLETED_DATE, 'P14-NOPERMIT' as PERMIT_NO
, CASE WHEN DATEDIFF(DAY, SCHEDULED_DATE, COMPLETED_DATE) < 2  THEN 'WITHIN MEASURE' 
 WHEN  DATEDIFF(DAY, SCHEDULED_DATE, COMPLETED_DATE) >= 2 THEN 'OUT OF MEASURE'
 ELSE 'NOT CALCULATED' END AS MEASUREMENT

 FROM  geo_inspections
 where SCHEDULED_DATE between @APPLIED_FROM and @APPLIED_TO --SAME AS APPLIED DATE

-----------------------------------------------------------------------------------------
--DROP TABLE #tmp_calc_measure_tot
-----------------------------------------------------------------------------------------
select SUM(case when MEASUREMENT = 'WITHIN MEASURE' THEN 1 ELSE 0 END) IN_MEASURE_COUNT
 ,SUM(case when MEASUREMENT = 'OUT OF MEASURE' THEN 1 ELSE 0 END) OUT_OF_MEASURE_COUNT
 ,SUM(CASE WHEN PERMIT_NO LIKE '%-%' THEN 1 END) TOTAL_PERMITS
 into #tmp_calc_measure_tot
  from #TMP_COUNT
  where MEASUREMENT != 'NOT CALCULATED'
  
 -----------------------------------------------------------------------------------------
--
-----------------------------------------------------------------------------------------

select SUM(cast(IN_MEASURE_COUNT as decimal)) / Sum(cast(total_permits as decimal)) * 100 as prcnt_insp_sameday, total_permits, IN_MEASURE_COUNT
from #tmp_calc_measure_tot
group by total_permits, IN_MEASURE_COUNT



end
GO
