USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_t9_GetDefaultReviewer]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_t9_GetDefaultReviewer]
GO
/****** Object:  StoredProcedure [dbo].[tsp_t9_GetDefaultReviewer]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Micah Neveu
-- Create date: 4/18/2017
-- Description:	returns the user id of the default reviewer or a blank string
-- =============================================
CREATE PROCEDURE [dbo].[tsp_t9_GetDefaultReviewer] 
	@ReviewType varchar(60),
	@GroupName varchar(60),
	@ReviewerUID varchar(60) out
	
AS
BEGIN
	SET NOCOUNT ON;

	declare @defaultReviewer varchar(60)
	-- set to blank if there isn't one
	set @defaultReviewer = ISNULL((select top(1) Field02 from Prmry_ReviewControl where Field01 = @ReviewType AND Category = 'REVIEW_TYPE' AND GroupName = @GroupName),'')
		
	if (LEN(@defaultReviewer) > 0)
	BEGIN
		SET @ReviewerUID = (select top(1) userid from PRMRY_USERS where UserName = SUBSTRING(@defaultReviewer, 0, CHARINDEX('(' ,@defaultReviewer) - 1))
	END

	IF (@ReviewerUID is null)
	BEGIN
		set @ReviewerUID = ''	
	END

END
GO
