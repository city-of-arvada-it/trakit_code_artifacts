USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GblSearchTimesheets]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_GblSearchTimesheets]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GblSearchTimesheets]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  					  
					  					  					  
/*

tsp_GblSearchTimesheets @UserID='NW',@UserName='Nancy Warrick',@vStartDate='2004-10-14',@vEndDate='2004-10-21'

*/


CREATE Procedure [dbo].[tsp_GblSearchTimesheets] (
@UserID Varchar(6) = Null,
@UserName Varchar(30) = Null,
@vStartDate Varchar(50) = Null,
@vEndDate Varchar(50) = Null,
@RecordID Varchar(100) = Null
)


As

Declare @SQL Varchar(8000)
Declare @WHERE Varchar(5000)
Declare @StartDate Datetime
Declare @EndDate Datetime

If @RecordID = '' Set @RecordID = Null

Set @WHERE = ''
Set @StartDate = Null
Set @EndDate = Null

If @vStartDate = '' Set @vStartDate = Null
If @vEndDate = '' Set @vEndDate = Null

If @vStartDate Is Not Null Set @StartDate = CONVERT(Datetime,@vStartDate)
If @vEndDate Is Not Null Set @EndDate = CONVERT(Datetime,DATEADD(dd,1,@vEndDate))


If @RecordID Is Null
 Begin
	Select * 
	From Prmry_Timesheets
	WHERE 1 = 1
	AND WORKDATE >= @StartDate AND WORKDATE < @EndDate
	AND ( USERID = @UserID OR USERNAME = @UserName )
End
ELSE
Begin
	Select * 
	From Prmry_Timesheets
	WHERE 1 = 1
	AND RecordID = @RecordID
End













GO
