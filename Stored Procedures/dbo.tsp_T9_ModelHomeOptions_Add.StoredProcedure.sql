USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ModelHomeOptions_Add]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_ModelHomeOptions_Add]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ModelHomeOptions_Add]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  
Create PROCEDURE [dbo].[tsp_T9_ModelHomeOptions_Add]               			 
			  @ModelRecordID varchar(10),			  
			  @OptionName varchar(255),	
              @UserID varchar(255) = '',
              @OrderId varchar(255) = '0'
AS
BEGIN

       Declare @date varchar(255) = getdate()

              if @OrderId = '0'
                     begin
                          set  @OrderId = (select count(RecordID) +1  from Prmry_ModelHomeOptions)
                     end

       if @ModelRecordID <> ''
              Begin

					Insert Into Prmry_ModelHomeOptions (ModelRecordID, OptionName,  ORDERID, LAST_MODIFIED_BY, LAST_MODIFIED)
					Values (@ModelRecordID, @OptionName,@OrderId,@UserID,@date)

              end

END





GO
