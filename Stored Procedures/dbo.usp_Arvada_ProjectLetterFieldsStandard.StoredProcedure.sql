USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_ProjectLetterFieldsStandard]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[usp_Arvada_ProjectLetterFieldsStandard]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_ProjectLetterFieldsStandard]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_Arvada_ProjectLetterFieldsStandard]
	@projectno varchar(30)
as
/**********************************************************************************************************************
* Creator:      	 
* Date:         	 
* Title:			 
* Description:  	 
* Examples:    

EXECUTE dbo.usp_Arvada_ProjectLetterFieldsStandard 'DA2017-0004' 
			 
* Modifications:  	
  Developer Name		Date		Description
  ------------------	----------	----------------------------------------------------------- 
*
**********************************************************************************************************************/

set nocount on
 
 --------------------------
 --- Declare Variables   --
 --------------------------
 declare	
	@appliedDate datetime,
	@ProjectList varchar(max),
	@TextBottom nvarchar(max),
	@TextTop nvarchar(max),
	@plannerFull nvarchar(50),
	@plannerFirst nvarchar(50),
	@ComplDate varchar(10),
	@AccDate varchar(10),
	@appName nvarchar(50)
 
 --------------------------
 --- Create Temp Tables  --
 --------------------------

create table #projectsList
(
	project_no varchar(30),
	levelValue int,
	applied datetime
)
	 
 --------------------------
 --- Body of Procedure   --
 --------------------------

 

select 
	@appliedDate = m.applied,
	@plannerFull = m.planner,
	@plannerFirst = case when charindex(' ', m.planner) > 0  then
			substring(m.planner,1,  charindex(' ', m.planner)-1)
			else isnull(m.planner,'')
			end,
	--@ComplDate = convert(Varchar(10),m.STATUS_DATE,101),
	@AccDate = convert(Varchar(10),m.APPLIED,101),
	@appName = app.NAME
from dbo.project_main m
left join dbo.Project_People app with (nolock)
	on app.PROJECT_NO = m.PROJECT_NO
	and app.NAMETYPE = 'applicant'
where m.PROJECT_NO = @projectno

select top 1 @ComplDate = convert(Varchar(10), action_date,101)
from dbo.Project_Actions a
where a.action_Type = 'finalized initial review'
and a.PROJECT_NO  = @projectno
order by 
	ACTION_DATE desc

 

--insert into #projectsList
--(
--	project_no ,
--	levelValue ,
--	applied 
--)
--select
--	project_no ,
--	levelValue ,
--	applied 
--from dbo.[tfn_Arvada_LinkedProjects](@projectno)
--where 
--	datediff(Day,applied,@appliedDate) = 0

--select @ProjectList =
-- stuff(
--			 (select ', '+ p.project_no 
--			  from #projectsList as p  
--			  for xml path(''), root('MyString'), type 
--			  ).value('/MyString[1]','nvarchar(max)') ,1,2,''
--		) 

declare @AccLetter varchar(10),
		@FirstReview varchar(10),
		@FirstResub varchar(10),
		@SecondReview varchar(10),
		@SecondResub varchar(10),
		@PlanningCommDate varchar(10),
		@trk_type varchar(60)

select top 1 @AccLetter = convert(Varchar(10), action_date,101)
from project_actions a
where 
	a.PROJECT_NO  = @projectno
and a.action_Type = 'Acceptance Letter'
order by 
	ACTION_DATE desc


select top 1 @FirstReview = convert(Varchar(10), action_date,101)
from project_actions a
where 
	a.PROJECT_NO  = @projectno
and a.action_Type = '1st Review Complete'
order by 
	ACTION_DATE desc

select top 1 @FirstResub = convert(Varchar(10), action_date,101)
from project_actions a
where 
	a.PROJECT_NO  = @projectno
and a.action_Type = 'Customer 1st Resubmittal'
order by 
	ACTION_DATE desc

select top 1 @SecondReview = convert(Varchar(10), action_date,101)
from project_actions a
where 
	a.PROJECT_NO  = @projectno
and a.action_Type = '2nd Review Complete'
order by 
	ACTION_DATE desc

select top 1 @SecondResub = convert(Varchar(10), action_date,101)
from project_actions a
where 
	a.PROJECT_NO  = @projectno
and a.action_Type = 'Customer 2nd Resubmittal'
order by 
	ACTION_DATE desc

select top 1 @PlanningCommDate = convert(Varchar(10), action_date,101)
from project_actions a
where 
	a.PROJECT_NO  = @projectno
and a.action_Type = 'Planning Commission Hearing'
order by 
	ACTION_DATE desc

select @trk_type = trk_type
from dbo.project_udf as a
where 
	a.PROJECT_NO  = @projectno
 

select 
	@ProjectList as ProjectList,
	m.project_no,
	app.NAME, 
	m.project_name,
	ltrim(rtrim(
	app.NAME +
	case when app.ADDRESS1 is not null and app.ADDRESS1 != '' then  app.ADDRESS1 else '' end +  
	case when app.ADDRESS2 is not null and app.ADDRESS2 != '' then  case when isnull(app.ADDRESS1,'') != '' then char(10) + char(13) else '' end + app.ADDRESS2 else '' end +  
	case when app.CITY is not null and app.CITY != '' then char(10) + char(13) + app.CITY else '' end +
	case when app.[STATE] is not null and app.[STATE] != '' then  ', ' + app.[STATE] else '' end +
	case when app.ZIP is not null and app.ZIP != '' then  ', ' + app.ZIP else '' end))  as ApplicantFullAddress,
	m.planner,

	case when charindex(' ', m.planner) > 0  then
			substring(m.planner,1,  charindex(' ', m.planner)-1)
			else m.planner
			end as PlannerFirstName,
	m.SITE_ADDR,


	
	app.NAME as ApplicantName,

	ltrim(rtrim(
	app.NAME + char(10) + char(13) +
	case when app.ADDRESS1 is not null and app.ADDRESS1 != '' then  app.ADDRESS1 else '' end +  
	case when app.ADDRESS2 is not null and app.ADDRESS2 != '' then  case when isnull(app.ADDRESS1,'') != '' then char(10) + char(13) else '' end  + app.ADDRESS2 else '' end +  
	case when app.CITY is not null and app.CITY != '' then char(10) + char(13) + app.CITY else '' end +
	case when app.[STATE] is not null and app.[STATE] != '' then  ', ' + app.[STATE] else '' end +
	case when app.ZIP is not null and app.ZIP != '' then  ', ' + app.ZIP else '' end))  as ApplicantFullAddressWithName,

	m.project_no + char(10) + char(13) +
	m.project_name + char(10) + char(13) +
	case when m.SITE_ADDR is not null and m.SITE_ADDR != '' then  m.SITE_ADDR else '' end +  
	case when m.SITE_CITY is not null and m.SITE_CITY != '' then char(10) + char(13) + m.SITE_CITY else '' end +
	case when m.SITE_STATE is not null and m.SITE_STATE != '' then  ', ' + m.SITE_STATE else '' end +
	case when m.SITE_ZIP is not null and m.SITE_ZIP != '' then  ', ' + m.SITE_ZIP else '' end  as SiteFullAddress,
	'Missing: ' +
		case when app.NAME is null then 'applicant name,' else '' end
		+  case when m.planner is null then 'planner,' else '' end
		+  case when @AccLetter is null then 'Acceptance Letter Date,' else '' end
		+  case when @trk_type is null then 'Track Type,' else '' end
		+  case when @FirstReview is null then 'First Review,' else '' end
		+  case when m.balance_Due is null then 'Balance Due,' else '' end
		+  case when @FirstResub is null then 'First Resubmittal Date,' else '' end
		+  case when @SecondResub is null then 'Second Resubmittal Date,' else '' end
		+  case when @PlanningCommDate is null then 'Planning Commission Date,' else '' end
		as MissingFields,
'
Dear '+app.NAME +',

The City of Arvada Planning and Development Division has accepted your development application and has assigned it to '+m.planner+', who will be your project planner.  '+m.planner+' will be responsible for processing your application and guiding you through the development review process, and can be reached at 720-898-7435.  

The application was accepted on '+@AccLetter+'.  Your project will be placed on our '+@trk_type+' project review schedule.  The City''s initial review comments on your application will be provided to you on '+@FirstReview+' for this application.

The application fee of $' + convert(varchar(50), m.balance_Due) +' is due now. If the payment has not been completed before the end of the 1st staff review, no comments will be released. Payments can be made online at www.arvadapermits.org by using a credit card or can be paid in person at the planning counter in City Hall.

Your project planner will consolidate review comments from all internal departments and external review agencies and provide a review letter to you via the email address provided as part of your application. Once you have received review comments, please feel free to contact your project planner with any questions you may have or if you would like to schedule a meeting to discuss the comments.  All calls and emails will be returned within 24 hours.

To remain on schedule, your resubmittal will need to be provided to your project planner by 4:00pm on '+@FirstResub+', Resubmittal documents will be attached by you through eTRAKiT.  The second review comments will be due to you on '+@SecondReview+'.   

If an additional review of you project is required following the second review comments, you will need to submit for a third review to your project planner by 4:00pm on '+@SecondResub+'. All resubmittal items are to be submitted electronically through www.arvadapermits.org.
 
If no additional review is necessary following the third submittal, your public hearing before the Planning Commission will take place on '+@PlanningCommDate+'.  Your City Council public hearing date will be scheduled in coordination with you prior to the Planning Commission hearing date.

If it is determined that your project is not ready for a Planning Commission public hearing following the third submittal, you may need to begin the application process again, or if you choose to proceed to the Planning Commission hearing, your application may be recommended for denial.  

Staff is committed to meeting our deadlines for comments.  If you should miss a resubmittal date, your project will no longer be on the designated review schedule outlined in this letter, and will therefore be scheduled for a future Planning Commission meeting when agenda space is available. 

Remember, all submissions will need to be complete and address each of the City review comments provided regarding the previous submittal.  The City reserves the right to reject any submission that is determined to be incomplete or if significant comments are not adequately addressed.

Please remember that all written notice of public hearings must be sent to all property owners within 400 feet of the subject property at least 12 days prior to the hearing date. The site will also need to be posted 15 days prior to the hearing date.  Additionally, at least 30 days prior to the public hearing, written notice of the application must be mailed to any owner of mineral rights associated with the subject property, pursuant to Colorado Revised Statute 24-65.5-103.   These notifications are your responsibility and the lack of proper notification will cause the public hearing date to be postponed.  It is important that you obtain an updated list of adjacent property owners from the county before the notices are sent out.  The posting sign(s) will be provided to you by the City, but it is your responsibility to verify that the sign(s) remain in place prior to the hearing in accordance with the Land Development Code requirements.

For additional information regarding your application or the development review process, please feel free to contact your project planner or visit arvada.org/develop.   Additionally, you can follow the progress on your project through the City’s online permit and project tracking system arvadapermits.org.  Instructions on creating an account and using the system are located at: https://arvada.org/permithelp. If at any time you have concerns about the timing or content of our reviews or have any other questions, you may also call me at 720-898-7345.

We look forward to working with you.

Sincerely,


Rob Smetana, AICP
Manager of City Planning and Development
'
 as LetterText
from dbo.project_main m with (nolock)
left join dbo.Project_People app with (nolock)
	on app.PROJECT_NO = m.PROJECT_NO
	and app.NAMETYPE = 'applicant'
where
	m.project_no = @projectno





 
GO
