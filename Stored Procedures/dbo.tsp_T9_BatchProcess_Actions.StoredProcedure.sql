USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_BatchProcess_Actions]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_BatchProcess_Actions]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_BatchProcess_Actions]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  




                                       

create PROCEDURE [dbo].[tsp_T9_BatchProcess_Actions] 
              @ListActionTypes as UpdateFields readonly,
			  @ListUpdateFields as UpdateFields readonly,
              @ListTableAndRecords as MainTableRecords readonly
              
AS
BEGIN

	Declare @SQL Varchar(8000)
	Declare @TableName varchar(50)
	Declare @KeyField varchar(50)  
	Declare @ColumnName varchar(50) 
	Declare @ColumnValue varchar(255)
	Declare @RowNum int
	Declare @RowId Int
	Declare @AltRowNum int
	Declare @AltRowId Int
	Declare @UserID as varchar(10)
	Declare @TblName as varchar(100)
	Declare @Contact as varchar(100)
	Declare @GroupName as varchar(255)
	Declare @HasNotes Varchar(8000)

	--Drop Tables

	IF object_id('tempdb..#ForInsertActionTypes') is not null drop table #ForInsertActionTypes
	IF object_id('tempdb..#ForInsertIntoPrmryAuditTrail') is not null drop table #ForInsertIntoPrmryAuditTrail
	IF object_id('tempdb..#ListActionTypes') is not null drop table #ListActionTypes
	IF object_id('tempdb..#ListUpdateFields') is not null drop table #ListUpdateFields
	IF object_id('tempdb..#MainTableRec') is not null  drop table #MainTableRec
	IF object_id('tempdb..#ForInsertIntoPrmry_Notes') is not null drop table #ForInsertIntoPrmry_Notes

	Create Table #ForInsertIntoPrmry_Notes(	
		ActivityGroup varchar(50),
		ActivityNo varchar(30), 
		ActivityRecordID varchar(50),
		SubGroup varchar(50) ,
		SubGroupRecordID varchar(30),
		UserID varchar(6) ,
		DateEntered datetime, 
		Notes varchar(7500) NULL)

	--Create tables
	Create Table #ForInsertActionTypes(
		ACTIVITY_NO varchar(15),
		ACTION_DATE datetime,
		ACTION_TYPE varchar(60),
		ACTION_BY varchar(30),
		ACTION_DESCRIPTION varchar(2000),
		COMPLETED_DATE datetime,
		RECORDID varchar(30), 
		ForRecID int identity)

	--Create table to be used for insert into Audit
	Create Table #ForInsertIntoPrmryAuditTrail(
		CURRENT_VALUE varchar(2000),
		FIELD_NAME varchar(50),
		ORIGINAL_VALUE varchar(2000),
		PRIMARY_KEY_VALUE varchar(50),
		SQL_ACTION varchar(1),
		TABLE_NAME varchar(50),
		USER_ID varchar(4), 
		TRANSACTION_DATETIME datetime)

	--Populate table variables
	select * into #ListActionTypes from  @ListActionTypes

	select * into #ListUpdateFields from @ListUpdateFields

	select * into #MainTableRec from @ListTableAndRecords

	-- Start populating table (this should create all combinations for insert)
	insert into #ForInsertActionTypes(ACTIVITY_NO,ACTION_TYPE) 
	select MTR.ActivityNo, lat.Value from #ListActionTypes lat, #MainTableRec MTR

	--Populate other variables
	set @TableName = (Select distinct tblName from @ListTableAndRecords)

	--use function to get key field
	select @KeyField = dbo.tfn_ActivityNoField(@TableName)

	--Update temp table based
	select @RowId=MAX(RowID) FROM #ListUpdateFields     
	Select @RowNum = Count(*) From #ListUpdateFields      
	WHILE @RowNum > 0                          
	BEGIN   

		select @ColumnName = ColumnName from #ListUpdateFields where RowID= @RowId    
		select @ColumnValue = Value from #ListUpdateFields where RowID= @RowId  

		if len(ltrim(rtrim(@ColumnValue))) > 0 and @ColumnName ='UserID'
		Begin
			set @UserID = @ColumnValue
		End

		if len(ltrim(rtrim(@ColumnValue))) > 0 and @ColumnName ='GroupName'
		Begin
			set @GroupName = @ColumnValue
		End 

		if len(ltrim(rtrim(@ColumnValue))) > 0 and @ColumnName ='TblName'
		Begin
			set @TblName = @ColumnValue 
		End 

		if len(ltrim(rtrim(@ColumnValue))) > 0 and @ColumnName ='ACTION_DESCRIPTION'
            Begin
                set @HasNotes =  @ColumnValue
            End 
		
		if len(ltrim(rtrim(@ColumnValue))) > 0 and @ColumnName not in ('UserID', 'GroupName','TblName')
		Begin
			set @SQL = 'Update #ForInsertActionTypes set ' + @ColumnName +  ' = ''' + @ColumnValue + ''''
			exec(@sql)
		End 
                                                 
		select top 1 @RowId=RowID from #ListUpdateFields where RowID < @RowId order by RowID desc
		set @RowNum = @RowNum - 1                             
       
	END

	update #ForInsertActionTypes set RECORDID ='BPRO:' + right(('00' + str(DATEPART(yy, GETDATE()))),1)
	+ right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)
	+ right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
	+ right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)
	+ right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
	+ right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)
	+ right(('000000'+ ltrim(str(ForRecID))),6);



	set @SQL = 'INSERT INTO '
	set @SQL = @SQL +    @TblName 
	set @SQL = @SQL + ' ('
	set @SQL = @SQL +    @KeyField  
	set @SQL = @SQL + ', ACTION_DATE, ACTION_TYPE, ACTION_BY, ACTION_DESCRIPTION, RECORDID, COMPLETED_DATE)
	SELECT        ACTIVITY_NO, ACTION_DATE, ACTION_TYPE, ACTION_BY, ACTION_DESCRIPTION, RECORDID, COMPLETED_DATE
	FROM            #ForInsertActionTypes' 

	print(@SQL)

	exec(@SQL)

	set @SQL = ''
	if len(ltrim(rtrim(@HasNotes))) > 0
	Begin
	
		insert into #ForInsertIntoPrmry_Notes(ActivityNo, Notes, SubGroupRecordID, UserID) select ACTIVITY_NO, ACTION_DESCRIPTION, Recordid, ACTION_BY from #ForInsertActionTypes  
		Update #ForInsertIntoPrmry_Notes set ActivityGroup = (select Value from #ListUpdateFields where ColumnName = 'Groupname')
		Update #ForInsertIntoPrmry_Notes set DateEntered = getdate()
		Update #ForInsertIntoPrmry_Notes set SubGroup = 'ACTION'
		
		set @SQL = 'Update #ForInsertIntoPrmry_Notes set activityrecordid = pm.recordid from '
		set @SQL = @SQL + @TableName 
		set @SQL = @SQL + ' pm inner join #ForInsertIntoPrmry_Notes fi on pm.'
		set @SQL = @SQL + @KeyField
		set @SQL = @SQL + ' = fi.ActivityNo'
		exec(@SQL)

		--Finally
		INSERT INTO prmry_notes (ActivityGroup, ACTIVITYNO, ActivityRecordID, Userid, DateEntered, Notes, SubGroup, SubGroupRecordID)
		SELECT ActivityGroup, ActivityNo, ActivityRecordID, Userid, DateEntered, Notes,SubGroup, SubGroupRecordID
		FROM #ForInsertIntoPrmry_Notes
	end


       --insert into #ForInsertIntoPrmryAuditTrail(TABLE_NAME,PRIMARY_KEY_VALUE ) select tblName, ActivityNo from #MainTableRec

       --insert into Prmry_AuditTrail (CURRENT_VALUE, PRIMARY_KEY_VALUE, SQL_ACTION, TABLE_NAME,TRANSACTION_DATETIME, USER_ID) select @SQL2, PRIMARY_KEY_VALUE, 'U', @TblName, getdate(), @UserID from #ForInsertIntoPrmryAuditTrail


END









GO
