USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ProjectCreateSubPermitLimitation]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_ProjectCreateSubPermitLimitation]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ProjectCreateSubPermitLimitation]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  
-- ==========================================================================================
-- Revision History:
--       2014-09-25 - RGB - added to sourcesafe
-- ==========================================================================================

CREATE Procedure [dbo].[tsp_T9_ProjectCreateSubPermitLimitation](
	   @ConditionID varchar(30),   
	   @StatusLimit bit,
	   @PermitList varchar(max),
	   @GeoList varchar(max),
	   @PermitsAllowed int,
	   @ProcedureAction varchar(25),  
	   @ProjectNo varchar(20)
)

As
 BEGIN

---- TEST ONLY
--Declare @ProjectNo			varchar(20) = 'Z14-0001'

 Declare @NSQLStatement nvarchar(500),	
	     @PreventSubPermit bit, 
         @RecordCount int,	
 	     @PermitLimitNo varchar,
 	     @PermitLimitStatus bit,
 	     @PermitLimitTypes varchar(max),
 	     @PermitLimitGeo varchar(max)
 	     
 Declare @PermitLimitTypesTable TABLE
 	     (
 			ConditionID varchar(30),
 			PermitType varchar(15)
 	     )
 	     
  Declare @PermitLimitGeoTable TABLE
 	     (
 			ConditionID varchar(30),
 			SiteInfo varchar(2000),
 			LocationID varchar(30)
 	     )
 	     
  Declare @PermitLimitNoTable table
		(
			ConditionID varchar(30),
 			LimitNo int
		)
		
  Declare @PermitLimitStatusTable table
		(
			ConditionID varchar(30),
 			[Status] int
		)
		
  Declare @PermitLimitDateSatisfiedTable table
		(
			ConditionID varchar(30),
 			[DateSatisfied] datetime
		)

		if @ProcedureAction = 'UPDATE' --Update
			begin
				if @StatusLimit	 = 1
					begin
	
						set @PermitLimitNo = CAST(@PermitsAllowed as varchar)
					
						Set @NSQLStatement = 'Update  project_conditions2 set SUBPERMIT_CREATION_LIMITATION = 1,
						SUBPERMIT_CREATION_LIMITATION_NO = ' + @PermitLimitNo +
						' where recordID = ' + Char(39) + @ConditionID + Char(39) 
						
						exec (@NSQLStatement)
						
						set @NSQLStatement = ''
						
						--Populate lnk_project_conditions2_permittypes table
						set @NSQLStatement = 'delete from lnk_project_conditions2_permittypes where ConditionID = ' 
											+ Char(39) + @ConditionID + Char(39) 
						
						exec (@NSQLStatement)
						
						set @NSQLStatement = ''
						
						set @PermitLimitTypes = @PermitList
											
						while(CHARINDEX('|',@PermitLimitTypes) > 0)
						begin
							insert into lnk_project_conditions2_permittypes(ConditionID,PermitType)
							select @ConditionID, LTRIM(rtrim(substring(@PermitLimitTypes,1,charindex('|',@PermitLimitTypes)-1)))
							
							set @PermitLimitTypes = SUBSTRING(@PermitLimitTypes,CHARINDEX('|',@PermitLimitTypes) + 1,
													len(@PermitLimitTypes))
						end	
						if LTRIM(rtrim(@PermitLimitTypes)) <> ''
						begin 
							insert into lnk_project_conditions2_permittypes(ConditionID,PermitType)
							select @ConditionID, ltrim(rtrim(@PermitLimitTypes))
						end	
						
						--Populate lnk_project_conditions2_geoownership table
						set @NSQLStatement = 'delete from lnk_project_conditions2_geoownership where ConditionID = ' 
											+ Char(39) + @ConditionID + Char(39) 
						
						exec (@NSQLStatement)
						
						set @NSQLStatement = ''
						
						set @PermitLimitGeo = @GeoList
								
						while(CHARINDEX('|',@PermitLimitGeo) > 0)
						begin
							insert into lnk_project_conditions2_geoownership(ConditionID,LocationID)
							select @ConditionID, ltrim(rtrim(substring(@PermitLimitGeo,1,charindex('|',
							@PermitLimitGeo)-1)))
							
							set @PermitLimitGeo = SUBSTRING(@PermitLimitGeo,CHARINDEX('|',@PermitLimitGeo) + 1,
													len(@PermitLimitGeo))
													
						end	
						if LTRIM(rtrim(@PermitLimitGeo)) <> ''
						begin 
							insert into lnk_project_conditions2_geoownership(ConditionID,LocationID)
							select @ConditionID, ltrim(rtrim(@PermitLimitGeo))
						end	
			
					end		
				else
					begin
						Set @NSQLStatement = 'Update  project_conditions2 set SUBPERMIT_CREATION_LIMITATION = 0 
						where recordID = ' + Char(39) + @conditionID + Char(39) 
					end
					exec (@NSQLStatement)
					set @NSQLStatement = ''
			end
		else
		begin --Select
			if @ProjectNo is not null or @ProjectNo <> ''
			   Begin
		   			-- Get Status
		   			
		   			insert into @PermitLimitStatusTable select @ConditionID, ISNULL(SUBPERMIT_CREATION_LIMITATION,0) 
		   			from Project_Conditions2
		   			where RECORDID = @ConditionID			
		   			
		   			insert into @PermitLimitNoTable select @ConditionID, ISNULL(SUBPERMIT_CREATION_LIMITATION_NO,0) 
		   			from Project_Conditions2
		   			where RECORDID = @ConditionID
		   			
					insert into @PermitLimitTypesTable select ConditionID, PermitType from lnk_project_conditions2_permittypes 
					where ConditionID = @ConditionID
					
					insert into @PermitLimitGeoTable Select @ConditionID, b.SITE_ADDR + ' | ' + b.SITE_APN, b.RECORDID from 
										lnk_project_conditions2_geoownership a 
										inner join geo_ownership b on a.LocationID = b.recordid  
										where a.ConditionID = @ConditionID
										
					insert into @PermitLimitDateSatisfiedTable select @ConditionID, DATE_SATISFIED
					from Project_Conditions2 where RECORDID = @ConditionID	
					
					select PermitType from @PermitLimitTypesTable where ConditionID = @ConditionID
					select SiteInfo,LocationID from @PermitLimitGeoTable where ConditionID = @ConditionID
					select [Status] from @PermitLimitStatusTable where ConditionID = @ConditionID
					select LimitNo from @PermitLimitNoTable where ConditionID = @ConditionID	
					select DateSatisfied from @PermitLimitDateSatisfiedTable where ConditionID = @ConditionID						
					
			End			
		end
 end





GO
