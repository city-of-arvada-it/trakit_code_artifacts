USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ParentConditionActNo]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_ParentConditionActNo]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ParentConditionActNo]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  					  
--DECLARE @retval2 varchar(1000)
--exec dbo.tsp_ParentConditionActNo 'ADT:1309041601350000000','PROJECT',@retval = @retval2 output
--select @retval2

CREATE Procedure [dbo].[tsp_T9_ParentConditionActNo]
@RecID varchar(30),@SubGroup varchar(10),@retval varchar(1000) output

As

	   SET NOCOUNT ON
	   
       DECLARE @parentid varchar(30)
       DECLARE @parentgroup varchar(10)
       
       if @SubGroup = 'PERMIT'
		begin	
		    select @parentid = ParentID,@parentgroup = ParentGroup from permit_conditions2 where recordid = @RecID
		    if @parentgroup = 'PERMIT'
		    begin
				select @retval = PERMIT_NO from permit_conditions2 where recordid = @parentid and 
				ltrim(rtrim(@parentid)) <> '' and @parentid is not null
		    end
		    if @parentgroup = 'PROJECT'
		    begin
				select @retval = PROJECT_NO from project_conditions2 where recordid = @parentid and 
				ltrim(rtrim(@parentid)) <> '' and @parentid is not null
		    end
		end
       if @SubGroup = 'PROJECT'
		begin	
			select @parentid = ParentID,@parentgroup = ParentGroup from project_conditions2 where recordid = @RecID
			if @parentgroup = 'PROJECT'
			begin
				select @retval = PROJECT_NO from project_conditions2 where recordid = @parentid and 
				ltrim(rtrim(@parentid)) <> '' and @parentid is not null
			end
		end
			     
   Return







GO
