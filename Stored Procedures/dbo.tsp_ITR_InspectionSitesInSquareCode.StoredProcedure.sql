USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ITR_InspectionSitesInSquareCode]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_ITR_InspectionSitesInSquareCode]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ITR_InspectionSitesInSquareCode]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:                            <Author,,Name>
-- Create date: <Create Date,,>
-- Description:    <Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[tsp_ITR_InspectionSitesInSquareCode] (
      -- Add the parameters for the stored procedure here
--@x FLOAT,
--@y FLOAT,
--@d FLOAT

@xN DECIMAL(18,10),          -- Latitude Northern boundary
@xS DECIMAL(18,10),          -- Latitude Southern boundary
@yW DECIMAL(18,10),          -- Longitude Western boundary
@yE DECIMAL(18,10)     -- Longitude Eastern boundary
)
AS
BEGIN
      -- SET NOCOUNT ON added to prevent extra result sets from
      -- interfering with SELECT statements.
      SET NOCOUNT ON;
      
      SELECT  DISTINCT top 100 loc_recordid,
            --((@xN - @xS)* .5) + @xS as LatMid,
            --@yE - ((@yE - @yW) * .5) As LonMid,
            --LAT,
            --ABS(LAT - (((@xN - @xS)* .5) + @xS)) As LatDiff,
            --LON,
            --ABS(LON - (@yE - ((@yE - @yW) * .5))) As LonDiff,
            ABS(LAT - (((@xN - @xS)* .5) + @xS)) + ABS(LON - (@yE - ((@yE - @yW) * .5))) As TotDiff,
            ROW_NUMBER() OVER(ORDER BY site_addr, ABS(LAT - (((@xN - @xS)* .5) + @xS)) + ABS(LON - (@yE - ((@yE - @yW) * .5)))) as Ranked 
            INTO #tempActivities
      FROM tvw_ITR_Activities
      WHERE (LAT <= @xN AND LAT >= @xS) 
            AND (LON >= @yW AND LON <= @yE) and (ABS(LAT - (((@xN - @xS)* .5) + @xS)) + ABS(LON - (@yE - ((@yE - @yW) * .5)))) <= 100
      
      
      -- Cases
      SELECT vw.loc_recordid, vw.Tgroup, vw.recordid, ACTIVITYNO, vw.site_apn, vw.site_addr, vw.site_city, vw.site_state, vw.site_zip, 
            vw.geotype, LAT, LON, ISNULL(x,0)as x, ISNULL(y,0)as y, RESTRICTIONS.RECORDID as restriction_id, RESTRICTIONS.DATE_ADDED as restriction_add_date,
            RESTRICTIONS.RESTRICTION_TYPE as restriction_type, RESTRICTIONS.RESTRICTION_NOTES as restriction_notes, RESTRICTIONS.OTHER_NOTES as restriction_notes2, 
            DATE1, DATE2, DATE3, DATE4, DATE5, DATE6, DATE7, vw.DatePropertyList,
            vw.status AS [actStatus], ACTIVITYTYPE AS actType, ACTIVITYSUBTYPE as actSubType,
            actMain.CASE_NAME AS [DESCRIPTION], '' AS actJobValue, actMain.SITE_LOT_NO, actMain.SITE_BLOCK, actMain.SITE_SUBDIVISION, actMain.SITE_TRACT,
            actFees.BALANCE_DUE as feeBalance, '' as InspectionType, '' AS Company
      FROM tvw_ITR_Activities vw INNER JOIN #tempActivities temp ON (vw.loc_recordid = temp.loc_recordid)
            LEFT JOIN CASE_MAIN actMain ON actMain.CASE_NO = vw.ACTIVITYNO
            LEFT JOIN CASE_MAIN actFees ON actFees.CASE_NO = vw.ACTIVITYNO
            LEFT JOIN (select RECORDID, DATE_ADDED, DATE_CLEARED, RESTRICTION_TYPE, RESTRICTION_NOTES, OTHER_NOTES, LOC_RECORDID
                              from Geo_RESTRICTIONS2
                              where DATE_CLEARED IS NULL AND RESTRICTION_NOTES NOT LIKE 'VOIDED (%'   
                           UNION
                           select RECORDID, '' AS DATE_ADDED, '' AS DATE_CLEARED, RESTRICTION_TYPE, RESTRICTION_NOTES, OTHER_NOTES, LOC_RECORDID
                            from Geo_RESTRICTIONS 
                            where RESTRICTION_TYPE <> 'NONE') AS RESTRICTIONS ON RESTRICTIONS.LOC_RECORDID = vw.loc_recordid
      WHERE Tgroup = 'CASE' 
            AND (DATE5 IS NOT NULL AND DATE5 <> '' AND IsDate(DATE5) = 1)
            AND (DATE1 IS NULL OR DATE1 = '')
      
            
      ORDER BY loc_recordid, Tgroup, ACTIVITYNO, restriction_id
      
END
GO
