USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[ALP_GetAvailableFeeCodesForLicense]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[ALP_GetAvailableFeeCodesForLicense]
GO
/****** Object:  StoredProcedure [dbo].[ALP_GetAvailableFeeCodesForLicense]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ====================================================================================================================
-- Author:		Erick Hampton
-- Revised:		Feb 20, 2017
-- Description:	Gets the fee codes that can be OR have been assessed against a specified license number
-- ====================================================================================================================
CREATE PROCEDURE [dbo].[ALP_GetAvailableFeeCodesForLicense]

	@LicenseNumber varchar(30) = NULL
	
AS
BEGIN

declare @ParameterSetID int
set @ParameterSetID = isnull((select top 1 ParameterSetID from [ALP_ParameterSetAndRenewalPeriodByLicenseNumber] where license_no=@LicenseNumber), -1)

select distinct [FeeCode] from
(
	select distinct pti.Value1 [FeeCode]
	from prmry_typesinfo pti inner join license2_main lm on pti.typename=lm.license_type
	where pti.groupname='LICENSE2' and pti.category='FEESALLOWABLE' --and pti.Value3='FEE' -- we don't seem to be enforcing this restriction in LicenseTRAK
		and lm.license_no=@LicenseNumber

	union select distinct code [FeeCode] from license2_fees where license_no = @LicenseNumber
	union select distinct item [FeeCode] from license2_subfees where license_no = @LicenseNumber

	-- include fee codes that should always be assessed at renewal
	union 
	select FeeCode
	from [ALP_FeeCodeAssessmentRules] fcar inner join [ALP_RenewalFeeAssessmentRules] rfar on fcar.RenewalFeeAssessmentRuleID = rfar.ID
	where ParameterSetID = @ParameterSetID and rfar.[Rule] = 'ASSESS'

) a

-- remove all fee codes that should never be assessed at renewal
where a.FeeCode not in
(
	select FeeCode
	from [ALP_FeeCodeAssessmentRules] fcar inner join [ALP_RenewalFeeAssessmentRules] rfar on fcar.RenewalFeeAssessmentRuleID = rfar.ID
	where ParameterSetID = @ParameterSetID and rfar.[Rule] = 'IGNORE'
)


END

GO
