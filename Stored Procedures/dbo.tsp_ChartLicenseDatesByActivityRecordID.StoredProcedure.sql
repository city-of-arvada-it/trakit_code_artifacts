USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ChartLicenseDatesByActivityRecordID]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_ChartLicenseDatesByActivityRecordID]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ChartLicenseDatesByActivityRecordID]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  					  
					  					  					  

CREATE Procedure [dbo].[tsp_ChartLicenseDatesByActivityRecordID](
      @RecordID1 varchar(8000) = Null,
      @RecordID2 varchar(8000) = Null,
      @RecordID3 varchar(8000) = Null,
      @RecordID4 varchar(8000) = Null
)

As

Declare @RecID table (RecordID varchar(200)) 

insert @RecID
select r.recordid
from dbo.tfn_TblRecID(@recordid1,@recordid2,@recordid3,@recordid4) r

set nocount on

SELECT  CONVERT(nvarchar(30), EXPIRED, 101) as [Date], 'EXPIRED' as 'DateType', COUNT(EXPIRED) AS 'cnt' FROM LICENSE2_MAIN where RECORDID in (select RECORDID from @recID)  GROUP BY EXPIRED
Union select CONVERT(nvarchar(30), ISSUED, 101) as [Date], 'ISSUED' as 'DateType', COUNT(ISSUED) AS 'cnt' FROM LICENSE2_MAIN where RECORDID in (select RECORDID from @recID) GROUP BY ISSUED
Union select CONVERT(nvarchar(30), LIAB_EXP, 101) as [Date], 'LIAB_EXP' as 'DateType', COUNT(LIAB_EXP) AS 'cnt' FROM LICENSE2_MAIN where RECORDID in (select RECORDID from @recID) GROUP BY LIAB_EXP
Union select CONVERT(nvarchar(30), LIAB_ISS, 101) as [Date], 'LIAB_ISS' as 'DateType', COUNT(LIAB_ISS) AS 'cnt' FROM LICENSE2_MAIN where RECORDID in (select RECORDID from @recID) GROUP BY LIAB_ISS
Union select CONVERT(nvarchar(30), ST_LIC_EXP, 101) as [Date], 'ST_LIC_EXP' as 'DateType', COUNT(ST_LIC_EXP) AS 'cnt' FROM LICENSE2_MAIN where RECORDID in (select RECORDID from @recID) GROUP BY ST_LIC_EXP
Union select CONVERT(nvarchar(30), ST_LIC_ISS, 101) as [Date], 'ST_LIC_ISS' as 'DateType', COUNT(ST_LIC_ISS) AS 'cnt' FROM LICENSE2_MAIN where RECORDID in (select RECORDID from @recID) GROUP BY ST_LIC_ISS
Union select CONVERT(nvarchar(30), W_COMP_EXP, 101) as [Date], 'W_COMP_EXP' as 'DateType', COUNT(W_COMP_EXP) AS 'cnt' FROM LICENSE2_MAIN where RECORDID in (select RECORDID from @recID) GROUP BY W_COMP_EXP
Union select CONVERT(nvarchar(30), W_COMP_ISS, 101) as [Date], 'W_COMP_ISS' as 'DateType', COUNT(W_COMP_ISS) AS 'cnt' FROM LICENSE2_MAIN where RECORDID in (select RECORDID from @recID) GROUP BY W_COMP_ISS
order by [Date], DateType















GO
