USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[usp_arvada_developmentTracking]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[usp_arvada_developmentTracking]
GO
/****** Object:  StoredProcedure [dbo].[usp_arvada_developmentTracking]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_arvada_developmentTracking]

as

/*
exec [usp_arvada_developmentTracking]
*/

create table #actionType
(
	ActionType varchar(60)
)


create table #final
(
		PLANNER nvarchar(500),
	StatusValue varchar(50),
	orderby int
)

insert into #final
(

	PLANNER,
	StatusValue
)
SELECT
	m.PLANNER,
	m.STATUS
from dbo.project_main m
where PROJECTTYPE IN ('DEVELOPMENT APPLICATION', 'PREAPPLICATION')
and STATUS in ('4TH REVIEW', 'CUSTOMER RESP. PEND', 'APPLIED', 'DECISION REVIEW', 'DOCUMENTS PENDING', 'CONDITIONAL APPROVAL', 
'2ND REVIEW', '3RD REVIEW', '1ST REVIEW')
AND STATUS_DATE < DATEADD(day, 180, getdate())
GROUP BY PROJECTTYPE, STATUS, PLANNER
ORDER BY PLANNER ASC





select
	PLANNER,
	StatusValue,
	OrderBy as RowOrderBy,
		case 
			ltrim(rtrim(StatusValue)) 
				when 'APPLIED' then 'Applied'
				when '1ST REVIEW' then '1ST REVIEW'
				when '2ND REVIEW' then '2ND REVIEW'
				when '3RD REVIEW' then '3RD REVIEW'
				when '4TH REVIEW' then '4TH REVIEW'
				when 'DECISION REVIEW' then 'DECISION REVIEW'
				when 'CUSTOMER RESP. PEND' then 'CUSTOMER RESP. PEND'
				when 'CONDITIONAL APPROVAL' then 'CONDITIONAL APPROVAL'
				when 'First Reading' then 'First Reading'
				when 'DOCUMENT PENDING' then 'DOCUMENT PENDING'
				else null end as OTHERSTATUS,
		case 
			ltrim(rtrim(StatusValue)) 
				when 'APPLIED' then 1
				when '1ST REVIEW' then 2
				when '2ND REVIEW' then 3
				when '3RD REVIEW' then 4
				when '4TH REVIEW' then 5
				when 'DECISION REVIEW' then 6
				when 'CUSTOMER RESP. PEND' then 7
				when 'CONDITIONAL APPROVAL' then 8
				when 'First Reading' then 9
				when 'DOCUMENT PENDING' then 10
				else 5 end as columnorderby
from #final
order by 
	PLANNER asc

GO
