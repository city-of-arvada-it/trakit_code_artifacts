USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_StaleLogins]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_StaleLogins]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_StaleLogins]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[tsp_T9_StaleLogins]
AS
BEGIN
	UPDATE pu
	SET LastLogout = GETDATE(),
		LASTCHECKIN = NULL
	FROM Prmry_Users (NOLOCK) AS pu
	JOIN TRAKIT9_login (NOLOCK) AS t ON pu.UserID = t.[UID]
	WHERE t.loginExpireDate IS NOT NULL AND t.loginExpireDate < GETDATE()
		AND LastLogout IS NULL

	UPDATE t
	SET loginID = NULL,
		loginExpireDate = NULL
	FROM Prmry_Users (NOLOCK) AS pu
	JOIN TRAKIT9_login (NOLOCK) AS t ON pu.UserID = t.[UID]
	WHERE t.loginExpireDate IS NOT NULL AND t.loginExpireDate < GETDATE()
END
GO
