USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AutoLicenseNoteSupport]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_AutoLicenseNoteSupport]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AutoLicenseNoteSupport]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  
-- =====================================================================
-- Revision History:
--	RB - 10/1/2015 Initial 

-- =====================================================================   	
CREATE Procedure [dbo].[tsp_T9_AutoLicenseNoteSupport](
	 @PermitNo varchar(20),
	 @PermitRecordID varchar(20),
	 @PermitConditionRecordID varchar(20),
	 @isPermitToLicCondNotes bit, 
	 @isPermitNote bit
)

As
BEGIN
	Declare @SQLStatement varchar(1500)
	
		
	If @isPermitToLicCondNotes = 1 and (@PermitNo is not NULL and @PermitNo <> '') and (@PermitConditionRecordID is not NULL and @PermitConditionRecordID <> '') and 
	   (@PermitRecordID is not NULL and @PermitRecordID <> '')
		Begin

			set @SQLStatement = 'select PN.Notes as NOTES, PM.recordid as ActivityRecordID, PC.recordid as ConditionRecordID from permit_main PM ' + 
                                         ' left join Permit_Conditions2 PC on PM.PERMIT_NO = PC.PERMIT_NO ' + 
                                         ' left join prmry_notes PN on pc.PERMIT_NO = PN.ActivityNo and PN.SubGroupRecordID = pc.RECORDID and PN.ActivityGroup = ' + CHAR(39) + 'PERMIT' + CHAR(39) + 
                                         ' where PM.permit_no = ' + CHAR(39) + @PermitNo + CHAR(39) + ' and PM.recordid = ' + CHAR(39) + @PermitRecordID  + CHAR(39) + 
										 ' and PC.RECORDID = ' + CHAR(39) + @PermitConditionRecordID + CHAR(39)

			print @SQLStatement

			exec (@SQLStatement)

		End
	Else If @isPermitNote = 1 and (@PermitNo is not NULL and @PermitNo <> '') and (@PermitRecordID is not NULL and @PermitRecordID <> '')
		Begin

			set @SQLStatement = 'select PM.permit_no, PN.Notes as NOTES from permit_main PM left join prmry_notes PN on pm.PERMIT_NO = PN.ActivityNo and pn.ActivityGroup  = ' + CHAR(39) + 'PERMIT' + CHAR(39) + 
								' where PM.permit_no = ' + CHAR(39) + @PermitNo  + CHAR(39) + ' and (SubGroup is NULL or SubGroup = ' + CHAR(39) + CHAR(39) + ') and PM.recordid = ' + CHAR(39) + @PermitRecordID  + CHAR(39)

			print @SQLStatement

			exec (@SQLStatement)

		End

End


GO
