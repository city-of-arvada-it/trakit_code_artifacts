USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearch_ListOfSavedSearches]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_AdvSearch_ListOfSavedSearches]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearch_ListOfSavedSearches]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[tsp_T9_AdvSearch_ListOfSavedSearches]

	@UserID varchar(50) = NULL,
	@ReturnHTML bit = 0,
	@FilterList bit = 1, -- filter results by DisplayInUserList = 1
	@PickListOrder int = -1 -- use this order instead of the one specified in user options [PickListOrder] column
AS
BEGIN

if @UserID is not null
begin
	-- get the [PickListOrder] value from user options
	if @PickListOrder = -1 select top 1 @PickListOrder = [PickListOrder] from [Prmry_AdvSearch_UserOptions] where UserID=@UserID
	
	-- identify the order by clause from user options
	declare @OrderBy varchar(max)
	select @OrderBy = case @PickListOrder 
							when 0 then '[SearchName], [RecordID]'
							when 1 then '[Seq], [RecordID]'
							when 2 then '[LastViewedResults] desc, [SearchName], [RecordID]'
							when 3 then '[ViewCount] desc, [SearchName], [RecordID]'
							else '[SearchName], [RecordID]'
						end

	-- we have to use dynamic sql now due to the complexity of the order by clause (dynamic sql also performs better with this anyway)
	declare @sql varchar(max)

	if @ReturnHTML = 1
	begin
		-- return an html string suitable for a select list *** NOTE! Filtered by [DisplayInUserList] ***
		set @sql = 'select ''saved_0_'' + cast(RecordID as nvarchar) [@id], [Description] [@title], SearchName [*] from Prmry_AdvSearch_SavedItem where (UserID = ''' + @UserID + ''') and ((' + cast(@FilterList as varchar) + '=0) or (DisplayInUserList=1)) order by ' + @OrderBy + 'for xml path(''option'')'
	end
	else
	begin
		-- return a table 
		set @sql = 'select RecordID [SearchID], SearchName, [Description], [DisplayInUserList], Seq from Prmry_AdvSearch_SavedItem where (UserID = ''' + @UserID + ''') and ((' + cast(@FilterList as varchar) + '=0) or (DisplayInUserList=1)) order by ' + @OrderBy
	end

	-- execute the sql statement set in one of the conditions above
	exec (@sql)

end

END

GO
