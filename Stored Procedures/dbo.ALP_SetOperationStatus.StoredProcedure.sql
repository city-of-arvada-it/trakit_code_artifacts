USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[ALP_SetOperationStatus]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[ALP_SetOperationStatus]
GO
/****** Object:  StoredProcedure [dbo].[ALP_SetOperationStatus]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- ===================================================================
-- Author:		Erick Hampton
-- Create date: Dec 11, 2016
-- Description:	Sets the status info for an operation in a batch
-- ===================================================================
CREATE PROCEDURE [dbo].[ALP_SetOperationStatus]
	
	@BatchID int = -1,
	@ParameterSetID int = -1,
	@LicenseNumber varchar(20) = '',
	@OperationID int = -1,
	@OperationStatusID int = -1,
	@OperationStart datetime = null,
	@OperationEnd datetime = null

AS
BEGIN

	update [ALP_BatchParameterSetLicenseNumberOperationStatus]
	set 
		  OperationStatusID = @OperationStatusID
		, OperationStart = case when @OperationStart is not null then @OperationStart else OperationStart end
		, OperationEnd = case when @OperationEnd is not null then @OperationEnd else OperationEnd end

	where
		BatchID=@BatchID and 
		ParameterSetID=@ParameterSetID and 
		LicenseNumber=@LicenseNumber and 
		OperationID=@OperationID 

	return @@ROWCOUNT -- so the caller can take action as necessary if no rows or > 1 row was updated based on context 

END

GO
