USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_t9_CheckListRulesRead]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_t9_CheckListRulesRead]
GO
/****** Object:  StoredProcedure [dbo].[tsp_t9_CheckListRulesRead]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  




create Procedure [dbo].[tsp_t9_CheckListRulesRead]
       @List as CheckListRulesType  readonly
as
Begin
       --For now simply read by Type
       Select CR.GroupID, CR.GroupType, CR.SubGroupID, CR.SubGroupType, CR.StaticRuleID, CR.RuleValue, CS.RuleText, CS.RuleCode from ChecklistRules CR 
       Join ChecklistRuleStatic CS
       on CR.StaticRuleID = CS.STaticRuleID
       Join @List L on L.RuleCode = CS.RuleCode
       and CR.GroupID = L.GroupID
       and (CR.GroupType = L.GroupType  or CR.GroupType = 'ALL')
       And (CR.SubGroupID = L.SubGroupID  or CR.SubGroupID = 'ALL')
       And (CR.SubGroupType = L.SubGroupType  or CR.SubGroupType = 'ALL')
              
End




GO
