USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GetDuplicateReceipts]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_GetDuplicateReceipts]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GetDuplicateReceipts]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[tsp_GetDuplicateReceipts] (@receiptNo varchar(50))
AS
BEGIN
	SELECT DISTINCT 'Project: ' + PROJECT_NO AS THE_NUMBER FROM Project_Fees WHERE RECEIPT_NO = @receiptNo
	UNION ALL 
	SELECT DISTINCT 'Permit: ' + PERMIT_NO AS THE_NUMBER FROM Permit_Fees WHERE RECEIPT_NO = @receiptNo
	UNION ALL 
	SELECT DISTINCT 'Case: ' + CASE_NO AS THE_NUMBER FROM Case_Fees WHERE RECEIPT_NO = @receiptNo
	UNION ALL
	SELECT DISTINCT 'License2: ' + LICENSE_NO AS THE_NUMBER FROM License2_Fees WHERE RECEIPT_NO = @receiptNo
	UNION ALL
	SELECT DISTINCT 'Permit Bonds: ' + PERMIT_NO AS THE_NUMBER FROM Permit_Bonds WHERE RECEIPT_NO = @receiptNo
	UNION ALL
	SELECT DISTINCT 'Project Bonds: ' + PROJECT_NO AS THE_NUMBER FROM Project_Bonds WHERE RECEIPT_NO = @receiptNo
END
GO
