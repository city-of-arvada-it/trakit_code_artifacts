USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_GblLinkActions]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_GblLinkActions]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_GblLinkActions]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_GblLinkActions]    Script Date: 9/14/2015 9:42:58 AM ******/

					  
-- =====================================================================
-- Revision History:
--	RB - 08/21/2015 Initial 
-- RB 02/24/2016 modified by rafael

-- =====================================================================   	
CREATE Procedure [dbo].[tsp_T9_GblLinkActions](
       @ParentActivityNo		varchar(30),
	   @ParentGroup			varchar(20),
	   @ChildActivityNo		varchar(30),
	   @ChildGroup			varchar(20),
	   @SearchActivityNo    varchar(30),
	   @SearchGroup			varchar(30),
	   @LoginUserId         varchar(15),
	   @IsParentSearch		bit, 
	   @IsChildSearch	    bit,
	   @IsSearch			bit,  
	   @IsLinkAdd    	    bit,
	   @IsLinkDelete   	    bit    
)

As
BEGIN
 Declare @SQLStatement varchar(500)
 Declare @CurrentDateTime datetime = getdate()

 Set @SQLStatement = null 

 if @IsLinkAdd = 1 
	Begin

		If (@ParentActivityNo is not null and @ParentActivityNo <> '') and (@ParentGroup is not null and @ParentGroup <> '') and
		   (@ChildActivityNo  is not null and @ChildGroup <> '') and (@ChildGroup  is not null and @ChildGroup <> '')
		   Begin			   
				If NOT EXISTS (select * from  	lnk_activities   where  	ParentActivityNo=	@ParentActivityNo and 	ChildActivityNo= @ChildActivityNo and ParentGroup=@ParentGroup and 
					 ChildGroup=@ChildGroup)
					Begin   			
						Set @SQLStatement = 'insert into lnk_activities (ParentActivityNo, ParentGroup , ChildActivityNo , ChildGroup , lastUpdated , lastUpdatedBy)' + 
									' Values (' + Char(39) + @ParentActivityNo + CHAR(39) + ',' + CHAR(39) +  @ParentGroup + CHAR(39) + ',' + CHAR(39) +  @ChildActivityNo + CHAR(39) +
									',' + CHAR(39) + @ChildGroup + CHAR(39) + ',' + 'getdate()' + ',' + CHAR(39) + @LoginUserId + CHAR(39) + ')'
																		
						Exec(@SQLStatement)	
					End				
		   End
   End
else if @IsLinkDelete = 1
	Begin
		if (@ParentActivityNo is not null and @ParentActivityNo <> '') and (@ParentGroup is not null and @ParentGroup <> '') and
		   (@ChildActivityNo  is not null and @ChildGroup <> '') and (@ChildGroup  is not null and @ChildGroup <> '')
			Begin
				set @SQLStatement = 'Delete from lnk_activities where (ParentActivityNo = ' + Char(39) + @ParentActivityNo + CHAR(39) + ' and ParentGroup =  ' + CHAR(39) +  @ParentGroup + CHAR(39) + ') and ( ChildActivityNo = ' + CHAR(39) +  @ChildActivityNo + CHAR(39) +
									' and ChildGroup = ' + CHAR(39) + @ChildGroup + CHAR(39) + ')'

				Exec(@SQLStatement)
			End
    End

else if @IsSearch = 1
	Begin
		if (@SearchActivityNo is not null and @SearchActivityNo <> '') and (@SearchGroup is not null and @SearchGroup <> '') 	
		   Begin					
			  Set @SQLStatement = 'Select LNK.ParentActivityNo, LNK.ChildActivityNo, TV.* from tvw_GblTreeActivities TV left join lnk_activities LNK on TV.ACTIVITYNO = LNK.ChildActivityno where LNK.ParentActivityNo = ' + 
								   CHAR(39) +  @SearchActivityNo + CHAR(39) + ' or LNK.ChildActivityNo = ' + CHAR(39) + @SearchActivityNo + CHAR(39) + ' order by TV.SORT_CRITERIA3 desc'

			  Exec (@SQLStatement)
	     End
	End

else if @IsParentSearch = 1
	Begin
		if (@ChildActivityNo is not null and @ChildActivityNo <> '') and (@ChildGroup is not null and @ChildGroup <> '') 
	
		   Begin
				set @SQLStatement = 'Select ParentActivityNo, ParentGroup, ChildActivityNo, ChildGroup from lnk_activities where ChildActivityNo = ' + Char(39) + @ChildActivityNo + CHAR(39) + ' and ChildGroup =  ' + CHAR(39) +  @ChildGroup + CHAR(39)
	
					Exec(@SQLStatement)
		   End
	End
else if @IsChildSearch = 1
	Begin
		if (@ParentActivityNo is not null and @ParentActivityNo <> '') and (@ParentGroup is not null and @ParentGroup <> '') 
	
		   Begin
				set @SQLStatement = 'Select ParentActivityNo, ParentGroup, ChildActivityNo, ChildGroup from lnk_activities where ParentActivityNo = ' + Char(39) + @ParentActivityNo + CHAR(39) + ' and ParentGroup =  ' + CHAR(39) +  @ParentGroup + CHAR(39)
	
					Exec(@SQLStatement)
		   End
	End
else
	Begin
		Select 'Invalid Function' as [ErrorMessage]

	End




 END



/****** Object:  StoredProcedure [dbo].[tsp_T9_LicenseRenew_Calculations]    Script Date: 8/26/2014 2:36:58 PM ******/
-- including erick changes for GLR as per dipti's request.(not an  email request)
SET ANSI_NULLS ON
GO
