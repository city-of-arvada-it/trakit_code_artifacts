USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ExportStandardTexts]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_ExportStandardTexts]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ExportStandardTexts]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  
-- =====================================================================
-- Revision History:
--	mdm - 12/02/13 - added revision history	
--  mdp - 11/26/13 - 
-- =====================================================================  	

/*
tsp_ExportStandardTexts @DBName='DEV', @FilePath='C:\CRW\iTrakitExports\', @FileName='StandardTextsExport'

*/

CREATE Procedure [dbo].[tsp_ExportStandardTexts](
@DBName Varchar(50),
@FilePath Varchar(1000),
@FileName Varchar(150),
@ToggleXP_CMDShell INT = 1
)

As

--SET NOCOUNT ON

Declare @SQL Varchar(8000)

If @ToggleXP_CMDShell Is Null Set @ToggleXP_CMDShell = 1

IF EXISTS (SELECT * FROM tempdb.sys.objects WHERE object_id = OBJECT_ID(N'tempdb..StandardTexts') AND type in (N'U'))
	Drop Table tempdb..StandardTexts

Create Table tempdb..StandardTexts([GROUPNAME] Varchar(200)
      ,[CATEGORY] Varchar(200)
      ,[TYPENAME] Varchar(200)
      ,[DEFAULTCONTACT] Varchar(200)
      ,[NOTES] Varchar(8000)
      ,[RECORDID] Varchar(200)
      ,[ORDERID] Varchar(200)
      ,[SEQ_NUM] Varchar(200)
      ,[MULTI] Varchar(200)
      ,[ENDCHAR] Varchar(5)
      )


Insert Into tempdb..StandardTexts(GROUPNAME,CATEGORY,TYPENAME,DEFAULTCONTACT,NOTES,RECORDID,ORDERID,SEQ_NUM,MULTI,ENDCHAR)
Values('GROUPNAME','CATEGORY','TYPENAME','DEFAULTCONTACT','NOTES','RECORDID','ORDERID','SEQ_NUM','MULTI','^/>')

Set @SQL = '
Insert Into tempdb..StandardTexts([GROUPNAME],[CATEGORY],[TYPENAME],[DEFAULTCONTACT],[NOTES],[RECORDID],[ORDERID],[SEQ_NUM],[MULTI],[ENDCHAR])
Select 
GROUPNAME=CASE WHEN GROUPNAME = '''' THEN Null ELSE GROUPNAME END,
CATEGORY=CASE WHEN CATEGORY = '''' THEN Null ELSE CATEGORY END,
TYPENAME=CASE WHEN TITLE = '''' THEN Null ELSE TITLE END,
DEFAULTCONTACT=CASE WHEN DEFAULTCONTACT = '''' THEN Null ELSE DEFAULTCONTACT END,
NOTES=CASE WHEN NOTES = '''' THEN Null ELSE NOTES END,
RECORDID=CASE WHEN RECORDID = '''' THEN Null ELSE RECORDID END,
ORDERID=CASE WHEN ORDERID Is Null THEN ''999'' ELSE Convert(Varchar,ORDERID) END,
SEQ_NUM=CASE WHEN SEQ_NUM Is Null THEN ''0'' ELSE Convert(Varchar,SEQ_NUM) END,
MULTI=CASE 
WHEN GROUPNAME IN (''PERMIT'',''PROJECT'',''LICENSE2'') AND CATEGORY = ''CONDITION'' THEN ''1'' 
WHEN GROUPNAME = ''LICENSE'' AND CATEGORY = ''CONDITION'' THEN ''0'' 
WHEN GROUPNAME = ''CASE'' AND CATEGORY IN (''VIOLATION'',''VIOLATIONS'') THEN ''0''
ELSE NULL END,
ENDCHAR = ''^/>''
From ' + @DBName + '..Prmry_StandardTexts
ORDER BY GroupName,TITLE,SEQ_NUM,ORDERID'
--Print(@SQL)
Exec(@SQL)

Set @SQL = '
Insert Into tempdb..StandardTexts([GROUPNAME],[CATEGORY],[TYPENAME],[DEFAULTCONTACT],[NOTES],[RECORDID],[ORDERID],[SEQ_NUM],[MULTI],[ENDCHAR])
Select 
GROUPNAME = ''CASE'',
CATEGORY=CASE WHEN CATEGORY = '''' THEN Null ELSE CATEGORY END,
[TYPENAME]=CASE WHEN TITLE = '''' THEN Null ELSE TITLE END,
DEFAULTCONTACT=Null,
NOTES=CASE WHEN [DESCRIPTION] = '''' THEN Null ELSE [DESCRIPTION] END,
RECORDID=CASE WHEN RECORDID = '''' THEN Null ELSE RECORDID END,
ORDERID=''999'',
SEQ_NUM=''0'',
MULTI=''1'',
ENDCHAR = ''^/>''
FROM ' + @DBName + '..Prmry_ViolationsInfo
WHERE CATEGORY = ''VIOLATION''
ORDER BY Title'
--Print(@SQL)
Exec(@SQL)


Select * From tempdb..StandardTexts

Declare @vDate Varchar(100)
Declare @OutputFileName Varchar(1000)

Set @vDate = Replace(Convert(Varchar,GetDate(),102),'.','_')
Print(@vDate)

--Set @OutputFileName = '"' + @FilePath + @FileName + '_' + @vDate + '.csv"'
Set @OutputFileName = '"' + @FilePath + @FileName + '.csv"'


If @ToggleXP_CMDShell = 1
 Begin
	--Enable xp_cmdshell
	Set @SQL = '
	EXEC sp_configure ''show advanced options'', 1
	RECONFIGURE
	EXEC sp_configure ''xp_cmdshell'', 1
	RECONFIGURE
	'
	Print(@SQL)
	Exec(@SQL)
 End

select @sql = 'bcp "select * from tempdb..StandardTexts" queryout ' + @OutputFileName + ' -c -t"|" -T -S'
+ @@servername
--Print(@SQL)
exec master..xp_cmdshell @sql


--IF EXISTS (SELECT * FROM tempdb.sys.objects WHERE object_id = OBJECT_ID(N'tempdb..StandardTexts') AND type in (N'U'))
--	Drop Table tempdb..StandardTexts
	

Set @SQL = '
DELETE ' + @DBName + '.dbo.iTrakitFeeds Where [FileName] = ''' + @FileName + '.csv''

INSERT INTO ' + @DBName + '.dbo.iTrakitFeeds([filename],[data])
SELECT ''' + @FileName + '.csv'', * FROM OPENROWSET(
  BULK N''' + REPLACE(@OutputFileName,'"','') + ''', SINGLE_BLOB
) bt'
Print(@SQL)
Exec(@SQL)

If @ToggleXP_CMDShell = 1
 Begin
	-- Disable xp_cmdshell
	Set @SQL = '
	EXEC sp_configure ''xp_cmdshell'', 0
	RECONFIGURE
	EXEC sp_configure ''show advanced options'', 0
	RECONFIGURE
	'
	Print(@SQL)
	Exec(@SQL)
 End









GO
