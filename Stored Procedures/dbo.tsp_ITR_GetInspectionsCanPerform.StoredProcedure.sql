USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ITR_GetInspectionsCanPerform]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_ITR_GetInspectionsCanPerform]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ITR_GetInspectionsCanPerform]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  
-- ===================================================================== 
-- Author:		Rance Richardson
-- Create date: 03/28/2013
-- Description:	Returns Inspections that an Inspector
--				is able to perform, including Inspections
--				by Trade.
-- Revision History:
--	RR - 10/28/13
--	mdm - 11/5/13 - added revision history	
-- =====================================================================  
CREATE PROCEDURE [dbo].[tsp_ITR_GetInspectionsCanPerform](
	@InspectorID varchar(4),
	@GroupName as varchar(12)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT Inspections.InspectionName, Inspections.TRADE as InspectionTrade
	FROM (select Field01 as InspectorId, Prmry_InspectionControl.GroupName, Prmry_InspectorByTrade.Trade 
			from Prmry_InspectionControl left join Prmry_InspectorByTrade on (Prmry_InspectionControl.Field01 = Prmry_InspectorByTrade.USERID)
			where Prmry_InspectionControl.Category = 'INSPECTOR' ) AS Inspectors
		INNER JOIN (select Field01 as InspectionName, GroupName, Trade from Prmry_InspectionControl where Category = 'INSPECTION_TYPE') 
				AS Inspections ON (Inspections.GroupName = Inspectors.GroupName) 	
	WHERE Inspectors.InspectorId = @InspectorID 
		AND Inspections.GroupName = @GroupName
		AND ((Inspections.TRADE = '' OR Inspections.TRADE IS NULL) OR Inspectors.TRADE = Inspections.TRADE)
END





GO
