USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GblAENotesE3]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_GblAENotesE3]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GblAENotesE3]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[tsp_GblAENotesE3](
@NoteID INT,
@UserID Varchar(6),
@ActivityGroup Varchar(50) = Null,
@ActivityNo Varchar(60) = Null,
@SubGroup Varchar(50) = Null,
@SubGroupRecordID Varchar(30) = Null,
@Notes Varchar(7500)
)

As

Declare @SQL Varchar(8000)

If @ActivityGroup = '' Set @ActivityGroup = Null
If @ActivityNo = '' Set @ActivityNo = Null
If @SubGroup = '' Set @SubGroup = Null
If @SubGroup = 'CHRONOLOGY' Set @SubGroup = 'ACTION'
If @SubGroupRecordID = '' Set @SubGroupRecordID = Null

Declare @ActivityRecordID Varchar(40)
Select @ActivityRecordID = RecordID From tvw_GblActivitiesE3 Where ActivityNo = @ActivityNo
If @ActivityRecordID Is Null
	Select @ActivityRecordID = RecordID From Geo_Ownership Where RecordID = @ActivityNo
If @ActivityRecordID Is Null
	Select @ActivityRecordID = RecordID From CRM_Issues Where Convert(Varchar,Issue_ID) = @ActivityNo


If @NoteID = 0
 Begin
	Insert Into Prmry_Notes(UserID,DateEntered,ActivityGroup,ActivityRecordID,ActivityNo,SubGroup,SubGroupRecordID,Notes)
	Values(
	@UserID,
	GetDate(),
	@ActivityGroup,
	@ActivityRecordID,
	@ActivityNo,
	@SubGroup,
	@SubGroupRecordID,
	@Notes
	)
 End

If @NoteID > 0
 Begin
	Update Prmry_Notes
	Set 
	UserID = @UserID,
	DateEntered = GetDate(),
	Notes = @Notes
	WHERE NoteID = @NoteID
 End

SET ANSI_NULLS ON
GO
