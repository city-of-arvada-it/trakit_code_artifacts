USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[usp_arvada_ExpirePermits]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[usp_arvada_ExpirePermits]
GO
/****** Object:  StoredProcedure [dbo].[usp_arvada_ExpirePermits]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_arvada_ExpirePermits]

/*
Trakit will not change the status of an expired permit to Expired, it just leaves the date there.
This sets the status and will email the contractor of the status change.
If ran in test, it email someone in the City instead.

exec dbo.usp_arvada_ExpirePermits

*/

as



declare 
		@s int,
		@e int,
		@permit varchar(25),
		@email nvarchar(1000),
		@emailString nvarchar(max),
		@SiteAddr varchar(50),
		@Expired varchar(10)

if object_id('tempdb.dbo.#createAudit') is not null drop table #createAudit
create table #createAudit
(
	PERMIT_NO varchar(25) not null,
	emailAddress nvarchar(1000),
	siteAddr varchar(50),
	Expired varchar(10),
	rid int identity(1,1)
)

--get permits that are expiring 30 days out from today and email people.
insert into #createAudit
(
	permit_no,
	emailAddress,
	siteAddr,
	Expired
)
select distinct
	m.PERMIT_NO,
	nullif(p.EMAIL,''),
	m.SITE_ADDR,
	convert(varchar(10),m.EXPIRED,101)
from dbo.permit_main m
left join dbo.Permit_People as p
	on p.PERMIT_NO = m.PERMIT_NO
	and p.NAMETYPE = 'CONTRACTOR'
where PermitType not in 
	(
	'RIGHT OF WAY',  'PROPTY MAINT', 
	'TEMP', 'BLDG RCPT', 'ENG RCPT', 'FLOODPL DEV', 
	'FOOD TRUCK VENDING', 'HISTORY', 'PLN RCPT', 
	'ROWYARD', 'SITE DEV', 'SPEC EVENT', 'TEMP REVOCABLE', 
	'TEMP USE', 'TRANS MERCH'
	)
and 
	expired <= dateadd(day,30,convert(Date,getdate()))
and 
	expired > convert(date, getdate())
and 
	ltrim(rtrim([status])) not in ('FINALED', 'applied','approved','COISSUE', 'COCISSUE', 'CO ISSUE', 'VOID','WITHDRAWN', 'EXPIRED')
 
--declare @sName nvarchar(50)
--select @sName = @@SERVERNAME
--if ( @sName like '%test%')
--begin
--	update c
--	set emailAddress = 'jhutchens@arvada.org'
--	from #createAudit c
--end


select 
	@s = min(rid),
	@e = max(rid)
from #createAudit

while @s <= @e
begin
	
	select 
		@permit = permit_no,
		@email = emailAddress,		
		@SiteAddr = siteAddr,
		@Expired = Expired
	from #createAudit
	where rid = @s

	--update m
	--set  
	--	Expired_by = 'CRW',
	--	[Status] = 'EXPIRED'
	--from dbo.permit_main m
	--where permit_no = @permit

	if (@email is not null )
	begin
	 
 		select @emailString = 
'Dear Permit Holder,
 
This is to notify you that permit ' + @permit + ' located at '+ @SiteAddr +' will expire on '+ @Expired +'. If the project is ready for inspection, please schedule the appropriate inspection as soon as possible online at www.arvadapermits.org. If inspections are not scheduled and completed before the permit expires, you may be required to obtain a new permit and pay additional fees.  If you cannot gain access due to property owner or occupant refusal, please complete the Notice of Refusal form that is available for download online at www.arvada.org/buildingcontractor. If you have any questions, please contact us at 720-898-7620.
 
Thank you,
 
City of Arvada
Building Inspection Division'


		 EXEC MSDB.DBO.sp_send_dbmail
			  @profile_name =  N'Outgoing Profile', 
			  @recipients = @email,
			  @subject = 'Test',
			  @body = @emailString,
			  @from_address = 'noreply@arvada.org',
			  @execute_query_database = 'msdb'    

	
	end

	set @s = @s + 1
end


 




	                   
GO
