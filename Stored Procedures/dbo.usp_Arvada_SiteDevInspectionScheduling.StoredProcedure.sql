USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_SiteDevInspectionScheduling]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[usp_Arvada_SiteDevInspectionScheduling]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_SiteDevInspectionScheduling]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[usp_Arvada_SiteDevInspectionScheduling]

as


 --------------------------
 --- Create Temp Tables  --
 --------------------------
 if object_id('tempdb..#inspections') is not null drop table #inspections
create table #inspections
(
	permit_no varchar(50),
	result nvarchar(50),
	scheduled_date datetime,
	rid int,
	NextScheduledDate datetime,
	RequiredNextSchedDate datetime,
	completed_Date datetime,
	LastCompletedDate  DateTime,
	InspRecordId varchar(50),
	NextInspRecordId varchar(50),
	MaxRid int
)
	 
 --------------------------
 --- Body of Procedure   --
 --------------------------

 --get all our inspections, they will drop out when they are finaled
insert into #inspections
(
	permit_no ,
	result ,
	scheduled_date ,
	rid,
	completed_Date,
	InspRecordId
)
Select 
	m.permit_no,
	isnull(i.RESULT,''),
	i.SCHEDULED_DATE,
	row_number() over (partition by m.permit_no order by SCHEDULED_DATE) as rid,
	i.completed_Date,
	i.RECORDID
From dbo.Permit_Main m
inner join dbo.Permit_Inspections i
	on i.permit_no = m.permit_no
where
	m.permitType = 'SITE DEV'
and i.InspectionType = 'ROUTINE'
and m.FINALED is null
and m.[STATUS] != 'finaled'


--set the last completed date so we will calculate from that inspection when the next scheduled one ought to be
update i
set LastCompletedDate = b.completed_Date,
	MaxRid = b.rid
from #inspections i
inner join 
	(	
		select
			row_number() over (partition by permit_no order by completed_Date desc, scheduled_date asc) as SortOrder,
			rid,
			completed_Date as completed_Date,
			permit_no
		from #inspections 
		where
			completed_Date is not null
	) as b
	on b.permit_no = i.permit_no
	and b.SortOrder = 1

--if there is no completed date, lets move to the scheduled date prior to today and see what we get, in case nothing got completed
update i
set LastCompletedDate = b.completed_Date,
	MaxRid = b.rid
from #inspections i
inner join 
	(	
		select
			row_number() over (partition by permit_no order by scheduled_date desc) as SortOrder,
			rid,
			completed_Date as completed_Date,
			permit_no
		from #inspections 
		where
			scheduled_date < getdate()
	) as b
	on b.permit_no = i.permit_no
	and b.SortOrder = 1
where
	i.maxrid is null 


--get the next scheduled date, while we have the full list of all inspections
update i
set NextScheduledDate = i2.scheduled_date,
	NextInspRecordId = i2.InspRecordId
from #inspections i 
left join #inspections i2
	on i.permit_no = i2.permit_no
	and i.rid = i2.rid-1
	--and i2.scheduled_date > i.scheduled_date

update i
set NextScheduledDate = null,
	NextInspRecordId = null
from #inspections i 
where NextScheduledDate = scheduled_date

--set the required next scheduled date based off the result of the completed inspection
update i
set RequiredNextSchedDate = case when result in ( 'COMPLIANCE ADVISORY','NOTICE OF NON COMPL') then dateadd(day,7,isnull(LastCompletedDate, SCHEDULED_DATE)) else dateadd(day, 30, isnull(LastCompletedDate,SCHEDULED_DATE)) end
from #inspections i 

 


--remove any prior completed recs that are not our last completed record or any scheduled records, we just want 1 record per completed inspection
delete i
from #inspections i
where
	i.rid != i.MaxRid 
 


--Permit_no, Inspector, Inspection Type, Result, Scheduled Date, Completed
--Date, Permit_Main.Description.
select 
	i.permit_no,
	main.[DESCRIPTION] AS PermitDescription,
	compInsp.UserName as CompletedInspector,
	Scheduled.SCHEDULED_DATE as ScheduledDate,
	schedInsp.UserName as ScheduledInspector,
	I.completed_Date as CompletedDate,
	i.result as CompletedResult,
	i.NextScheduledDate as NextScheduledDate,	
	i.RequiredNextSchedDate,
	datediff(day, I.completed_Date, i.NextScheduledDate) as ActualDaysBetweenInspections,
	datediff(day,I.completed_Date, i.RequiredNextSchedDate) as ReqdDaysBetweenInspections,
	completed.InspectionType as CompletedInspectionType, 
	Scheduled.InspectionType as ScheduledInspectionType
from #inspections i
inner join dbo.Permit_Main as main
	on main.PERMIT_NO = i.permit_no
inner join dbo.Permit_Inspections as completed
	on completed.RECORDID = i.InspRecordId
left join dbo.Permit_Inspections as Scheduled
	on scheduled.RECORDID = i.NextInspRecordId
left join dbo.prmry_users as compInsp
	on compInsp.userid = completed.INSPECTOR
left join dbo.prmry_users as schedInsp
	on schedInsp.userid = Scheduled.INSPECTOR
where
	isnull(datediff(day, I.completed_Date, i.NextScheduledDate),-99) != datediff(day,I.completed_Date, i.RequiredNextSchedDate)
order by 
	i.completed_Date,
	i.permit_no
	



/*
select * 
into prmry_users_04072016

update a
set UserEmailAddress1 = 'jsuk@arvada.org'
from prmry_users a
where UserEmailAddress1 != 'jhutchens@arvada.org'
*/
GO
