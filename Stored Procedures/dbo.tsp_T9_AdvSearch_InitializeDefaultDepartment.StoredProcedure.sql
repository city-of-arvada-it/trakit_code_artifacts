USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearch_InitializeDefaultDepartment]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_AdvSearch_InitializeDefaultDepartment]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearch_InitializeDefaultDepartment]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[tsp_T9_AdvSearch_InitializeDefaultDepartment]

	@DeptID int = -1 out

AS
BEGIN
	-- insert Default DepartmentID
	set @DeptID = (select top 1 DepartmentID from [Prmry_AdvSearch_Departments] (nolock) where DepartmentName = 'Default')

	if @DeptID is null or @DeptID = -1
	begin
		insert into [Prmry_AdvSearch_Departments] ([DepartmentName], [DepartmentLeader]) values ('Default', '<System>')
		set @DeptID = SCOPE_IDENTITY()
	end

	--print @DeptID

	-- insert default searches  *** IMPORTANT *** make sure each search item doesn't already exist before inserting it again
	declare @SearchName varchar(50)
	declare @SearchID int
	declare @InsertDetail bit
	
	-- ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	set @SearchName='Default Permit Search'
	-- ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	set @InsertDetail=0
	set @SearchID = (select top 1 RecordID from [Prmry_AdvSearch_SharedItem] where [DepartmentID]=@DeptID and [SearchName]=@SearchName)
	if @SearchID is null
	begin
		-- insert item into both item and detail
		insert into [Prmry_AdvSearch_SharedItem] ([DepartmentID], [SearchName], [GroupName], [SearchType], [ReadOnly], [Description]) values (@DeptID, @SearchName, 'PERMIT', '', 1, 'Default Permit Search')
		set @SearchID = SCOPE_IDENTITY()
		set @InsertDetail=1
	end
	else
	begin
		-- check if detail records exist for the item
		if (select count(*) from [Prmry_AdvSearch_SharedDetail] where ItemID=@SearchID) = 0 set @InsertDetail=1
	end

	if @InsertDetail=1 -- need to insert detail record?
	begin
		-- insert the detail record
		insert into [Prmry_AdvSearch_SharedDetail] ([ItemID], [TableName], [FieldName], [DisplayName], [Datatype], [Filter], [Value1], [Value2], [Seq]) values (@SearchID, 'Permit_Main', 'Status', 'Status', 'varchar', null, '', '', 0)
	end
	-- ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



	-- TODO: add more searches


END
GO
