USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ITR_ExportGeoFull]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_ITR_ExportGeoFull]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ITR_ExportGeoFull]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  
					  
					  
					  					  					  /*
tsp_ExportGeoFull @DBName='DEV', @FilePath='C:\CRW\iTrakitExports\', @FileName='GeoFullExport'

select 
replace(rtrim(replace(convert(varchar,cast(lat as decimal(30,15))),'0',' ')),' ','0'),
replace(rtrim(replace(convert(varchar,cast(lon as decimal(30,15))),'0',' ')),' ','0'),
convert(varchar,convert(real,lat)),
* 
from geo_ownership
where recordid = 'ADD:100'


*/

-- =====================================================================
-- Revision history: 
--	mdm - 12/10/13 - added to VSS
-- ===================================================================== 


CREATE Procedure [dbo].[tsp_ITR_ExportGeoFull](
@DBName Varchar(50),
@FilePath Varchar(1000),
@FileName Varchar(150),
@ToggleXP_CMDShell INT = 1
)

As

SET NOCOUNT ON

Declare @SQL Varchar(8000)

If @ToggleXP_CMDShell Is Null Set @ToggleXP_CMDShell = 1

IF EXISTS (SELECT * FROM tempdb.sys.objects WHERE object_id = OBJECT_ID(N'tempdb..GeoFullBackup') AND type in (N'U'))
	Drop Table tempdb..GeoFullBackup

Create Table tempdb..GeoFullBackup(Loc_RecordID Varchar(200),Site_APN Varchar(200),Site_Alternate_ID Varchar(200),GeoType Varchar(200),Site_Number Varchar(200),Site_Streetname Varchar(200),Site_Unit_No Varchar(200),Site_City Varchar(200),Site_State Varchar(200),Site_Zip Varchar(200),Owner_Name Varchar(200),Owner_Addr1 Varchar(200),Owner_Addr2 Varchar(200),Owner_City Varchar(200),Owner_State Varchar(200),Owner_Zip Varchar(200),COORD_X Varchar(200),COORD_Y Varchar(200),LAT Varchar(200),LON Varchar(200),SITE_ADDR Varchar(200))


Insert Into tempdb..GeoFullBackup(Loc_RecordID,Site_APN,Site_Alternate_ID,GeoType,Site_Number,Site_Streetname,Site_Unit_No,Site_City,Site_State,Site_Zip,Owner_Name,Owner_Addr1,Owner_Addr2,Owner_City,Owner_State,Owner_Zip,COORD_X,COORD_Y,LAT,LON,SITE_ADDR)
Values('Loc_RecordID','Site_APN','Site_Alternate_ID','GeoType','Site_Number','Site_Streetname','Site_Unit_No','Site_City','Site_State','Site_Zip','Owner_Name','Owner_Addr1','Owner_Addr2','Owner_City','Owner_State','Owner_Zip','COORD_X','COORD_Y','LAT','LON','SITE_ADDR')

Set @SQL = '
Insert Into tempdb..GeoFullBackup(Loc_RecordID,Site_APN,Site_Alternate_ID,GeoType,Site_Number,Site_Streetname,Site_Unit_No,Site_City,Site_State,Site_Zip,Owner_Name,Owner_Addr1,Owner_Addr2,Owner_City,Owner_State,Owner_Zip,COORD_X,COORD_Y,LAT,LON,SITE_ADDR)
Select 
Loc_RecordID=CASE WHEN RecordID = '''' THEN Null ELSE RecordID END,
Site_APN=CASE WHEN Site_APN = '''' THEN Null ELSE Site_APN END,
Site_Alternate_ID=CASE WHEN Site_Alternate_ID = '''' THEN Null ELSE Site_Alternate_ID END,
GeoType=CASE WHEN GeoType = '''' THEN Null ELSE GeoType END,
Site_Number=CASE WHEN Site_Number = '''' THEN Null ELSE Site_Number END,
Site_Streetname=CASE WHEN Site_Streetname = '''' THEN Null ELSE Site_Streetname END,
Site_Unit_No=CASE WHEN Site_Unit_No = '''' THEN Null ELSE Site_Unit_No END,
Site_City=CASE WHEN Site_City = '''' THEN Null ELSE Site_City END,
Site_State=CASE WHEN Site_State = '''' THEN Null ELSE Site_State END,
Site_Zip=CASE WHEN Site_Zip = '''' THEN Null ELSE Site_Zip END,
Owner_Name=CASE WHEN Owner_Name = '''' THEN Null ELSE Owner_Name END,
Owner_Addr1=CASE WHEN Owner_Addr1 = '''' THEN Null ELSE Owner_Addr1 END,
Owner_Addr2=CASE WHEN Owner_Addr2 = '''' THEN Null ELSE Owner_Addr2 END,
Owner_City=CASE WHEN Owner_City = '''' THEN Null ELSE Owner_City END,
Owner_State=CASE WHEN Owner_State = '''' THEN Null ELSE Owner_State END,
Owner_Zip=CASE WHEN Owner_Zip = '''' THEN Null ELSE Owner_Zip END,
COORD_X=replace(rtrim(replace(convert(varchar,cast(COORD_X as decimal(30,15))),''0'','' '')),'' '',''0''),
COORD_Y=replace(rtrim(replace(convert(varchar,cast(COORD_Y as decimal(30,15))),''0'','' '')),'' '',''0''),
LAT=replace(rtrim(replace(convert(varchar,cast(LAT as decimal(30,15))),''0'','' '')),'' '',''0''),
LON=replace(rtrim(replace(convert(varchar,cast(LON as decimal(30,15))),''0'','' '')),'' '',''0''),
SITE_ADDR=CASE WHEN Site_ADDR = '''' THEN Null ELSE Site_ADDR END
From ' + @DBName + '..Geo_Ownership'
--Print(@SQL)
Exec(@SQL)

--Set @SQL = 'Select Loc_RecordID = RecordID,Site_APN,Site_Alternate_ID,GeoType,Site_Number,Site_Streetname,Site_Unit_No,Site_City,Site_State,Site_Zip,Owner_Name,Owner_Addr1,Owner_Addr2,Owner_City,Owner_State,Owner_Zip From ' + @DBName + '..Geo_Ownership'

Declare @vDate Varchar(100)
Declare @OutputFileName Varchar(1000)

Set @vDate = Replace(Convert(Varchar,GetDate(),102),'.','_')
Print(@vDate)

--Set @OutputFileName = '"' + @FilePath + @FileName + '_' + @vDate + '.csv"'
Set @OutputFileName = '"' + @FilePath + @FileName + '.csv"'


If @ToggleXP_CMDShell = 1
 Begin
	--Enable xp_cmdshell
	Set @SQL = '
	EXEC sp_configure ''show advanced options'', 1
	RECONFIGURE
	EXEC sp_configure ''xp_cmdshell'', 1
	RECONFIGURE
	'
	Print(@SQL)
	Exec(@SQL)
 End

select @sql = 'bcp "select * from tempdb..GeoFullBackup" queryout ' + @OutputFileName + ' -c -t"|" -T -S'
+ @@servername
--Print(@SQL)
exec master..xp_cmdshell @sql

Drop Table tempdb..GeoFullBackup

Set @SQL = '
DELETE ' + @DBName + '.dbo.iTrakitFeeds Where [FileName] = ''' + @FileName + '.csv''

INSERT INTO ' + @DBName + '.dbo.iTrakitFeeds([filename],[data])
SELECT ''' + @FileName + '.csv'', * FROM OPENROWSET(
  BULK N''' + REPLACE(@OutputFileName,'"','') + ''', SINGLE_BLOB
) bt'
Print(@SQL)
Exec(@SQL)


If @ToggleXP_CMDShell = 1
 Begin
	-- Disable xp_cmdshell
	Set @SQL = '
	EXEC sp_configure ''xp_cmdshell'', 0
	RECONFIGURE
	EXEC sp_configure ''show advanced options'', 0
	RECONFIGURE
	'
	Print(@SQL)
	Exec(@SQL)
 End












GO
