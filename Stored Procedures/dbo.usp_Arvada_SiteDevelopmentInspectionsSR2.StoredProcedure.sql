USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_SiteDevelopmentInspectionsSR2]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[usp_Arvada_SiteDevelopmentInspectionsSR2]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_SiteDevelopmentInspectionsSR2]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_Arvada_SiteDevelopmentInspectionsSR2]
 
as
/**********************************************************************************************************************
* Creator:      	 
* Date:         	 
* Title:			 
* Description:  	 
* Examples:    

EXECUTE dbo.[usp_Arvada_SiteDevelopmentInspectionsSR2]  
					 
* Modifications:  	
  Developer Name		Date		Description
  ------------------	----------	----------------------------------------------------------- 
*
**********************************************************************************************************************/

set nocount on
 
 --------------------------
 --- Declare Variables   --
 --------------------------

 
 --------------------------
 --- Create Temp Tables  --
 --------------------------

	 
 --------------------------
 --- Body of Procedure   --
 --------------------------
 
select b.NameValue, b.PhoneValue, b.ob
from dbo.Prmry_Users u
inner join 
( 
Select 'Mark Thomas' as NameValue, '720-660-1214' as PhoneValue, 2 as ob
union all
Select 'Jim Schoepflin' as NameValue, '303-435-2664' as PhoneValue, 3 as ob
union all
Select 'Eddie Martinez' as NameValue, '720-614-7362' as PhoneValue, 4 as ob
union all
Select 'Jerry Fragua' as NameValue, '303-435-2649' as PhoneValue, 5 as ob
union all
Select 'Kevin Johnston' as NameValue, '303-598-9228' as PhoneValue, 6 as ob
union all
Select 'Justin Clark' as NameValue, '303-845-2229' as PhoneValue, 7 as ob
union all
Select 'David Peacock' as NameValue, '303-435-2660' as PhoneValue, 8 as ob
union all
Select 'Stephen Trujillo' as NameValue, '720-205-3691' as PhoneValue, 9 as ob
union all
Select 'Simon Hurera' as NameValue, '720-557-7125' as PhoneValue, 10 as ob
) as b
on u.UserName = b.NameValue
where 
	u.UserAccess = 'USER'
GO
