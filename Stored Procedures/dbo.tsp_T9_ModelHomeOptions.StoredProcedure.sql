USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ModelHomeOptions]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_ModelHomeOptions]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ModelHomeOptions]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  
Create PROCEDURE [dbo].[tsp_T9_ModelHomeOptions]
             @ModelRecordID varchar(10)
AS
BEGIN


       if @ModelRecordID <> ''
              Begin

					SELECT  RecordID, ModelRecordID, OptionName, ORDERID FROM Prmry_ModelHomeOptions 					
					where ModelRecordID = @ModelRecordID



              end

END



GO
