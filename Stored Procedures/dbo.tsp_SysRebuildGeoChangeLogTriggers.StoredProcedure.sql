USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_SysRebuildGeoChangeLogTriggers]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_SysRebuildGeoChangeLogTriggers]
GO
/****** Object:  StoredProcedure [dbo].[tsp_SysRebuildGeoChangeLogTriggers]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  
					  
					  					  					  

CREATE Procedure [dbo].[tsp_SysRebuildGeoChangeLogTriggers]

As

SET NOCOUNT ON

DECLARE @SQL varchar(8000)
DECLARE @TABLE_NAME sysname
DECLARE @TableNameVarchar Varchar(100)
DECLARE @RecordID INT

--Drop all existing triggers


--RB Check if table exists before creating it
   IF OBJECT_ID('tempdb..#SQL') IS NOT NULL  
	drop table #SQL

Create Table #SQL(SQLCmd Varchar(8000),RecordID INT Identity)

Insert Into #SQL(SQLCmd)
select 'Drop trigger ' + NAME + ''  
from sys.objects 
where name like 'Geo%ChangeLogTra%' 
AND type = 'TR'
order by name

While Exists ( Select * From #SQL )
Begin
      Select @RecordID = ( Select Min(RecordID) From #SQL )
      Select @SQL = ( Select SQLCmd From #SQL Where RecordID = @RecordID )
      Print(@SQL)
      Exec(@SQL)
      Delete #SQL Where RecordID = @RecordID
End


SELECT @TABLE_NAME = MIN(TABLE_NAME) FROM INFORMATION_SCHEMA.Tables 
WHERE TABLE_TYPE= 'BASE TABLE' 
AND TABLE_NAME in 
(
SELECT distinct st.name as TblName
FROM sys.tables st, sys.indexes si, sys.index_columns sic, sys.columns sc, sys.types styp
WHERE si.object_id = st.object_id
        AND sic.object_id = st.object_id 
        AND sic.index_id = si.index_id 
        AND sc.object_id = st.object_id 
        AND sc.column_id = sic.column_id 
        AND styp.system_type_id = sc.system_type_id
        AND (st.name = 'Geo_Ownership' 
        or st.name = 'Geo_OwnershipUDF'
        or st.name = 'Geo_UDF')
)
        
WHILE @TABLE_NAME IS NOT NULL 
BEGIN

Set @TableNameVarchar = CONVERT(Varchar,@TABLE_NAME)
Print(@TableNameVarchar)

If Exists( SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = @TABLE_NAME AND data_type IN ('image','ntext','text') )
 Begin 
	Select Message = 'Unsupported Field Type in table ' + @TableNameVarchar + ' - NO TRIGGER CREATED for this table.'
	GOTO LoopEnd
 End

SET @sql = 'create trigger ' + @TableNameVarchar + '_ChangeLogTracking on ' + @TableNameVarchar + ' for insert, update, delete
as
declare @bit int ,
@field int ,
@maxfield int ,
@char int ,
@fieldname varchar(128) ,
@TableName varchar(128) ,
@PKCols varchar(1000) ,
@sql varchar(8000), 
@UpdateDate varchar(21) ,
@Type varchar(1) ,
@PKFieldSelect varchar(1000),
@PKValueSelect varchar(1000),
@User varchar(256),
@RecID varchar(50),
@ActivityNo varchar(128),
@Message varchar(256),
@InsDelTableName varchar(100)

select @TableName = ''' + @TableNameVarchar + '''
select @UpdateDate = convert(varchar(8), getdate(), 112) + '' '' + convert(varchar(12), getdate(), 114)
select @User = HOST_NAME()
select @RecID = dbo.tfn_RecordidRowNumGeoChgLog()
select @ActivityNo = dbo.tfn_GeoActivityNoField(@TableName) 

if exists (select * from inserted)
      if exists (select * from deleted)
            select @Type = ''U''
      else
		begin
            select @Type = ''I''
            Set @Message = @TableName + '' RECORD CREATED''
            Set @InsDelTableName = ''#ins''
        end
else
 begin
      select @Type = ''D''
	  Set @Message = @TableName + '' RECORD DELETED''
	  Set @InsDelTableName = ''#del''
 end
      
select * into #ins from inserted
select * into #del from deleted


If @Type IN (''I'',''D'')
 Begin
	set @sql = ''
	Declare @ActivityNoValue varchar(128)
	Select @ActivityNoValue = '' + @ActivityNo + '' From '' + @InsDelTableName + ''
	Insert Into GEO_CHANGE_LOG (RECORDID, LOC_RECORDID, FIELD_CHANGED, OLD_VALUE, NEW_VALUE, ENTRY_DATE, USERID)
	Values('''''' + @RecID + '''''',@ActivityNoValue,'''''' + @Message + '''''',Null,Null,'''''' + @UpdateDate + '''''','''''' + @User + '''''')
	''
	Exec(@sql)

RETURN
 End
 
 
select @PKCols = coalesce(@PKCols + '' and'', '' on'') + '' i.'' + c.COLUMN_NAME + '' = d.'' + c.COLUMN_NAME
from
INFORMATION_SCHEMA.TABLE_CONSTRAINTS pk ,INFORMATION_SCHEMA.KEY_COLUMN_USAGE c
where pk.TABLE_NAME = @TableName 
and CONSTRAINT_TYPE = ''PRIMARY KEY''
and c.TABLE_NAME = pk.TABLE_NAME
and c.CONSTRAINT_NAME = pk.CONSTRAINT_NAME

select @PKFieldSelect = coalesce(@PKFieldSelect+''+'','''') + '''''''' + COLUMN_NAME + '''''''' 
from INFORMATION_SCHEMA.TABLE_CONSTRAINTS pk ,INFORMATION_SCHEMA.KEY_COLUMN_USAGE c
where pk.TABLE_NAME = @TableName
and CONSTRAINT_TYPE = ''PRIMARY KEY''and
c.TABLE_NAME = pk.TABLE_NAME 
and c.CONSTRAINT_NAME = pk.CONSTRAINT_NAME

select @PKValueSelect = coalesce(@PKValueSelect+''+'','''') + ''
convert(varchar(100), coalesce(i.'' + COLUMN_NAME + '',d.'' + COLUMN_NAME + ''))''
from INFORMATION_SCHEMA.TABLE_CONSTRAINTS pk ,    INFORMATION_SCHEMA.KEY_COLUMN_USAGE c   
where  pk.TABLE_NAME = @TableName   and CONSTRAINT_TYPE = ''PRIMARY KEY''   
and c.TABLE_NAME = pk.TABLE_NAME   and c.CONSTRAINT_NAME = pk.CONSTRAINT_NAME 

select @field = 0, @maxfield = max(ORDINAL_POSITION) from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = @TableName 

while @field < @maxfield
begin
select @field = min(ORDINAL_POSITION) from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = @TableName and ORDINAL_POSITION > @field
select @bit = (@field - 1 )% 8 + 1
select @bit = power(2,@bit - 1)
select @char = ((@field - 1) / 8) + 1

if substring(COLUMNS_UPDATED(),@char, 1) & @bit > 0 --or @Type in (''I'',''D'')
begin

select  @RecID = dbo.tfn_RecordidRowNumGeoChgLog()

select @ActivityNo = dbo.tfn_GeoActivityNoField(@TableName)  

select @fieldname = COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME = @TableName and ORDINAL_POSITION = @field 
set @sql = ''insert GEO_CHANGE_LOG (RECORDID, LOC_RECORDID, FIELD_CHANGED, OLD_VALUE, NEW_VALUE, ENTRY_DATE, USERID)''
select @sql = @sql + '' 
select '''''' + @RecID + ''''''''
select @sql = @sql + CASE WHEN @Type = ''I'' THEN '',convert(varchar(1000),i.'' + @ActivityNo + '')'' ELSE '',convert(varchar(1000),d.'' + @ActivityNo + '')'' END
select @sql = @sql + '','''''' + @fieldname + ''''''''
select @sql = @sql + '',substring((convert(varchar(1000),d.'' + @fieldname + '')),1,250)''
select @sql = @sql + '',substring((convert(varchar(1000),i.'' + @fieldname + '')),1,250)''
select @sql = @sql + '','''''' + @UpdateDate + ''''''''
select @sql = @sql + '','''''' + @User + ''''''''
select @sql = @sql + '' from #ins i full outer join #del d''
select @sql = @sql + @PKCols
select @sql = @sql + '' where i.'' + @fieldname + '' <> d.'' + @fieldname 
select @sql = @sql + '' or (i.'' + @fieldname + '' is null and  d.'' + @fieldname + '' is not null)'' 
select @sql = @sql + '' or (i.'' + @fieldname + '' is not null and  d.'' + @fieldname + '' is null)'' 
exec (@sql)
end
end
'
SELECT @sql
EXEC(@sql)


LoopEnd:

SELECT @TABLE_NAME= MIN(TABLE_NAME) FROM INFORMATION_SCHEMA.Tables 
WHERE TABLE_NAME> @TABLE_NAME
AND TABLE_TYPE= 'BASE TABLE' 
--AND TABLE_NAME!= 'sysdiagrams'
--AND TABLE_NAME!= 'Audit'
and TABLE_NAME in (SELECT distinct st.name as TblName
FROM sys.tables st, sys.indexes si, sys.index_columns sic, sys.columns sc, sys.types styp
WHERE si.object_id = st.object_id
        AND sic.object_id = st.object_id 
        AND sic.index_id = si.index_id 
        AND sc.object_id = st.object_id 
        AND sc.column_id = sic.column_id 
       AND styp.system_type_id = sc.system_type_id
        AND (st.name = 'Geo_Ownership' 
        or st.name = 'Geo_OwnershipUDF'
        or st.name = 'Geo_UDF'))
        


END














GO
