USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_Net_CreateBusiness]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_Net_CreateBusiness]
GO
/****** Object:  StoredProcedure [dbo].[tsp_Net_CreateBusiness]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  					  



CREATE PROCEDURE [dbo].[tsp_Net_CreateBusiness] 
      -- Add the parameters for the stored procedure here
      @Recordid Varchar(50) = Null
      ,@CreateBusiness Varchar(1) = 'Y'
      
AS

BEGIN
      -- SET NOCOUNT ON added to prevent extra result sets from
      -- interfering with SELECT statements.
      SET NOCOUNT ON;
      
      -- Initialize Prmry_Numbering if it's not been done
                  IF (SELECT count(*) from Prmry_Numbering where GROUPNAME = 'BUSINESS') = 0
                  BEGIN

                    DECLARE @maxbus INTEGER 
 
                    SELECT @maxbus = max(business_no) FROM dbo.License_Business WHERE business_no <> 999999

                    INSERT INTO Prmry_Numbering (GROUPNAME, LastNumber) values ('BUSINESS', @maxbus)
                   
                  END
      
                  -- Confirm the recordid is not already in the table
      DECLARE @count INT
      
      SELECT @count = COUNT(*) FROM  License_Business WHERE RECORDID = @Recordid 
 
      IF @count = 0 
            
            BEGIN 
            
            -- Set new business number and increment table
            DECLARE @BusNo INTEGER
            
            set @BusNo = dbo.tfn_API_MaxBusinessNo() + 1
                                                UPDATE Prmry_Numbering set LastNumber = @BusNo where GROUPNAME = 'BUSINESS'      
                                                      
            -- Create business (not done when fired from SYNC)
            IF @CreateBusiness = 'Y' 
                                                                BEGIN
                            INSERT INTO License_Business(BUSINESS_NO,RECORDID) VALUES(@BusNo, @Recordid) 
                                                                END
  
            END 
            
END

SELECT @BusNo













GO
