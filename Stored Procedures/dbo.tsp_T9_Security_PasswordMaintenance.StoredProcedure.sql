USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_Security_PasswordMaintenance]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_Security_PasswordMaintenance]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_Security_PasswordMaintenance]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================
-- Author:		Erick Hampton
-- Create date: July 21, 2016
-- Description:	Deletes rows of old passwords that won't be used to validate password reuse
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tsp_T9_Security_PasswordMaintenance]

@UserID varchar(4)

AS
BEGIN
	SET NOCOUNT ON;

	-- only proceed if we are not using active directory authentication
	declare @ACTIVE_DIRECTORY_ENABLED varchar(255)
	select @ACTIVE_DIRECTORY_ENABLED=Value1 from Prmry_Preferences where GroupName='SYSTEM' and Item='ACTIVE_DIRECTORY_ENABLED'
	if @ACTIVE_DIRECTORY_ENABLED='0' or @ACTIVE_DIRECTORY_ENABLED='NO' or @ACTIVE_DIRECTORY_ENABLED='FALSE' or @ACTIVE_DIRECTORY_ENABLED is null
	begin
		-- only proceed if we are enforcing the use of complex passwords
		declare @ENFORCE_COMPLEX_PASSWORDS varchar(255)
		select @ENFORCE_COMPLEX_PASSWORDS=Value1 from Prmry_Preferences where GroupName='SYSTEM' and Item='ENFORCE_COMPLEX_PASSWORDS'
		if @ENFORCE_COMPLEX_PASSWORDS is not null and (@ENFORCE_COMPLEX_PASSWORDS='1' or @ENFORCE_COMPLEX_PASSWORDS='YES' or @ENFORCE_COMPLEX_PASSWORDS='TRUE')
		begin
			-- only proceed if we are requiring a unique password count and duration
			declare @UNIQUE_PASSWORD_COUNT varchar(255)
			select @UNIQUE_PASSWORD_COUNT=Value1 from Prmry_Preferences where GroupName='SYSTEM' and Item='UNIQUE_PASSWORD_COUNT'

			declare @UNIQUE_PASSWORD_DURATION varchar(255)
			select @UNIQUE_PASSWORD_DURATION=Value1 from Prmry_Preferences where GroupName='SYSTEM' and Item='UNIQUE_PASSWORD_DURATION'

			if (@UNIQUE_PASSWORD_COUNT is not null and cast(@UNIQUE_PASSWORD_COUNT as int) > 0)  and (@UNIQUE_PASSWORD_DURATION is not null and cast(@UNIQUE_PASSWORD_DURATION as int) > 0) 
			begin
				-- determine if there are any rows to delete
				declare @count int -- how many rows of password data the user has currently 
				select @count=count(*) from Prmry_Security_UserData where UserID=@UserID
				if @count > cast(@UNIQUE_PASSWORD_COUNT as int)
				begin
					-- delete everything but the most recent rows necessary
					delete from Prmry_Security_UserData where UserID=@UserID and ID not in (select top(cast(@UNIQUE_PASSWORD_COUNT as int)) ID from Prmry_Security_UserData where UserID=@UserID order by ID desc)
				end
			end
		end
	end

END
GO
