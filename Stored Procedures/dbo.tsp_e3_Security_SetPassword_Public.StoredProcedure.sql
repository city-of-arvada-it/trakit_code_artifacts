USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_e3_Security_SetPassword_Public]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_e3_Security_SetPassword_Public]
GO
/****** Object:  StoredProcedure [dbo].[tsp_e3_Security_SetPassword_Public]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[tsp_e3_Security_SetPassword_Public]
  @UserID varchar(50)
, @Salt varchar(128)
, @HashedPassword varchar(128)
, @HashedSecretAnswer varchar(128)
, @Creator varchar(10)
, @CreationReason varchar(20)

AS
BEGIN
SET NOCOUNT ON;
	If Exists(select 1 from Prmry_Security_eTRAKiTPublicUserData WHERE eTRAKiT_Name = @UserID)
	Begin
		DELETE FROM [Prmry_Security_eTRAKiTPublicUserData] WHERE eTRAKiT_Name = @UserID
	End
INSERT INTO [Prmry_Security_eTRAKiTPublicUserData]  (etrakit_name, Salt, HashedPassword, HashedSecretAnswer,  Creator, CreationReason, CreationDate) values (@UserID, @Salt, @HashedPassword, @HashedSecretAnswer, @Creator, @CreationReason, getdate())

END

GO
