USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ActivityFees]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_ActivityFees]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ActivityFees]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
				 
					  					  					  					  
					   
/*
tsp_T9_ActivityFees 'permits','B13-0007'
*/
 
create Procedure [dbo].[tsp_T9_ActivityFees](
@Group Varchar(40),
@ActivityNo Varchar(40)
)
 
As
 
SET NOCOUNT ON
 
DECLARE @SQL Varchar(8000)
DECLARE @TableName varchar(40)
DECLARE @SubTableName varchar(40)
DECLARE @KeyField Varchar(40)
 
 
IF EXISTS (SELECT * FROM tempdb.sys.objects WHERE object_id = OBJECT_ID(N'tempdb..ActivityFees') AND type in (N'U'))
       Drop Table tempdb..ActivityFees
 
CREATE TABLE #ActivityFees(
       [GroupName] Varchar(200)
       , [Activity_No] varchar(50)
       , [ChildFee] int
       , [FormulaExists] int
       , [Valuation] float
       , [UDF] float
       , [TotalBalDueForActivity] float
       , [CODE] varchar(12)
       , [DESCRIPTION] varchar(60)
       , [ACCOUNT] varchar(30)
       , [FORMULA] varchar(80)
       , [QUANTITY] float
       , [AMOUNT] float
       , [DETAIL] varchar(1)
       , [PAID_DATE] datetime
       , [PAID_AMOUNT] float
       , [PAY_METHOD] varchar(20)
       , [CHECK_NO] varchar(20)
       , [RECEIPT_NO] varchar(20)
       , [PAID_BY] varchar(30)
       , [PAID] varchar(1)
       , [LOCKID] varchar(30)
       , [RECORDID] varchar(30)
       , [COMMENTS] varchar(10)
       , [ASSESSED_DATE] datetime
       , [ASSESSED_BY] varchar(6)
       , [COLLECTED_BY] varchar(6)
       , [DUE_DATE] datetime
       , [FEETYPE] varchar(8)
       , [DEPOSITID] varchar(30)
       , [PAY_BY_DEPOSIT] varchar(1)
       , [PROFFERID] varchar(30)
       , [BONDID] varchar(30)
       , [INVOICE_DATE] datetime
       , [INVOICE_NO] varchar(30)
       , [INVOICED] varchar(1) 
       , [SubFee_ITEM] varchar(12)
       , [SubFee_ PARENTID] varchar(30)
       )
       
       select @TableName =  CASE
                     When PATINDEX('AEC%',@Group) = 1 then 'AEC_Fees'
                     When PATINDEX('CASE%',@Group) = 1 then 'CASE_Fees'                                         
                     When PATINDEX('LICENSE2%',@Group) = 1 then 'LICENSE2_Fees'
                     when PATINDEX('PERMIT%',@Group) = 1 then 'PERMIT_Fees'
                     when PATINDEX('PROJECT%',@Group) = 1 then 'PROJECT_Fees'
              End
       
       select @SubTableName =  CASE
                     When PATINDEX('AEC%',@Group) = 1 then 'AEC_SubFees'
                     When PATINDEX('CASE%',@Group) = 1 then 'CASE_SubFees'                     
                     When PATINDEX('LICENSE2%',@Group) = 1 then 'LICENSE2_SubFees'
                     when PATINDEX('PERMIT%',@Group) = 1 then 'PERMIT_SubFees'
                     when PATINDEX('PROJECT%',@Group) = 1 then 'PROJECT_SubFees'
              End
       
       Select @KeyField = dbo.tfn_ActivityNoField(@TableName)
       
       --Print(@TableName)
       --Print(@KeyField)
 
Set @SQL = '
Insert Into #ActivityFees(
       [GroupName]
       , [Activity_No]
       , [ChildFee]
       , [FormulaExists]
       , [Valuation]
       , [UDF]
       , [TotalBalDueForActivity]
       , [CODE]
       , [DESCRIPTION]
       , [ACCOUNT]
       , [FORMULA]
       , [QUANTITY]
       , [AMOUNT]
       , [DETAIL]
       , [PAID_DATE]
       , [PAID_AMOUNT]
       , [PAY_METHOD]
       , [CHECK_NO]
       , [RECEIPT_NO]
       , [PAID_BY]
       , [PAID]
       , [LOCKID]
       , [RECORDID]
       , [COMMENTS]
       , [ASSESSED_DATE]
       , [ASSESSED_BY]
       , [COLLECTED_BY]
       , [DUE_DATE]
       , [FEETYPE]
       , [DEPOSITID]
       , [PAY_BY_DEPOSIT]
       , [PROFFERID]
       , [BONDID]
       , [INVOICE_DATE]
       , [INVOICE_NO]
       , [INVOICED] 
       , [SubFee_ITEM]
       , [SubFee_ PARENTID]
       )
       Select 
              ''' + @Group + ''' as GroupName
              , ' + @KeyField + ' as Activity_No
              , 0
              , CASE WHEN DETAIL = 0 and ((FORMULA like ''%qty%'') or (FORMULA like ''%]%'') or (FORMULA like ''%)%'')) THEN 1 ELSE 0 END               
              , 0
              , 0
              , 0
              , CODE
              , DESCRIPTION
              , ACCOUNT
              , FORMULA
              , QUANTITY
              , AMOUNT
              , DETAIL
              , PAID_DATE
              , PAID_AMOUNT
              , PAY_METHOD
              , CHECK_NO
              , RECEIPT_NO
              , PAID_BY
              , PAID
              , LOCKID
              , RECORDID
              , COMMENTS
              , ASSESSED_DATE
              , ASSESSED_BY
              , COLLECTED_BY
              , DUE_DATE
              , FEETYPE
              , DEPOSITID
              , PAY_BY_DEPOSIT
              , PROFFERID
              , BONDID
              , INVOICE_DATE
              , INVOICE_NO
              , INVOICED
              , ''''
              , ''''
              FROM  ' + @TableName + ' WHERE ' + @KeyField + ' = ''' + @ActivityNo + ''';'
 
--Print(@SQL)
Exec(@SQL)
 
 
Set @SQL = '
Insert Into #ActivityFees(
       [GroupName]
       , [Activity_No]
       , [ChildFee]
       , [FormulaExists]
       , [Valuation]
       , [UDF]
       , [TotalBalDueForActivity]
       , [CODE]
       , [DESCRIPTION]
       , [ACCOUNT]
       , [FORMULA]
       , [QUANTITY]
       , [AMOUNT]
       , [DETAIL]
       , [PAID_DATE]
       , [PAID_AMOUNT]
       , [PAY_METHOD]
       , [CHECK_NO]
       , [RECEIPT_NO]
       , [PAID_BY]
       , [PAID]
       , [LOCKID]
       , [RECORDID]
       , [COMMENTS]
       , [ASSESSED_DATE]
       , [ASSESSED_BY]
       , [COLLECTED_BY]
       , [DUE_DATE]
       , [FEETYPE]
       , [DEPOSITID]
       , [PAY_BY_DEPOSIT]
       , [PROFFERID]
       , [BONDID]
       , [INVOICE_DATE]
       , [INVOICE_NO]
       , [INVOICED] 
       , [SubFee_ITEM]
       , [SubFee_ PARENTID]
       )
       Select 
              ''' + @Group + ''' as GroupName
              , ' + @KeyField + ' as Activity_No
              , 1
              , CASE WHEN ((FORMULA like ''%qty%'') or (FORMULA like ''%]%'') or (FORMULA like ''%)%'')) THEN 1 ELSE 0 END    
              , 0
              , 0
              , 0
              , CODE
              , DESCRIPTION
              , ACCOUNT
              , FORMULA
              , QUANTITY
              , AMOUNT
              , null
              , null
              , ''''
              , null
              , null
              , null
              , null
              , PAID
              , LOCKID
              , RECORDID
              , COMMENTS
              , ASSESSED_DATE
              , ASSESSED_BY
              , null
              , null
              , SUBFEETYPE
              , DEPOSITID
              , ''''
              , PROFFERID
              , BONDID
              , null
              , null
              , null
              , ITEM
              , PARENTID
              FROM  ' + @SubTableName + ' WHERE ' + @KeyField + ' = ''' + @ActivityNo + ''';'
 
--Print(@SQL)
Exec(@SQL)
 
Set @SQL = '
UPDATE #ActivityFees set valuation = (select JOBVALUE from Permit_Main where permit_no = ''' + @ActivityNo + ''') WHERE formula like ''%JOBVALUE%'''
--Print(@SQL)
Exec(@SQL)
 
Set @SQL = '
UPDATE #ActivityFees set UDF = (select BLDG_SF from Permit_Main where permit_no = ''' + @ActivityNo + ''') WHERE formula like ''%BLDGSF%'''
--Print(@SQL)
Exec(@SQL)
 
Set @SQL = '
UPDATE #ActivityFees set UDF = (select BLDG2_SF from Permit_Main where permit_no = ''' + @ActivityNo + ''') WHERE formula like ''%BLDG2SF%'''
--Print(@SQL)
Exec(@SQL)
 
Set @SQL = '
UPDATE #ActivityFees set UDF = (select GAR_SF from Permit_Main where permit_no = ''' + @ActivityNo + ''') WHERE formula like ''%GARSF%'''
--Print(@SQL)
Exec(@SQL)
 
Set @SQL = '
UPDATE #ActivityFees set UDF = (select GAR2_SF from Permit_Main where permit_no = ''' + @ActivityNo + ''') WHERE formula like ''%GAR2SF%'''
--Print(@SQL)
Exec(@SQL)
 
Set @SQL = '
UPDATE #ActivityFees set UDF = (select NO_UNITS from Permit_Main where permit_no = ''' + @ActivityNo + ''') WHERE formula like ''%UNITS%'''
--Print(@SQL)
Exec(@SQL)
 
Set @SQL = '
UPDATE #ActivityFees set UDF = (select NO_BLDGS from Permit_Main where permit_no = ''' + @ActivityNo + ''') WHERE formula like ''%BLDGS%'''
--Print(@SQL)
Exec(@SQL)
 
Set @SQL = '
UPDATE #ActivityFees set TotalBalDueForActivity = (select sum(amount) FROM  ' + @TableName + ' WHERE ' + @KeyField + ' = ''' + @ActivityNo + ''' and paid = ''0'')'
--Print(@SQL)
Exec(@SQL)
 
Set @SQL = '
UPDATE #ActivityFees set TotalBalDueForActivity = 0 where TotalBalDueForActivity is null'
--Print(@SQL)
Exec(@SQL)
 
 
Select * from #ActivityFees
 


GO
