USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_export_CustomObj_SearchAndReplace]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_export_CustomObj_SearchAndReplace]
GO
/****** Object:  StoredProcedure [dbo].[tsp_export_CustomObj_SearchAndReplace]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  					  
					  /*
tsp_export_CustomObj_SearchAndReplace ''

tables:
IT = Internal table
S = System base table
TT = Table type
U = Table (user-defined)

constraints:
C = CHECK constraint
D = DEFAULT (constraint or stand-alone)
F = FOREIGN KEY constraint
UQ = UNIQUE constraint
PK = PRIMARY KEY constraint

functions:
FN = SQL scalar function
IF = SQL inline table-valued function
TF = SQL table-valued-function

stored procedures:
P = SQL Stored Procedure

triggers:
TR = SQL DML trigger

views:
V = View

The SQL expression for a DEFAULT constraint, object of type D, is found in the sys.default_constraints catalog view. The SQL expression for a CHECK constraint, object of type C, is found in the sys.check_constraints catalog view.

SELECT sm.object_id, OBJECT_NAME(sm.object_id) AS object_name, o.type, o.type_desc, sm.definition
FROM sys.sql_modules AS sm
JOIN sys.objects AS o ON sm.object_id = o.object_id
ORDER BY o.type

tsp_export_CustomObj_SearchAndReplace 'CRW_REPORTSTEMPDB', 'NEW_REPORTSTEMPDB'

tsp_export_CustomObj_SearchAndReplace

*/

CREATE PROCEDURE [dbo].[tsp_export_CustomObj_SearchAndReplace] (
@SearchText Varchar(1000) = Null,
@ReplaceText Varchar(1000) = Null,
@OutputPath Varchar(1000) = Null
)

AS

BEGIN
  SET NOCOUNT ON
  

If @SearchText Is Null Set @SearchText = ''

--RB Check if table exists before creating it
IF OBJECT_ID('tempdb..#Results') IS NOT NULL  
	drop table #Results

CREATE TABLE #Results(
ResultID INT,
ObjectID INT,
ObjectName Varchar(200),
ObjectType Varchar(10),
ObjectDescription Varchar(200),
SQLDefinition varchar(max) 
)


Insert Into #Results(ObjectID,ObjectName,ObjectType,ObjectDescription,SQLDefinition)
SELECT 
ObjectID = sm.object_id, 
ObjectName = OBJECT_NAME(sm.object_id), 
ObjectType= o.type, 
ObjectDescription = o.type_desc, 
SQLDefinition = sm.definition
FROM sys.sql_modules sm
JOIN sys.objects o
 ON sm.object_id = o.object_id
Where sm.definition like '%' + @SearchText + '%'
AND OBJECT_NAME(sm.object_id) <> 'tsp_export_CustomObj_SearchAndReplace'
ORDER BY ObjectType DESC, ObjectName


--Select * From #Results Order By ObjectType,ObjectDescription

--RB Check if table exists before creating it
IF OBJECT_ID('tempdb..#MyObjectHierarchy') IS NOT NULL  
	drop table #MyObjectHierarchy
  
CREATE TABLE #MyObjectHierarchy 
   (
    HID int identity(1,1) primary key,
    ObjectID int,
    Type int,
    OBJECTTYPE AS CASE 
                             WHEN TYPE =  1 THEN 'FUNCTION' 
                             WHEN TYPE =  4 THEN 'VIEW' 
                             WHEN TYPE = 16 THEN 'PROCEDURE'
                             WHEN TYPE = 256 THEN 'TRIGGER'
                             ELSE ''
                           END,
   ONAME varchar(255), 
   OOWNER varchar(255), 
   SEQ int
   )


--list of objects in dependancy order
  INSERT #MyObjectHierarchy (Type,ONAME,OOWNER,SEQ)
    EXEC sp_msdependencies @intrans = 1 

Update #MyObjectHierarchy SET ObjectId = object_id(OOWNER + '.' + ONAME)

--Select * from #MyObjectHierarchy

   DELETE FROM #MyObjectHierarchy WHERE objectid in(
    SELECT [object_id] FROM sys.synonyms UNION ALL
    SELECT [object_id] FROM master.sys.synonyms)
    
DELETE FROM #MyObjectHierarchy WHERE OBJECTTYPE = ''

--Select * from #MyObjectHierarchy

DELETE moh
FROM #MyObjectHierarchy moh
LEFT JOIN #Results tr
 ON moh.ObjectID = tr.ObjectID
WHERE tr.ObjectID Is Null


Update tr Set
ResultID = moh.HID
From #Results tr
JOIN #MyObjectHierarchy moh
 ON moh.ObjectID = tr.ObjectID
 
 
If @ReplaceText Is Not Null AND @ReplaceText <> ''
 Begin
	Update tr Set
	SQLDefinition = REPLACE(SQLDefinition,@SearchText,@ReplaceText)
	From #Results tr
 End
 
 
Update tr Set
SQLDefinition = 
'
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N''' + tr.ObjectName + '''))
DROP ' + moh.ObjectType + ' ' + tr.ObjectName + '
GO

' + SQLDefinition + '
GO

'
From #Results tr
JOIN #MyObjectHierarchy moh
 ON moh.ObjectID = tr.ObjectID




Declare @FullText Varchar(max)
Set @FullText = ''



Select 
@FullText = @FullText + SQLDefinition
From #Results 
Order By ResultID





SELECT @FullText AS [processing-instruction(x)] FOR XML PATH('')




Select ot=tr.ObjectType,tt=moh.Type,ot2=moh.OBJECTTYPE, tr.*,moh.* 
From #Results tr
JOIN #MyObjectHierarchy moh
 ON moh.ObjectID = tr.ObjectID
ORDER BY tr.ResultID



RETURN


/*
tsp_export_CustomObj_SearchAndReplace 'CRW_REPORTSTEMPDB', 'NEW_REPORTSTEMPDB'

tsp_export_CustomObj_SearchAndReplace

*/



--Declare @SQL Varchar(8000)
--Declare @OutputFileName Varchar(1000)
--Declare @DBName Varchar(100)
--Set @DBName = 'CRW_REPORTS'

--Declare @VersionName Varchar(100)
--Set @VersionName = CONVERT(Varchar,GetDate(),112) + '_' + REPLACE(CONVERT(VARCHAR, GETDATE(), 108),':','.') + '.sql'

--If @OutputPath Is Null OR @OutputPath = ''
--	Set @OutputFileName = 'C:\crw\TEMP\Trakit9_DBUpdates.sql'
--ELSE
--	Set @OutputFileName = @OutputPath

--Set @SQL = '
--INSERT INTO ' + @DBName + '.dbo.VersionControl_SQL([VersionFileName],[VersionSQL])
--SELECT ''' + @VersionName + '.sql'', ' + @FullText + ''
--Print(@SQL)
--Exec(@SQL)



 END





GO
