USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ChartTypeByActivityRecordID]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_ChartTypeByActivityRecordID]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ChartTypeByActivityRecordID]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create Procedure [dbo].[tsp_ChartTypeByActivityRecordID](
	@RecordID1 varchar(8000) = Null,
	@RecordID2 varchar(8000) = Null,
	@RecordID3 varchar(8000) = Null
	--rrr - adding one more
	,@RecordID4 varchar(8000) = Null
)

As

Declare @RecID table (RecordID varchar(2000)) 

insert @RecID
select r.recordid
from dbo.tfn_TblRecID(@recordid1,@recordid2,@recordid3,@recordid4) r  --rrr - added 4th param

set nocount on

SELECT AECTYPE as [TYPE], COUNT(AECTYPE) AS cnt FROM AEC_Main where RECORDID in (select RECORDID from @recID) GROUP BY AECTYPE
UNION SELECT CaseType as [TYPE], COUNT(CaseType) AS cnt FROM CASE_MAIN where RECORDID in (select RECORDID from @recID) GROUP BY CaseType
UNION SELECT GEOTYPE as [TYPE], COUNT(GEOTYPE) AS cnt FROM GEO_OWNERSHIP where RECORDID in (select RECORDID from @recID) GROUP BY GEOTYPE
UNION SELECT BUSINESS_TYPE as [TYPE], COUNT(BUSINESS_TYPE) AS cnt FROM LICENSE_BUSINESS where RECORDID in (select RECORDID from @recID) GROUP BY BUSINESS_TYPE
UNION SELECT LICENSE_TYPE as [TYPE], COUNT(LICENSE_TYPE) AS cnt FROM LICENSE2_MAIN where RECORDID in (select RECORDID from @recID) GROUP BY LICENSE_TYPE
UNION SELECT PermitType as [TYPE], COUNT(PermitType) AS cnt FROM PERMIT_MAIN where RECORDID in (select RECORDID from @recID) GROUP BY PermitType
UNION SELECT PROJECTTYPE as [TYPE], COUNT(PROJECTTYPE) AS cnt FROM PROJECT_MAIN where RECORDID in (select RECORDID from @recID) GROUP BY PROJECTTYPE
-- rrr 6/26/13 - Issue No. 22085 - added CRM
UNION SELECT  CRM_Lists.LIST_TEXT as [TYPE],  COUNT(CRM_Lists.LIST_TEXT) AS cnt FROM CRM_Issues 
				INNER JOIN CRM_Lists ON CRM_Issues.NATURETYPE_LIST_ID = CRM_Lists.LIST_ID where CRM_Issues.RECORDID in (select RECORDID from @RecID) 
				GROUP BY (CRM_Lists.LIST_TEXT) 
 
GO
