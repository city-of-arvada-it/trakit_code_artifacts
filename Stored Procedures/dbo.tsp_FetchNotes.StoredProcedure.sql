USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_FetchNotes]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_FetchNotes]
GO
/****** Object:  StoredProcedure [dbo].[tsp_FetchNotes]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[tsp_FetchNotes] (

	@ActivityGroup varchar(50) = NULL,
	@ActivityNo varchar(60) = NULL
)

AS
BEGIN
SET NOCOUNT ON;

	-- we need a table variable to house distinct rows of related notes records
	declare @notes table (UserID varchar(6), DateEntered datetime, ActivityGroup varchar(50), ActivityRecordID varchar(60), ActivityNo varchar(60), SubGroup varchar(50), SubGroupRecordID varchar(60), min_NoteID int, max_NoteID int, Notes varchar(max))
	
	-- we need a table that houses the exact NoteID values for Notes that were split
	declare @id table (NoteGroup int, NoteID int)


	if nullif(@ActivityGroup,'') is not null  and nullif(@ActivityNo,'') is not null 
	
	begin
		-- populate the table variable
		insert into @notes (UserID, DateEntered, ActivityGroup, ActivityRecordID, ActivityNo, SubGroup, SubGroupRecordID, min_NoteID, max_NoteID)
		select UserID, DateEntered, ActivityGroup, ActivityRecordID, ActivityNo, SubGroup, SubGroupRecordID, min(NoteID) [min_NoteID], max(NoteID) [max_NoteID]
		from Prmry_Notes where nullif(notes,'') is not null
		and ActivityNo=@ActivityNo and ActivityGroup=@ActivityGroup
		group by ActivityGroup, ActivityNo, ActivityRecordID, SubGroup, SubGroupRecordID, UserID, DateEntered
		
		-- concatenate the values in the Notes column that are within the NoteID min / max range and match the other values (NoteID values in the range may not be the same note!)
		
		update n 
		set n.Notes 
		= case when n.min_NoteID=n.max_NoteID then (select pn.notes from prmry_notes pn where  pn.NoteID = n.min_NoteID )  else 
		
		(select '' + pn.Notes 
					 from Prmry_Notes pn
					 where pn.NoteID between n.min_NoteID and n.max_NoteID 
					   and pn.ActivityNo=n.ActivityNo 
					   and pn.ActivityGroup=n.ActivityGroup
					   and pn.ActivityRecordID=n.ActivityRecordID
					   and isnull(pn.SubGroup,'')=isnull(n.SubGroup,'')
					   and isnull(pn.SubGroupRecordID,'')=isnull(n.SubGroupRecordID,'')
					  and  isnull(pn.UserID,'')=isnull(n.UserID,'')
					   and pn.DateEntered=n.DateEntered
					 order by NoteID 
					 for xml path(''))
					 end
		from @notes n
	end

	-- return the dataset to the caller
	select UserID, DateEntered, ActivityGroup, ActivityRecordID, ActivityNo, SubGroup, SubGroupRecordID, Notes from @notes

END
--*********************************************PLEASE ADD YOUR SCRIPTS ABOVE AND DO NOT DELETE PRINT STATEMENTS************
PRINT '18.2.6.01 Script ENDED'
GO
