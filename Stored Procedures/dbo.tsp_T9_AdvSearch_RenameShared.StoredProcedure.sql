USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearch_RenameShared]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_AdvSearch_RenameShared]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearch_RenameShared]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[tsp_T9_AdvSearch_RenameShared]
	@UserID varchar(50) = null,
	@DepartmentID int = -1,
	@SearchID int = -1,
	@NewName varchar(50) = null
AS
BEGIN

declare @ErrMsg varchar(50)
set @ErrMsg = ''

-- confirm the @DepartmentID exists
if (select count(*) from Prmry_AdvSearch_Departments where DepartmentID=@DepartmentID) = 1
begin
	-- confirm the @UserID is a member of the @DepartmentID
	if (select count(*) from Prmry_AdvSearch_DepartmentMembers where UserID=@UserID and DepartmentID=@DepartmentID) = 1
	begin
		-- confirm the @UserID has rename access in @DepartmentID
		if (select count(*) from Prmry_AdvSearch_DepartmentMembers where UserID=@UserID and DepartmentID=@DepartmentID and CanRename=1) = 1
		begin
			-- check if @NewName already exists in a list of search names that the @DepartmentID has saved currently
			if (select count(*) from Prmry_AdvSearch_SharedItem where DepartmentID=@DepartmentID and SearchName=@NewName) > 0 
			begin
				set @ErrMsg = 'Department already has a shared search saved as this name.'
			end
			else
			begin
				-- TODO: perform other validations?

				-- update shared search name
				update Prmry_AdvSearch_SharedItem set SearchName=@NewName where DepartmentID=@DepartmentID and RecordID=@SearchID
			end
		end
		else
		begin
			set @ErrMsg = 'User does not have privileges to rename shared searches for this Department.'
		end
	end
	else
	begin
		set @ErrMsg = 'User is not a member of this Department.'
	end
end
else
begin
	set @ErrMsg = 'Department does not exist for this DepartmentID (or less likely, there are more than one Department with the same DepartmentID).'
end


select @ErrMsg [ErrMsg]

END
GO
