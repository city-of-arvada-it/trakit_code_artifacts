USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_BatchProcess_Reviews]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_BatchProcess_Reviews]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_BatchProcess_Reviews]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  


create PROCEDURE [dbo].[tsp_T9_BatchProcess_Reviews] 
              @ListReviewTypes as UpdateFields readonly,
			  @ListUpdateFields as UpdateFields readonly,
              @ListTableAndRecords as MainTableRecords readonly
              
AS
BEGIN

	Declare @SQL Varchar(8000)
	Declare @TableName varchar(50)
	Declare @KeyField varchar(50)  
	Declare @ColumnName varchar(50) 
	Declare @ColumnValue varchar(255)
	Declare @RowNum int
	Declare @RowId Int
	Declare @AltRowNum int
	Declare @AltRowId Int
	Declare @UserID as varchar(10)
	Declare @TblName as varchar(100)
	Declare @Contact as varchar(100)
	Declare @GroupName as varchar(255)

	--Drop Tables

	IF object_id('tempdb..#ForInsertReviewTypes') is not null drop table #ForInsertReviewTypes
	IF object_id('tempdb..#ForInsertIntoPrmryAuditTrail') is not null drop table #ForInsertIntoPrmryAuditTrail
	IF object_id('tempdb..#ListReviewTypes') is not null drop table #ListReviewTypes
	IF object_id('tempdb..#ListUpdateFields') is not null drop table #ListUpdateFields
	IF object_id('tempdb..#MainTableRec') is not null  drop table #MainTableRec

	--Create tables
	Create Table #ForInsertReviewTypes(
		ACTIVITY_NO varchar(15),
		REVIEWTYPE varchar(60),
		CONTACT varchar(100),
		DATE_SENT datetime,
		DATE_DUE datetime,
		STATUS varchar(30),
		RECORDID varchar(30), 
		ForRecID int identity)


	--Create table to be used for insert into Audit
	Create Table #ForInsertIntoPrmryAuditTrail(
		CURRENT_VALUE varchar(2000),
		FIELD_NAME varchar(50),
		ORIGINAL_VALUE varchar(2000),
		PRIMARY_KEY_VALUE varchar(50),
		SQL_ACTION varchar(1),
		TABLE_NAME varchar(50),
		USER_ID varchar(4), 
		TRANSACTION_DATETIME datetime)

	--Populate table variables
	select * into #ListReviewTypes from  @ListReviewTypes

	select * into #ListUpdateFields from @ListUpdateFields

	select * into #MainTableRec from @ListTableAndRecords

	-- Start populating table (this should create all combinations for insert)
	insert into #ForInsertReviewTypes(ACTIVITY_NO,REVIEWTYPE) 
	select MTR.ActivityNo, lrt.Value from #ListReviewTypes lrt, #MainTableRec MTR

	--Populate other variables
	set @TableName = (Select distinct tblName from @ListTableAndRecords)

	--use function to get key field
	select @KeyField = dbo.tfn_ActivityNoField(@TableName)

	--Update temp table based; make 2 passes, 1 for everything else, then 1 for default
	select @RowId=MAX(RowID) FROM #ListUpdateFields     
	Select @RowNum = Count(*) From #ListUpdateFields      
	WHILE @RowNum > 0                          
	BEGIN   

		select @ColumnName = ColumnName from #ListUpdateFields where RowID= @RowId    
		select @ColumnValue = Value from #ListUpdateFields where RowID= @RowId  

		if len(ltrim(rtrim(@ColumnValue))) > 0 and @ColumnName ='UserID'
		Begin
			set @UserID = @ColumnValue
		End

		if len(ltrim(rtrim(@ColumnValue))) > 0 and @ColumnName ='GroupName'
		Begin
			set @GroupName = @ColumnValue
		End 

		if len(ltrim(rtrim(@ColumnValue))) > 0 and @ColumnName ='TblName'
		Begin
			set @TblName = @ColumnValue 
		End 
		if len(ltrim(rtrim(@ColumnValue))) > 0 and @ColumnName not in ('UserID', 'GroupName','TblName','CONTACT')
		Begin
			set @SQL = 'Update #ForInsertReviewTypes set ' + @ColumnName +  ' = ''' + @ColumnValue + ''''
			exec(@sql)
		End 
               
		select top 1 @RowId=RowID from #ListUpdateFields where RowID < @RowId order by RowID desc
		set @RowNum = @RowNum - 1                             
       
	END

	print('before contact')
	--Update temp table based
	select @AltRowId=MAX(RowID) FROM #ListUpdateFields     
	Select @AltRowNum = Count(*) From #ListUpdateFields      
	WHILE @AltRowNum > 0                          
	BEGIN   

		select @ColumnName = ColumnName from #ListUpdateFields where RowID= @AltRowId    
		select @ColumnValue = Value from #ListUpdateFields where RowID= @AltRowId  

		--Now the fun...set contact while checking for default                             
		if len(ltrim(rtrim(@ColumnValue))) > 0 and @ColumnName ='CONTACT' and upper(substring(ltrim(rtrim(@ColumnValue)),1,7)) <> 'DEFAULT'
		Begin

			set @Contact = @ColumnValue 
			print(@Contact)

			Update #ForInsertReviewTypes set CONTACT = @Contact

			select * from #ForInsertReviewTypes
		END

		--Now the fun...set inspector while checking for default                             
		if len(ltrim(rtrim(@ColumnValue))) > 0 and @ColumnName ='CONTACT' and upper(substring(ltrim(rtrim(@ColumnValue)),1,7)) = 'DEFAULT'
		Begin

					   
			set @SQL = 'Update #ForInsertReviewTypes set CONTACT = (select  LEFT(FIELD02, isnull(nullif(CHARINDEX(''('', FIELD02),0) - 1, 8000)) from prmry_ReviewControl where GroupName like '
			set @SQL = @SQL + '''' +   @GroupName + '%' + ''''
			set @SQL = @SQL + ' and Category = ''REVIEW_TYPE'' and #ForInsertReviewTypes.REVIEWTYPE = Field01)'
			
			exec(@SQL)

		END 
                     
		select top 1 @AltRowId=RowID from #ListUpdateFields where RowID < @AltRowId order by RowID desc
		set @AltRowNum = @AltRowNum - 1                             
       
	END                        
       
	update #ForInsertReviewTypes set RECORDID ='BPRO:' + right(('00' + str(DATEPART(yy, GETDATE()))),1)
	+ right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)
	+ right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
	+ right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)
	+ right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
	+ right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)
	+ right(('000000'+ ltrim(str(ForRecID))),6);


	set @SQL = 'INSERT INTO '
	set @SQL = @SQL +    @TblName 
	set @SQL = @SQL + ' ('
	set @SQL = @SQL +    @KeyField  
	set @SQL = @SQL + ', REVIEWTYPE, DATE_SENT, CONTACT, DATE_DUE,STATUS, RECORDID)
	SELECT ACTIVITY_NO, REVIEWTYPE, DATE_SENT, CONTACT, DATE_DUE,STATUS, RECORDID
	FROM #ForInsertReviewTypes' 

	 exec(@SQL)

--select * from #ForInsertInspectionTypes

       --set @SQL = left(@SQL, (len(@sql)-1))
       
       ----For Audit
       --set @SQL2 = @SQL

       --set @SQL = @SQL + @WhereClause + ';'
              
       --print(@SQL)
       --exec(@SQL)

       --insert into #ForInsertIntoPrmryAuditTrail(TABLE_NAME,PRIMARY_KEY_VALUE ) select tblName, ActivityNo from #MainTableRec

       --insert into Prmry_AuditTrail (CURRENT_VALUE, PRIMARY_KEY_VALUE, SQL_ACTION, TABLE_NAME,TRANSACTION_DATETIME, USER_ID) select @SQL2, PRIMARY_KEY_VALUE, 'U', @TblName, getdate(), @UserID from #ForInsertIntoPrmryAuditTrail

END






GO
