USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_e3GetPaidItems]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_e3GetPaidItems]
GO
/****** Object:  StoredProcedure [dbo].[tsp_e3GetPaidItems]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[tsp_e3GetPaidItems] (
-- Add the parameters for the stored procedure here
	@etrakit_name varchar(50)
)

AS
BEGIN
SET NOCOUNT ON;

SELECT * FROM (
SELECT ec.ACTIVITY_GROUP as activity_group, ec.ACTIVITY_NO as ACTIVITY_NO, pm.SITE_ADDR, pm.PermitType as type, ec.FEES_PAID
FROM etrakit_cart ec
INNER JOIN permit_main pm on pm.PERMIT_NO = ec.activity_no
where  etrakit_name = @etrakit_name and paid_datetime is not null 
AND ec.ACTIVITY_GROUP = 'PERMIT'
UNION ALL
SELECT ec.ACTIVITY_GROUP as activity_group, ec.ACTIVITY_NO as ACTIVITY_NO, pm.SITE_ADDR, pm.PROJECTTYPE as type, ec.FEES_PAID
FROM etrakit_cart ec
INNER JOIN project_main pm on pm.PROJECT_NO = ec.activity_no
where  etrakit_name = @etrakit_name and paid_datetime is not null 
AND ec.ACTIVITY_GROUP = 'PROJECT'
UNION ALL
SELECT ec.ACTIVITY_GROUP as activity_group, ec.ACTIVITY_NO as ACTIVITY_NO, cm.SITE_ADDR, cm.CaseType as type, ec.FEES_PAID
FROM etrakit_cart ec
INNER JOIN case_main cm on cm.case_no = ec.activity_no
where  etrakit_name = @etrakit_name and paid_datetime is not null 
AND ec.ACTIVITY_GROUP = 'CASE'
UNION ALL
SELECT 'PERMIT' as activity_group, eca.REAL_ACTIVITY_NO as ACTIVITY_NO, eca.SITE_ADDR, eca.TYPENAME as type, ec.FEES_PAID
FROM etrakit_cart ec
INNER JOIN etrakit_cart_activities eca on eca.ACTIVITY_NO = ec.ACTIVITY_NO
where  etrakit_name = @etrakit_name and paid_datetime is not null 
AND ec.ACTIVITY_GROUP = 'ETRAKIT' and eca.REAL_GROUPNAME = 'PERMIT'
UNION ALL
SELECT 'PROJECT' as activity_group, eca.REAL_ACTIVITY_NO as ACTIVITY_NO, eca.SITE_ADDR, eca.TYPENAME as type, ec.FEES_PAID
FROM etrakit_cart ec
INNER JOIN etrakit_cart_activities eca on eca.ACTIVITY_NO = ec.ACTIVITY_NO
where  etrakit_name = @etrakit_name and paid_datetime is not null 
AND ec.ACTIVITY_GROUP = 'ETRAKIT' and eca.REAL_GROUPNAME = 'PROJECT'
UNION ALL
SELECT ec.ACTIVITY_GROUP as activity_group, ec.ACTIVITY_NO as ACTIVITY_NO, lm.SITE_ADDR, lm.LICENSE_TYPE as type, ec.FEES_PAID
FROM etrakit_cart ec
INNER JOIN license2_main lm on lm.LICENSE_NO = ec.activity_no
where  etrakit_name = @etrakit_name and paid_datetime is not null 
AND ec.ACTIVITY_GROUP = 'LICENSE2'
UNION ALL
SELECT ec.ACTIVITY_GROUP as activity_group, ec.ACTIVITY_NO as ACTIVITY_NO, geo.SITE_ADDR, lb.BUSINESS_TYPE  as type, ec.FEES_PAID
FROM etrakit_cart ec
INNER JOIN License_main lm on lm.BUS_LIC_NO= ec.activity_no
INNER JOIN License_Business lb on lb.BUSINESS_NO = lm.BUSINESS_NO
INNER JOIN Geo_Ownership geo on geo.RECORDID = lb.LOC_RECORDID 
where  etrakit_name = @etrakit_name and paid_datetime is not null 
AND ec.ACTIVITY_GROUP = 'LICENSE'
) AS paditems ORDER BY ACTIVITY_NO

END
GO
