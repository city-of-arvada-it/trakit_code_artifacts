USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[GEOTRAKINT_06182020]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[GEOTRAKINT_06182020]
GO
/****** Object:  StoredProcedure [dbo].[GEOTRAKINT_06182020]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE Procedure [dbo].[GEOTRAKINT_06182020] AS
----------------------------------------------------------------------------------
-- Interface: dbo.GEOTRAKINT 
-- Database: SQL Server 2008r2
-- Client: Arvada, CO
-- Created By: Nikoel Carter
-- Created Date: 6/29/2012

-- This interface will transfer data from linked SQL Server source database to TRAKiT LandTrak 
/*
alter table GEO_OWNERSHIPUDF
alter column BLDGDATA03 varchar(50)

begin tran
exec [dbo].[GEOTRAKINT]
rollback

*/
----------------------------------------------------------------------------------

set nocount on

Declare


-- Street Source Variables
@szFRADDL		VARCHAR(11),
@szTOADDL		VARCHAR(11),
@szFRADDR		VARCHAR(11),
@szTOADDRght	VARCHAR(11),
@szFullStName	VARCHAR(50),
@lYrBuilt		INT,

-- Intersection Source Variables
@szIntersection	VARCHAR(255),

-- CIP Source Variables
@szProjName		VARCHAR(100),
@szProjNo		VARCHAR(30),

-- Subdivision Source Variables
@szReceptionNo	VARCHAR(20),
@dtRecorded		DATETIME,
@szWebLink		VARCHAR(255),

-- Parcel Source Variables
@fBaths			FLOAT,
@lBedrooms		INT,
@szSection		VARCHAR(11),
@fLGLSec		FLOAT,
@fLGLQtr		FLOAT,
@fLGLTwn		FLOAT,
@fLGLRng		FLOAT,
@szBlock		VARCHAR(20),
@szLot			VARCHAR(20),
@szATD			VARCHAR(4),
@lBldgID		INT,
@szStTypeUse	VARCHAR(50),
@fSTTYrBlt		FLOAT,
@fSqFt1			FLOAT,
@szStTypeUse2	VARCHAR(50),
@fSTTYrBlt2		FLOAT,
@fSqFt2			FLOAT,
@szStTypeUse3	VARCHAR(50),
@fSTTYrBlt3		FLOAT,
@fSqFt3			FLOAT,
@fSTTYrBlt4		FLOAT,
@fSqFt4			FLOAT,
@szStTypeUse4	VARCHAR(50),
@fTotalSqFt		FLOAT,
@szABSTCode		VARCHAR(10),
@szABSTDesc		VARCHAR(50),
@szClassCode1	VARCHAR(50),
@szCodeDesc1	VARCHAR(100),
@szClassCode2	VARCHAR(50),
@szCodeDesc2	VARCHAR(100),
@szClassCode3	VARCHAR(50),
@szCodeDesc3	VARCHAR(100),
@szClassCode4	VARCHAR(50),
@szCodeDesc4	VARCHAR(100),
@szFrntStBkFrom	VARCHAR(50),
@szPALFrntStbk	VARCHAR(50),
@szGrgFrntStbk	VARCHAR(50),
@szSideStbk		VARCHAR(50),
@szRearStbk		VARCHAR(50),
@szLotCvrg		VARCHAR(50),
@szLotCvrgOther	VARCHAR(50),
@szSpecStbkDev	VARCHAR(50),
@lBuildings		INT,
@szStructType	VARCHAR(50), --jbh : jeffco changes
@szLegal		VARCHAR(254),
@szFullAddr		VARCHAR(100),
@szOrdinanceNo	VARCHAR(20),
@fMultSqFt		FLOAT,
@fMultYrBlt		FLOAT,
@szOldPIN		VARCHAR(50),

-- Address Source Variables
@szAddress		VARCHAR(50),
@szAddrType		VARCHAR(20),
@szAddrSubType	VARCHAR(30),
@szAddrName		VARCHAR(50),
@szCornerAddr	VARCHAR(5),
@szDblFrntLot	VARCHAR(10),
@szInspector	VARCHAR(50),
@szStreetName	VARCHAR(60),

-- Multi Source and Join Variables
@szPIN			VARCHAR(40),
@szCRWID		VARCHAR(15),
@szBlockGrpID	VARCHAR(12),
@szCompPlanDesc	VARCHAR(50),
@szHistoricPIN	VARCHAR(13),
@szSubDivName	VARCHAR(100),
@fTotalImprv	FLOAT,
@fTotalLandVal	FLOAT,
@fNetSF			FLOAT,
@fStories		FLOAT,
@fNoUnits		FLOAT,
@szMailAddr		VARCHAR(60),
@szMailStrNbr	VARCHAR(5),
@szMailStrDir	VARCHAR(2),
@szMailStrName	VARCHAR(20),
@szMailStrType	VARCHAR(4),
@szMailStrSfx	VARCHAR(2),
@szMailStrUnit	VARCHAR(10),
@szOwnCo		VARCHAR(60),
@szMailCity		VARCHAR(60),
@szOwnerName	VARCHAR(60),
@szOwnerName2	VARCHAR(60),
@szMailState	VARCHAR(2),
@szMailZip		VARCHAR(10),
@szStNum		VARCHAR(15),
@szStName		VARCHAR(30),
@szPreDir		VARCHAR(2),
@szStSuffix		VARCHAR(50),
@szStType		VARCHAR(10),
@szUnitNum		VARCHAR(10),
@szCity			VARCHAR(24),
@szZip			VARCHAR(10),
@fYrBlt			FLOAT,
@szZoning		VARCHAR(10),
@szWaterDist	VARCHAR(50),
@szSanitDist	VARCHAR(50),
@szCouncilDist	VARCHAR(50),
@szFireDist		VARCHAR(50),
@szMetroDist	VARCHAR(50),
@szHistDist		VARCHAR(30),
@szUrbanRenew	VARCHAR(50),
@szCodeInsp		VARCHAR(50),
@fTAZID			FLOAT,
@szConservArea	VARCHAR(75),
@szGenZoning	VARCHAR(40),
@szZoningDesc	VARCHAR(100),
@szUnits		VARCHAR(15),
@szMaxHeight	VARCHAR(40),
@szOrdRestrict	VARCHAR(20),
@szFloodZone	VARCHAR(10),
@szLOMACaseNo	VARCHAR(20),
@szDevStndURL	VARCHAR(200),
@szSubdivLink	VARCHAR(150),

-- Procedural Variables
@szLocRecordID	VARCHAR(30),
@szRecordID		VARCHAR(30),
@szParentRecID	VARCHAR(30),
@lCounter		INT,
@szFromAddr		VARCHAR(10),
@szToAddr		VARCHAR(10),
@szSiteAddr		VARCHAR(70),
@fTotalVal		FLOAT,
@szSiteStreet	VARCHAR(50),
@szSiteNumber	VARCHAR(20),
@szSiteDesc		VARCHAR(2000),
@szOwnerAddr1	VARCHAR(40),
@szOwnerAddr2	VARCHAR(40),
@szOwnerCity	VARCHAR(30),
@szOwnerState	VARCHAR(2),
@szOwnerZip		VARCHAR(10),
@szLandUse01	VARCHAR(20),
@szLandUse03	VARCHAR(20),
@szSec			VARCHAR(10),
@szSecTwpRng	VARCHAR(20),
@szOldSiteAddr	VARCHAR(80),
@szOldSiteApn	VARCHAR(50),
@szOldStatus	VARCHAR(20),
@szOldBldg01	VARCHAR(20),
@szOldBldg03	VARCHAR(20),
@szOldLand01	VARCHAR(20),
@szOldLand03	VARCHAR(20),
@szOldLand05	VARCHAR(20),
@szOldGenPlan	VARCHAR(30),
@fOldImpVal		FLOAT,
@fOldLandVal	FLOAT,
@lOldLotSqFt	FLOAT,
@szOldBath		VARCHAR(4),
@szOldBed		VARCHAR(4),
@szOldOwner		VARCHAR(30),
@szOldOwnAddr1	VARCHAR(30),
@szOldOwnAddr2	VARCHAR(30),
@szOldOwnCity	VARCHAR(24),
@szOldOwnState	VARCHAR(2),
@szOldOwnZip	VARCHAR(10),
@szOldSecTwpRng	VARCHAR(20),
@szOldSubdiv	VARCHAR(100),
@szOldZone2		VARCHAR(10),
@szOldParentID	VARCHAR(30),
@szOldParent	VARCHAR(50),
@szOldCensus	VARCHAR(10),
@szOldNoUnits	VARCHAR(4),
@szOldLegalDesc	VARCHAR(2000),
@szOldNotes		VARCHAR(7500),
@fOldLotSize	FLOAT,
@lOldNoStories	INT,
@szOldBlock		VARCHAR(10),
@szOldLot		VARCHAR(10),
@fOldStructure	FLOAT,
@fOldYrBuilt	FLOAT,
@szOldOthName	VARCHAR(30),
@szOldWaterDist	VARCHAR(50),
@szOldSanitDist	VARCHAR(50),
@szOldCouncilDist VARCHAR(10),
@szOldFireDist	VARCHAR(50),
@szOldMetroDist	VARCHAR(50),
@szOldHistDist	VARCHAR(30),
@szOldUrbanRenew VARCHAR(50),
@szOldCodeInsp	VARCHAR(50),
@fOldTAZID		FLOAT,
@szOldConservArea VARCHAR(75),
@szOldZoning	VARCHAR(10),
@szOldGenZoning VARCHAR(40),
@szOldZoningDesc VARCHAR(100),
@szOldUnits		VARCHAR(15),
@szOldMaxHeight VARCHAR(40),
@szOldOrdRestrict VARCHAR(20),
@fOldBld1YrBlt	FLOAT,
@fOldBld2YrBlt	FLOAT,
@fOldBld3YrBlt	FLOAT,
@fOldBld4YrBlt	FLOAT,
@fOldBld1Sqft	FLOAT,
@fOldBld2Sqft	FLOAT,
@fOldBld3Sqft	FLOAT,
@fOldBld4Sqft	FLOAT,
@fOldSumYrBlt	FLOAT,
@fOldSumSqFt	FLOAT,
@szOldClassCode1 VARCHAR(50),
@szOldClassCode2 VARCHAR(50),
@szOldClassCode3 VARCHAR(50),
@szOldClassCode4 VARCHAR(50),
@szOldCodeDesc1	VARCHAR(100),
@szOldCodeDesc2	VARCHAR(100),
@szOldCodeDesc3	VARCHAR(100),
@szOldCodeDesc4	VARCHAR(100),
@szOldSpecStbkDev VARCHAR(50),
@szOldTRA		VARCHAR(10),
@szBLD1Stats	VARCHAR(50),
@szBLD2Stats	VARCHAR(50),
@szBLD3Stats	VARCHAR(50),
@szBLD4Stats	VARCHAR(50),
@szSchoolDist	VARCHAR(30),
@szOldCornerLot	VARCHAR(5),
@szOldDblFrntLot VARCHAR(50),
@szOldSiteDesc	VARCHAR(2000),
@szTheText		VARCHAR(2000),
@dtDtEntered	DATETIME,
@dtDtThru		DATETIME,
@szLHNUser		VARCHAR(4),
@szOldOrdNo		VARCHAR(20),
@szOldZoneLink  VARCHAR(200),
@szTrigToggle	VARCHAR(3),
@szOldSubDivLink VARCHAR(150),
@szOldDfltInspctr VARCHAR(50),
@fSUM_SQFT		FLOAT,
@szOldStreetName varchar(60)

Begin

---------------------------------------------------------------------------------------------------
-- import tables from oracle (because linked server takes 2 days to run
---------------------------------------------------------------------------------------------------
-- script to pull data from oracle GIS tables and create locally
DROP TABLE ADAMS_PARCELS

  SELECT PPIN, BLOCKGROUP_ID, COMPPLAN_DESCRIPTION, TOTALIMPSV, TOTALLANDV, NETSF,
		 PADDRESS, PCITY, OWNERNAME1, STATE, ZIPCODE, SECTION_, CONCATADDR, LGL_BLOCK, LOCCITY,
		 LOT_1, PSTREETNO, STREETNAME, STREETDIR, STREETSUF, SUBNAME_1, STREETUNIT, LOCZIP, ZONING,
		 WATER_DISTRICT, SANITATION_DISTRICT, COUNCIL_DISTRICT, FIRE_DISTRICT, METRO_DISTRICT, HISTORIC_DISTRICT,
		 URBAN_RENEWAL_AREA, CODE_INSPECTOR, TAZ_ID, OLDE_TOWN_AREA, GEN_ZONING, ZONING_DESCRIPTION, SJUNITS,
		 MAXIMUM_HEIGHT, ORD_RESTRICT, FLOOD_ZONE, LOMA_CASE_NO, BUILDINGS, PACCTTYPE, LEGAL, OWNERNAME2,
		 ORDINANCE_NO, SPECIAL_SETBACKS_DEV, DBLFRONTLOT, DEV_STANDARDS_URL, SUBDIVISION_LINK
INTO ADAMS_PARCELS
	FROM openquery (GEODATABASE, 'SELECT P.PIN AS PPIN, BLOCKGROUP_ID, COMPPLAN_DESCRIPTION, TOTALIMPSV, TOTALLANDV, NETSF,
		 P.ADDRESS AS PADDRESS, P.CITY AS PCITY, OWNERNAME1, STATE, ZIPCODE, SECTION_, CONCATADDR, LGL_BLOCK, LOCCITY,
		 LOT_1, P.STREETNO AS PSTREETNO, STREETNAME, STREETDIR, STREETSUF, SUBNAME_1, P.STREETUNIT, LOCZIP, ZONING,
		 WATER_DISTRICT, SANITATION_DISTRICT, COUNCIL_DISTRICT, FIRE_DISTRICT, METRO_DISTRICT, HISTORIC_DISTRICT,
		 URBAN_RENEWAL_AREA, CODE_INSPECTOR, TAZ_ID, OLDE_TOWN_AREA, GEN_ZONING, ZONING_DESCRIPTION, SJ.UNITS AS SJUNITS,
		 MAXIMUM_HEIGHT, ORD_RESTRICT, FLOOD_ZONE, LOMA_CASE_NO, BUILDINGS, P.ACCTTYPE AS PACCTTYPE, LEGAL, OWNERNAME2,
		 ORDINANCE_NO, SPECIAL_SETBACKS_DEV, DBLFRONTLOT, DEV_STANDARDS_URL, SUBDIVISION_LINK
		 FROM GEODATA.GDS.COA_PARCELS_ADAMS_ASSESSOR P
	LEFT JOIN GEODATA.GDS.CRW_SPATIALJOIN_FINAL SJ ON P.PIN = SJ.PIN
   WHERE P.PIN IS NOT NULL')
CREATE INDEX PIN_IDX ON ADAMS_PARCELS(PPIN)

DROP TABLE COA_HISTORICAL

	SELECT OLDPIN, PIN
	INTO COA_HISTORICAL
	FROM openquery(GEODATABASE, 'SELECT OLDPIN, PIN FROM GEODATA.GDS.COA_HISTORICAL_PIN WHERE CODE <> ''MERGE''')
CREATE INDEX PIN_IDX ON COA_HISTORICAL(PIN)
CREATE INDEX OLDPIN_IDX ON COA_HISTORICAL(OLDPIN)

DROP TABLE ADAMS_ASSESS_IMPROVEMENTS

	SELECT PIN, BLDGID, BATHS, BEDROOMS, STORIES, UNITS, TOTALSQFT, YRBLT
INTO ADAMS_ASSESS_IMPROVEMENTS
	  FROM openquery(GEODATABASE, 'SELECT PIN, BLDGID, BATHS, BEDROOMS, STORIES, UNITS, TOTALSQFT, YRBLT
										FROM GEODATA.GDS.ADAMS_ASSESS_IMPROVEMENTS')
	 WHERE BLDGID = '1'
CREATE INDEX PIN_IDX ON ADAMS_ASSESS_IMPROVEMENTS(PIN)

 DROP TABLE ADAMS_CODES

   SELECT PIN, BLDGID, TOTALSQFT, YRBLT, AAABSTCODE, LUDESC
INTO ADAMS_CODES
	  FROM openquery(GEODATABASE, 'SELECT PIN, BLDGID, TOTALSQFT, YRBLT, AA.ABSTCODE AS AAABSTCODE,
									LU.DESCRIPTION AS LUDESC FROM GEODATA.GDS.ADAMS_ASSESS_IMPROVEMENTS AA
							LEFT JOIN GEODATA.GDS.ADAMS_ASSESS_ABSTCODE_LU LU ON AA.ABSTCODE = LU.ABSTCODE')
CREATE INDEX CODE_IDX ON ADAMS_CODES(AAABSTCODE)

 DROP TABLE JEFFCO_PARCELS

  SELECT PPIN, BLOCKGROUP_ID, COMPPLAN_DESCRIPTION, TOTACTIMPV, TOTACTLNDV, TOTACR, STTNBRFLR, STTNBRUNT, MAILSTRNBR, MAILSTRDIR, MAILSTRNAM,
		 MAILSTRTYP, MAILSTRSFX, MAILSTRUNT, OWNICO, MAILCTYNAM, OWNNAM, MAILSTENAM, MAILZIP5, MAILZIP4,
		 LGLSEC, LGLQTR, LGLTWN, LGLRNG, LGLBLK, PRPCTYNAM, LGLLOT, PRPSTRNUM, PRPSTRNAM, PRPSTRDIR, PRPSTRTYP, PRPSTRSFX, SUBNAM,
		 PRPSTRUNT, PRPZIP5, PRPZIP4, ZONING, WATER_DISTRICT, SANITATION_DISTRICT, COUNCIL_DISTRICT, FIRE_DISTRICT,
		 METRO_DISTRICT, HISTORIC_DISTRICT, URBAN_RENEWAL_AREA, CODE_INSPECTOR, TAZ_ID, OLDE_TOWN_AREA, GEN_ZONING, ZONING_DESCRIPTION, UNITS,
		 MAXIMUM_HEIGHT, ORD_RESTRICT, STTTYPUSE, STTTYPUSE2, STTTYPUSE3, STTTYPUSE4, STTYRBLT, STTYRBLT2, STTYRBLT3, STTYRBLT4, STTGRSAREA, STTGRSARE2,
		 STTGRSARE3, STTGRSARE4, TAXCLS, TAXCLS2, TAXCLS3, TAXCLS4, FLOOD_ZONE, LOMA_CASE_NO, STTNBRBLDG, STTSTRC, OWNNAM2,
		 ORDINANCE_NO, SPECIAL_SETBACKS_DEV, DBLFRONTLOT, DEV_STANDARDS_URL, SUBDIVISION_LINK
INTO JEFFCO_PARCELS
	FROM openquery(GEODATABASE, 'SELECT P.PIN AS PPIN, BLOCKGROUP_ID, COMPPLAN_DESCRIPTION, TOTACTIMPV, TOTACTLNDV, TOTACR, STTNBRFLR, STTNBRUNT, MAILSTRNBR, MAILSTRDIR, MAILSTRNAM,
		 MAILSTRTYP, MAILSTRSFX, MAILSTRUNT, OWNICO, MAILCTYNAM, OWNNAM, MAILSTENAM, MAILZIP5, MAILZIP4,
		 LGLSEC, LGLQTR, LGLTWN, LGLRNG, LGLBLK, PRPCTYNAM, LGLLOT, PRPSTRNUM, PRPSTRNAM, PRPSTRDIR, PRPSTRTYP, PRPSTRSFX, SUBNAM,
		 PRPSTRUNT, PRPZIP5, PRPZIP4, ZONING, WATER_DISTRICT, SANITATION_DISTRICT, COUNCIL_DISTRICT, FIRE_DISTRICT,
		 METRO_DISTRICT, HISTORIC_DISTRICT, URBAN_RENEWAL_AREA, CODE_INSPECTOR, TAZ_ID, OLDE_TOWN_AREA, GEN_ZONING, ZONING_DESCRIPTION, UNITS,
		 MAXIMUM_HEIGHT, ORD_RESTRICT, STTTYPUSE, STTTYPUSE2, STTTYPUSE3, STTTYPUSE4, STTYRBLT, STTYRBLT2, STTYRBLT3, STTYRBLT4, STTGRSAREA, STTGRSARE2,
		 STTGRSARE3, STTGRSARE4, TAXCLS, TAXCLS2, TAXCLS3, TAXCLS4, FLOOD_ZONE, LOMA_CASE_NO, STTNBRBLDG, STTSTRC, OWNNAM2,
		 ORDINANCE_NO, SPECIAL_SETBACKS_DEV, DBLFRONTLOT, DEV_STANDARDS_URL, SUBDIVISION_LINK
		 FROM GEODATA.GDS.COA_PARCELS_JEFFCO_ASSESSOR P
	LEFT JOIN GEODATA.GDS.CRW_SPATIALJOIN_FINAL SJ ON P.PIN = SJ.PIN
   WHERE P.PIN IS NOT NULL
	 AND P.PIN NOT IN (''AD'', ''AR'', ''COGO_DISP'', ''STATE_ASSD'', ''SUB'', ''WATER'')')

CREATE INDEX PIN_IDX ON JEFFCO_PARCELS(PPIN)


--JBH : Once Descriptions were changed, lookup table no longer needed
-- DROP TABLE JEFF_STTYPEUSE

--	SELECT DESCRIPTION, ST_CODE
--INTO JEFF_STTYPEUSE
--	FROM openquery(GEODATABASE, 'SELECT DESCRIPTION, ST_CODE FROM GEODATA.GDS.JEFF_ASSESS_STTTYPUSE_LU')
--CREATE INDEX CODE_IDX ON JEFF_STTYPEUSE(ST_CODE)

 DROP TABLE JEFF_TAXCLASS

	SELECT DESCRIPTION, TAXCLSCODE
INTO JEFF_TAXCLASS
	FROM openquery(GEODATABASE, 'SELECT DESCRIPTION, TAXCLSCODE FROM GEODATA.GDS.JEFF_ASSESS_TAXCLASS_LU')
CREATE INDEX TXCODE_IDX ON JEFF_TAXCLASS(TAXCLSCODE)

 DROP TABLE COA_HIST_MERGE

  SELECT PIN, OLDPIN
INTO COA_HIST_MERGE
	FROM openquery(GEODATABASE, 'SELECT PIN, OLDPIN FROM GEODATA.GDS.COA_HISTORICAL_PIN WHERE CODE = ''MERGE''')

 DROP TABLE ADDRESSES

  SELECT APIN, ACRWID, BLOCKGROUP_ID, COMPPLAN_DESCRIPTION, JURISD_CITY, ADDR_TYPE, ADDR_SUBTYPE, NAME, STREET_NUMBER, STREET_NAME,
		 STREET_PRE_DIR, STREET_TYPE, STREET_SUFFIX, UNIT_NUM, ZIP_5, ZIP_4, ZONING, WATER_DISTRICT, SANITATION_DISTRICT,
		 COUNCIL_DISTRICT, FIRE_DISTRICT, METRO_DISTRICT, HISTORIC_DISTRICT, URBAN_RENEWAL_AREA, CODE_INSPECTOR, TAZ_ID, SUBDIVISION_NAME,
		 OLDE_TOWN_AREA, GEN_ZONING, ZONING_DESCRIPTION, UNITS, MAXIMUM_HEIGHT, ORD_RESTRICT, CORNER_ADDR, FLOOD_ZONE, LOMA_CASE_NO,
		 ORDINANCE_NO, DBLFRONTLOT, SPECIAL_SETBACKS_DEV, DEV_STANDARDS_URL, AADDRESS, SUBDIVISION_LINK,LATITUDE,LONGITUDE,POINT_X,POINT_Y
INTO ADDRESSES
	FROM openquery(GEODATABASE, 'SELECT A.PIN AS APIN, A.CRW_ID AS ACRWID, BLOCKGROUP_ID, COMPPLAN_DESCRIPTION, JURISD_CITY, ADDR_TYPE, ADDR_SUBTYPE, NAME, STREET_NUMBER, STREET_NAME,
		 STREET_PRE_DIR, STREET_TYPE, STREET_SUFFIX, UNIT_NUM, ZIP_5, ZIP_4, ZONING, WATER_DISTRICT, SANITATION_DISTRICT,
		 COUNCIL_DISTRICT, FIRE_DISTRICT, METRO_DISTRICT, HISTORIC_DISTRICT, URBAN_RENEWAL_AREA, CODE_INSPECTOR, TAZ_ID, SUBDIVISION_NAME,
		 OLDE_TOWN_AREA, GEN_ZONING, ZONING_DESCRIPTION, UNITS, MAXIMUM_HEIGHT, ORD_RESTRICT, CORNER_ADDR, FLOOD_ZONE, LOMA_CASE_NO,
		 ORDINANCE_NO, DBLFRONTLOT, SPECIAL_SETBACKS_DEV, DEV_STANDARDS_URL, A.ADDRESS AS AADDRESS , SUBDIVISION_LINK,
		 LATITUDE,LONGITUDE,POINT_X,POINT_Y
		 FROM GEODATA.GDS.MASTER_ADDRESS_FILE A
	LEFT JOIN GEODATA.GDS.CRW_SPATIALJOIN_FINAL SJ ON A.PIN = SJ.PIN
   WHERE A.PIN IS NOT NULL')
CREATE INDEX PIN_IDX ON ADDRESSES(APIN)
CREATE INDEX CRWID_IDX ON ADDRESSES(ACRWID)

 DROP TABLE ADDR_ADAMS

	SELECT PPIN, TOTALIMPSV, TOTALLANDV, NETSF, STORIES, UNITS, ADDRESS, CITY, OWNERNAME1, OWNERNAME2, STATE,
		   ZIPCODE, SECTION_,  LGL_BLOCK, LOT_1
INTO ADDR_ADAMS
	 FROM openquery(GEODATABASE, 'SELECT P.PIN AS PPIN, TOTALIMPSV, TOTALLANDV, NETSF, STORIES, UNITS, ADDRESS, CITY,
		    OWNERNAME1, OWNERNAME2, STATE, ZIPCODE, SECTION_, LGL_BLOCK, LOT_1 FROM GEODATA.GDS.COA_PARCELS_ADAMS_ASSESSOR P
			LEFT JOIN GEODATA.GDS.ADAMS_ASSESS_IMPROVEMENTS AA ON P.PIN = AA.PIN')
CREATE INDEX PIN_IDX ON ADDR_ADAMS(PPIN)

 DROP TABLE STREETS

 SELECT CRW_ID, FRADDL, TOADDL, FRADDR, TOADDR, STREET_NAME, YR_BUILT, FEANME, DIRPRE, DIRSUF, FEATYP
INTO STREETS
	FROM openquery(GEODATABASE, 'SELECT CRW_ID, FRADDL, TOADDL, FRADDR, TOADDR, STREET_NAME, YR_BUILT, FEANME, DIRPRE,
									DIRSUF, FEATYP FROM GEODATA.GDS.STREETS WHERE CRW_ID IS NOT NULL')
CREATE INDEX CRWID_IDX ON STREETS(CRW_ID)

 DROP TABLE INTERSECTIONS

  SELECT CRW_ID, INTERSECTION
INTO INTERSECTIONS
	FROM openquery(GEODATABASE, 'SELECT CRW_ID, INTERSECTION FROM GEODATA.GDS.STREET_INTERSECTIONS
									WHERE CRW_ID IS NOT NULL')
CREATE INDEX CRWID_IDX ON INTERSECTIONS(CRW_ID)

 DROP TABLE CIP

 SELECT CRW_ID, PROJECT_NAME, PROJECT_NO
INTO CIP
	FROM openquery(GEODATABASE, 'SELECT CRW_ID, PROJECT_NAME, PROJECT_NO FROM GEODATA.GDS.CIP
									WHERE CRW_ID IS NOT NULL')
CREATE INDEX CRWID_IDX ON CIP(CRW_ID)

 DROP TABLE SUBDIVISION

  SELECT CRW_ID, RECEPTION_NO, RECORDED_DATE, SUBDIVISION_NAME, WEB_LINK
INTO SUBDIVISION
	FROM openquery (GEODATABASE, 'SELECT CRW_ID, RECEPTION_NO, RECORDED_DATE, SUBDIVISION_NAME, WEB_LINK
									FROM GEODATA.GDS.SUBDIVISION WHERE CRW_ID IS NOT NULL')
CREATE INDEX CRWID_IDX ON SUBDIVISION(CRW_ID)

if exists(select 1 from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'PARCELS_INSPECTOR')
 DROP TABLE PARCELS_INSPECTOR

  SELECT PIN, INSPECTOR
  INTO PARCELS_INSPECTOR
	FROM openquery (GEODATABASE, 'SELECT PIN, INSPECTOR
									FROM GEODATA.GDS.PARCELS_INSPECTOR WHERE PIN IS NOT NULL')
CREATE INDEX PIN_IDX ON PARCELS_INSPECTOR(PIN)

if exists(select 1 from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'ADDRESS_INSPECTOR')
 DROP TABLE ADDRESS_INSPECTOR

  SELECT CRW_ID, INSPECTOR
  INTO ADDRESS_INSPECTOR
	FROM openquery (GEODATABASE, 'SELECT CRW_ID, INSPECTOR
									FROM GEODATA.GDS.ADDRESS_INSPECTOR WHERE CRW_ID IS NOT NULL')
CREATE INDEX CRWID_IDX ON ADDRESS_INSPECTOR(CRW_ID)

 if exists(select 1 from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'CIP_INSPECTOR')
DROP TABLE CIP_INSPECTOR

  SELECT CRW_ID, INSPECTOR
  INTO CIP_INSPECTOR
	FROM openquery (GEODATABASE, 'SELECT CRW_ID, INSPECTOR
									FROM GEODATA.GDS.CIP_INSPECTOR WHERE CRW_ID IS NOT NULL')
CREATE INDEX CRWID_IDX ON CIP_INSPECTOR(CRW_ID)

if exists(select 1 from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'INTERSECTIONS_INSPECTOR')
 DROP TABLE INTERSECTIONS_INSPECTOR

  SELECT CRW_ID, INSPECTOR
  INTO INTERSECTIONS_INSPECTOR
	FROM openquery (GEODATABASE, 'SELECT CRW_ID, INSPECTOR
									FROM GEODATA.GDS.INTERSECTIONS_INSPECTOR WHERE CRW_ID IS NOT NULL')
CREATE INDEX CRWID_IDX ON INTERSECTIONS_INSPECTOR(CRW_ID)

if exists(select 1 from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'STREETS_INSPECTOR')
 DROP TABLE STREETS_INSPECTOR

  SELECT CRW_ID, INSPECTOR
  INTO STREETS_INSPECTOR
	FROM openquery (GEODATABASE, 'SELECT CRW_ID, INSPECTOR
									FROM GEODATA.GDS.STREETS_INSPECTOR WHERE CRW_ID IS NOT NULL')
CREATE INDEX CRWID_IDX ON STREETS_INSPECTOR(CRW_ID)

if exists(select 1 from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'SUBDIVISION_INSPECTOR')
 DROP TABLE SUBDIVISION_INSPECTOR

  SELECT CRW_ID, INSPECTOR
  INTO SUBDIVISION_INSPECTOR
	FROM openquery (GEODATABASE, 'SELECT CRW_ID, INSPECTOR
									FROM GEODATA.GDS.SUBDIVISION_INSPECTOR WHERE CRW_ID IS NOT NULL')
CREATE INDEX CRWID_IDX ON SUBDIVISION_INSPECTOR(CRW_ID)


if exists(select 1 from INFORMATION_SCHEMA.TABLES where TABLE_NAME = 'GIS_LAT_LONG')
 DROP TABLE GIS_LAT_LONG

  SELECT ObjectId,CRW_ID,LATITUDE,LONGITUDE
INTO GIS_LAT_LONG
	FROM openquery (GEODATABASE, 'SELECT ObjectId,
											CRW_ID,
											LATITUDE,
											LONGITUDE
									FROM GEODATA.GDS.CRW_LAT_LONG 
									WHERE CRW_ID IS NOT NULL')
CREATE CLUSTERED INDEX GIS_LAT_LONG_CRWID_IDX ON GIS_LAT_LONG(CRW_ID)
 

----------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------

	-- Set variables
	SELECT @szLocRecordID = NULL, @szRecordID = NULL, @lCounter = 1

	-- Drop triggers before run
    Exec [DisableGeoTriggers]

	Insert Into dbo.convlog (Procname, Errcode, Action, Actdttm)
						Values ('LANDTRAKINT', 0, 'START', Getdate())

  --------------------------------------
  -- Process Adams County Parcel Data --
  --------------------------------------
  DECLARE cGetAdamsParcel CURSOR LOCAL FOR        
  SELECT RTRIM(PPIN), BLOCKGROUP_ID, UPPER(COMPPLAN_DESCRIPTION), TOTALIMPSV, TOTALLANDV, NETSF,
		 UPPER(PADDRESS), UPPER(PCITY), UPPER(OWNERNAME1), UPPER(STATE), ZIPCODE, UPPER(SECTION_), UPPER(CONCATADDR), UPPER(LGL_BLOCK), UPPER(LOCCITY),
		 UPPER(LOT_1), PSTREETNO, UPPER(STREETNAME), UPPER(STREETDIR), UPPER(STREETSUF), UPPER(SUBNAME_1), STREETUNIT, REPLACE(LOCZIP, '000000000', ''), UPPER(ZONING),
		 UPPER(WATER_DISTRICT), UPPER(SANITATION_DISTRICT), UPPER(COUNCIL_DISTRICT), UPPER(FIRE_DISTRICT), UPPER(METRO_DISTRICT), UPPER(HISTORIC_DISTRICT),
		 UPPER(URBAN_RENEWAL_AREA), UPPER(CODE_INSPECTOR), TAZ_ID, UPPER(OLDE_TOWN_AREA), UPPER(GEN_ZONING), UPPER(ZONING_DESCRIPTION), SJUNITS,
		 MAXIMUM_HEIGHT, UPPER(ORD_RESTRICT), UPPER(FLOOD_ZONE), LOMA_CASE_NO, BUILDINGS, UPPER(PACCTTYPE), UPPER(LEGAL), UPPER(OWNERNAME2),
		 UPPER(ORDINANCE_NO), SPECIAL_SETBACKS_DEV, DBLFRONTLOT, DEV_STANDARDS_URL, SUBDIVISION_LINK, I.INSPECTOR
	FROM ADAMS_PARCELS P
	LEFT JOIN PARCELS_INSPECTOR I ON P.PPIN = I.PIN
   
  Open cGetAdamsParcel 
  Fetch Next From cGetAdamsParcel Into @szPIN, @szBlockGrpID, @szCompPlanDesc, @fTotalImprv, @fTotalLandVal, @fNetSF,
								  @szMailAddr, @szMailCity, @szOwnerName, @szMailState, @szMailZip, @szSection, @szFullAddr, @szBlock, @szCity,
								  @szLot, @szStNum, @szStName, @szPreDir, @szStSuffix, @szSubDivName, @szUnitNum, @szZip, @szZoning,
								  @szWaterDist, @szSanitDist, @szCouncilDist, @szFireDist, @szMetroDist, @szHistDist,
								  @szUrbanRenew, @szCodeInsp, @fTAZID, @szConservArea, @szGenZoning, @szZoningDesc, @szUnits,
								  @szMaxHeight, @szOrdRestrict, @szFloodZone, @szLOMACaseNo, @lBuildings, @szStructType, @szLegal, @szOwnerName2,
								  @szOrdinanceNo, @szSpecStbkDev, @szDblFrntLot, @szDevStndURL, @szSubdivLink, @szInspector
  While @@Fetch_status <> -1 
  Begin
		-- Set StreetName
		SET @szSiteStreet = SUBSTRING(RTRIM(LTRIM(ISNULL(@szPreDir, ' ') + ' ' + @szStName) + ' ' + ISNULL(@szStSuffix, ' ')), 1, 60)
		
		-- Set Total Value
		SET @fTotalVal = @fTotalImprv + @fTotalLandVal
		
		-- Set Zip
		IF @szZip IS NOT NULL AND @szZip <> '' AND @szZip <> ' '
			SET @szZip = SUBSTRING(@szZip, 1, 5) + ' ' + SUBSTRING(@szZip, 6, 4)
		
		-- Get historic PIN
		SELECT @szOldPIN = OLDPIN FROM COA_HISTORICAL WHERE PIN = @szPIN

		-- Get associated misc data
		SELECT @fBaths = BATHS, @lBedrooms = BEDROOMS, @fStories = STORIES, @fNoUnits = UNITS, @fTotalSqFt = TOTALSQFT, @fYrBlt = YRBLT
		  FROM ADAMS_ASSESS_IMPROVEMENTS WHERE PIN = @szPIN

		DECLARE cGetMultiples CURSOR LOCAL FOR
	    SELECT BLDGID, TOTALSQFT, YRBLT, UPPER(AAABSTCODE), UPPER(LUDESC)
		  FROM ADAMS_CODES WHERE PIN = @szPIN

	    Open cGetMultiples 
	    Fetch Next From cGetMultiples Into @lBldgID, @fMultSqFt, @fMultYrBlt, @szABSTCode, @szABSTDesc
	    While @@Fetch_status <> -1 
	    Begin
			-- Set UDF YrBlt and SQFT fields
			IF @lBldgID = 1
				SELECT @fSTTYrBlt = @fMultYrBlt, @fSqFt1 = @fMultSqFt, @szClassCode1 = @szABSTCode, @szCodeDesc1 = @szABSTDesc
			IF @lBldgID = 2
				SELECT @fSTTYrBlt2 = @fMultYrBlt, @fSqFt2 = @fMultSqFt, @szClassCode2 = @szABSTCode, @szCodeDesc2 = @szABSTDesc
			IF @lBldgID = 3
				SELECT @fSTTYrBlt3 = @fMultYrBlt, @fSqFt3 = @fMultSqFt, @szClassCode3 = @szABSTCode, @szCodeDesc3 = @szABSTDesc
			IF @lBldgID = 4
				SELECT @fSTTYrBlt4 = @fMultYrBlt, @fSqFt4 = @fMultSqFt, @szClassCode4 = @szABSTCode, @szCodeDesc4 = @szABSTDesc

	    Fetch Next From cGetMultiples Into @lBldgID, @fMultSqFt, @fMultYrBlt, @szABSTCode, @szABSTDesc
		End --While
		Close cGetMultiples
		Deallocate cGetMultiples

		-- Set SecTwpRng
		IF RIGHT(@szSection, 1) = 1	SET @szSec = 'NE'
		ELSE IF RIGHT(@szSection, 1) = 2 SET @szSec = 'NW'	
		ELSE IF RIGHT(@szSection, 1) = 3 SET @szSec = 'SW'
		ELSE IF RIGHT(@szSection, 1) = 4 SET @szSec = 'SE'
			
		SET @szSecTwpRng = LEFT(@szSection, 2) + @szSec + '3S 68W'
		
		-- Set LandUse fields
		IF @szFloodZone IN ('AE', 'FW', 'A', 'AO') SET @szLandUse01 = 'YES'
		ELSE SET @szLandUse01 = NULL
			
		IF @szFloodZone = 'FW' SET @szLandUse03 = 'YES'
		ELSE SET @szLandUse03 = NULL

		-- Get RecordID if link found
		SELECT @szLocRecordID = RECORDID FROM GEO_OWNERSHIP WHERE SITE_APN = @szPIN AND GEOTYPE = 'PARCEL'
		
		-- Get ParentRecordID if link found
		SELECT @szParentRecID = RECORDID FROM GEO_OWNERSHIP WHERE SITE_APN = @szOldPIN AND GEOTYPE = 'PARCEL'

		-- If a link is found, update record
		IF @szLocRecordID IS NOT NULL
		BEGIN
			-- Get old info to compare and see if update is necessary
			SELECT @szOldBath = BATHROOMS, @szOldBed = BEDROOMS, @szOldCensus = CENSUS, @szOldGenPlan = GENERAL_PLAN, @fOldImpVal = IMPROVED_VALUE, @fOldLandVal = LAND_VALUE, @fOldLotSize = LOT_SIZE_SF,
					@lOldNoStories = NO_STORIES, @szOldNoUnits = NO_UNITS, @szOldSecTwpRng = SECTION_TWP_RNG, @szOldBlock = SITE_BLOCK, @szOldLot = SITE_LOT_NO, @szOldSubdiv = SITE_SUBDIVISION,
					@fOldStructure = STRUCTURE_SF, @fOldYrBuilt = YEAR_BUILT, @szOldZone2 = ZONE_CODE2, @szOldSiteAddr = SITE_ADDR, @szOldParent = PARENT_SITE_APN,
					@szOldOwner = OWNER_NAME, @szOldOwnAddr1 = OWNER_ADDR1, @szOldOwnCity = OWNER_CITY, @szOldOwnState = OWNER_STATE, @szOldOwnZip = OWNER_ZIP, @szOldStreetName = site_streetname
				FROM GEO_OWNERSHIP WHERE RECORDID = @szLocRecordID
			-- If anything has been updated, update record
			IF ISNULL(@szOldBath, 0) <> ISNULL(@fBaths, 0) OR ISNULL(@szOldBed, 0) <> ISNULL(@lBedrooms, 0) OR ISNULL(@szOldCensus, ' ') <> SUBSTRING(ISNULL(@szBlockGrpID, ' '), 1, 10) OR
			   ISNULL(@szOldGenPlan, ' ') <> SUBSTRING(ISNULL(@szCompPlanDesc, ' '), 1, 10) OR ISNULL(@fOldImpVal, 0) <> ISNULL(@fTotalImprv, 0) OR ISNULL(@fOldLandVal, 0) <> ISNULL(@fTotalLandVal, 0) OR
			   ISNULL(@fOldLotSize, 0) <> ISNULL(@fNetSF, 0) OR ISNULL(@lOldNoStories, 0) <> ISNULL(@fStories, 0) OR ISNULL(@szOldNoUnits, 0) <> ISNULL(@fNoUnits, 0) OR
			   ISNULL(@szOldSecTwpRng, ' ') <> SUBSTRING(ISNULL(@szSecTwpRng, ' '), 1, 20) OR ISNULL(@szOldBlock, ' ') <> SUBSTRING(ISNULL(@szBlock, ' '), 1, 10) OR
			   ISNULL(@szOldLot, ' ') <> SUBSTRING(ISNULL(@szLot, ' '), 1, 10) OR ISNULL(@szOldSubdiv, ' ') <> ISNULL(@szSubDivName, ' ') OR ISNULL(@fOldStructure, 0) <> ISNULL(@fTotalSqFt, 0) OR
			   ISNULL(@fOldYrBuilt, 0) <> ISNULL(@fYrBlt, 0) OR ISNULL(@szOldZone2, ' ') <> SUBSTRING(ISNULL(@szZoning, ' '), 1, 10) OR ISNULL(@szOldParent, ' ') <> ISNULL(@szOldPIN, ' ') OR
			   ISNULL(@szOldOwner, ' ') <> ISNULL(@szOwnerName, ' ') OR ISNULL(@szOldOwnAddr1, ' ') <> ISNULL(@szMailAddr, ' ') OR ISNULL(@szOldOwnCity, ' ') <> ISNULL(@szMailCity, ' ') OR
			   ISNULL(@szOldOwnState, ' ') <> ISNULL(@szMailState, ' ') OR ISNULL(@szOldOwnZip, ' ') <> ISNULL(@szMailZip, ' ') OR ISNULL(@szOldSiteAddr, ' ') <> ISNULL(@szFullAddr, ' ')
			   OR isnull(@szOldStreetName,'') <> isnull(@szSiteStreet,'')
			BEGIN
				UPDATE GEO_OWNERSHIP SET BATHROOMS = @fBaths, BEDROOMS = @lBedrooms, CENSUS = SUBSTRING(@szBlockGrpID, 1, 10), GENERAL_PLAN = SUBSTRING(@szCompPlanDesc, 1, 10),
										 IMPROVED_VALUE = @fTotalImprv, LAND_VALUE = @fTotalLandVal, TOTAL_VALUE = @fTotalVal, LOT_SIZE_SF = @fNetSF, NO_STORIES = @fStories,
										 NO_UNITS = @fNoUnits, SECTION_TWP_RNG = SUBSTRING(@szSecTwpRng, 1, 20), SITE_BLOCK = SUBSTRING(@szBlock, 1, 10), SITE_LOT_NO = SUBSTRING(@szLot, 1, 10),
										 SITE_SUBDIVISION = @szSubDivName, STRUCTURE_SF = @fTotalSqFt, YEAR_BUILT = @fYrBlt, ZONE_CODE2 = SUBSTRING(@szZoning, 1, 10),
										 OWNER_NAME = @szOwnerName, OWNER_ADDR1 = @szMailAddr, OWNER_ADDR2 = NULL, OWNER_CITY = @szMailCity, OWNER_STATE = @szMailState, OWNER_ZIP = @szMailZip,
										 SITE_ADDR = SUBSTRING(@szFullAddr, 1, 70), SITE_NUMBER = @szStNum, SITE_ST_NAME = SUBSTRING(@szStName, 1, 30), SITE_STREETNAME = @szSiteStreet,
										 SITE_ST_PREFIX = @szPreDir, SITE_ST_TYPE = @szStSuffix, SITE_UNIT_NO = @szUnitNum,
										 SITE_CITY = @szCity, SITE_ZIP = @szZip, Historical_APN = @szOldPIN, PARENT_SITE_APN = @szOldPIN, PARENT_RECORDID = @szParentRecID
						WHERE RECORDID = @szLocRecordID

				IF @@error <> 0
					Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Comments)
								Values ('LANDTRAKINT', 20, 'ERROR', Getdate(), 'SITE APN', @szPIN, 'Error updating Parcel record in Geo_Ownership')
					
				-- if ownername has changed, insert change log record
				IF ISNULL(@szOldOwner, ' ') <> ISNULL(@szOwnerName, ' ')
				BEGIN
					-- Set unique RecordID
					SET @szRecordID = NULL
					-- Reset counter for unique number string at end of RecordID
					SET @lCounter = @lCounter + 1
					SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
					 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
					 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
					 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)	 + right(('000000'+ ltrim(str(@lCounter))),6)
					 
					INSERT INTO GEO_CHANGE_LOG(ENTRY_DATE, FIELD_CHANGED, LOC_RECORDID, NEW_VALUE, OLD_VALUE, RECORDID)
										VALUES(GetDate(), 'OWNER_NAME', @szLocRecordID, @szOwnerName, @szOldOwner, @szRecordID)
				END
				
				-- if site address has changed, insert change log record
				IF ISNULL(@szOldSiteAddr, ' ') <> ISNULL(@szSiteAddr, ' ')
				BEGIN
					-- Set unique RecordID
					SET @szRecordID = NULL
					-- Reset counter for unique number string at end of RecordID
					SET @lCounter = @lCounter + 1
					SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
					 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)		 + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
					 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)		 + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
					 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)		 + right(('000000'+ ltrim(str(@lCounter))),6)
					 
					INSERT INTO GEO_CHANGE_LOG(ENTRY_DATE, FIELD_CHANGED, LOC_RECORDID, NEW_VALUE, OLD_VALUE, RECORDID)
										VALUES(GetDate(), 'SITE_ADDR', @szLocRecordID, @szSiteAddr, @szOldSiteAddr, @szRecordID)
				END

				-- if zonecode2 has changed, insert change log record
				IF ISNULL(@szOldZone2, ' ') <> SUBSTRING(ISNULL(@szZoning, ' '), 1, 10)
				BEGIN
					-- Set unique RecordID
					SET @szRecordID = NULL
					-- Reset counter for unique number string at end of RecordID
					SET @lCounter = @lCounter + 1
					SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
					 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
					 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
					 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)	 + right(('000000'+ ltrim(str(@lCounter))),6)
					 
					INSERT INTO GEO_CHANGE_LOG(ENTRY_DATE, FIELD_CHANGED, LOC_RECORDID, NEW_VALUE, OLD_VALUE, RECORDID)
										VALUES(GetDate(), 'ZONE_CODE2', @szLocRecordID, SUBSTRING(@szZoning, 1, 10), @szOldZone2, @szRecordID)
				END

			END

			-- Get old info to compare and see if update is necessary
			SELECT @szOldOwner = NAME, @szOldOthName = OTHER_NAME, @szOldOwnAddr1 = ADDRESS1, @szOldOwnCity = CITY, @szOldOwnState = STATE,
					@szOldOwnZip = ZIP FROM PEOPLE WHERE ActivityID = @szLocRecordID AND NAMETYPE = 'OWNER'
			-- If owner name is different, update nametype to 'PREV OWNER' and insert new owner
			IF ISNULL(@szOldOwner, ' ') <> ISNULL(@szOwnerName, ' ')
			BEGIN
				UPDATE PEOPLE SET NAMETYPE = 'PREV OWNER' WHERE ActivityID = @szLocRecordID AND NAMETYPE = 'OWNER'
				
				-- Reset counter for unique number string at end of RecordID
				SET @lCounter = @lCounter + 1
				-- Set unique RecordID
				SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
				 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
				 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
				 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)	 + right(('000000'+ ltrim(str(@lCounter))),6)

				INSERT INTO PEOPLE (RECORDID, ActivityID, ActivityTypeID, SITE_APN, NAMETYPE, NAME, OTHER_NAME, ADDRESS1, CITY, STATE, ZIP)
								VALUES (@szRecordID, @szLocRecordID, 5, @szPIN, 'OWNER', @szOwnerName, @szOwnerName2, @szMailAddr, @szMailCity, @szMailState, @szMailZip)

				IF @@error <> 0
					Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Keycol2, Keydata2, Comments)
								Values ('LANDTRAKINT', 23, 'ERROR', Getdate(), 'OWNER', @szOwnerName, 'SITE APN', @szPIN, 'Error adding Parcel record to People')
			END
			-- If anything except ownername has been updated, update record
			ELSE IF ISNULL(@szOldOwner, ' ') = ISNULL(@szOwnerName, ' ') AND (ISNULL(@szOldOthName, ' ') <> ISNULL(@szOwnerName2, ' ') OR
			   ISNULL(@szOldOwnAddr1, ' ') <> ISNULL(@szMailAddr, ' ') OR ISNULL(@szOldOwnCity, ' ') <> ISNULL(@szMailCity, ' ') OR ISNULL(@szOldOwnState, ' ') <> ISNULL(@szMailState, ' ') OR
			   ISNULL(@szOldOwnZip, ' ') <> ISNULL(@szMailZip, ' '))
			BEGIN
				UPDATE PEOPLE SET NAME = @szOwnerName, OTHER_NAME = @szOwnerName2, ADDRESS1 = @szMailAddr, ADDRESS2 = NULL, CITY = @szMailCity, STATE = @szMailState, ZIP = @szMailZip
						WHERE ActivityID = @szLocRecordID AND NAMETYPE = 'OWNER'
				IF @@error <> 0
					Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Keycol2, Keydata2, Comments)
								Values ('LANDTRAKINT', 24, 'ERROR', Getdate(), 'OWNER', SUBSTRING(@szOwnerName, 1, 30), 'SITE APN', @szPIN, 'Error updating Parcel record in People')
			END

			-- Get old info to compare and see if update is necessary
			SELECT @szOldLand01 = LANDUSE01, @szOldLand03 = LANDUSE03, @szOldLand05 = LANDUSE05, @szOldBldg01 = BLDGDATA01, @szOldBldg03 = BLDGDATA03, @szOldDfltInspctr = DFLT_INSPCTR_PERMIT
				FROM GEO_OWNERSHIPUDF WHERE LOC_RECORDID = @szLocRecordID
			-- If anything has been updated, update record
			IF ISNULL(@szOldLand01, ' ') <> ISNULL(@szLandUse01, ' ') OR ISNULL(@szOldLand03, ' ') <> ISNULL(@szLandUse03, ' ') OR ISNULL(@szOldDfltInspctr, ' ') <> ISNULL(@szInspector, ' ') OR
			   ISNULL(@szOldLand05, ' ') <> ISNULL(@szLOMACaseNo, ' ') OR ISNULL(@szOldBldg01, ' ') <> ISNULL(@lBuildings, ' ') OR ISNULL(@szOldBldg03, ' ') <> ISNULL(@szStructType, ' ')
			BEGIN
				UPDATE GEO_OWNERSHIPUDF SET LANDUSE01 = @szLandUse01, LANDUSE03 = @szLandUse03, LANDUSE05 = @szLOMACaseNo, BLDGDATA01 = @lBuildings, BLDGDATA03 = @szStructType,
											DFLT_INSPCTR_PERMIT = @szInspector	WHERE LOC_RECORDID = @szLocRecordID

				IF @@error <> 0
					Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Comments)
								Values ('LANDTRAKINT', 20, 'ERROR', Getdate(), 'SITE APN', @szPIN, 'Error updating Parcel record in Geo_OwnershipUDF')

				-- if landuse01 has changed, insert change log record
				IF ISNULL(@szOldLand01, ' ') <> ISNULL(@szLandUse01, ' ')
				BEGIN
					-- Set unique RecordID
					SET @szRecordID = NULL
					-- Reset counter for unique number string at end of RecordID
					SET @lCounter = @lCounter + 1
					SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
					 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
					 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
					 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)	 + right(('000000'+ ltrim(str(@lCounter))),6)
					 
					INSERT INTO GEO_CHANGE_LOG(ENTRY_DATE, FIELD_CHANGED, LOC_RECORDID, NEW_VALUE, OLD_VALUE, RECORDID)
										VALUES(GetDate(), 'LANDUSE01', @szLocRecordID, @szLandUse01, @szOldLand01, @szRecordID)
				END
				
			END

			-- Get old info to compare and see if update is necessary
			SELECT @szOldWaterDist = WATER_DIST, @szOldSanitDist = SANIT_DIST, @szOldCouncilDist = COUNCIL_DIST, @szOldFireDist = FIRE_DIST, @szOldMetroDist = METRO_DIST,
					@szOldHistDist = HISTORIC_DIST, @szOldUrbanRenew = URBRENEW_AREA, @szOldCodeInsp = CODE_INSP, @fOldTAZID = TAZ_ID, @szOldConservArea = CONSERV_AREA,
					@szOldZoning = ZONING, @szOldGenZoning = GEN_ZONING, @szOldZoningDesc = ZONE_DESC, @szOldUnits = UNITS, @szOldMaxHeight = MAX_HEIGHT, @szOldZoneLink = ZONE_LINK,
					@szOldOrdRestrict = ORD_RESTRICT, @fOldBld1YrBlt = BLD1_YRBLT, @fOldBld2YrBlt = BLD2_YRBLT, @fOldBld3YrBlt = BLD3_YRBLT, @fOldBld4YrBlt = BLD4_YRBLT,
					@fOldBld1Sqft = BLD1_SQFT, @fOldBld2Sqft = BLD2_SQFT, @fOldBld3Sqft = BLD3_SQFT, @fOldBld4Sqft = BLD4_SQFT, @szOldClassCode1 = CLASS_CODE1,
					@szOldClassCode2 = CLASS_CODE2, @szOldClassCode3 = CLASS_CODE3, @szOldClassCode4 = CLASS_CODE4, @szOldCodeDesc1 = CODE_DESC1, @szOldCodeDesc2 = CODE_DESC2, 
					@szOldCodeDesc3 = CODE_DESC3, @szOldCodeDesc4 = CODE_DESC4, @szOldSpecStbkDev = SPCL_STBK_DEV, @szOldOrdNo = ORD_NUMBER, @szOldDblFrntLot = DBL_FRNT_LOT,
					@szOldSubDivLink = SUBDIVISIONLINK FROM GEO_UDF WHERE LOC_RECORDID = @szLocRecordID
			-- If anything has been updated, update record
			IF ISNULL(@szOldWaterDist, ' ') <> ISNULL(@szWaterDist, ' ') OR ISNULL(@szOldSanitDist, ' ') <> ISNULL(@szSanitDist, ' ') OR
			   ISNULL(@szOldCouncilDist, ' ') <> ISNULL(@szCouncilDist, ' ') OR ISNULL(@szOldFireDist, ' ') <> ISNULL(@szFireDist, ' ') OR
			   ISNULL(@szOldMetroDist, ' ') <> ISNULL(@szMetroDist, ' ') OR ISNULL(@szOldHistDist, ' ') <> ISNULL(@szHistDist, ' ') OR
			   ISNULL(@szOldUrbanRenew, ' ') <> ISNULL(@szUrbanRenew, ' ') OR ISNULL(@szOldCodeInsp, ' ') <> ISNULL(@szCodeInsp, ' ') OR
			   ISNULL(@fOldTAZID, 0) <> ISNULL(@fTAZID, 0) OR ISNULL(@szOldConservArea, ' ') <> ISNULL(@szConservArea, ' ') OR ISNULL(@szOldZoning, ' ') <> ISNULL(@szZoning, ' ') OR
			   ISNULL(@szOldGenZoning, ' ') <> ISNULL(@szGenZoning, ' ') OR ISNULL(@szOldZoningDesc, ' ') <> ISNULL(@szZoningDesc, ' ') OR
			   ISNULL(@szOldUnits, ' ') <> ISNULL(@szUnits, ' ') OR ISNULL(@szOldMaxHeight, ' ') <> ISNULL(@szMaxHeight, ' ') OR ISNULL(@szOldOrdRestrict, ' ') <> ISNULL(@szOrdRestrict, ' ') OR
			   ISNULL(@fOldBld1YrBlt, ' ') <> ISNULL(@fSTTYrBlt, ' ') OR ISNULL(@fOldBld2YrBlt, ' ') <> ISNULL(@fSTTYrBlt2, ' ') OR ISNULL(@fOldBld3YrBlt, ' ') <> ISNULL(@fSTTYrBlt3, ' ') OR
			   ISNULL(@fOldBld4YrBlt, ' ') <> ISNULL(@fSTTYrBlt4, ' ') OR ISNULL(@fOldBld1SqFt, ' ') <> ISNULL(@fSqFt1, ' ') OR ISNULL(@fOldBld2SqFt, ' ') <> ISNULL(@fSqFt2, ' ') OR
			   ISNULL(@fOldBld3SqFt, ' ') <> ISNULL(@fSqFt3, ' ') OR ISNULL(@fOldBld4SqFt, ' ') <> ISNULL(@fSqFt4, ' ') OR ISNULL(@szOldClassCode1, ' ') <> ISNULL(@szClassCode1, ' ') OR
			   ISNULL(@szOldClassCode2, ' ') <> ISNULL(@szClassCode2, ' ') OR ISNULL(@szOldClassCode3, ' ') <> ISNULL(@szClassCode3, ' ') OR
			   ISNULL(@szOldClassCode4, ' ') <> ISNULL(@szClassCode4, ' ') OR ISNULL(@szOldCodeDesc1, ' ') <> ISNULL(@szCodeDesc1, ' ') OR
			   ISNULL(@szOldCodeDesc2, ' ') <> ISNULL(@szCodeDesc2, ' ') OR ISNULL(@szOldCodeDesc3, ' ') <> ISNULL(@szCodeDesc3, ' ') OR
			   ISNULL(@szOldCodeDesc4, ' ') <> ISNULL(@szCodeDesc4, ' ') OR ISNULL(@szOldSpecStbkDev, ' ') <> ISNULL(@szSpecStbkDev, ' ') OR
			   ISNULL(@szOldOrdNo, ' ') <> ISNULL(@szOrdinanceNo, ' ') OR ISNULL(@szOldDblFrntLot, ' ') <> ISNULL(@szDblFrntLot, ' ') OR
			   ISNULL(@szOldZoneLink, ' ') <> ISNULL(@szDevStndURL, ' ') OR ISNULL(@szOldSubDivLink, ' ') <> ISNULL(@szSubdivLink,' ')
			BEGIN
				UPDATE GEO_UDF SET WATER_DIST = @szWaterDist, SANIT_DIST = @szSanitDist, COUNCIL_DIST = @szCouncilDist, FIRE_DIST = @szFireDist, METRO_DIST = @szMetroDist,
								   HISTORIC_DIST = @szHistDist, URBRENEW_AREA = @szUrbanRenew, CODE_INSP = @szCodeInsp, TAZ_ID = @fTAZID, CONSERV_AREA = @szConservArea,
								   ZONING = @szZoning, GEN_ZONING = @szGenZoning, ZONE_DESC = @szZoningDesc, UNITS = @szUnits, MAX_HEIGHT = @szMaxHeight,
								   ORD_RESTRICT = @szOrdRestrict, BLD1_YRBLT = @fSTTYrBlt, BLD2_YRBLT = @fSTTYrBlt2, BLD3_YRBLT = @fSTTYrBlt3, BLD4_YRBLT = @fSTTYrBlt4,
								   BLD1_SQFT = @fSqFt1, BLD2_SQFT = @fSqFt2, BLD3_SQFT = @fSqFt3, BLD4_SQFT = @fSqFt4, CLASS_CODE1 = @szClassCode1, CLASS_CODE2 = @szClassCode2,
								   CLASS_CODE3 = @szClassCode3, CLASS_CODE4 = @szClassCode4, CODE_DESC1 = @szCodeDesc1, CODE_DESC2 = @szCodeDesc2, CODE_DESC3 = @szCodeDesc3,
								   CODE_DESC4 = @szCodeDesc4, SPCL_STBK_DEV = @szSpecStbkDev, ORD_NUMBER = @szOrdinanceNo, DBL_FRNT_LOT = @szDblFrntLot, ZONE_LINK = @szDevStndURL,
								   SUBDIVISIONLINK = @szSubdivLink
						WHERE LOC_RECORDID = @szLocRecordID

				IF @@error <> 0
					Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Comments)
								Values ('LANDTRAKINT', 20, 'ERROR', Getdate(), 'SITE APN', @szPIN, 'Error updating Parcel record in Geo_UDF')
			END

			-- Get old info to compare and see if update is necessary
			SELECT @szOldLegalDesc = DESCRIPTION FROM GEO_LEGALS WHERE LOC_RECORDID = @szLocRecordID
			-- If anything has been updated, update record
			IF @szOldLegalDesc <> @szLegal
			BEGIN
				UPDATE GEO_LEGALS SET DESCRIPTION = @szLegal WHERE LOC_RECORDID = @szLocRecordID

				IF @@error <> 0
					Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Comments)
								Values ('LANDTRAKINT', 20, 'ERROR', Getdate(), 'SITE APN', @szPIN, 'Error updating Parcel record in Geo_Legals')
			END

			-- If current restrictions - drop all and reinsert any that exist in source
			IF (SELECT COUNT(*) FROM GEO_RESTRICTIONS2 WHERE LOC_RECORDID = @szLocRecordID) > 0
				DELETE FROM GEO_RESTRICTIONS2 WHERE LOC_RECORDID = @szLocRecordID AND SUBSTRING(RECORDID,1,4) = 'CONV'

			-- Insert Flood Zone Restrictions
			IF @szFloodZone IN ('AE', 'FW', 'A', 'AO')
			BEGIN
				-- Reset counter for unique number string at end of RecordID
				SET @lCounter = @lCounter + 1
				-- Set unique RecordID
				SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
				 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
				 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
				 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)	 + right(('000000'+ ltrim(str(@lCounter))),6)

				INSERT INTO GEO_RESTRICTIONS2 (RECORDID, LOC_RECORDID, SITE_APN, RESTRICTION_TYPE, RESTRICTION_NOTES)
									   VALUES (@szRecordID, @szLocRecordID, @szPIN, 'WARNING', 'SEE ENGINEERING; PARCEL IN FLOODPLAIN')

				IF @@error <> 0
					Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Keycol2, Keydata2, Comments)
								Values ('LANDTRAKINT', 25, 'ERROR', Getdate(), 'RESTRICTION TYPE', 'WARNING: FLOODPLAIN', 'SITE_APN', @szPIN, 
										'Error adding Parcel record to Geo_Restrictions2') 
			END

			-- Insert Flood Zone Restrictions
			IF @szFloodZone = 'FW'
			BEGIN
				-- Reset counter for unique number string at end of RecordID
				SET @lCounter = @lCounter + 1
				-- Set unique RecordID
				SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
				 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)		 + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
				 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)		 + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
				 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)		 + right(('000000'+ ltrim(str(@lCounter))),6)

				INSERT INTO GEO_RESTRICTIONS2 (RECORDID, LOC_RECORDID, SITE_APN, RESTRICTION_TYPE, RESTRICTION_NOTES)
									   VALUES (@szRecordID, @szLocRecordID, @szPIN, 'WARNING', 'SEE ENGINEERING; PARCEL IN FLOODWAY')

				IF @@error <> 0
					Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Keycol2, Keydata2, Comments)
								Values ('LANDTRAKINT', 25, 'ERROR', Getdate(), 'RESTRICTION TYPE', 'WARNING: FLOODWAY', 'SITE_APN', @szPIN, 
										'Error adding Parcel record to Geo_Restrictions2') 
			END

			-- Insert Historic Restrictions
			IF @szHistDist = 'ARVADA DOWNTOWN'
			BEGIN
				-- Reset counter for unique number string at end of RecordID
				SET @lCounter = @lCounter + 1
				-- Set unique RecordID
				SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
				 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
				 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
				 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)	 + right(('000000'+ ltrim(str(@lCounter))),6)

				INSERT INTO GEO_RESTRICTIONS2 (RECORDID, LOC_RECORDID, SITE_APN, RESTRICTION_TYPE, RESTRICTION_NOTES)
									   VALUES (@szRecordID, @szLocRecordID, @szPIN, 'WARNING', 'PARCEL IN HISTORIC DISTRICT')

				IF @@error <> 0
					Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Keycol2, Keydata2, Comments)
								Values ('LANDTRAKINT', 25, 'ERROR', Getdate(), 'RESTRICTION TYPE', 'WARNING: HISTORIC DISTRICT', 'SITE_APN', @szPIN, 
										'Error adding Parcel record to Geo_Restrictions2') 
			END

			-- Insert Olde Town Area Restrictions
			IF @szConservArea = 'OLDE TOWN AREA'
			BEGIN
				-- Reset counter for unique number string at end of RecordID
				SET @lCounter = @lCounter + 1
				-- Set unique RecordID
				SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
				 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
				 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
				 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)	 + right(('000000'+ ltrim(str(@lCounter))),6)

				INSERT INTO GEO_RESTRICTIONS2 (RECORDID, LOC_RECORDID, SITE_APN, RESTRICTION_TYPE, RESTRICTION_NOTES)
									   VALUES (@szRecordID, @szLocRecordID, @szPIN, 'WARNING', 'OLDE TOWN AREA')

				IF @@error <> 0
					Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Keycol2, Keydata2, Comments)
								Values ('LANDTRAKINT', 25, 'ERROR', Getdate(), 'RESTRICTION TYPE', 'WARNING: OLDE TOWN AREA', 'SITE_APN', @szPIN, 
										'Error adding Parcel record to Geo_Restrictions2')
			END

		END
		ELSE -- If a link is not found, insert record
		BEGIN
			-- Set unique RecordID
			SET @szLocRecordID = NULL
			-- Reset counter for unique number string at end of RecordID
			SET @lCounter = @lCounter + 1
			SET @szLocRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
			 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
			 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
			 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)	 + right(('000000'+ ltrim(str(@lCounter))),6)

			INSERT INTO [dbo].[Activity] ([ActivityID], [ActivityTypeID]) VALUES(@szLocRecordID, 5 )

			INSERT INTO GEO_OWNERSHIP (ActivityTypeID, RECORDID, SITE_APN, BATHROOMS, BEDROOMS, CENSUS, GENERAL_PLAN, IMPROVED_VALUE, LAND_VALUE,
									   TOTAL_VALUE, LOT_SIZE_SF, NO_STORIES, NO_UNITS, SCHOOL_DIST, SECTION_TWP_RNG, SITE_BLOCK,
									   SITE_LOT_NO, SITE_SUBDIVISION, STRUCTURE_SF, YEAR_BUILT, ZONE_CODE2, ZONE_CODE1,
									   OWNER_NAME, OWNER_ADDR1, OWNER_CITY, OWNER_STATE, OWNER_ZIP,
									   SITE_ADDR, SITE_NUMBER, SITE_ST_NAME, SITE_STREETNAME,
									   SITE_ST_PREFIX, SITE_ST_TYPE, SITE_UNIT_NO, SITE_CITY, SITE_STATE, SITE_ZIP,
									   Historical_APN, PARENT_SITE_APN, PARENT_RECORDID, STATUS, GEOTYPE, MULTI_ADDRESS)
							   VALUES (5, @szLocRecordID, @szPIN, @fBaths, @lBedrooms, SUBSTRING(@szBlockGrpID, 1, 10), SUBSTRING(@szCompPlanDesc, 1, 10), @fTotalImprv, @fTotalLandVal,
									   @fTotalVal,  @fNetSF, @fStories, @fNoUnits, 'ADAMS DISTRICT 50', SUBSTRING(@szSecTwpRng, 1, 20), SUBSTRING(@szBlock, 1, 10),
									   SUBSTRING(@szLot, 1, 10), @szSubDivName, @fTotalSqFt, @fYrBlt, SUBSTRING(@szZoning, 1, 10), 'SEE CUSTOM ZONING SCREEN',
									   @szOwnerName, @szMailAddr, @szMailCity, @szMailState, @szMailZip,
									   SUBSTRING(@szFullAddr, 1, 70), @szStNum, SUBSTRING(@szStName, 1, 30), @szSiteStreet,
									   @szPreDir, @szStSuffix, @szUnitNum, @szCity, 'CO', @szZip,
									   @szOldPIN, @szOldPIN, @szParentRecID, 'ACTIVE', 'PARCEL', 0)

				IF @@error <> 0
				   Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Comments)
								Values ('LANDTRAKINT', 21, 'ERROR', Getdate(), 'SITE APN', @szPIN, 'Error adding Parcel record to Geo_Ownership')
				ELSE
				BEGIN
				   -- Log new record for reporting purposes
				   Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Comments)
								Values ('LANDTRAKINT', 30, 'LOG', Getdate(), 'SITE APN', @szPIN, 'New Parcel record added to Geo_Ownership')

					INSERT INTO GEO_OWNERSHIPUDF (LOC_RECORDID, SITE_APN, LANDUSE01, LANDUSE03, LANDUSE05, BLDGDATA01, BLDGDATA03, DFLT_INSPCTR_PERMIT)
										  VALUES (@szLocRecordID, @szPIN, @szLandUse01, @szLandUse03, @szLOMACaseNo, @lBuildings, @szStructType, @szInspector)

					-- Reset counter for unique number string at end of RecordID
					SET @lCounter = @lCounter + 1
					-- Set unique RecordID
					SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
					 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
					 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
					 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)	 + right(('000000'+ ltrim(str(@lCounter))),6)

					INSERT INTO GEO_UDF (RECORDID, LOC_RECORDID, SITE_APN, WATER_DIST, SANIT_DIST, COUNCIL_DIST, FIRE_DIST, SCHOOL_DIST, METRO_DIST,
										 HISTORIC_DIST, URBRENEW_AREA, CODE_INSP, TAZ_ID, CONSERV_AREA, ZONING, GEN_ZONING,
										 ZONE_DESC, UNITS, MAX_HEIGHT, ORD_RESTRICT, BLD1_YRBLT, BLD2_YRBLT, BLD3_YRBLT, BLD4_YRBLT, BLD1_SQFT,
										 BLD2_SQFT, BLD3_SQFT, BLD4_SQFT, CLASS_CODE1, CLASS_CODE2, CLASS_CODE3, CLASS_CODE4, CODE_DESC1, CODE_DESC2,
										 CODE_DESC3, CODE_DESC4, SPCL_STBK_DEV, ORD_NUMBER, DBL_FRNT_LOT, ZONE_LINK, SUBDIVISIONLINK)
								 VALUES (@szRecordID, @szLocRecordID, @szPIN, @szWaterDist, @szSanitDist, @szCouncilDist, @szFireDist, 'ADAMS DISTRICT 50', @szMetroDist,
										 @szHistDist, @szUrbanRenew, @szCodeInsp, @fTAZID, @szConservArea, @szZoning, @szGenZoning,
										 @szZoningDesc, @szUnits, @szMaxHeight, @szOrdRestrict, @fSTTYrBlt, @fSTTYrBlt2, @fSTTYrBlt3, @fSTTYrBlt4, @fSqFt1,
										 @fSqFt2, @fSqFt3, @fSqFt4, @szClassCode1, @szClassCode2, @szClassCode3, @szClassCode4, @szCodeDesc1, @szCodeDesc2,
										 @szCodeDesc3, @szCodeDesc4, @szSpecStbkDev, @szOrdinanceNo, @szDblFrntLot, @szDevStndURL, @szSubDivLink)

					-- Insert Owner into Geo_People
					IF @szOwnerName IS NOT NULL
					BEGIN
						-- Reset counter for unique number string at end of RecordID
						SET @lCounter = @lCounter + 1
						-- Set unique RecordID
						SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
						 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
						 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
						 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)	 + right(('000000'+ ltrim(str(@lCounter))),6)

						INSERT INTO PEOPLE (RECORDID, ActivityID, ActivityTypeID, SITE_APN, NAMETYPE, NAME, OTHER_NAME, ADDRESS1, CITY, STATE, ZIP)
										VALUES (@szRecordID, @szLocRecordID, 5, @szPIN, 'OWNER', @szOwnerName, @szOwnerName2, @szMailAddr, @szMailCity, @szMailState, @szMailZip)

						IF @@error <> 0
							Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Keycol2, Keydata2, Comments)
										Values ('LANDTRAKINT', 23, 'ERROR', Getdate(), 'OWNER', @szOwnerName, 'SITE APN', @szPIN, 'Error adding Parcel record to Geo_People')
					END

					-- Insert Legal Description
					IF @szLegal IS NOT NULL
					BEGIN
						INSERT INTO GEO_LEGALS (LOC_RECORDID, SITE_APN, DESCRIPTION) VALUES (@szLocRecordID, @szPIN, @szLegal)

						IF @@error <> 0
							Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Comments)
										Values ('LANDTRAKINT', 24, 'ERROR', Getdate(), 'SITE_APN', @szPIN, 'Error adding Parcel record to Geo_Legals')

					END

					-- Insert Flood Zone Restrictions
					IF @szFloodZone IN ('AE', 'FW', 'A', 'AO')
					BEGIN
						-- Reset counter for unique number string at end of RecordID
						SET @lCounter = @lCounter + 1
						-- Set unique RecordID
						SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
						 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
						 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
						 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)	 + right(('000000'+ ltrim(str(@lCounter))),6)

						INSERT INTO GEO_RESTRICTIONS2 (RECORDID, LOC_RECORDID, SITE_APN, RESTRICTION_TYPE, RESTRICTION_NOTES)
											   VALUES (@szRecordID, @szLocRecordID, @szPIN, 'WARNING', 'SEE ENGINEERING; PARCEL IN FLOODPLAIN')

						IF @@error <> 0
							Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Keycol2, Keydata2, Comments)
										Values ('LANDTRAKINT', 25, 'ERROR', Getdate(), 'RESTRICTION TYPE', 'WARNING: FLOODPLAIN', 'SITE_APN', @szPIN, 
												'Error adding Parcel record to Geo_Restrictions2') 
					END

					-- Insert Flood Zone Restrictions
					IF @szFloodZone = 'FW'
					BEGIN
						-- Reset counter for unique number string at end of RecordID
						SET @lCounter = @lCounter + 1
						-- Set unique RecordID
						SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
						 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
						 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
						 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)	 + right(('000000'+ ltrim(str(@lCounter))),6)

						INSERT INTO GEO_RESTRICTIONS2 (RECORDID, LOC_RECORDID, SITE_APN, RESTRICTION_TYPE, RESTRICTION_NOTES)
											   VALUES (@szRecordID, @szLocRecordID, @szPIN, 'WARNING', 'SEE ENGINEERING; PARCEL IN FLOODWAY')

						IF @@error <> 0
							Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Keycol2, Keydata2, Comments)
										Values ('LANDTRAKINT', 25, 'ERROR', Getdate(), 'RESTRICTION TYPE', 'WARNING: FLOODWAY', 'SITE_APN', @szPIN, 
												'Error adding Parcel record to Geo_Restrictions2') 
					END

					-- Insert Historic Restrictions
					IF @szHistDist = 'ARVADA DOWNTOWN'
					BEGIN
						-- Reset counter for unique number string at end of RecordID
						SET @lCounter = @lCounter + 1
						-- Set unique RecordID
						SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
						 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
						 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
						 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)	 + right(('000000'+ ltrim(str(@lCounter))),6)

						INSERT INTO GEO_RESTRICTIONS2 (RECORDID, LOC_RECORDID, SITE_APN, RESTRICTION_TYPE, RESTRICTION_NOTES)
											   VALUES (@szRecordID, @szLocRecordID, @szPIN, 'WARNING', 'PARCEL IN HISTORIC DISTRICT')

						IF @@error <> 0
							Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Keycol2, Keydata2, Comments)
										Values ('LANDTRAKINT', 25, 'ERROR', Getdate(), 'RESTRICTION TYPE', 'WARNING: HISTORIC DISTRICT', 'SITE_APN', @szPIN, 
												'Error adding Parcel record to Geo_Restrictions2') 
					END

					-- Insert Olde Town Area Restrictions
					IF @szConservArea = 'OLDE TOWN AREA'
					BEGIN
						-- Reset counter for unique number string at end of RecordID
						SET @lCounter = @lCounter + 1
						-- Set unique RecordID
						SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
						 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
						 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
						 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)	 + right(('000000'+ ltrim(str(@lCounter))),6)

						INSERT INTO GEO_RESTRICTIONS2 (RECORDID, LOC_RECORDID, SITE_APN, RESTRICTION_TYPE, RESTRICTION_NOTES)
											   VALUES (@szRecordID, @szLocRecordID, @szPIN, 'WARNING', 'OLDE TOWN AREA')

						IF @@error <> 0
							Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Keycol2, Keydata2, Comments)
										Values ('LANDTRAKINT', 25, 'ERROR', Getdate(), 'RESTRICTION TYPE', 'WARNING: OLDE TOWN AREA', 'SITE_APN', @szPIN, 
												'Error adding Parcel record to Geo_Restrictions2')
					END

				END -- End Inserts if Geo_Ownership inserted without error
		END -- End Inserts for Parcel

  --------------------
  -- Variable Reset --
  --------------------
  SELECT @szLocRecordID = NULL, @szRecordID = NULL, @szSiteStreet = NULL, @szSiteNumber = NULL, @fTotalVal = 0, @fSTTYrBlt = 0, @fSTTYrBlt2 = 0, @fSTTYrBlt3 = 0, @fSTTYrBlt4 = 0,
  @fSqFt1 = 0, @fSqFt2 = 0, @fSqFt3 = 0, @fSqFt4 = 0, @szClassCode1 = NULL, @szClassCode2 = NULL, @szClassCode3 = NULL, @szClassCode4 = NULL, @szCodeDesc1 = NULL, @szCodeDesc2 = NULL,
  @szCodeDesc3 = NULL, @szCodeDesc4 = NULL, @szSec = NULL, @szSecTwpRng = NULL, @szLandUse01 = NULL, @szLandUse03 = NULL, @szOldBath = NULL, @szOldBed = NULL, @szOldCensus = NULL,
  @szOldGenPlan = NULL, @fOldImpVal = 0, @fOldLandVal = 0, @fOldLotSize = 0, @lOldNoStories = 0, @szOldNoUnits = NULL, @szOldSecTwpRng = NULL, @szOldBlock = NULL, @szOldLot = NULL,
  @szOldSubdiv = NULL, @fOldStructure = 0, @fOldYrBuilt = 0, @szOldZone2 = NULL, @szOldSiteAddr = NULL, @szOldOwner = NULL, @szOldOwnAddr1 = NULL, @szOldOwnAddr2 = NULL,
  @szOldOwnCity = NULL, @szOldOwnState = NULL, @szOldOwnZip = NULL, @szOldOthName = NULL, @szOldWaterDist = NULL, @szOldSanitDist = NULL, @szOldCouncilDist = NULL, @szOldFireDist = NULL,
  @szOldMetroDist = NULL, @szOldHistDist = NULL, @szOldUrbanRenew = NULL, @szOldCodeInsp = NULL, @fOldTAZID = 0, @szOldConservArea = NULL, @szOldZoning = NULL, @szOldGenZoning = NULL,
  @szOldZoningDesc = NULL, @szOldUnits = NULL, @szOldMaxHeight = NULL, @szOldOrdRestrict = NULL, @fOldBld1YrBlt = 0, @fOldBld2YrBlt = 0, @fOldBld3YrBlt = 0, @fOldBld4YrBlt = 0,
  @fOldBld1Sqft = 0, @fOldBld2Sqft = 0, @fOldBld3Sqft = 0, @fOldBld4Sqft = 0, @szOldClassCode1 = NULL, @szOldClassCode2 = NULL, @szOldClassCode3 = NULL, @szOldClassCode4 = NULL,
  @szOldCodeDesc1 = NULL, @szOldCodeDesc2 = NULL, @szOldCodeDesc3 = NULL, @szOldCodeDesc4 = NULL, @szOldSpecStbkDev = NULL, @szOldLegalDesc = NULL, @szOldLand01 = NULL,
  @szOldLand03 = NULL, @szOldLand05 = NULL, @szOldBldg01 = NULL, @szOldBldg03 = NULL, @szOldOrdNo = NULL, @szOldDblFrntLot = NULL, @fBaths = NULL, @lBedrooms = NULL,
  @fStories = NULL, @fNoUnits = NULL, @fTotalSqFt = NULL, @fYrBlt = NULL, @szOldZoneLink = NULL, @szOldPIN = NULL, @szParentRecID = NULL, @szOldSubDivLink = NULL, @szOldDfltInspctr = NULL, @fSUM_SQFT = NULL,
  @szOldStreetName = null 

  Fetch Next From cGetAdamsParcel Into @szPIN, @szBlockGrpID, @szCompPlanDesc, @fTotalImprv, @fTotalLandVal, @fNetSF,
								  @szMailAddr, @szMailCity, @szOwnerName, @szMailState, @szMailZip, @szSection, @szFullAddr, @szBlock, @szCity,
								  @szLot, @szStNum, @szStName, @szPreDir, @szStSuffix, @szSubDivName, @szUnitNum, @szZip, @szZoning,
								  @szWaterDist, @szSanitDist, @szCouncilDist, @szFireDist, @szMetroDist, @szHistDist,
								  @szUrbanRenew, @szCodeInsp, @fTAZID, @szConservArea, @szGenZoning, @szZoningDesc, @szUnits,
								  @szMaxHeight, @szOrdRestrict, @szFloodZone, @szLOMACaseNo, @lBuildings, @szStructType, @szLegal, @szOwnerName2,
								  @szOrdinanceNo, @szSpecStbkDev, @szDblFrntLot, @szDevStndURL, @szSubDivLink, @szInspector
  End --While
  Close cGetAdamsParcel
  Deallocate cGetAdamsParcel

  ------------------------------------------
  -- Process Jefferson County Parcel Data --
  ------------------------------------------
  DECLARE cGetParcel CURSOR LOCAL FOR        
  SELECT RTRIM(PPIN), BLOCKGROUP_ID, UPPER(COMPPLAN_DESCRIPTION), TOTACTIMPV, TOTACTLNDV, TOTACR * 43560, STTNBRFLR, STTNBRUNT, MAILSTRNBR, MAILSTRDIR, MAILSTRNAM,
		 MAILSTRTYP, MAILSTRSFX, MAILSTRUNT, UPPER(OWNICO), UPPER(MAILCTYNAM), UPPER(OWNNAM), UPPER(MAILSTENAM), RTRIM(CAST(MAILZIP5 AS VARCHAR) + '-' + ISNULL(CAST(MAILZIP4 AS VARCHAR), ' ')),
		 CAST(CAST(LGLSEC AS INT) AS VARCHAR) + LGLQTR + CAST(CAST(LGLTWN AS INT) AS VARCHAR) + CAST(CAST(LGLRNG AS INT) AS VARCHAR), UPPER(LGLBLK), UPPER(PRPCTYNAM), UPPER(LGLLOT), PRPSTRNUM, UPPER(PRPSTRNAM), UPPER(PRPSTRDIR), UPPER(PRPSTRTYP), UPPER(PRPSTRSFX), UPPER(SUBNAM),
		 PRPSTRUNT, RTRIM(CAST(PRPZIP5 AS VARCHAR) + '-' + ISNULL(CAST(PRPZIP4 AS VARCHAR), ' ')), STTYRBLT, UPPER(ZONING), UPPER(WATER_DISTRICT), UPPER(SANITATION_DISTRICT), UPPER(COUNCIL_DISTRICT), UPPER(FIRE_DISTRICT),
		 UPPER(METRO_DISTRICT), UPPER(HISTORIC_DISTRICT), UPPER(URBAN_RENEWAL_AREA), UPPER(CODE_INSPECTOR), TAZ_ID, UPPER(OLDE_TOWN_AREA), UPPER(GEN_ZONING), UPPER(ZONING_DESCRIPTION), UNITS,
		 MAXIMUM_HEIGHT, UPPER(ORD_RESTRICT), UPPER(STTTYPUSE), UPPER(STTTYPUSE2), UPPER(STTTYPUSE3), UPPER(STTTYPUSE4), STTYRBLT, STTYRBLT2, STTYRBLT3, STTYRBLT4, STTGRSAREA, STTGRSARE2,
		 STTGRSARE3, STTGRSARE4, TAXCLS, TAXCLS2, TAXCLS3, TAXCLS4, UPPER(FLOOD_ZONE), LOMA_CASE_NO, STTNBRBLDG, UPPER(STTSTRC), UPPER(OWNNAM2),
		 ORDINANCE_NO, SPECIAL_SETBACKS_DEV, DBLFRONTLOT, DEV_STANDARDS_URL, SUBDIVISION_LINK, I.INSPECTOR
	FROM JEFFCO_PARCELS P
	LEFT JOIN PARCELS_INSPECTOR I ON P.PPIN = I.PIN
   
  Open cGetParcel 
  Fetch Next From cGetParcel Into @szPIN, @szBlockGrpID, @szCompPlanDesc, @fTotalImprv, @fTotalLandVal, @fNetSF, @fStories, @fNoUnits, @szMailStrNbr, @szMailStrDir, @szMailStrName,
								  @szMailStrType, @szMailStrSfx, @szMailStrUnit, @szOwnCo, @szMailCity, @szOwnerName, @szMailState, @szMailZip,
								  @szSection, @szBlock, @szCity, @szLot, @szStNum, @szStName, @szPreDir, @szStType, @szStSuffix, @szSubDivName,
								  @szUnitNum, @szZip, @fYrBlt, @szZoning, @szWaterDist, @szSanitDist, @szCouncilDist, @szFireDist,
								  @szMetroDist, @szHistDist, @szUrbanRenew, @szCodeInsp, @fTAZID, @szConservArea, @szGenZoning, @szZoningDesc, @szUnits,
								  @szMaxHeight, @szOrdRestrict, @szStTypeUse, @szStTypeUse2, @szStTypeUse3, @szStTypeUse4, @fSTTYrBlt, @fSTTYrBlt2, @fSTTYrBlt3, @fSTTYrBlt4, @fTotalSqFt, @fSqFt2,
								  @fSqFt3, @fSqFt4, @szClassCode1, @szClassCode2, @szClassCode3, @szClassCode4, @szFloodZone, @szLOMACaseNo, @lBuildings, @szStructType, @szOwnerName2,
								  @szOrdinanceNo, @szSpecStbkDev, @szDblFrntLot, @szDevStndURL, @szSubDivLink, @szInspector
  While @@Fetch_status <> -1 
  Begin
		-- Get historic PIN
		SELECT @szOldPIN = OLDPIN FROM COA_HISTORICAL WHERE PIN = @szPIN

		--JBH : Lookup table no longer needed
		-- Get LU Info
		--SELECT @szBLD1Stats = UPPER(DESCRIPTION) FROM JEFF_STTYPEUSE WHERE ST_CODE = @szStTypeUse
		--SELECT @szBLD2Stats = UPPER(DESCRIPTION) FROM JEFF_STTYPEUSE WHERE ST_CODE = @szStTypeUse2
		--SELECT @szBLD3Stats = UPPER(DESCRIPTION) FROM JEFF_STTYPEUSE WHERE ST_CODE = @szStTypeUse3
		--SELECT @szBLD4Stats = UPPER(DESCRIPTION) FROM JEFF_STTYPEUSE WHERE ST_CODE = @szStTypeUse4
		--these used to be lookups but now there the actual descriptions:
		SELECT @szBLD1Stats =  @szStTypeUse
		SELECT @szBLD2Stats =  @szStTypeUse2
		SELECT @szBLD3Stats =  @szStTypeUse3
		SELECT @szBLD4Stats =  @szStTypeUse4


		SELECT @szCodeDesc1 = UPPER(DESCRIPTION) FROM JEFF_TAXCLASS WHERE TAXCLSCODE = @szClassCode1
		SELECT @szCodeDesc2 = UPPER(DESCRIPTION) FROM JEFF_TAXCLASS WHERE TAXCLSCODE = @szClassCode2
		SELECT @szCodeDesc3 = UPPER(DESCRIPTION) FROM JEFF_TAXCLASS WHERE TAXCLSCODE = @szClassCode3
		SELECT @szCodeDesc4 = UPPER(DESCRIPTION) FROM JEFF_TAXCLASS WHERE TAXCLSCODE = @szClassCode4
		
		 
		--in jeffco, they do not give accurate data for # buildings and total sq feet
		-- for sq feet null it out unless there is just 1 building
		-- for number buildings as long as it is 3 and under we know

		---@fTotalSqFt is being used to set SUM_SQFT and STRUCTURE SQ Feet, we need a new variable
		-- so SUM_SQFT should get a new variable set to STTGRSAREA and always use it
		set @fSUM_SQFT = @fTotalSqFt

		if (isnull(@fSTTYrBlt4,0) > 0)
		begin
			set @fTotalSqFt = null --is it 4 or 4+ we do not know for sure, so have to blank them out
			set @lBuildings = null --? 4+>
		end
		else if (isnull(@fSTTYrBlt3,0) > 0)
		begin
			set @fTotalSqFt = isnull(@fTotalSqFt,0) + isnull(@fSqFt2,0) + isnull(@fSqFt3,0) + isnull(@fSqFt4,0)
			set @lBuildings = 3
		end
		else if (isnull(@fSTTYrBlt2,0) > 0)
		begin
			set @fTotalSqFt = isnull(@fTotalSqFt,0) + isnull(@fSqFt2,0) + isnull(@fSqFt3,0) + isnull(@fSqFt4,0)
			set @lBuildings = 2
		end
		else  
		begin 
			--total sq feet is correct for 1 building
			set @lBuildings = 1
		end
		 --@fTotalSqFt = the total structure sq feet and goes into geo_ownership table as StructureSqFt
		 --@fSUM_SQFT = BLD1 sq feet and goes into the custom udf table as SUM, which is really BLD1 now
		  


		-- Set MailAddr
		SET @szMailAddr = SUBSTRING(RTRIM(RTRIM(RTRIM(RTRIM(LTRIM(ISNULL(@szMailStrNbr, ' ') + ' ' + ISNULL(@szMailStrDir, ' '))) + ' ' + @szMailStrName + ' ' + ISNULL(@szMailStrType, ' ')) + ' ' + ISNULL(@szMailStrSfx, ' ')) + ' ' + ISNULL(@szMailStrUnit, ' ')), 1, 60)

		-- Set SiteAddr
		SET @szSiteAddr = LTRIM(SUBSTRING(RTRIM(RTRIM(RTRIM(RTRIM(LTRIM(ISNULL(@szStNum, ' ') + ' ' + ISNULL(@szPreDir, ' '))) + ' ' + @szStName + ' ' + ISNULL(@szStType, ' ')) + ' ' + ISNULL(@szStSuffix, ' ')) + ' ' + ISNULL(@szUnitNum, ' ')), 1, 70))

		-- Set StreetName
		SET @szSiteStreet = SUBSTRING(RTRIM(RTRIM(LTRIM(ISNULL(@szPreDir, ' ') + ' ' + @szStName) + ' ' + ISNULL(@szStType, ' ')) + ' ' + ISNULL(@szStSuffix, ' ')), 1, 60)
		
		-- Set Total Value
		SET @fTotalVal = @fTotalImprv + @fTotalLandVal
		
		-- Set CO Owner Addr2
		IF @szOwnCo IS NOT NULL AND @szOwnCo <> ' ' AND @szOwnCo <> ''
			SET @szOwnCo = 'C/O ' + @szOwnCo

		-- Set LandUse fields
		IF @szFloodZone IN ('AE', 'FW', 'A', 'AO')	SET @szLandUse01 = 'YES'
		ELSE SET @szLandUse01 = NULL
			
		IF @szFloodZone = 'FW'	SET @szLandUse03 = 'YES'
		ELSE SET @szLandUse03 = NULL
			
		-- Set LegalDesc
		SET @szLegal = 'SUB: ' + ISNULL(@szSubDivName, ' ') + ' BLK: ' + ISNULL(@szBlock, ' ') + ' LOT: ' + ISNULL(@szLot, ' ')

		-- Get RecordID if link found
		SELECT @szLocRecordID = RECORDID FROM GEO_OWNERSHIP WHERE SITE_APN = @szPIN AND GEOTYPE = 'PARCEL'

		-- Get ParentRecordID if link found
		SELECT @szParentRecID = RECORDID FROM GEO_OWNERSHIP WHERE SITE_APN = @szOldPIN AND GEOTYPE = 'PARCEL'

		-- If a link is found, update record
		IF @szLocRecordID IS NOT NULL
		BEGIN
			-- Get old info to compare and see if update is necessary
			SELECT @szOldCensus = CENSUS, @szOldGenPlan = GENERAL_PLAN, @fOldImpVal = IMPROVED_VALUE, @fOldLandVal = LAND_VALUE, @fOldLotSize = LOT_SIZE_SF,
					@lOldNoStories = NO_STORIES, @szOldNoUnits = NO_UNITS, @szOldSecTwpRng = SECTION_TWP_RNG, @szOldBlock = SITE_BLOCK, @szOldLot = SITE_LOT_NO, @szOldSubdiv = SITE_SUBDIVISION,
					@fOldStructure = STRUCTURE_SF, @fOldYrBuilt = YEAR_BUILT, @szOldZone2 = ZONE_CODE2, @szOldSiteAddr = SITE_ADDR, @szOldParent = @szOldPIN,
					@szOldOwner = OWNER_NAME, @szOldOwnAddr1 = OWNER_ADDR1, @szOldOwnAddr2 = OWNER_ADDR2, @szOldOwnCity = OWNER_CITY, @szOldOwnState = OWNER_STATE, @szOldOwnZip = OWNER_ZIP,
					@szOldStreetName = SITE_STREETNAME
				FROM GEO_OWNERSHIP WHERE RECORDID = @szLocRecordID
			-- If anything has been updated, update record
			IF ISNULL(@szOldCensus, ' ') <> SUBSTRING(ISNULL(@szBlockGrpID, ' '), 1, 10) OR
			   ISNULL(@szOldGenPlan, ' ') <> SUBSTRING(ISNULL(@szCompPlanDesc, ' '), 1, 10) OR ISNULL(@fOldImpVal, 0) <> ISNULL(@fTotalImprv, 0) OR ISNULL(@fOldLandVal, 0) <> ISNULL(@fTotalLandVal, 0) OR
			   ISNULL(@fOldLotSize, 0) <> ISNULL(@fNetSF, 0) OR ISNULL(@lOldNoStories, 0) <> ISNULL(@fStories, 0) OR ISNULL(@szOldNoUnits, 0) <> ISNULL(@fNoUnits, 0) OR
			   ISNULL(@szOldSecTwpRng, ' ') <> SUBSTRING(ISNULL(@szSection, ' '), 1, 20) OR ISNULL(@szOldBlock, ' ') <> SUBSTRING(ISNULL(@szBlock, ' '), 1, 10) OR
			   ISNULL(@szOldLot, ' ') <> SUBSTRING(ISNULL(@szLot, ' '), 1, 10) OR ISNULL(@szOldSubdiv, ' ') <> ISNULL(@szSubDivName, ' ') OR ISNULL(@fOldStructure, 0) <> ISNULL(@fTotalSqFt, 0) OR
			   ISNULL(@fOldYrBuilt, 0) <> ISNULL(@fYrBlt, 0) OR ISNULL(@szOldZone2, ' ') <> SUBSTRING(ISNULL(@szZoning, ' '), 1, 10) OR --ISNULL(@szOldTRA, ' ') <> ISNULL(@szATD, ' ') OR
			   ISNULL(@szOldOwner, ' ') <> ISNULL(@szOwnerName, ' ') OR ISNULL(@szOldOwnAddr1, ' ') <> ISNULL(@szMailAddr, ' ') OR
			   ISNULL(@szOldOwnAddr2, ' ') <> ISNULL( @szOwnCo, ' ') OR ISNULL(@szOldOwnCity, ' ') <> ISNULL(@szMailCity, ' ') OR
			   ISNULL(@szOldOwnState, ' ') <> ISNULL(@szMailState, ' ') OR ISNULL(@szOldOwnZip, ' ') <> ISNULL(@szMailZip, ' ') OR ISNULL(@szOldSiteAddr, ' ') <> ISNULL(@szSiteAddr, ' ') OR
			   ISNULL(@szOldParent, ' ') <> ISNULL(@szOldPIN, ' ') OR isnull(@szOldStreetName,'') <> isnull(@szSiteStreet,'')
			BEGIN
				UPDATE GEO_OWNERSHIP SET CENSUS = SUBSTRING(@szBlockGrpID, 1, 10), GENERAL_PLAN = SUBSTRING(@szCompPlanDesc, 1, 10), --TRA = @szATD,
										 IMPROVED_VALUE = @fTotalImprv, LAND_VALUE = @fTotalLandVal, TOTAL_VALUE = @fTotalVal, LOT_SIZE_SF = @fNetSF, NO_STORIES = @fStories,
										 NO_UNITS = @fNoUnits, SECTION_TWP_RNG = SUBSTRING(@szSection, 1, 20), SITE_BLOCK = SUBSTRING(@szBlock, 1, 10), SITE_LOT_NO = SUBSTRING(@szLot, 1, 10),
										 SITE_SUBDIVISION = @szSubDivName, STRUCTURE_SF = @fTotalSqFt, YEAR_BUILT = @fYrBlt, ZONE_CODE2 = SUBSTRING(@szZoning, 1, 10),
										 OWNER_NAME = @szOwnerName, OWNER_ADDR1 = @szMailAddr, OWNER_ADDR2 = @szOwnCo,
										 OWNER_CITY = @szMailCity, OWNER_STATE = @szMailState, OWNER_ZIP = @szMailZip,
										 SITE_ADDR = @szSiteAddr, SITE_NUMBER = @szStNum, SITE_ST_NAME = SUBSTRING(@szStName, 1, 30), SITE_STREETNAME = @szSiteStreet,
										 SITE_ST_PREFIX = @szPreDir, SITE_ST_TYPE = @szStSuffix, SITE_UNIT_NO = @szUnitNum,
										 SITE_CITY = @szCity, SITE_ZIP = @szZip, Historical_APN = @szOldPIN, PARENT_SITE_APN = @szOldPIN, PARENT_RECORDID = @szParentRecID
						WHERE RECORDID = @szLocRecordID

					IF @@error <> 0
					BEGIN
					   Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Comments)
									Values ('LANDTRAKINT', 20, 'ERROR', Getdate(), 'SITE APN', @szPIN, 'Error updating Parcel record in Geo_Ownership')
					END

				-- if ownername has changed, insert change log record
				IF ISNULL(@szOldOwner, ' ') <> ISNULL(@szOwnerName, ' ')
				BEGIN
					-- Set unique RecordID
					SET @szRecordID = NULL
					-- Reset counter for unique number string at end of RecordID
					SET @lCounter = @lCounter + 1
					SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
					 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
					 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
					 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)	 + right(('000000'+ ltrim(str(@lCounter))),6)
					 
					INSERT INTO GEO_CHANGE_LOG(ENTRY_DATE, FIELD_CHANGED, LOC_RECORDID, NEW_VALUE, OLD_VALUE, RECORDID)
										VALUES(GetDate(), 'OWNER_NAME', @szLocRecordID, @szOwnerName, @szOldOwner, @szRecordID)
				END

				-- if site address has changed, insert change log record
				IF ISNULL(@szOldSiteAddr, ' ') <> ISNULL(@szSiteAddr, ' ')
				BEGIN
					-- Set unique RecordID
					SET @szRecordID = NULL
					-- Reset counter for unique number string at end of RecordID
					SET @lCounter = @lCounter + 1
					SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
					 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2) + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
					 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2) + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
					 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2) + right(('000000'+ ltrim(str(@lCounter))),6)
					 
					INSERT INTO GEO_CHANGE_LOG(ENTRY_DATE, FIELD_CHANGED, LOC_RECORDID, NEW_VALUE, OLD_VALUE, RECORDID)
										VALUES(GetDate(), 'SITE_ADDR', @szLocRecordID, @szSiteAddr, @szOldSiteAddr, @szRecordID)
				END

				-- if zonecode2 has changed, insert change log record
				IF ISNULL(@szOldZone2, ' ') <> SUBSTRING(ISNULL(@szZoning, ' '), 1, 10)
				BEGIN
					-- Set unique RecordID
					SET @szRecordID = NULL
					-- Reset counter for unique number string at end of RecordID
					SET @lCounter = @lCounter + 1
					SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
					 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
					 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
					 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)	 + right(('000000'+ ltrim(str(@lCounter))),6)
					 
					INSERT INTO GEO_CHANGE_LOG(ENTRY_DATE, FIELD_CHANGED, LOC_RECORDID, NEW_VALUE, OLD_VALUE, RECORDID)
										VALUES(GetDate(), 'ZONE_CODE2', @szLocRecordID, SUBSTRING(@szZoning, 1, 10), @szOldZone2, @szRecordID)
				END

			END

			-- Get old info to compare and see if update is necessary
			SELECT @szOldOwner = NAME, @szOldOthName = OTHER_NAME, @szOldOwnAddr1 = ADDRESS1, @szOldOwnAddr2 = ADDRESS2, @szOldOwnCity = CITY, @szOldOwnState = STATE,
					@szOldOwnZip = ZIP FROM PEOPLE WHERE ActivityID = @szLocRecordID AND NAMETYPE = 'OWNER'
			-- If owner name is different, update nametype to 'PREV OWNER' and insert new owner
			IF ISNULL(@szOldOwner, ' ') <> ISNULL(@szOwnerName, ' ')
			BEGIN
				UPDATE PEOPLE SET NAMETYPE = 'PREV OWNER' WHERE ActivityID = @szLocRecordID AND NAMETYPE = 'OWNER'
				
				-- Reset counter for unique number string at end of RecordID
				SET @lCounter = @lCounter + 1
				-- Set unique RecordID
				SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
				 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
				 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
				 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)	 + right(('000000'+ ltrim(str(@lCounter))),6)

				INSERT INTO PEOPLE (RECORDID, ActivityID, ActivityTypeID, SITE_APN, NAMETYPE, NAME, OTHER_NAME, ADDRESS1, ADDRESS2, CITY, STATE, ZIP)
								VALUES (@szRecordID, @szLocRecordID, 5, @szPIN, 'OWNER', @szOwnerName, @szOwnerName2, @szMailAddr, @szOwnCo, @szMailCity, @szMailState, @szMailZip)

				IF @@error <> 0
					Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Keycol2, Keydata2, Comments)
								Values ('LANDTRAKINT', 23, 'ERROR', Getdate(), 'OWNER', SUBSTRING(@szOwnerName, 1, 30), 'SITE APN', @szPIN, 
										'Error adding Parcel record to People')
			END
			-- If anything except ownername has been updated, update record
			ELSE IF ISNULL(@szOldOwner, ' ') = ISNULL(@szOwnerName, ' ') AND (ISNULL(@szOldOthName, ' ') <> ISNULL(@szOwnerName2, ' ') OR
			   ISNULL(@szOldOwnAddr1, ' ') <> ISNULL(@szMailAddr, ' ') OR ISNULL(@szOldOwnAddr2, ' ') <> ISNULL(@szOwnCo, ' ') OR
			   ISNULL(@szOldOwnCity, ' ') <> ISNULL(@szMailCity, ' ') OR ISNULL(@szOldOwnState, ' ') <> ISNULL(@szMailState, ' ') OR ISNULL(@szOldOwnZip, ' ') <> ISNULL(@szMailZip, ' '))
			BEGIN
				UPDATE PEOPLE SET NAME = @szOwnerName, OTHER_NAME = @szOwnerName2, ADDRESS1 = @szMailAddr, ADDRESS2 = @szOwnCo, CITY = @szMailCity, STATE = @szMailState, ZIP = @szMailZip
						WHERE ActivityID = @szLocRecordID AND NAMETYPE = 'OWNER'
				IF @@error <> 0
					Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Keycol2, Keydata2, Comments)
								Values ('LANDTRAKINT', 24, 'ERROR', Getdate(), 'OWNER', SUBSTRING(@szOwnerName, 1, 30), 'SITE APN', @szPIN, 
										'Error updating Parcel record in People')
			END

			-- Get old info to compare and see if update is necessary
			SELECT @szOldLand01 = LANDUSE01, @szOldLand03 = LANDUSE03, @szOldLand05 = LANDUSE05, @szOldBldg01 = BLDGDATA01, @szOldBldg03 = BLDGDATA03, @szOldDfltInspctr = DFLT_INSPCTR_PERMIT
				FROM GEO_OWNERSHIPUDF WHERE LOC_RECORDID = @szLocRecordID
			-- If anything has been updated, update record
			IF ISNULL(@szOldLand01, ' ') <> ISNULL(@szLandUse01, ' ') OR ISNULL(@szOldLand03, ' ') <> ISNULL(@szLandUse03, ' ') OR ISNULL(@szOldDfltInspctr, ' ') <> ISNULL(@szInspector, ' ') OR
			   ISNULL(@szOldLand05, ' ') <> ISNULL(@szLOMACaseNo, ' ') OR ISNULL(@szOldBldg01, ' ') <> ISNULL(@lBuildings, ' ') OR ISNULL(@szOldBldg03, ' ') <> ISNULL(@szStructType, ' ')
			BEGIN
				UPDATE GEO_OWNERSHIPUDF SET LANDUSE01 = @szLandUse01, LANDUSE03 = @szLandUse03, LANDUSE05 = @szLOMACaseNo, BLDGDATA01 = @lBuildings, BLDGDATA03 = @szStructType,
						DFLT_INSPCTR_PERMIT = @szInspector WHERE LOC_RECORDID = @szLocRecordID

					IF @@error <> 0
					BEGIN
					   Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Comments)
									Values ('LANDTRAKINT', 20, 'ERROR', Getdate(), 'SITE APN', @szPIN, 'Error updating Parcel record in Geo_OwnershipUDF')
					END

				-- if landuse01 has changed, insert change log record
				IF ISNULL(@szOldLand01, ' ') <> ISNULL(@szLandUse01, ' ')
				BEGIN
					-- Set unique RecordID
					SET @szRecordID = NULL
					-- Reset counter for unique number string at end of RecordID
					SET @lCounter = @lCounter + 1
					SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
					 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2) + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
					 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2) + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
					 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2) + right(('000000'+ ltrim(str(@lCounter))),6)
					 
					INSERT INTO GEO_CHANGE_LOG(ENTRY_DATE, FIELD_CHANGED, LOC_RECORDID, NEW_VALUE, OLD_VALUE, RECORDID)
										VALUES(GetDate(), 'LANDUSE01', @szLocRecordID, @szLandUse01, @szOldLand01, @szRecordID)
				END
			END

			-- Get old info to compare and see if update is necessary
			SELECT @szOldWaterDist = WATER_DIST, @szOldSanitDist = SANIT_DIST, @szOldCouncilDist = COUNCIL_DIST, @szOldFireDist = FIRE_DIST, @szOldMetroDist = METRO_DIST,
					@szOldHistDist = HISTORIC_DIST, @szOldUrbanRenew = URBRENEW_AREA, @szOldCodeInsp = CODE_INSP, @fOldTAZID = TAZ_ID, @szOldConservArea = CONSERV_AREA,
					@szOldZoning = ZONING, @szOldGenZoning = GEN_ZONING, @szOldZoningDesc = ZONE_DESC, @szOldUnits = UNITS, @szOldMaxHeight = MAX_HEIGHT, @szOldZoneLink = ZONE_LINK,
					@szOldOrdRestrict = ORD_RESTRICT, @fOldBld1YrBlt = BLD1_YRBLT, @fOldBld2YrBlt = BLD2_YRBLT, @fOldBld3YrBlt = BLD3_YRBLT, @fOldSumYrBlt = SUM_YRBLT,
					@fOldBld1Sqft = BLD1_SQFT, @fOldBld2Sqft = BLD2_SQFT, @fOldBld3Sqft = BLD3_SQFT, @fOldSumSqFt = SUM_SQFT, @szOldClassCode1 = CLASS_CODE1,
					@szOldClassCode2 = CLASS_CODE2, @szOldClassCode3 = CLASS_CODE3, @szOldClassCode4 = CLASS_CODE4, @szOldCodeDesc1 = CODE_DESC1, @szOldCodeDesc2 = CODE_DESC2,
					@szOldCodeDesc3 = CODE_DESC3, @szOldCodeDesc4 = CODE_DESC4, @szOldSpecStbkDev = SPCL_STBK_DEV, @szOldOrdNo = ORD_NUMBER, @szOldDblFrntLot = DBL_FRNT_LOT,
					@szOldSubDivLink = SUBDIVISIONLINK FROM GEO_UDF WHERE LOC_RECORDID = @szLocRecordID
			-- If anything has been updated, update record
			IF ISNULL(@szOldWaterDist, ' ') <> ISNULL(@szWaterDist, ' ') OR ISNULL(@szOldSanitDist, ' ') <> ISNULL(@szSanitDist, ' ') OR
			   ISNULL(@szOldCouncilDist, ' ') <> ISNULL(@szCouncilDist, ' ') OR ISNULL(@szOldFireDist, ' ') <> ISNULL(@szFireDist, ' ') OR
			   ISNULL(@szOldMetroDist, ' ') <> ISNULL(@szMetroDist, ' ') OR ISNULL(@szOldHistDist, ' ') <> ISNULL(@szHistDist, ' ') OR
			   ISNULL(@szOldUrbanRenew, ' ') <> ISNULL(@szUrbanRenew, ' ') OR ISNULL(@szOldCodeInsp, ' ') <> ISNULL(@szCodeInsp, ' ') OR
			   ISNULL(@fOldTAZID, 0) <> ISNULL(@fTAZID, 0) OR ISNULL(@szOldConservArea, ' ') <> ISNULL(@szConservArea, ' ') OR ISNULL(@szOldZoning, ' ') <> ISNULL(@szZoning, ' ') OR
			   ISNULL(@szOldGenZoning, ' ') <> ISNULL(@szGenZoning, ' ') OR ISNULL(@szOldZoningDesc, ' ') <> ISNULL(@szZoningDesc, ' ') OR
			   ISNULL(@szOldUnits, ' ') <> ISNULL(@szUnits, ' ') OR ISNULL(@szOldMaxHeight, ' ') <> ISNULL(@szMaxHeight, ' ') OR ISNULL(@szOldOrdRestrict, ' ') <> ISNULL(@szOrdRestrict, ' ') OR
			   ISNULL(@fOldBld1YrBlt, ' ') <> ISNULL(@fSTTYrBlt2, ' ') OR ISNULL(@fOldBld2YrBlt, ' ') <> ISNULL(@fSTTYrBlt3, ' ') OR ISNULL(@fOldBld3YrBlt, ' ') <> ISNULL(@fSTTYrBlt4, ' ') OR
			   ISNULL(@fOldBld4YrBlt, ' ') <> ISNULL(@fSTTYrBlt, ' ') OR ISNULL(@fOldBld1SqFt, ' ') <> ISNULL(@fSqFt2, ' ') OR ISNULL(@fOldBld2SqFt, ' ') <> ISNULL(@fSqFt3, ' ') OR
			   ISNULL(@fOldBld3SqFt, ' ') <> ISNULL(@fSqFt4, ' ') OR ISNULL(@fOldSumSqFt, ' ') <> ISNULL(@fTotalSqFt, ' ') OR ISNULL(@szOldClassCode1, ' ') <> ISNULL(@szClassCode1, ' ') OR
			   ISNULL(@szOldClassCode2, ' ') <> ISNULL(@szClassCode2, ' ') OR ISNULL(@szOldClassCode3, ' ') <> ISNULL(@szClassCode3, ' ') OR
			   ISNULL(@szOldClassCode4, ' ') <> ISNULL(@szClassCode4, ' ') OR ISNULL(@szOldCodeDesc1, ' ') <> ISNULL(@szCodeDesc1, ' ') OR
			   ISNULL(@szOldCodeDesc2, ' ') <> ISNULL(@szCodeDesc2, ' ') OR ISNULL(@szOldCodeDesc3, ' ') <> ISNULL(@szCodeDesc3, ' ') OR ISNULL(@szOldCodeDesc4, ' ') <> ISNULL(@szCodeDesc4, ' ') OR
			   ISNULL(@szOldSpecStbkDev, ' ') <> ISNULL(@szSpecStbkDev, ' ') OR ISNULL(@szOldOrdNo, ' ') <> ISNULL(@szOrdinanceNo, ' ') OR ISNULL(@szOldDblFrntLot, ' ') <> ISNULL(@szDblFrntLot, ' ') OR
			   ISNULL(@szOldZoneLink, ' ') <> ISNULL(@szDevStndURL, ' ') OR ISNULL(@szOldSubDivLink,' ') <> ISNULL(@szSubDivLink, ' ')
			BEGIN
				UPDATE GEO_UDF SET WATER_DIST = @szWaterDist, SANIT_DIST = @szSanitDist, COUNCIL_DIST = @szCouncilDist, FIRE_DIST = @szFireDist, METRO_DIST = @szMetroDist,
								   HISTORIC_DIST = @szHistDist, URBRENEW_AREA = @szUrbanRenew, CODE_INSP = @szCodeInsp, TAZ_ID = @fTAZID, CONSERV_AREA = @szConservArea,
								   ZONING = @szZoning, GEN_ZONING = @szGenZoning, ZONE_DESC = @szZoningDesc, UNITS = @szUnits, MAX_HEIGHT = @szMaxHeight,
								   ORD_RESTRICT = @szOrdRestrict, BLD1J_YRBLT = @fSTTYrBlt2, BLD2J_YRBLT = @fSTTYrBlt3, BLD3J_YRBLT = @fSTTYrBlt4, 
								   BLD1_STATS = @szBLD2Stats, BLD2_STATS = @szBLD3Stats, BLD3_STATS = @szBLD4Stats, 
								   
								   SUM_STATS = @szBLD1Stats,SUM_YRBLT = @fSTTYrBlt,SUM_SQFT = @fSUM_SQFT, --@fTotalSqFt,--@fSUM_SQFT

								   BLD1J_SQFT = @fSqFt2, BLD2J_SQFT = @fSqFt3, BLD3J_SQFT = @fSqFt4,  CLASS_CODE1 = @szClassCode1, CLASS_CODE2 = @szClassCode2,
								   CLASS_CODE3 = @szClassCode3, CLASS_CODE4 = @szClassCode4, CODE_DESC1 = @szCodeDesc1, CODE_DESC2 = @szCodeDesc2, CODE_DESC3 = @szCodeDesc3,
								   CODE_DESC4 = @szCodeDesc4, SPCL_STBK_DEV = @szSpecStbkDev, ORD_NUMBER = @szOrdinanceNo, DBL_FRNT_LOT = @szDblFrntLot, ZONE_LINK = @szDevStndURL,
								   SUBDIVISIONLINK = @szSubDivLink
						WHERE LOC_RECORDID = @szLocRecordID

				IF @@error <> 0
					Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Comments)
								Values ('LANDTRAKINT', 20, 'ERROR', Getdate(), 'SITE APN', @szPIN, 'Error updating Parcel record in Geo_UDF')
			END

			-- Get old info to compare and see if update is necessary
			SELECT @szOldLegalDesc = DESCRIPTION FROM GEO_LEGALS WHERE LOC_RECORDID = @szLocRecordID
			-- If anything has been updated, update record
			IF @szOldLegalDesc <> @szLegal
			BEGIN
				UPDATE GEO_LEGALS SET DESCRIPTION = @szLegal WHERE LOC_RECORDID = @szLocRecordID

				IF @@error <> 0
					Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Comments)
								Values ('LANDTRAKINT', 20, 'ERROR', Getdate(), 'SITE APN', @szPIN, 'Error updating Parcel record in Geo_Legals')
			END

			-- If current restrictions - drop all and reinsert any that exist in source
			IF (SELECT COUNT(*) FROM GEO_RESTRICTIONS2 WHERE LOC_RECORDID = @szLocRecordID) > 0
				DELETE FROM GEO_RESTRICTIONS2 WHERE LOC_RECORDID = @szLocRecordID AND SUBSTRING(RECORDID,1,4) = 'CONV'

			-- Insert Flood Zone Restrictions
			IF @szFloodZone IN ('AE', 'FW', 'A', 'AO')
			BEGIN
				-- Reset counter for unique number string at end of RecordID
				SET @lCounter = @lCounter + 1
				-- Set unique RecordID
				SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
				 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
				 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
				 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)	 + right(('000000'+ ltrim(str(@lCounter))),6)

				INSERT INTO GEO_RESTRICTIONS2 (RECORDID, LOC_RECORDID, SITE_APN, RESTRICTION_TYPE, RESTRICTION_NOTES)
									   VALUES (@szRecordID, @szLocRecordID, @szPIN, 'WARNING', 'SEE ENGINEERING; PARCEL IN FLOODPLAIN')

				IF @@error <> 0
					Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Keycol2, Keydata2, Comments)
								Values ('LANDTRAKINT', 25, 'ERROR', Getdate(), 'RESTRICTION TYPE', 'WARNING: FLOODPLAIN', 'SITE_APN', @szPIN, 
										'Error adding Parcel record to Geo_Restrictions2') 
			END

			-- Insert Flood Zone Restrictions
			IF @szFloodZone = 'FW'
			BEGIN
				-- Reset counter for unique number string at end of RecordID
				SET @lCounter = @lCounter + 1
				-- Set unique RecordID
				SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
				 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
				 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
				 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)	 + right(('000000'+ ltrim(str(@lCounter))),6)

				INSERT INTO GEO_RESTRICTIONS2 (RECORDID, LOC_RECORDID, SITE_APN, RESTRICTION_TYPE, RESTRICTION_NOTES)
									   VALUES (@szRecordID, @szLocRecordID, @szPIN, 'WARNING', 'SEE ENGINEERING; PARCEL IN FLOODWAY')

				IF @@error <> 0
					Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Keycol2, Keydata2, Comments)
								Values ('LANDTRAKINT', 25, 'ERROR', Getdate(), 'RESTRICTION TYPE', 'WARNING: FLOODWAY', 'SITE_APN', @szPIN, 
										'Error adding Parcel record to Geo_Restrictions2') 

			END

			-- Insert Historic Restrictions
			IF @szHistDist = 'ARVADA DOWNTOWN'
			BEGIN
				-- Reset counter for unique number string at end of RecordID
				SET @lCounter = @lCounter + 1
				-- Set unique RecordID
				SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
				 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
				 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
				 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)	 + right(('000000'+ ltrim(str(@lCounter))),6)

				INSERT INTO GEO_RESTRICTIONS2 (RECORDID, LOC_RECORDID, SITE_APN, RESTRICTION_TYPE, RESTRICTION_NOTES)
									   VALUES (@szRecordID, @szLocRecordID, @szPIN, 'WARNING', 'PARCEL IN HISTORIC DISTRICT')

				IF @@error <> 0
					Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Keycol2, Keydata2, Comments)
								Values ('LANDTRAKINT', 25, 'ERROR', Getdate(), 'RESTRICTION TYPE', 'WARNING: HISTORIC DISTRICT', 'SITE_APN', @szPIN, 
										'Error adding Parcel record to Geo_Restrictions2') 
			END

			-- Insert Olde Town Area Restrictions
			IF @szConservArea = 'OLDE TOWN AREA'
			BEGIN
				-- Reset counter for unique number string at end of RecordID
				SET @lCounter = @lCounter + 1
				-- Set unique RecordID
				SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
				 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
				 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
				 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)	 + right(('000000'+ ltrim(str(@lCounter))),6)

				INSERT INTO GEO_RESTRICTIONS2 (RECORDID, LOC_RECORDID, SITE_APN, RESTRICTION_TYPE, RESTRICTION_NOTES)
									   VALUES (@szRecordID, @szLocRecordID, @szPIN, 'WARNING', 'OLDE TOWN AREA')

					IF @@error <> 0
					   Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Keycol2, Keydata2, Comments)
									Values ('LANDTRAKINT', 25, 'ERROR', Getdate(), 'RESTRICTION TYPE', 'WARNING: OLDE TOWN AREA', 'SITE_APN', @szPIN, 
											'Error adding Parcel record to Geo_Restrictions2')
			END
		END
		ELSE -- If a link is not found, insert record
		BEGIN
			-- Set unique RecordID
			SET @szLocRecordID = NULL
			-- Reset counter for unique number string at end of RecordID
			SET @lCounter = @lCounter + 1
			SET @szLocRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
			 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
			 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
			 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)	 + right(('000000'+ ltrim(str(@lCounter))),6)

			INSERT INTO [dbo].[Activity] ([ActivityID], [ActivityTypeID]) VALUES(@szLocRecordID, 5 )

			INSERT INTO GEO_OWNERSHIP (ActivityTypeID, RECORDID, SITE_APN, CENSUS, GENERAL_PLAN, IMPROVED_VALUE, LAND_VALUE,
									   TOTAL_VALUE, LOT_SIZE_SF, NO_STORIES, NO_UNITS, SCHOOL_DIST, SECTION_TWP_RNG, SITE_BLOCK,
									   SITE_LOT_NO, SITE_SUBDIVISION, STRUCTURE_SF, YEAR_BUILT, ZONE_CODE2, ZONE_CODE1,
									   OWNER_NAME, OWNER_ADDR1, OWNER_ADDR2, OWNER_CITY, OWNER_STATE, OWNER_ZIP,
									   SITE_ADDR, SITE_NUMBER, SITE_ST_NAME, SITE_STREETNAME,
									   SITE_ST_PREFIX, SITE_ST_TYPE, SITE_UNIT_NO, SITE_CITY, SITE_STATE, SITE_ZIP,
									   Historical_APN, PARENT_SITE_APN, PARENT_RECORDID, STATUS, GEOTYPE, MULTI_ADDRESS)
							   VALUES (5, @szLocRecordID, @szPIN, SUBSTRING(@szBlockGrpID, 1, 10), SUBSTRING(@szCompPlanDesc, 1, 10), @fTotalImprv, @fTotalLandVal,
									   @fTotalVal,  @fNetSF, @fStories, @fNoUnits, 'JEFFCO PUBLICSCHOOLS', SUBSTRING(@szSection, 1, 20), SUBSTRING(@szBlock, 1, 10),
									   SUBSTRING(@szLot, 1, 10), @szSubDivName, @fTotalSqFt, @fYrBlt, SUBSTRING(@szZoning, 1, 10), 'SEE CUSTOM ZONING SCREEN',
									   @szOwnerName, @szMailAddr, @szOwnCo, @szMailCity, @szMailState, @szMailZip,
									   @szSiteAddr, @szStNum, SUBSTRING(@szStName, 1, 30), @szSiteStreet,
									   @szPreDir, @szStSuffix, @szUnitNum, @szCity, 'CO', @szZip,
									   @szOldPIN, @szOldPIN, @szParentRecID, 'ACTIVE', 'PARCEL', 0)

				IF @@error <> 0
				   Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Comments)
								Values ('LANDTRAKINT', 21, 'ERROR', Getdate(), 'SITE APN', @szPIN, 'Error adding Parcel record to Geo_Ownership')
				ELSE
				BEGIN
				   -- Log new record for reporting purposes
				   Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Comments)
								Values ('LANDTRAKINT', 30, 'LOG', Getdate(), 'SITE APN', @szPIN, 'New Parcel record added to Geo_Ownership')

					INSERT INTO GEO_OWNERSHIPUDF (LOC_RECORDID, SITE_APN, LANDUSE01, LANDUSE03, LANDUSE05, BLDGDATA01, BLDGDATA03, DFLT_INSPCTR_PERMIT)
										  VALUES (@szLocRecordID, @szPIN, @szLandUse01, @szLandUse03, @szLOMACaseNo, @lBuildings, @szStructType, @szInspector)

					-- Reset counter for unique number string at end of RecordID
					SET @lCounter = @lCounter + 1
					-- Set unique RecordID
					SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
					 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
					 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
					 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)	 + right(('000000'+ ltrim(str(@lCounter))),6)

					INSERT INTO GEO_UDF (RECORDID, LOC_RECORDID, SITE_APN, WATER_DIST, SANIT_DIST, COUNCIL_DIST, FIRE_DIST, SCHOOL_DIST, METRO_DIST,
										 HISTORIC_DIST, URBRENEW_AREA, CODE_INSP, TAZ_ID, CONSERV_AREA, ZONING, GEN_ZONING,
										 ZONE_DESC, UNITS, MAX_HEIGHT, ORD_RESTRICT, SUM_YRBLT, BLD1J_YRBLT, BLD2J_YRBLT, BLD3J_YRBLT, SUM_SQFT,
										 BLD1J_SQFT, BLD2J_SQFT, BLD3J_SQFT, CLASS_CODE1, CLASS_CODE2, CLASS_CODE3, CLASS_CODE4, SPCL_STBK_DEV, SUM_STATS, BLD1_STATS, BLD2_STATS, BLD3_STATS,
										 CODE_DESC1, CODE_DESC2, CODE_DESC3, CODE_DESC4, ORD_NUMBER, DBL_FRNT_LOT, ZONE_LINK, SUBDIVISIONLINK)
								 VALUES (@szRecordID, @szLocRecordID, @szPIN, @szWaterDist, @szSanitDist, @szCouncilDist, @szFireDist, 'JEFFCO PUBLICSCHOOLS', @szMetroDist,
										 @szHistDist, @szUrbanRenew, @szCodeInsp, @fTAZID, @szConservArea, @szZoning, @szGenZoning,
										 @szZoningDesc, @szUnits, @szMaxHeight, @szOrdRestrict, @fSTTYrBlt, @fSTTYrBlt2, @fSTTYrBlt3, @fSTTYrBlt4, @fSUM_SQFT,
										 @fSqFt2, @fSqFt3, @fSqFt4, @szClassCode1, @szClassCode2, @szClassCode3, @szClassCode4, @szSpecStbkDev, @szBLD1Stats, @szBLD2Stats, @szBLD3Stats, @szBLD4Stats,
										 @szCodeDesc1, @szCodeDesc2, @szCodeDesc3, @szCodeDesc4, @szOrdinanceNo, @szDblFrntLot, @szDevStndURL, @szSubDivLink)

					-- Insert Legal Description
					IF @szLegal IS NOT NULL
					BEGIN
						INSERT INTO GEO_LEGALS (LOC_RECORDID, SITE_APN, DESCRIPTION) VALUES (@szLocRecordID, @szPIN, @szLegal)

						IF @@error <> 0
							Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Comments)
										Values ('LANDTRAKINT', 24, 'ERROR', Getdate(), 'SITE_APN', @szPIN, 'Error adding Parcel record to Geo_Legals')
					END

					-- Insert Owner into Geo_People
					IF @szOwnerName IS NOT NULL
					BEGIN
						-- Reset counter for unique number string at end of RecordID
						SET @lCounter = @lCounter + 1
						-- Set unique RecordID
						SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
						 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
						 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
						 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)	 + right(('000000'+ ltrim(str(@lCounter))),6)

						INSERT INTO PEOPLE (RECORDID, ActivityID, ActivityTypeID, SITE_APN, NAMETYPE, NAME, OTHER_NAME, ADDRESS1, ADDRESS2, CITY, STATE, ZIP)
										VALUES (@szRecordID, @szLocRecordID, 5, @szPIN, 'OWNER', @szOwnerName, @szOwnerName2, @szMailAddr, @szOwnCO, @szMailCity, @szMailState, @szMailZip)

						IF @@error <> 0
							Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Keycol2, Keydata2, Comments)
										Values ('LANDTRAKINT', 23, 'ERROR', Getdate(), 'OWNER', @szOwnerName, 'SITE APN', @szPIN, 
												'Error adding Parcel record to People')
					END

					-- Insert Flood Zone Restrictions
					IF @szFloodZone IN ('AE', 'FW', 'A', 'AO')
					BEGIN
						-- Reset counter for unique number string at end of RecordID
						SET @lCounter = @lCounter + 1
						-- Set unique RecordID
						SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
						 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2) + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
						 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2) + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
						 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2) + right(('000000'+ ltrim(str(@lCounter))),6)

						INSERT INTO GEO_RESTRICTIONS2 (RECORDID, LOC_RECORDID, SITE_APN, RESTRICTION_TYPE, RESTRICTION_NOTES)
											   VALUES (@szRecordID, @szLocRecordID, @szPIN, 'WARNING', 'SEE ENGINEERING; PARCEL IN FLOODPLAIN')

						IF @@error <> 0
							Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Keycol2, Keydata2, Comments)
										Values ('LANDTRAKINT', 25, 'ERROR', Getdate(), 'RESTRICTION TYPE', 'WARNING: FLOODPLAIN', 'SITE_APN', @szPIN, 
												'Error adding Parcel record to Geo_Restrictions2') 
					END

					-- Insert Flood Zone Restrictions
					IF @szFloodZone = 'FW'
					BEGIN
						-- Reset counter for unique number string at end of RecordID
						SET @lCounter = @lCounter + 1
						-- Set unique RecordID
						SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
						 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
						 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
						 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)	 + right(('000000'+ ltrim(str(@lCounter))),6)

						INSERT INTO GEO_RESTRICTIONS2 (RECORDID, LOC_RECORDID, SITE_APN, RESTRICTION_TYPE, RESTRICTION_NOTES)
											   VALUES (@szRecordID, @szLocRecordID, @szPIN, 'WARNING', 'SEE ENGINEERING; PARCEL IN FLOODWAY')

						IF @@error <> 0
							Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Keycol2, Keydata2, Comments)
										Values ('LANDTRAKINT', 25, 'ERROR', Getdate(), 'RESTRICTION TYPE', 'WARNING: FLOODWAY', 'SITE_APN', @szPIN, 
												'Error adding Parcel record to Geo_Restrictions2') 
					END

					-- Insert Historic Restrictions
					IF @szHistDist = 'ARVADA DOWNTOWN'
					BEGIN
						-- Reset counter for unique number string at end of RecordID
						SET @lCounter = @lCounter + 1
						-- Set unique RecordID
						SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
						 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
						 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
						 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)	 + right(('000000'+ ltrim(str(@lCounter))),6)

						INSERT INTO GEO_RESTRICTIONS2 (RECORDID, LOC_RECORDID, SITE_APN, RESTRICTION_TYPE, RESTRICTION_NOTES)
											   VALUES (@szRecordID, @szLocRecordID, @szPIN, 'WARNING', 'PARCEL IN HISTORIC DISTRICT')

						IF @@error <> 0
							Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Keycol2, Keydata2, Comments)
										Values ('LANDTRAKINT', 25, 'ERROR', Getdate(), 'RESTRICTION TYPE', 'WARNING: HISTORIC DISTRICT', 'SITE_APN', @szPIN, 
												'Error adding Parcel record to Geo_Restrictions2') 
					END

					-- Insert Olde Town Area Restrictions
					IF @szConservArea = 'OLDE TOWN AREA'
					BEGIN
						-- Reset counter for unique number string at end of RecordID
						SET @lCounter = @lCounter + 1
						-- Set unique RecordID
						SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
						 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2) + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
						 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2) + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
						 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2) + right(('000000'+ ltrim(str(@lCounter))),6)

						INSERT INTO GEO_RESTRICTIONS2 (RECORDID, LOC_RECORDID, SITE_APN, RESTRICTION_TYPE, RESTRICTION_NOTES)
											   VALUES (@szRecordID, @szLocRecordID, @szPIN, 'WARNING', 'OLDE TOWN AREA')

						IF @@error <> 0
							Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Keycol2, Keydata2, Comments)
										Values ('LANDTRAKINT', 25, 'ERROR', Getdate(), 'RESTRICTION TYPE', 'WARNING: OLDE TOWN AREA', 'SITE_APN', @szPIN, 
												'Error adding Parcel record to Geo_Restrictions2')
				END

				END -- End Inserts if Geo_Ownership inserted without error
		END -- End Inserts for Parcel

  --------------------
  -- Variable Reset --
  --------------------
  SELECT @szLocRecordID = NULL, @szRecordID = NULL, @szSiteStreet = NULL, @fTotalVal = 0, @szBLD1Stats = NULL, @szBLD2Stats = NULL, @szBLD3Stats = NULL, @szBLD4Stats = NULL,
		 @szCodeDesc1 = NULL, @szCodeDesc2 = NULL, @szCodeDesc3 = NULL, @szCodeDesc4 = NULL, @szMailAddr = NULL, @szSiteAddr = NULL, @szOwnCo = NULL, @szLandUse01 = NULL,
		 @szLandUse03 = NULL, @szOldCensus = NULL, @szOldGenPlan = NULL, @fOldImpVal = 0, @fOldLandVal = 0, @fOldLotSize = 0, @lOldNoStories = NULL, @szOldNoUnits = NULL,
		 @szOldSecTwpRng = NULL, @szOldBlock = NULL, @szOldLot = NULL, @szOldSubdiv = NULL,	@fOldStructure = 0, @fOldYrBuilt = 0, @szOldZone2 = NULL, @szOldTRA = NULL,
		 @szOldSiteAddr = NULL, @szOldOwner = NULL, @szOldOwnAddr1 = NULL, @szOldOwnAddr2 = NULL, @szOldOwnCity = NULL, @szOldOwnState = NULL, @szOldOwnZip = NULL, @szOldOthName = NULL,
		 @szOldWaterDist = NULL, @szOldSanitDist = NULL, @szOldCouncilDist = NULL, @szOldFireDist = NULL, @szOldMetroDist = NULL, @szOldHistDist = NULL, @szOldUrbanRenew = NULL,
		 @szOldCodeInsp = NULL, @fOldTAZID = 0, @szOldConservArea = NULL, @szOldZoning = NULL, @szOldGenZoning = NULL, @szOldZoningDesc = NULL, @szOldUnits = NULL, @szOldMaxHeight = NULL,
		 @szOldOrdRestrict = NULL, @fOldBld1YrBlt = 0, @fOldBld2YrBlt = 0, @fOldBld3YrBlt = 0, @fOldBld4YrBlt = 0, @fOldBld1Sqft = 0, @fOldBld2Sqft = 0, @fOldBld3Sqft = 0, @fOldBld4Sqft = 0,
		 @szOldClassCode1 = NULL, @szOldClassCode2 = NULL, @szOldClassCode3 = NULL, @szOldClassCode4 = NULL, @szOldCodeDesc1 = NULL, @szOldCodeDesc2 = NULL, @szOldCodeDesc3 = NULL,
		 @szOldCodeDesc4 = NULL, @szOldSpecStbkDev = NULL, @szOldLand01 = NULL, @szOldLand03 = NULL, @szOldLand05 = NULL, @szOldBldg01 = NULL, @szOldBldg03 = NULL, @szParentRecID = NULL,
		 @szClassCode1 = NULL, @szClassCode2 = NULL, @szClassCode3 = NULL, @szClassCode4 = NULL, @fSTTYrBlt = 0, @fSqFt1 = 0, @fSTTYrBlt2 = 0, @fSqFt2 = 0, @fSTTYrBlt3 = 0, @fSqFt3 = 0,
		 @fSTTYrBlt4 = 0, @fSqFt4 = 0, @szOldOrdNo = NULL, @szOldDblFrntLot = NULL, @szOldLegalDesc = NULL, @szLegal = NULL, @fOldSumYrBlt = 0, @szOldZoneLink = NULL, @szOldPIN = NULL,
		 @szOldSubDivLink = NULL, @szOldDfltInspctr = NULL, @fOldSumSqFt = null, @szOldStreetName = null

  Fetch Next From cGetParcel Into @szPIN, @szBlockGrpID, @szCompPlanDesc, @fTotalImprv, @fTotalLandVal, @fNetSF, @fStories, @fNoUnits, @szMailStrNbr, @szMailStrDir, @szMailStrName,
								  @szMailStrType, @szMailStrSfx, @szMailStrUnit, @szOwnCo, @szMailCity, @szOwnerName, @szMailState, @szMailZip,
								  @szSection, @szBlock, @szCity, @szLot, @szStNum, @szStName, @szPreDir, @szStType, @szStSuffix, @szSubDivName,
								  @szUnitNum, @szZip, @fYrBlt, @szZoning, @szWaterDist, @szSanitDist, @szCouncilDist, @szFireDist,
								  @szMetroDist, @szHistDist, @szUrbanRenew, @szCodeInsp, @fTAZID, @szConservArea, @szGenZoning, @szZoningDesc, @szUnits,
								  @szMaxHeight, @szOrdRestrict, @szStTypeUse, @szStTypeUse2, @szStTypeUse3, @szStTypeUse4, @fSTTYrBlt, @fSTTYrBlt2, @fSTTYrBlt3, @fSTTYrBlt4, @fTotalSqFt, @fSqFt2,
								  @fSqFt3, @fSqFt4, @szClassCode1, @szClassCode2, @szClassCode3, @szClassCode4, @szFloodZone, @szLOMACaseNo, @lBuildings, @szStructType, @szOwnerName2,
								  @szOrdinanceNo, @szSpecStbkDev, @szDblFrntLot, @szDevStndURL, @szSubDivLink, @szInspector
  End --While
  Close cGetParcel
  Deallocate cGetParcel

  ------------------------------------------
  -- Process Multiple Parents for Parcels --
  ------------------------------------------
  DECLARE cGetMultiParents CURSOR LOCAL FOR        
  SELECT RTRIM(PIN), RTRIM(OLDPIN)
	FROM COA_HIST_MERGE
   
  Open cGetMultiParents 
  Fetch Next From cGetMultiParents Into @szPIN, @szOldPIN
  While @@Fetch_status <> -1 
  Begin

	-- Get ParentRecordID if link found
	SELECT @szParentRecID = RECORDID FROM GEO_OWNERSHIP WHERE SITE_APN = @szOldPIN AND GEOTYPE = 'PARCEL'

	-- Get RecordID if link found
	SELECT @szRecordID = RECORDID FROM GEO_OWNERSHIP WHERE SITE_APN = @szPIN AND GEOTYPE = 'PARCEL'

	-- Create records for multiple parents
	IF @szParentRecID IS NOT NULL AND @szRecordID IS NOT NULL
			INSERT INTO PRMRY_PARENTGEO(PARENTID, CHILDID)	 VALUES(@szParentRecID, @szRecordID)

  -- Reset variables
  SELECT @szRecordID = NULL, @szParentRecID = NULL
  
  Fetch Next From cGetMultiParents Into @szPIN, @szOldPIN
  End --While
  Close cGetMultiParents
  Deallocate cGetMultiParents

  --------------------------
  -- Process Address Data --
  --------------------------
  DECLARE cGetAddress CURSOR LOCAL FOR        
  SELECT RTRIM(APIN), ACRWID, BLOCKGROUP_ID, UPPER(COMPPLAN_DESCRIPTION), UPPER(JURISD_CITY), UPPER(ADDR_TYPE), UPPER(ADDR_SUBTYPE), UPPER(NAME), STREET_NUMBER, UPPER(STREET_NAME),
		 UPPER(STREET_PRE_DIR), UPPER(STREET_TYPE), UPPER(STREET_SUFFIX), UNIT_NUM, RTRIM(CAST(ZIP_5 AS VARCHAR) + '-' + ISNULL(CAST(ZIP_4 AS VARCHAR), ' ')), UPPER(ZONING), UPPER(WATER_DISTRICT), UPPER(SANITATION_DISTRICT),
		 UPPER(COUNCIL_DISTRICT), UPPER(FIRE_DISTRICT), UPPER(METRO_DISTRICT), UPPER(HISTORIC_DISTRICT), UPPER(URBAN_RENEWAL_AREA), UPPER(CODE_INSPECTOR), TAZ_ID, SUBDIVISION_NAME,
		 UPPER(OLDE_TOWN_AREA), UPPER(GEN_ZONING), UPPER(ZONING_DESCRIPTION), UNITS, MAXIMUM_HEIGHT, UPPER(ORD_RESTRICT), CORNER_ADDR, FLOOD_ZONE, LOMA_CASE_NO,
		 ORDINANCE_NO, DBLFRONTLOT, SPECIAL_SETBACKS_DEV, DEV_STANDARDS_URL, AADDRESS, SUBDIVISION_LINK, I.INSPECTOR
	FROM ADDRESSES A
	LEFT JOIN ADDRESS_INSPECTOR I ON A.ACRWID = I.CRW_ID
  Open cGetAddress 
  Fetch Next From cGetAddress Into @szPIN, @szCRWID, @szBlockGrpID, @szCompPlanDesc, @szCity, @szAddrType, @szAddrSubType, @szAddrName, @szStNum, @szStName,
								  @szPreDir, @szStType, @szStSuffix, @szUnitNum, @szZip, @szZoning, @szWaterDist, @szSanitDist,
								  @szCouncilDist, @szFireDist, @szMetroDist, @szHistDist, @szUrbanRenew, @szCodeInsp, @fTAZID, @szSubDivName,
								  @szConservArea, @szGenZoning, @szZoningDesc, @szUnits, @szMaxHeight, @szOrdRestrict, @szCornerAddr, @szFloodZone, @szLOMACaseNo,
								  @szOrdinanceNo, @szDblFrntLot, @szSpecStbkDev, @szDevStndURL, @szAddress, @szSubDivLink, @szInspector
  While @@Fetch_status <> -1 
  Begin
	IF @szPIN <> ''
	BEGIN
		-- Get Adams Parcel Info
		SELECT @fTotalImprv = TOTALIMPSV, @fTotalLandVal = TOTALLANDV, @fNetSF = NETSF, @fStories = STORIES, @fNoUnits = UNITS, @szMailAddr = UPPER(ADDRESS), @szMailCity = UPPER(CITY),
			   @szOwnerName = UPPER(OWNERNAME1), @szOwnerName2 = UPPER(OWNERNAME2), @szMailState = UPPER(STATE), @szMailZip = ZIPCODE, @szSection = UPPER(SECTION_), @szBlock = UPPER(LGL_BLOCK),
			   @szLot = UPPER(LOT_1), @szSchoolDist = 'ADAMS DISTRICT 50'
		 FROM ADDR_ADAMS WHERE PPIN = @szPIN

		-- Get Jefferson Parcel Info
		SELECT @fTotalImprv = TOTACTIMPV, @fTotalLandVal = TOTACTLNDV, @fNetSF = TOTACR * 43560, @fStories = STTNBRFLR, @fNoUnits = STTNBRUNT, @szMailStrNbr = MAILSTRNBR, @szMailStrDir = MAILSTRDIR,
			   @szMailStrName = MAILSTRNAM, @szMailStrType = MAILSTRTYP, @szMailStrSfx = MAILSTRSFX, @szMailStrUnit = MAILSTRUNT, @szMailCity = UPPER(MAILCTYNAM), @szOwnCo = UPPER(OWNICO),
			   @szOwnerName = UPPER(OWNNAM), @szOwnerName2 = UPPER(OWNNAM2), @szMailState = UPPER(MAILSTENAM), @szMailZip = RTRIM(CAST(MAILZIP5 AS VARCHAR) + ISNULL(CAST(MAILZIP4 AS VARCHAR), ' ')),
			   @szSection =  CAST(CAST(LGLSEC AS INT) AS VARCHAR) + LGLQTR + CAST(CAST(LGLTWN AS INT) AS VARCHAR) + CAST(CAST(LGLRNG AS INT) AS VARCHAR), @szBlock = UPPER(LGLBLK), @szLot = UPPER(LGLLOT),
			   @szSchoolDist = 'JEFFCO PUBLICSCHOOLS'
		 FROM JEFFCO_PARCELS WHERE PPIN = @szPIN

		-- Set SecTwpRng for ADAMS county
		IF @szSchoolDist = 'ADAMS DISTRICT 50'
		BEGIN
			IF RIGHT(@szSection, 1) = 1
				SET @szSec = 'NE'
			ELSE IF RIGHT(@szSection, 1) = 2
				SET @szSec = 'NW'	
			ELSE IF RIGHT(@szSection, 1) = 3
				SET @szSec = 'SW'
			ELSE IF RIGHT(@szSection, 1) = 4
				SET @szSec = 'SE'
				
			SET @szSecTwpRng = LEFT(@szSection, 2) + @szSec + '3S 68W'
		END

		-- Set SiteAddr
		--SET @szSiteAddr = SUBSTRING(RTRIM(RTRIM(RTRIM(RTRIM(LTRIM(ISNULL(@szStNum, ' ') + ' ' + ISNULL(@szPreDir, ' '))) + ' ' + @szStName + ' ' + ISNULL(@szStType, ' ')) + ' ' + ISNULL(@szStSuffix, ' ')) + ' ' + ISNULL(@szUnitNum, ' ')), 1, 70)
		SET @szSiteAddr = SUBSTRING(RTRIM(LTRIM(ISNULL(@szAddress, ' ') + ' ' + ISNULL(@szUnitNum, ' '))), 1, 70)

		-- Set StreetName
		SET @szSiteStreet = SUBSTRING(RTRIM(RTRIM(LTRIM(ISNULL(@szPreDir, ' ') + ' ' + @szStName) + ' ' + ISNULL(@szStType, ' ')) + ' ' + ISNULL(@szStSuffix, ' ')), 1, 60)

		-- Set SiteDesc
		SET @szSiteDesc = SUBSTRING('TYPE: ' + ISNULL(@szAddrType, ' ') + ' SUB-TYPE: ' + ISNULL(@szAddrSubType, ' ') + ' NAME: ' + ISNULL(@szAddrName, ' '), 1, 2000)
		
		-- Set Total Value
		SET @fTotalVal = @fTotalImprv + @fTotalLandVal

		IF @szSchoolDist NOT LIKE 'ADAMS%'
		BEGIN
			-- Set MailAddr
			SET @szMailAddr = SUBSTRING(RTRIM(RTRIM(RTRIM(RTRIM(LTRIM(ISNULL(@szMailStrNbr, ' ') + ' ' + ISNULL(@szMailStrDir, ' '))) + ' ' + @szMailStrName + ' ' + ISNULL(@szMailStrType, ' ')) + ' ' + ISNULL(@szMailStrSfx, ' ')) + ' ' + ISNULL(@szMailStrUnit, ' ')), 1, 60)

			-- Set CO Owner Addr2
			IF @szOwnCo IS NOT NULL AND @szOwnCo <> ' ' AND @szOwnCo <> ''
				SET @szOwnCo = 'C/O ' + @szOwnCo
		END

		-- Set LandUse fields
		IF @szFloodZone IN ('AE', 'FW', 'A', 'AO') SET @szLandUse01 = 'YES'
		ELSE SET @szLandUse01 = NULL
			
		IF @szFloodZone = 'FW'	SET @szLandUse03 = 'YES'
		ELSE	SET @szLandUse03 = NULL

		-- Get RecordID if link found
		SELECT @szLocRecordID = RECORDID FROM GEO_OWNERSHIP WHERE SITE_ALTERNATE_ID = @szCRWID AND GEOTYPE = 'ADDRESS'

		-- Get ParentRecordID if link found
		SELECT @szParentRecID = RECORDID FROM GEO_OWNERSHIP WHERE SITE_APN = @szPIN AND GEOTYPE = 'PARCEL'

		-- If a link is found, update record
		IF @szLocRecordID IS NOT NULL
		BEGIN
			-- Get old info to compare and see if update is necessary
			SELECT @szOldCensus = CENSUS, @szOldGenPlan = GENERAL_PLAN, @fOldImpVal = IMPROVED_VALUE, @fOldLandVal = LAND_VALUE, @fOldLotSize = LOT_SIZE_SF, @szOldSiteAddr = SITE_ADDR,
					@lOldNoStories = NO_STORIES, @szOldNoUnits = NO_UNITS, @szOldSecTwpRng = SECTION_TWP_RNG, @szOldBlock = SITE_BLOCK, @szOldLot = SITE_LOT_NO, @szOldSubdiv = SITE_SUBDIVISION,
					@fOldStructure = STRUCTURE_SF, @fOldYrBuilt = YEAR_BUILT, @szOldZone2 = ZONE_CODE2, @szOldTRA = TRA, @szOldParentID = PARENT_RECORDID, @szOldParent = PARENT_SITE_APN,
					@szOldOwner = OWNER_NAME, @szOldOwnAddr1 = OWNER_ADDR1, @szOldOwnAddr2 = OWNER_ADDR2, @szOldOwnCity = OWNER_CITY, @szOldOwnState = OWNER_STATE, @szOldOwnZip = OWNER_ZIP,
					@szOldNotes = SITE_DESCRIPTION, @szOldSubdiv = SITE_SUBDIVISION, @szOldStreetName = site_streetname
				FROM GEO_OWNERSHIP WHERE RECORDID = @szLocRecordID
			-- If anything has been updated, update record
			IF ISNULL(@szOldCensus, ' ') <> SUBSTRING(ISNULL(@szBlockGrpID, ' '), 1, 10) OR ISNULL(@szOldGenPlan, ' ') <> SUBSTRING(ISNULL(@szCompPlanDesc, ' '), 1, 10) OR
			   ISNULL(@fOldImpVal, 0) <> ISNULL(@fTotalImprv, 0) OR ISNULL(@fOldLandVal, 0) <> ISNULL(@fTotalLandVal, 0) OR ISNULL(@fOldLotSize, 0) <> ISNULL(@fNetSF, 0) OR
			   ISNULL(@lOldNoStories, 0) <> ISNULL(@fStories, 0) OR ISNULL(@szOldNoUnits, 0) <> ISNULL(@fNoUnits, 0) OR ISNULL(@szOldSecTwpRng, ' ') <> SUBSTRING(ISNULL(@szSecTwpRng, ' '), 1, 20) OR
			   ISNULL(@szOldBlock, ' ') <> SUBSTRING(ISNULL(@szBlock, ' '), 1, 10) OR ISNULL(@szOldLot, ' ') <> SUBSTRING(ISNULL(@szLot, ' '), 1, 10) OR ISNULL(@szOldSubdiv, ' ') <> ISNULL(@szSubDivName, ' ') OR
			   ISNULL(@fOldStructure, 0) <> ISNULL(@fTotalSqFt, 0) OR ISNULL(@fOldYrBuilt, 0) <> ISNULL(@fYrBlt, 0) OR ISNULL(@szOldZone2, ' ') <> SUBSTRING(ISNULL(@szZoning, ' '), 1, 10) OR
			   ISNULL(@szOldTRA, ' ') <> ISNULL(@szATD, ' ') OR ISNULL(@szOldOwner, ' ') <> ISNULL(@szOwnerName, ' ') OR ISNULL(@szOldOwnAddr1, ' ') <> ISNULL(@szMailAddr, ' ') OR
			   ISNULL(@szOldOwnAddr2, ' ') <> ISNULL( @szOwnCo, ' ') OR ISNULL(@szOldOwnCity, ' ') <> ISNULL(@szMailCity, ' ') OR
			   ISNULL(@szOldOwnState, ' ') <> ISNULL(@szMailState, ' ') OR ISNULL(@szOldOwnZip, ' ') <> ISNULL(@szMailZip, ' ') OR ISNULL(@szOldSiteAddr, ' ') <> ISNULL(@szSiteAddr, ' ') OR
			   ISNULL(@szOldParentID, ' ') <> ISNULL(@szParentRecID, ' ') OR ISNULL(@szOldParent, ' ') <> ISNULL(@szPIN, ' ') OR ISNULL(@szOldNotes, ' ') <> ISNULL(@szSiteDesc, ' ') OR
			   ISNULL(@szOldSubdiv, ' ') <> ISNULL(@szSubDivName, ' ') OR isnull(@szOldStreetName, ' ') != isnull(@szSiteStreet, ' ')
			BEGIN
				UPDATE GEO_OWNERSHIP SET CENSUS = SUBSTRING(@szBlockGrpID, 1, 10), GENERAL_PLAN = SUBSTRING(@szCompPlanDesc, 1, 10), TRA = @szATD,
										 IMPROVED_VALUE = @fTotalImprv, LAND_VALUE = @fTotalLandVal, TOTAL_VALUE = @fTotalVal, LOT_SIZE_SF = @fNetSF, NO_STORIES = @fStories,
										 NO_UNITS = @fNoUnits, SECTION_TWP_RNG = SUBSTRING(@szSection, 1, 20), SITE_BLOCK = SUBSTRING(@szBlock, 1, 10), SITE_LOT_NO = SUBSTRING(@szLot, 1, 10),
										 SITE_SUBDIVISION = @szSubDivName, STRUCTURE_SF = @fTotalSqFt, YEAR_BUILT = @fYrBlt, ZONE_CODE2 = SUBSTRING(@szZoning, 1, 10),
										 OWNER_NAME = @szOwnerName, OWNER_ADDR1 = @szMailAddr, OWNER_ADDR2 = @szOwnCo, OWNER_CITY = @szMailCity, OWNER_STATE = @szMailState, OWNER_ZIP = @szMailZip, SITE_ADDR = @szSiteAddr,
										 SITE_NUMBER = @szStNum, SITE_ST_NAME = SUBSTRING(@szStName, 1, 30), SITE_STREETNAME = @szSiteStreet, SITE_ST_PREFIX = @szPreDir,
										 SITE_ST_TYPE = SUBSTRING(@szStSuffix, 1, 10), SITE_UNIT_NO = @szUnitNum, SITE_CITY = SUBSTRING(@szCity, 1, 30), SITE_ZIP = @szZip,
										 PARENT_RECORDID = @szParentRecID, PARENT_SITE_APN = @szPIN, SITE_DESCRIPTION = @szSiteDesc, SITE_APN = @szPIN
						WHERE RECORDID = @szLocRecordID

					IF @@error <> 0
					   Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Comments)
									Values ('LANDTRAKINT', 20, 'ERROR', Getdate(), 'SITE APN', @szPIN, 'Error updating Address record in Geo_Ownership')

				-- if ownername has changed, insert change log record
				IF ISNULL(@szOldOwner, ' ') <> ISNULL(@szOwnerName, ' ')
				BEGIN
					-- Set unique RecordID
					SET @szRecordID = NULL
					-- Reset counter for unique number string at end of RecordID
					SET @lCounter = @lCounter + 1
					SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
					 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
					 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
					 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)	 + right(('000000'+ ltrim(str(@lCounter))),6)
					 
					INSERT INTO GEO_CHANGE_LOG(ENTRY_DATE, FIELD_CHANGED, LOC_RECORDID, NEW_VALUE, OLD_VALUE, RECORDID)
										VALUES(GetDate(), 'OWNER_NAME', @szLocRecordID, @szOwnerName, @szOldOwner, @szRecordID)
				END
				
				-- if site address has changed, insert change log record
				IF ISNULL(@szOldSiteAddr, ' ') <> ISNULL(@szSiteAddr, ' ')
				BEGIN
					-- Set unique RecordID
					SET @szRecordID = NULL
					-- Reset counter for unique number string at end of RecordID
					SET @lCounter = @lCounter + 1
					SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
					 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
					 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
					 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)	 + right(('000000'+ ltrim(str(@lCounter))),6)
					 
					INSERT INTO GEO_CHANGE_LOG(ENTRY_DATE, FIELD_CHANGED, LOC_RECORDID, NEW_VALUE, OLD_VALUE, RECORDID)
										VALUES(GetDate(), 'SITE_ADDR', @szLocRecordID, @szSiteAddr, @szOldSiteAddr, @szRecordID)
				END

				-- if zonecode2 has changed, insert change log record
				IF ISNULL(@szOldZone2, ' ') <> SUBSTRING(ISNULL(@szZoning, ' '), 1, 10)
				BEGIN
					-- Set unique RecordID
					SET @szRecordID = NULL
					-- Reset counter for unique number string at end of RecordID
					SET @lCounter = @lCounter + 1
					SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
					 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
					 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
					 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)	 + right(('000000'+ ltrim(str(@lCounter))),6)
					 
					INSERT INTO GEO_CHANGE_LOG(ENTRY_DATE, FIELD_CHANGED, LOC_RECORDID, NEW_VALUE, OLD_VALUE, RECORDID)
										VALUES(GetDate(), 'ZONE_CODE2', @szLocRecordID, SUBSTRING(@szZoning, 1, 10), @szOldZone2, @szRecordID)
				END

			END

			-- Get old info to compare and see if update is necessary
			SELECT @szOldOwner = NAME, @szOldOthName = OTHER_NAME, @szOldOwnAddr1 = ADDRESS1, @szOldOwnAddr2 = ADDRESS2, @szOldOwnCity = CITY, @szOldOwnState = STATE,
					@szOldOwnZip = ZIP FROM PEOPLE WHERE ActivityID = @szLocRecordID AND NAMETYPE = 'OWNER'
			-- If owner name is different, update nametype to 'PREV OWNER' and insert new owner
			IF ISNULL(@szOldOwner, ' ') <> ISNULL(@szOwnerName, ' ')
			BEGIN
				UPDATE PEOPLE SET NAMETYPE = 'PREV OWNER' WHERE ActivityID = @szLocRecordID AND NAMETYPE = 'OWNER'
				
				-- Reset counter for unique number string at end of RecordID
				SET @lCounter = @lCounter + 1
				-- Set unique RecordID
				SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
				 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2) + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
				 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2) + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
				 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2) + right(('000000'+ ltrim(str(@lCounter))),6)

				INSERT INTO PEOPLE (RECORDID, ActivityID, ActivityTypeID, SITE_APN, NAMETYPE, NAME, OTHER_NAME, ADDRESS1, ADDRESS2, CITY, STATE, ZIP)
								VALUES (@szRecordID, @szLocRecordID, 5, @szPIN, 'OWNER', @szOwnerName, @szOwnerName2, @szMailAddr, @szOwnCo, @szMailCity, @szMailState, @szMailZip)

				IF @@error <> 0
					Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Keycol2, Keydata2, Comments)
								Values ('LANDTRAKINT', 23, 'ERROR', Getdate(), 'OWNER', @szOwnerName, 'SITE APN', @szPIN, 'Error adding Address record to People')
			END
			-- If anything except ownername has been updated, update record
			ELSE IF ISNULL(@szOldOwner, ' ') = ISNULL(@szOwnerName, ' ') AND (ISNULL(@szOldOthName, ' ') <> ISNULL(@szOwnerName2, ' ') OR
			   ISNULL(@szOldOwnAddr1, ' ') <> ISNULL(@szMailAddr, ' ') OR ISNULL(@szOldOwnAddr2, ' ') <> ISNULL(@szOwnCo, ' ') OR
			   ISNULL(@szOldOwnCity, ' ') <> ISNULL(@szMailCity, ' ') OR ISNULL(@szOldOwnState, ' ') <> ISNULL(@szMailState, ' ') OR ISNULL(@szOldOwnZip, ' ') <> ISNULL(@szMailZip, ' '))
			BEGIN
				UPDATE PEOPLE SET NAME = @szOwnerName, OTHER_NAME = @szOwnerName2, ADDRESS1 = @szMailAddr, ADDRESS2 = @szOwnCo, CITY = @szMailCity, STATE = @szMailState, ZIP = @szMailZip
						WHERE ActivityID = @szLocRecordID AND NAMETYPE = 'OWNER'
				IF @@error <> 0
					Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Keycol2, Keydata2, Comments)
								Values ('LANDTRAKINT', 24, 'ERROR', Getdate(), 'OWNER', @szOwnerName, 'SITE APN', @szPIN, 'Error updating Address record in People')
			END

			-- Get old info to compare and see if update is necessary
			SELECT @szOldLand01 = LANDUSE01, @szOldLand03 = LANDUSE03, @szOldLand05 = LANDUSE05, @szOldDfltInspctr = DFLT_INSPCTR_PERMIT FROM GEO_OWNERSHIPUDF WHERE LOC_RECORDID = @szLocRecordID
			-- If anything has been updated, update record
			IF ISNULL(@szOldLand01, ' ') <> ISNULL(@szLandUse01, ' ') OR ISNULL(@szOldLand03, ' ') <> ISNULL(@szLandUse03, ' ') OR
			   ISNULL(@szOldLand05, ' ') <> ISNULL(@szLOMACaseNo, ' ') OR ISNULL(@szOldDfltInspctr, ' ') <> ISNULL(@szInspector, ' ')
			BEGIN
				UPDATE GEO_OWNERSHIPUDF SET LANDUSE01 = @szLandUse01, LANDUSE03 = @szLandUse03, LANDUSE05 = @szLOMACaseNo, DFLT_INSPCTR_PERMIT = @szInspector
						WHERE LOC_RECORDID = @szLocRecordID

				IF @@error <> 0
					Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Comments)
								Values ('LANDTRAKINT', 20, 'ERROR', Getdate(), 'SITE APN', @szPIN, 'Error updating Address record in Geo_OwnershipUDF')

				-- if landuse01 has changed, insert change log record
				IF ISNULL(@szOldLand01, ' ') <> ISNULL(@szLandUse01, ' ')
				BEGIN
					-- Set unique RecordID
					SET @szRecordID = NULL
					-- Reset counter for unique number string at end of RecordID
					SET @lCounter = @lCounter + 1
					SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
					 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2) + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
					 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2) + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
					 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2) + right(('000000'+ ltrim(str(@lCounter))),6)
					 
					INSERT INTO GEO_CHANGE_LOG(ENTRY_DATE, FIELD_CHANGED, LOC_RECORDID, NEW_VALUE, OLD_VALUE, RECORDID)
										VALUES(GetDate(), 'LANDUSE01', @szLocRecordID, @szLandUse01, @szOldLand01, @szRecordID)
				END
				
			END

			-- Get old info to compare and see if update is necessary
			SELECT @szOldWaterDist = WATER_DIST, @szOldSanitDist = SANIT_DIST, @szOldCouncilDist = COUNCIL_DIST, @szOldFireDist = FIRE_DIST, @szOldMetroDist = METRO_DIST,
					@szOldHistDist = HISTORIC_DIST, @szOldUrbanRenew = URBRENEW_AREA, @szOldCodeInsp = CODE_INSP, @fOldTAZID = TAZ_ID, @szOldConservArea = CONSERV_AREA,
					@szOldZoning = ZONING, @szOldGenZoning = GEN_ZONING, @szOldZoningDesc = ZONE_DESC, @szOldUnits = UNITS, @szOldMaxHeight = MAX_HEIGHT, @szOldZoneLink = ZONE_LINK,
					@szOldOrdRestrict = ORD_RESTRICT, @szOldCornerLot = CORNER_LOT, @szOldDblFrntLot = DBL_FRNT_LOT, @szOldOrdNo = ORD_NUMBER, @szOldSpecStbkDev = SPCL_STBK_DEV,
					@szOldSubDivLink = SUBDIVISIONLINK
				FROM GEO_UDF WHERE LOC_RECORDID = @szLocRecordID
			-- If anything has been updated, update record
			IF ISNULL(@szOldWaterDist, ' ') <> ISNULL(@szWaterDist, ' ') OR ISNULL(@szOldSanitDist, ' ') <> ISNULL(@szSanitDist, ' ') OR
			   ISNULL(@szOldCouncilDist, ' ') <> ISNULL(@szCouncilDist, ' ') OR ISNULL(@szOldFireDist, ' ') <> ISNULL(@szFireDist, ' ') OR
			   ISNULL(@szOldMetroDist, ' ') <> ISNULL(@szMetroDist, ' ') OR ISNULL(@szOldHistDist, ' ') <> ISNULL(@szHistDist, ' ') OR
			   ISNULL(@szOldUrbanRenew, ' ') <> ISNULL(@szUrbanRenew, ' ') OR ISNULL(@szOldCodeInsp, ' ') <> ISNULL(@szCodeInsp, ' ') OR
			   ISNULL(@fOldTAZID, 0) <> ISNULL(@fTAZID, 0) OR ISNULL(@szOldConservArea, ' ') <> ISNULL(@szConservArea, ' ') OR ISNULL(@szOldZoning, ' ') <> ISNULL(@szZoning, ' ') OR
			   ISNULL(@szOldGenZoning, ' ') <> ISNULL(@szGenZoning, ' ') OR ISNULL(@szOldZoningDesc, ' ') <> ISNULL(@szZoningDesc, ' ') OR
			   ISNULL(@szOldUnits, ' ') <> ISNULL(@szUnits, ' ') OR ISNULL(@szOldMaxHeight, ' ') <> ISNULL(@szMaxHeight, ' ') OR ISNULL(@szOldOrdRestrict, ' ') <> ISNULL(@szOrdRestrict, ' ') OR
			   ISNULL(@szOldSpecStbkDev, ' ') <> ISNULL(@szSpecStbkDev, ' ') OR ISNULL(@szOldCornerLot, ' ') <> ISNULL(@szCornerAddr, ' ') OR ISNULL(@szOldDblFrntLot, ' ') <> ISNULL(@szDblFrntLot, ' ') OR
			   ISNULL(@szOldOrdNo, ' ') <> ISNULL(@szOrdinanceNo, ' ') OR ISNULL(@szOldZoneLink, ' ') <> ISNULL(@szDevStndURL, ' ') OR
			   ISNULL(@szOldSubDivLink, ' ') <> ISNULL(@szSubDivLink, ' ')
			BEGIN
				UPDATE GEO_UDF SET WATER_DIST = @szWaterDist, SANIT_DIST = @szSanitDist, COUNCIL_DIST = @szCouncilDist, FIRE_DIST = @szFireDist, METRO_DIST = @szMetroDist,
								   HISTORIC_DIST = @szHistDist, URBRENEW_AREA = @szUrbanRenew, CODE_INSP = @szCodeInsp, TAZ_ID = @fTAZID, CONSERV_AREA = @szConservArea,
								   ZONING = @szZoning, GEN_ZONING = @szGenZoning, ZONE_DESC = @szZoningDesc, UNITS = @szUnits, MAX_HEIGHT = @szMaxHeight, ZONE_LINK = @szDevStndURL,
								   ORD_RESTRICT = @szOrdRestrict, CORNER_LOT = @szCornerAddr, DBL_FRNT_LOT = @szDblFrntLot, ORD_NUMBER = @szOrdinanceNo, SPCL_STBK_DEV = @szSpecStbkDev,
								   SUBDIVISIONLINK = @szSubDivLink
						WHERE LOC_RECORDID = @szLocRecordID

				IF @@error <> 0
					Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Comments)
								Values ('LANDTRAKINT', 20, 'ERROR', Getdate(), 'SITE APN', @szPIN, 'Error updating Address record in Geo_UDF')
			END

			-- If current restrictions - drop all and reinsert any that exist in source
			IF (SELECT COUNT(*) FROM GEO_RESTRICTIONS2 WHERE LOC_RECORDID = @szLocRecordID) > 0
				DELETE FROM GEO_RESTRICTIONS2 WHERE LOC_RECORDID = @szLocRecordID AND SUBSTRING(RECORDID,1,4) = 'CONV'

			-- Insert Flood Zone Restrictions
			IF @szFloodZone IN ('AE', 'FW', 'A', 'AO')
			BEGIN
				-- Reset counter for unique number string at end of RecordID
				SET @lCounter = @lCounter + 1
				-- Set unique RecordID
				SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
				 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)+ right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
				 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)+ right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
				 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)+ right(('000000'+ ltrim(str(@lCounter))),6)

				INSERT INTO GEO_RESTRICTIONS2 (RECORDID, LOC_RECORDID, SITE_APN, RESTRICTION_TYPE, RESTRICTION_NOTES)
									   VALUES (@szRecordID, @szLocRecordID, @szPIN, 'WARNING', 'SEE ENGINEERING; PARCEL IN FLOODPLAIN')

				IF @@error <> 0
					Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Keycol2, Keydata2, Comments)
								Values ('LANDTRAKINT', 25, 'ERROR', Getdate(), 'RESTRICTION TYPE', 'WARNING: FLOODPLAIN', 'SITE_APN', @szPIN, 
										'Error adding Address record to Geo_Restrictions2') 
			END

			-- Insert Flood Zone Restrictions
			IF @szFloodZone = 'FW'
			BEGIN
				-- Reset counter for unique number string at end of RecordID
				SET @lCounter = @lCounter + 1
				-- Set unique RecordID
				SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
				 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2) + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
				 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2) + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
				 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2) + right(('000000'+ ltrim(str(@lCounter))),6)

				INSERT INTO GEO_RESTRICTIONS2 (RECORDID, LOC_RECORDID, SITE_APN, RESTRICTION_TYPE, RESTRICTION_NOTES)
									   VALUES (@szRecordID, @szLocRecordID, @szPIN, 'WARNING', 'SEE ENGINEERING; PARCEL IN FLOODWAY')

				IF @@error <> 0
					Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Keycol2, Keydata2, Comments)
								Values ('LANDTRAKINT', 25, 'ERROR', Getdate(), 'RESTRICTION TYPE', 'WARNING: FLOODWAY', 'SITE_APN', @szPIN, 
										'Error adding Address record to Geo_Restrictions2') 
			END

			-- Insert Historic Restrictions
			IF @szHistDist = 'ARVADA DOWNTOWN'
			BEGIN
				-- Reset counter for unique number string at end of RecordID
				SET @lCounter = @lCounter + 1
				-- Set unique RecordID
				SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
				 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2) + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
				 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2) + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
				 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2) + right(('000000'+ ltrim(str(@lCounter))),6)

				INSERT INTO GEO_RESTRICTIONS2 (RECORDID, LOC_RECORDID, SITE_APN, RESTRICTION_TYPE, RESTRICTION_NOTES)
									   VALUES (@szRecordID, @szLocRecordID, @szPIN, 'WARNING', 'PARCEL IN HISTORIC DISTRICT')

				IF @@error <> 0
					Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Keycol2, Keydata2, Comments)
								Values ('LANDTRAKINT', 25, 'ERROR', Getdate(), 'RESTRICTION TYPE', 'WARNING: HISTORIC DISTRICT', 'SITE_APN', @szPIN, 
										'Error adding Address record to Geo_Restrictions2') 
			END

			-- Insert Olde Town Area Restrictions
			IF @szConservArea = 'OLDE TOWN AREA'
			BEGIN
				-- Reset counter for unique number string at end of RecordID
				SET @lCounter = @lCounter + 1
				-- Set unique RecordID
				SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
				 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
				 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
				 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)	 + right(('000000'+ ltrim(str(@lCounter))),6)

				INSERT INTO GEO_RESTRICTIONS2 (RECORDID, LOC_RECORDID, SITE_APN, RESTRICTION_TYPE, RESTRICTION_NOTES)
									   VALUES (@szRecordID, @szLocRecordID, @szPIN, 'WARNING', 'OLDE TOWN AREA')

				IF @@error <> 0
					Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Keycol2, Keydata2, Comments)
								Values ('LANDTRAKINT', 25, 'ERROR', Getdate(), 'RESTRICTION TYPE', 'WARNING: OLDE TOWN AREA', 'SITE_APN', @szPIN, 
										'Error adding Address record to Geo_Restrictions2')
			END
		END
		ELSE -- If a link is not found, insert record
		BEGIN
			-- Set unique RecordID
			SET @szLocRecordID = NULL
			-- Reset counter for unique number string at end of RecordID
			SET @lCounter = @lCounter + 1
			SET @szLocRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
			 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
			 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
			 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)	 + right(('000000'+ ltrim(str(@lCounter))),6)

			INSERT INTO [dbo].[Activity] ([ActivityID], [ActivityTypeID]) VALUES(@szLocRecordID, 5 )

			INSERT INTO GEO_OWNERSHIP (ActivityTypeID, RECORDID, SITE_APN, SITE_ALTERNATE_ID, CENSUS, GENERAL_PLAN, IMPROVED_VALUE, LAND_VALUE,
									   TOTAL_VALUE, LOT_SIZE_SF, NO_STORIES, NO_UNITS, SCHOOL_DIST, SECTION_TWP_RNG, SITE_BLOCK,
									   SITE_LOT_NO, ZONE_CODE2, SITE_DESCRIPTION, ZONE_CODE1, SITE_SUBDIVISION,
									   OWNER_NAME, OWNER_ADDR1, OWNER_ADDR2, OWNER_CITY, OWNER_STATE, OWNER_ZIP,
									   SITE_ADDR, SITE_NUMBER, SITE_ST_NAME, SITE_STREETNAME,
									   SITE_ST_PREFIX, SITE_ST_TYPE, SITE_UNIT_NO,
									   SITE_CITY, SITE_STATE, SITE_ZIP, STATUS, GEOTYPE, MULTI_ADDRESS, PARENT_RECORDID, PARENT_SITE_APN)
							   VALUES (5, @szLocRecordID, @szPIN, @szCRWID, SUBSTRING(@szBlockGrpID, 1, 10), SUBSTRING(@szCompPlanDesc, 1, 10), @fTotalImprv, @fTotalLandVal,
									   @fTotalVal, @fNetSF, @fStories, @fNoUnits, @szSchoolDist, SUBSTRING(@szSection, 1, 20), SUBSTRING(@szBlock, 1, 10),
									   SUBSTRING(@szLot, 1, 10), SUBSTRING(@szZoning, 1, 10), @szSiteDesc, 'SEE CUSTOM ZONING SCREEN', @szSubDivName,
									   @szOwnerName, @szMailAddr, @szOwnCo, @szMailCity, @szMailState, @szMailZip,
									   @szSiteAddr, @szStNum, SUBSTRING(@szStName, 1, 30), @szSiteStreet,
									   @szPreDir, SUBSTRING(@szStSuffix, 1, 10), @szUnitNum,
									   SUBSTRING(@szCity, 1, 30), 'CO', @szZip, 'ACTIVE', 'ADDRESS', 0, @szParentRecID, @szPIN)

				IF @@error <> 0
				   Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Comments)
								Values ('LANDTRAKINT', 21, 'ERROR', Getdate(), 'SITE APN', @szPIN, 'Error adding Address record to Geo_Ownership')
				ELSE
				BEGIN
				   -- Log new record for reporting purposes
				   Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Comments)
								Values ('LANDTRAKINT', 30, 'LOG', Getdate(), 'SITE APN', @szPIN, 'New Address record added to Geo_Ownership')


					INSERT INTO GEO_OWNERSHIPUDF (LOC_RECORDID, SITE_APN, LANDUSE01, LANDUSE03, LANDUSE05, DFLT_INSPCTR_PERMIT)
										  VALUES (@szLocRecordID, @szPIN, @szLandUse01, @szLandUse03, @szLOMACaseNo, @szInspector)

					-- Reset counter for unique number string at end of RecordID
					SET @lCounter = @lCounter + 1
					-- Set unique RecordID
					SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
					 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
					 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
					 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)	 + right(('000000'+ ltrim(str(@lCounter))),6)

					INSERT INTO GEO_UDF (RECORDID, LOC_RECORDID, SITE_APN, WATER_DIST, SANIT_DIST, COUNCIL_DIST, FIRE_DIST, SCHOOL_DIST, METRO_DIST,
										 HISTORIC_DIST, URBRENEW_AREA, CODE_INSP, TAZ_ID, CONSERV_AREA, ZONING, GEN_ZONING, SUBDIVISIONLINK,
										 ZONE_DESC, UNITS, MAX_HEIGHT, ORD_RESTRICT, CORNER_LOT, DBL_FRNT_LOT, ORD_NUMBER, SPCL_STBK_DEV, ZONE_LINK)
								 VALUES (@szRecordID, @szLocRecordID, @szPIN, @szWaterDist, @szSanitDist, @szCouncilDist, @szFireDist, @szSchoolDist, @szMetroDist,
										 @szHistDist, @szUrbanRenew, @szCodeInsp, @fTAZID, @szConservArea, @szZoning, @szGenZoning, @szSubDivLink,
										 @szZoningDesc, @szUnits, @szMaxHeight, @szOrdRestrict, @szCornerAddr, @szDblFrntLot, @szOrdinanceNo, @szSpecStbkDev, @szDevStndURL)

					-- Insert Owner into Geo_People
					IF @szOwnerName IS NOT NULL
					BEGIN
						-- Reset counter for unique number string at end of RecordID
						SET @lCounter = @lCounter + 1
						-- Set unique RecordID
						SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
						 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
						 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
						 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)	 + right(('000000'+ ltrim(str(@lCounter))),6)

						INSERT INTO PEOPLE (RECORDID, ActivityID, ActivityTypeID, SITE_APN, NAMETYPE, NAME, OTHER_NAME, ADDRESS1, ADDRESS2, CITY, STATE, ZIP)
										VALUES (@szRecordID, @szLocRecordID, 5, @szPIN, 'OWNER', @szOwnerName, @szOwnerName2, @szMailAddr, @szOwnCO, @szMailCity, @szMailState, @szMailZip)

						IF @@error <> 0
							Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Keycol2, Keydata2, Comments)
										Values ('LANDTRAKINT', 23, 'ERROR', Getdate(), 'OWNER', @szOwnerName, 'SITE APN', @szPIN, 'Error adding Address record to People')
					END

					-- Insert Flood Zone Restrictions
					IF @szFloodZone IN ('AE', 'FW', 'A', 'AO')
					BEGIN
						-- Reset counter for unique number string at end of RecordID
						SET @lCounter = @lCounter + 1
						-- Set unique RecordID
						SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
						 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
						 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
						 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)	 + right(('000000'+ ltrim(str(@lCounter))),6)

						INSERT INTO GEO_RESTRICTIONS2 (RECORDID, LOC_RECORDID, SITE_APN, RESTRICTION_TYPE, RESTRICTION_NOTES)
											   VALUES (@szRecordID, @szLocRecordID, @szPIN, 'WARNING', 'SEE ENGINEERING; PARCEL IN FLOODPLAIN')

						IF @@error <> 0
							Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Keycol2, Keydata2, Comments)
										Values ('LANDTRAKINT', 25, 'ERROR', Getdate(), 'RESTRICTION TYPE', 'WARNING: FLOODPLAIN', 'SITE_APN', @szPIN, 
												'Error adding Address record to Geo_Restrictions2') 
					END

					-- Insert Flood Zone Restrictions
					IF @szFloodZone = 'FW'
					BEGIN
						-- Reset counter for unique number string at end of RecordID
						SET @lCounter = @lCounter + 1
						-- Set unique RecordID
						SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
						 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
						 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
						 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)	 + right(('000000'+ ltrim(str(@lCounter))),6)

						INSERT INTO GEO_RESTRICTIONS2 (RECORDID, LOC_RECORDID, SITE_APN, RESTRICTION_TYPE, RESTRICTION_NOTES)
											   VALUES (@szRecordID, @szLocRecordID, @szPIN, 'WARNING', 'SEE ENGINEERING; PARCEL IN FLOODWAY')

						IF @@error <> 0
							Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Keycol2, Keydata2, Comments)
										Values ('LANDTRAKINT', 25, 'ERROR', Getdate(), 'RESTRICTION TYPE', 'WARNING: FLOODWAY', 'SITE_APN', @szPIN, 
												'Error adding Address record to Geo_Restrictions2') 
					END

					-- Insert Historic Restrictions
					IF @szHistDist = 'ARVADA DOWNTOWN'
					BEGIN
						-- Reset counter for unique number string at end of RecordID
						SET @lCounter = @lCounter + 1
						-- Set unique RecordID
						SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
						 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
						 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
						 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)	 + right(('000000'+ ltrim(str(@lCounter))),6)

						INSERT INTO GEO_RESTRICTIONS2 (RECORDID, LOC_RECORDID, SITE_APN, RESTRICTION_TYPE, RESTRICTION_NOTES)
											   VALUES (@szRecordID, @szLocRecordID, @szPIN, 'WARNING', 'PARCEL IN HISTORIC DISTRICT')

						IF @@error <> 0
							Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Keycol2, Keydata2, Comments)
										Values ('LANDTRAKINT', 25, 'ERROR', Getdate(), 'RESTRICTION TYPE', 'WARNING: HISTORIC DISTRICT', 'SITE_APN', @szPIN, 
												'Error adding Address record to Geo_Restrictions2') 
					END

					-- Insert Olde Town Area Restrictions
					IF @szConservArea = 'OLDE TOWN AREA'
					BEGIN
						-- Reset counter for unique number string at end of RecordID
						SET @lCounter = @lCounter + 1
						-- Set unique RecordID
						SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
						 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
						 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
						 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)	 + right(('000000'+ ltrim(str(@lCounter))),6)

						INSERT INTO GEO_RESTRICTIONS2 (RECORDID, LOC_RECORDID, SITE_APN, RESTRICTION_TYPE, RESTRICTION_NOTES)
											   VALUES (@szRecordID, @szLocRecordID, @szPIN, 'WARNING', 'OLDE TOWN AREA')

						IF @@error <> 0
							Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Keycol2, Keydata2, Comments)
										Values ('LANDTRAKINT', 25, 'ERROR', Getdate(), 'RESTRICTION TYPE', 'WARNING: OLDE TOWN AREA', 'SITE_APN', @szPIN, 
												'Error adding Address record to Geo_Restrictions2')
					END

				END -- End Inserts if Geo_Ownership inserted without error

		END -- End Inserts for Parcel
	END -- If apn <> ''
  --------------------
  -- Variable Reset --
  --------------------
  SELECT @szLocRecordID = NULL, @szRecordID = NULL, @szSiteStreet = NULL, @fTotalVal = 0, @szParentRecID = NULL, @szMailAddr = NULL, @szSiteAddr = NULL, @szOwnCo = NULL, @szLandUse01 = NULL,
		 @szLandUse03 = NULL, @szOldCensus = NULL, @szOldGenPlan = NULL, @fOldImpVal = 0, @fOldLandVal = 0, @fOldLotSize = 0, @lOldNoStories = NULL, @szOldNoUnits = NULL, @szOldParent = NULL,
		 @szOldSecTwpRng = NULL, @szOldBlock = NULL, @szOldLot = NULL, @szOldSubdiv = NULL,	@fOldStructure = 0, @fOldYrBuilt = 0, @szOldZone2 = NULL, @szOldTRA = NULL, @szSiteDesc = NULL,
		 @szOldSiteAddr = NULL, @szOldOwner = NULL, @szOldOwnAddr1 = NULL, @szOldOwnAddr2 = NULL, @szOldOwnCity = NULL, @szOldOwnState = NULL, @szOldOwnZip = NULL, @szOldOthName = NULL,
		 @szOldWaterDist = NULL, @szOldSanitDist = NULL, @szOldCouncilDist = NULL, @szOldFireDist = NULL, @szOldMetroDist = NULL, @szOldHistDist = NULL, @szOldUrbanRenew = NULL,
		 @szOldCodeInsp = NULL, @fOldTAZID = 0, @szOldConservArea = NULL, @szOldZoning = NULL, @szOldGenZoning = NULL, @szOldZoningDesc = NULL, @szOldUnits = NULL, @szOldMaxHeight = NULL,
		 @szOldOrdRestrict = NULL, @szOldSpecStbkDev = NULL, @szOldLand01 = NULL, @szOldLand03 = NULL, @szOldLand05 = NULL, @szSchoolDist = NULL,
		 @szOldCornerLot = NULL, @szOldDblFrntLot = NULL, @fTotalImprv = NULL, @fTotalLandVal = NULL, @fNetSF = NULL, @fStories = NULL, @szOldParentID = NULL,
		 @fNoUnits = NULL, @szMailCity = NULL, @szOwnerName = NULL, @szOwnerName2 = NULL, @szMailState = NULL, @szMailZip = NULL, @szSection = NULL,
		 @szBlock = NULL, @szLot = NULL, @szSchoolDist = NULL, @szMailStrNbr = NULL, @szMailStrDir = NULL, @szMailStrName = NULL, @szMailStrType = NULL, @szMailStrSfx = NULL,
		 @szMailStrUnit = NULL, @szMailCity = NULL, @szSiteDesc = NULL, @szSec = NULL, @szSecTwpRng = NULL, @szOldOrdNo = NULL, @szOldSpecStbkDev = NULL, @szOldZoneLink = NULL,
		 @szOldSubDivLink = NULL, @szOldDfltInspctr = NULL, @szOldStreetName = null

  Fetch Next From cGetAddress Into @szPIN, @szCRWID, @szBlockGrpID, @szCompPlanDesc, @szCity, @szAddrType, @szAddrSubType, @szAddrName, @szStNum, @szStName,
								  @szPreDir, @szStType, @szStSuffix, @szUnitNum, @szZip, @szZoning, @szWaterDist, @szSanitDist,
								  @szCouncilDist, @szFireDist, @szMetroDist, @szHistDist, @szUrbanRenew, @szCodeInsp, @fTAZID, @szSubDivName,
								  @szConservArea, @szGenZoning, @szZoningDesc, @szUnits, @szMaxHeight, @szOrdRestrict, @szCornerAddr, @szFloodZone, @szLOMACaseNo,
								  @szOrdinanceNo, @szDblFrntLot, @szSpecStbkDev, @szDevStndURL, @szAddress, @szSubDivLink, @szInspector
  End --While
  Close cGetAddress
  Deallocate cGetAddress

  -------------------------
  -- Process Street Data --
  -------------------------
  DECLARE cGetStreets CURSOR LOCAL FOR        
  SELECT S.CRW_ID, S.FRADDL, S.TOADDL, S.FRADDR, S.TOADDR, UPPER(S.STREET_NAME), S.YR_BUILT, UPPER(S.FEANME), UPPER(S.DIRPRE), UPPER(S.DIRSUF), UPPER(S.FEATYP), I.INSPECTOR
	FROM STREETS S
	LEFT JOIN STREETS_INSPECTOR I ON S.CRW_ID = I.CRW_ID
  
  Open cGetStreets 
  Fetch Next From cGetStreets Into @szCRWID, @szFRADDL, @szTOADDL, @szFRADDR, @szTOADDRght, @szFullStName, @lYrBuilt, @szStName, @szPreDir, @szStSuffix, @szStType, @szInspector
  While @@Fetch_status <> -1 
  Begin
		-- Set SiteAddr
		IF @szTOADDRght > @szTOADDL	SET @szToAddr = CAST(@szTOADDRght AS VARCHAR)
		ELSE SET @szToAddr = CAST(@szTOADDL AS VARCHAR)
			
		IF @szFRADDR < @szFRADDL
			SET @szFromAddr = CAST(@szFRADDR AS VARCHAR)
		ELSE
			SET @szFromAddr = CAST(@szFRADDL AS VARCHAR)
				
		IF @szFromAddr IS NOT NULL
		BEGIN
			SET @szSiteNumber = @szFromAddr + '-' + @szToAddr
			SET @szSiteAddr = SUBSTRING(RTRIM(RTRIM(LTRIM(ISNULL(@szFromAddr, ' ') + '-' + ISNULL(@szToAddr, ' '))) + ' ' + ISNULL(@szFullStName, ' ')), 1, 70)
		END
		ELSE
			SET @szSiteAddr = SUBSTRING(@szFullStName, 1, 70)

		IF LEN(@szSiteNumber) > 12	SET @szSiteNumber = NULL
		
		-- Get RecordID if link found
		SELECT @szLocRecordID = RECORDID FROM GEO_OWNERSHIP WHERE SITE_ALTERNATE_ID = @szCRWID AND GEOTYPE = 'STREET'
	
		-- If a link is found, update record
		IF @szLocRecordID IS NOT NULL
		BEGIN
			-- Get old info to compare and see if update is necessary
			SELECT @szOldSiteAddr = SITE_ADDR, @fOldYrBuilt = YEAR_BUILT FROM GEO_OWNERSHIP WHERE RECORDID = @szLocRecordID
			-- If anything has been updated, update record
			IF ISNULL(@szOldSiteAddr, ' ') <> ISNULL(@szSiteAddr, ' ') OR ISNULL(@fOldYrBuilt, 0) <> CAST(ISNULL(@lYrBuilt, 0) AS FLOAT)
			BEGIN
				UPDATE GEO_OWNERSHIP SET SITE_ADDR = @szSiteAddr, SITE_ST_NAME = SUBSTRING(@szStName, 1, 30), SITE_ST_PREFIX = @szPreDir, SITE_ST_SUFFIX = @szStSuffix,
										 SITE_ST_TYPE = @szStType, SITE_NUMBER = @szSiteNumber, SITE_STREETNAME = SUBSTRING(@szFullStName, 1, 60), YEAR_BUILT = @lYrBuilt
						WHERE RECORDID = @szLocRecordID

				IF @@error <> 0
					Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Comments)
								Values ('LANDTRAKINT', 20, 'ERROR', Getdate(), 'SITE APN', @szCRWID, 'Error updating Street record in Geo_Ownership')

				-- if site address has changed, insert change log record
				IF ISNULL(@szOldSiteAddr, ' ') <> ISNULL(@szSiteAddr, ' ')
				BEGIN
					-- Set unique RecordID
					SET @szRecordID = NULL
					-- Reset counter for unique number string at end of RecordID
					SET @lCounter = @lCounter + 1
					SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
					 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2) + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
					 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2) + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
					 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2) + right(('000000'+ ltrim(str(@lCounter))),6)
					 
					INSERT INTO GEO_CHANGE_LOG(ENTRY_DATE, FIELD_CHANGED, LOC_RECORDID, NEW_VALUE, OLD_VALUE, RECORDID)
										VALUES(GetDate(), 'SITE_ADDR', @szLocRecordID, @szSiteAddr, @szOldSiteAddr, @szRecordID)
				END

			END

			-- Get old info to compare and see if update is necessary
			SELECT @szOldDfltInspctr = DFLT_INSPCTR_PERMIT FROM GEO_OWNERSHIPUDF WHERE LOC_RECORDID = @szLocRecordID
			-- If anything has been updated, update record
			IF isnull(@szOldDfltInspctr,'') <> isnull(@szInspector,'')
			BEGIN
				UPDATE GEO_OWNERSHIPUDF SET DFLT_INSPCTR_PERMIT = @szInspector WHERE LOC_RECORDID = @szLocRecordID

				IF @@error <> 0
					Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Comments)
								Values ('LANDTRAKINT', 22, 'ERROR', Getdate(), 'SITE APN', @szPIN, 'Error updating Street record in Geo_OwnershipUDF')
			END	
		END
		ELSE -- If a link is not found, insert record
		BEGIN
			-- Set unique RecordID
			SET @szLocRecordID = NULL
			-- Reset counter for unique number string at end of RecordID
			SET @lCounter = @lCounter + 1
			SET @szLocRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
			 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
			 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
			 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)	 + right(('000000'+ ltrim(str(@lCounter))),6)

			INSERT INTO [dbo].[Activity] ([ActivityID], [ActivityTypeID]) VALUES(@szLocRecordID, 5 )

			INSERT INTO GEO_OWNERSHIP (ActivityTypeID, RECORDID, SITE_APN, SITE_ALTERNATE_ID, SITE_ADDR, SITE_NUMBER, SITE_ST_NAME, SITE_ST_PREFIX,
									   SITE_ST_SUFFIX, SITE_ST_TYPE, SITE_STREETNAME, SITE_CITY, SITE_STATE, YEAR_BUILT, STATUS, GEOTYPE, MULTI_ADDRESS)
							   VALUES (5, @szLocRecordID, @szCRWID, @szCRWID, @szSiteAddr, @szSiteNumber, SUBSTRING(@szStName, 1, 30), @szPreDir,
									   @szStSuffix, @szStType, SUBSTRING(@szFullStName, 1, 60), 'ARVADA', 'CO', @lYrBuilt, 'ACTIVE', 'STREET', 0)

				IF @@error <> 0
				   Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Comments)
								Values ('LANDTRAKINT', 21, 'ERROR', Getdate(), 'SITE APN', @szCRWID, 'Error adding Street record to Geo_Ownership')
				ELSE
				BEGIN
				   -- Log new record for reporting purposes
				   Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Comments)
								Values ('LANDTRAKINT', 30, 'LOG', Getdate(), 'SITE APN', @szCRWID, 'New Street record added to Geo_Ownership')

					INSERT INTO GEO_OWNERSHIPUDF (LOC_RECORDID, SITE_APN, DFLT_INSPCTR_PERMIT)
										  VALUES (@szLocRecordID, @szCRWID, @szInspector)

					-- Reset counter for unique number string at end of RecordID
					SET @lCounter = @lCounter + 1
					-- Set unique RecordID
					SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
					 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
					 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
					 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)	 + right(('000000'+ ltrim(str(@lCounter))),6)

					INSERT INTO GEO_UDF (RECORDID, LOC_RECORDID, SITE_APN)	 VALUES (@szRecordID, @szLocRecordID, @szCRWID)

				END -- End Inserts if Geo_Ownership inserted without error
		END -- End Inserts for Street

  --------------------
  -- Variable Reset --
  --------------------
  SELECT @szLocRecordID = NULL, @szRecordID = NULL, @szSiteAddr = NULL, @szFromAddr = NULL, @szToAddr = NULL, @szSiteNumber = NULL, @szOldDfltInspctr = NULL

  Fetch Next From cGetStreets Into @szCRWID, @szFRADDL, @szTOADDL, @szFRADDR, @szTOADDRght, @szFullStName, @lYrBuilt, @szStName, @szPreDir, @szStSuffix, @szStType, @szInspector
  End --While
  Close cGetStreets 
  Deallocate cGetStreets

  -------------------------------
  -- Process Intersection Data --
  -------------------------------
  DECLARE cGetIntersection CURSOR LOCAL FOR        
  SELECT I.CRW_ID, UPPER(I.INTERSECTION), INS.INSPECTOR
	FROM INTERSECTIONS I
	LEFT JOIN INTERSECTIONS_INSPECTOR INS ON I.CRW_ID = INS.CRW_ID
  
  Open cGetIntersection 
  Fetch Next From cGetIntersection Into @szCRWID, @szIntersection, @szInspector
  While @@Fetch_status <> -1 
  Begin
		-- Get RecordID if link found
		SELECT @szLocRecordID = RECORDID FROM GEO_OWNERSHIP WHERE SITE_ALTERNATE_ID = @szCRWID AND GEOTYPE = 'INTERSECTION'
	
		-- If a link is found, update record
		IF @szLocRecordID IS NOT NULL
		BEGIN
			-- Get old info to compare and see if update is necessary
			SELECT @szOldSiteAddr = SITE_ADDR, @szOldNotes = SITE_DESCRIPTION, @szOldStreetName=SITE_ST_NAME
			FROM GEO_OWNERSHIP WHERE RECORDID = @szLocRecordID
			
			-- If anything has been updated, update record
			IF ISNULL(@szOldSiteAddr, ' ') <> SUBSTRING(ISNULL(@szIntersection, ' '), 1, 70) OR ISNULL(@szOldNotes, ' ') <> ISNULL(@szIntersection, ' ')
				OR isnull(@szOldStreetName,'') <> isnull(SUBSTRING(@szIntersection, 1, 30),'')
			BEGIN
				UPDATE GEO_OWNERSHIP SET SITE_ADDR = SUBSTRING(@szIntersection, 1, 70), SITE_STREETNAME = SUBSTRING(@szIntersection, 1, 60),
										 SITE_ST_NAME = SUBSTRING(@szIntersection, 1, 30), SITE_DESCRIPTION = @szSiteDesc
						WHERE RECORDID = @szLocRecordID

					IF @@error <> 0
					   Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Comments)
									Values ('LANDTRAKINT', 20, 'ERROR', Getdate(), 'SITE APN', @szCRWID, 'Error updating Intersection record in Geo_Ownership')

				-- if site address has changed, insert change log record
				IF ISNULL(@szOldSiteAddr, ' ') <> SUBSTRING(ISNULL(@szIntersection, ' '), 1, 70)
				BEGIN
					-- Set unique RecordID
					SET @szRecordID = NULL
					-- Reset counter for unique number string at end of RecordID
					SET @lCounter = @lCounter + 1
					SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
					 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
					 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
					 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)	 + right(('000000'+ ltrim(str(@lCounter))),6)
					 
					INSERT INTO GEO_CHANGE_LOG(ENTRY_DATE, FIELD_CHANGED, LOC_RECORDID, NEW_VALUE, OLD_VALUE, RECORDID)
										VALUES(GetDate(), 'SITE_ADDR', @szLocRecordID, SUBSTRING(@szIntersection, 1, 70), @szOldSiteAddr, @szRecordID)
				END
			END

			-- Get old info to compare and see if update is necessary
			SELECT @szOldDfltInspctr = DFLT_INSPCTR_PERMIT FROM GEO_OWNERSHIPUDF WHERE LOC_RECORDID = @szLocRecordID
			-- If anything has been updated, update record
			IF isnull(@szOldDfltInspctr,'') <> isnull(@szInspector,'')
			BEGIN
				UPDATE GEO_OWNERSHIPUDF SET DFLT_INSPCTR_PERMIT = @szInspector WHERE LOC_RECORDID = @szLocRecordID

				IF @@error <> 0
					Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Comments)
								Values ('LANDTRAKINT', 22, 'ERROR', Getdate(), 'SITE APN', @szPIN, 'Error updating Intersection record in Geo_OwnershipUDF')
			END	
		END
		ELSE -- If a link is not found, insert record
		BEGIN
			-- Set unique RecordID
			SET @szLocRecordID = NULL
			-- Reset counter for unique number string at end of RecordID
			SET @lCounter = @lCounter + 1
			SET @szLocRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
			 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2) + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
			 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2) + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
			 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2) + right(('000000'+ ltrim(str(@lCounter))),6)

			INSERT INTO [dbo].[Activity] ([ActivityID], [ActivityTypeID]) VALUES(@szLocRecordID, 5 )

			INSERT INTO GEO_OWNERSHIP (ActivityTypeID, RECORDID, SITE_APN, SITE_ALTERNATE_ID, SITE_DESCRIPTION, SITE_ADDR, SITE_ST_NAME,
									   SITE_STREETNAME, SITE_CITY, SITE_STATE, STATUS, GEOTYPE, MULTI_ADDRESS)
							   VALUES (5, @szLocRecordID, @szCRWID, @szCRWID, @szIntersection, SUBSTRING(@szIntersection, 1, 70), SUBSTRING(@szIntersection, 1, 30),
									   SUBSTRING(@szIntersection, 1, 60), 'ARVADA', 'CO', 'ACTIVE', 'INTERSECTION', 0)

				IF @@error <> 0
				   Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Comments)
								Values ('LANDTRAKINT', 21, 'ERROR', Getdate(), 'SITE APN', @szCRWID, 'Error adding Intersection record to Geo_Ownership')
				ELSE
				BEGIN
				   -- Log new record for reporting purposes
				   Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Comments)
								Values ('LANDTRAKINT', 30, 'LOG', Getdate(), 'SITE APN', @szCRWID, 'New Intersection record added to Geo_Ownership')

					INSERT INTO GEO_OWNERSHIPUDF (LOC_RECORDID, SITE_APN, DFLT_INSPCTR_PERMIT)	  VALUES (@szLocRecordID, @szCRWID, @szInspector)

					-- Reset counter for unique number string at end of RecordID
					SET @lCounter = @lCounter + 1
					-- Set unique RecordID
					SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
					 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2) + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
					 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2) + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
					 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2) + right(('000000'+ ltrim(str(@lCounter))),6)

					INSERT INTO GEO_UDF (RECORDID, LOC_RECORDID, SITE_APN)	 VALUES (@szRecordID, @szLocRecordID, @szCRWID)


				END -- End Inserts if Geo_Ownership inserted without error
		END -- End Inserts for Intersections

  --------------------
  -- Variable Reset --
  --------------------
  SELECT @szLocRecordID = NULL, @szRecordID = NULL, @szOldSiteDesc = NULL, @szOldDfltInspctr = NULL

  Fetch Next From cGetIntersection Into @szCRWID, @szIntersection, @szInspector
  End --While
  Close cGetIntersection 
  Deallocate cGetIntersection

  ----------------------
  -- Process CIP Data --
  ----------------------
  DECLARE cGetCIP CURSOR LOCAL FOR        
  SELECT C.CRW_ID, UPPER(C.PROJECT_NAME), C.PROJECT_NO, I.INSPECTOR
	FROM CIP C
	LEFT JOIN CIP_INSPECTOR I ON C.CRW_ID = I.CRW_ID
  
  Open cGetCIP 
  Fetch Next From cGetCIP Into @szCRWID, @szProjName, @szProjNo, @szInspector
  While @@Fetch_status <> -1 
  Begin
		-- Get RecordID if link found
		SELECT @szLocRecordID = RECORDID FROM GEO_OWNERSHIP WHERE SITE_ALTERNATE_ID = @szCRWID AND GEOTYPE = 'CIP'

		-- Set Description
		SET @szSiteDesc = @szProjName + ' ' + @szProjNo
		
		-- If a link is found, update record
		IF @szLocRecordID IS NOT NULL
		BEGIN

			-- Get old info to compare and see if update is necessary
			SELECT @szOldSiteDesc = SITE_DESCRIPTION FROM GEO_OWNERSHIP WHERE RECORDID = @szLocRecordID
			
			-- If anything has been updated, update record
			IF ISNULL(@szOldSiteDesc, ' ') <> ISNULL(@szSiteDesc, ' ')
			BEGIN
				UPDATE GEO_OWNERSHIP SET SITE_DESCRIPTION = @szSiteDesc	WHERE RECORDID = @szLocRecordID

				IF @@error <> 0
					Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Comments)
								Values ('LANDTRAKINT', 20, 'ERROR', Getdate(), 'SITE APN', @szCRWID, 'Error updating CIP record in Geo_Ownership')
			END

			-- Get old info to compare and see if update is necessary
			SELECT @szOldDfltInspctr = DFLT_INSPCTR_PERMIT FROM GEO_OWNERSHIPUDF WHERE LOC_RECORDID = @szLocRecordID
			-- If anything has been updated, update record
			IF isnull(@szOldDfltInspctr,'') <> isnull(@szInspector,'')
			BEGIN
				UPDATE GEO_OWNERSHIPUDF SET DFLT_INSPCTR_PERMIT = @szInspector WHERE LOC_RECORDID = @szLocRecordID

				IF @@error <> 0
					Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Comments)
								Values ('LANDTRAKINT', 22, 'ERROR', Getdate(), 'SITE APN', @szPIN, 'Error updating CIP record in Geo_OwnershipUDF')
			END	
			
		END
		ELSE -- If a link is not found, insert record
		BEGIN
			-- Set unique RecordID
			SET @szLocRecordID = NULL
			-- Reset counter for unique number string at end of RecordID
			SET @lCounter = @lCounter + 1
			SET @szLocRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
			 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2) + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
			 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2) + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
			 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2) + right(('000000'+ ltrim(str(@lCounter))),6)

			INSERT INTO [dbo].[Activity] ([ActivityID], [ActivityTypeID]) VALUES(@szLocRecordID, 5 )

			INSERT INTO GEO_OWNERSHIP (ActivityTypeID, RECORDID, SITE_APN, SITE_ALTERNATE_ID, SITE_DESCRIPTION, --SITE_ADDR,
									   SITE_CITY, SITE_STATE, STATUS, GEOTYPE, MULTI_ADDRESS)
							   VALUES (5, @szLocRecordID, @szCRWID, @szCRWID, @szSiteDesc,
									   'ARVADA', 'CO', 'ACTIVE', 'CIP', 0)

				IF @@error <> 0
				   Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Comments)
								Values ('LANDTRAKINT', 21, 'ERROR', Getdate(), 'SITE APN', @szCRWID, 'Error adding CIP record to Geo_Ownership')
				ELSE
				BEGIN
				   -- Log new record for reporting purposes
				   Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Comments)
								Values ('LANDTRAKINT', 30, 'LOG', Getdate(), 'SITE APN', @szCRWID, 'New CIP record added to Geo_Ownership')

					INSERT INTO GEO_OWNERSHIPUDF (LOC_RECORDID, SITE_APN, DFLT_INSPCTR_PERMIT)
										  VALUES (@szLocRecordID, @szCRWID, @szInspector)

					-- Reset counter for unique number string at end of RecordID
					SET @lCounter = @lCounter + 1
					-- Set unique RecordID
					SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
					 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2) + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
					 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2) + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
					 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2) + right(('000000'+ ltrim(str(@lCounter))),6)

					INSERT INTO GEO_UDF (RECORDID, LOC_RECORDID, SITE_APN)	 VALUES (@szRecordID, @szLocRecordID, @szCRWID)

				END -- End Inserts if Geo_Ownership inserted without error
		END -- End Inserts for CIP

  --------------------
  -- Variable Reset --
  --------------------
  SELECT @szLocRecordID = NULL, @szRecordID = NULL, @szSiteDesc = NULL, @szOldSiteDesc = NULL, @szOldDfltInspctr = NULL, @szOldStreetName = null

  Fetch Next From cGetCIP Into @szCRWID, @szProjName, @szProjNo, @szInspector
  End --While
  Close cGetCIP 
  Deallocate cGetCIP

  ------------------------------
  -- Process SubDivision Data --
  ------------------------------
  DECLARE cGetSubDivision CURSOR LOCAL FOR        
  SELECT S.CRW_ID, S.RECEPTION_NO, S.RECORDED_DATE, S.SUBDIVISION_NAME, S.WEB_LINK, I.INSPECTOR
	FROM SUBDIVISION S
	LEFT JOIN SUBDIVISION_INSPECTOR I ON S.CRW_ID = I.CRW_ID
  
  Open cGetSubDivision 
  Fetch Next From cGetSubDivision Into @szCRWID, @szReceptionNo, @dtRecorded, @szSubDivName, @szWebLink, @szInspector
  While @@Fetch_status <> -1 
  Begin
		-- Get RecordID if link found
		SELECT @szLocRecordID = RECORDID FROM GEO_OWNERSHIP WHERE SITE_ALTERNATE_ID = @szCRWID AND GEOTYPE = 'SUBDIVISION'
	
		-- Set Description
		SET @szSiteDesc = 'RECEPTION NUMBER: ' + @szReceptionNo + ' DATE: ' + CAST(@dtRecorded AS VARCHAR)

		-- If a link is found, update record
		IF @szLocRecordID IS NOT NULL
		BEGIN
			-- Get old info to compare and see if update is necessary
			SELECT @szOldSiteDesc = SITE_DESCRIPTION, @szOldSubdiv = @szSubDivName, @szOldStreetName = SITE_STREETNAME FROM GEO_OWNERSHIP WHERE RECORDID = @szLocRecordID
			-- If anything has been updated, update record
			IF ISNULL(@szOldSiteDesc, ' ') <> ISNULL(@szSiteDesc, ' ') OR ISNULL(@szOldSubdiv, ' ') <> ISNULL(@szSubDivName, ' ') or isnull(@szOldStreetName,'') <> isnull(@szSubDivName,'')
			BEGIN
				UPDATE GEO_OWNERSHIP SET SITE_DESCRIPTION = @szSiteDesc, SITE_SUBDIVISION = @szSubDivName, SITE_STREETNAME = substring(@szSubDivName,1,60)
						WHERE RECORDID = @szLocRecordID

				IF @@error <> 0
					Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Comments)
								Values ('LANDTRAKINT', 20, 'ERROR', Getdate(), 'SITE APN', @szCRWID, 'Error updating SubDivision record in Geo_Ownership')
			END

			-- Get old info to compare and see if update is necessary
			SELECT @szOldLegalDesc = DESCRIPTION FROM GEO_LEGALS WHERE LOC_RECORDID = @szLocRecordID
			-- If anything has been updated, update record
			IF @szOldLegalDesc <> @szWebLink
			BEGIN
				UPDATE GEO_LEGALS SET DESCRIPTION = @szWebLink WHERE LOC_RECORDID = @szLocRecordID

				IF @@error <> 0
					Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Comments)
								Values ('LANDTRAKINT', 21, 'ERROR', Getdate(), 'SITE APN', @szPIN, 'Error updating Subdivision record in Geo_Legals')
			END	

			-- Get old info to compare and see if update is necessary
			SELECT @szOldDfltInspctr = DFLT_INSPCTR_PERMIT FROM GEO_OWNERSHIPUDF WHERE LOC_RECORDID = @szLocRecordID
			-- If anything has been updated, update record
			IF isnull(@szOldDfltInspctr,'') <> isnull(@szInspector,'')
			BEGIN
				UPDATE GEO_OWNERSHIPUDF SET DFLT_INSPCTR_PERMIT = @szInspector WHERE LOC_RECORDID = @szLocRecordID

				IF @@error <> 0
					Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Comments)
								Values ('LANDTRAKINT', 22, 'ERROR', Getdate(), 'SITE APN', @szPIN, 'Error updating Subdivision record in Geo_OwnershipUDF')
			END	
		END
		ELSE -- If a link is not found, insert record
		BEGIN
			-- Set unique RecordID
			SET @szLocRecordID = NULL
			-- Reset counter for unique number string at end of RecordID
			SET @lCounter = @lCounter + 1
			SET @szLocRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
			 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
			 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
			 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)	 + right(('000000'+ ltrim(str(@lCounter))),6)

			INSERT INTO [dbo].[Activity] ([ActivityID], [ActivityTypeID]) VALUES(@szLocRecordID, 5 )

			INSERT INTO GEO_OWNERSHIP (ActivityTypeID, RECORDID, SITE_APN, SITE_ALTERNATE_ID, SITE_DESCRIPTION, SITE_SUBDIVISION,
									   SITE_CITY, SITE_STATE, STATUS, GEOTYPE, MULTI_ADDRESS, SITE_STREETNAME)
							   VALUES (5, @szLocRecordID, @szCRWID, @szCRWID, @szSiteDesc, @szSubDivName,
									   'ARVADA', 'CO', 'ACTIVE', 'SUBDIVISION', 0, substring(@szSubDivName,1,60))

				IF @@error <> 0
				   Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Comments)
								Values ('LANDTRAKINT', 23, 'ERROR', Getdate(), 'SITE APN', @szCRWID, 'Error adding SubDivision record to Geo_Ownership')
				ELSE
				BEGIN
				   -- Log new record for reporting purposes
				   Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Comments)
								Values ('LANDTRAKINT', 30, 'LOG', Getdate(), 'SITE APN', @szCRWID, 'New SubDivision record added to Geo_Ownership')

					INSERT INTO GEO_OWNERSHIPUDF (LOC_RECORDID, SITE_APN, DFLT_INSPCTR_PERMIT)
										  VALUES (@szLocRecordID, @szCRWID, @szInspector)

					-- Reset counter for unique number string at end of RecordID
					SET @lCounter = @lCounter + 1
					-- Set unique RecordID
					SET @szRecordID ='CONV:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
					 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
					 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)	 + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
					 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)	 + right(('000000'+ ltrim(str(@lCounter))),6)

					INSERT INTO GEO_UDF (RECORDID, LOC_RECORDID, SITE_APN)
								 VALUES (@szRecordID, @szLocRecordID, @szCRWID)

					-- Insert Legal Description
					IF @szWebLink IS NOT NULL
					BEGIN
						INSERT INTO GEO_LEGALS (LOC_RECORDID, SITE_APN, DESCRIPTION)
										VALUES (@szLocRecordID, @szPIN, @szWebLink)

						IF @@error <> 0
							Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Comments)
										Values ('LANDTRAKINT', 25, 'ERROR', Getdate(), 'SITE_APN', @szPIN, 'Error adding Subdivision record to Geo_Legals')

					END

				END -- End Inserts if Geo_Ownership inserted without error
		END -- End Inserts for Subdivision

  --------------------
  -- Variable Reset --
  --------------------
  SELECT @szLocRecordID = NULL, @szRecordID = NULL, @szSiteDesc = NULL, @szOldSiteDesc = NULL, @szOldDfltInspctr = NULL

  Fetch Next From cGetSubDivision Into @szCRWID, @szReceptionNo, @dtRecorded, @szSubDivName, @szWebLink, @szInspector
  End --While
  Close cGetSubDivision 
  Deallocate cGetSubDivision

  ----------------------------------------
  -- Retire non-existing Parcel Records --
  ----------------------------------------
  DECLARE cRetire CURSOR LOCAL FOR        
  SELECT G.RECORDID
	FROM GEO_OWNERSHIP G
  WHERE ((G.GEOTYPE = 'PARCEL' AND G.SITE_APN NOT IN ( SELECT PPIN FROM ADAMS_PARCELS
													    WHERE PPIN = G.SITE_APN AND PPIN IS NOT NULL ) 
							   AND G.SITE_APN NOT IN ( SELECT PPIN FROM JEFFCO_PARCELS WHERE PPIN = G.SITE_APN AND PPIN IS NOT NULL ))
      OR (G.GEOTYPE = 'ADDRESS' AND G.SITE_ALTERNATE_ID NOT IN ( SELECT ACRWID FROM ADDRESSES
														 WHERE APIN IS NOT NULL AND APIN <> '' AND ACRWID = G.SITE_ALTERNATE_ID AND ACRWID IS NOT NULL ))
      OR (G.GEOTYPE = 'STREET' AND G.SITE_ALTERNATE_ID NOT IN ( SELECT CRW_ID FROM STREETS WHERE CRW_ID = G.SITE_ALTERNATE_ID AND CRW_ID IS NOT NULL ))
      OR (G.GEOTYPE = 'INTERSECTION' AND G.SITE_ALTERNATE_ID NOT IN ( SELECT CRW_ID FROM INTERSECTIONS WHERE CRW_ID = G.SITE_ALTERNATE_ID AND CRW_ID IS NOT NULL ))
      OR (G.GEOTYPE = 'CIP' AND G.SITE_ALTERNATE_ID NOT IN ( SELECT CRW_ID FROM CIP WHERE CRW_ID = G.SITE_ALTERNATE_ID AND CRW_ID IS NOT NULL ))
      OR (G.GEOTYPE = 'SUBDIVISION' AND G.SITE_ALTERNATE_ID NOT IN ( SELECT CRW_ID FROM SUBDIVISION WHERE CRW_ID = G.SITE_ALTERNATE_ID AND CRW_ID IS NOT NULL )))
	AND G.GEOTYPE IN ('PARCEL', 'ADDRESS', 'STREET', 'INTERSECTION', 'CIP', 'SUBDIVISION')
	AND ISNULL(G.STATUS, ' ') <> 'RETIRED'
	AND G.SITE_APN <> 'CRW-PARCEL'

  Open cRetire 
  Fetch Next From cRetire Into @szLocRecordID
  While @@Fetch_status <> -1 
  Begin

		   UPDATE GEO_OWNERSHIP SET STATUS = 'RETIRED' WHERE RECORDID = @szLocRecordID

			-- Log record update for reporting purposes
		   Insert Into convlog (Procname, Errcode, Action, Actdttm, Keycol1, Keydata1, Comments)
						Values ('LANDTRAKINT', 50, 'LOG', Getdate(), 'LOC_RECORDID', @szLocRecordID, 'Geo_Ownership record status updated - RETIRED')

  Fetch Next From cRetire Into @szLocRecordID
  End --While
  Close cRetire 
  Deallocate cRetire


  --update lat and long for all the geo types from GIS data:
      --run an update of lat and long and coordX and Y en masse so "plot on map" from GIS works and so does iTrakIt


  --address gets its info from ADDRESSES table:
	update o
	set lat = a.LATITUDE,
		lon = a.LONGITUDE,
		COORD_X = a.POINT_X, 
		COORD_Y = a.POINT_Y
	from dbo.ADDRESSES a
	inner join dbo.geo_ownership o
		on o.GEOTYPE = 'ADDRESS'
		and o.SITE_ALTERNATE_ID = a.ACRWID
	where
	(
		a.latitude is not null
		and o.lat is null
	)
	and a.LONGITUDE is not null

	--update lat and long where lat is enough off from GIS (GIS has less decimal places than TrakIt stores making this a little tricky)
	update o
	set lat = a.LATITUDE,
		lon = a.LONGITUDE,
		COORD_X = a.POINT_X, 
		COORD_Y = a.POINT_Y 
	from dbo.ADDRESSES a
	inner join dbo.geo_ownership o
		on o.GEOTYPE = 'ADDRESS'
		and o.SITE_ALTERNATE_ID = a.ACRWID
	where
	(
		a.latitude is not null
		and convert(decimal(18,5), round(a.LATITUDE,5)) != convert(decimal(18,5), round(o.lat,5))
		and o.lat is not null
	)
	and a.LONGITUDE is not null


   --SUBDIVISION LAT AND LONG UPDATE:
	update o
	set lat = a.LATITUDE,
		lon = a.LONGITUDE
	from dbo.GIS_LAT_LONG a
	inner join dbo.geo_ownership o
		on o.GEOTYPE = 'SUBDIVISION'
		and o.SITE_APN = a.CRW_ID
	where
	(
		a.latitude is not null
		and o.lat is null
	)
	and a.LONGITUDE is not null


	update o
	set lat = a.LATITUDE,
		lon = a.LONGITUDE  
	from dbo.GIS_LAT_LONG a
	inner join dbo.geo_ownership o
		on o.GEOTYPE = 'SUBDIVISION'
		and o.SITE_APN = a.CRW_ID
	where
	(
		a.latitude is not null
		and convert(decimal(18,5), round(a.LATITUDE,5)) != convert(decimal(18,5), round(o.lat,5))
		and o.lat is not null
	)
	and a.LONGITUDE is not null


  --STREET LAT AND LONG UPDATE:
	update o
	set lat = a.LATITUDE,
		lon = a.LONGITUDE
	from dbo.GIS_LAT_LONG a
	inner join dbo.geo_ownership o
		on o.GEOTYPE = 'STREET'
		and o.SITE_APN = a.CRW_ID
	where
	(
		a.latitude is not null
		and o.lat is null
	)
	and a.LONGITUDE is not null


	update o
	set lat = a.LATITUDE,
		lon = a.LONGITUDE  
	from dbo.GIS_LAT_LONG a
	inner join dbo.geo_ownership o
		on o.GEOTYPE = 'STREET'
		and o.SITE_APN = a.CRW_ID
	where
	(
		a.latitude is not null
		and convert(decimal(18,5), round(a.LATITUDE,5)) != convert(decimal(18,5), round(o.lat,5))
		and o.lat is not null
	)
	and a.LONGITUDE is not null


  --PARCEL LAT AND LONG UPDATE:
	update o
	set lat = a.LATITUDE,
		lon = a.LONGITUDE 
	from dbo.GIS_LAT_LONG a
	inner join dbo.geo_ownership o
		on o.GEOTYPE = 'PARCEL'
		and o.SITE_APN = a.CRW_ID
	where
	(
		a.latitude is not null
		and o.lat is null
	)
	and a.LONGITUDE is not null


	update o
	set lat = a.LATITUDE,
		lon = a.LONGITUDE   
	from dbo.GIS_LAT_LONG a
	inner join dbo.geo_ownership o
		on o.GEOTYPE = 'PARCEL'
		and o.SITE_APN = a.CRW_ID
	where
	(
		a.latitude is not null
		and convert(decimal(18,5), round(a.LATITUDE,5)) != convert(decimal(18,5), round(o.lat,5))
		and o.lat is not null
	)
	and a.LONGITUDE is not null

	

  --INTERSECTION LAT AND LONG UPDATE
	update o
	set lat = a.LATITUDE,
		lon = a.LONGITUDE
	from dbo.GIS_LAT_LONG a
	inner join dbo.geo_ownership o
		on o.GEOTYPE = 'INTERSECTION'
		and o.SITE_APN = a.CRW_ID
	where
	(
		a.latitude is not null
		and o.lat is null
	)
	and a.LONGITUDE is not null


	update o
	set lat = a.LATITUDE,
		lon = a.LONGITUDE  
	from dbo.GIS_LAT_LONG a
	inner join dbo.geo_ownership o
		on o.GEOTYPE = 'INTERSECTION'
		and o.SITE_APN = a.CRW_ID
	where
	(
		a.latitude is not null
		and convert(decimal(18,5), round(a.LATITUDE,5)) != convert(decimal(18,5), round(o.lat,5))
		and o.lat is not null
	)
	and a.LONGITUDE is not null



  Insert Into dbo.convlog (Procname, Errcode, Action, Actdttm)
                   Values ('LANDTRAKINT', 0, 'END', Getdate())

    -- Recreate triggers that were dropped at beginning of run
	Exec [enableGeoTriggers]


End -- Procedure
GO
