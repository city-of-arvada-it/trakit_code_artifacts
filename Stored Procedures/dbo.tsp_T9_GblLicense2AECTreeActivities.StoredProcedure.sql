USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_GblLicense2AECTreeActivities]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_GblLicense2AECTreeActivities]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_GblLicense2AECTreeActivities]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  					  
-- =====================================================================
-- Revision History:
--	RB - 3/31/2014

-- =====================================================================   	
Create Procedure [dbo].[tsp_T9_GblLicense2AECTreeActivities](     
	   @Activity_no			varchar(50),
	   @Group				varchar(20)	 
)

As
 BEGIN


 Declare @SQLStatement varchar(500)

 Set @SQLStatement = null 

 Set @SQLStatement = 'select ID, RECORDID from ' 

		Set @SQLStatement = @SQLStatement + 

		Case @Group
			
			When 'LICENSE2' Then ' license2_people where LICENSE_NO = ' + CHAR(39) + @Activity_no + CHAR(39) 			
			
		End 


		if @SQLStatement is not null and @SQLStatement <> ''
		 Begin
		     EXEC(@SQLStatement)
	     End

 END




GO
