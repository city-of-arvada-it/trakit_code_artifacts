USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GblAdditionalSiteTreeActivities]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_GblAdditionalSiteTreeActivities]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GblAdditionalSiteTreeActivities]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[tsp_GblAdditionalSiteTreeActivities]  
(  
      @LocRecordID varchar(100)   
      , @maxLimit int = -1  
)  
  
As  
 BEGIN  
  
  if @LocRecordID IS NOT null and @LocRecordID <> ''  
 BEGIN  
  
 IF OBJECT_ID('tempdb..#tmpSiteTreeRecords') IS NOT NULL    
 BEGIN  
  drop table #tmpSiteTreeRecords  
 END  
   
 Select top 0 * into #tmpSiteTreeRecords from tvw_GblTreeActivities  
  
   if @maxLimit <> -1   
   BEGIN   
     insert into #tmpSiteTreeRecords    
     SELECT TOP (@maxLimit) * from tvw_GblTreeActivities   
     where (recordid = @LocRecordID or loc_recordid = @LocRecordID or PARENT_RECORDID = @LocRecordID) and Tgroup = 'PERMIT'  
     insert into #tmpSiteTreeRecords    
     SELECT TOP (@maxLimit) * from tvw_GblTreeActivities   
     where (recordid = @LocRecordID or loc_recordid = @LocRecordID or PARENT_RECORDID = @LocRecordID) and Tgroup = 'PROJECT'  
     insert into #tmpSiteTreeRecords    
     SELECT TOP (@maxLimit) * from tvw_GblTreeActivities   
     where (recordid = @LocRecordID or loc_recordid = @LocRecordID or PARENT_RECORDID = @LocRecordID) and Tgroup = 'CASE'  
     insert into #tmpSiteTreeRecords    
     SELECT TOP (@maxLimit) * from tvw_GblTreeActivities   
     where (recordid = @LocRecordID or loc_recordid = @LocRecordID or PARENT_RECORDID = @LocRecordID) and Tgroup = 'CRM'  
     insert into #tmpSiteTreeRecords    
     SELECT TOP (@maxLimit) * from tvw_GblTreeActivities   
     where (recordid = @LocRecordID or loc_recordid = @LocRecordID or PARENT_RECORDID = @LocRecordID) and Tgroup = 'LICENSE2'  
     insert into #tmpSiteTreeRecords         
     SELECT * from tvw_GblTreeActivities  
     where (recordid = @LocRecordID or loc_recordid = @LocRecordID or PARENT_RECORDID = @LocRecordID) and Tgroup = 'GEO'  
     order by SORT_CRITERIA1 desc, site_addr  asc  
  
     declare @permitQuery nvarchar(MAX)= ''  
     declare @permitMaxLimit int = -1;  
     declare @permitCount int = (select count('Tgroup') from #tmpSiteTreeRecords where Tgroup = 'PERMIT');  
     if @permitCount < @maxLimit  
     BEGIN  
      SET @permitMaxLimit = (@maxLimit - @permitCount);  
  
      SET @permitQuery = N'Select TOP (' + cast(@permitMaxLimit as nvarchar(MAX)) + ') a.permit_no as ACTIVITYNO, b.permittype as ACTIVITYTYPE, b.PermitSubType as ACTIVITYSUBTYPE, b.DESCRIPTION as DESCRIPTION, a.SITE_APN, b.RECORDID, b.LOC_RECORDID,  b.STATUS, ''PERMIT'' as 
Tgroup, '''' as GEOTYPE,   
        '''' as SITE_ALTERNATE_ID, '''' as PARENT_PROJECT_NO, '''' as PARENT_PERMIT_NO, '''' as PARENT_SITE_APN,  a.site_addr, '''' as PARENT_RECORDID, '''' as PARENT_BUS_LIC_NO,   
          '''' as PARENT_AEC_ST_LIC_NO, '''' as DATEPROPERTYLIST, CONVERT(VARCHAR(50), b.applied, 101) AS DATE1,   
          CONVERT(VARCHAR(50), b.approved, 101) AS DATE2, CONVERT(VARCHAR(50), b.expired, 101) AS DATE3, CONVERT(VARCHAR(50), b.finaled, 101) AS DATE4,   
          CONVERT(VARCHAR(50), b.issued, 101) AS DATE5, CONVERT(VARCHAR(50), b.other_date1, 101) AS DATE6, '''' AS DATE7, '''' AS DATE8 ,    
          '''' as PARENT_GENERIC1_ACTIVITYNO, '''' as ISSUE_PREFIX, '''' as LICENSE_NO, '''' as PARENT_LICENSE_NO,  CONVERT(VARCHAR(50), b.applied, 101) AS SORT_CRITERIA1,   
          a.site_addr as SORT_CRITERIA2, '''' as SORT_CRITERIA3, '''' as PARENTGEOTYPE  
         from permit_parcels a inner join permit_main b on a.permit_no = b.permit_no where a.loc_recordid = ''' + @LocRecordID + ''''  
  
      --print @permitQuery;  
     END  
  
     declare @projectQuery nvarchar(MAX)= ''  
     declare @projectMaxLimit int = -1;  
     declare @projectCount int = (select count('Tgroup') from #tmpSiteTreeRecords where Tgroup = 'PROJECT');  
       
        if @projectCount < @maxLimit  
      BEGIN  
       SET @projectMaxLimit = (@maxLimit - @projectCount);  
  
       SET @projectQuery = N'Select TOP (' + cast(@projectMaxLimit as nvarchar(MAX)) + ') a.project_no as ACTIVITYNO, b.projecttype as ACTIVITYTYPE, b.PROJECTSUBTYPE as ACTIVITYSUBTYPE, b.DESCRIPTION as DESCRIPTION, a.SITE_APN, b.RECORDID, b.LOC_RECORDID,  b.STATUS, ''PROJECT'' as Tgroup, '''' as GEOTYPE,   
        '''' as SITE_ALTERNATE_ID, '''' as PARENT_PROJECT_NO, '''' as PARENT_PERMIT_NO, '''' as PARENT_SITE_APN,  a.site_addr, '''' as PARENT_RECORDID, '''' as PARENT_BUS_LIC_NO,   
        '''' as PARENT_AEC_ST_LIC_NO, '''' as DATEPROPERTYLIST, CONVERT(VARCHAR(50), b.applied, 101) AS DATE1,   
        CONVERT(VARCHAR(50), b.approved, 101) AS DATE2, CONVERT(VARCHAR(50), b.closed, 101) AS DATE3, CONVERT(VARCHAR(50), b.expired, 101) AS DATE4,   
        CONVERT(VARCHAR(50), other_date1, 101) AS DATE5, CONVERT(VARCHAR(50), b.status_date, 101) AS DATE6, '''' AS DATE7, '''' AS DATE8 ,    
        '''' as PARENT_GENERIC1_ACTIVITYNO, '''' as ISSUE_PREFIX, '''' as LICENSE_NO, '''' as PARENT_LICENSE_NO,  CONVERT(VARCHAR(50), b.applied, 101) AS SORT_CRITERIA1,   
        a.site_addr as SORT_CRITERIA2, '''' as SORT_CRITERIA3, '''' as PARENTGEOTYPE  
        from project_parcels a inner join project_main b on a.project_no = b.project_no where a.loc_recordid = ''' + @LocRecordID + ''''   
  
        --print @projectQuery;  
      END  
        
  
      declare @caseQuery nvarchar(MAX)= ''  
      declare @caseMaxLimit int = -1;  
      declare @caseCount int = (select count('Tgroup') from #tmpSiteTreeRecords where Tgroup = 'CASE');  
       
      if @caseCount < @maxLimit  
       BEGIN  
        SET @caseMaxLimit = (@maxLimit - @caseCount);  
        SET @caseQuery = N'Select TOP (' + cast(@caseMaxLimit as nvarchar(MAX)) + ') a.case_no as ACTIVITYNO, b.casetype as ACTIVITYTYPE, b.CaseSubType as ACTIVITYSUBTYPE, b.DESCRIPTION as DESCRIPTION, a.SITE_APN, b.RECORDID, b.LOC_RECORDID,  b.STATUS, ''CASE'' as Tgroup, 
'''' as GEOTYPE,   
          '''' as SITE_ALTERNATE_ID, '''' as PARENT_PROJECT_NO, '''' as PARENT_PERMIT_NO, '''' as PARENT_SITE_APN,  a.site_addr, '''' as PARENT_RECORDID, '''' as PARENT_BUS_LIC_NO,   
          '''' as PARENT_AEC_ST_LIC_NO, '''' as DATEPROPERTYLIST, CONVERT(VARCHAR(50), b.started, 101) AS DATE1,   
          CONVERT(VARCHAR(50), b.followup, 101) AS DATE2, CONVERT(VARCHAR(50), b.closed, 101) AS DATE3, CONVERT(VARCHAR(50), b.other_date1, 101) AS DATE4,   
          CONVERT(VARCHAR(50), b.started, 101) AS DATE5, '''' AS DATE6, '''' AS DATE7, '''' AS DATE8,    
          '''' as PARENT_GENERIC1_ACTIVITYNO, '''' as ISSUE_PREFIX, '''' as LICENSE_NO, '''' as PARENT_LICENSE_NO,  CONVERT(VARCHAR(50), b.started, 101) AS SORT_CRITERIA1,   
          a.site_addr as SORT_CRITERIA2, '''' as SORT_CRITERIA3, '''' as PARENTGEOTYPE  
          from case_parcels a inner join case_main b on a.case_no = b.case_no where a.loc_recordid = ''' +@LocRecordID + ''''  
       END  
  
  
       declare @licenseQuery nvarchar(MAX)= ''  
       declare @licenseMaxLimit int = -1;  
       declare @licenseCount int = (select count('Tgroup') from #tmpSiteTreeRecords where Tgroup = 'LICENSE2');  
       
       if @licenseCount < @maxLimit  
        BEGIN  
         SET @licenseMaxLimit = (@maxLimit - @licenseCount);  
  
         SET @licenseQuery = N'Select TOP (' + cast(@licenseMaxLimit as nvarchar(MAX)) + ') a.license_no as ACTIVITYNO, b.license_type as ACTIVITYTYPE, b.LICENSE_SUBTYPE as ACTIVITYSUBTYPE, '''' as DESCRIPTION, a.SITE_APN, b.RECORDID, b.LOC_RECORDID,  b.STATUS, ''LICENSE'' as Tgroup, '''' as GEOTYPE,   
          '''' as SITE_ALTERNATE_ID, '''' as PARENT_PROJECT_NO, '''' as PARENT_PERMIT_NO, '''' as PARENT_SITE_APN,  a.site_addr, '''' as PARENT_RECORDID, '''' as PARENT_BUS_LIC_NO,   
          '''' as PARENT_AEC_ST_LIC_NO, '''' as DATEPROPERTYLIST, CONVERT(VARCHAR(50), b.issued, 101) AS DATE1,   
          CONVERT(VARCHAR(50), b.expired, 101) AS DATE2, CONVERT(VARCHAR(50), b.st_lic_iss, 101) AS DATE3, CONVERT(VARCHAR(50), b.st_lic_exp, 101) AS DATE4,   
          CONVERT(VARCHAR(50), b.liab_iss, 101) AS DATE5, CONVERT(VARCHAR(50), b.liab_exp, 101) AS DATE6, CONVERT(VARCHAR(50), b.liab_exp, 101) AS DATE7, '''' AS DATE8 ,    
          '''' as PARENT_GENERIC1_ACTIVITYNO, '''' as ISSUE_PREFIX, '''' as LICENSE_NO, '''' as PARENT_LICENSE_NO,  CONVERT(VARCHAR(50), b.issued, 101) AS SORT_CRITERIA1,   
          a.site_addr as SORT_CRITERIA2, '''' as SORT_CRITERIA3, '''' as PARENTGEOTYPE  
          from license2_parcels a inner join license2_main b on a.license_no = b.license_no where a.loc_recordid = ''' +@LocRecordID + ''''  
        END  
         
  
     declare @s nvarchar(MAX)  = N''   
     if @permitQuery <> ''   
      SET @s = @s + @permitQuery;  
  
     if @projectQuery <> '' and @s = ''  
      SET @s = @s + @projectQuery;  
     else if @projectQuery <> ''  
      SET @s = @s + CAST(' UNION ' As nvarchar(MAX)) + @projectQuery;  
  
     if @caseQuery <> '' and @s = ''  
      SET @s = @s + @caseQuery;  
     else if @caseQuery <> ''  
      SET @s = @s + CAST(' UNION ' As nvarchar(MAX)) + @caseQuery;  
  
     if @licenseQuery <> '' and @s = ''  
      SET @s = @s + @licenseQuery;  
     else if @licenseQuery <> ''  
      SET @s = @s + CAST(' UNION ' As nvarchar(MAX)) + @licenseQuery;       
      
  
     print @s;    
  
     if @s <> ''  
      insert into #tmpSiteTreeRecords EXECUTE sp_executesql @s  
  
  
    END  
  else   
   BEGIN   
     insert into #tmpSiteTreeRecords select * from tvw_GblTreeActivities   
      where RECORDID = @LocRecordID  
      OR LOC_RECORDID = @LocRecordID  
      OR PARENT_RECORDID = @LocRecordID                      
      ORDER BY sort_criteria1 desc, site_addr  asc  
  
     insert into #tmpSiteTreeRecords  
      Select a.permit_no as ACTIVITYNO, b.permittype as ACTIVITYTYPE, '' as ACTIVITYSUBTYPE, '' as DESCRIPTION, a.SITE_APN, b.RECORDID, b.LOC_RECORDID,  b.STATUS, 'PERMIT' as Tgroup, '' as GEOTYPE,   
       '' as SITE_ALTERNATE_ID, '' as PARENT_PROJECT_NO, '' as PARENT_PERMIT_NO, '' as PARENT_SITE_APN,  a.site_addr, '' as PARENT_RECORDID, '' as PARENT_BUS_LIC_NO,   
       '' as PARENT_AEC_ST_LIC_NO, '' as DATEPROPERTYLIST, CONVERT(VARCHAR(50), b.applied, 101) AS DATE1,   
       CONVERT(VARCHAR(50), b.approved, 101) AS DATE2, CONVERT(VARCHAR(50), b.expired, 101) AS DATE3, CONVERT(VARCHAR(50), b.finaled, 101) AS DATE4,   
       CONVERT(VARCHAR(50), b.issued, 101) AS DATE5, CONVERT(VARCHAR(50), b.other_date1, 101) AS DATE6, '' AS DATE7, '' AS DATE8 ,    
       '' as PARENT_GENERIC1_ACTIVITYNO, '' as ISSUE_PREFIX, '' as LICENSE_NO, '' as PARENT_LICENSE_NO,  CONVERT(VARCHAR(50), b.applied, 101) AS SORT_CRITERIA1,   
       a.site_addr as SORT_CRITERIA2, '' as SORT_CRITERIA3, '' as PARENTGEOTYPE  
      from permit_parcels a inner join permit_main b on a.permit_no = b.permit_no where a.loc_recordid = @LocRecordID  
  
      Union  
     
       Select a.project_no as ACTIVITYNO, b.projecttype as ACTIVITYTYPE, '' as ACTIVITYSUBTYPE, '' as DESCRIPTION, a.SITE_APN, b.RECORDID, b.LOC_RECORDID,  b.STATUS, 'PROJECT' as Tgroup, '' as GEOTYPE,   
       '' as SITE_ALTERNATE_ID, '' as PARENT_PROJECT_NO, '' as PARENT_PERMIT_NO, '' as PARENT_SITE_APN,  a.site_addr, '' as PARENT_RECORDID, '' as PARENT_BUS_LIC_NO,   
       '' as PARENT_AEC_ST_LIC_NO, '' as DATEPROPERTYLIST, CONVERT(VARCHAR(50), b.applied, 101) AS DATE1,   
       CONVERT(VARCHAR(50), b.approved, 101) AS DATE2, CONVERT(VARCHAR(50), b.closed, 101) AS DATE3, CONVERT(VARCHAR(50), b.expired, 101) AS DATE4,   
       CONVERT(VARCHAR(50), other_date1, 101) AS DATE5, CONVERT(VARCHAR(50), b.status_date, 101) AS DATE6, '' AS DATE7, '' AS DATE8 ,    
       '' as PARENT_GENERIC1_ACTIVITYNO, '' as ISSUE_PREFIX, '' as LICENSE_NO, '' as PARENT_LICENSE_NO,  CONVERT(VARCHAR(50), b.applied, 101) AS SORT_CRITERIA1,   
       a.site_addr as SORT_CRITERIA2, '' as SORT_CRITERIA3, '' as PARENTGEOTYPE  
       from project_parcels a inner join project_main b on a.project_no = b.project_no where a.loc_recordid = @LocRecordID  
  
      Union  
  
       Select a.case_no as ACTIVITYNO, b.casetype as ACTIVITYTYPE, '' as ACTIVITYSUBTYPE, '' as DESCRIPTION, a.SITE_APN, b.RECORDID, b.LOC_RECORDID,  b.STATUS, 'CASE' as Tgroup, '' as GEOTYPE,   
       '' as SITE_ALTERNATE_ID, '' as PARENT_PROJECT_NO, '' as PARENT_PERMIT_NO, '' as PARENT_SITE_APN,  a.site_addr, '' as PARENT_RECORDID, '' as PARENT_BUS_LIC_NO,   
       '' as PARENT_AEC_ST_LIC_NO, '' as DATEPROPERTYLIST, CONVERT(VARCHAR(50), b.started, 101) AS DATE1,   
       CONVERT(VARCHAR(50), b.followup, 101) AS DATE2, CONVERT(VARCHAR(50), b.closed, 101) AS DATE3, CONVERT(VARCHAR(50), b.other_date1, 101) AS DATE4,   
       CONVERT(VARCHAR(50), b.started, 101) AS DATE5, '' AS DATE6, '' AS DATE7, '' AS DATE8,    
       '' as PARENT_GENERIC1_ACTIVITYNO, '' as ISSUE_PREFIX, '' as LICENSE_NO, '' as PARENT_LICENSE_NO,  CONVERT(VARCHAR(50), b.started, 101) AS SORT_CRITERIA1,   
       a.site_addr as SORT_CRITERIA2, '' as SORT_CRITERIA3, '' as PARENTGEOTYPE  
       from case_parcels a inner join case_main b on a.case_no = b.case_no where a.loc_recordid = @LocRecordID  
  
      Union  
  
       Select a.license_no as ACTIVITYNO, b.license_type as ACTIVITYTYPE, '' as ACTIVITYSUBTYPE, '' as DESCRIPTION, a.SITE_APN, b.RECORDID, b.LOC_RECORDID,  b.STATUS, 'LICENSE' as Tgroup, '' as GEOTYPE,   
       '' as SITE_ALTERNATE_ID, '' as PARENT_PROJECT_NO, '' as PARENT_PERMIT_NO, '' as PARENT_SITE_APN,  a.site_addr, '' as PARENT_RECORDID, '' as PARENT_BUS_LIC_NO,   
       '' as PARENT_AEC_ST_LIC_NO, '' as DATEPROPERTYLIST, CONVERT(VARCHAR(50), b.issued, 101) AS DATE1,   
       CONVERT(VARCHAR(50), b.expired, 101) AS DATE2, CONVERT(VARCHAR(50), b.st_lic_iss, 101) AS DATE3, CONVERT(VARCHAR(50), b.st_lic_exp, 101) AS DATE4,   
       CONVERT(VARCHAR(50), b.liab_iss, 101) AS DATE5, CONVERT(VARCHAR(50), b.liab_exp, 101) AS DATE6, CONVERT(VARCHAR(50), b.liab_exp, 101) AS DATE7, '' AS DATE8 ,    
       '' as PARENT_GENERIC1_ACTIVITYNO, '' as ISSUE_PREFIX, '' as LICENSE_NO, '' as PARENT_LICENSE_NO,  CONVERT(VARCHAR(50), b.issued, 101) AS SORT_CRITERIA1,   
       a.site_addr as SORT_CRITERIA2, '' as SORT_CRITERIA3, '' as PARENTGEOTYPE  
       from license2_parcels a inner join license2_main b on a.license_no = b.license_no where a.loc_recordid = @LocRecordID  
        
    END  
   
   select * from #tmpSiteTreeRecords ORDER BY sort_criteria1 desc, site_addr asc  
 END  
  
 END    

GO
