USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ExportInspectionControl]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_ExportInspectionControl]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ExportInspectionControl]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  
					  					  					  /*
tsp_ExportInspectionControl @DBName='DEV', @FilePath='C:\CRW\iTrakitExports\', @FileName='InspectionControlExport'

select * from Prmry_InspectionControl

*/

CREATE Procedure [dbo].[tsp_ExportInspectionControl](
@DBName Varchar(50),
@FilePath Varchar(1000),
@FileName Varchar(150),
@ToggleXP_CMDShell INT = 1
)

As

SET NOCOUNT ON

Declare @SQL Varchar(8000)

If @ToggleXP_CMDShell Is Null Set @ToggleXP_CMDShell = 1

IF EXISTS (SELECT * FROM tempdb.sys.objects WHERE object_id = OBJECT_ID(N'tempdb..InspectionControl') AND type in (N'U'))
	Drop Table tempdb..InspectionControl

Create Table tempdb..InspectionControl([Category] Varchar(200)
      ,[GroupName] Varchar(200)
      ,[Field01] Varchar(200)
      ,[Field02] Varchar(200)
      ,[Field03] Varchar(200)
      ,[Field04] Varchar(200)
      ,[Field05] Varchar(200)
      ,[Field06] Varchar(2000)
      ,[Field07] Varchar(200)
      ,[Field08] Varchar(200)
      ,[RECORDID] Varchar(200)
      ,[ORDERID] Varchar(200))


Insert Into tempdb..InspectionControl(Category,GroupName,Field01,Field02,Field03,Field04,Field05,Field06,Field07,Field08,RecordID,OrderID)
Values('Category','GroupName','Field01','Field02','Field03','Field04','Field05','Field06','Field07','Field08','RecordID','OrderID')

Set @SQL = '
Insert Into tempdb..InspectionControl([Category],[GroupName],[Field01],[Field02],[Field03],[Field04],[Field05],[Field06],[Field07],[Field08],[RECORDID],[ORDERID])
Select 
Category=CASE WHEN Category = '''' THEN Null ELSE REPLACE(REPLACE(REPLACE(Category,CHAR(9),''''),CHAR(10),''''),CHAR(13),'''') END,
GroupName=CASE WHEN GroupName = '''' THEN Null ELSE GroupName END,
Field01=CASE WHEN Field01 = '''' THEN Null ELSE REPLACE(REPLACE(REPLACE(Field01,CHAR(9),''''),CHAR(10),''''),CHAR(13),'''') END,
Field02=CASE WHEN Field02 = '''' THEN Null ELSE REPLACE(REPLACE(REPLACE(Field02,CHAR(9),''''),CHAR(10),''''),CHAR(13),'''') END,
Field03=CASE WHEN Field03 = '''' THEN Null ELSE REPLACE(REPLACE(REPLACE(Field03,CHAR(9),''''),CHAR(10),''''),CHAR(13),'''') END,
Field04=CASE WHEN Field04 = '''' THEN Null ELSE REPLACE(REPLACE(REPLACE(Field04,CHAR(9),''''),CHAR(10),''''),CHAR(13),'''') END,
Field05=CASE WHEN Field05 = '''' THEN Null ELSE REPLACE(REPLACE(REPLACE(Field05,CHAR(9),''''),CHAR(10),''''),CHAR(13),'''') END,
Field06=CASE WHEN Field06 = '''' THEN Null ELSE REPLACE(REPLACE(REPLACE(Field06,CHAR(9),''''),CHAR(10),''''),CHAR(13),'''') END,
Field07=CASE WHEN Field07 = '''' THEN Null ELSE REPLACE(REPLACE(REPLACE(Field07,CHAR(9),''''),CHAR(10),''''),CHAR(13),'''') END,
Field08=CASE WHEN Field08 = '''' THEN Null ELSE REPLACE(REPLACE(REPLACE(Field08,CHAR(9),''''),CHAR(10),''''),CHAR(13),'''') END,
RECORDID=CASE WHEN RECORDID = '''' THEN Null ELSE RECORDID END,
ORDERID=CASE WHEN [ID] Is Null THEN ''999'' ELSE Convert(Varchar,[ID]) END
From ' + @DBName + '..Prmry_InspectionControl
ORDER BY Category,GroupName,Field01'
--Print(@SQL)
Exec(@SQL)

Select * From tempdb..InspectionControl


Declare @vDate Varchar(100)
Declare @OutputFileName Varchar(1000)

Set @vDate = Replace(Convert(Varchar,GetDate(),102),'.','_')
Print(@vDate)

--Set @OutputFileName = '"' + @FilePath + @FileName + '_' + @vDate + '.csv"'
Set @OutputFileName = '"' + @FilePath + @FileName + '.csv"'


If @ToggleXP_CMDShell = 1
 Begin
	--Enable xp_cmdshell
	Set @SQL = '
	EXEC sp_configure ''show advanced options'', 1
	RECONFIGURE
	EXEC sp_configure ''xp_cmdshell'', 1
	RECONFIGURE
	'
	Print(@SQL)
	Exec(@SQL)
 End

select @sql = 'bcp "select * from tempdb..InspectionControl" queryout ' + @OutputFileName + ' -c -t"|" -T -S'
+ @@servername
--Print(@SQL)
exec master..xp_cmdshell @sql

Drop Table tempdb..InspectionControl

Set @SQL = '
DELETE ' + @DBName + '.dbo.iTrakitFeeds Where [FileName] = ''' + @FileName + '.csv''

INSERT INTO ' + @DBName + '.dbo.iTrakitFeeds([filename],[data])
SELECT ''' + @FileName + '.csv'', * FROM OPENROWSET(
  BULK N''' + REPLACE(@OutputFileName,'"','') + ''', SINGLE_BLOB
) bt'
Print(@SQL)
Exec(@SQL)

If @ToggleXP_CMDShell = 1
 Begin
	-- Disable xp_cmdshell
	Set @SQL = '
	EXEC sp_configure ''xp_cmdshell'', 0
	RECONFIGURE
	EXEC sp_configure ''show advanced options'', 0
	RECONFIGURE
	'
	Print(@SQL)
	Exec(@SQL)
 End







GO
