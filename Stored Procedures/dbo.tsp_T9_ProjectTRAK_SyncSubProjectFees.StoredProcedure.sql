USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ProjectTRAK_SyncSubProjectFees]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_ProjectTRAK_SyncSubProjectFees]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ProjectTRAK_SyncSubProjectFees]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  -- =====================================================================
-- Revision history: 
--	WL - 12/11/14 - Created.
-- ===================================================================== 
CREATE PROCEDURE [dbo].[tsp_T9_ProjectTRAK_SyncSubProjectFees] (
	@ParentProjectNO VARCHAR(50)
)

AS
	SET NOCOUNT ON;

		-- update fee totals for non-formulas
	;WITH CTE_FeeTotalsA (Total, Code)
	AS 
	(
		SELECT SUM(AMOUNT), Code
		FROM Project_Fees
		WHERE PROJECT_NO = @ParentProjectNO
			AND ISNUMERIC(Formula) > 0
		GROUP BY Code
	)
	UPDATE PROJECT_Fees
	SET AMOUNT = Total, Formula = Total
	FROM CTE_FeeTotalsA
	INNER JOIN Project_Fees ON CTE_FeeTotalsA.Code = Project_Fees.CODE
	WHERE Project_Fees.PROJECT_NO = @ParentProjectNO

    -- remove duplicate fee
	;WITH CTE_Fees (Code, DuplicateCount)
	AS 
	(
		SELECT Code, ROW_NUMBER() OVER (PARTITION BY Code ORDER BY Code) AS DuplicateCount
		FROM Project_Fees
		WHERE PROJECT_NO = @ParentProjectNO
	)
	DELETE
	FROM CTE_Fees
	WHERE DuplicateCount > 1	
			
	-- update fee totals for non-formulas
	;WITH CTE_SubFeeTotals (Total, Code, Item)
	AS 
	(
		SELECT SUM(AMOUNT), Code, Item
		FROM Project_SubFees
		WHERE PROJECT_NO = @ParentProjectNO
			AND ISNUMERIC(Formula) > 0
		GROUP BY Code, Item
	)
	UPDATE PROJECT_SubFees
	SET AMOUNT = Total, Formula = Total
	FROM CTE_SubFeeTotals
	INNER JOIN Project_SubFees ON CTE_SubFeeTotals.Code = Project_SubFees.CODE AND CTE_SubFeeTotals.Item = Project_SubFees.Item 
	WHERE Project_SubFees.PROJECT_NO = @ParentProjectNO
		
	-- remove duplicate sub fee
	;WITH CTE_SubFees (Code, Item, DuplicateCount)
	AS 
	(
		SELECT Code, Item, ROW_NUMBER() OVER (PARTITION BY Code, Item ORDER BY Code, Item) AS DuplicateCount
		FROM Project_SubFees
		WHERE PROJECT_NO = @ParentProjectNO
	)
	DELETE
	FROM CTE_SubFees
	WHERE DuplicateCount > 1

	-- sync up the record ids
	UPDATE Project_SubFees
	SET Project_SubFees.PARENTID = Project_Fees.RECORDID
	FROM Project_SubFees
	INNER JOIN Project_Fees ON Project_Fees.CODE = Project_SubFees.CODE AND Project_SubFees.PROJECT_NO = Project_Fees.PROJECT_NO
	WHERE Project_SubFees.PROJECT_NO = @ParentProjectNO
	
	-- update fee totals for non-formulas where fees are itemized
	;WITH CTE_SubFeeTotals (Total, Code)
	AS 
	(
		SELECT SUM(AMOUNT), Code
		FROM Project_SubFees
		WHERE PROJECT_NO = @ParentProjectNO
			AND ISNUMERIC(Formula) > 0
		GROUP BY Code
	)
	UPDATE PROJECT_Fees
	SET Amount = Total
	FROM CTE_SubFeeTotals
	INNER JOIN Project_Fees ON CTE_SubFeeTotals.Code = Project_Fees.CODE
	WHERE Project_Fees.PROJECT_NO = @ParentProjectNO and PROJECT_Fees.FORMULA = '[ITEMIZE]'






GO
