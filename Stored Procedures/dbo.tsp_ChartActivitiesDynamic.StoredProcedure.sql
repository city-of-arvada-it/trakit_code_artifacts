USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ChartActivitiesDynamic]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_ChartActivitiesDynamic]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ChartActivitiesDynamic]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  					  
					  					  					  
/*

tsp_ChartActivitiesDynamic
@ActivityTableName='Permit_Main'
,@DateType='APPLIED'
,@vStartDate='1/1/12'
,@vEndDate='6/30/12'
,@GroupByClauseFieldName='Status'
,@WhereClauseField1Name='Status'
,@WhereClauseField1Value='Approved'
--,@WhereClauseField2Name='Site_Subdivision'
--,@WhereClauseField2Value='Quail Crossing'
,@WhereClauseField3Name='APPLIED_BY'
,@WhereClauseField3Value='MDP,RJE'

*/


CREATE Procedure [dbo].[tsp_ChartActivitiesDynamic](
@ActivityTableName Varchar(200),
@DateType Varchar(100) = Null,
@vStartDate Varchar(100) = Null,
@vEndDate Varchar(100) = Null,
@GroupByClauseFieldName Varchar(100),
@WhereClauseField1Name Varchar(100) = Null,
@WhereClauseField1Value Varchar(1000) = Null,
@WhereClauseField2Name Varchar(100) = Null,
@WhereClauseField2Value Varchar(1000) = Null,
@WhereClauseField3Name Varchar(100) = Null,
@WhereClauseField3Value Varchar(1000) = Null,
@WhereClauseField4Name Varchar(100) = Null,
@WhereClauseField4Value Varchar(1000) = Null,
@WhereClauseField5Name Varchar(100) = Null,
@WhereClauseField5Value Varchar(1000) = Null
)


As

Declare @SQL Varchar(8000)
Declare @WHERE Varchar(5000)
Declare @StartDate Datetime 
Declare @EndDate Datetime


Set @WHERE = ''
Set @StartDate  = Null
Set @EndDate  = Null
If @vStartDate = '' Set @vStartDate = Null
If @vStartDate = '1/1/0001 12:00:00 AM' Set @vStartDate = Null
If @vEndDate = '' Set @vEndDate = Null
If @vEndDate = '1/1/0001 12:00:00 AM' Set @vEndDate = Null

If @vStartDate Is Null Set @vStartDate = Convert(Varchar,GetDate(),101)
If @vEndDate Is Null Set @vEndDate = Convert(Varchar,GetDate(),101)

Set @StartDate = CONVERT(Datetime,@vStartDate)
Set @EndDate = CONVERT(Datetime,DATEADD(dd,1,@vEndDate))


If @DateType = '' Set @DateType = Null
If @vStartDate = '' Set @vStartDate = Null
If @vEndDate = '' Set @vEndDate = Null
If @WhereClauseField1Name = '' Set @WhereClauseField1Name = Null
If @WhereClauseField1Value = '' Set @WhereClauseField1Value = Null
If @WhereClauseField2Name = '' Set @WhereClauseField2Name = Null
If @WhereClauseField2Value = '' Set @WhereClauseField2Value = Null
If @WhereClauseField3Name = '' Set @WhereClauseField3Name = Null
If @WhereClauseField3Value = '' Set @WhereClauseField3Value = Null
If @WhereClauseField4Name = '' Set @WhereClauseField4Name = Null
If @WhereClauseField4Value = '' Set @WhereClauseField4Value = Null
If @WhereClauseField5Name = '' Set @WhereClauseField5Name = Null
If @WhereClauseField5Value = '' Set @WhereClauseField5Value = Null


Set @StartDate = CONVERT(Datetime,@vStartDate)
Set @EndDate = CONVERT(Datetime,DATEADD(dd,1,@vEndDate))
Print(@StartDate)
Print(@EndDate)

If @DateType Is Not Null AND @StartDate Is Not Null AND @EndDate Is Not Null
 Begin
	Set @WHERE = @WHERE + CHAR(10) + ' AND ' + @DateType + ' >= Convert(Datetime,''' + Convert(Varchar,@StartDate) + ''')'
	Set @WHERE = @WHERE + CHAR(10) + ' AND ' + @DateType + ' < Convert(Datetime,''' + Convert(Varchar,@EndDate) + ''')'
 End

If @WhereClauseField1Name Is Not Null AND @WhereClauseField1Value Is Not Null
	Set @WHERE = @WHERE + CHAR(10) + ' AND ' + @WhereClauseField1Name + ' IN (''' + REPLACE(@WhereClauseField1Value,',',''',''') + ''')'

If @WhereClauseField2Name Is Not Null AND @WhereClauseField2Value Is Not Null
	Set @WHERE = @WHERE + CHAR(10) + ' AND ' + @WhereClauseField2Name + ' IN (''' + REPLACE(@WhereClauseField2Value,',',''',''') + ''')'

If @WhereClauseField3Name Is Not Null AND @WhereClauseField3Value Is Not Null
	Set @WHERE = @WHERE + CHAR(10) + ' AND ' + @WhereClauseField3Name + ' IN (''' + REPLACE(@WhereClauseField3Value,',',''',''') + ''')'
	
If @WhereClauseField4Name Is Not Null AND @WhereClauseField4Value Is Not Null
	Set @WHERE = @WHERE + CHAR(10) + ' AND ' + @WhereClauseField4Name + ' IN (''' + REPLACE(@WhereClauseField4Value,',',''',''') + ''')'
	
If @WhereClauseField5Name Is Not Null AND @WhereClauseField5Value Is Not Null
	Set @WHERE = @WHERE + CHAR(10) + ' AND ' + @WhereClauseField5Name + ' IN (''' + REPLACE(@WhereClauseField5Value,',',''',''') + ''')'
	

--Print(@WHERE)


Set @SQL = '
Select ' + @GroupByClauseFieldName + ',Total=Count(*)
From ' + @ActivityTableName + ' 
WHERE 1 = 1 ' + @WHERE + '
GROUP BY ' + @GroupByClauseFieldName 


Print(@SQL)
Exec(@SQL)









GO
