USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_BatchProcess_GeoRestrictions]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_BatchProcess_GeoRestrictions]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_BatchProcess_GeoRestrictions]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  



                                       

create PROCEDURE [dbo].[tsp_T9_BatchProcess_GeoRestrictions] 
              @ListRestrictions as UpdateFields readonly,
			  @ListUpdateFields as UpdateFields readonly,
              @ListTableAndRecords as MainTableRecords readonly
              
AS
BEGIN

       Declare @SQL Varchar(8000)
       Declare @TableName varchar(50) 
       Declare @ColumnName varchar(50) 
       Declare @ColumnValue varchar(255)
       Declare @RowNum int
       Declare @RowId Int
       Declare @UserID as varchar(10)
       Declare @TblName as varchar(100)
	   Declare @KeyField varchar(50)  
       Declare @Contact as varchar(100)
       Declare @GroupName as varchar(255)
	   Declare @Multi as varchar(10)

--Drop Tables
	   IF object_id('tempdb..#GeoRestrictions') is not null drop table #GeoRestrictions
	   IF object_id('tempdb..#GeoRestrictions2') is not null drop table #GeoRestrictions2
       IF object_id('tempdb..#ForInsertIntoPrmryAuditTrail') is not null drop table #ForInsertIntoPrmryAuditTrail
       IF object_id('tempdb..#ListConditionTypes') is not null drop table #ListConditionTypes
       IF object_id('tempdb..#ListUpdateFields') is not null drop table #ListUpdateFields
       IF object_id('tempdb..#MainTableRec') is not null  drop table #MainTableRec

       
	     --Create tables, checking if pref for single or multi
	   set @Multi = (Select value1 from Prmry_Preferences where item = 'LANDTRAK_MULTIRESTRICTIONS')
	  

		   CREATE TABLE #GeoRestrictions(
				DATE_ADDED datetime,
				RESTRICTION_NOTES varchar(2000),
				RESTRICTION_TYPE varchar(60),
				USER_ADDED varchar(4),
				LOC_RECORDID varchar(30),
				RECORDID varchar(30),
				SITE_APN varchar(50),
				ForRecID int identity)


      --Create table to be used for insert into Audit
       Create Table #ForInsertIntoPrmryAuditTrail(
              CURRENT_VALUE varchar(2000),
              FIELD_NAME varchar(50),
              ORIGINAL_VALUE varchar(2000),
              PRIMARY_KEY_VALUE varchar(50),
              SQL_ACTION varchar(1),
              TABLE_NAME varchar(50),
              USER_ID varchar(4), 
              TRANSACTION_DATETIME datetime)

       --Populate table variables
       select * into #ListUpdateFields from @ListUpdateFields

       select * into #MainTableRec from @ListTableAndRecords

       -- Start populating table (this should create all combinations for insert)
	   set @TableName = (select distinct tblName from #MainTableRec)

		--use function to get key field
		select @KeyField = dbo.tfn_ActivityNoField(@TableName)

	  Set @sql = 'Insert into #GeoRestrictions (LOC_RECORDID) select distinct loc_recordid from '
	  Set @sql = @sql + @TableName
	  Set @sql = @sql + ' where  '
	  Set @sql = @sql + @KeyField
	  Set @sql = @sql +' in (select ActivityNo from #MainTableRec)'
	  exec(@sql)

	   --Update temp table based
       select @RowId=MAX(RowID) FROM #ListUpdateFields     
       Select @RowNum = Count(*) From #ListUpdateFields      
       WHILE @RowNum > 0                          
       BEGIN   

              select @ColumnName = ColumnName from #ListUpdateFields where RowID= @RowId    
              select @ColumnValue = Value from #ListUpdateFields where RowID= @RowId  

              if len(ltrim(rtrim(@ColumnValue))) > 0 and @ColumnName ='UserID'
                     Begin
                           set @UserID = @ColumnValue
                     End

              if len(ltrim(rtrim(@ColumnValue))) > 0 and @ColumnName ='GroupName'
                     Begin
                           set @GroupName = @ColumnValue
                     End 

				if len(ltrim(rtrim(@ColumnValue))) > 0 and @ColumnName ='TblName'
                     Begin
                           set @TblName = @ColumnValue 
                     End 
              if len(ltrim(rtrim(@ColumnValue))) > 0 and @ColumnName not in ('UserID', 'GroupName','TblName','CONTACT')
                     Begin
                           set @SQL = 'Update #GeoRestrictions set ' + @ColumnName +  ' = ''' + @ColumnValue + ''''
                           exec(@sql)
		             End 

                                  
              select top 1 @RowId=RowID from #ListUpdateFields where RowID < @RowId order by RowID desc
              set @RowNum = @RowNum - 1                             
       
       END

update #GeoRestrictions set RECORDID ='BPRO:' + right(('00' + str(DATEPART(yy, GETDATE()))),1)
+ right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)
+ right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
+ right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)
+ right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
+ right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)
+ right(('000000'+ ltrim(str(ForRecID))),6);

update #GeoRestrictions set SITE_APN = geo_ownership.Site_apn from geo_ownership where geo_ownership.RECORDID = #GeoRestrictions.loc_recordid

delete from #GeoRestrictions where LOC_RECORDID is null


	if upper(@Multi) = 'YES'
	Begin

		update #GeoRestrictions set DATE_ADDED = GETDATE(),USER_ADDED = @UserID

		insert into Geo_Restrictions2(DATE_ADDED,RESTRICTION_NOTES,RESTRICTION_TYPE,USER_ADDED,LOC_RECORDID,RECORDID,				SITE_APN) select DATE_ADDED,RESTRICTION_NOTES,RESTRICTION_TYPE,USER_ADDED,LOC_RECORDID,RECORDID,SITE_APN
		from #GeoRestrictions

	end

	if upper(@Multi) = 'NO'
	Begin

		insert into Geo_Restrictions (SITE_APN,RESTRICTION_TYPE ,RESTRICTION_NOTES,LOC_RECORDID,RECORDID)
		select SITE_APN,RESTRICTION_TYPE ,RESTRICTION_NOTES,LOC_RECORDID,RECORDID
			from #GeoRestrictions
	End

       --insert into #ForInsertIntoPrmryAuditTrail(TABLE_NAME,PRIMARY_KEY_VALUE ) select tblName, ActivityNo from #MainTableRec

       --insert into Prmry_AuditTrail (CURRENT_VALUE, PRIMARY_KEY_VALUE, SQL_ACTION, TABLE_NAME,TRANSACTION_DATETIME, USER_ID) select @SQL2, PRIMARY_KEY_VALUE, 'U', @TblName, getdate(), @UserID from #ForInsertIntoPrmryAuditTrail


END








GO
