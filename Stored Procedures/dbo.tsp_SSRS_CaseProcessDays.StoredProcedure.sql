USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_SSRS_CaseProcessDays]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_SSRS_CaseProcessDays]
GO
/****** Object:  StoredProcedure [dbo].[tsp_SSRS_CaseProcessDays]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  					  
					  					  					  
-- =============================================
-- Author:	Dan Haynes
-- Create date: 1/19/2012
-- Description:	Calculates Days Between Dates in CodeTRAK Process
-- =============================================
CREATE PROCEDURE [dbo].[tsp_SSRS_CaseProcessDays] 
		@CASE_NO varchar(15)
AS
BEGIN

--Create Temp Table

--RB Check if table exists before creating it
   IF OBJECT_ID('tempdb..#table1') IS NOT NULL  
	drop table #table1

create table #table1
	(GROUPNAME varchar(30)
	,SORT_ORDER int
	,[STARTED] datetime
	,CLOSED datetime
	,[DAYS] int
	,TOTAL int
	,OPENED int
	,COMPLETED int);

--Add Groups
insert into #table1 (GROUPNAME, SORT_ORDER) values('DAYS', '1');
insert into #table1 (GROUPNAME, SORT_ORDER) values('INSPECTIONS', '2');
insert into #table1 (GROUPNAME, SORT_ORDER) values('VIOLATIONS', '3');
insert into #table1 (GROUPNAME, SORT_ORDER) values('ACTIONS', '4');

--Get STARTED, CLOSED Dates
update #table1 set [STARTED] = (select [STARTED] from Case_Main where CASE_NO = @CASE_NO and GROUPNAME = 'DAYS') ;
update #table1 set CLOSED = (select CLOSED from Case_Main where CASE_NO = @CASE_NO and GROUPNAME = 'DAYS') ;
	
--Get TOTALDAYS
update #table1 set [DAYS] = CASE
	WHEN CLOSED is NULL THEN CONVERT(bigint,CONVERT(datetime,GETDATE()))- CONVERT(bigint,CONVERT(datetime,[STARTED]))
	WHEN CLOSED is not NULL THEN CONVERT(bigint,CONVERT(datetime,CLOSED))- CONVERT(bigint,CONVERT(datetime,[STARTED])) END 
	WHERE GROUPNAME = 'DAYS';
	
--Get Number of Valid Inspections
update #table1 set TOTAL = (select COUNT(CASE_NO) from Case_Inspections where CASE_NO = @CASE_NO 
	and (REMARKS not like '%VOIDED%' or REMARKS is NULL or REMARKS = ''))
	where GROUPNAME = 'INSPECTIONS' ;
	
--Get Number of Open Valid Inspections
update #table1 set OPENED = (select COUNT(CASE_NO) from Case_Inspections where CASE_NO = @CASE_NO 
	and (REMARKS not like '%VOIDED%' or REMARKS is NULL or REMARKS = '')
	and COMPLETED_DATE is NULL) 
	where GROUPNAME = 'INSPECTIONS' ;

--Get Number of Completed Valid Inspections	
update #table1 set COMPLETED = (select COUNT(CASE_NO) from Case_Inspections where CASE_NO = @CASE_NO 
	and (REMARKS not like '%VOIDED%' or REMARKS is NULL or REMARKS = '')
	and COMPLETED_DATE is not NULL) 
	where GROUPNAME = 'INSPECTIONS' ;

--Get Number of Valid Violations
update #table1 set TOTAL = (select COUNT(CASE_NO) from Case_Violations2 where CASE_NO = @CASE_NO 
	and (VIOLATION_NOTES not like '%VOIDED%' or VIOLATION_NOTES is NULL or VIOLATION_NOTES = ''))
	where GROUPNAME = 'VIOLATIONS' ;
	
--Get Number of Opened Valid Violations
update #table1 set OPENED = (select COUNT(CASE_NO) from Case_Violations2 where CASE_NO = @CASE_NO 
	and (VIOLATION_NOTES not like '%VOIDED%' or VIOLATION_NOTES is NULL or VIOLATION_NOTES = '')
	and DATE_CORRECTED is NULL) 
	where GROUPNAME = 'VIOLATIONS' ;

--Get Number of Completed Valid Violations	
update #table1 set COMPLETED = (select COUNT(CASE_NO) from Case_Violations2 where CASE_NO = @CASE_NO 
	and (VIOLATION_NOTES not like '%VOIDED%' or VIOLATION_NOTES is NULL or VIOLATION_NOTES = '')
	and DATE_CORRECTED is not NULL) 
	where GROUPNAME = 'VIOLATIONS' ;

--Get Number of Valid Actions	
update #table1 set TOTAL = (select COUNT(CASE_NO) from Case_Actions where CASE_NO = @CASE_NO
	and (ACTION_DESCRIPTION not like '%VOIDED%' or ACTION_DESCRIPTION is NULL or ACTION_DESCRIPTION = '')) 
	where GROUPNAME = 'ACTIONS' ;
	
--Get Number of Opened Valid Actions	
update #table1 set OPENED = (select COUNT(CASE_NO) from Case_Actions where CASE_NO = @CASE_NO 
	and (ACTION_DESCRIPTION not like '%VOIDED%' or ACTION_DESCRIPTION is NULL or ACTION_DESCRIPTION = '')
	and COMPLETED_DATE is NULL)
	where GROUPNAME = 'ACTIONS' ;

--Get Number of Completed Valid Actions	
update #table1 set COMPLETED = (select COUNT(CASE_NO) from Case_Actions where CASE_NO = @CASE_NO 
	and (ACTION_DESCRIPTION not like '%VOIDED%' or ACTION_DESCRIPTION is NULL or ACTION_DESCRIPTION = '')
	and COMPLETED_DATE is not NULL)
	where GROUPNAME = 'ACTIONS' ;

update #table1 set TOTAL = 0 where TOTAL < 0 or TOTAL is NULL;
update #table1 set OPENED = 0 where OPENED < 0 or OPENED is NULL;
update #table1 set COMPLETED = 0 where COMPLETED < 0 or COMPLETED is NULL;

select * from #table1 order by SORT_ORDER;
END











GO
