USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GblGetPostableTimesheetHours]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_GblGetPostableTimesheetHours]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GblGetPostableTimesheetHours]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

					  					  					  
CREATE Procedure [dbo].[tsp_GblGetPostableTimesheetHours](
@vEndDate Varchar(300)
)

As


Declare @EndDate Datetime

Set @EndDate = DATEADD(dd,1,Convert(Varchar,Convert(Datetime,@vEndDate),1))
Print(@EndDate)

--RB Check if table exists before creating it
   IF OBJECT_ID('tempdb..#Activities') IS NOT NULL  
	drop table #Activities
Create Table #Activities(ActivityGroup Varchar(30),ActivityNo Varchar(100),UserName Varchar(1000),ActivityHours Float,RecordID Varchar(100),RecordUpdated INT Default 0)

Insert Into #Activities(ActivityGroup,ActivityNo,UserName,ActivityHours,RecordID)
Select Activity_Group,Activity_No,UserName,[Hours],RecordID
From Prmry_Timesheets
Where Posted = 0
AND Workdate < @EndDate

SELECT
	STUFF((SELECT ',' +  RecordID
           FROM #Activities b 
           WHERE b.username = a.username 
          FOR XML PATH('')), 1, 1, '') AS RecordIDArray,
    STUFF((SELECT ',' +  Convert(Varchar,ActivityHours)
           FROM #Activities b 
           WHERE b.username = a.username 
          FOR XML PATH('')), 1, 1, '') AS HoursArray,
	STUFF((SELECT ',' +  ActivityGroup
           FROM #Activities b 
           WHERE b.username = a.username 
          FOR XML PATH('')), 1, 1, '') AS ActivityGroupArray,
	STUFF((SELECT ',' +  ActivityNo
           FROM #Activities b 
           WHERE b.username = a.username 
          FOR XML PATH('')), 1, 1, '') AS ActivityNoArray,
	Username,
	FLOOR((Sum(ActivityHours)+0.000000000051)*10000000000)/10000000000.0 AS TotalHours --Floating point rounding
FROM #Activities a
GROUP BY username

GO
