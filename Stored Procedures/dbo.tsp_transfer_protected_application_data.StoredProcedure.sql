USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_transfer_protected_application_data]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_transfer_protected_application_data]
GO
/****** Object:  StoredProcedure [dbo].[tsp_transfer_protected_application_data]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[tsp_transfer_protected_application_data] 
	@recordId VARCHAR(50),
	@applicationId UniqueIdentifier
AS
BEGIN
	Declare @field1 VARCHAR(30),
			@field2 VARCHAR(30),
			@field3 VARCHAR(30),
			@field4 VARCHAR(30),
			@field5 VARCHAR(30)

	OPEN SYMMETRIC KEY SymKey
	DECRYPTION BY CERTIFICATE EncryptCert;

	SELECT	@field1 = CONVERT(VARCHAR(30), DECRYPTBYKEY(protected_field1)),
			@field2 = CONVERT(VARCHAR(30), DECRYPTBYKEY(protected_field2)),
			@field3 = CONVERT(VARCHAR(30), DECRYPTBYKEY(protected_field3)),
			@field4 = CONVERT(VARCHAR(30), DECRYPTBYKEY(protected_field4)),
			@field5 = CONVERT(VARCHAR(30), DECRYPTBYKEY(protected_field5))
	FROM ApplicationProtectedData
	WHERE application_id = @applicationId

	BEGIN
		IF EXISTS (SELECT 1 FROM protected_data WHERE license_recordid = @recordid)
			BEGIN
				UPDATE protected_data 
				SET protected_field1 = ENCRYPTBYKEY(KEY_GUID('SymKey'), @field1),
					protected_field2 = ENCRYPTBYKEY(KEY_GUID('SymKey'), @field2),
					protected_field3 = ENCRYPTBYKEY(KEY_GUID('SymKey'), @field3),
					protected_field4 = ENCRYPTBYKEY(KEY_GUID('SymKey'), @field4),
					protected_field5 = ENCRYPTBYKEY(KEY_GUID('SymKey'), @field5)
				WHERE license_recordid = @recordid; 
			END
		ELSE
			BEGIN
				INSERT INTO protected_data (license_recordid, protected_field1, protected_field2, protected_field3, protected_field4, protected_field5)
				VALUES (@recordid, 
						ENCRYPTBYKEY(KEY_GUID('SymKey'), @field1), 
						ENCRYPTBYKEY(KEY_GUID('SymKey'), @field2), 
						ENCRYPTBYKEY(KEY_GUID('SymKey'), @field3), 
						ENCRYPTBYKEY(KEY_GUID('SymKey'), @field4), 
						ENCRYPTBYKEY(KEY_GUID('SymKey'), @field5))
			END
	End
	CLOSE SYMMETRIC KEY symkey
END 
GO
