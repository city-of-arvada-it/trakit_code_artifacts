USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GblMyRecords]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_GblMyRecords]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GblMyRecords]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  					  
					  					  					  
-- =============================================
-- Author:		Vance Bradshaw, CRW Systems, Inc
-- Create date: December 2011
-- Description:	My Records
--	Returns records assigned to the user
-- =============================================
CREATE PROCEDURE [dbo].[tsp_GblMyRecords]
	@USERID varchar(6) = 'CRW'

AS
BEGIN
	SET NOCOUNT ON;
	-----------------------
	-- CodeTRAK
	-----------------------
	select top 25 --temp logic by mdp to limit total results
		'CASE' as MODULE, 
		CASE_NO as RECORD_NO,
		CASETYPE as RECORD_TYPE,
		STATUS as RECORD_STATUS,
		SITE_ADDR as SITE_ADDR,
		LOC_RECORDID as LOC_RECORDID,
		SITE_CITY as SITE_CITY,
		ASSIGNED_TO as ASSIGNEE
	from Case_Main
		where (ASSIGNED_TO = @USERID		
			or ASSIGNED_TO in (select USERNAME from Prmry_Users where USERID = @USERID))
		and CLOSED is null
UNION		
	-----------------------
	-- ProjectTRAK
	-----------------------
	select top 25 --temp logic by mdp to limit total results
		'PROJECT' as MODULE, 
		PROJECT_NO as RECORD_NO,
		PROJECTTYPE as RECORD_TYPE,
		STATUS as RECORD_STATUS,
		SITE_ADDR as SITE_ADDR,
		LOC_RECORDID as LOC_RECORDID,
		SITE_CITY as SITE_CITY,
		PLANNER as ASSIGNEE
	from Project_Main
		where (PLANNER = @USERID		
			or PLANNER in (select USERNAME from Prmry_Users where USERID = @USERID))
		and CLOSED is null
UNION		
	-----------------------
	-- PermitTRAK
	-----------------------
	select top 25 --temp logic by mdp to limit total results
		'PERMIT' as MODULE, 
		M.PERMIT_NO as RECORD_NO,
		M.PERMITTYPE as RECORD_TYPE,
		M.STATUS as RECORD_STATUS,
		M.SITE_ADDR as SITE_ADDR,
		M.LOC_RECORDID as LOC_RECORDID,
		M.SITE_CITY as SITE_CITY,
		U.DEFAULT_INSPECTOR as ASSIGNEE
	from Permit_Main as M, Permit_UDF as U
		where M.PERMIT_NO = U.PERMIT_NO
		and (U.DEFAULT_INSPECTOR = @USERID		
			or U.DEFAULT_INSPECTOR in (select USERNAME from Prmry_Users where USERID = @USERID))
		and FINALED is null
		and (EXPIRED is null
			or datediff(d,getdate(),EXPIRED) > 0)
UNION
	-----------------------
	-- CRM TRAK
	-----------------------
	select 
		'CRM' as MODULE, 
		M.ISSUE_LABEL as RECORD_NO,
		TL.LIST_TEXT as RECORD_TYPE,
		SL.LIST_TEXT as RECORD_STATUS,
		M.ISSUE_ADDRESS as SITE_ADDR,
		M.ISSUE_LOC_RECORDID as LOC_RECORDID,
		M.ISSUE_CITY as SITE_CITY,
		M.ASSIGNED_USER_ID as ASSIGNEE
	from CRM_Issues as M, CRM_Lists as TL, CRM_Lists as SL
		where M.ASSIGNED_USER_ID = @USERID
		and M.COMPLETED_DATE is null
		and TL.LIST_CATEGORY = '2'
		and TL.LIST_ID = M.NATURETYPE_LIST_ID
		and SL.LIST_CATEGORY = '5'
		and SL.LIST_ID = M.STATUS_LIST_ID	

ORDER BY RECORD_NO ;
		
END












GO
