USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ITR_CRMIssuesInRange]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_ITR_CRMIssuesInRange]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ITR_CRMIssuesInRange]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  
                                                  
-- =====================================================================
/*
       Returns either all issues within the bounding box, or xN - yW as a point.
       If xS AND yE are provided, it uses bounding box
       If either are omitted, it uses xN an yW as a center point and returns the
       closest 50.
       The bounding box is probably the better of the two methods just to prevent 
       issues that don't matter from showing.
       Another item that may need to be added is filtering for status.
       The framework of that has been created.
*/
-- =====================================================================
                                                                                                        
-- =====================================================================
-- Revision History:

-- =====================================================================  

CREATE Procedure [dbo].[tsp_ITR_CRMIssuesInRange](
       @xN Decimal(18,10),
       @xS    Decimal(18,10) = NULL,
       @yW Decimal(18,10),
       @yE Decimal(18,10) = NULL,
       @recordCount INT = 50,
       @statuses VARCHAR(500) = ''
)
As

-- for status, right now, all statuses are available
SELECT List_Text INTO #AvailableStatus FROM crm_lists WHERE list_category = 5
--================================(STATUSES) NO SPACES BETWEEN ENTRIES
IF @statuses <> ''
BEGIN
       DELETE FROM #AvailableStatus

       DECLARE @Delimiter NVarChar(1) = ','
       DECLARE @statusLength INT = LEN(@statuses) +1, @delimiterLength INT = LEN(@Delimiter);

         WITH a AS
   (
       SELECT
           [start] = 1,
           [end]   = COALESCE(NULLIF(CHARINDEX(@Delimiter, 
                       @statuses, @delimiterLength), 0), @statusLength),
           [value] = SUBSTRING(@statuses, 1, 
                     COALESCE(NULLIF(CHARINDEX(@Delimiter, 
                       @statuses, @delimiterLength), 0), @statusLength) - 1)
       UNION ALL
       SELECT
           [start] = CONVERT(INT, [end]) + @delimiterLength,
           [end]   = COALESCE(NULLIF(CHARINDEX(@Delimiter, 
                       @statuses, [end] + @delimiterLength), 0), @statusLength),
           [value] = SUBSTRING(@statuses, [end] + @delimiterLength, 
                     COALESCE(NULLIF(CHARINDEX(@Delimiter, 
                       @statuses, [end] + @delimiterLength), 0), @statusLength)-[end]-@delimiterLength)
       FROM a
       WHERE [end] < @statusLength
   )
   INSERT INTO #AvailableStatus  SELECT [value]
   FROM a
   WHERE LEN([value]) > 0
   OPTION (MAXRECURSION 0);
END
-- ================================== STATUSES, END


IF ((@xS is null) OR (@yE is NULL))
BEGIN
       -- POINT 
       -- distance from CENTER
       SELECT TOP(@recordCount) Issue_ID, CLI.list_text, issue_loc_recordid, geo.lat, geo.LON, (SQRT((@xN - geo.LAT)*(@xN - geo.LAT) + (@yW - geo.LON)*(@yW - geo.LON))) AS dist 
       FROM crm_issues JOIN geo_ownership AS GEO ON geo.RECORDID = ISSUE_LOC_RECORDID 
       INNER JOIN crm_lists AS CLI ON status_list_id = CLI.list_id
       WHERE issue_loc_recordid IS NOT NULL AND geo.lat IS NOT NULL AND geo.lon IS NOT NULL AND CLI.List_Text in (SELECT * FROM #AvailableStatus) ORDER BY dist ASC
END
ELSE
BEGIN
       -- Bounding
       SELECT Issue_ID, CLI.list_text, issue_loc_recordid, geo.lat, geo.LON 
       FROM crm_issues JOIN geo_ownership AS GEO ON geo.RECORDID = ISSUE_LOC_RECORDID 
       INNER JOIN crm_lists AS CLI ON status_list_id = CLI.list_id
       WHERE issue_loc_recordid IS NOT NULL AND geo.lat IS NOT NULL AND geo.lon IS NOT NULL 
       AND geo.lat <= @xN AND geo.lat >= @xS
       AND geo.lon >= @yW AND geo.lon <= @yE
       AND CLI.List_Text in (SELECT * FROM #AvailableStatus)
END




GO
