USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ChartStatusByActivityRecordID]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_ChartStatusByActivityRecordID]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ChartStatusByActivityRecordID]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create Procedure [dbo].[tsp_ChartStatusByActivityRecordID](
	@RecordID1 varchar(8000) = Null,
	@RecordID2 varchar(8000) = Null,
	@RecordID3 varchar(8000) = Null
		--rrr - adding one more
	,@RecordID4 varchar(8000) = Null
)

As

Declare @RecID table (RecordID varchar(2000)) 

insert @RecID
select r.recordid
from dbo.tfn_TblRecID(@recordid1,@recordid2,@recordid3,@recordid4) r  --rrr - added 4th param

set nocount on

SELECT STATUS, COUNT(STATUS) AS cnt FROM AEC_Main where RECORDID in (select RECORDID from @recID)  GROUP BY STATUS
UNION SELECT STATUS, COUNT(STATUS) AS cnt FROM CASE_MAIN where RECORDID in (select RECORDID from @recID)  GROUP BY STATUS
UNION SELECT STATUS, COUNT(STATUS) AS cnt FROM GEO_OWNERSHIP where RECORDID in (select RECORDID from @recID)  GROUP BY STATUS
UNION SELECT STATUS, COUNT(STATUS) AS cnt FROM LICENSE_MAIN where RECORDID in (select RECORDID from @recID)  GROUP BY STATUS
UNION SELECT STATUS, COUNT(STATUS) AS cnt FROM LICENSE2_MAIN where RECORDID in (select RECORDID from @recID)  GROUP BY STATUS
UNION SELECT STATUS, COUNT(STATUS) AS cnt FROM PERMIT_MAIN where RECORDID in (select RECORDID from @recID)  GROUP BY STATUS
UNION SELECT STATUS, COUNT(STATUS) AS cnt FROM PROJECT_MAIN where RECORDID in (select RECORDID from @recID)  GROUP BY STATUS
UNION SELECT CRM_Lists.LIST_TEXT AS STATUS, COUNT(CRM_Lists.LIST_TEXT) AS cnt FROM CRM_Issues 
	INNER JOIN CRM_Lists ON CRM_Issues.STATUS_LIST_ID = CRM_Lists.LIST_ID where CRM_Issues.RECORDID in (select RECORDID from @RecID) 
	GROUP BY CRM_Lists.LIST_TEXT
 


GO
