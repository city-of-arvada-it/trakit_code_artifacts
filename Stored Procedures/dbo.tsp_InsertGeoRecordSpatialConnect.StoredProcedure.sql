USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_InsertGeoRecordSpatialConnect]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_InsertGeoRecordSpatialConnect]
GO
/****** Object:  StoredProcedure [dbo].[tsp_InsertGeoRecordSpatialConnect]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[tsp_InsertGeoRecordSpatialConnect] (	
	@apn Varchar(50)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	declare @geotype varchar(15);
	
	set @geotype = (select typename from Prmry_Types where GroupName = 'GEO' and typename like '%PARCEL');

	if @@ROWCOUNT = 1
	insert into geo_ownership (SITE_APN, RECORDID, ActivityTypeID, GEOTYPE) values (@apn, (select 'SPC:' + right(('00' + str(DATEPART(yy, GETDATE()))),2)
								+ right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)
								+ right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
								+ right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)
								+ right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
								+ right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)
								+ right(('000000'+ CONVERT(varchar,ROW_NUMBER() OVER (ORDER BY (SELECT NULL)))),6)), 5, @geotype);
	else
		print 'More than one Parcel Geotype exists'

   
	
END
GO
