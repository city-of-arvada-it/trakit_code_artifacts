USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[ALP_GetLicenseAvailability]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[ALP_GetLicenseAvailability]
GO
/****** Object:  StoredProcedure [dbo].[ALP_GetLicenseAvailability]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ALP_GetLicenseAvailability]
-- ==================================================================================================================================================================
-- Author:		Sravanti Adibhatla, Erick Hampton
-- Create date: Dec 28, 2016
-- Description:	Returns renewal availability info for a specified license number
-- Revisions:	Feb 8, 2017:  added @IsRenewal parameter so we don't check if the license is eligible for renewal if it isn't being processed for renewal 
--				Mar 14, 2017: added @IsRenewal=1 condition to ParameterSetOperationsConfigIssue, as we can only infer the parameter set ID for the RENEWAL type
--				Sep 20, 2018: modified check for opstatus to underlying table rather than view, due to timeouts.
--				Dec 18, 2018: reworked logic due to timeouts.
-- ==================================================================================================================================================================
		@License_No varchar(20) ='',
		@IsRenewal bit = 1,
		@IgnoreThisBatchID int = -1

AS
BEGIN
	DECLARE @availability VARCHAR(50)	
	
	BEGIN

		IF @availability IS NULL 
		BEGIN 
		-- identify licenses from @LicenseNumbers that are in batches already where its operations are still pending
			SELECT @availability = CASE opstatus.OperationStatusID
								   WHEN 0 THEN 'PendingProcessingElsewhere'
								   WHEN 1 THEN 'CurrentlyProcessingElsewhere'
								   END 
			FROM [ALP_OperationStatusByBatchParameterSetLicenseNumber] opstatus 
			INNER JOIN [ALP_Batches] b 
			  ON opstatus.BatchID=b.Id
			WHERE opstatus.LicenseNumber = @License_No
			  AND b.StatusID IN (1, 2, 3) 
			  AND opstatus.OperationStatusID = 0 
			  AND b.Id <> @IgnoreThisBatchID
		END 

		IF @IsRenewal = 1 AND @availability IS NULL 
			BEGIN
				IF @License_No IN 
					(
						SELECT lrpr.LICENSE_NO
						FROM [ALP_LicensesRunPerRenewal] lrpr
							inner join [ALP_ParameterSetAndRenewalPeriodByLicenseNumber] psarpbln 
							on lrpr.[LICENSE_NO]=psarpbln.[LICENSE_NO] and lrpr.[ParameterSetID]=psarpbln.[ParameterSetID]
						where getdate() between psarpbln.[RenewalPeriodStart] and psarpbln.[RenewalPeriodEnd]
					)
					BEGIN
						SET @availability = 'AlreadyRenewed'
					END
				ELSE
					
					DECLARE @type AS Varchar(60) = '';
					DECLARE @subtype AS varchar(60) = '';

					SELECT @type = LICENSE_TYPE FROM license2_main WHERE license_no = @License_No
					SELECT @subtype = LICENSE_SUBTYPE FROM license2_main WHERE license_no = @License_No

					PRINT (@type + ' ' + @subtype	)

					DECLARE @parameterSet AS varchar(10) = '';
					DECLARE @renewalStart AS Date;
					DECLARE @renewalEnd AS Date;

					IF ISNULL(@subtype,'') <> '' -- Search on type / subtype
						BEGIN
							SELECT 
							@parameterSet = ParameterSetID
							FROM 
							[ALP_RenewalPeriodsByParameterSetOrLicenseTypeSubType] rp
							WHERE LicenseType = @type AND LicenseSubType = @subtype
							-- check by date here?
						END

					IF @parameterSet <> '' -- Is it just outside of range?
						BEGIN
							SET @parameterSet = ''
							SELECT 
							@parameterSet = ParameterSetID
							FROM 
							[ALP_RenewalPeriodsByParameterSetOrLicenseTypeSubType] rp
							WHERE LicenseType = @type AND LicenseSubType = @subtype
							AND (GETDATE() BETWEEN rp.RenewalPeriodStart AND rp.RenewalPeriodEnd)
						END 
					ELSE IF @parameterSet = '' -- Nothing found using subtype, try to use parent type
						BEGIN
							SELECT 
							@parameterSet = ParameterSetID
							FROM 
							[ALP_RenewalPeriodsByParameterSetOrLicenseTypeSubType] rp
							WHERE LicenseType = @type AND LicenseSubType = ''
							AND GETDATE() BETWEEN rp.RenewalPeriodStart AND rp.RenewalPeriodEnd
						END 

					IF (ISNULL(@parameterSet,'') = '') 
						BEGIN
							SET @availability = 'NotEligibleForRenewal'
						END
	
					DECLARE @parameterSetType AS Varchar(60);
					SELECT @parameterSetType = TypeName FROM [ALP_ParameterSets] ps 
					INNER JOIN [ALP_ParameterSetTypes] pst ON @parameterSet	= ps.Id
					WHERE ps.ParameterSetTypeId=pst.ID
	
					IF(@parameterSetType <> 'RENEWAL')
						BEGIN
							SET @availability = 'ParameterSetOperationsConfigIssue'
						END

			END
			
		SELECT COALESCE(@availability , 'Ready') [Availability], @License_No [License No], 1 [Processing Order]
	END
END

GO
