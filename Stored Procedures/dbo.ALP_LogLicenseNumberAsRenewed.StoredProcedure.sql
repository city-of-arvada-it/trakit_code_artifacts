USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[ALP_LogLicenseNumberAsRenewed]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[ALP_LogLicenseNumberAsRenewed]
GO
/****** Object:  StoredProcedure [dbo].[ALP_LogLicenseNumberAsRenewed]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ====================================================================================================================
-- Author:		Erick Hampton
-- Revised:		Feb 7, 2017
-- Description:	Insert a row into the ALP_LicensesRunPerRenewal table to document info 
--				from when the specified license number was most recently renewed.
-- ====================================================================================================================
CREATE PROCEDURE [dbo].[ALP_LogLicenseNumberAsRenewed]

	@LicenseNumber varchar(30) = NULL,
	@ParameterSetID int,
	@SharedSearchID int
	
AS
BEGIN

	set @SharedSearchID = isnull(@SharedSearchID, -1) -- make sure we have a value
	if @LicenseNumber is not null and @ParameterSetID is not null and @SharedSearchID is not null
	begin
		-- need to make sure we have all the data before we insert anything
		declare @RenewalRangeStart datetime 
		declare @RenewalRangeEnd datetime
		set @RenewalRangeStart = NULL
		set @RenewalRangeEnd = NULL

		-- fetch the renewal date range
		select top 1 @RenewalRangeStart=RenewalPeriodStart, @RenewalRangeEnd=RenewalPeriodEnd
		from [ALP_RenewalPeriodsByParameterSetOrLicenseTypeSubType] 
		where ParameterSetID=@ParameterSetID

		-- insert the data into [ALP_LicensesRunPerRenewal]
		if @RenewalRangeStart is not null and @RenewalRangeEnd is not null 
		begin 
			insert into [ALP_LicensesRunPerRenewal] (LICENSE_NO, ParameterSetID, SharedSearchID, RenewalRangeStart, RenewalRangeEnd)
			select @LicenseNumber [LICENSE_NO], @ParameterSetID [ParameterSetID], @SharedSearchID [SharedSearchID], @RenewalRangeStart [RenewalRangeStart], @RenewalRangeEnd [RenewalRangeEnd]
		end
	end

END

GO
