USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_StaleLogins_Precheck]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_StaleLogins_Precheck]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_StaleLogins_Precheck]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[tsp_T9_StaleLogins_Precheck](
	@UserID Varchar(50), 
	@UserPwd varchar(50)
 )
AS
BEGIN
       
	   SET NOCOUNT ON;
       
IF EXISTS (
			SELECT   
			t.UID
			FROM TRAKIT9_login t
			join Prmry_Users pu
			ON pu.UserID = t.UID
			WHERE 
			t.uid = @UserID 
			--AND pu.UserPassword = @userpwd
			AND t.loginExpireDate IS NOT NULL
			AND t.lastlogin is not null
			)
					
 Begin
	update TRAKIT9_login 
			set loginExpireDate = NULL
			where uid = @UserID 			
 End
 

END
GO
