USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[ALP_ValidateLicenseNumbers]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[ALP_ValidateLicenseNumbers]
GO
/****** Object:  StoredProcedure [dbo].[ALP_ValidateLicenseNumbers]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Author:		Erick Hampton
-- Create date: Jul 12, 2017
-- Description:	Validates a set of supplied license numbers
-- Revisions:	10/26/2018 - AG - Subtype check changed to 'is null'
--				01/22/2019 - AG - Modified Type and Subtype Check
-- ==================================================================================================================================================================
CREATE PROCEDURE [dbo].[ALP_ValidateLicenseNumbers]
		
		@LicenseList [ALP_LicenseNumberList] READONLY,
		@IsRenewal bit = 1,
		@ParameterSetID int = -1 -- this is always -1 for renewals
			
AS
BEGIN
	-- IMPORTANT: Currently searches are irrespective of parameter set type, but elsewhere in ALP we consider a license to be a renewal when @ParameterSetID = -1.
	--			  If / when we choose to allow users to run renewal parameter set types as ad-hoc, we need to confirm that doing so will not break this proc.
	if @IsRenewal = 1 set @ParameterSetID = -1
	declare @today date = getdate()
	declare @ErrorType varchar(50) = ''
		
	-- this table gets populated with the distinct license numbers we want
	-- [Availability] = AlreadyRenewed, PendingProcessingElsewhere, CurrentlyProcessingElsewhere, NotEligibleForRenewal, ...
	-- or Ready if fine to process
	if object_id('tempdb..#LicenseNumbers') is not null drop table #LicenseNumbers
	create table #LicenseNumbers ([ProcessingOrder] int NOT NULL identity(1,1)
								, [Availability] varchar(50) NULL
								, [LICENSE_NO] varchar(30) NOT NULL
								, [LicenseType] varchar(60)
								, [LicenseSubType] varchar(60)
								, [ParameterSetID] int
								, unique clustered ([LICENSE_NO],[LicenseType], [LicenseSubType],[ParameterSetID])
								)
	-- populate #LicenseNumbers with the license numbers
	insert into #LicenseNumbers (LICENSE_NO) select LICENSE_NO from @LicenseList
	-- ****************************************************************************************************************************************************************
	-- ADD THE PARAMETERSETID TO THE TEMP TABLE FOR USE WITH VALIDATIONS FOR RENEWALS (RENEWALS ONLY)
	-- ****************************************************************************************************************************************************************
	if @IsRenewal = 1 
	begin 
		-- we need to identify the parameter set for each license type / subtype to check as renewals could have multiple parameter set ID values
				
		-- NOTE: there is no significant time difference by using the view [ALP_RenewalPeriodsByParameterSetOrLicenseTypeSubType]
				
		/*
		-- ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
		-- TIME STUDY: best time to get to here including the statements in this section = 7.124 seconds to populate 77,295 rows (this is slower)
				
		-- first: update the parameter set ID values with both matching License Type and Subtype values where Subtype is not null
		-- NOTE: there is no time savings in checking to see if there is data that will be selected before executing the update statement
		update ln set ParameterSetID = rp.ParameterSetID
		from #LicenseNumbers ln inner join [ALP_RenewalPeriodsByParameterSetOrLicenseTypeSubType] rp 
			on (ln.LicenseType = rp.LicenseType) and (ln.LicenseSubType = rp.LicenseSubType)
		where rp.LicenseSubType is not null and rp.LicenseSubType <> '' and @today between rp.RenewalPeriodStart and rp.RenewalPeriodEnd
		-- second: update the remaining parameter set ID values with matching License Type values only, where Subtype is null in [ALP_RenewalPeriodsByParameterSetOrLicenseTypeSubType]
		update ln set ParameterSetID = rp.ParameterSetID
		from #LicenseNumbers ln inner join [ALP_RenewalPeriodsByParameterSetOrLicenseTypeSubType] rp 
			on (ln.LicenseType = rp.LicenseType) and (rp.LicenseSubType is null or rp.LicenseSubType = '')
		where ln.ParameterSetID is null and @today between rp.RenewalPeriodStart and rp.RenewalPeriodEnd
		-- ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
		*/
		-- ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
		-- TIME STUDY: best time to get to here including the statements in this section = 6.493 seconds to populate 77,295 rows
				
		update ln set ln.ParameterSetID = a.ParameterSetID,
		ln.LicenseType = a.LicenseType,
		ln.LicenseSubType = a.LicenseSubType
		from #LicenseNumbers ln
		inner join (
		SELECT distinct [ParameterSetID], 
			licenseMain.LICENSE_NO,
			[LicenseType],
			[LicenseSubType]
			FROM [ALP_RenewalPeriodsByParameterSetOrLicenseTypeSubType] renewalPeriods
				inner join license2_main licenseMain on renewalPeriods.LicenseType = licenseMain.LICENSE_TYPE
                 and (ISNULL(licenseMain.LICENSE_SUBTYPE,'') = case when 
						ISNULL(renewalPeriods.LicenseSubType,'') = ''
                        then ISNULL(licenseMain.LICENSE_SUBTYPE,'')
                        else renewalPeriods.LicenseSubType
                        end)
		where @today between RenewalPeriodStart and RenewalPeriodEnd 
		) a
		on ln.LICENSE_NO = a.LICENSE_NO
		-- ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	end
	-- ****************************************************************************************************************************************************************
	-- ********************************************************************************************************************************************************
	-- VALIDATION 1: SET [AVAILABILITY] to NotEligibleForRenewal, ParameterSetOperationsConfigIssue (APPLIES TO RENEWALS ONLY)
	-- ********************************************************************************************************************************************************
	if @IsRenewal = 1 
	begin 
		-- at this point, anything license_no in #LicenseNumbers without a ParameterSetID is:
		-- (1) there is not a parameter set that is mapped to a renewal period that encompasses today's date = "NotEligibleForRenewal"
		-- (2) the license type / subtype is not mapped to a parameter set = "ParameterSetOperationsConfigIssue"
		-- (3) the parameter set ID cannot uniquely be identified for the renewal periods that encompass today's date = "ParameterSetOperationsConfigIssue"				
				
		-- set [Availability] to "NotEligibleForRenewal" for license_no values with no ParameterSetID that have a ParameterSetID mapping
		-- IMPORTANT: this may be updating the [Availability] to the wrong reason as 
		--			  (1) there may be a more accurate reason that would have been assigned if validated data in the order we did previously
		--			  (2) there may not be a ParameterSetID mapping set up for only the license type, so this may be a "ParameterSetOperationsConfigIssue"
		update ln set [Availability] = 'NotEligibleForRenewal'
		from #LicenseNumbers ln	inner join [ALP_RenewalParameterSetAssignment] rpsa 
			on ln.LicenseType = rpsa.LicenseType
		where ln.[ParameterSetID] is null and [Availability] is null 
		-- set [Availability] to "ParameterSetOperationsConfigIssue" for any remaining license_no values with no ParameterSetID 
		update #LicenseNumbers set [Availability] = 'ParameterSetOperationsConfigIssue' where [ParameterSetID] is null and [Availability] is null
				
	end
	-- ********************************************************************************************************************************************************
	-- ********************************************************************************************************************************************************
	-- VALIDATION 2: EXEC [ALP_CheckParameterSets] TO CHECK FOR CONFIGURATION ERRORS (APPLIES TO ALL PARAMETER SET TYPES)
	-- ********************************************************************************************************************************************************
	-- if a parameter set ID is not specified (i.e. in the case of renewals), we need to identify the parameter sets to validate
	if @ParameterSetID <= 0
	begin
		-- construct a checklist of parameter set ID values to validate
		-- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		declare @ParameterSetsToValidate table ([ParameterSetID] int, ErrorType varchar(50))
		insert into @ParameterSetsToValidate (ParameterSetID) 
		select distinct ParameterSetId
		from #LicenseNumbers
		where ParameterSetID is not null and [Availability] is null -- don't need to check licenses that are already unavailable
		-- :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
		-- validate every parameter set ID
		-- '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
		declare @testPassed bit = 0 -- not used here, but required to exec proc
		if (select count(*) from @ParameterSetsToValidate) > 0
		begin
			declare @psid int = -1
			while (select count(*) from @ParameterSetsToValidate) > 0
			begin
				set @ErrorType = ''
				select top 1 @psid = ParameterSetID from @ParameterSetsToValidate
						
				-- TODO: using sp_executesql instead of exec here may be faster
						
				exec [ALP_CheckParameterSets] @psid, @testPassed out, @ErrorType out
				if @ErrorType <> '' update #LicenseNumbers set [Availability] = @ErrorType where ParameterSetID = @psid and [Availability] is null
				delete from @ParameterSetsToValidate where ParameterSetID = @psid -- next!
			end
		end
		-- '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
	end
	else
	begin
		-- we just have the one supplied parameter set id to check that applies to all license numbers
		exec [ALP_CheckParameterSets] @ParameterSetID, @testPassed out, @ErrorType out
		if @ErrorType <> '' update #LicenseNumbers set [Availability] = @ErrorType where [Availability] is null
	end
	-- ********************************************************************************************************************************************************
	-- ********************************************************************************************************************************************************
	-- create a temp table to house [LICENSE_NO] and [Availability] that we can use for the next two validations (as necessary)
	-- ********************************************************************************************************************************************************
			
	if object_id('tempdb..#LicensesToValidate') is not null drop table #LicensesToValidate
	create table #LicensesToValidate ([LICENSE_NO] varchar(20), [Availability] varchar(50))
	-- ********************************************************************************************************************************************************
	-- ********************************************************************************************************************************************************
	-- VALIDATION 3: EXEC [ALP_GetLicenseAvailabilities] TO CHECK IF LICENSES ARE ALREADY PROCESSING (APPLIES TO ALL PARAMETER SET TYPES)
	-- ********************************************************************************************************************************************************
			
	declare @CheckLicenseAvailability [ALP_LicenseNumberList] -- we are repurposing the [ProcessingOrder] column in [ALP_LicenseNumberList] so we don't have to design another UDTT
	
	-- populate the [ProcessingOrder] column in @CheckLicenseAvailability with the parameter set ID values
	if @IsRenewal = 1
	begin
		-- we could have many parameter set ID values stored in #LicenseNumbers
		insert into @CheckLicenseAvailability (LICENSE_NO, ProcessingOrder)
		select LICENSE_NO, ParameterSetID from #LicenseNumbers where [Availability] is null
	end
	else
	begin
		-- we have only one parameter set ID value stored in @ParameterSetID
		insert into @CheckLicenseAvailability (LICENSE_NO, ProcessingOrder)
		select LICENSE_NO, @ParameterSetID from #LicenseNumbers where [Availability] is null
	end
	insert into #LicensesToValidate exec [ALP_GetLicenseAvailabilities] @CheckLicenseAvailability, 0, 1
	update ln 
	set ln.[Availability] = ltv.[Availability]
	from #LicenseNumbers ln inner join #LicensesToValidate ltv on ln.[LICENSE_NO] = ltv.[LICENSE_NO]
	where ln.[Availability] is null -- ltv.Availability should never be NULL
	-- ********************************************************************************************************************************************************
	-- ********************************************************************************************************************************************************
	-- VALIDATION 4: CHECK IF LICENSES HAVE ALREADY BEEN RENEWED DURING THE RENEWAL DATE RANGE (APPLIES TO RENEWALS ONLY)
	-- ********************************************************************************************************************************************************
			
	if @IsRenewal = 1
	begin
		-- let's just do the update here to avoid passing potentially large data sets around by calling another proc just to perform this measely task
		-- update [Availability] in #LicenseNumbers with NotEligibleForRenewal for licenses numbers in [ALP_LicensesRunPerRenewal] 
		-- that fall within the renewal date range for the parameter set ID value
		update ln set [Availability] = 'AlreadyRenewed'
		from #LicenseNumbers ln 
			inner join [ALP_LicensesRunPerRenewal] lrpr on (ln.LICENSE_NO = lrpr.LICENSE_NO) and (ln.ParameterSetID = lrpr.ParameterSetID)
			inner join [ALP_RenewalPeriodsByParameterSetOrLicenseTypeSubType] rp on ln.ParameterSetID = rp.ParameterSetID
		where (ln.ParameterSetID is not null) and (lrpr.RenewalDate between rp.RenewalPeriodStart and rp.RenewalPeriodEnd)
	end
	-- ********************************************************************************************************************************************************
	-- ********************************************************************************************************************************************************
	-- RETURN THE RESULTS TO THE CALLER -- RETURN [Availability] = "Ready" FOR LICENSES WITHOUT ISSUES
	-- ********************************************************************************************************************************************************
	-- TIME STUDY: best duration for 77,295 rows = 12.88 seconds (this is slower)
	--update #LicenseNumbers set [Availability] = 'Ready' where [Availability] is null-- or [Availability]='')
	--select isnull([Availability],'Ready'), [LICENSE_NO], [ProcessingOrder] from #LicenseNumbers order by [ProcessingOrder] 
	-- TIME STUDY: best duration for 77,295 rows = 11.366 seconds
	select isnull([Availability],'Ready') [Availability], [LICENSE_NO], [ProcessingOrder] from #LicenseNumbers order by [ProcessingOrder] 
	-- ********************************************************************************************************************************************************
	
	-- ********************************************************************************************************************************************************
	-- CLEAN UP TEMP TABLES
	-- ********************************************************************************************************************************************************
			
	if object_id('tempdb..#LicensesToValidate') is not null drop table #LicensesToValidate
	if object_id('tempdb..#LicenseNumbers') is not null drop table #LicenseNumbers
	-- ********************************************************************************************************************************************************
		
END
GO
