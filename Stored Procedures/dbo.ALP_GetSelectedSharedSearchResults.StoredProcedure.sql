USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[ALP_GetSelectedSharedSearchResults]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[ALP_GetSelectedSharedSearchResults]
GO
/****** Object:  StoredProcedure [dbo].[ALP_GetSelectedSharedSearchResults]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- ========================================================================================
-- Author:		Erick Hampton
-- Create date: Dec 28, 2016
-- Description:	Returns the search results of a shared search with the supplied Search ID
-- 
-- Revisions:	EWH Jul 12, 2017: refactored validations for performance
--
-- ========================================================================================
CREATE PROCEDURE [dbo].[ALP_GetSelectedSharedSearchResults]

	@SearchID int = -1,
	@IsRenewal bit = 1,
	@ValidateSearchResults bit = 1,
	@ParameterSetID int = -1, -- this is always -1 for renewals
	@StartingRow bigint = -1, -- default to return records starting with the first row
	@EndingRow bigint = -1,	  -- default to return all records
	@ErrorType varchar(50) out 

AS
BEGIN	
	

	-- make sure we have a valid search ID
	if @SearchID > -1 
	begin
		
		set @ErrorType = '' -- initialize the ErrorType value
		declare @today date = getdate()


		-- IMPORTANT: Currently searches are irrespective of parameter set type, but elsewhere in ALP we consider a license to be a renewal when @ParameterSetID = -1.
		--			  If / when we choose to allow users to run renewal parameter set types as ad-hoc, we need to confirm that doing so will not break this proc.
		if @IsRenewal = 1 set @ParameterSetID = -1





		-- ****************************************************************************************************************************************************************
		-- INSERT THE SHARED SEARCH RESULTS INTO A TEMP TABLE
		-- ****************************************************************************************************************************************************************
		
		-- this table gets populated with the distinct license numbers we want
		-- [Availability] = AlreadyRenewed, PendingProcessingElsewhere, CurrentlyProcessingElsewhere, NotEligibleForRenewal or Ready if fine to process


		if object_id('tempdb..#LicenseNumbersFromSearchResults') is not null drop table #LicenseNumbersFromSearchResults
		create table #LicenseNumbersFromSearchResults 
									( [ProcessingOrder] int NOT NULL identity(1,1)
									, [Availability] varchar(50) NULL
									, [LICENSE_NO] varchar(30) NOT NULL
									, [LicenseType] varchar(60)
									, [LicenseSubType] varchar(60)
									, [ParameterSetID] int
									, unique clustered ([LICENSE_NO],[LicenseType], [LicenseSubType],[ParameterSetID])
									)



		-- get the alias for license_no for the shared search
		declare @license_no_alias varchar(255) = ''
		select top 1 @license_no_alias=DisplayName from [Prmry_AdvSearch_SharedDetail] where ItemID=@SearchID and TableName='LICENSE2_MAIN' and FieldName='LICENSE_NO' 
		if @license_no_alias='' or @license_no_alias is null 
		begin
			set @ErrorType = 'MissingColumnForLicenseNo'
		end
		else
		begin
			-- license_no is part of the search and we know the alias
		
			-- populate variables needed for search
			-- NOTE: UserID is irrelevent; it is used for CRM searches not LICENSE2 searches
			declare @SearchType varchar(50) = 'LICENSE2_MAIN' 
			declare @Description varchar(250) = null
			declare @SearchName varchar(50) = null
			declare @Group varchar(50) = 'LICENSE2'
			declare @RefSearchID int = null
			declare @RefTable varchar(50) = null
			declare @SelectClause varchar(max) = ''
			declare @SelectAlias varchar(max) = ''
			declare @FromClause varchar(max) = ''
			declare @WhereClause varchar(max) = ''
			declare @GroupByClause varchar(max) = ''
			declare @OrderByClause varchar(max) = @license_no_alias 
			declare @OrderByInOver varchar(max) = '[LICENSE2_MAIN].[LICENSE_NO]' 

			-- get the clauses to construct the select statement
			EXEC [tsp_T9_AdvSearch_CompileQuery] '_ALP', 'shared' , @SearchID, @license_no_alias, 'asc', 'NA', 'NA', @SearchType out, @Description out, @SearchName out, @Group out, @RefSearchID out, @RefTable out, @SelectClause out, @SelectAlias out, @FromClause out, @WhereClause out, @OrderByClause out, @OrderByInOver out, 0, 0 

			-- construct the where clause if data exists for it
			if @WhereClause <> '' set @WhereClause = ' WHERE ' + @WhereClause 

			-- construct the group by clause if data exists for it
			if @GroupByClause <> '' set @GroupByClause = ' GROUP BY ' + @GroupByClause

			-- add specific row numbers to the where clause if the starting / ending row values are > 0 and the ending row number is the same or greater than the starting row number
			if (@StartingRow > 0) and (@EndingRow > 0) and (@EndingRow >= @StartingRow)
			begin
				set @WhereClause = @WhereClause + ' and ([row number] between ' + cast(@StartingRow as varchar) + ' and ' + cast(@EndingRow as varchar) + ') '
			end
			
			-- construct the select statement and execute shared search to get list of license_no values into a temp table 
			-- NOTE: we only select LICENSE_NO; sort order is not implemented (aside from the default sort order of LICENSE_NO
			declare @sql nvarchar(max) = ''
			set @sql = 'SELECT ' + @SelectAlias + ' FROM (SELECT ' + ' ROW_NUMBER() OVER(ORDER BY [LICENSE2_MAIN].[LICENSE_NO]) as ["row number"], ' + @SelectClause + ' FROM ' + @FromClause + @WhereClause + @GroupByClause + ') a ' 


			-- replace any XML encoding made by SQL Server / ADO.NET
			-- this could be in a few different clauses, so we need to check almost the whole select statement	
			set @sql=replace(replace(replace(replace(replace(@sql,'&amp;','&'),'&apos;',char(39)),'&quot;','"'),'&gt;','>'),'&lt;','<')

			-- it's a little easier to read like this:
			--set @sql=replace(@sql,'&lt;','<')
			--set @sql=replace(@sql,'&gt;','>')
			--set @sql=replace(@sql,'&quot;','"')
			--set @sql=replace(@sql,'&apos;',char(39))
			--set @sql=replace(@sql,'&amp;','&')


			-- only fetch the license type and subtype if we need to figure out the parameter set id
			if @ParameterSetID <= 0 and @IsRenewal = 1 and @ValidateSearchResults = 1
			begin
				set @sql = 'SELECT DISTINCT alp.[' + @license_no_alias + '] as [LICENSE_NO], alp2.license_type [alp license type], alp2.license_subtype [alp license subtype] FROM (' 
							+ @sql + ') alp left join LICENSE2_MAIN alp2 on alp.license_no=alp2.license_no'
				print @sql
				insert into #LicenseNumbersFromSearchResults ([LICENSE_NO], [LicenseType], [LicenseSubType]) exec (@sql)
			end
			else
			begin
				set @sql = 'SELECT DISTINCT [' + @license_no_alias + '] as [LICENSE_NO] FROM (' + @sql + ') alp'
				print @sql
				insert into #LicenseNumbersFromSearchResults ([LICENSE_NO]) exec (@sql)
			end
			

			-- make sure we have results from the search 
			if (select count(*) from #LicenseNumbersFromSearchResults) = 0
			begin
				set @ErrorType = 'NoSearchResults'
			end
		end
		-- ****************************************************************************************************************************************************************




		------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		-- VALIDATE THE SEARCH RESULTS
		------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		-- only continue if we are error-free and we want to validate the results
		if @ErrorType = '' and @ValidateSearchResults = 1
		begin
			declare @LicenseNumbersToValidate [ALP_LicenseNumberList]
			insert into @LicenseNumbersToValidate (LICENSE_NO) select LICENSE_NO from #LicenseNumbersFromSearchResults
			exec [ALP_ValidateLicenseNumbers] @LicenseNumbersToValidate, @IsRenewal, @ParameterSetID
		end
		------------------------------------------------------------------------------------------------------------------------------------------------------------------------



		-- clean up the temp table
		if object_id('tempdb..#LicenseNumbersFromSearchResults') is not null drop table #LicenseNumbersFromSearchResults




	end 
	else
	begin
		set @ErrorType = 'InvalidSearchID'
	end
	
END
GO
