USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_GetEtrakItUDFFields]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[usp_Arvada_GetEtrakItUDFFields]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_GetEtrakItUDFFields]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*=============================================
-- Author:		Jesse Hutchens
-- Create date: 06/23/2016
-- Description:	Used in printPermit_TH.aspx
-- =============================================
exec  dbo.usp_Arvada_GetEtrakItUDFFields 'FENC16-00172'
*/
CREATE PROCEDURE [dbo].[usp_Arvada_GetEtrakItUDFFields]
	@permitno varchar(50)
AS
BEGIN 

SET NOCOUNT ON;
 --------------------------
 --- Declare Variables   --
 --------------------------
 DECLARE 
	@permitType VARCHAR(50),
	@s INT,
	@e INT,
	@fieldName VARCHAR(50)	,
	@sql NVARCHAR(MAX)
 
 --------------------------
 --- Create Temp Tables  --
 --------------------------

	 
 --------------------------
 --- Body of Procedure   --
 --------------------------

SELECT @permitType = permitType
FROM dbo.Permit_MAIN
WHERE Permit_NO = @permitno

CREATE TABLE #CustomFields
(
	GroupName VARCHAR(50),
	TypeName VARCHAR(50),
	ScreenName VARCHAR(50),
	DataField VARCHAR(50),
	Caption VARCHAR(150),
	DateFieldConverted VARCHAR(50),
	DataValue NVARCHAR(MAX),
	rid INT IDENTITY(1,1)
)

INSERT INTO  #CustomFields
(
	GroupName ,
	TypeName ,
	ScreenName ,
	DataField ,
	Caption ,
	DateFieldConverted
)
SELECT  
	pt.groupname,
	pt.TypeName,
	screens.DataField AS ScreenName,
	pt.DataField,
	pt.Caption,
	CASE WHEN CHARINDEX('.', pt.datafield) > 0 THEN 'u.[' + SUBSTRING(pt.dataField,CHARINDEX('.', pt.datafield)+1, LEN(pt.datafield)) + ']' ELSE 'm.[' + pt.dataField + ']' END
FROM dbo.Prmry_TypesUDF pt 
INNER JOIN dbo.Prmry_TypesUDF Screens
	ON Screens.Screen = pt.Screen
	AND Screens.UDF = 'TITL'
	AND Screens.groupname = pt.groupname
	AND Screens.TypeName = pt.typename
WHERE 
	pt.UDF != 'TITL'
and	
	pt.groupname = 'permits'
AND 
	pt.TypeName = @permitType
and		
	screens.DataField like '%etrakit%'
ORDER BY 
	pt.groupname, 
	pt.typename,  
	pt.ORDERID

SELECT
	@s = MIN(rid),
	@e = MAX(rid)
FROM #CustomFields

WHILE @s <= @e
BEGIN
	SELECT @fieldName = DateFieldConverted
	FROM #CustomFields
	WHERE rid = @s

	SET @sql = '
	UPDATE f 
	SET DataValue = (SELECT TOP 1 '+@fieldName+' FROM dbo.permit_udf u inner join dbo.permit_main m on m.permit_no = u.permit_no WHERE u.Permit_NO = '''+@permitno+''')
	FROM #CustomFields f
	WHERE rid = ' + CONVERT(VARCHAR(100), @s)+'
	'

	EXEC(@sql)


	SET @s = @s+1
end

SELECT 
	Caption + ' : ' + isnull(convert(nvarchar(500),DataValue),'') as UDFValue,
	Caption,
	isnull(convert(nvarchar(500),DataValue),'') as Datavalue
	--GroupName ,
	--TypeName ,
	--ScreenName ,
	--DataField ,
	--Caption ,
	--DateFieldConverted ,
	--DataValue
FROM #CustomFields
ORDER BY rid




END
GO
