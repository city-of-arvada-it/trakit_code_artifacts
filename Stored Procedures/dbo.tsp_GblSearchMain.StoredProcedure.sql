USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GblSearchMain]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_GblSearchMain]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GblSearchMain]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[tsp_GblSearchMain] (
	@searchString Varchar(500),
	@Top INT = Null,
	@Group Varchar(50) = Null,
	@ActivityGroupsOnly INT = Null,
 	@ExcludedGroups Varchar(250) = Null
 )
 
AS
BEGIN

Declare @SQL Varchar(8000)
Declare @WHERE Varchar(2000)
Declare @vTop Varchar(50)


--SET NOCOUNT ON
	
	
If @Top Is Null Set @Top = 0
-- MLM Change -- Set @vTop = ' Top 1000 '
Set @vTop = ''
If @Top > 0 Set @vTop = ' Top ' + Convert(Varchar,@Top) + ' '
If @ActivityGroupsOnly Is Null Set @ActivityGroupsOnly = 0
If @Group = '' Set @Group = Null
If @ExcludedGroups = '' Set @ExcludedGroups = Null
If @ExcludedGroups Is Not Null 
 Begin
	Set @ExcludedGroups = '''' + @ExcludedGroups + ''''
	Set @ExcludedGroups = REPLACE(@ExcludedGroups,',',''',''')
 End
If LEFT(@searchString,1) <> '%' AND RIGHT(@searchString,1) <> '%' Set @searchString = '%' + @searchString + '%'
Set @searchString = REPLACE(@searchString,'''','''''')

Create Table #Results(RecordID Varchar(30),Loc_RecordID Varchar(30),Tgroup Varchar(8),ActivityNo Varchar(30),Site_APN Varchar(50),Site_Addr Varchar(100), Site_City Varchar(30),RecordType Varchar(30) Default 'Activity')


Print 'Insert Parcels Into #Results'
If @Group Is Not Null
 Begin
	Set @SQL = '
	Insert Into #Results(RecordID,Loc_RecordID,ActivityNo,Tgroup,Site_APN,Site_Addr,Site_City,RecordType)
	SELECT RecordID,Loc_RecordID,ActivityNo,Tgroup,Site_APN,Site_Addr, city,''Parcel''
	From tvw_GblActivityParcels
	WHERE ( site_APN like ''' + @searchString + ''' OR site_addr like ''' + @searchString + ''' OR city like ''' + @searchString + '''
	OR site_subdivision like ''' + @searchString + ''' )
	AND ( tgroup = ''' + @Group + ''' )
	ORDER BY ActivityNo, Loc_RecordID
	'
	Print(@SQL)
	Exec(@SQL)
 End
ELSE
 Begin
	Set @SQL = '
	Insert Into #Results(RecordID,Loc_RecordID,ActivityNo,Tgroup,Site_APN,Site_Addr,Site_City,RecordType)
	SELECT RecordID,Loc_RecordID,ActivityNo,Tgroup,Site_APN,Site_Addr,city, ''Parcel''
	From tvw_GblActivityParcels
	WHERE ( site_APN like ''' + @searchString + ''' OR site_addr like ''' + @searchString + ''' OR city like ''' + @searchString + ''' 
	OR site_subdivision like ''' + @searchString + ''')
	'
	If @ExcludedGroups Is Not Null
		Set @SQL = @SQL + ' AND tgroup NOT IN (' + @ExcludedGroups + ')
		'
	Set @SQL = @SQL + ' ORDER BY Tgroup DESC, ActivityNo, Loc_RecordID 
	'
	Print(@SQL)
	Exec(@SQL)
 End


If @ActivityGroupsOnly = 1
 Begin
	Print 'Insert Into #Results - @ActivityGroupsOnly = 1'
	
	Set @SQL = '
	Insert Into #Results(RecordID,Loc_RecordID,ActivityNo,Tgroup,Site_APN,Site_Addr,Site_City)
	SELECT RecordID,Loc_RecordID,ActivityNo,Tgroup,Site_APN,Site_Addr,Site_City
	From tvw_GblActivities 
	WHERE
		( activityNo like ''' + @searchString + ''' 
		OR site_APN like ''' + @searchString + '''
		OR site_addr like ''' + @searchString + '''
		OR Site_City like ''' + @searchString + '''
		OR owner_name like ''' + @searchString + '''
		OR title like ''' + @searchString + '''
		OR site_alternate_id like ''' + @searchString + '''
		OR site_subdivision like ''' + @searchString + ''' )
		AND ( tgroup in (''PERMIT'',''CASE'',''PROJECT'',''LICENSE2'',''PAYMENT'') )
	'
	If @ExcludedGroups Is Not Null
		Set @SQL = @SQL + ' AND ( tgroup NOT IN (' + @ExcludedGroups + ') )
		'
	Set @SQL = @SQL + ' ORDER BY Tgroup DESC, ActivityNo, Loc_RecordID 
	'
	Print(@SQL)
	Exec(@SQL)
 End
ELSE
 Begin
	If @Group Is Not Null
	 Begin
		Print 'Insert Into #Results - Group = ' + @Group
		
		Set @SQL = '
		Insert Into #Results(RecordID,Loc_RecordID,ActivityNo,Tgroup,Site_APN,Site_Addr,Site_City)
		SELECT RecordID,Loc_RecordID,ActivityNo,Tgroup,Site_APN,Site_Addr,Site_City
		From tvw_GblActivities where
			( activityNo like ''' + @searchString + ''' 
			OR site_APN like ''' + @searchString + '''
			OR site_addr like ''' + @searchString + '''
			OR owner_name like ''' + @searchString + '''
			OR Site_City like ''' + @searchString + '''
			OR title like ''' + @searchString + '''
			OR site_alternate_id like ''' + @searchString + '''
			OR site_subdivision like ''' + @searchString + ''' )
			AND ( tgroup = ''' + @Group + ''' )
		ORDER BY ActivityNo, Loc_RecordID
		'
		Print(@SQL)
		Exec(@SQL)
	 End
	ELSE
	 Begin
		Print 'Insert Into #Results - Group Is Null'
		
		Set @SQL = '
		Insert Into #Results(RecordID,Loc_RecordID,ActivityNo,Tgroup,Site_APN,Site_Addr,Site_City)
		SELECT RecordID,Loc_RecordID,ActivityNo,Tgroup,Site_APN,Site_Addr,Site_City
		From tvw_GblActivities 
		WHERE
			( activityNo like ''' + @searchString + '''
			OR site_APN like ''' + @searchString + '''
			OR site_addr like ''' + @searchString + '''
			OR owner_name like ''' + @searchString + '''
			OR title like ''' + @searchString + '''
			OR site_alternate_id like ''' + @searchString + '''
			OR site_subdivision like ''' + @searchString + ''' )
		'
		If @ExcludedGroups Is Not Null
			Set @SQL = @SQL + ' AND ( tgroup NOT IN (' + @ExcludedGroups + ') )
		'
		Set @SQL = @SQL + ' ORDER BY Tgroup DESC, ActivityNo, Loc_RecordID 
		'
		Print(@SQL)
		Exec(@SQL)
	 End

	Set @SQL = 'Delete #Results 
	Where ActivityNo like ''' + @searchString + ''' AND Tgroup = ''GEO''
	AND Site_APN not like ''' + @searchString + '''
	AND Site_Addr not like ''' + @searchString + '''
	'
	Print(@SQL)
	Exec(@SQL)
 End


Set @SQL = '
SELECT ' + @vTop + '
ACTIVITYNO = tr.ActivityNo,
TITLE, 
ACTIVITYTYPE, 
ACTIVITYSUBTYPE, 
SITE_APN = tr.Site_Apn, 
SITE_ADDR = tr.Site_Addr,
SITE_CITY = tr.Site_City,
OWNER_NAME,
SITE_SUBDIVISION, 
RECORDID = tr.RecordID, 
LOC_RECORDID = tr.Loc_RecordID, 
[STATUS], 
Tgroup = tr.Tgroup, 
x, 
y, 
GEOTYPE, 
SITE_ALTERNATE_ID, 
PARENT_PROJECT_NO, 
PARENT_PERMIT_NO, 
PARENT_SITE_APN, 
PARENT_RECORDID, 
PARENT_BUS_LIC_NO, 
PARENT_AEC_ST_LIC_NO, 
DatePropertyList, 
DATE1, 
DATE2, 
DATE3, 
DATE4, 
DATE5, 
DATE6, 
DATE7, 
DATE8,
tr.RecordType
From #Results tr
LEFT JOIN tvw_GblActivities va
 ON tr.RecordID = va.RecordID AND tr.Tgroup = va.Tgroup AND tr.RecordType = ''Activity'' AND tr.ActivityNo = va.ActivityNo
ORDER BY tr.Tgroup DESC, tr.ActivityNo, tr.Loc_RecordID
 '
Print(@SQL)
Exec(@SQL)

	
END

GO
