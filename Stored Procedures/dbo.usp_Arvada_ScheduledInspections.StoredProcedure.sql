USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_ScheduledInspections]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[usp_Arvada_ScheduledInspections]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_ScheduledInspections]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_Arvada_ScheduledInspections]
	@Startdate datetime,
	@endDate datetime,
	@inspectors nvarchar(max) = null
as
/**********************************************************************************************************************
* Creator:      	 
* Date:         	 
* Title:			 
* Description:  	 
* Examples:    
			EXECUTE dbo.usp_Arvada_ScheduledInspections '12/03/2015','12/04/2015'
					 
* Modifications:  	
  Developer Name		Date		Description
  ------------------	----------	----------------------------------------------------------- 
*
**********************************************************************************************************************/

set nocount on
 
 --------------------------
 --- Declare Variables   --
 --------------------------

 
 --------------------------
 --- Create Temp Tables  --
 --------------------------
 
create table #final
(
	InspectionType varchar(60),
	PermitSubType varchar(60), 
	Inspector varchar(10),
	ScheduledDate datetime,
	PermitNo varchar(15),
	SiteAddr varchar(40),
	InspRecordID varchar(30),
	Result Varchar(60)
)

create table #inspects
(
	inspector nvarchar(500)
)

	 
 --------------------------
 --- Body of Procedure   --
 --------------------------
 
 insert into #inspects
(
	inspector
)
select
	val
from [dbo].[tfn_SSRS_String_To_Table](@Inspectors,'|',1) as b

insert into #final
(
	InspectionType ,
	Inspector ,
	ScheduledDate ,
	PermitNo ,
	SiteAddr ,
	InspRecordID,
	Result,
	PermitSubType
)
select 
	i.InspectionType, 
	I.INSPECTOR, 
	I.SCHEDULED_DATE, 
	m.permit_no, 
	m.site_addr,
	i.RECORDID,
	i.RESULT,
	m.PermitSubType
from dbo.Permit_Inspections i
inner join dbo.permit_main m
	on m.permit_no = i.permit_no
inner join #inspects ins
		on ins.inspector = i.INSPECTOR
where
	i.scheduled_Date >= @StartDate
and i.SCHEDULED_DATE < dateadd(day,1,@EndDate)
and i.scheduled_date is not null
and (i.RESULT is null or i.RESULT = '')

insert into #final
(
	InspectionType ,
	Inspector ,
	ScheduledDate ,
	PermitNo ,
	SiteAddr ,
	InspRecordID,
	Result,
	PermitSubType
)
select 
	i.InspectionType, 
	I.INSPECTOR, 
	I.SCHEDULED_DATE, 
	m.permit_no, 
	m.site_addr,
	i.RECORDID,
	i.Result,
	m.PermitSubType
from dbo.Permit_Inspections i
inner join dbo.permit_main m
	on m.permit_no = i.permit_no
left join #final f
	on f.InspRecordID = i.RECORDID
where
	f.InspRecordID is null
and i.RESULT = 'DENIED'
and exists(
			select 1
			from #final f2
			where 
					f2.PermitNo = m.PERMIT_NO
				and f2.InspectionType = i.InspectionType
				and f2.InspRecordID != i.RECORDID
			)

 

select 
	InspectionType ,
	Inspector ,
	ScheduledDate ,
	PermitNo ,
	isnull(nullif(SiteAddr,''),'No Site Address') as SiteAddr,
	InspRecordID,
	Result,
	PermitSubType
from #final m 
order by
	SiteAddr,
	PermitNo,
	InspectionType,
	ScheduledDate desc
GO
