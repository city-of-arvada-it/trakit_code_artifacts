USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ITR_ExportContactTypes]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_ITR_ExportContactTypes]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ITR_ExportContactTypes]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  
					  
-- =============================================
-- Author:		Rance Richardson
-- Create date: 
-- Description:	Exports Contact types to .csv

-- tsp_ExportContactTypes @DBName='DEV', @FilePath='C:\CRW\iTrakitExports\', @FileName='ContactTypesExport', @ToggleXP_CMDShell=1

-- =============================================
CREATE PROCEDURE [dbo].[tsp_ITR_ExportContactTypes](
@DBName Varchar(50),
@FilePath Varchar(1000),
@FileName Varchar(150),
@ToggleXP_CMDShell INT = 1
)

As

SET NOCOUNT ON

Declare @SQL Varchar(8000)

If @ToggleXP_CMDShell Is Null Set @ToggleXP_CMDShell = 1

IF EXISTS (SELECT * FROM tempdb.sys.objects WHERE object_id = OBJECT_ID(N'tempdb..ContactTypes') AND type in (N'U'))
	Drop Table tempdb..ContactTypes

CREATE TABLE tempdb..ContactTypes([GroupName] varchar(200)
		,[TypeName] varchar(200)
      ,[ContactType] varchar(200)
      ,[OrderID] varchar(200))
      
INSERT INTO tempdb..ContactTypes(GroupName, TypeName, ContactType, OrderID)
VALUES ('GroupName','TypeName', 'ContactType', 'OrderID')

SET @SQL = '
INSERT INTO tempdb..ContactTypes([GroupName],[TypeName],[ContactType],[OrderID])
SELECT
Distinct GroupName, TypeName, Value1 As ContactType, 
CAST(IsNull(ORDERID, 0) As Int) As OrderID
FROM ' + @DBName + '..Prmry_TypesInfo
WHERE Category = ''PEOPLE'' AND
GroupName IN (''CASE'', ''LICENSE2'', ''LICENSES'', ''PERMITS'', ''PROJECTS'', ''VIOLATIONS'')
UNION
SELECT GroupName, ''NONE'' AS TypeName, Value1 AS ContactType, 
CAST(Value2 AS Int) + 900 AS OrderID 
FROM Prmry_Preferences
WHERE GroupName = ''SYSTEM''
 AND Item = ''PEOPLETITLE''
ORDER BY GroupName, TypeName, OrderID'

Print(@SQL)
Exec(@SQL)

SELECT * FROM tempdb..ContactTypes

Declare @vDate Varchar(100)
Declare @OutputFileName Varchar(1000)

Set @vDate = Replace(Convert(Varchar,GetDate(),102),'.','_')
Print(@vDate)

--Set @OutputFileName = '"' + @FilePath + @FileName + '_' + @vDate + '.csv"'
Set @OutputFileName = '"' + @FilePath + @FileName + '.csv"'

If @ToggleXP_CMDShell = 1
 Begin
	--Enable xp_cmdshell
	Set @SQL = '
	EXEC sp_configure ''show advanced options'', 1
	RECONFIGURE
	EXEC sp_configure ''xp_cmdshell'', 1
	RECONFIGURE
	'
	Print(@SQL)
	Exec(@SQL)
 End
 
 select @sql = 'bcp "select * from tempdb..ContactTypes" queryout ' + @OutputFileName + ' -c -t"|" -T -S'
+ @@servername
--Print(@SQL)
exec master..xp_cmdshell @sql

drop Table tempdb..ContactTypes

Set @SQL = '
DELETE ' + @DBName + '.dbo.iTrakitFeeds Where [FileName] = ''' + @FileName + '.csv''

INSERT INTO ' + @DBName + '.dbo.iTrakitFeeds([filename],[data])
SELECT ''' + @FileName + '.csv'', * FROM OPENROWSET(
  BULK N''' + REPLACE(@OutputFileName,'"','') + ''', SINGLE_BLOB
) bt'
Print(@SQL)
Exec(@SQL)


If @ToggleXP_CMDShell = 1
 Begin
	-- Disable xp_cmdshell
	Set @SQL = '
	EXEC sp_configure ''xp_cmdshell'', 0
	RECONFIGURE
	EXEC sp_configure ''show advanced options'', 0
	RECONFIGURE
	'
	Print(@SQL)
	Exec(@SQL)
 End





GO
