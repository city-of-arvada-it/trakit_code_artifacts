USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AutoLicenseCountSupport]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_AutoLicenseCountSupport]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AutoLicenseCountSupport]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =====================================================================
-- Revision History:
--	RB - 10/1/2015 Initial 

-- =====================================================================   	
CREATE Procedure [dbo].[tsp_T9_AutoLicenseCountSupport](
	 @LicenseNo varchar(20), 
	 @PermitNo varchar(20),
	 @LicInspType varchar(60),
	 @LicInspector varchar(60),
	 @PermitTypeName varchar(60), 
	 @isLinkActivities bit,
	 @isTVRecords bit, 
	 @isInspectionType bit, 
	 @isInspector bit, 
	 @isContactDiscrepancy bit

)

As
BEGIN
	Declare @SQLStatement varchar(1500)
	
		
	If @isLinkActivities = 1 and (@PermitNo is not NULL and @PermitNo <> '')
		Begin

			set @SQLStatement = 'Select Count(*) from lnk_activities where ParentActivityNo=' + CHAR(39) + @PermitNo  + CHAR(39) + ' and ChildGroup =' + CHAR(39) + 'LICENSE2' + CHAR(39)

			print @SQLStatement

			exec (@SQLStatement)

		End
	Else If @isTVRecords = 1 and (@LicenseNo is not NULL and @LicenseNo <> '') and (@PermitNo is not NULL and @PermitNo <> '')
		Begin

			--set @SQLStatement = 'select COUNT(*) as [TV-COUNT] from License2_main LM left join geo_ownership GOWN on LM.LOC_RECORDID = GOWN.RECORDID ' + 
			--					' left join permit_main pm on pm.LOC_RECORDID = gown.RECORDID where LM.LICENSE_NO = ' + CHAR(39) + @LicenseNo + CHAR(39) + ' and PM.prefix = ' + CHAR(39) + 'TV' + CHAR(39)


			set @SQLStatement = 'select COUNT(*) as [TV-COUNT] from License2_main LM left join geo_ownership GOWN on LM.LOC_RECORDID = GOWN.RECORDID ' + 
								' left join permit_main pm on pm.LOC_RECORDID = gown.RECORDID where LM.LICENSE_NO = ' + CHAR(39) + @LicenseNo + CHAR(39) + ' and PM.prefix = ' + CHAR(39) + 'TV' + CHAR(39) + 
								' and PM.PERMIT_NO not in(' + CHAR(39) + @PermitNo + CHAR(39) + ')'


			print @SQLStatement

			exec (@SQLStatement)

		End
	Else If @isInspectionType = 1 and (@LicInspType is not NULL and @LicInspType <> '')
		Begin

			set @SQLStatement = 'select Count(*) from Prmry_InspectionControl where groupname = ' + CHAR(39) + 'LICENSE2' + CHAR(39) + 
								' and category = ' + CHAR(39) + 'INSPECTION_TYPE' + CHAR(39) + ' and field01 = ' + CHAR(39) + @LicInspType  + CHAR(39)

			print @SQLStatement

			exec (@SQLStatement)

		End
	Else If @isInspector = 1 and (@LicInspector is not NULL and @LicInspector <> '')
		Begin

			set @SQLStatement = 'select Count(*) from Prmry_InspectionControl where groupname = ' + CHAR(39) + 'LICENSE2' + CHAR(39) + 
								' and category = ' + CHAR(39) +'INSPECTOR' + CHAR(39) + ' and field03 = ' + CHAR(39) + @LicInspector + CHAR(39)

			print @SQLStatement

			exec (@SQLStatement)

		End
	Else If @isContactDiscrepancy = 1 and (@PermitTypeName is not NULL and @PermitTypeName <> '')
		Begin

			set @SQLStatement = 'select count(*) from Prmry_TypesInfo where groupname = ' + CHAR(39) +'PERMITS' + CHAR(39) + 
								' and Category = ' + CHAR(39) + 'PEOPLE' + CHAR(39) + ' and [TYPENAME] = ' + CHAR(39) + @PermitTypeName + CHAR(39) + 
                                ' and VALUE1 not in (select Value1 from Prmry_TypesInfo where groupname = ' + CHAR(39) + 'LICENSE2' + CHAR(39) + 
								' and Category = ' + CHAR(39) + 'PEOPLE' + CHAR(39) + ' and [TYPENAME] = ' + CHAR(39) + @PermitTypeName + CHAR(39) + ')'
			print @SQLStatement
			
			exec (@SQLStatement)

		End
	

End
 


GO
