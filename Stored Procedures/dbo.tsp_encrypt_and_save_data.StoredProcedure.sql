USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_encrypt_and_save_data]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_encrypt_and_save_data]
GO
/****** Object:  StoredProcedure [dbo].[tsp_encrypt_and_save_data]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[tsp_encrypt_and_save_data] 
       @recordid AS VARCHAR(50),
       @data_to_encrypt AS VARCHAR(30),
       @field AS VARCHAR(30),
       @table AS VARCHAR(30) = NULL,
	   @encryptedData AS VARBINARY(128) = NULL OUTPUT
AS
BEGIN
       OPEN SYMMETRIC KEY SymKey
       DECRYPTION BY CERTIFICATE EncryptCert;
	   DECLARE @etbl AS table(encryptedTaxID VARBINARY(128))
       IF @table IS NULL 
              BEGIN
                     IF @field = 'protected_field1' 
                     BEGIN
                           IF EXISTS (SELECT license_recordid FROM protected_data WHERE license_recordid = @recordid)
                           BEGIN
                                  UPDATE protected_data 
                                  SET protected_field1 = ENCRYPTBYKEY(KEY_GUID('SymKey'), @data_to_encrypt)
                                  WHERE license_recordid = @recordid; 
                            END
                     ELSE
                           INSERT INTO protected_data
                           (license_recordid, protected_field1)
                           VALUES
                           (@recordid, ENCRYPTBYKEY(KEY_GUID('SymKey'), @data_to_encrypt))
                     End

                     IF @field = 'protected_field2' 
                     BEGIN
                           IF EXISTS (SELECT license_recordid FROM protected_data WHERE license_recordid = @recordid)
                           BEGIN
                                  UPDATE protected_data 
                                  SET protected_field2 = ENCRYPTBYKEY(KEY_GUID('SymKey'), @data_to_encrypt)
                                  WHERE license_recordid = @recordid; 
                           END
                     ELSE
                           INSERT INTO protected_data
                           (license_recordid, protected_field2)
                           VALUES
                           (@recordid, ENCRYPTBYKEY(KEY_GUID('SymKey'), @data_to_encrypt))
                     End

                     IF @field = 'protected_field3' 
                     BEGIN
                           IF EXISTS (SELECT license_recordid FROM protected_data WHERE license_recordid = @recordid)
                           BEGIN
                                  UPDATE protected_data 
                                  SET protected_field3 = ENCRYPTBYKEY(KEY_GUID('SymKey'), @data_to_encrypt)
                                  WHERE license_recordid = @recordid; 
                           END
                     ELSE
                           INSERT INTO protected_data
                           (license_recordid, protected_field3)
                           VALUES
                           (@recordid, ENCRYPTBYKEY(KEY_GUID('SymKey'), @data_to_encrypt))
                     End

                     IF @field = 'protected_field4' 
                     BEGIN
                           IF EXISTS (SELECT license_recordid FROM protected_data WHERE license_recordid = @recordid)
                           BEGIN
                                  UPDATE protected_data 
                                  SET protected_field4 = ENCRYPTBYKEY(KEY_GUID('SymKey'), @data_to_encrypt)
                                  WHERE license_recordid = @recordid; 
                           END
                     ELSE
                           INSERT INTO protected_data
                           (license_recordid, protected_field4)
                           VALUES
                           (@recordid, ENCRYPTBYKEY(KEY_GUID('SymKey'), @data_to_encrypt))
                     End

                     IF @field = 'protected_field5' 
                     BEGIN
                           IF EXISTS (SELECT license_recordid FROM protected_data WHERE license_recordid = @recordid)
                           BEGIN
                                  UPDATE protected_data 
                                  SET protected_field5 = ENCRYPTBYKEY(KEY_GUID('SymKey'), @data_to_encrypt)
                                  WHERE license_recordid = @recordid; 
                           END
                     ELSE
                           INSERT INTO protected_data
                           (license_recordid, protected_field5)
                           VALUES
                           (@recordid, ENCRYPTBYKEY(KEY_GUID('SymKey'), @data_to_encrypt))
                     End
              END
       ELSE
              BEGIN
                     IF @table = 'license2_main'
                           UPDATE LICENSE2_MAIN 
                           SET tax_id = ENCRYPTBYKEY(KEY_GUID('SymKey'), @data_to_encrypt)
						   OUTPUT inserted.TAX_ID INTO @etbl
                           WHERE RECORDID = @recordid;
                     IF @table = 'aec_main'
                           UPDATE AEC_MAIN
                           SET tax_id = ENCRYPTBYKEY(KEY_GUID('SymKey'), @data_to_encrypt)
						   OUTPUT inserted.TAX_ID INTO @etbl
                           WHERE RECORDID = @recordid;
                     If @table = 'ApplicationProtectedData'
                     BEGIN
                           IF @field = 'protected_field1' 
                           BEGIN
                                  IF EXISTS (SELECT application_id FROM ApplicationProtectedData WHERE application_Id = @recordid)
                                  BEGIN
                                         UPDATE ApplicationProtectedData 
                                         SET protected_field1 = ENCRYPTBYKEY(KEY_GUID('SymKey'), @data_to_encrypt)
                                         WHERE application_id = @recordid; 
                                  END
                           ELSE
                                  INSERT INTO ApplicationProtectedData
                                  (application_id, protected_field1)
                                  VALUES
                                  (@recordid, ENCRYPTBYKEY(KEY_GUID('SymKey'), @data_to_encrypt))
                           End

                           IF @field = 'protected_field2' 
                           BEGIN
                                  IF EXISTS (SELECT application_id FROM ApplicationProtectedData WHERE application_id = @recordid)
                                  BEGIN
                                         UPDATE ApplicationProtectedData 
                                         SET protected_field2 = ENCRYPTBYKEY(KEY_GUID('SymKey'), @data_to_encrypt)
                                         WHERE application_id = @recordid; 
                                  END
                           ELSE
                                  INSERT INTO ApplicationProtectedData
                                  (application_id, protected_field2)
                                  VALUES
                                  (@recordid, ENCRYPTBYKEY(KEY_GUID('SymKey'), @data_to_encrypt))
                           End

                           IF @field = 'protected_field3' 
                           BEGIN
                                  IF EXISTS (SELECT application_id FROM ApplicationProtectedData WHERE application_id = @recordid)
                                  BEGIN
                                         UPDATE ApplicationProtectedData 
                                         SET protected_field3 = ENCRYPTBYKEY(KEY_GUID('SymKey'), @data_to_encrypt)
                                         WHERE application_id = @recordid; 
                                  END
                           ELSE
                                  INSERT INTO ApplicationProtectedData
                                  (application_id, protected_field3)
                                  VALUES
                                  (@recordid, ENCRYPTBYKEY(KEY_GUID('SymKey'), @data_to_encrypt))
                           End

                           IF @field = 'protected_field4' 
                           BEGIN
                                  IF EXISTS (SELECT application_id FROM ApplicationProtectedData WHERE application_id = @recordid)
                                  BEGIN
                                         UPDATE ApplicationProtectedData 
                                         SET protected_field4 = ENCRYPTBYKEY(KEY_GUID('SymKey'), @data_to_encrypt)
                                         WHERE application_id = @recordid; 
                                  END
                           ELSE
                                  INSERT INTO ApplicationProtectedData
                                  (application_id, protected_field4)
                                  VALUES
                                  (@recordid, ENCRYPTBYKEY(KEY_GUID('SymKey'), @data_to_encrypt))
                           End

                           IF @field = 'protected_field5' 
                           BEGIN
                                  IF EXISTS (SELECT application_id FROM ApplicationProtectedData WHERE application_id = @recordid)
                                  BEGIN
                                         UPDATE ApplicationProtectedData 
                                         SET protected_field5 = ENCRYPTBYKEY(KEY_GUID('SymKey'), @data_to_encrypt)
                                         WHERE application_id = @recordid; 
                                  END
                           ELSE
                                  INSERT INTO ApplicationProtectedData
                                  (application_id, protected_field5)
                                  VALUES
                                  (@recordid, ENCRYPTBYKEY(KEY_GUID('SymKey'), @data_to_encrypt))
                           End
                     END
					 SELECT @encryptedData = encryptedTaxID FROM @etbl
					 RETURN @encryptedData	
              END 
       CLOSE SYMMETRIC KEY symkey
END 

GO
