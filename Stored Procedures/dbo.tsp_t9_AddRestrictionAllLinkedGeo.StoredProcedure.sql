USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_t9_AddRestrictionAllLinkedGeo]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_t9_AddRestrictionAllLinkedGeo]
GO
/****** Object:  StoredProcedure [dbo].[tsp_t9_AddRestrictionAllLinkedGeo]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  
-- =====================================================================
-- Revision History:
--	RB - 9/25/2014 - Added to Source Safe

-- =====================================================================  

Create Procedure [dbo].[tsp_t9_AddRestrictionAllLinkedGeo](
	   	   @ProjectNo			varchar(80),
		   @RestrictionMessage varchar(2000),
		   @ConditionID varchar(80),
		   @RestrictionFlag varchar(2),
		   @UserID varchar(4)
	   )
	   as
	   begin
			--Declare @projectNO varchar(100)
			--Declare @Restrictionmessage varchar(100)
			--Declare @ConditionID varchar(100)
			--declare @UserID varchar(4)
			--Declare @RestrictionFlag varchar(2)
			
			--set @RestrictionFlag = '1'
			--set @projectNO = 'Z14-0003'
			--set @Restrictionmessage = 'New rule update'
			--set @ConditionID = 'MJ:1409110751330020'
			--set @UserID = 'MJ'


			declare @RecordID as varchar(100)
		    
				--select @rand = right(('000000'+ ltrim(str(Rand() * 1000000))),6)
				set @RecordID = @userID + ':' + right(('00' + str(DATEPART(yy, GETDATE()))),1)
					+ right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)
					+ right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
					+ right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)
					+ right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
					+ right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)			
			
			
		if @RestrictionFlag = '1'
				begin

				update Project_Conditions2 set RestrictionFlag = @RestrictionFlag, RestrictionMessage = @Restrictionmessage where RecordID = @ConditionID
				-- Get all geo records linked to the proejct


				--RB Check if table exists before creating it
				IF OBJECT_ID('tempdb..#tempRestrict') IS NOT NULL  
					drop table #tempRestrict

				Create Table #tempRestrict(ID  int identity, Date_Added datetime, date_cleared datetime, other_notes varchar(2000), restriction_notes varchar(2000),  Restriction_type varchar(60),
				user_added varchar(4), user_cleared varchar(4), loc_recordID varchar(30), recordID varchar(30), site_apn varchar(50))
			
				insert into #tempRestrict select GetDate(), null,  @RestrictionMessage, @ProjectNo, 'Condition Rule', null, null, a.recordID, @RecordID, a.site_apn 
				from geo_Ownership a where a.recordid = (select loc_recordID from project_main where project_no = @ProjectNo)

				insert into #tempRestrict select GetDate(), null, @RestrictionMessage, @ProjectNo, 'Condition Rule', null, null, a.recordID,@RecordID, a.site_apn 
				from geo_Ownership a where a.recordid in (select loc_recordID from project_parcels where project_no = @ProjectNo)
			
				--insert new Geo_Restriction2 (mostly hardcoded values) 
				
				Update #tempRestrict set RecordID = RecordID + Right('000000' + convert(varchar,((Select Count(RecordID) From Geo_RESTRICTIONS2) +  ID)),6)
				
				Insert Into Prmry_Notes(UserID,DateEntered,ActivityGroup,ActivityRecordID,ActivityNo,SubGroup,SubGroupRecordID,Notes)
				(Select
				@UserID,
				GetDate(),
				'GEO',
				tr.loc_recordID,
				tr.loc_recordID,
				'RESTRICTION',
				tr.RecordID,
				@RestrictionMessage
				from #tempRestrict tr) 
				
				Insert into Geo_RESTRICTIONS2 Select Date_Added, date_cleared, other_notes, restriction_notes,  Restriction_type,
				user_added, user_cleared, loc_recordID, recordID, site_apn from #tempRestrict
				drop table #tempRestrict
			end
			
 	   end



GO
