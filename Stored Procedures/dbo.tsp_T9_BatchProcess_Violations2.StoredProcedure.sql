USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_BatchProcess_Violations2]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_BatchProcess_Violations2]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_BatchProcess_Violations2]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  


create PROCEDURE [dbo].[tsp_T9_BatchProcess_Violations2] 
	@ListViolationTypes as UpdateFields readonly,
	@ListUpdateFields as UpdateFields readonly,
	@ListTableAndRecords as MainTableRecords readonly
              
AS
BEGIN

	Declare @SQL Varchar(8000)
	Declare @TableName varchar(50)
	Declare @KeyField varchar(50)  
	Declare @ColumnName varchar(50) 
	Declare @ColumnValue varchar(255)
	Declare @RowNum int
	Declare @RowId Int
	Declare @AltRowNum int
	Declare @AltRowId Int
	Declare @UserID as varchar(10)
	Declare @TblName as varchar(100)
	Declare @Contact as varchar(100)
	Declare @GroupName as varchar(255)

	--Drop Tables

	IF object_id('tempdb..#ForInsertViolationTypes') is not null drop table #ForInsertViolationTypes
	IF object_id('tempdb..#ForInsertIntoPrmryAuditTrail') is not null drop table #ForInsertIntoPrmryAuditTrail
	IF object_id('tempdb..#ListViolationTypes') is not null drop table #ListViolationTypes
	IF object_id('tempdb..#ListUpdateFields') is not null drop table #ListUpdateFields
	IF object_id('tempdb..#MainTableRec') is not null  drop table #MainTableRec

	--Create tables
	Create Table #ForInsertViolationTypes(
		ACTIVITY_NO varchar(15),
		VIOLATION_TYPE varchar(100),
		VIOLATION_NOTES varchar(2000),
		REMARKS varchar(100),
		LOCATION varchar(500),
		DATE_OBSERVED datetime,
		DATE_CORRECTED datetime,
		STATUS varchar(30),
		USERID varchar(4),
		RECORDID varchar(30), 
		ForRecID int identity)

	--Create table to be used for insert into Audit
	Create Table #ForInsertIntoPrmryAuditTrail(
		CURRENT_VALUE varchar(2000),
		FIELD_NAME varchar(50),
		ORIGINAL_VALUE varchar(2000),
		PRIMARY_KEY_VALUE varchar(50),
		SQL_ACTION varchar(1),
		TABLE_NAME varchar(50),
		USER_ID varchar(4), 
		TRANSACTION_DATETIME datetime)

	--Populate table variables
	select * into #ListViolationTypes from  @ListViolationTypes

	select * into #ListUpdateFields from @ListUpdateFields

	select * into #MainTableRec from @ListTableAndRecords

	-- Start populating table (this should create all combinations for insert)
	insert into #ForInsertViolationTypes(ACTIVITY_NO,VIOLATION_TYPE) 
	select MTR.ActivityNo, lvt.Value from #ListViolationTypes lvt, #MainTableRec MTR

	--Populate other variables
	set @TableName = (Select distinct tblName from @ListTableAndRecords)

	--use function to get key field
	select @KeyField = dbo.tfn_ActivityNoField(@TableName)

	--Update temp table based
	select @RowId=MAX(RowID) FROM #ListUpdateFields     
	Select @RowNum = Count(*) From #ListUpdateFields      
	WHILE @RowNum > 0                          
	BEGIN   

		select @ColumnName = ColumnName from #ListUpdateFields where RowID= @RowId    
		select @ColumnValue = Value from #ListUpdateFields where RowID= @RowId  

		if len(ltrim(rtrim(@ColumnValue))) > 0 and @ColumnName ='UserID'
		Begin
			set @UserID = @ColumnValue
		End

		if len(ltrim(rtrim(@ColumnValue))) > 0 and @ColumnName ='GroupName'
		Begin
			set @GroupName = @ColumnValue
		End 

		if len(ltrim(rtrim(@ColumnValue))) > 0 and @ColumnName ='TblName'
		Begin
			set @TblName = @ColumnValue 
		End 
		if len(ltrim(rtrim(@ColumnValue))) > 0 and @ColumnName not in ('UserID', 'GroupName','TblName','CONTACT')
		Begin
			set @SQL = 'Update #ForInsertViolationTypes set ' + @ColumnName +  ' = ''' + @ColumnValue + ''''
			exec(@sql)
		End 
                                  
		select top 1 @RowId=RowID from #ListUpdateFields where RowID < @RowId order by RowID desc
		set @RowNum = @RowNum - 1                             
       
	END

	update #ForInsertViolationTypes set RECORDID ='BPRO:' + right(('00' + str(DATEPART(yy, GETDATE()))),1)
	+ right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)
	+ right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
	+ right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)
	+ right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
	+ right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)
	+ right(('000000'+ ltrim(str(ForRecID))),6);

	set @SQL = 'Update #ForInsertViolationTypes set Userid = '
	set @SQL = @SQL + '''' + @UserID + ''''
	exec(@sql)

	set @SQL = 'INSERT INTO '
	set @SQL = @SQL +    @TblName 
	set @SQL = @SQL + ' ('
	set @SQL = @SQL +    @KeyField  
	set @SQL = @SQL + ', VIOLATION_TYPE, VIOLATION_NOTES, STATUS, REMARKS, LOCATION, DATE_OBSERVED, DATE_CORRECTED,USERID, RECORDID)
	SELECT       ACTIVITY_NO, VIOLATION_TYPE, VIOLATION_NOTES, STATUS, REMARKS, LOCATION, DATE_OBSERVED, DATE_CORRECTED, USERID, RECORDID
	FROM   #ForInsertViolationTypes' 

	print(@SQL)

	exec(@SQL)


       --insert into #ForInsertIntoPrmryAuditTrail(TABLE_NAME,PRIMARY_KEY_VALUE ) select tblName, ActivityNo from #MainTableRec

       --insert into Prmry_AuditTrail (CURRENT_VALUE, PRIMARY_KEY_VALUE, SQL_ACTION, TABLE_NAME,TRANSACTION_DATETIME, USER_ID) select @SQL2, PRIMARY_KEY_VALUE, 'U', @TblName, getdate(), @UserID from #ForInsertIntoPrmryAuditTrail


END






GO
