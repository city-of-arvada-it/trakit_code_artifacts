USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_InvoiceCreationInvoiceTo]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_InvoiceCreationInvoiceTo]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_InvoiceCreationInvoiceTo]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

					  
-- ==========================================================================================
-- Revision History:
--	  2014-02-04 - MLM - added to sourcesafe
--	  2014-06-16 - RRR - add City, State, Zip to Address1 to get full address
--	  2016-03-22 - RB  - TKT_037 - Billing Address Moved Out Of Invoice_Main 
--	  2016-04-19 - RGB - TKT_051 Add unique identifier for contact to Invoice_Main table
--    2016-05-10 - RB  - TKT_051 - Add RecordID + Group for contact to Invoice_Main table
--    2017-04-07 - MK  - TKT_8205 - if the contact name has more than 30 characters SP was erroring out
--    2019-08-02 - SNK - Altered ContactRecordID to hold 40 char instead of 20
-- ==========================================================================================		


CREATE procedure [dbo].[tsp_T9_InvoiceCreationInvoiceTo]
	@activities varchar(8000)
as
declare @activitiestemp varchar(8000)
declare @section varchar(100)

create table #tempcontacts (NAME varchar(100),ID varchar(35),[TYPE] varchar(10),Module varchar(10),ActNo varchar(30),Address1 varchar(600), 
							[Address2] varchar(600), [City] varchar(60), [State] varchar(2), [ZipCode] varchar(10), [NameType] varchar(60), [ContactRecordID] varchar(40)) 

set @activitiestemp = @activities
--set @activitiestemp = 'PROJECT|PAPP1302-0002,CASE|GRF1311-0005'
while LEN(@activitiestemp) > 0
begin
	declare @group varchar(15)
	declare @actno varchar(30)
	declare @contacttable varchar(600)
	declare @actnofield varchar(40) 
	declare @sql varchar(2000)
	if CHARINDEX(',',@activitiestemp) > 0
	begin
		set @section = SUBSTRING(@activitiestemp,0,CHARINDEX(',',@activitiestemp))
		set @activitiestemp = SUBSTRING(@activitiestemp,LEN(@section) + 2,LEN(@activitiestemp))
	end
	else
	begin
		set @section = @activitiestemp
		set @activitiestemp = null
	end
	set @group = SUBSTRING(@section,0,CHARINDEX('|',@section,0))
	set @actno = SUBSTRING(@section,CHARINDEX('|',@section,0) + 1,LEN(@section) - (len(@group) + 1))
	
	set @contacttable = 
	(
		case
			when @group = 'PROJECT' then 'project_people'
			when @group = 'PERMIT' then 'permit_people'
			when @group = 'CASE' then 'case_people'
			when @group = 'LICENSE2' then 'license2_people'
			when @group = 'AEC' then 'aec_people'
		end
	)
	set @actnofield = 
	(
		case
			when @group = 'PROJECT' then 'PROJECT_NO'
			when @group = 'PERMIT' then 'PERMIT_NO'
			when @group = 'CASE' then 'CASE_NO'
			when @group = 'LICENSE2' then 'LICENSE_NO'
			when @group = 'AEC' then 'ST_LIC_NO'
		end
	)

	 set @sql = 'insert into #tempcontacts (NAME,ID,[TYPE],Module,ActNo,Address1, [Address2],[City], [State], [ZipCode], [NameType],[ContactRecordID]) select NAME,ID,''USER'' as [TYPE],''' + @group + ''',' + @actnofield + ',Address1 AS [Address1], Address2 as [Address2],  
	  City as [City], State as [State], Zip as [ZipCode], NameType as [NameType], RECORDID as [ContactRecordID]
	  from ' + @contacttable + ' where ' + @actnofield + ' = ''' + @actno + ''' and ID is null union ' +
      'select NAME,SUBSTRING(ID,(CHARINDEX('':'',ID) + 1),(LEN(ID) -    
                  CHARINDEX('':'',ID))) as ID, ''GEOOWNER'' as [TYPE], ''' + @group + ''',' + @actnofield + ',
                  Address1 AS [Address1], Address2 as [Address2],  
				  City as [City], State as [State], Zip as [ZipCode], NameType as [NameType], RECORDID as [ContactRecordID] from ' + @contacttable + '
                  where ' + @actnofield + ' = ''' + @actno + ''' and ID is not null and ID like ''OL:%'' union
                  select NAME, SUBSTRING(ID,(CHARINDEX('':'',ID) + 1),(LEN(ID) - CHARINDEX('':'',ID))) as ID, ''AEC'' as [TYPE], ''' + @group + ''',' + @actnofield + ',Address1 AS [Address1], 
				  Address2 as [Address2], City as [City], State as [State], Zip as [ZipCode], NameType as [NameType], RECORDID as [ContactRecordID]
                  from ' + @contacttable + ' where ' + @actnofield + ' = ''' + @actno + ''' and ID is not null and ID like ''C:%'' union
                  select NAME, SUBSTRING(ID,(CHARINDEX('':'',ID) + 1),(LEN(ID) - CHARINDEX('':'',ID))) as ID, ''LICENSE'' as [TYPE], ''' + @group + ''',' + @actnofield + ',Address1 AS [Address1], 
				  Address2 as [Address2], City as [City], State as [State], Zip as [ZipCode], NameType as [NameType], RECORDID as [ContactRecordID]
                  from ' + @contacttable + ' where ' + @actnofield + ' = ''' + @actno + ''' and ID is not null and ID like ''L2:%'''

	--print(@sql)
	exec(@sql)
end
select distinct NAME,ID,[type],Module,ActNo,[Address1], [Address2], [City], [State],[ZipCode], [NameType], [ContactRecordID]
from #tempcontacts
drop table #tempcontacts
GO
