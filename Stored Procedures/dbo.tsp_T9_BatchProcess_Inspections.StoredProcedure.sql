USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_BatchProcess_Inspections]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_BatchProcess_Inspections]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_BatchProcess_Inspections]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  

                                      

create PROCEDURE [dbo].[tsp_T9_BatchProcess_Inspections] 
              @ListInspectionTypes as UpdateFields readonly,
			  @ListUpdateFields as UpdateFields readonly,
              @ListTableAndRecords as MainTableRecords readonly
              
AS
BEGIN

	Declare @SQL Varchar(8000)
	Declare @SQL2 Varchar(1000)
	Declare @WhereClause varchar(255)
	Declare @UpdateStmt varchar(50) 
	Declare @TableName varchar(50) 
	Declare @KeyField varchar(50) 
	Declare @ColumnName varchar(50) 
	Declare @ColumnValue varchar(255)
	Declare @RowNum int
	Declare @RowId Int
	Declare @InspRowNum int
	Declare @InspRowId Int
	Declare @UserID as varchar(10)
	Declare @TblName as varchar(100)
	Declare @Inspector as varchar(100)
	Declare @GroupName as varchar(255)

--Drop Tables

	IF object_id('tempdb..#ForInsertInspectionTypes') is not null drop table #ForInsertInspectionTypes
	IF object_id('tempdb..#ForInsertIntoPrmryAuditTrail') is not null drop table #ForInsertIntoPrmryAuditTrail
	IF object_id('tempdb..#ListInspectionTypes') is not null drop table #ListInspectionTypes
	IF object_id('tempdb..#ListUpdateFields') is not null drop table #ListUpdateFields
	IF object_id('tempdb..#MainTableRec') is not null  drop table #MainTableRec

	--Create tables
	Create Table #ForInsertInspectionTypes(
		ACTIVITY_NO varchar(15),
		REMARKS varchar(40),
		InspectionType varchar(60),
		INSPECTOR varchar(100),
		SCHEDULED_DATE datetime,
		SCHEDULED_TIME varchar(10),
		COMPLETED_DATE datetime,
		COMPLETED_TIME varchar(10),
		DURATION int,
		RESULT varchar(20),
		NOTES varchar(2000),
		LOCKID varchar(30),
		RECORDID varchar(30), 
		ForRecID int identity)

	--Create table to be used for insert into Audit
	Create Table #ForInsertIntoPrmryAuditTrail(
		CURRENT_VALUE varchar(2000),
		FIELD_NAME varchar(50),
		ORIGINAL_VALUE varchar(2000),
		PRIMARY_KEY_VALUE varchar(50),
		SQL_ACTION varchar(1),
		TABLE_NAME varchar(50),
		USER_ID varchar(4), 
		TRANSACTION_DATETIME datetime)

	--Populate table variables
	select * into #ListInspectionTypes from  @ListInspectionTypes

	select * into #ListUpdateFields from @ListUpdateFields

	select * into #MainTableRec from @ListTableAndRecords

	-- Start populating table (this should create all combinations for insert)
	insert into #ForInsertInspectionTypes(ACTIVITY_NO,InspectionType) 
	select MTR.ActivityNo, lit.Value from #ListInspectionTypes lit, #MainTableRec MTR

	--Populate other variables
	set @TableName = (Select distinct tblName from @ListTableAndRecords)

	--use function to get key field
	select @KeyField = dbo.tfn_ActivityNoField(@TableName)

	--Update temp table based; make 2 passes, 1 for everything else, then 1 for inspector
       select @RowId=MAX(RowID) FROM #ListUpdateFields     
       Select @RowNum = Count(*) From #ListUpdateFields      
       WHILE @RowNum > 0                          
       BEGIN   

              select @ColumnName = ColumnName from #ListUpdateFields where RowID= @RowId    
              select @ColumnValue = Value from #ListUpdateFields where RowID= @RowId  

              if len(ltrim(rtrim(@ColumnValue))) > 0 and @ColumnName ='UserID'
                     Begin
                           set @UserID = @ColumnValue
                     End

              if len(ltrim(rtrim(@ColumnValue))) > 0 and @ColumnName ='GroupName'
                     Begin
                           set @GroupName = @ColumnValue
                                            print('grp:')
                                            print(@GroupName)
                     End 

			if len(ltrim(rtrim(@ColumnValue))) > 0 and @ColumnName ='TblName'
                     Begin
                           set @TblName = @ColumnValue 
                     End 
              if len(ltrim(rtrim(@ColumnValue))) > 0 and @ColumnName not in ('UserID', 'GroupName','TblName','INSPECTOR')
                     Begin
                           set @SQL = 'Update #ForInsertInspectionTypes set ' + @ColumnName +  ' = ''' + @ColumnValue + ''''
                           exec(@sql)
                     End 
                                    
              select top 1 @RowId=RowID from #ListUpdateFields where RowID < @RowId order by RowID desc
              set @RowNum = @RowNum - 1                             
       
       END

	   --Get Inspector
	   print('inspector')
	  select @InspRowId=MAX(RowID) FROM #ListUpdateFields     
       Select @InspRowNum = Count(*) From #ListUpdateFields      
       WHILE @InspRowNum > 0   
	   BEGIN

	   print('inspector1')
              select @ColumnName = ColumnName from #ListUpdateFields where RowID= @InspRowNum    
              select @ColumnValue = Value from #ListUpdateFields where RowID= @InspRowNum  

			  print( upper(substring(ltrim(rtrim(@ColumnValue)),1,7)))
                           
              --Now the fun...set inspector while checking for default                             
              if len(ltrim(rtrim(@ColumnValue))) > 0 and @ColumnName ='INSPECTOR' and upper(substring(ltrim(rtrim(@ColumnValue)),1,7)) <> 'DEFAULT'
                     Begin

      --               print('insp')
      --               print(@ColumnName)
      --               print(@ColumnValue)
					 --print(upper(substring(ltrim(rtrim(@ColumnValue)),1,7)))

                           set @Inspector = @ColumnValue 
                           print(@Inspector)

                           --So they have selected a specific inspector
                           
                                  set @Inspector = (select distinct Field01 from prmry_inspectioncontrol where field03 = @inspector) 
                                  --print(@Inspector)
                                  --print(@InspRowNum)
                                  --print(@ColumnName)
                                  --print(@ColumnValue)
                                  Update #ForInsertInspectionTypes set INSPECTOR = @Inspector

                                  select * from #ForInsertInspectionTypes
                           END


                           --Now the fun...set inspector while checking for default                             
              if len(ltrim(rtrim(@ColumnValue))) > 0 and @ColumnName ='INSPECTOR' and upper(substring(ltrim(rtrim(@ColumnValue)),1,7)) = 'DEFAULT'
                     
					 Begin

                        print('def insp')
						print(@GroupName)
						   
						set @SQL = 'Update #ForInsertInspectionTypes set INSPECTOR = (select  pic1.field01
						from prmry_inspectioncontrol pic1 where GroupName like '
						--print(@SQL)
						set @SQL = @SQL + '''' +   @GroupName + '%' + ''''
						--print(@SQL)
						set @SQL = @SQL + ' and Category = ''inspector'' and pic1.FIELD03 in (select  LEFT(pic2.FIELD05, isnull(nullif(CHARINDEX(''('', pic2.FIELD05),0) - 1, 8000)) from prmry_inspectioncontrol pic2 where pic2.GroupName like '
						--print(@SQL)
						set @SQL = @SQL + '''' +   @GroupName + '%' + ''''
						--print(@SQL)
						set @SQL = @SQL + ' and pic2.Category = ''INSPECTION_TYPE'' and #ForInsertInspectionTypes.InspectionType = pic2.Field01))'
						print('after')

						print(@SQL)

						exec(@SQL)

						select * from #ForInsertInspectionTypes
                    
					END 
                     
              select top 1 @InspRowiD=RowID from #ListUpdateFields where RowID < @InspRowid order by RowID desc
              set @inspRowNum = @InspRowNum - 1                                
       
       END

update #ForInsertInspectionTypes set RECORDID ='BPRO:' + right(('00' + str(DATEPART(yy, GETDATE()))),1)
 + right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)
 + right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
 + right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)
 + right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
 + right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)
 + right(('000000'+ ltrim(str(ForRecID))),6);



set @SQL = 'INSERT INTO '
print(@SQL)
set @SQL = @SQL +    @TblName 
print(@SQL)
set @SQL = @SQL + ' ('
set @SQL = @SQL +    @KeyField  
set @SQL = @SQL + ', InspectionType, INSPECTOR, SCHEDULED_DATE, SCHEDULED_TIME, COMPLETED_DATE, COMPLETED_TIME, RESULT, NOTES, RECORDID) SELECT ACTIVITY_NO, InspectionType, INSPECTOR, SCHEDULED_DATE, SCHEDULED_TIME, COMPLETED_DATE, COMPLETED_TIME, RESULT, NOTES, RECORDID FROM #ForInsertInspectionTypes' 


print(@SQL)

 exec(@SQL)

select * from #ForInsertInspectionTypes



--select * from #ForInsertInspectionTypes

       --set @SQL = left(@SQL, (len(@sql)-1))
       
       ----For Audit
       --set @SQL2 = @SQL

       --set @SQL = @SQL + @WhereClause + ';'
              
       --print(@SQL)
       --exec(@SQL)

       --insert into #ForInsertIntoPrmryAuditTrail(TABLE_NAME,PRIMARY_KEY_VALUE ) select tblName, ActivityNo from #MainTableRec

       --insert into Prmry_AuditTrail (CURRENT_VALUE, PRIMARY_KEY_VALUE, SQL_ACTION, TABLE_NAME,TRANSACTION_DATETIME, USER_ID) select @SQL2, PRIMARY_KEY_VALUE, 'U', @TblName, getdate(), @UserID from #ForInsertIntoPrmryAuditTrail




END









GO
