USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_SiteDevCheckListNotesPosting]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[usp_Arvada_SiteDevCheckListNotesPosting]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_SiteDevCheckListNotesPosting]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE procedure [dbo].[usp_Arvada_SiteDevCheckListNotesPosting]

as

/*

J. Hutchens - 09/19/2018
This is intended to grab all comments in the site dev check list and stuff them into a note on the inspection
This then allows the contractor on the permit to see the comments and know what to address via eTrakit.  
This replaces a manual and labor intensive process that made Site Dev people grumpy

begin tran
exec usp_Arvada_SiteDevCheckListNotesPosting
commit
rollback


 

select * 
from prmry_notes (nolock)
where activityno = 'SITE18-00002'
and subgrouprecordid = 'CRW:1805140906510074'

select * 
from prmry_notes
where activityno = 'SITE18-00001'
and subgrouprecordid = 'CRW:1806070956120169'

delete
from prmry_notes
where activityno = 'SITE18-00002'
and subgrouprecordid = 'CRW:1805140906510074'

delete
from prmry_notes
where activityno = 'SITE18-00001'
and subgrouprecordid = 'CRW:1806070956120169'


*/


begin

set nocount on

--create table dbo.ArvadaCheckListRun
--(
--	RecordID varchar(50),
--	SubRecordID varchar(50),
--	LastRunDate datetime
--)


if object_id('tempdb.dbo.#Raw') is not null drop table #Raw
create table #Raw
(
	recordid varchar(50),
	subrecordid varchar(50),	
	UpdateDate datetime,
	RollupNote nvarchar(max),
	itemorder int,
	UserID varchar(25)
)

insert into #Raw
(
	recordid ,
	subrecordid ,
	UpdateDate ,
	RollupNote,
	itemorder,
	UserID
)
SELECT 
	i.PERMIT_NO,
	i.RECORDID,
	case 
		when cl2.lastupdate is null and v.checkDate is not null then v.checkdate
		when v.checkdate > cl2.LastUpdate then v.checkdate 
				else cl2.LastUpdate  end as UpdateDate,
	'Checklist Comments: ' + cli.ItemText + ' : ' 
				 + case 
						when v.itemvalue = 1 then 'Pass '
						when v.itemvalue = 0 then 'N/A '
						when v.itemvalue = -1 then 'Fail '
						else ' ' 
					end + case when cl2.note is not null then ':' + cl2.Note else '' end,
	c.itemorder,
	v.userid
FROM   dbo.checkListValues as v					
INNER JOIN dbo.checkLists as c
	on c.CheckListID = v.checkListID
INNER JOIN dbo.checkListItems as cli 
	on cli.checkListID = c.checkListItemID
inner join dbo.Permit_Inspections i
	on i.RECORDID = v.subRecordID
	and i.PERMIT_NO = v.recordID
LEFT JOIN  dbo.CheckListNotes cl2  
	on v.checkListValueID = cl2.checkListValueID
where i.inspectionType in ('routine', 'follow up', 'initial site dev')
and (cl2.lastupdate is not null or v.checkDate is not null)
ORDER BY 
	v.recordID, 
	c.itemorder

--get the rolled up 1 note per permit+inspection combo
if object_id('tempdb.dbo.#final') is not null drop table #final
create table #final
(
	recordid varchar(50),
	subrecordid varchar(50),	
	UpdateDate datetime, 
	RollupNote nvarchar(max) ,
	rid int ,
	pkid int not null identity(1,1),
	UserID varchar(25)
)

 

insert into #final

(
	recordid,
	subrecordid,
	UpdateDate,
	RollupNote,
	UserID
)
select 
	a.recordid ,
	a.subrecordid ,
	max(a.UpdateDate) as UpdateDate,
	max(ltrim(rtrim(b.NoteString))) as NoteString,
	max(userID)
from #raw a
inner join 
(
	select distinct
			recordid,
			subrecordid,
			STUFF((select 
							 char(10)+char(13) + r2.RollupNote
						 FROM    #raw r2
						 where r2.recordid = r1.recordid
						 and r2.subrecordid = r1.subrecordid
						 ORDER BY 
							recordid, 
							itemorder
						 FOR XML PATH(''), TYPE).value('.', 'nvarchar(max)'), 1, 2, '') as NoteString 
	from #raw r1
) as b
on b.recordid = a.recordid
and b.subrecordid = a.subrecordid
group by
	a.recordid ,
	a.subrecordid


--remove notes where we have already inserted
delete a
from #final a
where
	exists(select 1
			from dbo.ArvadaCheckListRun b
			where 
				b.recordid = a.recordid
				and b.subrecordid = a.subrecordid
				and b.LastRunDate > a.UpdateDate
				)


--set rid to loop thru
update f
set rid = b.rid
from #final f
inner join 
	(select		
			pkid,
			row_number() over (order by pkid) as rid

	from #final
	) as b
	on b.pkid = f.pkid


--loop through, remove prior note, add current note
declare @s int,
		@e int,
		@RecId varchar(50),
		@SubGr varchar(50),
		@Note varchar(7500),
		@UserID varchar(6)


select 
	@s = min(rid),
	@e  = max(rid)
from #Final


	while @s <= @e
	begin

		select
			@RecId = recordid,
			@SubGr = subrecordid,
			@Note = convert(Varchar(7500), left(RollupNote,7500)),
			@UserID = UserID
		from #final
		where rid = @s

		--clear out any notes that already exist
		delete a
		from #final n
		inner join Prmry_Notes a
			on n.recordid = a.ActivityNo
			and n.subrecordid = a.SubGroupRecordID
			and a.Notes like 'Checklist%' 
		where 
			n.RecordID = @RecId and
			n.subrecordid = @SubGr  


		exec tsp_GblAENotes @NoteID='0',@UserID=@UserID,@ActivityGroup='PERMIT',@ActivityNo=@RecId,@SubGroup='INSPECTION',@SubGroupRecordID=@SubGr,@Notes=@Note

	 	insert into dbo.ArvadaCheckListRun
		(
			recordid,
			subrecordid,
			LastRunDate
		)
		select
			@RecId,@SubGr, getdate()


		set @s = @s + 1

	end 	

	 



end
GO
