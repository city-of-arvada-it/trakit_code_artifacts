USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_BB_AddBAXMarkup]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_BB_AddBAXMarkup]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_BB_AddBAXMarkup]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  
-- =============================================
-- Author:		<Micah Neveu>
-- Create date: <4/6/2015>
-- Description:	<Inserts a new row into bfx markups>
-- =============================================
CREATE PROCEDURE [dbo].[tsp_T9_BB_AddBAXMarkup] 
	-- Add the parameters for the stored procedure here
	@GroupName varchar(20),
	@ActivityRecordID varchar(30),
	@BAXFileName varchar(200),
	@userName varchar(30),
	@userID varchar(4),
	@AttachmentRecordID varchar(30)
	
	AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
		
	IF (select COUNT(*) from Prmry_BAX_Markups WHERE ActivityRecordID = @ActivityRecordID AND UserID = @userID AND AttachmentRecordID = @AttachmentRecordID) = 0
		BEGIN
				INSERT INTO Prmry_BAX_Markups (GroupName, ActivityRecordID, BaxFileName, UserName, UserID, AttachmentRecordID) VALUES(@GroupName, @ActivityRecordID, @BAXFileName, @userName, @userID, @AttachmentRecordID)
		END
END





GO
