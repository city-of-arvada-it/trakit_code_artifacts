USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ProjectCreateSubPermitRule]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_ProjectCreateSubPermitRule]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ProjectCreateSubPermitRule]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  

-- =====================================================================
-- Revision History:
--	RB - 9/25/2014 - Added to Source Safe

-- =====================================================================  
CREATE Procedure [dbo].[tsp_T9_ProjectCreateSubPermitRule](     
	   @ProjectNo			varchar(20),
	   @conditionID		    varchar(30),
	   @statusLock			bit,
	   @actionType			varchar(30)

)

As
 BEGIN

---- TEST ONLY
--Declare @ProjectNo			varchar(20) = '2012-0004'



 Declare @NSQLStatement     nvarchar(500),	
	     @PreventSubPermit	bit, 
         @RecordCount       int,	
         @True bit, @False  bit,
 	     @ProjectStatus		varchar(20)

        Set @True = 1
		Set @False = 0	


		if @actionType = 'UPDATE'
			begin
				if @statusLock = 1
					begin
						Set @NSQLStatement = 'Update  project_conditions2 set SubPermit_Status_Lock = 1 where recordID = ' + Char(39) + @conditionID + Char(39) 
					end
				else
					begin
						Set @NSQLStatement = 'Update  project_conditions2 set SubPermit_Status_Lock = 0 where recordID = ' + Char(39) + @conditionID + Char(39) 
					end
				exec (@NSQLStatement)
			end
		else
		begin
			if @ProjectNo is not null or @ProjectNo <> ''
			   Begin
		   			-- Get Status

					Set @NSQLStatement = 'Select @ProjectStatus = status from project_main where project_no = ' + Char(39) + @ProjectNo + Char(39) 
										
					execute sp_executesql @NSQLStatement, @params1 =  N'@ProjectStatus varchar(20) OUTPUT', @ProjectStatus = @ProjectStatus OUTPUT	
		
					Set @NSQLStatement = ''

					if @ProjectStatus <> ''
					   Begin

				   			-- Get Selected Status Count

							if len(@conditionID) > 0 
								begin
								Set @NSQLStatement = 'Select @RecordCount = count(*) from project_main m 
													join project_conditions2 c on c.project_no = m.project_no and SubPermit_Status_Lock = 1 and c.recordid = ' + Char(39) + @conditionID + Char(39) + '
													 where (m.project_no = ' + CHar(39) + @ProjectNo + Char(39) + 
													' and m.status = ' + CHar(39) + @ProjectStatus + Char(39) + ')' + 
													' and m.status in ( select STATUS from prmry_status p where p.GroupName =' + CHar(39) + 'PROJECTS' + Char(39) +   
													' and  p.PREVENT_SUBPERMITS = 1)'	
								end
							else
								begin
									Set @NSQLStatement = 'Select @RecordCount = count(*) from project_main m 												
													 where (m.project_no = ' + CHar(39) + @ProjectNo + Char(39) + 
													' and m.status = ' + CHar(39) + @ProjectStatus + Char(39) + ')' + 
													' and m.status in ( select STATUS from prmry_status p where p.GroupName =' + CHar(39) + 'PROJECTS' + Char(39) +   
													' and  p.PREVENT_SUBPERMITS = 1)'	
								end
						
		
						
							print @NSQLStatement		

							execute sp_executesql @NSQLStatement, @params =  N'@RecordCount int OUTPUT', @RecordCount = @RecordCount OUTPUT	

							if @RecordCount > 0
								Begin
									set  @PreventSubPermit = @True
								End
							Else
								Begin
									set  @PreventSubPermit = @False
								End

							Select @PreventSubPermit
						End
					else
						Begin
							set  @PreventSubPermit = @False
							Select @PreventSubPermit
						End
				End
			else
				Begin
					set  @PreventSubPermit = @False
					Select @PreventSubPermit
				End			
			end
 end




GO
