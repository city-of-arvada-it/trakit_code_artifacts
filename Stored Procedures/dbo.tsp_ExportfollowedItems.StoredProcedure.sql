USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ExportfollowedItems]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_ExportfollowedItems]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ExportfollowedItems]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[tsp_ExportfollowedItems] @UserID varchar(50)
AS
BEGIN
	select FollowID, follow.[Group], 
	case when follow.[Group]='CRM' then 
	( select 
		top 1 ISSUE_LABEL [ActivityNo] 
		from crm_issues (nolock) 
		where issue_id=follow.ActivityNo ) 
	else 
		follow.ActivityNo 
	end [ActivityNo],
	(Select Case when count(ActivityNo) > 0 then 'True' else 'False' end  
	From [Prmry_Notes] Where subgroup Is null And activityGroup = follow.[Group] and ActivityNo = follow.ActivityNo) [HasNotes],
	case when follow.[Group]='CRM' then 
	(Select Case when count(ISSUE_ID) > 0 then 'True' else 'False' end  
	From [CRM_Attachments] Where ISSUE_ID = follow.ActivityNo)
	else
	(Select Case when count(ActivityID) > 0 then 'True' else 'False' end  
	From [Attachments] LEFT JOIN [ActivityType] ON Attachments.ActivityTypeid = ActivityType.ActivityTypeID Where ActivityType.ActivityTypeName = follow.[Group] and ActivityId = follow.ActivityNo) 
	end [HasAttachments],
    (select isnull(sum([bonds].bond_amount), 0 ) from [bonds] where [bonds].ActivityID = follow.ActivityNo and [bonds].ActivityTypeID = 1 and [bonds].paid = 0 and [bonds].PAID_AMOUNT = 0)  [BondBalanceDue],
	case when follow.[Group]='AEC' then 
	(select [STATUS] from AEC_MAIN where [ST_LIC_ISSUE] = follow.ActivityNo)
	 when follow.[Group]='PERMIT' then 
	(select [STATUS] from Permit_Main where [PERMIT_NO] = follow.ActivityNo)
	 when follow.[Group]='LICENSE2' then 
	(select [STATUS] from LICENSE2_MAIN where [LICENSE_NO] = follow.ActivityNo)
	 when follow.[Group]='CASE' then 
	(select [STATUS] from CASE_MAIN where [CASE_NO] = follow.ActivityNo)
	 when follow.[Group]='PROJECT' then 
	(select [STATUS] from PROJECT_MAIN where [PROJECT_NO] = follow.ActivityNo)
	 when follow.[Group]='GEO' then 
	(select [STATUS] from geo_ownership where [SITE_APN] = follow.ActivityNo)
	else
	(select '')
	end [Status],
	case when follow.[Group]='PERMIT' then 
	(select [DESCRIPTION] from Permit_Main where [PERMIT_NO] = follow.ActivityNo)
	 when follow.[Group]='LICENSE2' then 
	(select [COMPANY] from LICENSE2_MAIN where [LICENSE_NO] = follow.ActivityNo)
	 when follow.[Group]='CASE' then 
	(select [DESCRIPTION] from CASE_MAIN where [CASE_NO] = follow.ActivityNo)
	 when follow.[Group]='PROJECT' then 
	(select [DESCRIPTION] from PROJECT_MAIN where [PROJECT_NO] = follow.ActivityNo)
	when follow.[Group]='CRM' then 
	(select [DESCRIPTION] from CRM_Issues where [ISSUE_ID] = follow.ActivityNo)
	when follow.[Group]='AEC' then 
	(select [COMPANY] from AEC_MAIN where [ST_LIC_ISSUE] = follow.ActivityNo)
	 when follow.[Group]='GEO' then 
	(select GEOTYPE + ' _ ' + SITE_ALTERNATE_ID from geo_ownership where [SITE_APN] = follow.ActivityNo)
	else
	(select '')
	end [Description],
	case when follow.[Group]='PERMIT' then 
	(select [SITE_ADDR] from Permit_Main where [PERMIT_NO] = follow.ActivityNo)
	 when follow.[Group]='LICENSE2' then 
	(select [SITE_ADDR] from LICENSE2_MAIN where [LICENSE_NO] = follow.ActivityNo)
	 when follow.[Group]='CASE' then 
	(select [SITE_ADDR] from CASE_MAIN where [CASE_NO] = follow.ActivityNo)
	 when follow.[Group]='PROJECT' then 
	(select [SITE_ADDR] from PROJECT_MAIN where [PROJECT_NO] = follow.ActivityNo)
	when follow.[Group]='CRM' then 
	(select [ISSUE_ADDRESS] from CRM_Issues where [ISSUE_ID] = follow.ActivityNo)
	when follow.[Group]='AEC' then 
	(select [ADDRESS1] + ' _ ' + [ADDRESS2] from AEC_MAIN where [ST_LIC_ISSUE] = follow.ActivityNo)
	 when follow.[Group]='GEO' then 
	(select [SITE_ADDR] from geo_ownership where [SITE_APN] = follow.ActivityNo)
	else
	(select '')
	end [Address],
	MAX(audit.UpdateDate) [Last_Updated] 
	from follow (nolock)
	left join audit (nolock) on follow.activityno = audit.ActivityNo where follow.userid=@UserID
	group by FollowID, follow.ActivityNo, follow.[Group] 
	order by [Last_Updated] desc
END

GO
