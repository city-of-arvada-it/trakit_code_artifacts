USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[ALP_GetOperationStatus]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[ALP_GetOperationStatus]
GO
/****** Object:  StoredProcedure [dbo].[ALP_GetOperationStatus]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[ALP_GetOperationStatus]

	@BatchID int = -1 

AS
BEGIN

	select
		  [status].[ID]
		, [status].[BatchID]
		, [status].[LicenseNumber]
		, [status].[ProcessingOrder]
		, [status].[ParameterSetID]
		, [status].[OperationID]
		, [status].[OperationStatusID]
		, [status].[OperationStart]
		, [status].[OperationEnd]
		, pso.[Sequence] [OperationSequence]
		, o.[OperationName]
		, os.[Name]
		, os.[Description]

	from [ALP_BatchParameterSetLicenseNumberOperationStatus] status
		left join [ALP_OperationStatuses] os on status.OperationStatusID=os.ID
		left join [ALP_Operations] o on status.OperationID=o.Id
		left join [ALP_ParameterSetOperations] pso on status.ParameterSetID=pso.ParameterSetId and status.OperationID=pso.OperationId
	where status.BatchID=@BatchID
	order by 
		  [BatchID]
		, [LicenseNumber]
		, [ParameterSetID]
		, [OperationSequence]
END
GO
