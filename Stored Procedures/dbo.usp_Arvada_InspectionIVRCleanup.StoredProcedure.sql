USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_InspectionIVRCleanup]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[usp_Arvada_InspectionIVRCleanup]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_InspectionIVRCleanup]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
	CREATE procedure [dbo].[usp_Arvada_InspectionIVRCleanup]
	@filename varchar(500),
	@Result int

as

	insert into ArvadaIVRVMResults
	(
		FileNameValue,
		Result,
		DateTimeRan
	)
	select @filename, @Result, getdate()
GO
