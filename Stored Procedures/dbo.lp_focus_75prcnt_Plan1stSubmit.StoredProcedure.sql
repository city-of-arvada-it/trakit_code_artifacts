USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[lp_focus_75prcnt_Plan1stSubmit]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[lp_focus_75prcnt_Plan1stSubmit]
GO
/****** Object:  StoredProcedure [dbo].[lp_focus_75prcnt_Plan1stSubmit]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* 
--Ychavez 3/17/2014 - FOR FOCUS Measures report
-----------------------------------------------------------------
--75% of plans which are complete at first submittal. 
exec lp_focus_75prcnt_Plan1stSubmit @APPLIED_FROM = '01/01/2014' , @APPLIED_TO = '01/31/2014'
--------------------------------------------------------------------
*/
CREATE procedure [dbo].[lp_focus_75prcnt_Plan1stSubmit]
		@APPLIED_FROM datetime = null,
		@APPLIED_TO datetime = null
as
begin


-------------------------------------------------------------------------------
--Josie is putting in a checkbox that it says it was completed in 1st submittal
--------------------------------------------------------------------------------
--select * from  #tmp_measure where issued is null and applied between '01/01/2013' and getdate()
--DROP TABLE #tmp_measure
--drop table #TMP_COUNT
select P.PERMIT_NO, P.APPLIED, P.ISSUED,
CASE WHEN u.CPR_FIRST_SUB = 1  THEN 'WITHIN MEASURE'  ELSE 'OUT OF MEASURE'
 END AS MEASURE
 INTO #TMP_COUNT
 FROM PERMIT_MAIN P 
 join permit_udf u on p.PERMIT_NO = u.PERMIT_NO 
--WHERE APPLIED BETWEEN  @APPLIED_FROM and @APPLIED_TO --SAME AS APPLIED DATE
WHERE APPLIED BETWEEN '01/01/2014' and '02/01/2014' --SAME AS APPLIED DATE
and permittype in ( 'COMMERCIAL', 'RESIDENTIAL', 'NEW SINGLE FAMILY')

ORDER BY APPLIED
----------------------------------------------------------------
--select * from #TMP_COUNT
-----------------------------------------------------------------
select SUM(case when measure = 'WITHIN MEASURE' THEN 1 ELSE 0 END) IN_MEASURE_COUNT
 ,SUM(case when measure = 'OUT OF MEASURE' THEN 1 ELSE 0 END) OUT_OF_MEASURE_COUNT
 ,SUM(CASE WHEN PERMIT_NO LIKE '%-%' THEN 1 END) TOTAL_PERMITS
 into #tmp_calc_measure_tot
  from #TMP_COUNT
  
 select SUM(cast(IN_MEASURE_COUNT as decimal)) / Sum(cast(total_permits as decimal)) * 100 as percent_plns1stSubmittal, total_permits, IN_MEASURE_COUNT
from #tmp_calc_measure_tot
group by total_permits, IN_MEASURE_COUNT
end
GO
