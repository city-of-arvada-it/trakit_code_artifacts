USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[ALP_ParameterSetDeleteById]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[ALP_ParameterSetDeleteById]
GO
/****** Object:  StoredProcedure [dbo].[ALP_ParameterSetDeleteById]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==========================================================================================================
-- Revision History:
--	  2016-12-11 - RGB Initial
--	  2017-12-12 - EWH - #12562 throw the exception that is caught so the caller knows there is an issue
-- ==========================================================================================================
CREATE PROCEDURE [dbo].[ALP_ParameterSetDeleteById] (@InputParameterSetId int = Null)
AS
BEGIN

		Declare @iRwCnt int, 
				@ParameterSetOperationId int,				
				@Id int	 		

		-- Test Data
		--Declare @InputParameterSetId int =40
	
	
		Begin Try

		IF OBJECT_ID('tempdb..#TempParameterSets') IS NOT NULL
		  DROP TABLE #TempParameterSets 	

		if @InputParameterSetId is not null
			Begin
				If @InputParameterSetId <=0
					Begin
						Return
					End
			End
		
		-- Put Requested Data into temp Table
		
		Select Id=IDENTITY(INT,1,1) ,APSO.id as ParameterSetOperationsId into #TempParameterSets from ALP_ParameterSets APS
		left join ALP_ParameterSetOperations APSO on APS.id = APSO.ParameterSetId 
		where APS.Id = @InputParameterSetId

		set @iRwCnt = 0

		-- Get number of records returned
		set @iRwCnt = @@ROWCOUNT	

			-- Start Loop to go thru No Operation Values records
		While @iRwCnt > 0
			Begin

				-- Select one Record
				Select top 1 @Id = Id, @ParameterSetOperationId = ParameterSetOperationsId 
				from #TempParameterSets

				-- Get Record Count
				set @iRwCnt = @@ROWCOUNT 

				-- If Record Count > 0 then there are parameter sets

				if @iRwCnt > 0
					Begin
					
						if @ParameterSetOperationId is not null 
							Begin							
								if  @ParameterSetOperationId > 0
									Begin

										-- Delete Operations and Operation Values
										exec dbo.ALP_ParameterSetOperationsDeleteById @ParameterSetOperationId 
									
									End
							End																						
					
						   delete from #TempParameterSets where id = @Id 			

					End				
			End

				delete from ALP_ParameterSets where id = @InputParameterSetId 
			

		End Try
		Begin Catch
			-- If error then reset @iRwCnt to 0 and return 
			Set @iRwCnt = 0;
			throw; -- EWH Dec 12, 2017 #12562: let the caller know there is an issue
			Return
		End Catch
End
GO
