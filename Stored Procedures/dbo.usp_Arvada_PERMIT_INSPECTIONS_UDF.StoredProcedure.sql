USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_PERMIT_INSPECTIONS_UDF]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[usp_Arvada_PERMIT_INSPECTIONS_UDF]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_PERMIT_INSPECTIONS_UDF]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_Arvada_PERMIT_INSPECTIONS_UDF]
	@PermitNo varchar(30),
	@RecID varchar(30) = null 
as
/**********************************************************************************************************************
* Creator:      	 
* Date:         	 
* Title:			 
* Description:  	 
* Examples:    

EXECUTE dbo.[usp_Arvada_PERMIT_INSPECTIONS_UDF] @PermitNo = 'C12-0114' , @RecID = 'API_1304051429081458'

 select ''''' as ' + column_name + ',' 
 from INFORMATION_SCHEMA.columns
 where table_name = 'PERMIT_INSPECTIONS_UDF'
 and ordinal_position > 2
 order by column_name

 select  column_name + ',' 
 from INFORMATION_SCHEMA.columns
 where table_name = 'PERMIT_INSPECTIONS_UDF'
 and ordinal_position > 2
 and column_name like '%inact%'
 order by column_name
  					 
* Modifications:  	
  Developer Name		Date		Description
  ------------------	----------	----------------------------------------------------------- 
*
**********************************************************************************************************************/

set nocount on
 
 --------------------------
 --- Declare Variables   --
 --------------------------

 
 --------------------------
 --- Create Temp Tables  --
 --------------------------

	 
 --------------------------
 --- Body of Procedure   --
 --------------------------

 if exists(
				select 1
				FROM dbo.PERMIT_INSPECTIONS_UDF m
				WHERE
					m.PERMIT_NO = @PermitNo
				and (@RecID is null or m.insp_recordid = @RecID)
		)
begin
	SELECT 
		PERMIT_NO,
		insp_recordid as UniqueInspection,
		BLD_INSP_UNIT_CT,
		INSP_BUILD_ARCH,
		INSP_COMMENTS,
		INSP_DATE,
		INSP_INSP_FEE,
		INSP_LAND,
		INSP_LIGHTING,
		INSP_MECH,
		INSP_PARK_ACC,
		INSP_PARK_BIKE,
		INSP_PARK_VEC,
		INSP_SIGNAGE,
		INSP_TRASH,
		SD_AD_DWAT,
		SD_ADDW_COM,
		SD_CD_COM,
		SD_CHECK_DAM,
		SD_CONCR_WO,
		SD_CONWO_COM,
		SD_DIV_SWL,
		SD_DSB_COM,
		SD_EB_COM,
		SD_ERO_BNK,
		SD_GH_COM,
		SD_GOOD,
		SD_IN_PRO,
		SD_INSP_REP,
		SD_IP_COM,
		SD_MAJSITE_COM,
		SD_MAJSITE_INT,
		SD_MAS_CON,
		SD_MAS_CONT,
		SD_MASC_COM,
		SD_MC_COM,
		SD_OP_COM,
		SD_OTHER,
		SD_OTHER_COM,
		SD_OUTLET_PRO,
		SD_PC_COM,
		SD_PERMIT,
		SD_PRE_CON,
		SD_PREST_COM,
		SD_PREST_STA,
		SD_REV_COM,
		SD_REV_ONSITE,
		SD_SB_COM,
		SD_SDI_BASE,
		SD_SDM_COM,
		SD_SDMLCH,
		SD_SF_COM,
		SD_SILT_FENC,
		SD_SLP_COM,
		SD_SLP_DRN,
		SD_SOIL_STAB,
		SD_SOIL_TRACK,
		SD_SOILST_COM,
		SD_ST_SWP,
		SD_STOCBM_COM,
		SD_STOCK_BM,
		SD_STOCK_SOIL,
		SD_STOCKSO_COM,
		SD_STRACK_COM,
		SD_STS_COM,
		SD_STSW_COM,
		SD_STSW_REQ,
		SD_SURF_COMM,
		SD_SURF_ROUGH,
		SD_SWMP_REVIEWED,
		SD_SWMP_SITE,
		SD_VEH_MAIN,
		SD_VEH_PAD,
		SD_VMAIN_COM,
		SD_VTP_COM,
		SD_INACT_COM
	FROM dbo.PERMIT_INSPECTIONS_UDF m
	WHERE
		m.PERMIT_NO = @PermitNo
	and (@RecID is null or m.insp_recordid = @RecID)
 
 end
 else
 begin



 SELECT 
		@PermitNo as PERMIT_NO,
		@RecID as UniqueInspection,
		'' as BLD_INSP_UNIT_CT,
		'' as INSP_BUILD_ARCH,
		'' as INSP_COMMENTS,
		'' as INSP_DATE,
		'' as INSP_INSP_FEE,
		'' as INSP_LAND,
		'' as INSP_LIGHTING,
		'' as INSP_MECH,
		'' as INSP_PARK_ACC,
		'' as INSP_PARK_BIKE,
		'' as INSP_PARK_VEC,
		'' as INSP_SIGNAGE,
		'' as INSP_TRASH,
		'' as SD_AD_DWAT,
		'' as SD_ADDW_COM,
		'' as SD_CD_COM,
		'' as SD_CHECK_DAM,
		'' as SD_CONCR_WO,
		'' as SD_CONWO_COM,
		'' as SD_DIV_SWL,
		'' as SD_DSB_COM,
		'' as SD_EB_COM,
		'' as SD_ERO_BNK,
		'' as SD_GH_COM,
		'' as SD_GOOD,
		'' as SD_IN_PRO,
		'' as SD_INSP_REP,
		'' as SD_IP_COM,
		'' as SD_MAJSITE_COM,
		'' as SD_MAJSITE_INT,
		'' as SD_MAS_CON,
		'' as SD_MAS_CONT,
		'' as SD_MASC_COM,
		'' as SD_MC_COM,
		'' as SD_OP_COM,
		'' as SD_OTHER,
		'' as SD_OTHER_COM,
		'' as SD_OUTLET_PRO,
		'' as SD_PC_COM,
		'' as SD_PERMIT,
		'' as SD_PRE_CON,
		'' as SD_PREST_COM,
		'' as SD_PREST_STA,
		'' as SD_REV_COM,
		'' as SD_REV_ONSITE,
		'' as SD_SB_COM,
		'' as SD_SDI_BASE,
		'' as SD_SDM_COM,
		'' as SD_SDMLCH,
		'' as SD_SF_COM,
		'' as SD_SILT_FENC,
		'' as SD_SLP_COM,
		'' as SD_SLP_DRN,
		'' as SD_SOIL_STAB,
		'' as SD_SOIL_TRACK,
		'' as SD_SOILST_COM,
		'' as SD_ST_SWP,
		'' as SD_STOCBM_COM,
		'' as SD_STOCK_BM,
		'' as SD_STOCK_SOIL,
		'' as SD_STOCKSO_COM,
		'' as SD_STRACK_COM,
		'' as SD_STS_COM,
		'' as SD_STSW_COM,
		'' as SD_STSW_REQ,
		'' as SD_SURF_COMM,
		'' as SD_SURF_ROUGH,
		'' as SD_SWMP_REVIEWED,
		'' as SD_SWMP_SITE,
		'' as SD_VEH_MAIN,
		'' as SD_VEH_PAD,
		'' as SD_VMAIN_COM,
		'' as SD_VTP_COM,
		'' as SD_INACT_COM
 
 
 end

GO
