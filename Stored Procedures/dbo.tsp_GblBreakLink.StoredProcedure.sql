USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GblBreakLink]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_GblBreakLink]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GblBreakLink]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

					  
-- =====================================================================
-- Revision History:
--	RB - 3/19/2014
-- MJ  - 09/20/2016 - Remove Generics

-- =====================================================================   	
create Procedure [dbo].[tsp_GblBreakLink](     
	   @CurActivityNo			varchar(50),
	   @LinkedActivityNo		varchar(50),
	   @ActiveGroup				varchar(20), 
	   @LinkedItemGroup			varchar(20),
	   @LinkedParentGroup		varchar(20),
	   @LinkType				Varchar(20) 

	   	 
)

As
 BEGIN


-- TESTING ONLY
--Declare	    @CurActivityNo			varchar(50) = 'H1303-0008',
--			@LinkedActivityNo		varchar(50) = 'H1403-0004',
--	        @ActiveGroup			varchar(20) = 'LICENSE2', 
--		    @LinkedItemGroup		varchar(20) = 'LICENSE2',
--	        @LinkedParentGroup		varchar(20) = 'NONE',
--	        @LinkType				Varchar(20) = 'SUB' 



 Declare @SQLStatement varchar(500)
 Declare @NSQLStatement nvarchar(500)
 Declare @IssueId      int	
Declare @IssueIdStr   varchar(30)

Set @SQLStatement = null 

if (@CurActivityNo is not null and @CurActivityNo <> '') and (@LinkedActivityNo is not null and @LinkedActivityNo <> '') and (@ActiveGroup is not null and @ActiveGroup <> '') and 
   (@LinkedItemGroup is not null and @LinkedItemGroup <> '') and (@LinkedParentGroup is not null and @LinkedParentGroup <> '') and (@LinkType is not null and @LinkType <> '')
   Begin
		if  @LinkedParentGroup <> 'CRM'
			Begin		
											
					 if @LinkedItemGroup = 'PERMIT' 
						Begin
					
							If @LinkType = 'PARENT'
							   Begin
								   Set @SQLStatement=    
								   Case @LinkedParentGroup
										When 'PERMIT'   Then ' Update permit_main set Parent_Permit_No  = ' + CHAR(39) + CHAR(39) + ' Where permit_no = ' + CHAR(39) + @CurActivityNo + CHAR(39)
										When 'PROJECT'  Then ' Update permit_main set Parent_Project_No  = ' + CHAR(39) + CHAR(39) + ' Where permit_no = ' + CHAR(39) + @CurActivityNo + CHAR(39)										
										When 'LICENSE2' Then ' Update permit_main set PARENT_BUS_LIC_NO  = ' + CHAR(39) + CHAR(39) + ' Where permit_no = ' + CHAR(39) + @CurActivityNo + CHAR(39)
								   End				
							End	
							else if @LinkType = 'SUB'
							   Begin
						
								   Set @SQLStatement=    
								   Case @ActiveGroup
										When 'PERMIT'   Then ' Update permit_main set Parent_Permit_No  = ' + CHAR(39) + CHAR(39) + ' Where permit_no = ' + CHAR(39) + @LinkedActivityNo + CHAR(39)
										When 'PROJECT'  Then ' Update permit_main set Parent_Project_No  = ' + CHAR(39) + CHAR(39) + ' Where permit_no = ' + CHAR(39) + @LinkedActivityNo + CHAR(39)										
										When 'LICENSE2' Then ' Update permit_main set PARENT_BUS_LIC_NO  = ' + CHAR(39) + CHAR(39) + ' Where permit_no = ' + CHAR(39) + @LinkedActivityNo + CHAR(39)
										When 'AEC'      Then ' update permit_people set ID = null where permit_no = ' + CHAR(39) + @LinkedActivityNo + CHAR(39) + ' and ID = ' + CHAR(39) + 'C:' + @CurActivityNo + CHAR(39)
								   End
							   End
						End
					else if @LinkedItemGroup = 'PROJECT' 
						Begin
							If @LinkType = 'PARENT'
							   Begin
								   Set @SQLStatement=    
								   Case @LinkedParentGroup					
										When 'PROJECT'  Then ' Update project_main set Parent_Project_No  = ' + CHAR(39) + CHAR(39) + ' Where project_no = ' + CHAR(39) + @CurActivityNo + CHAR(39)										
				
								   End				
							End	
							else if @LinkType = 'SUB'
							   Begin
								   Set @SQLStatement=    
								   Case @ActiveGroup
										When 'PERMIT'   Then ' Update project_main set Parent_Permit_No  = ' + CHAR(39) + CHAR(39) + ' Where project_no = ' + CHAR(39) + @LinkedActivityNo + CHAR(39)
										When 'PROJECT'  Then ' Update project_main set Parent_Project_No  = ' + CHAR(39) + CHAR(39) + ' Where project_no = ' + CHAR(39) + @LinkedActivityNo + CHAR(39)										
										When 'AEC'      Then ' update project_people set ID = null where project_no = ' + CHAR(39) + @LinkedActivityNo + CHAR(39) + ' and ID = ' + CHAR(39) + 'C:' + @CurActivityNo + CHAR(39)
					
								   End
							   End
						End					
					else if @LinkedItemGroup = 'CASE' 
						Begin
							If @LinkType = 'PARENT'
							   Begin
								   Set @SQLStatement=    
								   Case @LinkedParentGroup
										When 'PERMIT'   Then ' Update case_main set Parent_Permit_No  = ' + CHAR(39) + CHAR(39) + ' Where case_no = ' + CHAR(39) + @CurActivityNo + CHAR(39)
										When 'PROJECT'  Then ' Update case_main set Parent_Project_No  = ' + CHAR(39) + CHAR(39) + ' Where case_no = ' + CHAR(39) + @CurActivityNo + CHAR(39)										
										When 'LICENSE2' Then ' Update case_main set PARENT_BUS_LIC_NO  = ' + CHAR(39) + CHAR(39) + ' Where case_no = ' + CHAR(39) + @CurActivityNo + CHAR(39)
								   End				
							End	
							else if @LinkType = 'SUB'
							   Begin
								   Set @SQLStatement=    
								   Case @ActiveGroup
										When 'PERMIT'   Then ' Update case_main set Parent_Permit_No  = ' + CHAR(39) + CHAR(39) + ' Where case_no = ' + CHAR(39) + @LinkedActivityNo + CHAR(39)
										When 'PROJECT'  Then ' Update case_main set Parent_Project_No  = ' + CHAR(39) + CHAR(39) + ' Where case_no = ' + CHAR(39) + @LinkedActivityNo + CHAR(39)										
										When 'LICENSE2' Then ' Update case_main set PARENT_BUS_LIC_NO  = ' + CHAR(39) + CHAR(39) + ' Where case_no = ' + CHAR(39) + @LinkedActivityNo + CHAR(39)
										When 'AEC'      Then ' update case_people set ID = null where case_no = ' + CHAR(39) + @LinkedActivityNo + CHAR(39) + ' and ID = ' + CHAR(39) + 'C:' + @CurActivityNo + CHAR(39)
								   End
							   End
						End
					else if @LinkedItemGroup = 'LICENSE2' 
						Begin
							
							If @LinkType = 'PARENT'
							   Begin
								   Set @SQLStatement=    
								   Case @LinkedParentGroup					
										When 'LICENSE2' Then ' update license2_main set parent_recordid = ' + char(39) + char(39) + ' where license_no = ' + CHAR(39) + @CurActivityNo + CHAR(39) +  
																						' and parent_recordid in (select recordid from license2_main where license_no =' + CHAR(39) + @LinkedActivityNo + CHAR(39) + ')'
										When 'PERMIT' Then ' delete from lnk_activities where ParentActivityNo =' + CHAR(39) + @LinkedActivityNo + CHAR(39) + ' AND ChildActivityNo = ' + CHAR(39) + @CurActivityNo + CHAR(39)
								   End				
							End	
							else if @LinkType = 'SUB'
							   Begin																	
								   Set @SQLStatement=    
								   Case @ActiveGroup					
										When 'LICENSE2' Then ' update license2_main set parent_recordid = ' + char(39) + char(39) + ' where license_no = ' + CHAR(39) + @LinkedActivityNo + CHAR(39) +  
																						' and parent_recordid in (select recordid from license2_main where license_no =' + CHAR(39) + @CurActivityNo + CHAR(39) + ')'
										When 'AEC'      Then ' update License2_people set ID = null where license_no = ' + CHAR(39) + @LinkedActivityNo + CHAR(39) + ' and ID = ' + CHAR(39) + 'C:' + @CurActivityNo + CHAR(39)
								   End
							   End
						End   
					else if @LinkedItemGroup = 'AEC' 
						Begin
				
							If @LinkType = 'PARENT'
							   Begin
								   Set @SQLStatement=    
								   Case @LinkedParentGroup
					
										When 'LICENSE2' Then ' Update license2_main set PARENT_BUS_LIC_NO  = ' + CHAR(39) + CHAR(39) + ' Where license_no = ' + CHAR(39) + @CurActivityNo + CHAR(39)
										When 'AEC'      Then ' Update AEC_main set parent_AEC_ST_LIC_NO  = ' + CHAR(39) + CHAR(39) + ' Where st_lic_no = ' + CHAR(39) + @CurActivityNo + CHAR(39) + 
										                                                                                          'and parent_AEC_ST_LIC_NO = ' + CHAR(39) + @LinkedActivityNo + CHAR(39)
										
								   End				
							End	
							else if @LinkType = 'SUB'
							   Begin
								   Set @SQLStatement=    
								   Case @ActiveGroup
					
										When 'LICENSE2' Then ' update license2_people set ID = null Where license_no = ' + CHAR(39) + @CurActivityNo + CHAR(39) + 
																																  'and ID = ' + CHAR(39) + 'C:' + @LinkedActivityNo + CHAR(39)
										When 'AEC'      Then ' Update AEC_main set parent_AEC_ST_LIC_NO  = ' + CHAR(39) + CHAR(39) + ' Where st_lic_no = ' + CHAR(39) + @LinkedActivityNo + CHAR(39) + 
										                                                                                          'and parent_AEC_ST_LIC_NO = ' + CHAR(39) + @CurActivityNo + CHAR(39)
								   End
							   End
						End
					else if @LinkedItemGroup = 'CRM' 
						Begin				
							-- Get the Issue ID. In many cases ISSUE ID is the same as ISSUE LABEL but there are some cases where this is not the case
		
							 set @NSQLStatement = 'Select @IssueId = ISSUE_ID from CRM_Issues where ISSUE_LABEL = ' + CHar(39) + @linkedActivityNo + Char(39)		

							 execute sp_executesql @NSQLStatement, @params =  N'@IssueId int OUTPUT', @IssueId = @IssueId OUTPUT	
 
							 if @IssueId >  0
								Begin

								  set @IssueIdStr = convert(varchar(30), @IssueId) 			  	
				 
									if @ActiveGroup	 = 'PERMIT' or @ActiveGroup	= 'PROJECT' or @ActiveGroup = 'CASE' or @ActiveGroup = 'LICENSE' or @ActiveGroup = 'AEC' or 
									   @ActiveGroup	 = 'GEO' 
									   Begin
						
										   Set @SQLStatement = 'Delete from CRM_Links where ISSUE_ID = ' +  @IssueIdStr  + ' and LINK_GROUP = ' + CHAR(39) + @ActiveGroup + CHAR(39) + 
																						 ' and LINK_NO = ' + CHAR(39) + @CurActivityNo + CHAR(39)																

									   End	
									Else if @ActiveGroup = 'CRM'
									   Begin
						
											Set @SQLStatement = 'Delete from CRM_Links where ((ISSUE_ID = ' + CHAR(39) + @IssueIdStr + CHAR(39) + ' and LINK_ID = ' + CHAR(39) + @CurActivityNo + CHAR(39) + 
																						 ') or (ISSUE_ID = ' + CHAR(39) + @CurActivityNo + (39) + ' and LINK_ID = ' + CHAR(39) + @IssueIdStr + CHAR(39) + 
																						 ')) and LINK_GROUP = '  + CHAR(39) + 'CRM' + Char(39)
									   End
								 End	  
						End
			End
		else 
			Begin		
		  			 
					if @LinkedItemGroup = 'PERMIT' or @LinkedItemGroup	= 'PROJECT' or @LinkedItemGroup = 'CASE' or @LinkedItemGroup = 'LICENSE' or @LinkedItemGroup = 'AEC'
						Begin

						set @NSQLStatement = 'Select @IssueId = ISSUE_ID from CRM_Issues where ISSUE_LABEL = ' + CHar(39) + @CurActivityNo + Char(39)		

						execute sp_executesql @NSQLStatement, @params =  N'@IssueId int OUTPUT', @IssueId = @IssueId OUTPUT	

						if @IssueId >  0
							Begin

								set @IssueIdStr = convert(varchar(30), @IssueId) 		
						
								Set @SQLStatement = 'Delete from CRM_Links where ISSUE_ID = ' +  @IssueIdStr  + ' and LINK_GROUP = ' + CHAR(39) + @LinkedItemGroup + CHAR(39) + 
																		' and LINK_NO = ' + CHAR(39) + @LinkedActivityNo + CHAR(39)	
							End			
																		 															

						End	
					Else if @LinkedItemGroup = 'CRM'
						Begin					
		
							Set @SQLStatement = 'Delete from CRM_Links where ((ISSUE_ID = ' +  @LinkedActivityNo + ' and LINK_ID = ' + @CurActivityNo + 
																			') or (ISSUE_ID = ' + @CurActivityNo + ' and LINK_ID = ' +  @LinkedActivityNo + 
																			')) and LINK_GROUP = '  + CHAR(39) + 'CRM' + Char(39)															
						End
			   		  

			End
End

print @SQLStatement
	
if @SQLStatement <> '' 
   Begin
		EXEC(@SQLStatement)
		--print @SQLStatement
   End

 END

GO
