USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[___TRAKiT_173_UPDATE_LOG]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[___TRAKiT_173_UPDATE_LOG]
GO
/****** Object:  StoredProcedure [dbo].[___TRAKiT_173_UPDATE_LOG]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[___TRAKiT_173_UPDATE_LOG]
(
	@position char(10) = '', -- typical values: 'BEGIN', '', 'ERROR', 'END', 'ERROR', 'WARNING'
	@action varchar(500) = NULL,
	@successful bit = NULL,
	@errormessage nvarchar(max) = NULL
)
AS
BEGIN
	SET NOCOUNT ON;
	declare @errMsg nvarchar(max) = '';
	begin try
		insert into [dbo].[___TRAKiT_173_UPDATE] (position, action, successful, errormessage) values (@position, @action, @successful, @errormessage)
	end try
	begin catch
		set @errMsg = error_message();
		if @errMsg <> '' raiserror (@errMsg, 20, 0) with log, nowait; -- we need to stop if we can't tell if there are errors before the next batch runs
	end catch
END
GO
