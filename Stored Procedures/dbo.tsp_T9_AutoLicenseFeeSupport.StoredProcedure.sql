USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AutoLicenseFeeSupport]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_AutoLicenseFeeSupport]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AutoLicenseFeeSupport]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  
-- =====================================================================
-- Revision History:
--	RB - 10/1/2015 Initial 

-- =====================================================================   	
CREATE Procedure [dbo].[tsp_T9_AutoLicenseFeeSupport](
	 @LicenseNo varchar(20), 
	 @LicensePrefix varchar(10),
	 @LicenseTypeName varchar(60),	
	 @isFeeCode bit, 
	 @isEmissions bit, 
	 @isBankEmissions bit, 
	 @isToxic bit, 
	 @isWasteWater bit, 
	 @isNozzles bit
)

As
BEGIN
	Declare @SQLStatement varchar(1500)
		
	If @isFeeCode = 1 and (@LicensePrefix is not NULL and @LicensePrefix <> '') and (@LicenseTypeName is not NULL and @LicenseTypeName <> '')
		Begin

			set @SQLStatement = 'select FeeCode from LicenseRenew_BillingRenewalCodes where PREFIX = ' + CHAR(39) + @LicensePrefix + CHAR(39) + 
								' and TYPENAME = ' + CHAR(39) + @LicenseTypeName + CHAR(39)

			exec (@SQLStatement)

		End
	Else If @isEmissions = 1 and (@LicenseNo is not NULL and @LicenseNo <> '')
		Begin

			set @SQLStatement = 'select EMISSION_TONS from LICENSE2_UDF where license_no = ' + CHAR(39) + @LicenseNo + CHAR(39)

			exec (@SQLStatement)

		End
	Else If @isBankEmissions = 1 and (@LicenseNo is not NULL and @LicenseNo <> '')
		Begin

			set @SQLStatement = 'select BANK_EMISSION_FEE from LICENSE2_UDF where license_no =  ' + CHAR(39) + @LicenseNo + CHAR(39)

			exec (@SQLStatement)

		End
	Else If @isBankEmissions = 1 and (@LicenseNo is not NULL and @LicenseNo <> '')
		Begin

			set @SQLStatement = 'select BANK_EMISSION_FEE from LICENSE2_UDF where license_no =  ' + CHAR(39) + @LicenseNo + CHAR(39)

			exec (@SQLStatement)

		End
	Else If @isToxic = 1 and (@LicenseNo is not NULL and @LicenseNo <> '')
		Begin

			set @SQLStatement = 'select TOXIC_FEES, TOXIC_GALLONS  from LICENSE2_UDF where license_no = ' + CHAR(39) + @LicenseNo + CHAR(39)

			exec (@SQLStatement)

		End
	Else If @isWasteWater = 1 and (@LicenseNo is not NULL and @LicenseNo <> '')
		Begin

			set @SQLStatement = 'select WASTE_WATER_FLOW from LICENSE2_UDF where license_no = ' + CHAR(39) + @LicenseNo + CHAR(39)

			exec (@SQLStatement)

		End
	Else If @isNozzles = 1 and (@LicenseNo is not NULL and @LicenseNo <> '')
		Begin

			set @SQLStatement = 'select  BILLABLE_NOZZLES from LICENSE2_UDF where license_no =' + CHAR(39) + @LicenseNo + CHAR(39)

			exec (@SQLStatement)

		End	
End
 


GO
