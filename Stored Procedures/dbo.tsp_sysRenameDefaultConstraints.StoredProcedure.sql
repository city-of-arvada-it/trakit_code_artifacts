USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_sysRenameDefaultConstraints]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_sysRenameDefaultConstraints]
GO
/****** Object:  StoredProcedure [dbo].[tsp_sysRenameDefaultConstraints]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  					  					  
CREATE PROCEDURE [dbo].[tsp_sysRenameDefaultConstraints]
AS
BEGIN
  SET NOCOUNT ON

	declare @OldDefaultConstraint nvarchar(max)
	declare @NewDefaultConstraint nvarchar(max)
	declare @SqlStmt nvarchar(max)

	declare @TblConstraints table
	(
		OldDefaultConstraint nvarchar(max),
		NewDefaultConstraint nvarchar(max)
	)

	insert into @TblConstraints (OldDefaultConstraint, NewDefaultConstraint)
		   select OldDefaultConstraint, NewDefaultConstraint from (select OBJECT_NAME( sdc.parent_object_id) as tblname, sdc.name as OldDefaultConstraint, sc.name as columnName, 'DF_' + OBJECT_NAME( sdc.parent_object_id) + '_' + sc.name as NewDefaultConstraint  from sys.default_constraints sdc inner join sys.columns sc on sc.object_id = sdc.parent_object_id  and sc.column_id = sdc.parent_column_id   where SUBSTRING(sdc.name,4,1) = '_') mdm


	--select * from @TblConstraints

	declare cur cursor for
	select OldDefaultConstraint,
	NewDefaultConstraint
	from @TblConstraints

	open cur
	while (1=1)
	begin

		fetch next from cur into @OldDefaultConstraint, @NewDefaultConstraint

		if @@FETCH_STATUS <> 0
			break

		set @SqlStmt = N'sp_rename ''' + @OldDefaultConstraint + ''', ''' + @NewDefaultConstraint + ''''

		select @SqlStmt

		exec sp_executesql @SqlStmt

	end

	close cur
	deallocate cur

END





GO
