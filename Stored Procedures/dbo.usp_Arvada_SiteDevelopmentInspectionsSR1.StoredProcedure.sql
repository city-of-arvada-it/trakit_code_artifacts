USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_SiteDevelopmentInspectionsSR1]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[usp_Arvada_SiteDevelopmentInspectionsSR1]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_SiteDevelopmentInspectionsSR1]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_Arvada_SiteDevelopmentInspectionsSR1]
	@PermitNo varchar(30),
	@RecID varchar(30) = null 
as
/**********************************************************************************************************************
* Creator:      	 
* Date:         	 
* Title:			 
* Description:  	 
* Examples:    

EXECUTE dbo.[usp_Arvada_SiteDevelopmentInspectionsSR1] @PermitNo = 'C12-0114' 
					 
* Modifications:  	
  Developer Name		Date		Description
  ------------------	----------	----------------------------------------------------------- 
*
**********************************************************************************************************************/

set nocount on
 
 --------------------------
 --- Declare Variables   --
 --------------------------

 
 --------------------------
 --- Create Temp Tables  --
 --------------------------

	 
 --------------------------
 --- Body of Procedure   --
 --------------------------

 if exists(
				select 1
				FROM dbo.PERMIT_INSPECTIONS_UDF m
				WHERE
					m.PERMIT_NO = @PermitNo
				and (@RecID is null or m.insp_recordid = @RecID)
		)
begin
	SELECT 
		PERMIT_NO,
		insp_recordid as UniqueInspection,
		SD_PERMIT, 
		SD_SWMP_SITE,
		--SD_SWMP_REVIEWED,
		SD_INSP_REP,
		SD_VEH_MAIN,
		SD_STOCK_BM,
		SD_STOCK_SOIL,
		SD_SOIL_STAB,
		SD_SOIL_TRACK,
		SD_STSW_REQ,
		SD_CONCR_WO,
		SD_DIV_SWL,
		SD_DSB_COM,
		SD_SLP_DRN,
		SD_CHECK_DAM,
		SD_CD_COM,
		SD_OUTLET_PRO,
		SD_OP_COM,
		SD_SDI_BASE,
		SD_SB_COM,
		SD_SILT_FENC,
		SD_SF_COM,
		SD_PRE_CON,
		SD_PC_COM,
		SD_VEH_PAD,
		SD_VTP_COM,
		SD_IN_PRO,
		SD_IP_COM,
		SD_ERO_BNK,
		SD_EB_COM,
		SD_ST_SWP,
		SD_STS_COM,
		SD_GOOD,
		SD_GH_COM,
		SD_MAS_CON,
		SD_MC_COM,
		SD_SLP_COM,
		SD_SDMLCH,
		SD_SDM_COM,
		--SD_MAS_CONT,
		--SD_MASC_COM,
		SD_OTHER,
		SD_OTHER_COM
	FROM dbo.PERMIT_INSPECTIONS_UDF m
	WHERE
		m.PERMIT_NO = @PermitNo
	and (@RecID is null or m.insp_recordid = @RecID)
 
 end
 else
 begin


 SELECT 
		@PermitNo as PERMIT_NO,
		@RecID as UniqueInspection,
		'' as SD_PERMIT,
		'' as SD_SWMP_SITE,
		'' as SD_SWMP_REVIEWED,
		'' as SD_INSP_REP,
		'' as SD_VEH_MAIN,
		'' as SD_STOCK_BM,
		'' as SD_STOCK_SOIL,
		'' as SD_SOIL_STAB,
		'' as SD_SOIL_TRACK,
		'' as SD_STSW_REQ,
		'' as SD_CONCR_WO,
		'' as SD_DIV_SWL,
		'' as SD_DSB_COM,
		'' as SD_SLP_DRN,
		'' as SD_CHECK_DAM,
		'' as SD_CD_COM,
		'' as SD_OUTLET_PRO,
		'' as SD_OP_COM,
		'' as SD_SDI_BASE,
		'' as SD_SB_COM,
		'' as SD_SILT_FENC,
		'' as SD_SF_COM,
		'' as SD_PRE_CON,
		'' as SD_PC_COM,
		'' as SD_VEH_PAD,
		'' as SD_VTP_COM,
		'' as SD_IN_PRO,
		'' as SD_IP_COM,
		'' as SD_ERO_BNK,
		'' as SD_EB_COM,
		'' as SD_ST_SWP,
		'' as SD_STS_COM,
		'' as SD_GOOD,
		'' as SD_GH_COM,
		'' as SD_MAS_CON,
		'' as SD_MC_COM,
		'' as SD_SLP_COM,
		'' as SD_SDMLCH,
		'' as SD_SDM_COM,
		'' as SD_MAS_CONT,
		'' as SD_MASC_COM,
		'' as SD_OTHER,
		'' as SD_OTHER_COM
 
 
 end

GO
