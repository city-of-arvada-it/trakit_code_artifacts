USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_encrypt_pii]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_encrypt_pii]
GO
/****** Object:  StoredProcedure [dbo].[tsp_encrypt_pii]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[tsp_encrypt_pii]
	@MainTextField1 AS BIT,
	@MainTextField2 AS BIT,
	@MainTextField3 AS BIT,
	@MainTextField4 AS BIT,
	@MainTextField5 AS BIT
AS 
BEGIN 	    
	OPEN SYMMETRIC KEY SymKey
	DECRYPTION BY CERTIFICATE EncryptCert;

	WITH existing_data AS (
		SELECT 
			l.recordid,
			IIF(@MainTextField1 = 1, ENCRYPTBYKEY(KEY_GUID('SymKey'), MAINTEXTFIELD1), p.protected_field1) AS e1,
			IIF(@MainTextField2 = 1, ENCRYPTBYKEY(KEY_GUID('SymKey'), MAINTEXTFIELD2), p.protected_field2) AS e2,
			IIF(@MainTextField3 = 1, ENCRYPTBYKEY(KEY_GUID('SymKey'), MAINTEXTFIELD3), p.protected_field3) AS e3,	
			IIF(@MainTextField4 = 1, ENCRYPTBYKEY(KEY_GUID('SymKey'), MAINTEXTFIELD4), p.protected_field4) AS e4,
			IIF(@MainTextField5 = 1, ENCRYPTBYKEY(KEY_GUID('SymKey'), MAINTEXTFIELD5), p.protected_field5) AS e5  
		FROM license2_main AS l
		JOIN protected_data AS p
		  ON l.recordid = p.license_recordid
		WHERE @MainTextField1 = 1 OR
			  @MainTextField2 = 1 OR 
			  @MainTextField3 = 1 OR 
			  @MainTextField4 = 1 OR 
			  @MainTextField5 = 1
		  ) 

	UPDATE protected_data 
		SET protected_field1 = e1, protected_field2 = e2, protected_field3 = e3, protected_field4 = e4, protected_field5 = e5
		FROM existing_data AS curr
		WHERE protected_data.license_recordid = curr.recordid;

	WITH new_data AS (
		SELECT 
			l.recordid,
			IIF(@MainTextField1 = 1, ENCRYPTBYKEY(KEY_GUID('SymKey'), MAINTEXTFIELD1),	NULL) AS e1,
			IIF(@MainTextField2 = 1, ENCRYPTBYKEY(KEY_GUID('SymKey'), MAINTEXTFIELD2),	NULL) AS e2,
			IIF(@MainTextField3 = 1, ENCRYPTBYKEY(KEY_GUID('SymKey'), MAINTEXTFIELD3),	NULL) AS e3,	
			IIF(@MainTextField4 = 1, ENCRYPTBYKEY(KEY_GUID('SymKey'), MAINTEXTFIELD4),	NULL) AS e4,
			IIF(@MainTextField5 = 1, ENCRYPTBYKEY(KEY_GUID('SymKey'), MAINTEXTFIELD5),	NULL) AS e5  
		FROM license2_main AS l
		LEFT OUTER JOIN protected_data AS p
		  ON l.recordid = p.license_recordid
		WHERE p.license_recordid IS NULL 	
		  AND ((@MainTextField1 = 1 AND l.MAINTEXTFIELD1 IS NOT NULL) OR
			   (@MainTextField2 = 1 AND l.MAINTEXTFIELD2 IS NOT NULL) OR 
			   (@MainTextField3 = 1 AND l.MAINTEXTFIELD3 IS NOT NULL) OR 
			   (@MainTextField4 = 1 AND l.MAINTEXTFIELD4 IS NOT NULL) OR 
			   (@MainTextField5 = 1 AND l.MAINTEXTFIELD5 IS NOT NULL) )
		)

	INSERT INTO protected_data 
	SELECT recordid, e1, e2, e3, e4, e5 
	FROM new_data;

	CLOSE SYMMETRIC KEY SymKey;

	UPDATE LICENSE2_MAIN SET MAINTEXTFIELD1 = NULL WHERE @MainTextField1 = 1
	UPDATE LICENSE2_MAIN SET MAINTEXTFIELD2 = NULL WHERE @MainTextField2 = 1
	UPDATE LICENSE2_MAIN SET MAINTEXTFIELD3 = NULL WHERE @MainTextField3 = 1
	UPDATE LICENSE2_MAIN SET MAINTEXTFIELD4 = NULL WHERE @MainTextField4 = 1
	UPDATE LICENSE2_MAIN SET MAINTEXTFIELD5 = NULL WHERE @MainTextField5 = 1
END 

GO
