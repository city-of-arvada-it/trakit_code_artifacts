USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_ROWPermitFees]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[usp_Arvada_ROWPermitFees]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_ROWPermitFees]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_Arvada_ROWPermitFees]
	@Permit_No varchar(50),
	@Fees int = 1

as 
begin
/*
exec usp_Arvada_ROWPermitFees @Permit_No='ROW20-01200',@Fees=1

*/
	set nocount on

	if object_id('tempdb.dbo.#FEES') is not null drop table #FEES
	create table #FEES
		(PERMIT_NO varchar(30)
		, CODE varchar(100)
		, PARENT_DESCRIPTION varchar(100)
		, ITEM varchar(100)
		, DETAIL varchar(100)
		, DESCRIPTION varchar(100)
		, ACCOUNT varchar(100)
		, AMOUNT float
		, QUANTITY float
		, PAID varchar(1)
		, COLLECTED_BY varchar(30)
		, RECORDID varchar(30)
		,PAID_DATE DATETIME
		,CHECK_NO varchar(100)
		,PAID_BY VARCHAR(100)
		,RECEIPT_NO VARCHAR(100)
		,PAY_METHOD VARCHAR(500)	
		, PARENTID varchar(30)
		, TOTAL_AMOUNT float
		) ;

  
	--Collect Parent Fees
	insert into #FEES
		(PERMIT_NO, RECEIPT_NO, CODE, PARENT_DESCRIPTION, DESCRIPTION, DETAIL, ACCOUNT, AMOUNT, QUANTITY, PAID, COLLECTED_BY, RECORDID, PAID_DATE,CHECK_NO,PAID_BY,PAY_METHOD)
	select
		PERMIT_NO, RECEIPT_NO, CODE, DESCRIPTION, DESCRIPTION, DETAIL, ACCOUNT, PAID_AMOUNT, QUANTITY, PAID, COLLECTED_BY, RECORDID,PAID_DATE,CHECK_NO,PAID_BY,PAY_METHOD
	from PERMIT_Fees
	 where PERMIT_NO = @PERMIT_NO 
	and PAID = 1
	and AMOUNT > 0

	--Collect Sub Fees
	insert into #FEES
		(PERMIT_NO, CODE, ITEM, DESCRIPTION, ACCOUNT, AMOUNT, QUANTITY, PARENTID)
	select
		PERMIT_NO, CODE, ITEM, DESCRIPTION, ACCOUNT, AMOUNT, QUANTITY, PARENTID
	from PERMIT_SubFees where PERMIT_NO = @PERMIT_NO and PARENTID in (select RECORDID from #FEES)
	and PAID = 1
	and AMOUNT > 0 ;
 

	--Account for negatives
	update f 
	set PARENT_DESCRIPTION = f2.[DESCRIPTION]
	from #fees f
	inner join  permit_fees f2
		on f2.RECORDID = f.PARENTID
	where
		f.PARENT_DESCRIPTION is null


	--Account for negatives
	update #FEES set AMOUNT = AMOUNT * -1 where ITEM = 'CREDIT';

	update #FEES set AMOUNT = AMOUNT * -1 where CODE = 'REFUND';

	update #FEES set TOTAL_AMOUNT = (select sum(AMOUNT) from #FEES);

	--ROW permits have all their fees in sub fees, so as to avoid double counts, remove the parent fees
	
	if @Fees = 1
	begin 
		delete a
		from #Fees a
		where PARENTID is null
		and exists(select 1 from #Fees b where b.parentid = a.recordid)

		select 	
			row_number() over (order by DESCRIPTION desc) AS NUMBER
			,DESCRIPTION
			,AMOUNT
			,TOTAL_AMOUNT
			,PAID_DATE
			, RECEIPT_NO
			,CHECK_NO
			,PAID_BY
			,PAY_METHOD
			,CASE WHEN QUANTITY = 0 and AMOUNT != 0 then 1 else QUANTITY end as QUANTITY
			, DETAIL --THIS IS THE LINE THAT GIVES THE CHECK AND PAYMENT INFO
			, PARENTID
		 from #FEES
		order by DESCRIPTION desc
	end
	else begin
		
		select distinct 
			PAID_DATE 
			,CHECK_NO 
			,PAID_BY
			,RECEIPT_NO 
			,PAY_METHOD
		 from #FEES
		 where PAID_DATE is not null
		 order by
			paid_date,
			RECEIPT_NO
	end
end
GO
