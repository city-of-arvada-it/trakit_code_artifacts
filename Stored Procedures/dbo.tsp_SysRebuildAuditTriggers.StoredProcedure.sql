USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_SysRebuildAuditTriggers]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_SysRebuildAuditTriggers]
GO
/****** Object:  StoredProcedure [dbo].[tsp_SysRebuildAuditTriggers]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[tsp_SysRebuildAuditTriggers](
@DropOnly INT = 0
)
As
SET NOCOUNT ON
BEGIN TRANSACTION
DECLARE @DynamicSQL VARCHAR(MAX); 
WITH cte AS (
select 'Drop trigger ' + NAME + '; ' AS script
from sys.objects 
where name like '%_ChangeTracking%' 
AND name not like 'tgr_T9_Pref_%' 
AND type = 'TR'
)
select @DynamicSQL = COALESCE(@DynamicSQL, '') + COALESCE(script,'')
from cte
EXEC(@DynamicSQL)
If @DropOnly = 1 
	Begin
		Commit
		RETURN;
	End 
--Build and create triggers for all defined tables
SET @DynamicSQL = ''	
DECLARE @triggerScript varchar(MAX)
SELECT @triggerScript = '
	CREATE TRIGGER !tbl_ChangeTracking ON !tbl AFTER !triggerAction 
	AS 
	DECLARE @Type varchar(1) , 
			@RecID varchar(50), 
			@group varchar(50) = '''''''', 
			@UserComputerID varchar(256) = HOST_NAME(), 
			@ActivityNoValue varchar(128), 
			@groupIndex TINYINT 
 
	SET @groupIndex = CHARINDEX(''''_'''', ''''!tbl'''')
	IF  @groupIndex > 0 
	BEGIN 
		SELECT @group = SUBSTRING(''''!tbl'''', 1, (@groupIndex -1)) 
	END 
 
	IF EXISTS (SELECT * FROM inserted) 
		BEGIN 
			IF EXISTS (SELECT * FROM deleted) 
				SET @Type = ''''U'''' 
			ELSE 
				SET @Type = ''''I''''
		END 
	ELSE 
		BEGIN 
			SET @Type = ''''D''''
		END 
		
	BEGIN 
		  IF EXISTS (SELECT * FROM deleted) 
				SELECT @ActivityNoValue = !ActField FROM deleted 
		  ELSE 
				SELECT @ActivityNoValue = !ActField FROM inserted 
	END  
 
	BEGIN 
		  IF EXISTS (SELECT * FROM deleted) 
				SELECT @RecID = recordid FROM deleted 
		  ELSE 
				SELECT @RecID = recordid FROM inserted 
	END 
	
	!NOLOCK 
	
	if @Type in (''''I'''',''''D'''',''''U'''') 
	 begin 
		insert into dbo.Audit([Group], TableName, ActivityNo, [Type], UpdateDate, UserComputerID, PrimaryKeyField, PrimaryKeyValue) 
		values (
			@group, 
			''''!tbl'''', 
			@ActivityNoValue, 
			@Type, 
			GETDATE(), 
			@UserComputerID, 
			CASE WHEN @RecID IS NULL THEN NULL  ELSE ''''RecordID'''' END, 
			@RecID )
	 END
'; 
WITH main_tables AS (
	SELECT distinct st.name as TblName
	FROM sys.tables st, sys.indexes si, sys.index_columns sic, sys.columns sc, sys.types styp
	WHERE si.object_id = st.object_id
	AND sic.object_id = st.object_id 
	AND sic.index_id = si.index_id 
	AND sc.object_id = st.object_id 
	AND sc.column_id = sic.column_id 
	AND styp.system_type_id = sc.system_type_id
	and st.name not like 'PRMRY%'
	and st.name not like 'AEC[_]TRUSTACCTS%'
	and st.name not like 'Etrakit%'
	and st.name not like '%Proffer%'
	and st.name not like '%Bond[_]Inspection%'
	and st.name not like 'Invoice[_]%'
	and st.name not like 'lnk[_]%'
	and st.name not like 'checklistActions'
	and st.name not like 'BUSINESS[_]ATTACHMENTS'
	and st.name not like '%MEMO%'
	and st.name not like '%LICENSE[_]%'
	and st.name <> 'Workflow_Actions'
	and (st.name like '%Actions' 
	or st.name like '%[_]Ownership'
	or st.name like '%Attachments'
	or st.name like '%[_]MAIN'
	or st.name like '%[_]Restrictions'
	or st.name like '%[_]Restrictions2'
	or st.name = 'Fees'
	or st.name = 'people'
	or st.name = 'subFees'
	or st.name = 'Inspections'
	or st.name = 'Conditions'
	or st.name = 'Violations'
	or st.name = 'Reviews'
	or st.name = 'CRM_Issues'
	)
)
	
SELECT @DynamicSQL = COALESCE(@DynamicSQL, '') + 
					  COALESCE(
						REPLACE(
							REPLACE(
								REPLACE(
									REPLACE('exec (''' + @triggerScript, '!tbl', TblName), 
								'!triggerAction', IIF(TblName = 'SubFees', 'insert, delete ', 'insert, update, delete ')),
							'!NOLOCK', IIF(tblName = 'Fees', 'if UPDATE(LOCKID) RETURN', '')),
						'!ActField', dbo.tfn_ActivityNoField(TblName))
						,'') + ''' ) '
FROM main_tables
EXEC(@DynamicSQL)
COMMIT
GO
