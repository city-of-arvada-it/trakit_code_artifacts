USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_LicenseRenew_Preprocessor]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_LicenseRenew_Preprocessor]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_LicenseRenew_Preprocessor]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  
					  
-- =============================================
-- Author:		Micah Neveu
-- create date: 
-- Description:	<Description,,>
-- Added Logging
-- Added additional tables for exclusions and exemptions
-- =============================================
CREATE PROCEDURE [dbo].[tsp_T9_LicenseRenew_Preprocessor](
	@ParameterSetID INT,
	@USERID NVARCHAR(20) = 'CRW'
)

AS
BEGIN
	SET NOCOUNT ON;
-- Begin some Logging!
INSERT INTO LicenseRenew_EventLog (SourceMode, EventText, USERID) VALUES ('PREPROC','Preprocessor invoked with parameter set ' + CONVERT(NVARCHAR(40), @ParameterSetID) + '.', @USERID)

TRUNCATE TABLE LicenseRenew_License_List

-- 2b) Clear existing excluded licenses for renew
TRUNCATE TABLE LicenseRenew_ExcludedLicense_List 

-- 3) Gather license #'s to be reviewed/committed: this can be a specific procedure unique to each client
EXEC tsp_T9_LicenseRenew_ParameterProcessor @ParameterSetID,'PREPROC',@USERID

-- log the initial count
INSERT INTO LicenseRenew_EventLog (SourceMode, EventText, USERID) VALUES('PREPROC', (SELECT CONVERT(NVARCHAR,COUNT(*)) + ' licenses listed for renewal process.' FROM LicenseRenew_License_List), @USERID)

-- 4) Identify those License in LicenseRenew_License_List that have any fees and sub-fees that are
--    scripted/have deposit minimums/are in text file and insert those into LicenseRenew_ExcludedLicense_List
-- FEES/SUBFEES with scripting
INSERT INTO LicenseRenew_ExcludedLicense_List
	SELECT DISTINCT License2_fees.License_No, 'Fee or sub fee has custom scripting and cannot be processed automatically.' 
	FROM LICENSE2_FEES JOIN LicenseRenew_License_List 
		ON LICENSE2_FEES.LICENSE_NO = LicenseRenew_License_List.LICENSE_NO
		INNER JOIN LICENSE2_SUBFEES
		ON LICENSE2_SUBFEES.PARENTID = LICENSE2_FEES.RECORDID
	WHERE LICENSE2_FEES.FORMULA LIKE '%CUSTOM:%' OR   --scripted
		LICENSE2_FEES.FORMULA LIKE '%MINIMUM:%'  OR   --deposit minimum
		LICENSE2_FEES.FORMULA LIKE '%@%' OR				--contained in text file
		LICENSE2_SUBFEES.FORMULA LIKE '%CUSTOM:%' OR
		LICENSE2_SUBFEES.FORMULA LIKE '%MINIMUM:%'  OR
		LICENSE2_SUBFEES.FORMULA LIKE '%@%'
	
DELETE FROM LicenseRenew_License_List 
WHERE LicenseRenew_License_List.LICENSE_NO IN (SELECT DISTINCT LICENSE_NO FROM LicenseRenew_ExcludedLicense_List)

DECLARE @ExclusionCount INT = (SELECT COUNT(*) FROM LicenseRenew_ExcludedLicense_List)

IF (@ExclusionCount > 0)
	BEGIN
		-- event log notification that items were removed
		INSERT INTO LicenseRenew_EventLog (SourceMode, EventText, USERID) VALUES('PREPROC', CONVERT(NVARCHAR,@ExclusionCount) + ' licenses removed due to custom scripting.',@USERID)	
	END

-- build the list of licenses, fees and subfees that we will be processing
	
--*********************************************************************************************************
--mdm; 01/12/15
-- was throwing:
--   Msg 257, Level 16, State 3, Procedure tsp_T9_LicenseRenew_Preprocessor, Line 113
--   Implicit conversion from data type datetime to float is not allowed. Use the CONVERT function to run this query.
-- so modified to drop table (if exists, then create new).  Was:
--TRUNCATE TABLE LicenseRenew_Review_Main
--INSERT INTO LicenseRenew_Review_Main SELECT * FROM LICENSE2_MAIN AS LM WHERE LM.LICENSE_NO IN (SELECT * FROM LicenseRenew_License_List	
--*********************************************************************************************************
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LicenseRenew_Review_Main]') AND type in (N'U'))
DROP TABLE LicenseRenew_Review_Main

select * into LicenseRenew_Review_Main from LICENSE2_MAIN AS LM WHERE LM.LICENSE_NO IN (SELECT license_no FROM LicenseRenew_License_List)


TRUNCATE TABLE LicenseRenew_Review_Fees

--*********************************************************************************************************
--mdm; 09/17/14
-- running:
--   INSERT INTO LicenseRenew_Review_Fees SELECT *, '' AS Formula_working_col, NULL AS Calc_Value FROM LICENSE2_FEES
-- was throwing:
--   Msg 257, Level 16, State 3, Procedure tsp_T9_LicenseRenew_Preprocessor, Line 113
--   Implicit conversion from data type datetime to float is not allowed. Use the CONVERT function to run this query.
-- so modified to use explict column names:
--*********************************************************************************************************
INSERT INTO LicenseRenew_Review_Fees
                         (ACCOUNT, AMOUNT, ASSESSED_BY, ASSESSED_DATE, CHECK_NO, CODE, COLLECTED_BY, COMMENTS, DEPOSITID, DESCRIPTION, DETAIL, DUE_DATE, 
                         FEETYPE, FORMULA, INVOICE_DATE, INVOICE_NO, INVOICED, LICENSE_NO, LOCKID, PAID, PAID_AMOUNT, PAID_BY, PAID_DATE, PAY_BY_DEPOSIT, 
                         PAY_METHOD, PROFFERID, QUANTITY, RECEIPT_NO, RECORDID,Formula_working_col, Calc_value)
SELECT        ACCOUNT, AMOUNT, ASSESSED_BY, ASSESSED_DATE, CHECK_NO, CODE, COLLECTED_BY, COMMENTS, DEPOSITID, DESCRIPTION, DETAIL, DUE_DATE, 
                         FEETYPE, FORMULA, INVOICE_DATE, INVOICE_NO, INVOICED, LICENSE_NO, LOCKID, PAID, PAID_AMOUNT, PAID_BY, PAID_DATE, PAY_BY_DEPOSIT, 
                         PAY_METHOD, PROFFERID, QUANTITY, RECEIPT_NO, RECORDID, '',null
FROM            LICENSE2_FEES AS LF 
	WHERE LF.LICENSE_NO IN (SELECT DISTINCT LICENSE_NO FROM LicenseRenew_Review_Main AS zLM)
	--AND LF.CODE NOT IN (SELECT FeeCode FROM LicenseRenew_ExemptedFeeCodes)
-- Instead of omitting them as above, I'm going to manually remove them so we can have an event log trail of WHY certain fees are not processed (that happens in EXCLUDED FEES/SUBFEES below

TRUNCATE TABLE LicenseRenew_Review_SubFees
INSERT INTO LicenseRenew_Review_SubFees SELECT *, '' AS Formula_working_col, NULL AS Calc_Value FROM LICENSE2_SUBFEES AS LSF 
	WHERE LSF.LICENSE_NO IN (SELECT DISTINCT LICENSE_NO FROM LicenseRenew_Review_Main AS zLM)


-- remove EXEMPT FEES/SUBFEES (excluded on purpose, like late fees and other non-renewing fees)
SET @ExclusionCount = (SELECT COUNT(*) FROM LicenseRenew_Review_Fees WHERE CODE IN (SELECT FEECODE FROM LicenseRenew_ExemptedFeeCodes))
IF (@ExclusionCount > 0)
	BEGIN
		INSERT INTO LicenseRenew_ExcludedFees_List SELECT LF.License_no, 1 AS FeeType, LF.RecordID, LF.CODE + ' is exempt.' AS ExclusionReason FROM LicenseRenew_Review_Fees AS LF
			WHERE LF.CODE IN (SELECT FEECODE FROM LicenseRenew_ExemptedFeeCodes)
		DELETE FROM LicenseRenew_Review_Fees WHERE CODE IN (SELECT FEECODE FROM LicenseRenew_ExemptedFeeCodes)
		INSERT INTO LicenseRenew_EventLog (SourceMode, EventText, USERID) VALUES('PREPROC', CONVERT(NVARCHAR,@ExclusionCount) + ' fees ignored that appear in exemption list.','CRW')	
	END

-- subfees
SET @ExclusionCount = (SELECT COUNT(*) FROM LicenseRenew_Review_SubFees WHERE ITEM IN (SELECT FEECODE FROM LicenseRenew_ExemptedFeeCodes))
IF (@ExclusionCount > 0)
	BEGIN
		INSERT INTO LicenseRenew_ExcludedFees_List SELECT LSF.License_no, 2 AS FeeType, LSF.RecordID, LSF.ITEM + ' is exempt.' AS ExclusionReason FROM LicenseRenew_Review_SubFees AS LSF
			WHERE LSF.ITEM IN (SELECT FEECODE FROM LicenseRenew_ExemptedFeeCodes)
		DELETE FROM LicenseRenew_Review_SubFees WHERE ITEM IN (SELECT FEECODE FROM LicenseRenew_ExemptedFeeCodes)
		INSERT INTO LicenseRenew_EventLog (SourceMode, EventText, USERID) VALUES('PREPROC', CONVERT(NVARCHAR,@ExclusionCount) + ' subfees ignored that appear in exemption list.','CRW')	
	END

-- LICENSE RENEWAL PREPROCESSOR (END)================================================================
END








GO
