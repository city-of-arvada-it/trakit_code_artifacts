USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[csp_T9_AlternateReviewerEscalation]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[csp_T9_AlternateReviewerEscalation]
GO
/****** Object:  StoredProcedure [dbo].[csp_T9_AlternateReviewerEscalation]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Micah Neveu>
-- Create date: <2/10/2015>
-- Description:	<Alternate reviewer/email nightly, currently using brute force to process and to make it easily understood what is going on>
-- =============================================
CREATE PROCEDURE [dbo].[csp_T9_AlternateReviewerEscalation] 

AS
BEGIN

SET NOCOUNT ON;
--drop table #targetreviews
SELECT PERMIT_NO AS PARENTRECORDID, REVIEWTYPE,CONTACT, DATE_SENT, ASSIGNED_DATE,DATE_DUE,RECORDID INTO #TargetReviews FROM permit_reviews WHERE REVIEWTYPE IN (SELECT Field01 FROM Prmry_ReviewControl WHERE Category = 'ALTREVIEWER')
	AND REVIEW_ACK = 0
	AND ASSIGNED_DATE IS NOT NULL
	AND DATE_RECEIVED IS NULL
	AND DATE_DUE IS NOT NULL

ALTER TABLE #TargetReviews ADD DAYSPASSED INT
ALTER TABLE #TargetReviews ADD ALTREVIEWER VARCHAR(24)
ALTER TABLE #TargetReviews ADD GROUPNAME VARCHAR(20)
ALTER TABLE #TargetReviews ADD EMAIL VARCHAR(60)
ALTER TABLE #TargetReviews ADD SENDEMAIL INT 

-- PERMIT SPECIFIC
UPDATE #TargetReviews SET GROUPNAME = 'PERMITS'

-- PROJECT SPECIFIC
-- Add project reviews
INSERT INTO #TargetReviews SELECT PROJECT_NO AS PARENTRECORDID, REVIEWTYPE, CONTACT, DATE_SENT, ASSIGNED_DATE, DATE_DUE, RECORDID,NULL,NULL,'PROJECTS',NULL,NULL FROM Project_Reviews WHERE REVIEWTYPE IN (SELECT Field01 FROM Prmry_ReviewControl WHERE Category = 'ALTREVIEWER')
	AND REVIEW_ACK = 0
	AND ASSIGNED_DATE IS NOT NULL
	AND DATE_RECEIVED IS NULL
	AND DATE_DUE IS NOT NULL

	-- select * from #targetreviews

UPDATE #TargetReviews SET DAYSPASSED = DATEDIFF(day, ASSIGNED_DATE, GETDATE())
DELETE FROM #TargetReviews WHERE DAYSPASSED <= 2
UPDATE #TargetReviews SET ALTREVIEWER = (SELECT TOP 1 Field02 FROM Prmry_ReviewControl WHERE Category = 'ALTREVIEWER' AND Field01 = REVIEWTYPE)
UPDATE #TargetReviews SET EMAIL = (SELECT TOP 1 Field03 FROM Prmry_ReviewControl WHERE Category = 'ALTREVIEWER' AND Field01 = REVIEWTYPE)
UPDATE #TargetReviews SET SENDEMAIL = 1

-- CURSOR OPTION
DECLARE @RECORDID VARCHAR(30), @CONTACT VARCHAR(24), @ALTREVIEWER VARCHAR(24), @ALTAVAILABLE INT, @REVIEWTYPE VARCHAR(60), @PERMITNO VARCHAR(60), @DATESENT DATETIME, @DATEDUE DATETIME, @PARENTRECORDID VARCHAR(60), @GROUPNAME VARCHAR(20)
SET @ALTAVAILABLE = 1

DECLARE @body varchar(2000)
DECLARE @ReviewAlertTemplate varchar(2000)
DECLARE @ReviewAlertTitle varchar(200)
DECLARE @recipients varchar(60)
DECLARE @Address varchar(2000)

SET @ReviewAlertTemplate = (SELECT TOP 1 Body FROM prmry_reviewcontrolalertemail)
SET @ReviewAlertTitle = (SELECT TOP 1 [Subject] FROM prmry_reviewcontrolalertemail)

------------------------------------------------------------test
--SET @ReviewAlertTemplate = 'The {ReviewType} review for {RecordNo}, assigned to {Reviewer} has not been acknowledged in a timely manner.'
------------------------------------------------------------test



insert into zAlternateReviewerEmailTable values ('ready',getdate(),'set', 'go')
DECLARE AlternateAssignCursor CURSOR
	LOCAL SCROLL STATIC

	FOR
		SELECT RECORDID, DATE_SENT, DATE_DUE, ALTREVIEWER, REVIEWTYPE, PARENTRECORDID, GROUPNAME FROM #TargetReviews --WHERE CONTACT <> ALTREVIEWER
		OPEN AlternateAssignCursor
		FETCH NEXT FROM AlternateAssignCursor INTO @RECORDID, @DATESENT, @DATEDUE, @ALTREVIEWER, @REVIEWTYPE, @PARENTRECORDID, @GROUPNAME
		WHILE @@FETCH_STATUS = 0
		BEGIN

------------------------------------------------------------------test
--insert into zAlternateReviewerEmailTable values('test',getdate(),'test',@ALTREVIEWER)
------------------------------------------------------------------test
			
			EXEC csp_T9_ReviewerAvailable @DATESENT, @DATEDUE, @ALTREVIEWER, @AVAILABLE = @ALTAVAILABLE OUT

------------------------------------------------------------------test
			--declare @altreviewer varchar(40)
			--set @altreviewer = 'MDNX'
			--declare @ALTAVAILABLE int
			--declare @thisdate datetime
			--set @thisdate = getdate()
			--declare @dayplus datetime
			--set @dayplus = dateadd(day,1,@thisdate)
			--EXEC csp_T9_ReviewerAvailable @thisdate, @dayplus, @ALTREVIEWER, @AVAILABLE = @ALTAVAILABLE OUT

			--select @ALTAVAILABLE as altAvailable, @thisdate as thisdate, @dayplus as dayplus
------------------------------------------------------------------test

			IF @ALTAVAILABLE = 1 -- they ARE available
			BEGIN
				IF @GROUPNAME = 'PERMITS'
					BEGIN
						-- PERMIT

------------------------------------------------------------------test
--insert into zAlternateReviewerEmailTable values('ALT',getdate(),'ASSIGNED','IN HERE')
------------------------------------------------------------------test

						UPDATE Permit_Reviews SET Contact = @ALTREVIEWER, ASSIGNED_DATE = GETDATE()
							WHERE RECORDID = @RECORDID
					END
				ELSE IF @GROUPNAME = 'PROJECTS'
					BEGIN
					-- PROJECT
						UPDATE Project_Reviews SET Contact = @ALTREVIEWER, ASSIGNED_DATE = GETDATE()
							WHERE RECORDID = @RECORDID
					END
				-- no need to send a notification
				UPDATE #TargetReviews SET SENDEMAIL = 0 WHERE RECORDID = @RECORDID
			END
			ELSE IF @ALTAVAILABLE = 0
				BEGIN

------------------------------------------------------------------test
--insert into zAlternateReviewerEmailTable values('ALT',getdate(),'NOT','AVAILABLE')
------------------------------------------------------------------test

					-- initial setup of the template
					SET @body = REPLACE(@ReviewAlertTemplate,'{ReviewType}', @REVIEWTYPE)
					
					-- values specific to permit or project
					IF @GROUPNAME = 'PERMITS'
						BEGIN
							SET @Address = (select isnull(SITE_ADDR,'') + ' ' + isnull(SITE_CITY,'') + ', ' + isnull(SITE_STATE,'') from permit_main where Permit_No = @PARENTRECORDID)
							SET @body = REPLACE(@body,'{RecordType}', COALESCE((SELECT PermitType FROM Permit_Main WHERE Permit_No = @PARENTRECORDID),' '))
							SET @body = REPLACE(@body,'{RecordSubType}', COALESCE((SELECT PermitSubType FROM Permit_Main WHERE Permit_No = @PARENTRECORDID), ' '))
							SET @body = REPLACE(@body,'{RecordDescription}', COALESCE((SELECT [Description] FROM Permit_Main WHERE Permit_No = @PARENTRECORDID), ' ')) 
							-- mark as acknowledged
							UPDATE Permit_Reviews SET REVIEW_ACK = 1 WHERE RECORDID = @RECORDID
						END
					ELSE IF @GROUPNAME = 'PROJECTS'
						BEGIN
							SET @Address = (select isnull(SITE_ADDR,'') + ' ' + isnull(SITE_CITY,'') + ', ' + isnull(SITE_STATE,'') from project_main where Project_No = @PARENTRECORDID)
							SET @body = REPLACE(@body,'{RecordType}', COALESCE((SELECT ProjectType FROM Project_Main WHERE Project_No = @PARENTRECORDID),' '))
							SET @body = REPLACE(@body,'{RecordSubType}', COALESCE((SELECT ProjectSubType FROM Project_Main WHERE Project_No = @PARENTRECORDID), ' '))
							SET @body = REPLACE(@body,'{RecordDescription}', COALESCE((SELECT [Description] FROM Project_Main WHERE Project_No = @PARENTRECORDID), ' ')) 
							-- mark as acknowledged
							UPDATE Project_Reviews SET REVIEW_ACK = 1 WHERE RECORDID = @RECORDID
						END
						
					SET @recipients = (SELECT EMAIL FROM #TargetReviews WHERE RECORDID = @RECORDID)

					-- common to all and already available				
					SET @body= REPLACE(@body,'{RecordNo}', COALESCE((SELECT PARENTRECORDID FROM #TargetReviews WHERE RECORDID = @RECORDID), ' '))
					SET @body = REPLACE(@body,'{Address}', @Address)
					SET @body = REPLACE(@body,'{ReviewDueDate}', COALESCE((SELECT DATE_DUE FROM #TargetReviews WHERE RECORDID = @RECORDID), ' '))
					SET @body = REPLACE(@body,'{ReviewSentDate}', COALESCE((SELECT DATE_SENT FROM #TargetReviews WHERE RECORDID = @RECORDID), ' '))
					SET @body = REPLACE(@body,'{Reviewer}', COALESCE((SELECT CONTACT FROM #TargetReviews WHERE RECORDID = @RECORDID), ' '))

------------------------------------------------------------------test
--SET @body = (SELECT TOP 1 Body FROM prmry_reviewcontrolalertemail)
--insert into zAlternateReviewerEmailTable values(@recipients,getdate(),@body,@ReviewAlertTitle )
------------------------------------------------------------------test

					-- and send an email using SQL
					-- GET THE CORRECT PROFILE NAME, this will need to be changed per client most likely
					EXEC msdb.dbo.sp_send_dbmail @profile_name = 'SMTP', @recipients = @recipients, @body = 'bad', @subject = @ReviewAlertTitle, @exclude_query_output = 1
					/*
						some helper functions for mail issues
						SELECT is_broker_enabled FROM sys.databases WHERE name = 'msdb'
						EXEC msdb.dbo.sysmail_help_status_sp
						EXEC msdb.dbo.sysmail_help_queue_sp @queue_type = 'Mail'
						EXEC msdb.sys.sp_helprolemember 'DatabaseMailUserRole'
					*/
				END

		FETCH NEXT FROM AlternateAssignCursor INTO @RECORDID, @DATESENT, @DATEDUE, @ALTREVIEWER, @REVIEWTYPE, @PARENTRECORDID, @GROUPNAME
	END
	
CLOSE AlternateAssignCursor
DEALLOCATE AlternateAssignCursor

DROP TABLE #TargetReviews

END

GO
