USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[lp_focus_95_LicenseIssue]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[lp_focus_95_LicenseIssue]
GO
/****** Object:  StoredProcedure [dbo].[lp_focus_95_LicenseIssue]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* 
--Ychavez 3/11/2014 - FOR FOCUS Measures report
-------------------------------------------------------------------------------------------
--95% of contractor licenses APP_CMPLT within 15 minutes of less of completed applications.
--
exec lp_focus_95_LicenseIssue @APPLIED_FROM = '03/01/2014' , @APPLIED_TO = '03/21/2014'
-------------------------------------------------------------------------------------------
*/
CREATE procedure [dbo].[lp_focus_95_LicenseIssue]
		@APPLIED_FROM datetime = null,
		@APPLIED_TO datetime = null
as
begin

--still  need to add in UDF APP_CMPLT
------------------------------------------------------------------------
--95% of contractor licenses APP_CMPLT within 15 minutes of less of completed applications.
--Start of the measure will be on the change in expiration date
-- and the end of the measure will be a UDF APP_CMPLT
------------------------------------------------------------------------
--GET ST_LIC_EXPIRE DATE AND APP_CMPLT DATE AND CALCULATE IF IT WAS WITHIN 15 MINUTES
select 
case when field_name = 'ST_LIC_EXPIRE' then transaction_datetime  end as ST_LIC_EXPIRE_date,
case when field_name = 'APP_CMPLT' then transaction_datetime  end as APP_CMPLT_date, 
ST_LIC_NO
INTO #tmp_over_counter_license
  from Prmry_AuditTrail a
join (select ST_LIC_NO from  Aec_Main where AECTYPE = 'BID CONTRACTOR')
 p on p.ST_LIC_NO = a.primary_key_value
      where TABLE_NAME IN ('Aec_Main','AEC_UDF')
and field_name in ('ST_LIC_EXPIRE', 'ST_LIC_ISSUE', 'APP_CMPLT')
--and 
--a.primary_key_value = '1B-0004'
and TRANSACTION_DATETIME between @APPLIED_FROM and @APPLIED_TO
--and TRANSACTION_DATETIME between '03/01/2014' and '03/21/2014'-- and @APPLIED_TO

--maybe add in the aec_udf here
--select * from dbo.AEC_UDF where APP_CMPLT is not null
--select * from Prmry_AuditTrail where primary_key_value = '9R-0055' and field_name = 'APP_CMPLT'
--select * from  Aec_Main where AECTYPE = 'BID CONTRACTOR'or STATUS like 'LIMITED%'
--and ST_LIC_NO in (select ST_LIC_NO   from dbo.AEC_UDF where APP_CMPLT is NOT null)

--select distinct ST_LIC_NO from #tmp_over_counter_license
-----------------------------------------------------------------------------------------------
--The dates are on different rows for the license. -- put in one table, one row per permit
--drop table #tmp_work  select * from #tmp_work where li = '9R-0055'
-----------------------------------------------------------------------------------------------

delete from #tmp_over_counter_license
where ST_LIC_EXPIRE_date is null and APP_CMPLT_date is null

--delete from #tmp_over_counter_license
--where ST_LIC_EXPIRE_date is null and APP_CMPLT_date is null

SELECT *
INTO #tmp_work
 FROM #tmp_over_counter_license
 where APP_CMPLT_date is null and ST_LIC_EXPIRE_date is not null-- and APP_CMPLT_date is not null
 
 insert into #tmp_work
 select *
  FROM #tmp_over_counter_license
 where ST_LIC_EXPIRE_date is null and APP_CMPLT_date is not null-- and APP_CMPLT_date is not null

 
  update #tmp_work
set APP_CMPLT_date = #tmp_over_counter_license.APP_CMPLT_date
FROM #tmp_over_counter_license 
 where #tmp_work.APP_CMPLT_date is null
 and #tmp_over_counter_license.ST_LIC_NO = #tmp_work.ST_LIC_NO
 
update #tmp_work
set ST_LIC_EXPIRE_date = #tmp_over_counter_license.ST_LIC_EXPIRE_date
FROM #tmp_over_counter_license 
 where #tmp_work.ST_LIC_EXPIRE_date is null
 and #tmp_over_counter_license.ST_LIC_NO = #tmp_work.ST_LIC_NO
 

 -----------------------------------------------------------------------------------------------
 ---which license do not have an ST_LIC_EXPIRE date- should not be any
 --select * from #tmp_noST_LIC_EXPIRE_date --drop table #tmp_noST_LIC_EXPIRE_date
 -----------------------------------------------------------------------------------------------

 select * 
 into #tmp_noST_LIC_EXPIRE_date
 from #tmp_over_counter_license 
 where ST_LIC_NO not in (select ST_LIC_NO from #tmp_work)
 -----------------------------------------------------------------------------------------------
--GET MINUTES DIFFERENCE--
------------------------------------------------------------------------------------------------- 
 --select * from #tmp_work

select DATEDIFF(MINUTE, ST_LIC_EXPIRE_date, APP_CMPLT_date) AS Minutes_Difference, ST_LIC_EXPIRE_date, APP_CMPLT_date, ST_LIC_NO
into #temp_calc
  from #tmp_work
 where ST_LIC_EXPIRE_date between @APPLIED_FROM and @APPLIED_TO --since the dates are from the transactions, some ST_LIC_EXPIRE dates may be in a different month for this permit. Filter by ST_LIC_EXPIRE date.
  --where ST_LIC_EXPIRE_date between '01/01/2014' and '03/21/2014' --since the dates are from the transactions, some ST_LIC_EXPIRE dates may be in a different month for this permit. Filter by ST_LIC_EXPIRE date.

group by ST_LIC_NO, ST_LIC_EXPIRE_date,APP_CMPLT_date

-------------------------------------------------------
--CALCULATE PERCENTAGE--SELECT * FROM #temp_calc
---------------------------------------------------
select CASE WHEN Minutes_Difference IS NULL OR Minutes_Difference > 15 THEN 'OUT OF MEASURE'
WHEN Minutes_Difference <=15 THEN 'IN MEASURE'
END AS MEASURE, ST_LIC_NO
INTO #temp_calc_measure
FROM #temp_calc


select SUM(case when measure = 'IN MEASURE' THEN 1 ELSE 0 END) IN_MEASURE_COUNT
 ,SUM(case when measure = 'OUT OF MEASURE' THEN 1 ELSE 0 END) OUT_OF_MEASURE_COUNT
 ,SUM(CASE WHEN ST_LIC_NO LIKE '%-%' THEN 1 END) TOTAL_license
 into #tmp_calc_measure_tot
  from #temp_calc_measure
  
 select SUM(cast(IN_MEASURE_COUNT as decimal)) / Sum(cast(total_license as decimal)) * 100 
 as percent_license_complete_15, total_license, IN_MEASURE_COUNT
from #tmp_calc_measure_tot
group by total_license, IN_MEASURE_COUNT



end
GO
