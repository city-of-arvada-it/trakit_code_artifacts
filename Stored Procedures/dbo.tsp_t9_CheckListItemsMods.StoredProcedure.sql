USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_t9_CheckListItemsMods]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_t9_CheckListItemsMods]
GO
/****** Object:  StoredProcedure [dbo].[tsp_t9_CheckListItemsMods]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  

CREATE Procedure [dbo].[tsp_t9_CheckListItemsMods]
       @List as [CheckListItemType] readonly
as
Begin
       Insert into CheckListItems(ItemText, CheckListGroupType, Active) 
       Select ItemText, CheckListGroupType, 1 from @List where CheckListID = 0
       
       --Delete From @List Where CheckListID = 0

       --Delete

       Delete from ChecklistITems where ChecklistID in (Select CheckListID from @List where Active = 2)

       --delete from @List where Active = 2

       --Updatess
       
       Update CheckListItems 
       set CheckListItems.ItemText = li.ItemText, 
       CheckListItems.CheckListGroupType = li.CheckListGroupType
       From CheckListItems ci
       inner Join @List li on li.CheckListid = ci.CheckListID and li.checklistid <> 0 and li.active <> 2

End




GO
