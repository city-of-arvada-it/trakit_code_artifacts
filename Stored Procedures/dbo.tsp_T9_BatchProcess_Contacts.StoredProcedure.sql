USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_BatchProcess_Contacts]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_BatchProcess_Contacts]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_BatchProcess_Contacts]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  



                                     

create PROCEDURE [dbo].[tsp_T9_BatchProcess_Contacts] 
              @ListContacts as UpdateFields readonly,
			  @ListUpdateFields as UpdateFields readonly,
              @ListTableAndRecords as MainTableRecords readonly
              
AS
BEGIN

	Declare @SQL Varchar(8000)
	Declare @TableName varchar(50) 
	Declare @ColumnName varchar(50) 
	Declare @ColumnValue varchar(255)
	Declare @RowNum int
	Declare @RowId Int
	Declare @UserID as varchar(10)
	Declare @TblName as varchar(100)
	Declare @KeyField varchar(50)  
	Declare @GroupName as varchar(255)
	Declare @Multi as varchar(10)

	--Drop Tables
	IF object_id('tempdb..#Contacts') is not null drop table #Contacts
	IF object_id('tempdb..#ForInsertIntoPrmryAuditTrail') is not null drop table #ForInsertIntoPrmryAuditTrail
	IF object_id('tempdb..#ListConditionTypes') is not null drop table #ListConditionTypes
	IF object_id('tempdb..#ListUpdateFields') is not null drop table #ListUpdateFields
	IF object_id('tempdb..#MainTableRec') is not null  drop table #MainTableRec

          

	CREATE TABLE [dbo].[#Contacts](
		   [Activity_NO] [varchar](15) NULL,
		   [NAMETYPE] [varchar](60) NULL,
		   [NAME] [varchar](60) NULL,
		   [ADDRESS1] [varchar](60) NULL,
		   [ADDRESS2] [varchar](60) NULL,
		   [CITY] [varchar](60) NULL,
		   [STATE] [varchar](2) NULL,
		   [ZIP] [varchar](10) NULL,
		   [PHONE] [varchar](15) NULL,
		   [CELL] [varchar](15) NULL,
		   [FAX] [varchar](15) NULL,
		   [EMAIL] [varchar](80) NULL,
		   [PHONE_EXT] [varchar](5) NULL,
		   [RECORDID] [varchar](30) NULL,
		   ForRecID int identity)


	--Create table to be used for insert into Audit
	Create Table #ForInsertIntoPrmryAuditTrail(
		CURRENT_VALUE varchar(2000),
		FIELD_NAME varchar(50),
		ORIGINAL_VALUE varchar(2000),
		PRIMARY_KEY_VALUE varchar(50),
		SQL_ACTION varchar(1),
		TABLE_NAME varchar(50),
		USER_ID varchar(4), 
		TRANSACTION_DATETIME datetime)

	--Populate table variables
	select * into #ListUpdateFields from @ListUpdateFields

	select * into #MainTableRec from @ListTableAndRecords

	-- Start populating table (this should create all combinations for insert)
	set @TableName = (select distinct tblName from #MainTableRec)

		  
	--use function to get key field
	select @KeyField = dbo.tfn_ActivityNoField(@TableName)

	Insert into #Contacts (Activity_NO) select ActivityNo from #MainTableRec 



	--Update temp table based
	select @RowId=MAX(RowID) FROM #ListUpdateFields     
	Select @RowNum = Count(*) From #ListUpdateFields      
	WHILE @RowNum > 0                          
	BEGIN   

		select @ColumnName = ColumnName from #ListUpdateFields where RowID= @RowId    
		select @ColumnValue = Value from #ListUpdateFields where RowID= @RowId  

		if len(ltrim(rtrim(@ColumnValue))) > 0 and @ColumnName ='UserID'
		Begin
			set @UserID = @ColumnValue
		End

		if len(ltrim(rtrim(@ColumnValue))) > 0 and @ColumnName ='GroupName'
		Begin
			set @GroupName = @ColumnValue
		End 

		if len(ltrim(rtrim(@ColumnValue))) > 0 and @ColumnName ='TblName'
		Begin
			set @TblName = @ColumnValue 
		End 

		if len(ltrim(rtrim(@ColumnValue))) > 0 and @ColumnName not in ('UserID', 'GroupName','TblName')
		Begin
			set @SQL = 'Update #Contacts set ' + @ColumnName +  ' = ''' + @ColumnValue + ''''
			exec(@sql)
		End 
                                  
		select top 1 @RowId=RowID from #ListUpdateFields where RowID < @RowId order by RowID desc
		set @RowNum = @RowNum - 1                             
       
	END


	update #Contacts set RECORDID ='BPRO:' + right(('00' + str(DATEPART(yy, GETDATE()))),1)
	+ right(('00' + ltrim(str(DATEPART(mm, GETDATE())))),2)
	+ right(('00' + ltrim(str(DATEPART(d, GETDATE())))),2)
	+ right(('00' + ltrim(str(DATEPART(hh, GETDATE())))),2)
	+ right(('00' + ltrim(str(DATEPART(n, GETDATE())))),2)
	+ right(('00' + ltrim(str(DATEPART(ss, GETDATE())))),2)
	+ right(('000000'+ ltrim(str(ForRecID))),6);

	select * from #Contacts

	Set @sql = 'insert into '
	Set @sql = @sql + @TblName
	Set @sql = @sql + '('
	Set @sql = @sql + @KeyField
	Set @sql = @sql + ', NAMETYPE, NAME,ADDRESS1,ADDRESS2,CITY,STATE,ZIP,PHONE,EMAIL,RECORDID,FAX,CELL,PHONE_EXT)
	select Activity_NO,NAMETYPE, NAME,ADDRESS1,ADDRESS2,CITY,STATE,ZIP,PHONE,EMAIL,RECORDID,FAX,CELL,PHONE_EXT from #Contacts'

	--print(@sql)

	exec(@sql)

       --insert into #ForInsertIntoPrmryAuditTrail(TABLE_NAME,PRIMARY_KEY_VALUE ) select tblName, ActivityNo from #MainTableRec

       --insert into Prmry_AuditTrail (CURRENT_VALUE, PRIMARY_KEY_VALUE, SQL_ACTION, TABLE_NAME,TRANSACTION_DATETIME, USER_ID) select @SQL2, PRIMARY_KEY_VALUE, 'U', @TblName, getdate(), @UserID from #ForInsertIntoPrmryAuditTrail


END










GO
