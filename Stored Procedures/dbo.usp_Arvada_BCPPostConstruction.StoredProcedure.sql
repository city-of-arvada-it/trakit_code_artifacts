USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_BCPPostConstruction]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[usp_Arvada_BCPPostConstruction]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_BCPPostConstruction]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[usp_Arvada_BCPPostConstruction]
	@PERMIT_NO varchar(30),
	@recordid varchar(30)=null
as

set nocount on

SELECT 
	i.PERMIT_NO, 
	i.notes, 
	i.InspectionType,
	CONVERT(VARCHAR(20), i.SCHEDULED_DATE, 101) AS SCHEDULED_DATE, 
	CONVERT(VARCHAR(20), i.COMPLETED_DATE, 101) AS COMPLETED_DATE, 
	i.RESULT, 
	i.RECORDID,
	m.site_addr, 
	m.site_subdivision, 
	m.[DESCRIPTION]
FROM   Permit_Inspections  as i
inner join permit_main as m
	on m.PERMIT_NO = i.PERMIT_NO
inner join PERMIT_UDF as u
	on u.permit_no = m.permit_no
WHERE (i.InspectionType = 'POSTCON') AND (i.PERMIT_NO = @PERMIT_NO)
--and i.recordid = @recordid

GO
