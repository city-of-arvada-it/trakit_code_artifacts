USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_GetInspectionsToDelete]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[usp_Arvada_GetInspectionsToDelete]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_GetInspectionsToDelete]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[usp_Arvada_GetInspectionsToDelete]

as
 
select pathURL, ltrim(rtrim(replace(pathURL,'http://74.114.70.93/ivr',''))) as RemoveFile
from Permit_Inspections
where pathurl is not null
and
	(Completed_Date is not null
		or (COMPLETED_DATE is null and 
				(
					(scheduled_Date < dateadd(day,-14,convert(Date, getdate())) and scheduled_Date is not null)
				)
			)
	)	 
 
GO
