USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ExportStandardNotes]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_ExportStandardNotes]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ExportStandardNotes]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  
					  					  					  /*
tsp_ExportStandardNotes @DBName='DEV', @FilePath='C:\CRW\iTrakitExports\', @FileName='StandardNotesExport'

*/

-- =====================================================================
-- Revision History: 
--	RE - 11/8/13 - Modified for 7500 std note
--	mdm - 11/8/13 - added revision history	
-- =====================================================================  

CREATE Procedure [dbo].[tsp_ExportStandardNotes](
@DBName Varchar(50),
@FilePath Varchar(1000),
@FileName Varchar(150),
@ToggleXP_CMDShell INT = 1
)

As

SET NOCOUNT ON

Declare @SQL Varchar(8000)

If @ToggleXP_CMDShell Is Null Set @ToggleXP_CMDShell = 1

IF EXISTS (SELECT * FROM tempdb.sys.objects WHERE object_id = OBJECT_ID(N'tempdb..StandardNotes') AND type in (N'U'))
	Drop Table tempdb..StandardNotes

Create Table tempdb..StandardNotes([GROUPNAME] Varchar(200)
      ,[CATEGORY] Varchar(200)
      ,[TYPENAME] Varchar(200)
      ,[USERID] Varchar(200)
      ,[STD_NOTE] Varchar(7500)
      ,[RECORDID] Varchar(200)
      ,[ORDERID] Varchar(200)
      ,[ENDCHAR] Varchar(5))


Insert Into tempdb..StandardNotes(GROUPNAME,CATEGORY,TYPENAME,USERID,STD_NOTE,RECORDID,ORDERID,ENDCHAR)
Values('GROUPNAME','CATEGORY','TYPENAME','USERID','STD_NOTE','RECORDID','ORDERID','^/>')

Set @SQL = '
Insert Into tempdb..StandardNotes([GROUPNAME],[CATEGORY],[TYPENAME],[USERID],[STD_NOTE],[RECORDID],[ORDERID],[ENDCHAR])
Select 
GROUPNAME=CASE WHEN GROUPNAME = '''' THEN Null ELSE GROUPNAME END,
CATEGORY=CASE WHEN CATEGORY = '''' THEN Null ELSE CATEGORY END,
TYPENAME=CASE WHEN TYPENAME = '''' THEN Null ELSE TYPENAME END,
USERID=CASE WHEN USERID = '''' THEN Null ELSE USERID END,
STD_NOTE=CASE WHEN STD_NOTE = '''' THEN Null ELSE STD_NOTE END,
RECORDID=CASE WHEN RECORDID = '''' THEN Null ELSE RECORDID END,
ORDERID=CASE WHEN ORDERID = Null THEN ''999'' ELSE Convert(Varchar,ORDERID) END,
ENDCHAR = ''^/>''
From ' + @DBName + '..Prmry_StandardNotes
ORDER BY GroupName,ORDERID'
--Print(@SQL)
Exec(@SQL)

Select * From tempdb..StandardNotes


Declare @vDate Varchar(100)
Declare @OutputFileName Varchar(1000)

Set @vDate = Replace(Convert(Varchar,GetDate(),102),'.','_')
Print(@vDate)

--Set @OutputFileName = '"' + @FilePath + @FileName + '_' + @vDate + '.csv"'
Set @OutputFileName = '"' + @FilePath + @FileName + '.csv"'


If @ToggleXP_CMDShell = 1
 Begin
	--Enable xp_cmdshell
	Set @SQL = '
	EXEC sp_configure ''show advanced options'', 1
	RECONFIGURE
	EXEC sp_configure ''xp_cmdshell'', 1
	RECONFIGURE
	'
	Print(@SQL)
	Exec(@SQL)
 End

select @sql = 'bcp "select * from tempdb..StandardNotes" queryout ' + @OutputFileName + ' -c -t"|" -T -S'
+ @@servername
--Print(@SQL)
exec master..xp_cmdshell @sql

Drop Table tempdb..StandardNotes

Set @SQL = '
DELETE ' + @DBName + '.dbo.iTrakitFeeds Where [FileName] = ''' + @FileName + '.csv''

INSERT INTO ' + @DBName + '.dbo.iTrakitFeeds([filename],[data])
SELECT ''' + @FileName + '.csv'', * FROM OPENROWSET(
  BULK N''' + REPLACE(@OutputFileName,'"','') + ''', SINGLE_BLOB
) bt'
Print(@SQL)
Exec(@SQL)

If @ToggleXP_CMDShell = 1
 Begin
	-- Disable xp_cmdshell
	Set @SQL = '
	EXEC sp_configure ''xp_cmdshell'', 0
	RECONFIGURE
	EXEC sp_configure ''show advanced options'', 0
	RECONFIGURE
	'
	Print(@SQL)
	Exec(@SQL)
 End







GO
