USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearch_CompileQuery]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_AdvSearch_CompileQuery]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearch_CompileQuery]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


create procedure [dbo].[tsp_T9_AdvSearch_CompileQuery] 
(
	@UserID varchar(50) = null, -- EWH Dec 11, 2014 issue #33974: restrict CRM records by user
	@SearchTable varchar(50) = 'current', -- current / last / new / saved / shared
	@SearchID int = '-2',
	@SortByColumnAlias varchar(255) = null, -- the column alias the user would like to sort by; secondarily sorted by ["Key Field"] asc
	@SortByDirection varchar(4) = 'asc', -- the sort direction: asc / desc
	@MainWindowRef varchar(30) = 'window.parent', -- used to create html link to navigate to TRAKiT item
	@ResultsWindowRef varchar(30) = '', -- used to create html link to GIS item. If empty an img tag is used instead
	@SearchType varchar(50) = '' out, -- ex: Permit_Actions, Case_People; has to be out because of how we are using it here
	@Description varchar(250) = null out,
	@SearchName varchar(50) = null out,
	@Group varchar(50) out,
	@RefSearchID int = null out,
	@RefTable varchar(50) = null out,
	@SelectClause varchar(max) = '' out,
	@SelectAlias varchar(max) = '' out,
	@FromClause varchar(max) = '' out,
	@WhereClause varchar(max) = '' out,
	@GroupByClause varchar(max) = '' out,
	@OrderByClause varchar(max) = '' out, -- specify [alias] here
	@OrderByInOver varchar(max) = '' out, -- specify [table].[column] here
	@ExecuteQuery bit = 0, -- setting this to 1 will return all records with the aliases as headers
	@IncludeHtmlFlagsForCountColumns bit = 0 -- setting this to 1 will encapsulate the count column values (except 0) with ~dds~ and ~dde~ (i.e. ~dds~123~dde~) which are replaced in the handler with html span tags for setting class name
)

AS
BEGIN
--print(convert(varchar,getdate(),109)) -- EWH: used for measuring the duration of this proc

declare @MainTable varchar(50)
declare @UdfTable varchar(50) -- the custom fields table for a group

declare @details as [udtt_T9_AdvSearch_Detail] -- where the search details are stored
declare @AdditionalTable varchar(50) -- this is @SearchType 
declare @IsMultiTableSearch bit -- true if there is more than 1 table in search

Declare @KeyField_Main as varchar(50)
Declare @MainTableDotKeyField as varchar(255)

Declare @SQL varchar(max) = ''

Declare @TableName as varchar(255)
Declare @FieldName as varchar(255)
Declare @DisplayName as varchar(255)
Declare @Filter as varchar(255)
Declare @Datatype as varchar(255)
Declare @Value1 as varchar(255)
Declare @Value2 as varchar(255)

declare @GisTable varchar(50)
declare @GisRecordID int
declare @GeometryFilterEnabled bit
declare @GeoMainTableJoinPhrase varchar(max)
declare @GisTableJoinPhrase varchar(max)
declare @GisWherePhrase varchar(100)

Declare @CrmRestrictions as varchar(max) = '' -- EWH Dec 11, 2014 issue #33974: restrict CRM records by user

	 
--print(convert(varchar,getdate(),109)) -- EWH: used for measuring the duration of this proc

-- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- FORMAT VARIABLES
-- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------
if @SortByColumnAlias is null set @SortByColumnAlias = ''
if @SortByColumnAlias <> ''
begin
	if left(@SortByColumnAlias,1) = '[' set @SortByColumnAlias = right(@SortByColumnAlias, len(@SortByColumnAlias)-1)
	if right(@SortByColumnAlias,1) = ']' set @SortByColumnAlias = left(@SortByColumnAlias, len(@SortByColumnAlias)-1)
end
-- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------


-- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- FETCH ITEM DATA AND DETAILS
-- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- determine search id for current, new, or last search tables
if @SearchTable in ('current','new','last') set @SearchID = [dbo].[tfn_T9_AdvSearch_GetSearchID](@UserID, @SearchTable)

-- get search item info
declare @IsDirty bit -- not used, but need it to retrieve other out parameter values
insert into @details exec [tsp_T9_AdvSearch_Get] @UserID, @SearchTable, @SearchID, @Group out, @SearchType out, @Description out, @SearchName out, @RefSearchID out, @RefTable out, @IsDirty out, @GisTable out, @GisRecordID out, @GeometryFilterEnabled out

-- make sure we have a group 
if @Group is null set @Group = 'PERMIT' --  TODO: REPLACE THIS WITH...?
--print ' --> --> --> GROUP NAME FROM GET IS: ' + @Group
-- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------


-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- identify @AdditionalTable based on @SearchType, and set main table and UDF table for join
-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

set @AdditionalTable = ''
set @UdfTable = ''
if @Group = 'CRM' or @Group = 'GEO'
begin
	 if @Group = 'CRM'
	 begin
		set @MainTable = 'TVW_CRMISSUES2'  --'CRM_ISSUES'   --> EWH Nov 8, 2017 #11004
		-- CRM has no UDF table
	 end
	 else
	 begin
		set @MainTable = 'GEO_OWNERSHIP'
		set @UdfTable = 'GEO_UDF'
	 end
end
else
begin
	set @MainTable =  @Group + '_MAIN' 
	set @UdfTable = @Group + '_UDF'
end

if @SearchType <> '' set @AdditionalTable = @SearchType -- @SearchType is just the additional table 

set @KeyField_Main = [dbo].[tfn_T9_AdvSearch_KeyField](@MainTable)

if @SearchType = @MainTable set @AdditionalTable = '' -- make sure we don't reference the main table twice

set @MainTableDotKeyField =  '[' + @MainTable + '].[' + @KeyField_Main + ']'

-- determine if there are any tables in search beside the main table
if (select distinct count(TableName) from @details where TableName <> @MainTable) = 0 set @IsMultiTableSearch = 0
else set @IsMultiTableSearch = 1
-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- EWH Sep 29, 2015: set up where and join phrases for geometry filter
-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- EWH Sep 30, 2015: make sure we don't try and create filters using partial info 
if @GeometryFilterEnabled is null set @GeometryFilterEnabled = 0
if @GisTable is null set @GisTable = ''
if @GisRecordID is null set @GisRecordID = -1

if @GeometryFilterEnabled=0 or @GisTable='' or @GisRecordID<1
begin
	set @GeometryFilterEnabled = 0
	set @GisTableJoinPhrase = ''
	set @GeoMainTableJoinPhrase = ''
	set @GisWherePhrase = ''
end
else
begin
	-- establish phrases that join main table to geo_ownership and gis table to main table
	if @GeometryFilterEnabled = 0 or @Group = 'AEC' -- AEC doesn't have a way to join to geo_ownership
	begin
		-- no geometry filters 
		set @GeometryFilterEnabled = 0 
		set @GisTableJoinPhrase = ''
		set @GeoMainTableJoinPhrase = ''
		set @GisWherePhrase = ''
	end
	else
	begin
		-- set GisTable join -- need to use EITHER x,y OR lat,lon -- whichever is not null
		set @GisTableJoinPhrase = ' 
		inner join [Prmry_AdvSearch_Gis' + @GisTable + '] on 
		case when [geo_ownership].[COORD_X] is not null and [geo_ownership].[COORD_Y] is not null then
			[Prmry_AdvSearch_Gis' + @GisTable + '].[Boundary].STIntersects(geometry::Point(geo_ownership.COORD_X, geo_ownership.COORD_Y, [Prmry_AdvSearch_Gis' + @GisTable + '].[SRID]))
		else
			case when geo_ownership.LAT is not null and geo_ownership.LON is not null then 
				[Prmry_AdvSearch_Gis' + @GisTable + '].[Boundary].STIntersects(geometry::Point(geo_ownership.LAT, geo_ownership.LON, [Prmry_AdvSearch_Gis' + @GisTable + '].[SRID]))
			else 0
			end
		end
		=1 '		

		-- set phrase to be added to where clause that filters results by the record in the specified gis table
		set @GisWherePhrase = ' and [Prmry_AdvSearch_Gis' + @GisTable + '].[RecordID]=' + cast(@GisRecordID as varchar)

		-- set phrase that joins geo_ownership to main table

		--> EWH Nov 8, 2017 #11004 [start]
		-- if this is a geo search, geo_ownership is the main table
		if @Group = 'GEO' set @GeoMainTableJoinPhrase = ''
		else set @GeoMainTableJoinPhrase = ' inner join geo_ownership on [' + @MainTable + '].[loc_recordid] = [geo_ownership].[recordid]'
		
		--if @Group = 'CRM' 
		--begin
		--	-- TODO: establish joins for CRM. disabled for now
		--	set @GeometryFilterEnabled = 0  
		--	set @GeoMainTableJoinPhrase = ''
		--	set @GisTableJoinPhrase = ''
		--	set @GisWherePhrase = ''
		--end
		--else
		--begin
		--	-- if this is a geo search, geo_ownership is the main table
		--	if @Group = 'GEO' set @GeoMainTableJoinPhrase = ''
		--	else set @GeoMainTableJoinPhrase = ' inner join geo_ownership on [' + @MainTable + '].[loc_recordid] = [geo_ownership].[recordid]'
		--end
		--> EWH Nov 8, 2017 #11004 [end]

	end
end
-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- EWH Dec 11, 2014 issue #33974: restrict CRM records by user - create where clause segment in @CrmRestrictions if @MainTable = 'TVW_CRMISSUES2'
--									note that if @UserID is not passed then we won't return any records
-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
if @MainTable = 'TVW_CRMISSUES2' --'CRM_ISSUES' --> EWH Nov 8, 2017 #11004
begin
	if isnull(@UserID,'') = '' --don't allow passing an empty string either
	begin
		set @CrmRestrictions=' AND 0=1' -- make sure we don't return any results at all if @UserID is not supplied
	end
	else
	begin
		-- EWH Jan 12, 2015: fixed query below for CRM restrictions
		-- EWH Nov 8, 2017 #11004: replaced crm_issues with new view TVW_CRMISSUES2 in CRM restriction query below
		set @CrmRestrictions=' AND (TVW_CRMISSUES2.NATURETYPE_LIST_ID IS NULL OR TVW_CRMISSUES2.NATURETYPE_LIST_ID IN (select distinct TVW_CRMISSUES2.NATURETYPE_LIST_ID from TVW_CRMISSUES2 inner join crm_lists on NATURETYPE_LIST_ID = list_id where list_text not in (SELECT distinct TYPENAME FROM PRMRY_CRM_RESTR_TYPE_ACCESS WHERE USERID <> ''' + @UserID + ''')))' 
		--set @CrmRestrictions=' AND (CRM_ISSUES.NATURETYPE_LIST_ID IS NULL OR CRM_ISSUES.NATURETYPE_LIST_ID IN (select distinct CRM_Issues.NATURETYPE_LIST_ID from CRM_Issues inner join crm_lists on NATURETYPE_LIST_ID = list_id where list_text not in (SELECT distinct TYPENAME FROM PRMRY_CRM_RESTR_TYPE_ACCESS WHERE USERID <> ''' + @UserID + ''')))' 
	end
end
-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------




-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Split details into 2 objects: outer for the columns in the main / udf / additional tables; inner for the columns that are consolidated to one column displaying counts
-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- columns that are in the main, main udf, or additional tables
declare @detailsOuter [udtt_T9_AdvSearch_Detail]
insert into @detailsOuter select * from @details where (TableName = @MainTable or TableName = @UdfTable or TableName = @AdditionalTable) order by Seq
--select * from @detailsOuter  -- testing


-- columns that are displayed as counts
declare @detailsInner [udtt_T9_AdvSearch_Detail]
insert into @detailsInner select * from @details where (TableName <> @MainTable and TableName <> @UdfTable and TableName <> @AdditionalTable) order by Seq
-- select * from @detailsInner  -- testing
-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Compile filter segments for @detailsOuter and @detailsInner; @filtersInner consolidates columns for the same table into one column and updates the table info accordingly
-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- compile filters for columns that are in the main, main udf, or additional tables
declare @filtersOuter udtt_T9_AdvSearch_DetailFilters
insert into @filtersOuter EXEC [tsp_T9_AdvSearch_CompileFilters] @detailsOuter, 0
--select * from @filtersOuter  -- testing


-- compile filters for columns that are displayed as counts
declare @filtersInner udtt_T9_AdvSearch_DetailFilters
insert into @filtersInner EXEC [tsp_T9_AdvSearch_CompileFilters] @detailsInner, 1
--select * from @filtersInner   -- testing
-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--  Create SELECT clause - includes sorting, but it is not very important here as it is addressed in the Aliases section after this
-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
set @SelectClause = ''
declare @s varchar(max) 
set @s = (
			select case when (tablename = @AdditionalTable) or (tablename = @MainTable) or (tablename = @UdfTable) then 
							-- format column data
							case Datatype	when 'float' then ',cast(round([' + tablename + '].[' + FieldName + '], 2) as numeric(36, 2)) as [' + displayname + ']' -- EWH Oct 27, 2015: round floats to 2 decimal places -- ',str([' + tablename + '].[' + FieldName + ']) as [' + displayname + ']' 
											when 'date'	then ',convert(varchar(10),[' + tablename + '].[' + FieldName + '],101) as [' + displayname + ']' 
											when 'datetime' then ',convert(varchar(10),[' + tablename + '].[' + FieldName + '],101) as [' + displayname + ']' 
											when 'datetime2' then ',convert(varchar(10),[' + tablename + '].[' + FieldName + '],101) as [' + displayname + ']' 
											else ',[' + tablename + '].[' + FieldName + '] as [' + displayname + ']' 
											end
						-- these are the columns with counts
						else
							case when @IncludeHtmlFlagsForCountColumns = 0 
								then ',isnull([' + tablename + '].[' + FieldName + '],''0'') as [' + displayname + ']' -- EWH Aug 8, 2015: we want to see a '0' instead of null for the count fields 
								else ',isnull([' + tablename + '].h,''0'') as [' + displayname + ']' -- EWH Aug 12, 2015: added "html" column 
							end
						end 
			from  
				(
					select TableName, FieldName, DisplayName, Datatype, Seq from @detailsOuter -- the main table, udf table, and additional table columns
					union 

					-- the columns that we just want the counts for - use @filtersInner because the columns are consolidated there
					-- select the TableName as the DisplayName for the Table Columns" as we establish the alias in the JOIN clauses for these
					select [DisplayName] TableName, FieldName, DisplayName, 'int', 10000 from @filtersInner 
				) b
			order by seq 
			for xml path('')
		)
set @SelectClause =  @MainTableDotKeyField + ' as ["Key Field"]' + isnull(@s,'') -- put the key field value first, plus we don't have to strip off the leading comma :)
-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--  Create Column Aliases - the use of quotation marks and a space in ["Key Field"] is intentional to keep from duplicating an alias a user has specified.  
--		NOTE: @SelectAlias should ALWAYS contain ["Key Field"]
-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
set @s = '' -- reuse @s
set @s = (select ',[' + displayname + ']' 
from  
(
	select DisplayName, Seq from @detailsOuter -- the main table, udf table, and additional table columns
	union
	select DisplayName, 10000 from @filtersInner -- the columns that we just want the counts for - 10000 so they are always last - use @filtersInner because the columns are consolidated there
) b
order by seq 
for xml path(''))
set @SelectAlias = '["Key Field"]' + isnull(@s,'')
-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--  Create GROUP BY clause -- creates one row of data containing fields that are in the main or additional tables
-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
set @GroupByClause = ''  -- no more GROUP BY clause
-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--  Create ORDER BY clause in main query - use [alias]   - cast column to sort by as its datatype and add key field as secondary sort
-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--set @OrderByClause = '' -- EWH Aug 12, 2015: we don't need this; the order by in the over clause orders the data for us already

set @OrderByClause = 
(
	select top 1 DisplayName 
	from  
		(
			-- the main table, udf table, and additional table columns
			-- cast to the column's data type (except if varchar / nvarchar already) so the data is sorted appropriately for its data type
			select case when Datatype not in ('varchar','nvarchar') then 'cast([' + DisplayName + '] as ' + Datatype + ') ' 
						else '[' + DisplayName + '] ' 
						end DisplayName 
			from @detailsOuter 
			where DisplayName = @SortByColumnAlias -- confirm the requested display name 

			union
			
			-- the columns that we just want the counts for 
			select '[' + DisplayName + '] '  
			from @filtersInner 
			where DisplayName = @SortByColumnAlias -- confirm the requested display name 
		) b
) 

if (@OrderByClause <> '') and (@OrderByClause is not null)
begin
	if @OrderByClause <> '["Key Field"]' set @OrderByClause = @OrderByClause + @SortByDirection + ',' + '["Key Field"]'  -- add secondary sort
	else set @OrderByClause = @OrderByClause + @SortByDirection -- we are already sorting on ["Key Field"]
end
else set @OrderByClause = '["Key Field"]'

-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------




-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--  Create ORDER BY clause for use in OVER() - use [table].[column] and wrap in count() for columns in tables that are not the main / additional tables
-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
set @OrderByInOver = 
(
select top 1 '[' + tablename + '].[' + FieldName + '] ' -- add in sort direction later so we can compare @OrderByInOver to @MainTableDotKeyField 
from 
	(
		select TableName, FieldName, DisplayName from @detailsOuter -- the main table, udf table, and additional table columns
		union 

		-- the columns that we just want the counts for - use @filtersInner because the columns are consolidated there
		-- select the TableName as the DisplayName for the Table Columns" as we establish the alias in the JOIN clauses for these
		select [DisplayName] TableName, FieldName, DisplayName from @filtersInner 
	) b
where displayname = @SortByColumnAlias
)
-- sort on key field secondarily (or primarily, if a sort column is not specified)
if (@OrderByInOver <> '') and (@OrderByInOver is not null)
begin
	-- make sure we don't reference the same column twice!
	if @OrderByInOver <> @MainTableDotKeyField set @OrderByInOver = @OrderByInOver + @SortByDirection + ',' + @MainTableDotKeyField  -- add secondary sort
	else set @OrderByInOver = @OrderByInOver + @SortByDirection -- we are already sorting on @MainTableDotKeyField
end
else set @OrderByInOver = @MainTableDotKeyField
-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------





-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Create FROM clause 
-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- start with columns that will display data (not counts)
set @FromClause = @MainTable + isnull(
(
	select ' INNER JOIN ' + TableName + ' ON ' + @MainTableDotKeyField + ' = ' + TableName + '.' + [dbo].[tfn_T9_AdvSearch_KeyField](TableName) -- EWH Aug 5, 2015: need to get the key field for the join table too, it is not always the same as the main table key field.
	from @detailsOuter
	where TableName <> @MainTable -- make sure we don't join the main table to itself
	group by TableName -- make sure we don't join a table more than once because it contains more than one column
	for xml path('')
),'')


-- add columns that will be joined as one column per table
if @IsMultiTableSearch = 1 -- only worry about joining if we have more than one table
begin
	
	-- EWH Aug 12, 2015: added column 'h' for columns with counts to display html, assuming we want to display as html
	declare @hColumn varchar(100)
	if @IncludeHtmlFlagsForCountColumns = 1 set @hColumn = ', (''~dds~'' + cast(count(*) as nvarchar) + ''~dde~'') h'
	else set @hColumn = ''

	-- assemble from clause 
	set @FromClause = @FromClause + 
		isnull(
				(
					select	case isnull(CompiledFilter,'') when '' then ' LEFT ' else ' INNER ' end + 'JOIN (select ' + [dbo].[tfn_T9_AdvSearch_KeyField](TableName) + ' kf, count(*) c' + @hColumn + ' FROM [' + TableName + '] ' + -- EWH Aug 5, 2015: look up the key field for the table instead of using @KeyField_Main 
							case isnull(CompiledFilter,'') when '' then '' else ' WHERE ' + isnull(CompiledFilter,'') end + 
							' GROUP BY ' + [dbo].[tfn_T9_AdvSearch_KeyField](TableName) + ') [' + DisplayName + '] ON ' + @MainTableDotKeyField + '=[' + DisplayName + '].kf' -- EWH Aug 5, 2015: look up the key field for the table instead of using @KeyField_Main 
					from @filtersInner
					for xml path('')
				)
		,'')
end


-- EWH Sep 29, 2-15: add joins to the geo_ownership and the appropriate gis table to filter by geometry
set @FromClause = @FromClause + @GeoMainTableJoinPhrase + @GisTableJoinPhrase
-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------





-- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Create WHERE clause for columns that will display data (columns that display counts have where clauses in the joins)
-- ------------------------------------------------------------------------------------------------------------------------------------
set @WhereClause = '1=1' -- initialize 
if (select count(*) from @filtersOuter where CompiledFilter is not null and CompiledFilter<>'') > 0 -- only do this if we have records
begin
	-- assemble where clause with segments
	set @WhereClause = @WhereClause + isnull(
	(
		select ' AND ' + CompiledFilter
		from @filtersOuter where CompiledFilter<>''
		for xml path('')
	),'')
end
set @WhereClause = @WhereClause + @GisWherePhrase + @CrmRestrictions -- add in geometry filter and CRM restrictions
-- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


print '@UserID = ' + isnull(@UserID,'NULL')
print '@SearchTable = ' + isnull(@SearchTable,'NULL')
print '@SearchID (out) = ' + isnull(cast(@SearchID as varchar),'NULL')
print '@Group (out) = ' + isnull(@Group,'NULL')
print '@SearchType (out) = ' + isnull(@SearchType,'NULL')
print '@Description (out) = ' + isnull(@Description,'NULL')
print '@SearchName (out) = ' + isnull(@SearchName,'NULL')
print '@RefTable (out) = ' + isnull(@RefTable,'NULL')
print '@RefSearchID (out) = ' + isnull(cast(@RefSearchID as varchar),'NULL')
print '@SelectAlias (out) = ' + isnull(@SelectAlias, 'NULL')
print '@SelectClause (out) = ' + isnull(@SelectClause, 'NULL')
print '@FromClause (out) = ' + isnull(@FromClause, 'NULL')
print '@WhereClause (out) = ' + isnull(@WhereClause, 'NULL')
print '@GroupByClause (out) = ' + isnull(@GroupByClause, 'NULL')
print '@OrderByClause (out) = ' + isnull(@OrderByClause, 'NULL')
print '@OrderByInOver (out) = ' + isnull(@OrderByInOver, 'NULL')

print '@GisTable = ' + isnull(@GisTable, 'NULL')
print '@GisRecordID = ' + isnull(cast(@GisRecordID as varchar), 'NULL')
print '@GeometryFilterEnabled = ' + isnull(cast(@GeometryFilterEnabled as varchar), 'NULL')
print '@GeoMainTableJoinPhrase = ' + isnull(@GeoMainTableJoinPhrase, 'NULL')
print '@GisTableJoinPhrase = ' + isnull(@GisTableJoinPhrase, 'NULL')
print '@GisWherePhrase = ' + isnull(@GisWherePhrase, 'NULL')
print '------------------------------------------------------------------------------'
print ' select ' + isnull(@SelectClause, '')
print ' from ' + isnull(@FromClause, '')
print case when @WhereClause <> '' then ' where ' + isnull(@WhereClause, '') end
print case when @GroupByClause <> '' then ' group by ' + isnull(@GroupByClause, '') end
print ' order by ' + @OrderByClause

--print(convert(varchar,getdate(),109)) -- EWH: used for measuring the duration of this proc

return
END
GO
