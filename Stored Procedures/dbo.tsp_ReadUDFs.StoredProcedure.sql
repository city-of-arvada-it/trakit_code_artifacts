USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ReadUDFs]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_ReadUDFs]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ReadUDFs]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[tsp_ReadUDFs]
	@TableName varchar(250),
	@FieldName varchar(255),
	@FieldValue varchar(max)
	
AS
BEGIN
	Declare @SQL varchar(max)

	
	if @FieldName is not null and @FieldValue is not null 
		And ( @FieldName like '%_NO' or @FieldName like '%RecordID')
		begin
			Set @SQL  = 'Select Top 1 * from [' + @TableName + '] where [' + @FieldName + '] = ''' + @FieldValue + ''''
			print @SQL
		end
	
	/* permits, projects etc read by their individual activityIDs */
	/* it will only return a single row 

	If i have time afterwards change this to do some sanity checks.
	*/

	exec (@SQL)
	



	
END
GO
