USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ITR_ExportViolationsControl]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_ITR_ExportViolationsControl]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ITR_ExportViolationsControl]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  
					  
					  					  					  /*
tsp_ExportViolationsControl @DBName='DEV', @FilePath='C:\CRW\iTrakitExports\', @FileName='ViolationsControlExport'

*/

CREATE Procedure [dbo].[tsp_ITR_ExportViolationsControl](
@DBName Varchar(50),
@FilePath Varchar(1000),
@FileName Varchar(150),
@ToggleXP_CMDShell INT = 1
)

As

SET NOCOUNT ON

Declare @SQL Varchar(8000)

If @ToggleXP_CMDShell Is Null Set @ToggleXP_CMDShell = 1

IF EXISTS (SELECT * FROM tempdb.sys.objects WHERE object_id = OBJECT_ID(N'tempdb..ViolationsControl') AND type in (N'U'))
	Drop Table tempdb..ViolationsControl

Create Table tempdb..ViolationsControl([Violation_Group] Varchar(200)
      ,[Violation_Title] Varchar(200)
      ,[RECORDID] Varchar(200)
      ,[ORDERID] Varchar(200))


Insert Into tempdb..ViolationsControl(Violation_Group,Violation_Title,RecordID,OrderID)
Values('Violation_Group','Violation_Title','RecordID','OrderID')

Set @SQL = '
Insert Into tempdb..ViolationsControl([Violation_Group],[Violation_Title],[RECORDID],[ORDERID])
Select 
Violation_Group=CASE WHEN Violation_Group = '''' THEN Null ELSE Violation_Group END,
Violation_Title=CASE WHEN Violation_Title = '''' THEN Null ELSE REPLACE(REPLACE(REPLACE(Violation_Title,CHAR(9),''''),CHAR(10),''''),CHAR(13),'''') END,
RECORDID=CASE WHEN RECORDID = '''' THEN Null ELSE RECORDID END,
ORDERID=CASE WHEN ORDERID Is Null THEN ''999'' ELSE Convert(Varchar,ORDERID) END
From ' + @DBName + '..Prmry_ViolationsControl
ORDER BY Violation_Group,Violation_Title'
--Print(@SQL)
Exec(@SQL)

Select * From tempdb..ViolationsControl


Declare @vDate Varchar(100)
Declare @OutputFileName Varchar(1000)

Set @vDate = Replace(Convert(Varchar,GetDate(),102),'.','_')
Print(@vDate)

--Set @OutputFileName = '"' + @FilePath + @FileName + '_' + @vDate + '.csv"'
Set @OutputFileName = '"' + @FilePath + @FileName + '.csv"'


If @ToggleXP_CMDShell = 1
 Begin
	--Enable xp_cmdshell
	Set @SQL = '
	EXEC sp_configure ''show advanced options'', 1
	RECONFIGURE
	EXEC sp_configure ''xp_cmdshell'', 1
	RECONFIGURE
	'
	Print(@SQL)
	Exec(@SQL)
 End

select @sql = 'bcp "select * from tempdb..ViolationsControl" queryout ' + @OutputFileName + ' -c -t"|" -T -S'
+ @@servername
--Print(@SQL)
exec master..xp_cmdshell @sql

Drop Table tempdb..ViolationsControl

Set @SQL = '
DELETE ' + @DBName + '.dbo.iTrakitFeeds Where [FileName] = ''' + @FileName + '.csv''

INSERT INTO ' + @DBName + '.dbo.iTrakitFeeds([filename],[data])
SELECT ''' + @FileName + '.csv'', * FROM OPENROWSET(
  BULK N''' + REPLACE(@OutputFileName,'"','') + ''', SINGLE_BLOB
) bt'
Print(@SQL)
Exec(@SQL)

If @ToggleXP_CMDShell = 1
 Begin
	-- Disable xp_cmdshell
	Set @SQL = '
	EXEC sp_configure ''xp_cmdshell'', 0
	RECONFIGURE
	EXEC sp_configure ''show advanced options'', 0
	RECONFIGURE
	'
	Print(@SQL)
	Exec(@SQL)
 End










GO
