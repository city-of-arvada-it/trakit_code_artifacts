USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_sys_Data_Step4_ExpandColumnSize]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_sys_Data_Step4_ExpandColumnSize]
GO
/****** Object:  StoredProcedure [dbo].[tsp_sys_Data_Step4_ExpandColumnSize]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  					  					  
-- =====================================================================
-- Revision History:
--	mdm - 11/19/13 - added revision history	
--	mdm - 11/19/13 - added chg to cope with '(max)' columns
-- =====================================================================   

CREATE PROCEDURE [dbo].[tsp_sys_Data_Step4_ExpandColumnSize] (
@OutputPath Varchar(1000) = Null
)

AS
BEGIN
  SET NOCOUNT ON

--mdm - 08/15/13; sample @OutputPath:  c:\mayo\Step4_DS_ColumnsAlterSize.sql


----mdm - 08/14/13 - May need to enable xp_cmdshell
---- To allow advanced options to be changed.
--EXEC sp_configure 'show advanced options', 1
--GO
---- To update the currently configured value for advanced options.
--RECONFIGURE
--GO
---- To enable the feature.
--EXEC sp_configure 'xp_cmdshell', 1
--GO
---- To update the currently configured value for this feature.
--RECONFIGURE
--GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[zColumnAlterSizeTemp]') AND type in (N'U'))
BEGIN
create table zColumnAlterSizeTemp
(
SqlToExec varchar(2000)
)
END

truncate table zColumnAlterSizeTemp

insert into zColumnAlterSizeTemp
Select 'IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_NAME = ''' + isc.TABLE_NAME + '''
AND COLUMN_NAME = ''' + isc.COLUMN_NAME + '''
AND CHARACTER_MAXIMUM_LENGTH < ' + cast((isc.CHARACTER_MAXIMUM_LENGTH)as varchar) + ')   
  ALTER TABLE ' + isc.TABLE_NAME + ' ALTER COLUMN [' + isc.COLUMN_NAME + '] ' + isc.data_type + '' + 
  (case isc.data_type
       when 'char' then ' ('
              when 'nchar' then ' ('
                     when 'varbinary' then ' ('
                           when 'varchar' then ' ('
								when 'text' then ' ('
                           else '' end)
                           +
(case isc.data_type
       when 'char' then cast(isc.CHARACTER_MAXIMUM_LENGTH as varchar)
              when 'nchar' then cast(isc.CHARACTER_MAXIMUM_LENGTH as varchar)
                    when 'varbinary' then case when isc.CHARACTER_MAXIMUM_LENGTH =  -1 then 'MAX' else cast(isc.CHARACTER_MAXIMUM_LENGTH as varchar) end
                           when 'varchar' then case when isc.CHARACTER_MAXIMUM_LENGTH =  -1 then 'MAX' else cast(isc.CHARACTER_MAXIMUM_LENGTH as varchar) end
								when 'text' then cast(isc.CHARACTER_MAXIMUM_LENGTH as varchar)
                           else '' end)
                                                       +
  (case isc.data_type
       when 'char' then ')'
              when 'nchar' then ')'
                     when 'varbinary' then ')'
                           when 'varchar' then ')'
								when 'text' then ')'
                           else '' end)                                                                             
                           +
         ';' 
        + ''
  from    information_schema.COLUMNS isc 
       where isc.table_name not like 'z%' and isc.table_name not like 'tvw%' and isc.DATA_TYPE in ('char','nchar','nvarchar','varbinary','varchar')
order by  isc.TABLE_NAME, isc.COLUMN_NAME

update zColumnAlterSizeTemp set SqlToExec = replace([sqltoexec],-1,8000)

delete from zColumnAlterSizeTemp where SqlToExec = 'ALTER TABLE Prmry_Main ALTER COLUMN [Agency_Name] varchar (255);'

declare @FileName as varchar(1000)
If @OutputPath Is Null OR @OutputPath = ''
	Set @FileName = 'C:\crw\TEMP\Step4_DS_ColumnsAlterSize.sql'
ELSE
	Set @FileName = @OutputPath
--set @FileName = 'c:\mayo\Step4_DS_ColumnsAlterSize.sql'
--select @FileName

declare @dbName varchar(200)
set @dbName = db_name()
--select @dbName


declare @cmd varchar(2000)
--set @cmd = 'bcp "select SqlToExec from SanClemente2012.dbo.zColumnAlterSizeTemp order by SqlToExec" queryout "'
set @cmd = 'bcp "select SqlToExec from '
set @cmd = @cmd + @dbname + '.dbo.zColumnAlterSizeTemp order by SqlToExec" queryout "'
set @cmd = @cmd + @FileName + '" -Utrakit -Ptrakit -c -S' + @@SERVERNAME

--select @cmd
exec xp_cmdshell @cmd

select SqlToExec from zColumnAlterSizeTemp order by SqlToExec


--drop table zColumnAlterSizeTemp

---- Disable xp_cmdshell
--Set @SQL = '
--EXEC sp_configure ''xp_cmdshell'', 0
--RECONFIGURE
--EXEC sp_configure ''show advanced options'', 0
--RECONFIGURE
--GO


END









GO
