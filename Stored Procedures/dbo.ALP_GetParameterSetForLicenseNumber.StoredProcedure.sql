USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[ALP_GetParameterSetForLicenseNumber]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[ALP_GetParameterSetForLicenseNumber]
GO
/****** Object:  StoredProcedure [dbo].[ALP_GetParameterSetForLicenseNumber]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- ===============================================================================
-- Author:		Erick Hampton
-- Create date: Dec 16, 2016
-- Description:	Gets the Parameter Set ID for a given license number. 
--				Optionally only returns value if license is eligible for renewal
-- ===============================================================================
CREATE PROCEDURE [dbo].[ALP_GetParameterSetForLicenseNumber]

	@LicenseNumber varchar(20) = NULL,
	@EligibleOnly bit = 1	-- if this is 1, the ParameterSetID will be returned only if the license is eligible for renewal

AS
BEGIN
	SET NOCOUNT ON;

	if @EligibleOnly = 1
	begin
		select ParameterSetID from [ALP_LicenseNumbersEligibleForRenewal] where license_no=@LicenseNumber
	end
	else
	begin

		select ParameterSetID from [ALP_ParameterSetAndRenewalPeriodByLicenseNumber] where license_no=@LicenseNumber
	end
END


GO
