USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_SSRS_Multi_Receipt_Fees]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_SSRS_Multi_Receipt_Fees]
GO
/****** Object:  StoredProcedure [dbo].[tsp_SSRS_Multi_Receipt_Fees]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[tsp_SSRS_Multi_Receipt_Fees] 
	@RECEIPT_NO VARCHAR(300),
	@USERID VARCHAR(100) = NULL,
	@VAL1 VARCHAR(100) = NULL
	
AS
BEGIN

create table #table1
	(RECORD_NO varchar(2000),
	RECORD_TYPE varchar(2000),
	CODE varchar(2000),
	ITEM varchar(2000),
	PARENT_DESCRIPTION varchar(2000),
	DESCRIPTION varchar(2000),
	ACCOUNT varchar(2000),
	QUANTITY float,
	PAID_AMOUNT float,
	PAID_DATE date,
	RECEIPT_NO varchar(2000),
	CHECK_NO varchar(2000),
	PAY_METHOD varchar(2000),
	PAID_BY varchar(2000),
	COLLECTED_BY varchar(2000),
	FEETYPE varchar(2000),
	DEPOSITID varchar(2000),
	SITE_ADDR varchar(2000),
	SITE_APN varchar (2000),
	STATUS varchar(2000)) ;

create index #table1_ix1 on #table1 (RECORD_NO);

--ALL FEES-------------------------------------------------------------------------------------------------------------------------------------------

BEGIN
	insert into #table1 
	(RECORD_NO
	,RECORD_TYPE
	,CODE
	,ITEM
	,DESCRIPTION
	,PARENT_DESCRIPTION
	,ACCOUNT
	,QUANTITY
	,PAID_AMOUNT
	,PAID_DATE
	,RECEIPT_NO
	,CHECK_NO
	,SITE_ADDR
	,SITE_APN
	,PAY_METHOD
	,COLLECTED_BY
	,FEETYPE
	,DEPOSITID
	,PAID_BY)
	select	Fees.ACTIVITYID
			,CASE WHEN Fees.ActivityTypeID = 1
			THEN 'PermitTRAK'
			WHEN Fees.ActivityTypeID = 2
			THEN 'ProjectTRAK'
			WHEN Fees.ActivityTypeID = 3
			THEN 'LicenseTRAK'
			WHEN Fees.ActivityTypeID = 4
			THEN 'CodeTRAK'
			WHEN Fees.ActivityTypeID = 6
			THEN 'AECTRAK'
			END
			,Fees.CODE
			,NULL
			,Fees.DESCRIPTION
			,NULL
			,Fees.ACCOUNT
			,Fees.QUANTITY
			,Fees.PAID_AMOUNT
			,Fees.PAID_DATE
			,Fees.RECEIPT_NO
			,Fees.CHECK_NO
			,CASE WHEN Fees.ActivityTypeID = 1
			THEN Permit_Main.SITE_ADDR
			WHEN Fees.ActivityTypeID = 2
			THEN Project_Main.SITE_ADDR
			WHEN Fees.ActivityTypeID = 3
			THEN License2_Main.SITE_ADDR
			WHEN Fees.ActivityTypeID = 4
			THEN Case_Main.SITE_ADDR
			WHEN Fees.ActivityTypeID = 6
			THEN AEC_Main.ADDRESS1
			END
			,CASE WHEN Fees.ActivityTypeID = 1
			THEN Permit_Main.SITE_APN
			WHEN Fees.ActivityTypeID = 2
			THEN Project_Main.SITE_APN
			WHEN Fees.ActivityTypeID = 3
			THEN License2_Main.SITE_APN
			WHEN Fees.ActivityTypeID = 4
			THEN Case_Main.SITE_APN
			WHEN Fees.ActivityTypeID = 6
			THEN NULL
			END
			,Fees.PAY_METHOD
			,Fees.COLLECTED_BY
			,Fees.FEETYPE
			,Fees.DEPOSITID
			,Fees.PAID_BY
	from Fees left join Permit_Main 
	on Permit_Main.PERMIT_NO = Fees.ACTIVITYID
	and Fees.ActivityTypeID = Permit_Main.ActivityTypeID
	left join Project_Main
	on Project_Main.PROJECT_NO = Fees.ACTIVITYID
	and Fees.ActivityTypeID = Project_Main.ActivityTypeID
	left join License2_Main
	on License2_Main.LICENSE_NO = Fees.ACTIVITYID
	and Fees.ActivityTypeID = License2_Main.ActivityTypeID
	left join Case_Main
	on Case_Main.CASE_NO = Fees.ACTIVITYID
	and Fees.ActivityTypeID = Case_Main.ActivityTypeID
	left join AEC_Main
	on AEC_Main.ST_LIC_NO = Fees.ACTIVITYID
	and Fees.ActivityTypeID = AEC_Main.ActivityTypeID
	where DETAIL = 0 
	and PAID = 1 
	and RECEIPT_NO = @RECEIPT_NO 
	and PAID_AMOUNT <> 0 
	and Fees.ActivityTypeID in (1,2,3,4,6)
UNION ALL
	select	SubFees.ACTIVITYID
			,CASE WHEN Fees.ActivityTypeID = 1
			THEN 'PermitTRAK'
			WHEN Fees.ActivityTypeID = 2
			THEN 'ProjectTRAK'
			WHEN Fees.ActivityTypeID = 3
			THEN 'LicenseTRAK'
			WHEN Fees.ActivityTypeID = 4
			THEN 'CodeTRAK'
			WHEN Fees.ActivityTypeID = 6
			THEN 'AECTRAK'
			END
			,SubFees.CODE
			,SubFees.ITEM
			,SubFees.DESCRIPTION
			,Fees.DESCRIPTION
			,SubFees.ACCOUNT
			,SubFees.QUANTITY
			,SubFees.AMOUNT
			,Fees.PAID_DATE 
			,Fees.RECEIPT_NO
			,Fees.CHECK_NO
			,CASE WHEN Fees.ActivityTypeID = 1
			THEN Permit_Main.SITE_ADDR
			WHEN Fees.ActivityTypeID = 2
			THEN Project_Main.SITE_ADDR
			WHEN Fees.ActivityTypeID = 3
			THEN License2_Main.SITE_ADDR
			WHEN Fees.ActivityTypeID = 4
			THEN Case_Main.SITE_ADDR
			WHEN Fees.ActivityTypeID = 6
			THEN AEC_Main.ADDRESS1
			END
			,CASE WHEN Fees.ActivityTypeID = 1
			THEN Permit_Main.SITE_APN
			WHEN Fees.ActivityTypeID = 2
			THEN Project_Main.SITE_APN
			WHEN Fees.ActivityTypeID = 3
			THEN License2_Main.SITE_APN
			WHEN Fees.ActivityTypeID = 4
			THEN Case_Main.SITE_APN
			WHEN Fees.ActivityTypeID = 6
			THEN NULL
			END
			,Fees.PAY_METHOD
			,Fees.COLLECTED_BY
			,SubFees.SUBFEETYPE
			,SubFees.DEPOSITID
			,Fees.PAID_BY
	from SubFees inner join Fees 
	on Fees.RECORDID = SubFees.PARENTID
	left join Permit_Main 
	on Permit_Main.PERMIT_NO = SubFees.ACTIVITYID
	and SubFees.ActivityTypeID = Permit_Main.ActivityTypeID
	left join Project_Main
	on Project_Main.PROJECT_NO = SubFees.ACTIVITYID
	and SubFees.ActivityTypeID = Project_Main.ActivityTypeID
	left join License2_Main
	on License2_Main.LICENSE_NO = SubFees.ACTIVITYID
	and SubFees.ActivityTypeID = License2_Main.ActivityTypeID
	left join Case_Main
	on Case_Main.CASE_NO = SubFees.ACTIVITYID
	and SubFees.ActivityTypeID = Case_Main.ActivityTypeID
	left join AEC_Main
	on AEC_Main.ST_LIC_NO = SubFees.ACTIVITYID
	and SubFees.ActivityTypeID = AEC_Main.ActivityTypeID
	where Fees.DETAIL = 1 
	and SubFees.PAID = 1
	and RECEIPT_NO = @RECEIPT_NO
	and SubFees.AMOUNT <> 0
	and Fees.ActivityTypeID in (1,2,3,4,6)
	and SUBFEETYPE <> 'REFUND'	
UNION ALL
	select	SubFees.ACTIVITYID
			,CASE WHEN Fees.ActivityTypeID = 1
			THEN 'PermitTRAK'
			WHEN Fees.ActivityTypeID = 2
			THEN 'ProjectTRAK'
			WHEN Fees.ActivityTypeID = 3
			THEN 'LicenseTRAK'
			WHEN Fees.ActivityTypeID = 4
			THEN 'CodeTRAK'
			WHEN Fees.ActivityTypeID = 6
			THEN 'AECTRAK'
			END
			,SubFees.CODE
			,SubFees.ITEM
			,SubFees.DESCRIPTION
			,Fees.DESCRIPTION
			,SubFees.ACCOUNT
			,SubFees.QUANTITY
			,SubFees.AMOUNT
			,CONVERT(date, PaymentTransaction.Date) 
			,PaymentTransaction.ReceiptNumber 
			,PaymentTransactionFee.PaymentMethodDetail 
			,CASE WHEN Fees.ActivityTypeID = 1
			THEN Permit_Main.SITE_ADDR
			WHEN Fees.ActivityTypeID = 2
			THEN Project_Main.SITE_ADDR
			WHEN Fees.ActivityTypeID = 3
			THEN License2_Main.SITE_ADDR
			WHEN Fees.ActivityTypeID = 4
			THEN Case_Main.SITE_ADDR
			WHEN Fees.ActivityTypeID = 6
			THEN AEC_Main.ADDRESS1
			END
			,CASE WHEN Fees.ActivityTypeID = 1
			THEN Permit_Main.SITE_APN
			WHEN Fees.ActivityTypeID = 2
			THEN Project_Main.SITE_APN
			WHEN Fees.ActivityTypeID = 3
			THEN License2_Main.SITE_APN
			WHEN Fees.ActivityTypeID = 4
			THEN Case_Main.SITE_APN
			WHEN Fees.ActivityTypeID = 6
			THEN NULL
			END
			,PaymentTransactionFee.PaymentMethod 
			,SubFees.ASSESSED_BY 
			,SUBFEETYPE
			,Fees.DEPOSITID
			,PaymentTransaction.PaidBy 
	from SubFees inner join Fees 
	on Fees.RECORDID = SubFees.PARENTID
	inner join  PaymentTransactionFee
	on SubFees.ActivityID = PaymentTransactionFee.ActivityId
	and SubFees.PARENTID = PaymentTransactionFee.RecordId
	inner join PaymentTransactionSubfee
	on SubFees.RECORDID = PaymentTransactionSubFee.RECORDID
	and PaymentTransactionFee.ID = PaymentTransactionSubFee.TRANSACTIONFEEID
	inner join PaymentTransaction
	on PaymentTransaction.ID = PaymentTransactionFee.TransactionId
	and PaymentTransaction.TypeId = 2
	and PaymentTransaction.StatusId in (4,6)
	left join Permit_Main 
	on Permit_Main.PERMIT_NO = SubFees.ACTIVITYID
	and SubFees.ActivityTypeID = Permit_Main.ActivityTypeID
	left join Project_Main
	on Project_Main.PROJECT_NO = SubFees.ACTIVITYID
	and SubFees.ActivityTypeID = Project_Main.ActivityTypeID
	left join License2_Main
	on License2_Main.LICENSE_NO = SubFees.ACTIVITYID
	and SubFees.ActivityTypeID = License2_Main.ActivityTypeID
	left join Case_Main
	on Case_Main.CASE_NO = SubFees.ACTIVITYID
	and SubFees.ActivityTypeID = Case_Main.ActivityTypeID
	left join AEC_Main
	on AEC_Main.ST_LIC_NO = SubFees.ACTIVITYID
	and SubFees.ActivityTypeID = AEC_Main.ActivityTypeID
	where Fees.DETAIL = 1 
	and SubFees.PAID = 1
	and PaymentTransaction.ReceiptNumber = @RECEIPT_NO
	and SubFees.AMOUNT <> 0
	and Fees.ActivityTypeID in (1,2,3,4,6)
	and SUBFEETYPE = 'REFUND';
END

--BONDS-------------------------------------------------------------------------------------------------------------------------------------------

BEGIN

	insert into #table1 
	(RECORD_NO
	,RECORD_TYPE
	,CODE
	,DESCRIPTION
	,STATUS
	,ACCOUNT
	,PAID_AMOUNT
	,PAID_DATE
	,RECEIPT_NO
	,CHECK_NO
	,SITE_ADDR
	,SITE_APN
	,PAY_METHOD
	,COLLECTED_BY
	,FEETYPE
	,PAID_BY)
	select	Bonds.ACTIVITYID
		,CASE WHEN Bonds.ActivityTypeID = 1
			THEN 'PermitTRAK'
			WHEN Bonds.ActivityTypeID = 2
			THEN 'ProjectTRAK'
		END
		,Bonds.CODE
		,Bonds.DESCRIPTION
		,Bonds.STATUS
		,Bonds.ACCOUNT
		,Bonds.BOND_AMOUNT
		,Bonds.PAID_DATE
		,Bonds.RECEIPT_NO
		,Bonds.CHECK_NO
		,CASE WHEN Bonds.ActivityTypeID = 1
			THEN Permit_Main.SITE_ADDR
			WHEN Bonds.ActivityTypeID = 2
			THEN Project_Main.SITE_ADDR
		END
		,CASE WHEN Bonds.ActivityTypeID = 1
			THEN Permit_Main.SITE_APN
			WHEN Bonds.ActivityTypeID = 2
			THEN Project_Main.SITE_APN
		END
		,Bonds.PAY_METHOD
		,Bonds.COLLECTED_BY
		,Bonds.FEETYPE
		,Bonds.PAID_BY
	from Bonds left join Permit_Main 
	on Permit_Main.PERMIT_NO = Bonds.ACTIVITYID
	and Bonds.ActivityTypeID = Permit_Main.ActivityTypeID
	left join Project_Main 
	on Project_Main.PROJECT_NO = Bonds.ACTIVITYID
	and Bonds.ActivityTypeID = Project_Main.ActivityTypeID
	where PAID = 1 
	and RECEIPT_NO = @RECEIPT_NO 
	and PAID_AMOUNT > 0 
	and Bonds.STATUS = 'ORIGINAL'
	and Bonds.ACTIVITYTYPEID in (1,2);

END	

--Offset any Deposit Reductions
insert into #table1
	(RECORD_NO
	,RECORD_TYPE
	,ACCOUNT
	,CODE
	,PAID_AMOUNT
	,PAID_DATE
	,RECEIPT_NO
	,CHECK_NO
	,PAY_METHOD
	,FEETYPE
	,DEPOSITID
	,COLLECTED_BY)
select
	  RECORD_NO
	,RECORD_TYPE
	,(select ACCOUNT from Fees where Fees.RECORDID = #table1.DEPOSITID)
	,CODE
	,(PAID_AMOUNT*-1)
	,PAID_DATE
	,'OFFSET'
	,CHECK_NO
	,'OFFSET'
	,'OFFSET'
	,DEPOSITID
	,COLLECTED_BY
from #table1 
	where DEPOSITID in 
		(select RECORDID from Fees 
			where ACTIVITYID = #table1.RECORD_NO 
			and FEETYPE = 'DEPOSIT'
			and Fees.ActivityTypeID in (1,2,3,4,6))
	and FEETYPE <> 'REFUND' ;

--Update Credits and Refunds
update #table1 set #table1.PAID_AMOUNT = #table1.PAID_AMOUNT * -1 where (CODE = 'REFUND' or FEETYPE = 'REFUND') ;
update #table1 set #table1.PAID_AMOUNT = #table1.PAID_AMOUNT * -1 where (CODE = 'CREDIT' or ITEM = 'CREDIT')  ;

--THIS SELECT NEEDS TO BE HERE FOR SSRS TO GET DATA------------------------------------------
select * from #table1 ;
---------------------------------------------------------------------------------------------
END

--********************************************START - Andrew Thomas 6/18/18 - Adding tables for RMConsole Integration**********************************************
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RMConsole_Document_Paths]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[RMConsole_Document_Paths] (
	[Document Type] varchar(100) not null,
	[Module] varchar(15) not null,
	[RMConsole Path] varchar(2000) not null,
	[ID] int identity(1,1) not null,
	PRIMARY KEY (ID)
	);
END
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RMConsole_Document_Types]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[RMConsole_Document_Types] (
	[ID] int identity(1,1) not null,
	[Document Type] varchar(200) not null,
	PRIMARY KEY (ID)
	);
END
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RMConsole_Filename_Config]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[RMConsole_Filename_Config] (
	[ID] int identity(1,1) not null,
	[FileName_Part_1] varchar(50) null,
	[FileName_Part_2] varchar(50) null,
	[FileName_Part_3] varchar(50) null,
	[FileName_Part_4] varchar(50) null,
	[FormatForActivity] varchar(20) null,
	PRIMARY KEY (ID)
	);
END
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[RMConsole_Metadata_Mappings]') AND type in (N'U'))
BEGIN
	CREATE TABLE [dbo].[RMConsole_Metadata_Mappings] (
	[ID] int identity(1,1) not null,
	[MetaDataFieldName] varchar(100) null,
	[TrakitField] varchar(100) null,
	PRIMARY KEY (ID)
	);
END
GO
