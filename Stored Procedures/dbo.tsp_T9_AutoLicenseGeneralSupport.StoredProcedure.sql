USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AutoLicenseGeneralSupport]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_AutoLicenseGeneralSupport]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AutoLicenseGeneralSupport]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =====================================================================
-- Revision History:
--	RB - 10/1/2015 Initial 

-- =====================================================================   	
CREATE Procedure [dbo].[tsp_T9_AutoLicenseGeneralSupport](
	 @LicenseNo varchar(20), 
	 @ParentActivityNo varchar(15),   
	 @ChildGroup varchar(20),
	 @SiteAPN varchar(50),
	 @isGeoUDFSearch bit, 
	 @isLinkedActivities bit, 
	 @isLinkedAECBilling bit, 
	 @isMostRecentInspDate bit

)

As
BEGIN
	Declare @SQLStatement varchar(1500)
	
	
		
	If @isGeoUDFSearch = 1 and (@LicenseNo is not NULL and @LicenseNo <> '')
		Begin

		print 'here'

			set @SQLStatement = 'select GOWN.status, GU.[QUARTER], GU.[INTERVAL] from license2_main LM left join geo_ownership GOWN on LM.loc_recordid = GOWN.recordid ' + 
								'left join GEO_UDF GU on GOWN.RECORDID  = GU.LOC_RECORDID where  LM.license_no = ' + 
								CHAR(39) + @LicenseNo + CHAR(39) + ' and GOWN.status = ' + CHAR(39) + 'ACTIVE' + CHAR(39)

			print @SQLStatement				

			exec (@SQLStatement)

		End
	Else if @isLinkedActivities = 1 and (@ParentActivityNo is not NULL and  @ParentActivityNo <> '') and (@ParentActivityNo is not NULL and @ParentActivityNo <> '')
		Begin
			set @SQLStatement =  'Select ChildActivityNo from lnk_activities where ParentActivityNo=' + CHAR(39) + @ParentActivityNo + CHAR(39) + 
									' and ChildGroup = ' + CHAR(39) + @ChildGroup + CHAR(39)

			print @SQLStatement

			exec (@SQLStatement)

		End

	Else if @isLinkedAECBilling  = 1 and (@LicenseNo is not NULL and @LicenseNo <> '')
		Begin
			set @SQLStatement =   'Select * from license2_people where LICENSE_NO = ' + CHAR(39) + @LicenseNo + CHAR(39) + 
									' and (ID is not NULL and ID <> ' + CHAR(39) + CHAR(39) + ') and NAMETYPE = ' + CHAR(39) + 'BILLING CONTACT' + CHAR(39)

			print @SQLStatement

			exec (@SQLStatement)

		End
	Else if @isMostRecentInspDate  = 1 and (@SiteAPN is not NULL and @SiteAPN <> '')
		Begin
			set @SQLStatement =  'Select ISNULL(MAX(LI.COMPLETED_DATE), ' + CHAR(39) + CHAR(39) + ') as [LATEST_INSPECTION_DATE] from LICENSE2_MAIN LM left join LICENSE2_INSPECTIONS LI on LM.LICENSE_NO = LI.LICENSE_NO 
                                  where LM.SITE_APN = ' + CHAR(39) + @SiteAPN + CHAR(39) + ' and LM.SITE_GEOTYPE = ' + CHAR(39) + 'ADDRESS' + CHAR(39) + ' and LM.STATUS NOT IN (' + CHAR(39) + 'CANCELLED' + CHAR(39) + ', ' + CHAR(39) + 'VOID' + CHAR(39) + ' ) and LI.INSPECTIONTYPE = ' + CHAR(39) + 'COMPLIANCE INSPECTION' + CHAR(39) + ' AND  
								  LI.COMPLETED_DATE BETWEEN cast(' + CHAR(39) + '01/01/' + CHAR(39) + ' + cast(year(dateadd(year, -1, getdate())) as varchar(4)) as datetime) AND cast(' + CHAR(39) + '12/31/' + CHAR(39) + ' + cast(year(dateadd(year, -1, getdate())) as varchar(4)) as datetime)'

			print @SQLStatement

			exec (@SQLStatement)

		End

End

GO
