USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearch_UpdateSearchManager]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_AdvSearch_UpdateSearchManager]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearch_UpdateSearchManager]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[tsp_T9_AdvSearch_UpdateSearchManager]
	@UserID varchar(50) = NULL,
	@udtt_T9_AdvSearch_IncludedSearches as udtt_T9_AdvSearch_IncludedSearches readonly
as
begin


--Saved Searches


Update P
set P.DisplayInUserList = I.DisplayInUserList, P.Seq = I.Seq
from Prmry_AdvSearch_SavedItem P
Join @udtt_T9_AdvSearch_IncludedSearches I 
on P.RecordID = I.ItemID  and I.DepartmentID = -1

--Shared Searches
Update DMI
set DMI.DisplayInUserList = I.DisplayInUserList, DMI.Seq = I.Seq
from Prmry_AdvSearch_DepartmentMembersItems DMI
join Prmry_AdvSearch_SharedItem SI 
On DMI.SharedSearchID = SI.RecordID
Join @udtt_T9_AdvSearch_IncludedSearches I 
on SI.RecordID = I.ItemID and I.DepartmentID <> -1 


end

GO
