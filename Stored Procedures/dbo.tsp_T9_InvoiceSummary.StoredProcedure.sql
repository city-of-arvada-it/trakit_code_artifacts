USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_InvoiceSummary]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_InvoiceSummary]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_InvoiceSummary]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================
-- Revision history: 
--	mdm - 01/22/14 - added to VSS
--	AU  - 05/08/19 - Increased ACTNO to be inline with the rest of Community Development
-- ===================================================================== 

--MODULE,ACTNO,DESCRIPTION,PAID_AMOUNT,AMOUNT,PAY_METHOD,PAID_DATE,CHECK_NO
CREATE procedure [dbo].[tsp_T9_InvoiceSummary]
@InvoiceNumber as varchar(200)
as

--RB Check if table exists before creating it
   IF OBJECT_ID('tempdb..#Invoicedfees') IS NOT NULL  
	drop table #Invoicedfees

create table #Invoicedfees (MODULE varchar(10),ACTNO varchar(30),[DESCRIPTION] varchar(60),PAID_AMOUNT DECIMAL(14,2),
AMOUNT DECIMAL(14,2),PAY_METHOD varchar(20),CHECK_NO varchar(20),PAID_DATE datetime)

--RB Check if table exists before creating it
   IF OBJECT_ID('tempdb..#InvoicedDateUser') IS NOT NULL  
	drop table #InvoicedDateUser

create table #InvoicedDateUser(InvoiceDate datetime,CreatedBy varchar(60),InvoiceTotal money)

insert into #Invoicedfees
select 'PROJECT' as MODULE,PROJECT_NO as ACTNO,[DESCRIPTION],PAID_AMOUNT,AMOUNT,PAY_METHOD,CHECK_NO,PAID_DATE
from project_fees
where INVOICE_NO = @InvoiceNumber
insert into #Invoicedfees
select 'PERMIT' as MODULE,PERMIT_NO as ACTNO,[DESCRIPTION],PAID_AMOUNT,AMOUNT,PAY_METHOD,CHECK_NO,PAID_DATE
from permit_fees
where INVOICE_NO = @InvoiceNumber
insert into #Invoicedfees
select 'CASE' as MODULE,CASE_NO as ACTNO,[DESCRIPTION],PAID_AMOUNT,AMOUNT,PAY_METHOD,CHECK_NO,PAID_DATE
from case_fees
where INVOICE_NO = @InvoiceNumber
insert into #Invoicedfees
select 'LICENSE2' as MODULE,LICENSE_NO as ACTNO,[DESCRIPTION],PAID_AMOUNT,AMOUNT,PAY_METHOD,CHECK_NO,PAID_DATE
from license2_fees
where INVOICE_NO = @InvoiceNumber
insert into #Invoicedfees
select 'AEC' as MODULE,ST_LIC_NO as ACTNO,[DESCRIPTION],PAID_AMOUNT,AMOUNT,PAY_METHOD,CHECK_NO,PAID_DATE
from aec_fees
where INVOICE_NO = @InvoiceNumber

select * from #Invoicedfees
insert into #InvoicedDateUser select InvoiceDate,CreatedBy,InvoiceTotal from Invoice_Main 
where InvoiceNumber = @InvoiceNumber
select * from #InvoicedDateUser
drop table #Invoicedfees
drop table #InvoicedDateUser 

GO
