USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GblSearchReviewCenter]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_GblSearchReviewCenter]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GblSearchReviewCenter]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  					  					  					  
					  					


/*

Testing:
EXEC	[dbo].[tsp_GblSearchReviewCenter]
		@ContactID = NULL,
		@vStartDate = '2012-04-18 00:00:00',
		@vEndDate = '2017-04-18 00:00:00',
		@DateType = 'Scheduled_Date',
		@ReviewGroupName = NULL,
		@ReviewGroupGroupName = NULL,
		@Group = NULL,
		@Type = NULL,
		@ShowOverdueItems = 1

select * from reviews
*/

CREATE Procedure [dbo].[tsp_GblSearchReviewCenter](

@ContactID Varchar(1000) = Null,
@vStartDate Varchar(100) = Null,
@vEndDate Varchar(100) = Null,
@DateType Varchar(100) = Null,
@ReviewGroupName Varchar(500) = Null,
@ReviewGroupGroupName Varchar(500) = Null,
@Group Varchar(500) = Null,
@Type Varchar(500) = Null,
@ShowOverdueItems INT = Null
)


As

Declare @SQL Varchar(8000)
Declare @WHERE Varchar(5000)
Declare @StartDate Datetime 
Declare @EndDate Datetime

Set @WHERE = ''
Set @StartDate  = Null
Set @EndDate  = Null
If @ShowOverdueItems Is Null Set @ShowOverdueItems = 0   -- rrr 2/27/2013 - added
If @vStartDate = '' Set @vStartDate = Null
If @vStartDate = '1/1/0001 12:00:00 AM' Set @vStartDate = Null
If @vEndDate = '' Set @vEndDate = Null
If @vEndDate = '1/1/0001 12:00:00 AM' Set @vEndDate = Null

If @vStartDate Is Null Set @vStartDate = Convert(Varchar,GetDate(),101)
If @vEndDate Is Null Set @vEndDate = Convert(Varchar,GetDate(),101)

Set @vStartDate = CONVERT(Varchar,CONVERT(Datetime,@vStartDate),101)
Set @vEndDate = CONVERT(Varchar,CONVERT(Datetime,@vEndDate),101)

Print(@vStartDate)
Print(@vEndDate)

Set @StartDate = CONVERT(Datetime,@vStartDate)
Set @EndDate = CONVERT(Datetime,DATEADD(dd,1,@vEndDate))


If @ContactID = '' Set @ContactID = Null
If @ReviewGroupName = '' Set @ReviewGroupName = Null
If @ReviewGroupGroupName = '' Set @ReviewGroupGroupName = Null
If @DateType = '' Set @DateType = Null
If @vStartDate = '' Set @vStartDate = Null
If @vEndDate = '' Set @vEndDate = Null
If @Group = '' Set @Group = Null
If @Type = '' Set @Type = Null


If @ContactID Is Not Null Set @WHERE = @WHERE + CHAR(10) + ' AND CONTACT IN (''' + REPLACE(REPLACE(@ContactID,'''',''''''),',',''',''') + ''')'
If @ReviewGroupGroupName Is Not Null Set @ReviewGroupGroupName = Replace(Replace(Replace(@ReviewGroupGroupName,'LICENSES','LICENSE2'),'PROJECTS','PROJECT'),'PERMITS','PERMIT')
If @ReviewGroupName Is Not Null Set @WHERE = @WHERE + CHAR(10) + ' AND ( tGroup = ''' + @ReviewGroupGroupName + ''' AND ReviewGroup = ''' + @ReviewGroupName + ''') '


If @Group Is Not Null Set @WHERE = @WHERE + CHAR(10) + ' AND tGroup IN (''' + REPLACE(@Group,',',''',''') + ''')'
If @Type Is Not Null Set @WHERE = @WHERE + CHAR(10) + ' AND ReviewType IN (''' + REPLACE(@Type,',',''',''') + ''')'

 If @ShowOverdueItems = 1
	Set @WHERE = @WHERE + CHAR(10) + ' AND DATE_RECEIVED is Null '

If @vStartDate Is Not Null Set @StartDate = CONVERT(Datetime,@vStartDate)
If @vEndDate Is Not Null Set @EndDate = CONVERT(Datetime,DATEADD(dd,1,@vEndDate))
Print(@StartDate)
Print(@EndDate)

 
If @DateType Is Not Null
 Begin
	If @StartDate Is Not Null 
	 Begin
		Set @WHERE = @WHERE + ' AND ( '
		Set @WHERE = @WHERE + CHAR(10) + ' ( ' + @DateType + ' >= Convert(Datetime,''' + Convert(Varchar,@StartDate) + ''')'
		If @EndDate Is Null Set @WHERE = @WHERE + ' ) '
	 End
	 
	If @EndDate Is Not Null 
	 Begin
		If @StartDate Is Null Set @WHERE = @WHERE + ' AND ( ( ' ELSE Set @WHERE = @WHERE + ' AND '
		Set @WHERE = @WHERE + CHAR(10) + ' ' + @DateType + ' < Convert(Datetime,''' + Convert(Varchar,@EndDate) + ''') ) '
	 End
	 
	 -- rrr 2/27/2013 - changed to reflect same logic as tsp_GblSearchInspectionCenter
	 --If @ShowOverdueItems Is Null Set @WHERE = @WHERE + ' ) '
	 If @ShowOverdueItems = 0 Set @WHERE = @WHERE + ' ) ' 
 End

 -- rrr 2/27/2013 - changed to reflect same logic as tsp_GblSearchInspectionCenter	
 --If @ShowOverdueItems Is Not Null
 If @ShowOverdueItems = 1 
	Set @WHERE = @WHERE + CHAR(10) + ' OR ( DATE_DUE < CAST(FLOOR(CAST(getdate() AS float)) AS DATETIME) AND DATE_RECEIVED is null)) '

	

Create Table #Results(ActivityNo Varchar(40),RecordID Varchar(40),Loc_RecordID Varchar(40),tGroup Varchar(40),ActivityTypeID int
,AttachmentTotal INT Default 0, ConditionsTotal INT Default 0, NotesTotal INT Default 0, RestrictionsTotal INT Default 0, FeesDue Money Default 0, RestrictionsSummary Varchar(5000) Default '',RestrictionsRecordIDUsed Varchar(40),RECORD_TYPE Varchar(60),RECORD_STATUS Varchar(60),SITE_ADDR Varchar(500),InspectorName Varchar(150), BondBalanceDue Money Default 0)
--declare @SQL varchar(max)
Set @SQL = '
insert into #results
(ActivityNo,RecordID,tGroup,ActivityTypeID,Loc_RecordID,Record_Type,Record_Status,SITE_ADDR)
Select distinct Top 500 ActivityID as ActivityNo,r.RecordID,[ActivityTypeName] as tGroup,a.ActivityTypeID,
b.Loc_RecordID,b.ActivityType,b.[Status],b.SITE_ADDR + IsNull('', '' + b.SITE_CITY,'''')
from [dbo].[Reviews]  r join [dbo].[ActivityType] a
on a.ActivityTypeID=r.ActivityTypeID
join  tvw_GblActivities b
on b.ActivityNo=r.ActivityID
--and b.ActivityType=a.ActivityTypeName
Where 1 = 1 
AND (Remarks Is Null OR Remarks NOT LIKE ''VOIDED (%'')
' + @WHERE
PRINT(@SQL)
EXEC(@SQL)


print 'bond amount'

 Update #Results Set
BondBalanceDue =  a.bond_Amount 
From #Results tr
JOIN (	
select ActivityID,ActivityTypeID,
isnull(sum([bonds].bond_amount), 0 ) bond_Amount
from [bonds] 
where  [bonds].paid = 0 and [bonds].PAID_AMOUNT = 0
group by  ActivityID,ActivityTypeID
 ) a
  ON tr.ActivityNo = a.ActivityID
and tr.ActivityTypeID=a.ActivityTypeID
 

Print 'Note Count'
Update #Results Set
NotesTotal = n.Total
From #Results tr
 JOIN (
	Select Total=Count(*),SubGroupRecordID From Prmry_Notes
	WHERE SubGroup = 'REVIEW'
	Group By SubGroupRecordID
 ) n
  ON tr.RecordID = n.SubGroupRecordID

  
Print 'Restriction Count'
Update #Results Set
RestrictionsTotal = isnull(r.Total,0)
From #Results tr
 JOIN (
	Select Total=Count(*),Loc_RecordID From Geo_Restrictions2
	Where Date_Cleared Is Null AND Restriction_Notes Not Like 'VOIDED (%'
	Group By Loc_RecordID
 ) r
  ON tr.Loc_RecordID = r.Loc_RecordID
  

IF OBJECT_ID(N'tempdb..#Restrictions') IS NOT NULL
             BEGIN
                    DROP TABLE #Restrictions
             END
SELECT      DISTINCT
         loc_recordid,

         RESTRICTION=STUFF(
               (SELECT      '<br />' + summary
               FROM      
			   (select loc_recordid, summary= isnull(x2.Restriction_Type,'') + ' ('
				+ Convert(Varchar,isnull(x2.Date_Added,''),101) + ') ' + SubString(x2.Restriction_Notes,1,50)
               FROM      Geo_RESTRICTIONS2 x2
			   where x2.Date_Cleared Is Null AND x2.Restriction_Notes Not Like 'VOIDED (%')
			   --and  loc_recordid='CONV:151116183544066713'
			    AS x2
               WHERE   
			    x.loc_recordid = x2.loc_recordid
			   
               FOR XML PATH('')), 1, 1, '')  
			  into #Restrictions 
FROM      Geo_RESTRICTIONS2 as x 
where loc_Recordid in(select loc_recordid from #results)
ORDER BY   loc_recordid 

Print 'update #results'
	Update #Results Set
	RestrictionsSummary = RESTRICTION	
	From #Results tr
	JOIN #Restrictions r
	 ON tr.Loc_RecordID = r.Loc_RecordID
	 
  
Print 'Attachments'
Update #Results Set
AttachmentTotal = isnull(a.total,0)
From #Results tr
 JOIN (
	Select Total=Count(*),ActivityID,ActivityTypeID From attachments
	Group By ActivityID,ActivityTypeID
 ) a
  ON tr.ActivityNo = a.ActivityID
  and tr.ActivityTypeID=a.ActivityTypeID
 

Print ' Fees Due'
Update #Results Set
FeesDue =  a.Total 
From #Results tr
JOIN (	
Select ActivityID ,ActivityTypeID,Total=Sum(amount)-sum(PAID_AMOUNT)  From fees	
		Group By ActivityID ,ActivityTypeID
		having  Sum(amount)-sum(PAID_AMOUNT) >0
 ) a
  ON tr.ActivityNo = a.ActivityID
and tr.ActivityTypeID=a.ActivityTypeID



  
Print 'Conditions Count'
Update #Results Set
ConditionsTotal =  c.Total 
From #Results tr
JOIN (
	Select Total=Count(*),activityid,ActivityTypeID From Conditions
	Group By activityid,ActivityTypeID
 ) c
  ON tr.ActivityNo = c.activityid
WHERE tr.ActivityTypeID =tr.ActivityTypeID



Select tr.AttachmentTotal,tr.ConditionsTotal,tr.NotesTotal,tr.RestrictionsTotal,tr.FeesDue,tr.RestrictionsSummary,tr.Loc_RecordID,tr.Record_Type,tr.Record_Status,tr.Site_Addr,tr.BondBalanceDue,vr.*
From tvw_GblReviews vr
JOIN #Results tr
  ON vr.RecordID = tr.RecordID





GO
