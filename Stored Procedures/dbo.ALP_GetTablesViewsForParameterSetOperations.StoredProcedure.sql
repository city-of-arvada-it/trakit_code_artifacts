USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[ALP_GetTablesViewsForParameterSetOperations]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[ALP_GetTablesViewsForParameterSetOperations]
GO
/****** Object:  StoredProcedure [dbo].[ALP_GetTablesViewsForParameterSetOperations]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================================
-- Author:		Erick Hampton
-- Create date: Dec 19, 2016
-- Description:	Gets the distict DisplayInfo.TableName values for all operations in a ParameterSet 
-- ==================================================================================================
CREATE PROCEDURE [dbo].[ALP_GetTablesViewsForParameterSetOperations]

	@ParameterSetID int = -1 

AS
BEGIN

	select distinct orc.TableName
	from [ALP_OperationRequiredColumns] orc
	inner join [ALP_ParameterSetOperations] pso on orc.OperationID = pso.OperationId
	where pso.ParameterSetId=@ParameterSetID

END

GO
