USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AutoLicenseFeeProrateSupport]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_AutoLicenseFeeProrateSupport]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AutoLicenseFeeProrateSupport]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  
-- =====================================================================
-- Revision History:
--	RB - 10/1/2015 Initial 

-- =====================================================================   	
CREATE Procedure [dbo].[tsp_T9_AutoLicenseFeeProrateSupport](
	 @FeeCode varchar(30),
     @FeeDate datetime,
	 @ATCIssuedDate datetime,
     @QTY float, 
	 @isLookupFee bit, 
	 @isGNRFee bit
)

As
BEGIN

	Declare @SQLStatement nvarchar(500)
	Declare @FeeAmount float
	Declare @FeeDescription varchar(60)
	Declare @FeeAccount varchar(30)
	Declare @FeeFormula varchar(2000)
	Declare @FeeAmountMonth float
	Declare @MonthDifference int
	Declare @MonthDiffFromAssessedDate int
	Declare @ProratedFee float
	Declare @AssessedFeeDate as datetime
	Declare @ErrorCode int
	Declare @ErrorDescription varchar(250)
	Declare @RecordID varchar(30)



	IF OBJECT_ID('tempdb..#ResultTable') IS NOT NULL DROP TABLE #ResultTable

	CREATE TABLE #ResultTable(
	ProratedFee float,
	AssessedFeeDate datetime,
	FeeDescription varchar(60),
	FeeAccount varchar(30),
	FeeFormula varchar(2000),
	MonthDiffFromAssessedDate int,	
	ErrorCode int,
	ErrorDescription varchar(250))

	Begin try
		
		if @isLookupFee = 1
			Begin
				Set @SQLStatement = 'select @RecordID = PF.Recordid, @FeeFormula = PF.Formula, @FeeAmount = PFT.base_amount, @FeeDescription = PF.Description, @FeeAccount = PF.Account from  Prmry_FeeSchedule PF ' + 
					 ' left join Prmry_FeeTables PFT on  PF.Formula like ' + 
					 CHAR(39) + '%' + CHAR(39) + '+ pft.tablename + ' + CHAR(39) + '%' + CHAR(39) + 
					 ' Where PF.groupname = ' + CHAR(39) + 'LICENSE2' + CHAR(39) + ' and PF.Code = ' + CHAR(39) + @FeeCode  + CHAR(39) + 
					 ' and ' + convert(varchar(10),@QTY) + ' between PFT.MIN_RANGE and PFT.MAX_RANGE and SCHEDULE_NAME = ' + CHAR(39) + 'DEFAULT' + CHAR(39)

					 Execute sp_executesql @sqlstatement, N' @FeeFormula varchar(2000) OUTPUT, @RecordID varchar(30) OUTPUT, @QTY int, @FeeCode varchar(30), @FeeAmount float OUTPUT, @FeeDescription varchar(60) OUTPUT, @FeeAccount varchar(30) OUTPUT', @FeeFormula = @FeeFormula OUTPUT, @RecordID = @RecordID OUTPUT, @FeeCode = @Feecode, @QTY = @QTY, @FeeAmount = @FeeAmount OUTPUT, @FeeDescription = @FeeDescription OUTPUT, @FeeAccount = @FeeAccount OUTPUT
			End
		else
			Begin
				Set @SQLStatement = 'select @RecordID = PF.Recordid, @FeeFormula = PF.Formula, @FeeDescription = PF.Description, @FeeAccount = PF.Account from  Prmry_FeeSchedule PF ' + 					
					 ' Where PF.groupname = ' + CHAR(39) + 'LICENSE2' + CHAR(39) + ' and PF.Code = ' + CHAR(39) + @FeeCode  + CHAR(39) + 
					 ' and SCHEDULE_NAME = ' + CHAR(39) + 'DEFAULT' + CHAR(39)

					 Execute sp_executesql @sqlstatement, N'  @FeeFormula varchar(2000) OUTPUT, @RecordID varchar(30) OUTPUT, @QTY int, @FeeCode varchar(30), @FeeAmount float OUTPUT, @FeeDescription varchar(60) OUTPUT, @FeeAccount varchar(30) OUTPUT', @FeeFormula = @FeeFormula OUTPUT, @RecordID = @RecordID OUTPUT, @FeeCode = @Feecode, @QTY = @QTY, @FeeAmount = @FeeAmount OUTPUT, @FeeDescription = @FeeDescription OUTPUT, @FeeAccount = @FeeAccount OUTPUT
				
					
			End

			print @SQLStatement


		if @RecordID is null or @RecordID = ''
			Begin
				Set @ProratedFee = 0
				Set @AssessedFeeDate = Null		
				Set @ErrorCode = -100
				Set @ErrorDescription = 'No Fee found in Fee Table. No Fees assessed. Please assess fees manually '			
			End
		Else if (@FeeAmount > 0 and ISDATE(@FeeDate) = 1 and @isLookupFee = 1) or (ISDATE(@FeeDate) = 1 and @isLookupFee = 0)
			Begin	

			print 'here'
				
				If @FeeAmount > 0 
					Begin
						set @FeeAmountMonth = @FeeAmount / 12
					End
				else
					Begin
						Set @FeeAmountMonth = 0
					End

				set @MonthDifference = datediff(MONTH, getdate(), @FeeDate) 

				if @isGNRFee = 0
					Begin
						
						if @MonthDifference >= 0 and @MonthDifference <= 5 
							Begin				
		
								Set @AssessedFeeDate = dateadd(yy,1, @FeeDate)

								set @MonthDiffFromAssessedDate = datediff(MONTH, getdate(), @AssessedFeeDate) 

								Set @ProratedFee = @MonthDiffFromAssessedDate * @FeeAmountMonth 
				
								Set @ErrorCode = 0
								Set @ErrorDescription = ''
							End
						else if @MonthDifference >= 6 and @MonthDifference <= 12
							Begin			
								Set @AssessedFeeDate = @FeeDate
								set @MonthDiffFromAssessedDate = datediff(MONTH, getdate(), @AssessedFeeDate) 
								Set @ProratedFee = @MonthDiffFromAssessedDate * @FeeAmountMonth 
								Set @ErrorCode = 0
								Set @ErrorDescription = ''
							End
						else if @MonthDifference > 12
							Begin
								Set @ProratedFee = 0
								Set @AssessedFeeDate = null
								Set @ErrorCode = -101
								Set @ErrorDescription = 'AEC Billing Date is more than 1 year in The future. No Fees Assessed. Please assess fees manually.'
							End
						else if @MonthDifference < 0 
							Begin
								Set @ProratedFee = 0
								Set @AssessedFeeDate = null
								Set @ErrorCode = -102
								Set @ErrorDescription = 'AEC Billing Date is in the past. No Fees Assessed. Please assess fees manually.'
							End
					End
					Else if @isGNRFee = 1 and ISDATE(@ATCIssuedDate) = 1
						Begin
							set @MonthDifference = datediff(MONTH, @ATCIssuedDate, @FeeDate)

							Set @AssessedFeeDate = @FeeDate

							set @MonthDiffFromAssessedDate = @MonthDifference % 12							
							
							Set @ProratedFee = @MonthDiffFromAssessedDate * @FeeAmountMonth 
				
							Set @ErrorCode = 0
							Set @ErrorDescription = ''	
						End
			End

		else
			Begin	
				Set @ProratedFee = 0
				Set @AssessedFeeDate = Null		
				Set @ErrorCode = -103
				Set @ErrorDescription = 'Fee Amount is zero in Fee Table or Fee Date empty. No Fees assessed. Please assess fees manually '			
			End
	
	end try
	begin catch
				Set @ProratedFee = 0
				Set @AssessedFeeDate = null
				Set @ErrorCode = error_number()
				Set @ErrorDescription = ERROR_MESSAGE() 
	end catch

insert into #ResultTable VALUES (@ProratedFee, @AssessedFeeDate, @FeeDescription, @FeeAccount, @FeeFormula, @MonthDiffFromAssessedDate,  @ErrorCode, @ErrorDescription)

Select ProratedFee, AssessedFeeDate, FeeDescription, FeeAccount, FeeFormula, MonthDiffFromAssessedDate, ErrorCode, ErrorDescription from #ResultTable


End
 


GO
