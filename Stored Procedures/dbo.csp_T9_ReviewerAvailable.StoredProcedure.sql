USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[csp_T9_ReviewerAvailable]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[csp_T9_ReviewerAvailable]
GO
/****** Object:  StoredProcedure [dbo].[csp_T9_ReviewerAvailable]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Micah Neveu>
-- Create date: <2/12/2015>
-- Description:	<Sets the ALTAVAILABLE to 0 if the provided user is not available during the review time>
-- =============================================
CREATE PROCEDURE [dbo].[csp_T9_ReviewerAvailable] 
	@STARTDATE DATETIME,
	@ENDDATE DATETIME, 
	@REVIEWER VARCHAR(20), 
	@AVAILABLE INT OUT
AS
BEGIN
	SET NOCOUNT ON;

	-- default to UNAVAILABLE
	SET @AVAILABLE = 0

create table #dayTable (
	theDay [Datetime],
	dayNumber [int], 
	theWorker [int]
)

-- we have start date and end date
DECLARE @reviewDays INT = DATEDIFF(day, @STARTDATE,@ENDDATE)
DECLARE @dayStep INT = 0


WHILE @dayStep < (@reviewDays + 1) BEGIN
	INSERT INTO #dayTable VALUES (DATEADD(day,@dayStep,@STARTDATE), @dayStep, 0) -- available by default
	SET @dayStep = @dayStep + 1
END

-- GET a list from prmry_workschedule based on the user (or no user) and other various values, ONLY 'out of the office' and
-- weekend/holidays are considered
DECLARE @WorkDate DATETIME
DECLARE DayTripperCursor CURSOR
	LOCAL SCROLL STATIC

	FOR
		SELECT WORKDATE FROM PRMRY_Workschedule WHERE WorkDate >= @STARTDATE AND WorkDate <= @ENDDATE AND ((USERID=@REVIEWER AND OutOfTheOffice =1) OR (USERID = '' AND AVAILABLE=0))
		OPEN DayTripperCursor
		FETCH NEXT FROM DayTripperCursor INTO @WorkDate
		WHILE @@FETCH_STATUS = 0
		BEGIN
			UPDATE #dayTable SET theWorker = 0 WHERE dayNumber = (DATEDIFF(day,@STARTDATE,@WorkDate))
	
			FETCH NEXT FROM DayTripperCursor INTO @WorkDate
		END
CLOSE DayTripperCursor
DEALLOCATE DayTripperCursor

-- If there are any 1's, then there is at least ONE day available where the poor sod can do EVERY SINGLE REVIEW that was assigned to him...
IF ((SELECT COUNT(*) FROM #dayTable WHERE theWorker <> 0) > 0)
	BEGIN
		SET @AVAILABLE = 1
	END

DROP TABLE #dayTable
	    
END


GO
