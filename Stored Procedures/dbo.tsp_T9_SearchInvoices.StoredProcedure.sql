USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_SearchInvoices]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_SearchInvoices]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_SearchInvoices]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  					  -- =============================================
-- Author:		Rance Richardson
-- Create date: 2013-11-26
-- Description:	Used for Invoice Search
-- Revision History:
--		2013-12-10 - Added AEC (RRR)
-- =============================================
/* 
tsp_T9_SearchInvoices @searchString='002', @Filter='invoice_no', @Top=10
tsp_T9_SearchInvoices @searchString='2411', @Filter='address'
tsp_T9_SearchInvoices @searchString='hall', @Filter='contact_name'
*/

CREATE PROCEDURE [dbo].[tsp_T9_SearchInvoices](	
	@searchString varchar(200),
	@Filter varchar(50),
	@Top int = Null

)
	
AS
BEGIN
	--SET NOCOUNT ON;
	DECLARE @SQL varchar(8000)
	DECLARE @WHERE varchar(2000)
	DECLARE @strTop varchar(50)
	DECLARE @UNION varchar(7)
	SET @WHERE = ' WHERE '
	SET @UNION = ' UNION '
	
	IF @Top Is Null 
	BEGIN
		SET @Top = 0
		SET @strTop = ' '
	END
	ELSE SET @strTop = ' TOP(' + CONVERT(varchar, @Top) + ') '
	IF LEFT(@searchString, 1) <> '%' AND RIGHT(@searchString, 1) <> '%'
		SET @searchString = '%' + @searchString + '%'
	SET @searchString = REPLACE(@searchString, '''', '''''')
	
	
	
	IF @Filter = 'invoice_no' SET @WHERE = @WHERE + 'M.InvoiceNumber LIKE ''' + @searchString + ''''
	IF @Filter = 'address' SET @WHERE = @WHERE + 'M.InvoiceAddress LIKE ''' + @searchString + ''''
	IF @Filter = 'contact_name' SET @WHERE = @WHERE + 'M.InvoiceContact LIKE ''' + @searchString + ''''
	IF @Filter = 'record_no' SET @WHERE = @WHERE + '''' + @searchString
	

--RB Check if table exists before creating it
   IF OBJECT_ID('tempdb..#results') IS NOT NULL  
	drop table #results

CREATE TABLE #results(
	invoice_no varchar(200),
	invoice_date varchar(10),
	invoice_total varchar (15),
	invoice_paid varchar(15),
	invoice_unpaid varchar(15),
	invoice_address varchar(600),
	invoice_contact varchar(300),
	record_no varchar(50)
)
	
	
SET @SQL = 'SELECT M.InvoiceNumber as invoice_no, 
		CONVERT(varchar(12), M.InvoiceDate, 1) as invoice_date,
		M.InvoiceTotal as invoice_total, 
		M.InvoicePaid as  invoice_paid, 
		M.InvoiceDue as invoice_unpaid,
		M.InvoiceAddress as invoice_address,
		M.InvoiceContact as invoice_contact,
		'''' as record_no
		INTO #results
	FROM Invoice_Main M 
		LEFT OUTER JOIN AEC_Fees AECF ON M.InvoiceNumber = AECF.INVOICE_NO' +
	@WHERE + @UNION +
	'SELECT M.InvoiceNumber as invoice_no, 
		CONVERT(varchar(12), M.InvoiceDate, 1) as invoice_date,
		M.InvoiceTotal as invoice_total, 
		M.InvoicePaid as  invoice_paid, 
		M.InvoiceDue as invoice_unpaid,
		M.InvoiceAddress as invoice_address,
		M.InvoiceContact as invoice_contact,
		'''' as record_no
	FROM Invoice_Main M 
		LEFT OUTER JOIN Case_Fees CASF ON M.InvoiceNumber = CASF.INVOICE_NO' +
	@WHERE + @UNION +
	'SELECT M.InvoiceNumber as invoice_no, 
		CONVERT(varchar(12), M.InvoiceDate, 1) as invoice_date,
		M.InvoiceTotal as invoice_total, 
		M.InvoicePaid as  invoice_paid, 
		M.InvoiceDue as invoice_unpaid,
		M.InvoiceAddress as invoice_address,
		M.InvoiceContact as invoice_contact,
		'''' as record_no 
		FROM Invoice_Main M 
		LEFT OUTER JOIN LICENSE2_FEES LICF ON M.InvoiceNumber = LICF.INVOICE_NO' +
	@WHERE + @UNION +
	'SELECT M.InvoiceNumber as invoice_no, 
		CONVERT(varchar(12), M.InvoiceDate, 1) as invoice_date,
		M.InvoiceTotal as invoice_total, 
		M.InvoicePaid as  invoice_paid, 
		M.InvoiceDue as invoice_unpaid,
		M.InvoiceAddress as invoice_address,
		M.InvoiceContact as invoice_contact,
		'''' as record_no 
		FROM Invoice_Main M 
		LEFT OUTER JOIN Permit_Fees PMTF ON M.InvoiceNumber = PMTF.INVOICE_NO' + 
	@WHERE + @UNION +
	'SELECT M.InvoiceNumber as invoice_no, 
		CONVERT(varchar(12), M.InvoiceDate, 1) as invoice_date,
		M.InvoiceTotal as invoice_total, 
		M.InvoicePaid as  invoice_paid, 
		M.InvoiceDue as invoice_unpaid,
		M.InvoiceAddress as invoice_address,
		M.InvoiceContact as invoice_contact,
		'''' as record_no 
		FROM Invoice_Main M 
		LEFT OUTER JOIN Project_Fees PRJF ON M.InvoiceNumber = PRJF.INVOICE_NO' + 
	 @WHERE + ' ' +
	 ' SELECT' + @strTop + 'invoice_no, invoice_date, invoice_total, invoice_paid, invoice_unpaid, invoice_address, invoice_contact, record_no FROM #results '
	 
PRINT(@SQL)
EXEC(@SQL)

DROP TABLE #results

 
END




GO
