USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_t9_InvoiceRelatedRecords]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_t9_InvoiceRelatedRecords]
GO
/****** Object:  StoredProcedure [dbo].[tsp_t9_InvoiceRelatedRecords]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- ==========================================================================================
-- Author:		Victor Sinfuente
-- Create date: 2016-1-29
-- Description:	Used for Invoice related activities
-- Revision History:
-- VS - 2/4/2016 - For invoice creation	
-- ==========================================================================================
CREATE PROCEDURE [dbo].[tsp_t9_InvoiceRelatedRecords](@st_lic_no VARCHAR(20), 
                                                     @nametype  VARCHAR(60)) 
AS 
  BEGIN 
  	  DECLARE @default_merchant VARCHAR(60)
	  SET @default_merchant = (SELECT ISNULL(value2, '') from Prmry_Preferences where item = 'ADVANCED_MERCHANT_ACCT' and value1 = 'YES')


	  -- RB Check if temp table exists
		IF OBJECT_ID('tempdb..#InvoiceRelated') IS NOT NULL  
			drop table #InvoiceRelated

      SELECT * INTO #InvoiceRelated FROM (
      SELECT DISTINCT pm.permit_no AS ACTIVITY_NO, 
                      '1' AS ACTIVITY_GROUP, 
					  pm.PermitType as ACTIVITY_TYPE,
					  pm.description AS ACTIVITY_NAME,
                      (SELECT isnull(Sum(amount) , 0)
                       FROM   permit_fees pf 
                       WHERE  pf.permit_no = pm.permit_no 
                              AND Isnull(pf.paid, '0') = '0' 
                              AND Isnull(pf.invoice_no, '') = '') AS ACTIVITY_BALANCEDUE, 
                      pm.site_addr                                AS ACTIVITY_ADDRESS,
					  PP.NAME AS CONTACT_NAME, 
					  ISNULL(pud.MerchantAccount, @default_merchant) as MERCHANT_ACCOUNT
      FROM   permit_main pm 
             INNER JOIN permit_people pp ON pp.permit_no = pm.permit_no AND pp.id = 'C:' + @st_lic_no AND pp.nametype = @nametype 
			 LEFT JOIN prmry_types pt ON pt.TypeName = pm.PermitType AND pt.GroupName = 'PERMITS'
			 LEFT JOIN prmry_userdepts pud ON pud.UserDept = pt.OwnerDept
      UNION ALL 
      SELECT DISTINCT pr.project_no AS ACTIVITY_NO, 
                      '2' AS ACTIVITY_GROUP, 
					  pr.projecttype as ACTIVITY_TYPE,
					  pr.PROJECT_NAME AS ACTIVITY_NAME,
                      (SELECT isnull(Sum(amount) , 0)
                       FROM   project_fees pf 
                       WHERE  pf.project_no = pr.project_no 
                              AND Isnull(pf.paid, '0') = '0' 
                              AND Isnull(pf.invoice_no, '') = '') AS ACTIVITY_BALANCEDUE, 
                      pr.site_addr AS ACTIVITY_ADDRESS,
					  PP.NAME AS CONTACT_NAME,
					  ISNULL(pud.MerchantAccount, @default_merchant) as MERCHANT_ACCOUNT
      FROM   project_main pr 
             INNER JOIN project_people pp ON pp.project_no = pr.project_no AND pp.id = 'C:' + @st_lic_no AND pp.nametype = @nametype 
			 LEFT JOIN prmry_types pt ON pt.TypeName = pr.PROJECTTYPE AND pt.GroupName = 'PROJECTS'
			 LEFT JOIN prmry_userdepts pud ON pud.UserDept = pt.OwnerDept
      UNION ALL 
      SELECT DISTINCT aec.st_lic_no AS ACTIVITY_NO, 
                      '6' AS ACTIVITY_GROUP, 
					  aec.AECTYPE as ACTIVITY_TYPE,
					  aec.COMPANY AS ACTIVITY_NAME,
                      (SELECT isnull(Sum(amount) , 0)
                       FROM   aec_fees af 
                       WHERE  af.st_lic_no = aec.st_lic_no 
                              AND Isnull(AF.paid, '0') = '0' 
                              AND Isnull(af.invoice_no, '') = '') AS ACTIVITY_BALANCEDUE, 
                      aec.address1 AS ACTIVITY_ADDRESS ,
					  PP.NAME AS CONTACT_NAME,
					  ISNULL(pud.MerchantAccount, @default_merchant) as MERCHANT_ACCOUNT
      FROM   aec_main aec 
             INNER JOIN aec_people pp ON pp.st_lic_no = AEC.st_lic_no AND pp.id = 'C:' + @st_lic_no AND pp.nametype = @nametype 
			 LEFT JOIN prmry_types pt ON pt.TypeName = AEC.AECTYPE AND pt.GroupName = 'AEC'
			 LEFT JOIN prmry_userdepts pud ON pud.UserDept = pt.OwnerDept
      UNION ALL 
      SELECT DISTINCT cm.case_no AS ACTIVITY_NO, 
                      '3' AS ACTIVITY_GROUP, 
					  cm.CaseType as ACTIVITY_TYPE,
					  cm.DESCRIPTION AS ACTIVITY_NAME,
                      (SELECT isnull(Sum(amount) , 0)
                       FROM   case_fees cf 
                       WHERE  cf.case_no = cm.case_no 
                              AND Isnull(cf.paid, '0') = '0' 
                              AND Isnull(cf.invoice_no, '') = '') AS 
                      ACTIVITY_BALANCEDUE, 
                      cm.site_addr AS ACTIVITY_ADDRESS ,
					  cp.NAME AS CONTACT_NAME ,
					  ISNULL(pud.MerchantAccount, @default_merchant) as MERCHANT_ACCOUNT
      FROM   case_main cm 
             INNER JOIN case_people cp ON cp.case_no = cm.case_no AND cp.id = 'C:' + @st_lic_no AND cp.nametype = @nametype 
			 LEFT JOIN prmry_types pt ON pt.TypeName = CM.CaseType AND pt.GroupName = 'VIOLATIONS'
			 LEFT JOIN prmry_userdepts pud ON pud.UserDept = pt.OwnerDept
      UNION ALL 
      SELECT DISTINCT lm.license_no AS ACTIVITY_NO, 
                      '17' AS ACTIVITY_GROUP, 
					  lm.LICENSE_TYPE as ACTIVITY_TYPE,
					  lm.COMPANY AS ACTIVITY_NAME,
                      (SELECT isnull(Sum(amount) , 0)
                       FROM   license2_fees lf 
                       WHERE  lf.license_no = lm.license_no 
                              AND Isnull(lf.paid, '0') = '0' 
                              AND Isnull(lf.invoice_no, '') = '') AS ACTIVITY_BALANCEDUE, 
                      lm.site_addr as ACTIVITY_ADDRESS , 
					  lp.NAME AS CONTACT_NAME,
					  ISNULL(pud.MerchantAccount, @default_merchant) as MERCHANT_ACCOUNT
      FROM   license2_main lm 
             INNER JOIN license2_people lp ON lp.license_no = lm.license_no AND lp.id = 'C:' + @st_lic_no AND lp.nametype = @nametype 
			 LEFT JOIN prmry_types pt ON pt.TypeName = lm.LICENSE_TYPE AND pt.GroupName = 'LICENSE2'
			 LEFT JOIN prmry_userdepts pud ON pud.UserDept = pt.OwnerDept
			 ) InvoiceRelated

	SELECT * FROM #InvoiceRelated WHERE ACTIVITY_BALANCEDUE > 0 

  END   
-- ==========================================================================================
/****** Object:  StoredProcedure [dbo].[tsp_T9_InvoiceDetails]    Script Date: 12/12/2013 12:27:47 ******/
SET ANSI_NULLS ON
GO
