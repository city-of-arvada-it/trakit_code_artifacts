USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ChartCaseDatesByActivityRecordID]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_ChartCaseDatesByActivityRecordID]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ChartCaseDatesByActivityRecordID]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  					  					  					  					  					  

CREATE Procedure [dbo].[tsp_ChartCaseDatesByActivityRecordID](
      @RecordID1 varchar(8000) = Null,
      @RecordID2 varchar(8000) = Null,
      @RecordID3 varchar(8000) = Null,
      @RecordID4 varchar(8000) = Null
)

As

Declare @RecID table (RecordID varchar(200)) 

insert @RecID
select r.recordid
from dbo.tfn_TblRecID(@recordid1,@recordid2,@recordid3,@recordid4) r

set nocount on

SELECT  CONVERT(nvarchar(30), CLOSED, 101) as [Date], 'CLOSED' as 'DateType', COUNT(CLOSED) AS 'cnt' FROM CASE_MAIN where RECORDID in (select RECORDID from @recID)  GROUP BY CLOSED
Union select CONVERT(nvarchar(30), FOLLOWUP, 101) as [Date], 'FOLLOWUP' as 'DateType', COUNT(FOLLOWUP) AS 'cnt' FROM CASE_MAIN where RECORDID in (select RECORDID from @recID) GROUP BY FOLLOWUP
Union select CONVERT(nvarchar(30), LASTACTION, 101) as [Date], 'LASTACTION' as 'DateType', COUNT(LASTACTION) AS 'cnt' FROM CASE_MAIN where RECORDID in (select RECORDID from @recID) GROUP BY LASTACTION
Union select CONVERT(nvarchar(30), OTHER_DATE1, 101) as [Date], 'OTHER_DATE1' as 'DateType', COUNT(OTHER_DATE1) AS 'cnt' FROM CASE_MAIN where RECORDID in (select RECORDID from @recID) GROUP BY OTHER_DATE1
Union select CONVERT(nvarchar(30), STARTED, 101) as [Date], 'STARTED' as 'DateType', COUNT(STARTED) AS 'cnt' FROM CASE_MAIN where RECORDID in (select RECORDID from @recID) GROUP BY STARTED
order by [Date], DateType















GO
