USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[lp_focus_Tot_PermitIssue_BLDG]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[lp_focus_Tot_PermitIssue_BLDG]
GO
/****** Object:  StoredProcedure [dbo].[lp_focus_Tot_PermitIssue_BLDG]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* 
--Ychavez 1/11/2014 - FOR FOCUS Measures report
-------------------------------------------------------------------------------------------
--Total permits issued for building
--Permit types not to include: ENG RCPT, FLOODPL DEV, FOOD TRUCK VENDING, HISTORICAL, PLN RCPT, BLD 
exec lp_focus_Tot_PermitIssue_BLDG @APPLIED_FROM = '02/01/2014' , @APPLIED_TO = '03/01/2014'
-------------------------------------------------------------------------------------------
*/
CREATE procedure [dbo].[lp_focus_Tot_PermitIssue_BLDG]
		@APPLIED_FROM datetime = null, --issued
		@APPLIED_TO datetime = NULL ---issued
as
begin


SELECT  DISTINCT COUNT(permit_no) AS num_of_permits 
FROM dbo.Permit_Main
WHERE ISSUED BETWEEN @APPLIED_FROM AND @APPLIED_TO
AND PermitType NOT IN ('ENG RCPT', 'FLOODPL DEV', 'FOOD TRUCK VENDING', 'HISTORICAL', 'PLN RCPT', 'BLD',
'RCPT', 'PROPTY MAINT', 'RIGHT OF WAY', 'ROWYARD', 'SITE DEV', 'SPEC EVENT', 'TEMP RECOVABLE', 'TRANS MERCH')

end
GO
