USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ExportAllFeeds]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_ExportAllFeeds]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ExportAllFeeds]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  
					  
					  					  					  /*
tsp_ExportAllFeeds @DBName='[(2008R2)_Arvada_Merge]', @FilePath='\\crw-pmweb\iTrakitExports_Arvada_Merge\', @TempFilePath='C:\CRW\iTrakitExportsTemp\'

tsp_ExportAllFeeds @DBName='DEV', @FilePath='C:\CRW\iTrakitExports\'

*/
-- =====================================================================
-- Revision History:
--	RR - 10/31/13
--	mdm - 11/5/13 - added revision history	
--  mdm - 06/27/14 - Modified to point to tsp_ITR%
-- =====================================================================     

CREATE Procedure [dbo].[tsp_ExportAllFeeds](
@DBName Varchar(50),
@FilePath Varchar(1000),
@TempFilePath Varchar(1000) = Null,
@ClearDirectoriesBefore INT = 1,
@ClearDirectoriesAfter INT = 1,
@ToggleXP_CMDShell INT = 1
)

As

SET NOCOUNT ON

If @ClearDirectoriesBefore Is Null Set @ClearDirectoriesBefore = 1
If @ClearDirectoriesAfter Is Null Set @ClearDirectoriesAfter = 1
If @ToggleXP_CMDShell Is Null Set @ToggleXP_CMDShell = 1


Declare @SQL Varchar(8000)
Declare @WriteFilePath Varchar(1000)

Set @WriteFilePath = @FilePath
If @TempFilePath Is Not Null 
	Set @WriteFilePath = @TempFilePath



--RB Check if table exists before creating it
   IF OBJECT_ID('tempdb..#Directory') IS NOT NULL  
	drop table #Directory

Create Table #Directory(DirText Varchar(1000))
Declare @FileName Varchar(500)
Declare @FileCount INT


If @ClearDirectoriesBefore = 1
 Begin
	If @ToggleXP_CMDShell = 1
	 Begin
		 --Enable xp_cmdshell
		Set @SQL = '
		EXEC sp_configure ''show advanced options'', 1
		RECONFIGURE
		EXEC sp_configure ''xp_cmdshell'', 1
		RECONFIGURE
		'
		Print(@SQL)
		Exec(@SQL)
	 End

	--Clean out destination directory
	Set @SQL = 'del /Q "' + @FilePath + '*.*"'
	--Set @SQL = 'dir "' + @FilePath + '"'
	Print(@SQL)
	exec master..xp_cmdshell @sql

	--Clean out temp directory if used
	If @TempFilePath Is Not Null 
	 Begin
		Set @SQL = 'del /Q "' + @TempFilePath + '*.*"'
		Print(@SQL)
		exec master..xp_cmdshell @sql
	 End
	 
	If @ToggleXP_CMDShell = 1
	 Begin
		 -- Disable xp_cmdshell
		Set @SQL = '
		EXEC sp_configure ''xp_cmdshell'', 0
		RECONFIGURE
		EXEC sp_configure ''show advanced options'', 0
		RECONFIGURE
		'
		Print(@SQL)
		Exec(@SQL)
	 End
 End

--RETURN

Set @FileName = 'ViolationsUDFExport'
Set @SQL = 'tsp_ITR_ExportViolationsUDF @DBName=''' + @DBName + ''', @FilePath=''' + @WriteFilePath + ''', @FileName=''' + @FileName + ''', @ToggleXP_CMDShell=' + Convert(Varchar,@ToggleXP_CMDShell)
Print(@FileName)
Print(@SQL)
Exec(@SQL)


----Check if file was created and if not, attempt to export again
--Set @SQL = 'dir "' + @WriteFilePath + '"'
Print(@SQL)
--Insert Into #Directory
--EXEC master..xp_cmdshell @sql

--Set @FileName = 'ViolationsUDFExport'
--Delete #Directory Where DirText Is Null OR DirText NOT LIKE '%' + @FileName + '%'

--Set @FileCount = 0
--Select @FileCount = COUNT(*) From #Directory
----Select * From #Directory
--If @FileCount < 1 PRINT (@FileName + '.csv file NOT created successfully')

--RETURN

Set @FileName = 'ViolationsInfoExport'
Set @SQL = 'tsp_ExportViolationsInfo @DBName=''' + @DBName + ''', @FilePath=''' + @WriteFilePath + ''', @FileName=''' + @FileName + ''', @ToggleXP_CMDShell=' + Convert(Varchar,@ToggleXP_CMDShell)
Print(@FileName)
Print(@SQL)
Exec(@SQL)

Set @FileName = 'ViolationsControlExport'
Set @SQL = 'tsp_ITR_ExportViolationsControl @DBName=''' + @DBName + ''', @FilePath=''' + @WriteFilePath + ''', @FileName=''' + @FileName + ''', @ToggleXP_CMDShell=' + Convert(Varchar,@ToggleXP_CMDShell)
Print(@FileName)
Print(@SQL)
Exec(@SQL)

Set @FileName = 'StandardTextsExport'
Set @SQL = 'tsp_ITR_ExportStandardTexts @DBName=''' + @DBName + ''', @FilePath=''' + @WriteFilePath + ''', @FileName=''' + @FileName + ''', @ToggleXP_CMDShell=' + Convert(Varchar,@ToggleXP_CMDShell)
Print(@FileName)
Print(@SQL)
Exec(@SQL)

Set @FileName = 'StandardNotesExport'
Set @SQL = 'tsp_ITR_ExportStandardNotes @DBName=''' + @DBName + ''', @FilePath=''' + @WriteFilePath + ''', @FileName=''' + @FileName + ''', @ToggleXP_CMDShell=' + Convert(Varchar,@ToggleXP_CMDShell)
Print(@FileName)
Print(@SQL)
Exec(@SQL)

Set @FileName = 'iTrakitDocsExport'
Set @SQL = 'tsp_ITR_ExportITrakitDocs @DBName=''' + @DBName + ''', @FilePath=''' + @WriteFilePath + ''', @FileName=''' + @FileName + ''', @ToggleXP_CMDShell=' + Convert(Varchar,@ToggleXP_CMDShell)
Print(@FileName)
Print(@SQL)
Exec(@SQL)

Set @FileName = 'GeoFullExport'
Set @SQL = 'tsp_ITR_ExportGeoFull @DBName=''' + @DBName + ''', @FilePath=''' + @WriteFilePath + ''', @FileName=''' + @FileName + ''', @ToggleXP_CMDShell=' + Convert(Varchar,@ToggleXP_CMDShell)
Print(@FileName)
Print(@SQL)
Exec(@SQL)

Set @FileName = 'ConditionsInfoExport'
Set @SQL = 'tsp_ITR_ExportConditionsInfo @DBName=''' + @DBName + ''', @FilePath=''' + @WriteFilePath + ''', @FileName=''' + @FileName + ''', @ToggleXP_CMDShell=' + Convert(Varchar,@ToggleXP_CMDShell)
Print(@FileName)
Print(@SQL)
Exec(@SQL)

Set @FileName = 'CaseUDFExport'
Set @SQL = 'tsp_ITR_ExportCaseUDF @DBName=''' + @DBName + ''', @FilePath=''' + @WriteFilePath + ''', @FileName=''' + @FileName + ''', @ToggleXP_CMDShell=' + Convert(Varchar,@ToggleXP_CMDShell)
Print(@FileName)
Print(@SQL)
Exec(@SQL)

Set @FileName = 'ActivityTypesExport'
Set @SQL = 'tsp_ITR_ExportActivityTypes @DBName=''' + @DBName + ''', @FilePath=''' + @WriteFilePath + ''', @FileName=''' + @FileName + ''', @ToggleXP_CMDShell=' + Convert(Varchar,@ToggleXP_CMDShell)
Print(@FileName)
Print(@SQL)
Exec(@SQL)

Set @FileName = 'ActivitySubTypesExport'
Set @SQL = 'tsp_ITR_ExportActivitySubTypes @DBName=''' + @DBName + ''', @FilePath=''' + @WriteFilePath + ''', @FileName=''' + @FileName + ''', @ToggleXP_CMDShell=' + Convert(Varchar,@ToggleXP_CMDShell)
Print(@FileName)
Print(@SQL)
Exec(@SQL)

Set @FileName = 'ActivityStatusExport'
Set @SQL = 'tsp_ITR_ExportActivityStatuses @DBName=''' + @DBName + ''', @FilePath=''' + @WriteFilePath + ''', @FileName=''' + @FileName + ''', @ToggleXP_CMDShell=' + Convert(Varchar,@ToggleXP_CMDShell)
Print(@FileName)
Print(@SQL)
Exec(@SQL)

Set @FileName = 'ActionTypesExport'
Set @SQL = 'tsp_ITR_ExportActionTypes @DBName=''' + @DBName + ''', @FilePath=''' + @WriteFilePath + ''', @FileName=''' + @FileName + ''', @ToggleXP_CMDShell=' + Convert(Varchar,@ToggleXP_CMDShell)
Print(@FileName)
Print(@SQL)
Exec(@SQL)

Set @FileName = 'InspectionControlExport'
Set @SQL = 'tsp_ITR_ExportInspectionControl @DBName=''' + @DBName + ''', @FilePath=''' + @WriteFilePath + ''', @FileName=''' + @FileName + ''', @ToggleXP_CMDShell=' + Convert(Varchar,@ToggleXP_CMDShell)
Print(@FileName)
Print(@SQL)
Exec(@SQL)

Set @FileName = 'InspectionsUDFExport'
Set @SQL = 'tsp_ITR_ExportInspectionsUDF @DBName=''' + @DBName + ''', @FilePath=''' + @WriteFilePath + ''', @FileName=''' + @FileName + ''', @ToggleXP_CMDShell=' + Convert(Varchar,@ToggleXP_CMDShell)
Print(@FileName)
Print(@SQL)
Exec(@SQL)

Set @FileName = 'ContactTypesExport'
Set @SQL = 'tsp_ITR_ExportContactTypes @DBName=''' + @DBName + ''', @FilePath=''' + @WriteFilePath + ''', @FileName=''' + @FileName + ''', @ToggleXP_CMDShell=' + Convert(Varchar,@ToggleXP_CMDShell)
Print(@FileName)
Print(@SQL)
Exec(@SQL)

--Copy files from temp directory to destination directory if @TempFilePath is used
If @TempFilePath Is Not Null 
 Begin
	If @ToggleXP_CMDShell = 1
	 Begin
		--Enable xp_cmdshell
		Set @SQL = '
		EXEC sp_configure ''show advanced options'', 1
		RECONFIGURE
		EXEC sp_configure ''xp_cmdshell'', 1
		RECONFIGURE
		'
		Print(@SQL)
		Exec(@SQL)
	 End

	Set @SQL = 'copy /Y "' + @TempFilePath + '*.*" "' + @FilePath + '"'
	Print(@SQL)
	exec master..xp_cmdshell @sql
	
	If @ToggleXP_CMDShell = 1
	 Begin
		-- Disable xp_cmdshell
		Set @SQL = '
		EXEC sp_configure ''xp_cmdshell'', 0
		RECONFIGURE
		EXEC sp_configure ''show advanced options'', 0
		RECONFIGURE
		'
		Print(@SQL)
		Exec(@SQL)
	 End
 End


If @ClearDirectoriesAfter = 1
 Begin
	If @ToggleXP_CMDShell = 1
	 Begin
		 --Enable xp_cmdshell
		Set @SQL = '
		EXEC sp_configure ''show advanced options'', 1
		RECONFIGURE
		EXEC sp_configure ''xp_cmdshell'', 1
		RECONFIGURE
		'
		Print(@SQL)
		Exec(@SQL)
	 End

	--Clean out destination directory
	Set @SQL = 'del /Q "' + @FilePath + '*.*"'
	--Set @SQL = 'dir "' + @FilePath + '"'
	Print(@SQL)
	exec master..xp_cmdshell @sql

	--Clean out temp directory if used
	If @TempFilePath Is Not Null 
	 Begin
		Set @SQL = 'del /Q "' + @TempFilePath + '*.*"'
		Print(@SQL)
		exec master..xp_cmdshell @sql
	 End
	 
	If @ToggleXP_CMDShell = 1
	 Begin
		-- Disable xp_cmdshell
		Set @SQL = '
		EXEC sp_configure ''xp_cmdshell'', 0
		RECONFIGURE
		EXEC sp_configure ''show advanced options'', 0
		RECONFIGURE
		'
		Print(@SQL)
		Exec(@SQL)
	 End
 End
 

Print('All Feeds Completed Successfully')













GO
