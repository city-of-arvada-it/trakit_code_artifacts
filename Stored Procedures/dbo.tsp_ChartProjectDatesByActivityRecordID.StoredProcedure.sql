USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ChartProjectDatesByActivityRecordID]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_ChartProjectDatesByActivityRecordID]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ChartProjectDatesByActivityRecordID]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  					  
					  					  					  

CREATE Procedure [dbo].[tsp_ChartProjectDatesByActivityRecordID](
      @RecordID1 varchar(8000) = Null,
      @RecordID2 varchar(8000) = Null,
      @RecordID3 varchar(8000) = Null,
      @RecordID4 varchar(8000) = Null
)

As

Declare @RecID table (RecordID varchar(2000)) 

insert @RecID
select r.recordid
from dbo.tfn_TblRecID(@recordid1,@recordid2,@recordid3,@recordid4) r

set nocount on

SELECT  CONVERT(nvarchar(30), APPLIED, 101) as [Date], 'APPLIED' as 'DateType', COUNT(APPLIED) AS 'cnt' FROM PROJECT_MAIN where RECORDID in (select RECORDID from @recID)  GROUP BY APPLIED
Union select CONVERT(nvarchar(30), APPROVED, 101) as [Date], 'APPROVED' as 'DateType', COUNT(APPROVED) AS 'cnt' FROM PROJECT_MAIN where RECORDID in (select RECORDID from @recID) GROUP BY APPROVED
Union select CONVERT(nvarchar(30), EXPIRED, 101) as [Date], 'EXPIRED' as 'DateType', COUNT(EXPIRED) AS 'cnt' FROM PROJECT_MAIN where RECORDID in (select RECORDID from @recID) GROUP BY EXPIRED
Union select CONVERT(nvarchar(30), CLOSED, 101) as [Date], 'CLOSED' as 'DateType', COUNT(CLOSED) AS 'cnt' FROM PROJECT_MAIN where RECORDID in (select RECORDID from @recID) GROUP BY CLOSED
Union select CONVERT(nvarchar(30), STATUS_DATE, 101) as [Date], 'STATUS_DATE' as 'DateType', COUNT(STATUS_DATE) AS 'cnt' FROM PROJECT_MAIN where RECORDID in (select RECORDID from @recID) GROUP BY STATUS_DATE
order by [Date], DateType
















GO
