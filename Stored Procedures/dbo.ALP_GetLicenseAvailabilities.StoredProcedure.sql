USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[ALP_GetLicenseAvailabilities]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[ALP_GetLicenseAvailabilities]
GO
/****** Object:  StoredProcedure [dbo].[ALP_GetLicenseAvailabilities]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



-- ==================================================================================================================================================================
-- Author:		Sravanti Adibhatla, Erick Hampton
-- Create date: Dec 28, 2016
-- Description:	Returns renewal availability info for a specified license number
-- Revisions:	Jul 11, 2017: created this new proc from a copy of [ALP_GetLicenseAvailability]
--							  differences:
--							  * removed the validations could result in ParameterSetOperationsConfigIssue that are done in [ALP_GetLicenseAvailabilities] 
--							  * removed the @IsRenewal parameter and the validationgs that are done in the new proc [ALP_LicenseNumbersIneligibleForRenewal]
--							  * replaced the license number input parameter with a list of licenses and parameter set ID values as input
--							  * added an input parameter to only return the license numbers that have issues
--							  * removed the code that sets [Availability] = '' and the code that checks if [Availability] = '' 
--							  * changed the two updates to a single update statement based on a table instead of a view
--							  * removed [ProcessingOrder] from the return results since it doesn't apply to this validation
-- ==================================================================================================================================================================
CREATE PROCEDURE [dbo].[ALP_GetLicenseAvailabilities]
		@LicenseList [ALP_LicenseNumberList] READONLY,
		@IgnoreThisBatchID int = -1,
		@ReturnUnavailableLicensesOnly bit = 1 -- don't return the license numbers that are available to process ("Ready")
AS
BEGIN
		
	-- create a table we can update with license numbers to check
	-- [Availability] = AlreadyRenewed, PendingProcessingElsewhere, CurrentlyProcessingElsewhere, NotEligibleForRenewal or Ready if fine to process
	if object_id('tempdb..#LicenseNumbersToValidateForAvailability') is not null drop table #LicenseNumbersToValidateForAvailability
	create table #LicenseNumbersToValidateForAvailability ([LICENSE_NO] varchar(30) NOT NULL, [ParameterSetID] int NOT NULL, [Availability] varchar(50))
	insert into #LicenseNumbersToValidateForAvailability ([LICENSE_NO], [ParameterSetID]) 
	select [LICENSE_NO], [ProcessingOrder] from @LicenseList -- -- we are repurposing the [ProcessingOrder] column in [ALP_LicenseNumberList] so we don't have to design another UDTT
	


	-- IMPORTANT!  We are expecting that [ParameterSetID] values are NEVER NULL in [ALP_LicenseNumberList].



	-- identify licenses from @LicenseNumbers that are in batches already where its operations are still pending
	update ln 
	set [Availability] = case when bpslnos.OperationStatusID = 0 then 'PendingProcessingElsewhere' 
							  when bpslnos.OperationStatusID = 1 then 'CurrentlyProcessingElsewhere'
							  end
	from #LicenseNumbersToValidateForAvailability ln 
		inner join [ALP_BatchParameterSetLicenseNumberOperationStatus] bpslnos on ln.LICENSE_NO = bpslnos.LicenseNumber
		inner join [ALP_Batches] b on bpslnos.BatchID=b.Id
	where [Availability] is null and b.StatusID in (1, 2, 3) and b.Id <> @IgnoreThisBatchID


	-- return the list of results
	if @ReturnUnavailableLicensesOnly = 1
	begin
		select [LICENSE_NO], [Availability] from #LicenseNumbersToValidateForAvailability where [Availability] is not null
	end
	else
	begin
		-- update [Availability] in #LicenseNumbersToValidateForAvailability to "Ready" if there are no issues and return the list
		update #LicenseNumbersToValidateForAvailability set [Availability] = 'Ready' where [Availability] is null
		select [LICENSE_NO], [Availability] from #LicenseNumbersToValidateForAvailability 

		-- TODO: test if this query would be faster than an update and select:  select [LICENSE_NO], isnull([Availability],'Ready') from @LicenseNumbers 
	end

	-- clean up so we don't overwhelm the tempdb storage
	if object_id('tempdb..#LicenseNumbersToValidateForAvailability') is not null drop table #LicenseNumbersToValidateForAvailability

END
GO
