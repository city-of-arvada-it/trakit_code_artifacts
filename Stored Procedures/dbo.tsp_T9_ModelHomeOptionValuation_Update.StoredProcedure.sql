USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ModelHomeOptionValuation_Update]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_ModelHomeOptionValuation_Update]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ModelHomeOptionValuation_Update]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  
CREATE PROCEDURE [dbo].[tsp_T9_ModelHomeOptionValuation_Update] 
              @List as UpdateOptionValuation readonly
                       
AS
BEGIN

	update Prmry_ModelHomeOptionValuations 
		set QUANTITY = ttbl.Quantity from @List ttbl 
			join  Prmry_ModelHomeOptionValuations on ttbl.RecID = Prmry_ModelHomeOptionValuations.RecordID;
				

END




GO
