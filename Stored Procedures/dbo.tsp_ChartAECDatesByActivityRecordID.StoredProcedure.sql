USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ChartAECDatesByActivityRecordID]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_ChartAECDatesByActivityRecordID]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ChartAECDatesByActivityRecordID]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  					  
					  					  					  

CREATE Procedure [dbo].[tsp_ChartAECDatesByActivityRecordID](
      @RecordID1 varchar(8000) = Null,
      @RecordID2 varchar(8000) = Null,
      @RecordID3 varchar(8000) = Null,
      @RecordID4 varchar(8000) = Null
)

As

Declare @RecID table (RecordID varchar(200)) 

insert @RecID
select r.recordid
from dbo.tfn_TblRecID(@recordid1,@recordid2,@recordid3,@recordid4) r

set nocount on

SELECT  CONVERT(nvarchar(30), APPLIED, 101) as [Date], 'APPLIED' as 'DateType', COUNT(APPLIED) AS 'cnt' FROM AEC_MAIN where RECORDID in (select RECORDID from @recID)  GROUP BY APPLIED
Union select CONVERT(nvarchar(30), BUS_LIC_1_EXPIRE, 101) as [Date], 'BUS_LIC_1_EXPIRE' as 'DateType', COUNT(BUS_LIC_1_EXPIRE) AS 'cnt' FROM AEC_MAIN where RECORDID in (select RECORDID from @recID) GROUP BY BUS_LIC_1_EXPIRE
Union select CONVERT(nvarchar(30), BUS_LIC_1_ISSUE, 101) as [Date], 'BUS_LIC_1_ISSUE' as 'DateType', COUNT(BUS_LIC_1_ISSUE) AS 'cnt' FROM AEC_MAIN where RECORDID in (select RECORDID from @recID) GROUP BY BUS_LIC_1_ISSUE
Union select CONVERT(nvarchar(30), BUS_LIC_2_EXPIRE, 101) as [Date], 'BUS_LIC_2_EXPIRE' as 'DateType', COUNT(BUS_LIC_2_EXPIRE) AS 'cnt' FROM AEC_MAIN where RECORDID in (select RECORDID from @recID) GROUP BY BUS_LIC_2_EXPIRE
Union select CONVERT(nvarchar(30), BUS_LIC_2_ISSUE, 101) as [Date], 'BUS_LIC_2_ISSUE' as 'DateType', COUNT(BUS_LIC_2_ISSUE) AS 'cnt' FROM AEC_MAIN where RECORDID in (select RECORDID from @recID) GROUP BY BUS_LIC_2_ISSUE
Union select CONVERT(nvarchar(30), PASSWORD_CHANGE_DATE, 101) as [Date], 'PASSWORD_CHANGE_DATE' as 'DateType', COUNT(PASSWORD_CHANGE_DATE) AS 'cnt' FROM AEC_MAIN where RECORDID in (select RECORDID from @recID) GROUP BY PASSWORD_CHANGE_DATE
Union select CONVERT(nvarchar(30), ST_LIC_EXPIRE, 101) as [Date], 'ST_LIC_EXPIRE' as 'DateType', COUNT(ST_LIC_EXPIRE) AS 'cnt' FROM AEC_MAIN where RECORDID in (select RECORDID from @recID) GROUP BY ST_LIC_EXPIRE
Union select CONVERT(nvarchar(30), ST_LIC_ISSUE, 101) as [Date], 'ST_LIC_ISSUE' as 'DateType', COUNT(ST_LIC_ISSUE) AS 'cnt' FROM AEC_MAIN where RECORDID in (select RECORDID from @recID) GROUP BY ST_LIC_ISSUE
order by [Date], DateType
















GO
