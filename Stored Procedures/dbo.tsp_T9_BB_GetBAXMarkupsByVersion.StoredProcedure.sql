USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_BB_GetBAXMarkupsByVersion]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_BB_GetBAXMarkupsByVersion]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_BB_GetBAXMarkupsByVersion]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  
-- =============================================
-- Author:		<Micah Neveu>
-- Create date: <4/7/2015>
-- Description:	<Simple read, returns the markups for the group and attachment id>
-- =============================================
CREATE PROCEDURE [dbo].[tsp_T9_BB_GetBAXMarkupsByVersion]
	@GroupName VARCHAR(30),
	@Pathname VARCHAR(200),
	@Version INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF (@GroupName = 'AEC')  BEGIN SELECT * FROM prmry_bax_markups WHERE attachmentrecordid = (SELECT TOP(1) RECORDID FROM AEC_ATTACHMENTS WHERE [Pathname] = @Pathname and [VERSION] = @Version) END
	IF (@GroupName = 'CASE')  BEGIN SELECT * FROM prmry_bax_markups WHERE attachmentrecordid = (SELECT TOP(1) RECORDID FROM CASE_ATTACHMENTS WHERE [Pathname] = @Pathname and [VERSION] = @Version) END
	IF (@GroupName = 'CRM')  BEGIN SELECT * FROM prmry_bax_markups WHERE attachmentrecordid = (SELECT TOP(1) RECORDID FROM CRM_ATTACHMENTS WHERE [Pathname] = @Pathname and [VERSION] = @Version) END
	IF (@GroupName = 'GEO')  BEGIN SELECT * FROM prmry_bax_markups WHERE attachmentrecordid = (SELECT TOP(1) RECORDID FROM GEO_ATTACHMENTS WHERE [Pathname] = @Pathname and [VERSION] = @Version) END
	IF (@GroupName = 'LICENSE2')  BEGIN SELECT * FROM prmry_bax_markups WHERE attachmentrecordid = (SELECT TOP(1) RECORDID FROM LICENSE2_ATTACHMENTS WHERE [Pathname] = @Pathname and [VERSION] = @Version) END
	IF (@GroupName = 'PAYMENT')  BEGIN SELECT * FROM prmry_bax_markups WHERE attachmentrecordid = (SELECT TOP(1) RECORDID FROM PAYMENT_ATTACHMENTS WHERE [Pathname] = @Pathname and [VERSION] = @Version) END
	IF (@GroupName = 'PERMIT')  BEGIN SELECT * FROM prmry_bax_markups WHERE attachmentrecordid = (SELECT TOP(1) RECORDID FROM PERMIT_ATTACHMENTS WHERE [Pathname] = @Pathname and [VERSION] = @Version) END
	IF (@GroupName = 'PROJECT')	BEGIN SELECT * FROM prmry_bax_markups WHERE attachmentrecordid = (SELECT TOP(1) RECORDID FROM PROJECT_ATTACHMENTS WHERE [Pathname] = @Pathname and [VERSION] = @Version) END	
END


--***********************************************************************************************************
--*** mdm - 07/21/15 - For Rafael



GO
