USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearch_InsertUpdateCurrent]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_AdvSearch_InsertUpdateCurrent]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearch_InsertUpdateCurrent]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROCEDURE [dbo].[tsp_T9_AdvSearch_InsertUpdateCurrent]

	@Action varchar(6) = null, -- insert or update
	@UserID varchar(50) = null,
	@RecordID int = -1,
	@TableName varchar(255) = null,
	@FieldName varchar(255) = null,
	@DisplayName varchar(255) = null,
	@Datatype varchar(50) = null,
	@Filter varchar(50) = null,
	@Value1 varchar(2000) = null,
	@Value2 varchar(255) = null,
	@Seq int = null,
	@DetailID int = -1 out -- set as identity when inserting

AS
BEGIN

if @UserID is not null
begin

declare @ItemID int 

	-- make sure the item exists for the user before inserting detail records
	select top 1 @ItemID = RecordID from Prmry_AdvSearch_CurrentItem where UserID=@UserID
	if @ItemID is not null
	begin
		-- insert
		if @Action = 'insert'
		begin
			insert into Prmry_AdvSearch_CurrentDetail ([ItemID],[TableName],[FieldName],[DisplayName],[Datatype],[Filter],[Value1],[Value2],[Seq])
			values (@ItemID,@TableName,@FieldName,@DisplayName,@Datatype,@Filter,@Value1,@Value2,@Seq)
			set @DetailID = @@IDENTITY
		end
		else
		begin
			-- update
			if @Action = 'update'
			begin
				update Prmry_AdvSearch_CurrentDetail 
				set	  [TableName] = @TableName
					, [FieldName] = @FieldName
					, [DisplayName] = @DisplayName
					, [Datatype] = @Datatype
					, [Filter] = @Filter
					, [Value1] = @Value1
					, [Value2] = @Value2
					, [Seq] = @Seq
				where RecordID=@RecordID and ItemID=@ItemID
			end
		end
	end
end

END
GO
