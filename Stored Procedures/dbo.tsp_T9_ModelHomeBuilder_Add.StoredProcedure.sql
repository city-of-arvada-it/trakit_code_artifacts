USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ModelHomeBuilder_Add]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_ModelHomeBuilder_Add]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ModelHomeBuilder_Add]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  
CREATE PROCEDURE [dbo].[tsp_T9_ModelHomeBuilder_Add] 
		@BuildersName varchar(255) = '',
		@UserID varchar(255) = '',
		@OrderId varchar(255) = '0'
AS
BEGIN

	Declare @SQL Varchar(8000)
	Declare @date varchar(255) = getdate()

		if @OrderId = '0'
			begin
				set  @OrderId = (select count(RecordID) +1  from  Prmry_ModelHomeBuilder)
			end


	if @BuildersName <> ''
		Begin

set @SQL = 'INSERT INTO Prmry_ModelHomeBuilder (Builder, ORDERID, LAST_MODIFIED_BY, LAST_MODIFIED)
					VALUES (''' + @BuildersName + ''',' + @OrderId + ',''' + @UserID + ''', ''' + @date + ''');'


			print(@sql)
			exec(@sql)

		end

END





GO
