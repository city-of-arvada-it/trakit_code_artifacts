USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_Security_CreateInvitation]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_Security_CreateInvitation]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_Security_CreateInvitation]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===========================================================================================================================
-- Author:		Erick Hampton
-- Create date: July 7, 2016
-- Description:	Insert invitation to the self-service reset password web page 
-- ===========================================================================================================================
CREATE PROCEDURE [dbo].[tsp_T9_Security_CreateInvitation]

  @UserID varchar(4)
, @Invitation varchar(128)
, @Inviter varchar(10)
, @InvitationReason varchar(20)

AS
BEGIN
	
SET NOCOUNT ON;
	
insert into [Prmry_Security_SelfService]  (UserID, Invitation, Inviter, InvitationReason, InvitationDateTime) values (@UserID, @Invitation, @Inviter, @InvitationReason, getdate())

END
GO
