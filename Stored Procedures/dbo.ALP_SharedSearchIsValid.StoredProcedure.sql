USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[ALP_SharedSearchIsValid]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[ALP_SharedSearchIsValid]
GO
/****** Object:  StoredProcedure [dbo].[ALP_SharedSearchIsValid]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- ========================================================================================
-- Author:		Erick Hampton
-- Create date: Jan 8, 2017
-- Description:	Validates that the supplied shared search ID can be used for ALP
-- ========================================================================================
CREATE PROCEDURE [dbo].[ALP_SharedSearchIsValid]

	@SearchID int = -1,
	@IsValid bit = 0 out

AS
BEGIN

	-- check if a license_no column and alias exists for the shared search
	if exists (select top 1 DisplayName from [Prmry_AdvSearch_SharedDetail] where ItemID=@SearchID and TableName='LICENSE2_MAIN' and FieldName='LICENSE_NO') 
	begin

		-- CHECK MORE STUFF AS NECESSARY

		set @isValid = 1
	end
	else
	begin
		set @isValid = 0
	end

	
END
GO
