USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[FindSchemaMatch]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[FindSchemaMatch]
GO
/****** Object:  StoredProcedure [dbo].[FindSchemaMatch]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[FindSchemaMatch] @SourceTableName sysname, @otherName sysname
AS
BEGIN
	declare @name sysname
	declare @columnList2 table (name sysname, columnName sysname)
	declare @results table (TableName sysname, ColumnName sysname NULL, Count1 int, TableList nvarchar(max))
	declare @whiteList table (TableName sysname)

	insert into @results select @SourceTableName, name, 1, @SourceTableName
	from syscolumns where object_name(id) = @SourceTableName

	insert into @whiteList 
	select 'Permit\_' UNION ALL SELECT 'Project\_' UNION ALL SELECT 'License2\_' UNION ALL SELECT 'Case\_' UNION ALL SELECT 'AEC\_' UNION ALL SELECT 'CRM\_' UNION ALL SELECT 'GEO\_'
	--SELECT 'GEO\_'

	declare c1 cursor fast_forward
	for select name from dbo.sysobjects as o
		inner join @whiteList as w
			on o.name like w.TableName + '%' ESCAPE '\'
		where name like '%'+@otherName ESCAPE '\' and type = 'U' 				

	open c1

	fetch next from c1 into @name

	while (@@FETCH_STATUS = 0)
	begin
		delete from @columnList2
		insert into @columnList2 select @name, c.name from dbo.syscolumns as c inner join dbo.sysobjects as o on o.id = c.id
		 where o.name = @name
		
		update @results set 
			count1 = Count1 + 1
			,TableList = TableList + ',' + @name
		from @results t1 
			inner join @columnList2 as t2 on t1.columnName = t2.columnName			
		
		fetch next from c1 into @name 
	end
	close c1
	deallocate c1

	select * from @Results order by count1 desc
END
GO
