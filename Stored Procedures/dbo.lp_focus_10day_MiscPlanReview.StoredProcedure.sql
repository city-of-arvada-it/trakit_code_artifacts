USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[lp_focus_10day_MiscPlanReview]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[lp_focus_10day_MiscPlanReview]
GO
/****** Object:  StoredProcedure [dbo].[lp_focus_10day_MiscPlanReview]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* 
--Ychavez 3/17/2014 - FOR FOCUS Measures report
-----------------------------------------------------------------
--90% of miscellaneous plan reviews completed within 10 business days. 
(Solar, demo
exec lp_focus_10day_MiscPlanReview @APPLIED_FROM = '01/01/2014' , @APPLIED_TO = '01/31/2014'
--------------------------------------------------------------------
*/
CREATE procedure [dbo].[lp_focus_10day_MiscPlanReview]
		@APPLIED_FROM datetime = null,
		@APPLIED_TO datetime = null
as
begin


-----------------------------------------------------------------
--90% of miscellaneous plan reviews completed within 10 business days. (Solar, demo
--this one may need more filters. such as subtypes on signs. We may or may not keep the issued is null as a filter
------------------------------------------------------------------
--select * from  #tmp_measure where issued is null and applied between '01/01/2013' and getdate()
--DROP TABLE #tmp_measure
Select 
case when DATEDIFF(day, applied, issued) < 11 then 'less than 10 business days' else
'out of measure'
end as measure
,0 as count_less_10_days, 0 as count_outofmeasure, 0 as total_permits, *
into #tmp_measure
 from permit_main
where permittype in ('SOLAR' , 'DEMOLITION', 'ELEVATOR', 'TELECOM', 'SIGN')

	--AND ISSUED IS NOT NULL 
		AND APPLIED IS NOT NULL
AND STATUS NOT IN ('WITHDRAWN', 'DENIED', 'VOID    ', 'VOID')
and applied between @APPLIED_FROM and @APPLIED_TO
------------------------------------------------------------------------------------------
--CALCULATE TOTAL PERMITS / PERMITS IN MEASURE - OUT OF MEASURE / LESS THAN 10 day MEASURE--
------------------------------------------------------------------------------------------

--drop table #tmp_count10
select COUNT(PERMIT_NO) as count_less_10_days
into #tmp_count10
FROM #tmp_measure 
WHERE applied between @APPLIED_FROM and @APPLIED_TO
--WHERE applied between '01/01/2014' and '02/01/2014'
and measure = 'less than 10 business days'


--drop table #tmp_outofmeasure
select COUNT(PERMIT_NO) as count_outofmeasure
into #tmp_outofmeasure
FROM #tmp_measure 
--WHERE applied between '01/01/2014' and '02/01/2014'
WHERE applied between  @APPLIED_FROM and @APPLIED_TO
and measure = 'out of measure'

update #tmp_measure
set count_less_10_days =  b.count_less_10_days
from #tmp_count10 b

--drop table #tmp_totpermits
update #tmp_measure
set count_outofmeasure =  b.count_outofmeasure
from #tmp_outofmeasure b

select distinct sum(count_less_10_days + count_outofmeasure) as total_permits--, count_less_10_days
into #tmp_totpermits
 from #tmp_outofmeasure,  #tmp_count10


update #tmp_measure
set total_permits =  b.total_permits
from #tmp_totpermits b

--need to combine all the results into one table
--USE THESE TWO NUMBERS TO CALCULATE IN REPORT--
--select (count_less_10_days  /total_permits) as total
--select * from #tmp_measure


select
SUM(cast(count_less_10_days as decimal)) / Sum(cast(total_permits as decimal)) * 100 as percent_com_reviews10days, total_permits, count_less_10_days
from #tmp_measure
group by total_permits, count_less_10_days


--select
--* --SUM(cast(total_permits as decimal)) / Sum(cast(count_less_10_days  as decimal)) as test
--from #tmp_measure
end
GO
