USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_UpdateBusinessNo]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_UpdateBusinessNo]
GO
/****** Object:  StoredProcedure [dbo].[tsp_UpdateBusinessNo]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  					  
					  					  					  


CREATE PROCEDURE [dbo].[tsp_UpdateBusinessNo] 
      
      @Recordid Varchar(50) = Null
      
AS

BEGIN
      
      SET NOCOUNT ON;

      DECLARE @count INT
      
      SELECT @count = COUNT(*) FROM  License_Business WHERE RECORDID = @Recordid 
 
      IF @count = 1 
            
            BEGIN 
            
            DECLARE @BusNo int 
            
            set @BusNo = dbo.tfn_MaxBusinessNo() + 1

			Update License_Business set BUSINESS_NO = @BusNo where RecordID = @Recordid
   
            END 
            
END


















GO
