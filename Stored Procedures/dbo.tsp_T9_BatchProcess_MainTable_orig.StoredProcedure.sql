USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_BatchProcess_MainTable_orig]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_BatchProcess_MainTable_orig]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_BatchProcess_MainTable_orig]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  



                                          

create PROCEDURE [dbo].[tsp_T9_BatchProcess_MainTable_orig] 
              @Status varchar(255) = '',
              @DateName varchar(255) = '',
              @DateValue varchar(255) = '',
              @Notes varchar(2000) = '',
                @Description varchar(2000) = '',
                @SubDiv varchar(255) = '',
				@UserID varchar(255) = '',
              @List as MainTableRecords readonly
              
AS
BEGIN

	Declare @SQL Varchar(8000)
	Declare @WhereClause varchar(255)
	declare @UpdateStmt varchar(50) 
	declare @TableName varchar(50) 

	IF object_id('#MainTableRec') is not null drop table #MainTableRec

	select * into #MainTableRec from @List
		             
	set @TableName = (Select distinct tblName from @list)

	--use function to get key field
	set @WhereClause =   ' where Permit_No in (Select ActivityNo from #MainTableRec)'
      
	set @SQL  = 'Update ' + @TableName + ' set '
          
    if len(ltrim(rtrim(@status))) > 0
            Begin
                    set @SQL = @SQL + 'Status = ''' + @Status + ''','
            End 

    if len(ltrim(rtrim(@DateName))) > 0
            Begin
                    set @SQL = @SQL + @DateName + ' = ''' + @DateValue + + ''','
            End 

    if len(ltrim(rtrim(@Notes))) > 0
            Begin
                    set @SQL = @SQL + 'Notes = ''' + @Notes + + ''','
            End

    if len(ltrim(rtrim(@Description))) > 0
            Begin
                    set @SQL = @SQL + 'Description = ''' + @Description + + ''','
            End

    if len(ltrim(rtrim(@SubDiv))) > 0
            Begin
                    set @SQL = @SQL + 'Site_SubDivision = ''' + @SubDiv + + ''','
            End 

	set @SQL = left(@SQL, (len(@sql)-1))
	set @SQL = @SQL + @WhereClause + ';'
		
	print(@SQL)
	exec(@SQL)

END







GO
