USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[ALP_GetUdfNamesForAllLicenses]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[ALP_GetUdfNamesForAllLicenses]
GO
/****** Object:  StoredProcedure [dbo].[ALP_GetUdfNamesForAllLicenses]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==============================================================================
-- Author:		Erick Hampton
-- Revised:		Feb 19, 2017
-- Description:	Gets all UDF names for licenses that have a type of float / int
-- ==============================================================================
CREATE PROCEDURE [dbo].[ALP_GetUdfNamesForAllLicenses]

AS
BEGIN

	select distinct sc.name [UDF_NAME]
	from sys.columns sc inner join sys.types st on sc.system_type_id = st.system_type_id
	where (object_id = object_id('LICENSE2_UDF','U') and sc.name <> 'LICENSE_NO' and sc.name <> 'RECORDID') 
		and st.name in ('bigint','int','smallint','tinyint','decimal','float','money','numeric','real','smallmoney')

	
END

GO
