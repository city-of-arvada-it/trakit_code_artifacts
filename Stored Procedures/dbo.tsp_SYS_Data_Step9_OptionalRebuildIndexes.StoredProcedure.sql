USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_SYS_Data_Step9_OptionalRebuildIndexes]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_SYS_Data_Step9_OptionalRebuildIndexes]
GO
/****** Object:  StoredProcedure [dbo].[tsp_SYS_Data_Step9_OptionalRebuildIndexes]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  					  
-- =====================================================================
-- Revision History:
--	mdm - 12/02/13 - added revision history	
--	mdm - 11/19/13 - added to source safe
-- =====================================================================   				  

CREATE PROCEDURE [dbo].[tsp_SYS_Data_Step9_OptionalRebuildIndexes] (
@OutputPath Varchar(1000) = Null
)

AS
BEGIN
  SET NOCOUNT ON

  --mdm - 09/20/13; sample @OutputPath: Step6_DS_Options_RebuildIndexesHavingFragmentationGreaterThan20.sql

  IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[zIndexListForSP]') AND type in (N'U'))
	BEGIN
		CREATE TABLE zIndexListForSP
		(
				TblName Varchar(255),
				IndexName Varchar(255),
				ColName Varchar(255), 
				Fragmentation float
			)
	End

	TRUNCATE TABLE  zIndexListForSP

	INSERT INTO zIndexListForSP(TblName,IndexName,ColName)
		SELECT  a.name AS TblName, b.name AS Indexname, c.name AS ColName
			FROM sys.objects a, sys.indexes b, sys.columns c, sys.index_columns d
			WHERE(b.object_id = a.object_id)
			AND b.object_id = c.object_id
			AND a.object_id = d.object_id
			AND b.index_id = d.index_id
			AND d.column_id = c.column_id
			AND (a.name not like 'sys%' and a.name not like 'queue_messages%' and a.name not like 'file%')
			ORDER BY a.name, c.name, b.name;

	--select * from #IndexList order by TblName,IndexName,ColName
	--select * from zIndexListForSP 	order by TblName, IndexName, ColName

	DECLARE @SQL varchar(2000)
	DECLARE @dbName varchar(200)
	Set @dbName = db_name()
	DECLARE @TableWithIndexes Varchar(255)
	DECLARE TableWithIndexes CURSOR FOR
	SELECT Distinct tblname FROM zIndexListForSP
	OPEN TableWithIndexes
	FETCH NEXT FROM TableWithIndexes INTO @TableWithIndexes

	WHILE @@FETCH_STATUS = 0
	 
		BEGIN
		
			--  update table with fragment percentages
			
			update zIndexListForSP set zIndexListForSP.Fragmentation = a.avg_fragmentation_in_percent
			FROM sys.dm_db_index_physical_stats (DB_ID(@dbName), OBJECT_ID(@TableWithIndexes), NULL, NULL, NULL) AS a
			JOIN sys.indexes AS b ON a.object_id = b.object_id AND a.index_id = b.index_id 
			where b.name = zIndexListForSP.IndexName; 
		 
	
			FETCH NEXT FROM TableWithIndexes INTO @TableWithIndexes

		END

		CLOSE TableWithIndexes
		DEALLOCATE TableWithIndexes

	IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[zIndexRebuildTemp]') AND type in (N'U'))
		BEGIN
			create table zIndexRebuildTemp
			(
			SqlToExec varchar(2000)
			)
		END

	TRUNCATE TABLE zIndexRebuildTemp

insert into zIndexRebuildTemp
Select 'ALTER INDEX ' + zIndexListForSP.IndexName + ' ON ' + zIndexListForSP.TblName + '    REBUILD;'
		from zIndexListForSP where Fragmentation > 20 order by TblName, IndexName, ColName

declare @FileName as varchar(1000)
If @OutputPath Is Null OR @OutputPath = ''
	Set @FileName = 'C:\crw\TEMP\DataStructure_RebuildINDEX.sql'
ELSE
	Set @FileName = @OutputPath
--set @FileName = 'c:\mayo\Step6_DS_RebuildIndexes.sql'
--select @FileName


declare @cmd varchar(2000)
set @cmd = 'bcp "select SqlToExec from '
set @cmd = @cmd + @dbname + '.dbo.zIndexRebuildTemp order by SqlToExec" queryout "'
set @cmd = @cmd + @FileName + '" -Utrakit -Ptrakit -c -S' + @@SERVERNAME

--select @cmd
exec xp_cmdshell @cmd

select SqlToExec from zIndexRebuildTemp order by SqlToExec


END












GO
