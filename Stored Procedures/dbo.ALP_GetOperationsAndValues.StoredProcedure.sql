USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[ALP_GetOperationsAndValues]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[ALP_GetOperationsAndValues]
GO
/****** Object:  StoredProcedure [dbo].[ALP_GetOperationsAndValues]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- ==============================================================================================================
-- Author:		Erick Hampton
-- Create date: 12/8/2016
-- Description:	Get the operations and values, optionally for a specific parameter set ID or operation ID
-- ==============================================================================================================
CREATE PROCEDURE [dbo].[ALP_GetOperationsAndValues]
	
	@ParameterSetID int = -1,
	@OperationID int = -1

AS
BEGIN
	SET NOCOUNT ON;

		select ParameterSetId, OperationId, OperationName, Command, OperationSequence, OperationValueNameId, ValueName, IsValueOptional, OperationValueId, Value 
		from [ALP_OperationValuesByParameterSet] 
		where 
			-- just the operations info for the specified parameter set id and specified operation id
			(@ParameterSetID > -1 and @OperationID > -1 and ParameterSetID=@ParameterSetID and OperationID=@OperationID)
			or
			-- just the operations info for the specified parameter set id
			(@ParameterSetID > -1 and @OperationID = -1 and ParameterSetID=@ParameterSetID)
			or
			-- just the operations info for the specified operation id
			(@ParameterSetID = -1 and @OperationID > -1 and OperationID=@OperationID)
			or
			-- all the operations info irrespective of the parameter set id or operation id
			(@ParameterSetID = -1 and @OperationID = -1)
		order by ParameterSetId, OperationSequence, OperationValueNameID


/*
	if @ParameterSetID > -1 and @OperationID > -1 
	begin
		-- just the operations info for the parameter set id and operation id
		select ParameterSetId, OperationId, OperationName, Command, OperationSequence, OperationValueNameId, ValueName, IsValueOptional, OperationValueId, Value 
		from [ALP_OperationValuesByParameterSet] 
		where ParameterSetID=@ParameterSetID and OperationID=@OperationID
		order by OperationValueNameID
	end
	else
	begin
		if @ParameterSetID > -1 and @OperationID = -1 
		begin
			-- everything for the specified parameter set id
			select ParameterSetId, OperationId, OperationName, Command, OperationSequence, OperationValueNameId, ValueName, IsValueOptional, OperationValueId, Value 
			from [ALP_OperationValuesByParameterSet] 
			where ParameterSetID=@ParameterSetID 
			order by OperationValueNameID
		end
		else
		begin
			if @ParameterSetID = -1 and @OperationID = -1 
			begin
				-- all operations info
				select ParameterSetId, OperationId, OperationName, Command, OperationSequence, OperationValueNameId, ValueName, IsValueOptional, OperationValueId, Value 
				from [ALP_OperationValuesByParameterSet] 
				order by OperationValueNameID
			end
		end
	end
*/

END



GO
