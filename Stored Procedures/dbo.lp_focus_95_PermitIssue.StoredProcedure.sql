USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[lp_focus_95_PermitIssue]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[lp_focus_95_PermitIssue]
GO
/****** Object:  StoredProcedure [dbo].[lp_focus_95_PermitIssue]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* 
--Ychavez 3/11/2014 - FOR FOCUS Measures report
-------------------------------------------------------------------------------------------
--95% of over-the-counter permits which do not require additional review will be issued within 15 minutes or less.--
--stole code from audit report--
exec lp_focus_95_PermitIssue @APPLIED_FROM = '02/01/2014' , @APPLIED_TO = '03/01/2014'
-------------------------------------------------------------------------------------------
*/
CREATE procedure [dbo].[lp_focus_95_PermitIssue]
		@APPLIED_FROM datetime = null,
		@APPLIED_TO datetime = null
as
begin


------------------------------------------------------------------------
--95% of over-the-counter permits which do not require additional review will be issued within 15 minutes or less.--
--stole code from audit report
--drop table #tmp_over_counter_permits
------------------------------------------------------------------------
--GET APPLIED DATE AND ISSUED DATE AND CALCULATE IF IT WAS WITHIN 15 MINUTES
select 
case when field_name = 'APPLIED' then transaction_datetime  end as applied_date,
case when field_name = 'ISSUED' then transaction_datetime  end as ISSUED_date, 
PERMIT_NO
INTO #tmp_over_counter_permits

  from Prmry_AuditTrail a
join (select permit_no 
from permit_main where permittype in('RESMISC', 'ROOF', 'FIRE PRO','COMMISC', 'COMMMISC', 'MISC') )
 p on p.permit_no = a.primary_key_value
      where TABLE_NAME = 'Permit_Main' 
and field_name in ('APPLIED', 'ISSUED', 'RECORDID')
--and PERMIT_NO = 'CMIS14-00001'
and TRANSACTION_DATETIME between @APPLIED_FROM and @APPLIED_TO

--and TRANSACTION_DATETIME between '02/01/2014' and '03/01/2014'

-----------------------------------------------------------------------------------------------
--The dates are on different rows for the permits. -- put in one table, one row per permit
--drop table #tmp_work  select * from #tmp_work
-----------------------------------------------------------------------------------------------

delete from #tmp_over_counter_permits
where applied_date is null and issued_date is null

--delete from #tmp_over_counter_permits
--where applied_date is null and issued_date is null

SELECT *
INTO #tmp_work
 FROM #tmp_over_counter_permits
 where ISSUED_date is null and applied_date is not null-- and issued_date is not null
 
 insert into #tmp_work
 select *
  FROM #tmp_over_counter_permits
 where applied_date is null and issued_date is not null-- and issued_date is not null

 
  update #tmp_work
set ISSUED_date = #tmp_over_counter_permits.ISSUED_date
FROM #tmp_over_counter_permits 
 where #tmp_work.ISSUED_date is null
 and #tmp_over_counter_permits.PERMIT_NO = #tmp_work.PERMIT_NO
 
update #tmp_work
set applied_date = #tmp_over_counter_permits.applied_date
FROM #tmp_over_counter_permits 
 where #tmp_work.applied_date is null
 and #tmp_over_counter_permits.PERMIT_NO = #tmp_work.PERMIT_NO
 

 -----------------------------------------------------------------------------------------------
 ---which permits do not have an applied date- should not be any
 --select * from #tmp_noapplied_date --drop table #tmp_noapplied_date
 -----------------------------------------------------------------------------------------------

 select * 
 into #tmp_noapplied_date
 from #tmp_over_counter_permits 
 where PERMIT_NO not in (select PERMIT_NO from #tmp_work)
 -----------------------------------------------------------------------------------------------
--GET MINUTES DIFFERENCE--
------------------------------------------------------------------------------------------------- 
 --select * from #tmp_work

select DATEDIFF(MINUTE, applied_date, issued_date) AS Minutes_Difference, applied_date, issued_date, PERMIT_NO
into #temp_calc
  from #tmp_work
 where applied_date between @APPLIED_FROM and @APPLIED_TO --since the dates are from the transactions, some applied dates may be in a different month for this permit. Filter by applied date.
--    where applied_date between '01/01/2014' and '01/31/2014' --since the dates are from the transactions, some applied dates may be in a different month for this permit. Filter by applied date.

group by permit_no, applied_date,issued_date

-------------------------------------------------------
--CALCULATE PERCENTAGE--SELECT * FROM #temp_calc
---------------------------------------------------
select CASE WHEN Minutes_Difference IS NULL OR Minutes_Difference > 15 THEN 'OUT OF MEASURE'
WHEN Minutes_Difference <=15 THEN 'IN MEASURE'
END AS MEASURE, PERMIT_NO
INTO #temp_calc_measure
FROM #temp_calc


select SUM(case when measure = 'IN MEASURE' THEN 1 ELSE 0 END) IN_MEASURE_COUNT
 ,SUM(case when measure = 'OUT OF MEASURE' THEN 1 ELSE 0 END) OUT_OF_MEASURE_COUNT
 ,SUM(CASE WHEN PERMIT_NO LIKE '%-%' THEN 1 END) TOTAL_PERMITS
 into #tmp_calc_measure_tot
  from #temp_calc_measure
  
 select SUM(cast(IN_MEASURE_COUNT as decimal)) / Sum(cast(total_permits as decimal)) * 100 as percent_permits_adtl_review, total_permits, IN_MEASURE_COUNT
from #tmp_calc_measure_tot
group by total_permits, IN_MEASURE_COUNT
end
GO
