USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_SSRS_GetFeeActivity]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_SSRS_GetFeeActivity]
GO
/****** Object:  StoredProcedure [dbo].[tsp_SSRS_GetFeeActivity]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  					  
					  
CREATE PROCEDURE [dbo].[tsp_SSRS_GetFeeActivity] 
	@FromDate DATETIME,
	@ToDate DATETIME,
	@Module VARCHAR(2000)
	
AS
BEGIN

--RB Check if table exists before creating it
   IF OBJECT_ID('tempdb..#table1') IS NOT NULL  
	drop table #table1

create table #table1
	(RECORD_NO varchar(20),
	RECORD_TYPE varchar(20),
	CODE varchar(30),
	DESCRIPTION varchar(100),
	ACCOUNT varchar(30),
	PAID_AMOUNT float,
	PAID_DATE date,
	RECEIPT_NO varchar(30),
	CHECK_NO varchar(30),
	PAY_METHOD varchar(30),
	COLLECTED_BY varchar(100),
	FEETYPE varchar(10),
	DEPOSITID varchar(30),
	SITE_ADDR varchar(100),
	SITE_APN varchar (100)) ;

create index #table1_ix1 on #table1 (RECORD_NO);

--GET FEES--
DECLARE @PERMITS varchar(50)
DECLARE @PROJECTS varchar(50)
DECLARE @CASES varchar(50)
DECLARE @LICENSE2 varchar(50)
DECLARE @AEC varchar(50)

SET @PERMITS = 'PermitTRAK'
SET @PROJECTS = 'ProjectTRAK'
SET @CASES = 'CodeTRAK'
SET @LICENSE2 = 'LicenseTRAK'
SET @AEC = 'AEC TRAK'

--BEGIN of TEST ONLY SCRIPT-------------------------------------------------------------------------------------------------------------------------------------------------
/*
DECLARE	@FromDate DATETIME
DECLARE	@ToDate DATETIME
DECLARE	@Account VARCHAR(2000)
DECLARE	@Module VARCHAR(2000)

SET @Module = ('PERMITS')
SET @Account = ('1015071130244115,1015074230144261')
SET @FromDate = '11/03/2011'
SET @ToDate = '11/03/2011'

update Permit_Fees set ACCOUNT = NULL where ACCOUNT = '' and DETAIL = 0 and PAID = 1 and PAID_DATE between @FromDate and @ToDate  ;
update Permit_SubFees set ACCOUNT = NULL where ACCOUNT = '' and PARENTID in (Select RECORDID from Permit_Fees where DETAIL = 1 and PAID_DATE between @FromDate and @ToDate) ;
*/
--END of TEST ONLY FEATURES--------------------------------------------------------------------------------------------------------------------------------------------------

--PERMITS
IF @PERMITS in (SELECT * FROM [dbo].[tfn_SSRS_String_To_Table](@Module,',',1))

BEGIN
	insert into #table1 (RECORD_NO, RECORD_TYPE, CODE, DESCRIPTION, ACCOUNT, PAID_AMOUNT, PAID_DATE, RECEIPT_NO, CHECK_NO, SITE_ADDR, SITE_APN, PAY_METHOD, COLLECTED_BY, FEETYPE, DEPOSITID)
	select Permit_Fees.PERMIT_NO, 'PermitTRAK', Permit_Fees.CODE, Permit_Fees.DESCRIPTION, Permit_Fees.ACCOUNT, Permit_Fees.PAID_AMOUNT, 
		Permit_Fees.PAID_DATE, Permit_Fees.RECEIPT_NO, Permit_Fees.CHECK_NO, Permit_Main.SITE_ADDR, Permit_Main.SITE_APN,
		Permit_Fees.PAY_METHOD, Permit_Fees.COLLECTED_BY, Permit_Fees.FEETYPE, Permit_Fees.DEPOSITID
	from Permit_Fees inner join Permit_Main on Permit_Main.PERMIT_NO = Permit_Fees.PERMIT_NO
	where DETAIL = 0 and PAID = 1 and PAID_AMOUNT > 0 and PAID_DATE between @FromDate and @ToDate 
	UNION ALL
	select Permit_SubFees.PERMIT_NO, 'PermitTRAK', ITEM as CODE, Permit_SubFees.DESCRIPTION, Permit_SubFees.ACCOUNT, Permit_SubFees.AMOUNT as PAID_AMOUNT, 
		Permit_Fees.PAID_DATE as PAID_DATE, Permit_Fees.RECEIPT_NO as RECEIPT_NO, Permit_Fees.CHECK_NO as CHECK_NO, Permit_Main.SITE_ADDR, Permit_Main.SITE_APN,
		Permit_Fees.PAY_METHOD, Permit_Fees.COLLECTED_BY, Permit_Fees.FEETYPE, Permit_Fees.DEPOSITID
	from Permit_SubFees inner join Permit_Fees on Permit_Fees.RECORDID = Permit_SubFees.PARENTID
	inner join Permit_Main on Permit_Main.PERMIT_NO = Permit_SubFees.PERMIT_NO
	where PARENTID in (Select RECORDID from Permit_Fees where DETAIL = 1 and Permit_Fees.PAID_AMOUNT > 0 and PAID_DATE between @FromDate and @ToDate)
	and Permit_SubFees.PAID = 1  and Permit_SubFees.AMOUNT > 0  ;
	
--Offset any Permit Deposit Reductions
insert into #table1
	( RECORD_NO, RECORD_TYPE, ACCOUNT, CODE, PAID_AMOUNT, PAID_DATE, RECEIPT_NO, CHECK_NO, PAY_METHOD, FEETYPE, DEPOSITID, COLLECTED_BY)
select
	  RECORD_NO, RECORD_TYPE,(select ACCOUNT from Permit_Fees where Permit_Fees.RECORDID = #table1 .DEPOSITID), CODE, (PAID_AMOUNT*-1), PAID_DATE, 'OFFSET', CHECK_NO, 'OFFSET', 'OFFSET', DEPOSITID, COLLECTED_BY
from #table1 
	where RECORD_TYPE = 'PermitTRAK'
	and DEPOSITID in 
		(select RECORDID from Permit_Fees 
			where PERMIT_NO = #table1.RECORD_NO 
			and FEETYPE = 'DEPOSIT')
	and (FEETYPE <> 'REFUND'
	or CODE <> 'REFUND') ;
		
END
--Select * from #Table1

--PROJECTS
IF @PROJECTS in (SELECT * FROM [dbo].[tfn_SSRS_String_To_Table](@Module,',',1))

BEGIN
	insert into #table1 (RECORD_NO, RECORD_TYPE, CODE, DESCRIPTION, ACCOUNT, PAID_AMOUNT, PAID_DATE, RECEIPT_NO, CHECK_NO, SITE_ADDR, SITE_APN, PAY_METHOD, COLLECTED_BY, FEETYPE, DEPOSITID)
	select Project_Fees.PROJECT_NO, 'ProjectTRAK', Project_Fees.CODE, Project_Fees.DESCRIPTION, Project_Fees.ACCOUNT, Project_Fees.PAID_AMOUNT, 
		Project_Fees.PAID_DATE, Project_Fees.RECEIPT_NO, Project_Fees.CHECK_NO, Project_Main.SITE_ADDR, Project_Main.SITE_APN,
		Project_Fees.PAY_METHOD, Project_Fees.COLLECTED_BY, Project_Fees.FEETYPE, Project_Fees.DEPOSITID
	from Project_Fees inner join Project_Main on Project_Main.PROJECT_NO = Project_Fees.PROJECT_NO
	where DETAIL = 0 and PAID = 1 and PAID_AMOUNT > 0 and PAID_DATE between @FromDate and @ToDate 
	UNION ALL
	select Project_SubFees.PROJECT_NO, 'ProjectTRAK', ITEM as CODE, Project_SubFees.DESCRIPTION, Project_SubFees.ACCOUNT, Project_SubFees.AMOUNT as PAID_AMOUNT, 
		Project_Fees.PAID_DATE as PAID_DATE, Project_Fees.RECEIPT_NO as RECEIPT_NO, Project_Fees.CHECK_NO as CHECK_NO, Project_Main.SITE_ADDR, Project_Main.SITE_APN,
		Project_Fees.PAY_METHOD, Project_Fees.COLLECTED_BY, Project_Fees.FEETYPE, Project_Fees.DEPOSITID
	from Project_SubFees inner join Project_Fees on Project_Fees.RECORDID = Project_SubFees.PARENTID
	inner join Project_Main on Project_Main.PROJECT_NO = Project_SubFees.PROJECT_NO
	where PARENTID in (Select RECORDID from Project_Fees where DETAIL = 1 and PAID_AMOUNT > 0 and PAID_DATE between @FromDate and @ToDate)
	and Project_SubFees.PAID = 1  ;
	
--Offset any Project Deposit Reductions
insert into #table1
	( RECORD_NO, RECORD_TYPE, ACCOUNT, CODE, PAID_AMOUNT, PAID_DATE, RECEIPT_NO, CHECK_NO, PAY_METHOD, FEETYPE, DEPOSITID, COLLECTED_BY)
select
	  RECORD_NO, RECORD_TYPE, (select ACCOUNT from Project_Fees where Project_Fees.RECORDID = #table1 .DEPOSITID), CODE, (PAID_AMOUNT*-1), PAID_DATE, 'OFFSET', CHECK_NO, 'OFFSET', 'OFFSET', DEPOSITID, COLLECTED_BY
from #table1 
	where RECORD_TYPE = 'ProjectTRAK'
	and DEPOSITID in 
		(select RECORDID from Project_Fees 
			where PROJECT_NO = #table1.RECORD_NO 
			and FEETYPE = 'DEPOSIT')
	and (FEETYPE <> 'REFUND'
	or CODE <> 'REFUND') ;

END

--CASES
IF @CASES in (SELECT * FROM [dbo].[tfn_SSRS_String_To_Table](@Module,',',1))

BEGIN
	insert into #table1 (RECORD_NO, RECORD_TYPE, CODE, DESCRIPTION, ACCOUNT, PAID_AMOUNT, PAID_DATE, RECEIPT_NO, CHECK_NO, SITE_ADDR, SITE_APN, PAY_METHOD, COLLECTED_BY, FEETYPE, DEPOSITID)
	select Case_Fees.CASE_NO, 'CodeTRAK', Case_Fees.CODE, Case_Fees.DESCRIPTION, Case_Fees.ACCOUNT, Case_Fees.PAID_AMOUNT, 
		Case_Fees.PAID_DATE, Case_Fees.RECEIPT_NO, Case_Fees.CHECK_NO, Case_Main.SITE_ADDR, Case_Main.SITE_APN,
		Case_Fees.PAY_METHOD, Case_Fees.COLLECTED_BY, Case_Fees.FEETYPE, Case_Fees.DEPOSITID
	from Case_Fees inner join Case_Main on Case_Main.CASE_NO = Case_Fees.CASE_NO
	where DETAIL = 0 and PAID = 1 and PAID_AMOUNT > 0 and PAID_DATE between @FromDate and @ToDate 
	UNION ALL
	select Case_SubFees.CASE_NO, 'CodeTRAK', ITEM as CODE, Case_SubFees.DESCRIPTION, Case_SubFees.ACCOUNT, Case_SubFees.AMOUNT as PAID_AMOUNT, 
		Case_Fees.PAID_DATE as PAID_DATE, Case_Fees.RECEIPT_NO as RECEIPT_NO, Case_Fees.CHECK_NO as CHECK_NO, Case_Main.SITE_ADDR, Case_Main.SITE_APN,
		Case_Fees.PAY_METHOD, Case_Fees.COLLECTED_BY, Case_Fees.FEETYPE, Case_Fees.DEPOSITID
	from Case_SubFees inner join Case_Fees on Case_Fees.RECORDID = Case_SubFees.PARENTID
	inner join Case_Main on Case_Main.CASE_NO = Case_SubFees.CASE_NO
	where PARENTID in (Select RECORDID from Case_Fees where DETAIL = 1 and PAID_AMOUNT > 0 and PAID_DATE between @FromDate and @ToDate)
	and Case_SubFees.PAID = 1 and Case_SubFees.AMOUNT > 0   ;
	
--Offset any Code Deposit Reductions	
insert into #table1
	( RECORD_NO, RECORD_TYPE, ACCOUNT, CODE, PAID_AMOUNT, PAID_DATE, RECEIPT_NO, CHECK_NO, PAY_METHOD, FEETYPE, DEPOSITID, COLLECTED_BY)
select
	  RECORD_NO, RECORD_TYPE, (select ACCOUNT from Case_Fees where Case_Fees.RECORDID = #table1 .DEPOSITID), CODE, (PAID_AMOUNT*-1), PAID_DATE, 'OFFSET', CHECK_NO, 'OFFSET', 'OFFSET', DEPOSITID, COLLECTED_BY
from #table1 
	where RECORD_TYPE = 'CodeTRAK'
	and DEPOSITID in 
		(select RECORDID from Case_Fees 
			where CASE_NO = #table1.RECORD_NO 
			and FEETYPE = 'DEPOSIT')
	and (FEETYPE <> 'REFUND'
	or CODE <> 'REFUND') ;
END

--LICENSE2
IF @LICENSE2 in (SELECT * FROM [dbo].[tfn_SSRS_String_To_Table](@Module,',',1))

BEGIN
	insert into #table1 (RECORD_NO, RECORD_TYPE, CODE, DESCRIPTION, ACCOUNT, PAID_AMOUNT, PAID_DATE, RECEIPT_NO, CHECK_NO, SITE_ADDR, SITE_APN, PAY_METHOD, COLLECTED_BY, FEETYPE, DEPOSITID)
	select License2_Fees.LICENSE_NO, 'LicenseTRAK', License2_Fees.CODE, License2_Fees.DESCRIPTION, License2_Fees.ACCOUNT, License2_Fees.PAID_AMOUNT, 
		License2_Fees.PAID_DATE, License2_Fees.RECEIPT_NO, License2_Fees.CHECK_NO, License2_Main.SITE_ADDR, License2_Main.SITE_APN,
		License2_Fees.PAY_METHOD, License2_Fees.COLLECTED_BY, License2_Fees.FEETYPE, License2_Fees.DEPOSITID
	from License2_Fees inner join License2_Main on License2_Main.LICENSE_NO = License2_Fees.LICENSE_NO
	where DETAIL = 0 and PAID = 1 and PAID_AMOUNT > 0 and PAID_DATE between @FromDate and @ToDate 
	UNION ALL
	select License2_SubFees.LICENSE_NO, 'LicenseTRAK', ITEM as CODE, License2_SubFees.DESCRIPTION, License2_SubFees.ACCOUNT, License2_SubFees.AMOUNT as PAID_AMOUNT, 
		License2_Fees.PAID_DATE as PAID_DATE, License2_Fees.RECEIPT_NO as RECEIPT_NO, License2_Fees.CHECK_NO as CHECK_NO, License2_Main.SITE_ADDR, License2_Main.SITE_APN,
		License2_Fees.PAY_METHOD, License2_Fees.COLLECTED_BY, License2_Fees.FEETYPE, License2_Fees.DEPOSITID
	from License2_SubFees inner join License2_Fees on License2_Fees.RECORDID = License2_SubFees.PARENTID
	inner join License2_Main on License2_Main.LICENSE_NO = License2_SubFees.LICENSE_NO
	where PARENTID in (Select RECORDID from License2_Fees where DETAIL = 1 and PAID_AMOUNT > 0 and PAID_DATE between @FromDate and @ToDate)
	and License2_SubFees.PAID = 1 and License2_SubFees.AMOUNT > 0  ;

--Offset any License Deposit Reductions	
insert into #table1
	( RECORD_NO, RECORD_TYPE, ACCOUNT, CODE, PAID_AMOUNT, PAID_DATE, RECEIPT_NO, CHECK_NO, PAY_METHOD, FEETYPE, DEPOSITID, COLLECTED_BY)
select
	  RECORD_NO, RECORD_TYPE, (select ACCOUNT from License2_Fees where License2_Fees.RECORDID = #table1 .DEPOSITID), CODE, (PAID_AMOUNT*-1), PAID_DATE, 'OFFSET', CHECK_NO, 'OFFSET', 'OFFSET', DEPOSITID, COLLECTED_BY
from #table1 
	where RECORD_TYPE = 'LicenseTRAK'
	and DEPOSITID in 
		(select RECORDID from License2_Fees 
			where LICENSE_NO = #table1.RECORD_NO 
			and FEETYPE = 'DEPOSIT')
	and (FEETYPE <> 'REFUND'
	or CODE <> 'REFUND') ;
END

--AEC
IF @AEC in (SELECT * FROM [dbo].[tfn_SSRS_String_To_Table](@Module,',',1))

BEGIN
	insert into #table1 (RECORD_NO, RECORD_TYPE, CODE, DESCRIPTION, ACCOUNT, PAID_AMOUNT, PAID_DATE, RECEIPT_NO, CHECK_NO, SITE_ADDR, PAY_METHOD, COLLECTED_BY, FEETYPE, DEPOSITID)
	select AEC_Fees.ST_LIC_NO, 'AEC TRAK', AEC_Fees.CODE, AEC_Fees.DESCRIPTION, AEC_Fees.ACCOUNT, AEC_Fees.PAID_AMOUNT, 
		AEC_Fees.PAID_DATE, AEC_Fees.RECEIPT_NO, AEC_Fees.CHECK_NO, AEC_Main.ADDRESS1,
		AEC_Fees.PAY_METHOD, AEC_Fees.COLLECTED_BY, AEC_Fees.FEETYPE, AEC_Fees.DEPOSITID
	from AEC_Fees inner join AEC_Main on AEC_Main.ST_LIC_NO = AEC_Fees.ST_LIC_NO
	where DETAIL = 0 and PAID = 1  and PAID_AMOUNT > 0 and PAID_DATE between @FromDate and @ToDate 
	UNION ALL
	select AEC_SubFees.ST_LIC_NO, 'AEC TRAK', ITEM as CODE, AEC_SubFees.DESCRIPTION, AEC_SubFees.ACCOUNT, AEC_SubFees.AMOUNT as PAID_AMOUNT, 
		AEC_Fees.PAID_DATE as PAID_DATE, AEC_Fees.RECEIPT_NO as RECEIPT_NO, AEC_Fees.CHECK_NO as CHECK_NO, AEC_Main.ADDRESS1,
		AEC_Fees.PAY_METHOD, AEC_Fees.COLLECTED_BY, AEC_Fees.FEETYPE, AEC_Fees.DEPOSITID
	from AEC_SubFees inner join AEC_Fees on AEC_Fees.RECORDID = AEC_SubFees.PARENTID
	inner join AEC_Main on AEC_Main.ST_LIC_NO = AEC_SubFees.ST_LIC_NO
	where PARENTID in (Select RECORDID from AEC_Fees where DETAIL = 1 and PAID_AMOUNT > 0 and PAID_DATE between @FromDate and @ToDate)
	and AEC_SubFees.PAID = 1 and AEC_SubFees.AMOUNT > 0 ;
	
--Offset any AEC Deposit Reductions	
insert into #table1
	( RECORD_NO, RECORD_TYPE, ACCOUNT, CODE, PAID_AMOUNT, PAID_DATE, RECEIPT_NO, CHECK_NO, PAY_METHOD, FEETYPE, DEPOSITID, COLLECTED_BY)
select
	  RECORD_NO, RECORD_TYPE, (select ACCOUNT from AEC_Fees where AEC_Fees.RECORDID = #table1 .DEPOSITID), CODE, (PAID_AMOUNT*-1), PAID_DATE, 'OFFSET', CHECK_NO, 'OFFSET', 'OFFSET', DEPOSITID, COLLECTED_BY
from #table1 
	where RECORD_TYPE = 'AEC TRAK'
	and DEPOSITID in 
		(select RECORDID from AEC_Fees 
			where ST_LIC_NO = #table1.RECORD_NO 
			and FEETYPE = 'DEPOSIT')
	and (FEETYPE <> 'REFUND'
	or CODE <> 'REFUND') ;

END

--Update Credits and Refunds
update #table1 set #table1.PAID_AMOUNT = #table1.PAID_AMOUNT * -1 where (FEETYPE = 'REFUND'or CODE = 'REFUND') ;
update #table1 set #table1.PAID_AMOUNT = #table1.PAID_AMOUNT * -1 where CODE = 'CREDIT' ;
update #table1 set #table1.ACCOUNT = '*NONE*' where (#table1.ACCOUNT is NULL OR #table1.ACCOUNT = '');

--THIS SELECT NEEDS TO BE HERE FOR SSRS TO GET DATA------------------------------------------
select * from #table1 ;
---------------------------------------------------------------------------------------------
END







GO
