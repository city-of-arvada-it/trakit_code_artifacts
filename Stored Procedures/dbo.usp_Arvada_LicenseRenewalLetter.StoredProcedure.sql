USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_LicenseRenewalLetter]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[usp_Arvada_LicenseRenewalLetter]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_LicenseRenewalLetter]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_Arvada_LicenseRenewalLetter]
	@EXPIRATION_STATUS varchar(max) 
as
/**********************************************************************************************************************
* Creator:      	 
* Date:         	 
* Title:			 
* Description:  	 
* Examples:    
			EXECUTE dbo.usp_Arvada_LicenseRenewalLetter @EXPIRATION_STATUS = '' 

SELECT ST_LIC_NO, COMPANY, ISNULL(ADDRESS1, '') + ' ' + ISNULL(ADDRESS2, '') AS ADDRESS, ISNULL(CITY, '') + ' ' + ISNULL(STATE, '') + ' ' + ISNULL(ZIP, '') AS CITYSTATEZIP, AECTYPE, AECSUBTYPE, STATUS, 
             ST_LIC_ISSUE, ST_LIC_EXPIRE,
                 (SELECT NAME
                 FROM    AEC_PEOPLE
                 WHERE (NAMETYPE = 'OWNER') AND (AEC_MAIN.ST_LIC_NO = ST_LIC_NO)) AS OWNER_NAME,
                 (SELECT ISNULL(ADDRESS1, '') + ' ' + ISNULL(ADDRESS2, '') AS Expr1
                 FROM    AEC_PEOPLE AS AEC_PEOPLE_2
                 WHERE (NAMETYPE = 'OWNER') AND (AEC_MAIN.ST_LIC_NO = ST_LIC_NO)) AS OWNER_ADDRESS,
                 (SELECT ISNULL(CITY, '') + ', ' + ISNULL(STATE, '') + ' ' + ISNULL(ZIP, '') AS Expr1
                 FROM    AEC_PEOPLE AS AEC_PEOPLE_1
                 WHERE (NAMETYPE = 'OWNER') AND (AEC_MAIN.ST_LIC_NO = ST_LIC_NO)) AS OWNER_CITYSTATEZIP, CASE WHEN st_lic_expire BETWEEN getdate() AND GETDATE() 
             - 30 THEN 'EXPIRED IN LAST 30 DAYS' WHEN st_lic_expire BETWEEN GETDATE() - 366 AND getdate() THEN 'EXPIRED IN LAST 365 DAYS' WHEN st_lic_expire BETWEEN getdate() - 90 AND GETDATE() 
             - 30 THEN 'EXPIRED IN LAST 90 DAYS' WHEN st_lic_expire BETWEEN getdate() - 1000 AND GETDATE() - 366 THEN 'EXPIRED OVER 365 DAYS' WHEN st_lic_expire BETWEEN getdate() AND GETDATE() 
             + 30 THEN 'EXPIRING IN 30 DAYS' WHEN st_lic_expire BETWEEN GETDATE() + 30 AND getdate() + 61 THEN 'EXPIRING IN 60 DAYS' WHEN st_lic_expire BETWEEN GETDATE() + 61 AND getdate() 
             + 91 THEN 'EXPIRING IN 90 DAYS' END AS EXPIRATION_STATUS
FROM   AEC_MAIN
WHERE (ST_LIC_EXPIRE BETWEEN GETDATE() - 1000 AND GETDATE() + 91)
ORDER BY ST_LIC_EXPIRE
					 
* Modifications:  	
  Developer Name		Date		Description
  ------------------	----------	----------------------------------------------------------- 
*
**********************************************************************************************************************/

set nocount on
 
--------------------------
--- Declare Variables   --
--------------------------
declare @currDate datetime = convert(Date,getdate())

 
--------------------------
--- Create Temp Tables  --
--------------------------
create table #AECSUBTYPE
(
	AECSUBTYPE varchar(60)
)

create table #EXPSTATUS
(
	EXPSTATUS varchar(160),
	startdate datetime,
	enddate datetime
)
 
	 
 --------------------------
 --- Body of Procedure   --
 --------------------------


INSERT INTO #EXPSTATUS
( 
	EXPSTATUS ,
	startdate,
	enddate
)
 select
	main.paramText,
	main.startdate,
	main.EndDate
from
(
	select
	'EXPIRED IN LAST 30 DAYS'  as ParamText,@currDate as EndDate, @currDate-30 as StartDate
	union all select
	'EXPIRED IN LAST 90 DAYS' as ParamText, @currDate-30, @currDate-90
	union all select
	'EXPIRED IN LAST 365 DAYS' as ParamText,  @currDate-90, @currDate-365
	union all select
	'EXPIRED OVER 365 DAYS' as ParamText,  @currDate-365, '01/01/1900'
	union all select
	'EXPIRING IN 30 DAYS' as ParamText,  @currDate+30 as EndDate, @currDate
	union all select
	'EXPIRING IN 60 DAYS' as ParamText,  @currDate+60, @currDate+30
	union all select
	'EXPIRING IN 90 DAYS' as ParamText,  @currDate+90, @currDate+60
) as main
inner join  [dbo].[tfn_SSRS_String_To_Table](@EXPIRATION_STATUS,'|',1) as b
	on b.Val = main.paramText



if @@ROWCOUNT = 0
begin
	INSERT INTO #EXPSTATUS
	( 
		EXPSTATUS ,
		startdate,
		enddate
	)
	 select
		main.paramText,
		main.startdate,
		main.EndDate
	from
	(
	select
	'EXPIRED IN LAST 30 DAYS'  as ParamText,@currDate as EndDate, @currDate-30 as StartDate
	union all select
	'EXPIRED IN LAST 90 DAYS' as ParamText, @currDate-30, @currDate-90
	union all select
	'EXPIRED IN LAST 365 DAYS' as ParamText,  @currDate-90, @currDate-365
	union all select
	'EXPIRED OVER 365 DAYS' as ParamText,  @currDate-365, '01/01/1900'
	union all select
	'EXPIRING IN 30 DAYS' as ParamText,  @currDate+30 as EndDate, @currDate
	union all select
	'EXPIRING IN 60 DAYS' as ParamText,  @currDate+60, @currDate+30
	union all select
	'EXPIRING IN 90 DAYS' as ParamText,  @currDate+90, @currDate+60
	) as main
end

--select * 
--from #EXPSTATUS
--order by startdate

SELECT 
	a.ST_LIC_NO, 
	a.COMPANY, 
	ISNULL(a.ADDRESS1, '') + ' ' + ISNULL(a.ADDRESS2, '') AS [ADDRESS], 
	ISNULL(a.CITY, '') + ' ' + ISNULL(a.[STATE], '') + ' ' + ISNULL(a.ZIP, '') AS CITYSTATEZIP, 
	a.AECTYPE, 
	a.AECSUBTYPE, 
	a.[STATUS], 
    a.ST_LIC_ISSUE, 
	a.ST_LIC_EXPIRE,
    (SELECT NAME
    FROM    AEC_PEOPLE
    WHERE (NAMETYPE = 'OWNER') AND (a.ST_LIC_NO = ST_LIC_NO)) AS OWNER_NAME,
	(SELECT ISNULL(ADDRESS1, '') + ' ' + ISNULL(ADDRESS2, '') AS Expr1
					FROM    AEC_PEOPLE AS AEC_PEOPLE_2
					WHERE (NAMETYPE = 'OWNER') AND (a.ST_LIC_NO = ST_LIC_NO)) AS OWNER_ADDRESS,
					(SELECT ISNULL(CITY, '') + ', ' + ISNULL(STATE, '') + ' ' + ISNULL(ZIP, '') AS Expr1
					FROM    AEC_PEOPLE AS AEC_PEOPLE_1
					WHERE (NAMETYPE = 'OWNER') AND (a.ST_LIC_NO = ST_LIC_NO)) AS OWNER_CITYSTATEZIP,
	e.EXPSTATUS
	

FROM  dbo.AEC_MAIN a
inner join #EXPSTATUS e
	on a.st_lic_expire >= e.startdate
	and a.st_lic_expire < dateadd(day,1,e.enddate)
where
	a.AECSUBTYPE = 'RIGHT OF WAY' 
ORDER BY 
	a.ST_LIC_EXPIRE
	 
GO
