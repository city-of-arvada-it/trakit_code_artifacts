USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_SysStartSQLAgentJob]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_SysStartSQLAgentJob]
GO
/****** Object:  StoredProcedure [dbo].[tsp_SysStartSQLAgentJob]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  					  
					  					  					  /*
tsp_SysStartSQLAgentJob 'tsp_ExportAllFeeds'

*/

CREATE PROCEDURE [dbo].[tsp_SysStartSQLAgentJob](
@JobName Varchar(300)
)

As

Declare @SQL Varchar(8000)

Set @SQL = 'msdb.dbo.sp_start_job N''' + @JobName + ''''
--PRINT(@SQL)
EXEC(@SQL)








GO
