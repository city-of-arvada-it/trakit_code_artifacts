USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_t9_CheckMarkList]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_t9_CheckMarkList]
GO
/****** Object:  StoredProcedure [dbo].[tsp_t9_CheckMarkList]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



  
CREATE Procedure [dbo].[tsp_t9_CheckMarkList](       
    @Group   varchar(20),  
    @SubGroup  varchar(20),  
    @RecordID  varchar(20),   
    @GroupType  varchar(20),  
    @GroupSubType varchar(20),   
    @Resultado  int output  
          
)  
  
AS  
BEGIN  
  
  Declare @Passed_Items int = 0,  
    @Failed_Items int = 0,  
    @NA_Items int = 0,  
    @None_Items int = 0,     
    @Total_Items int = 0,  
    @Total_No_Result int = 0,  
    @Result int = -1,  
    @PASSED int = 1,  
    @FAILED int = -1,   
    @NA int = 0,  
    @NONE int = 99,   
    @GREEN int = 1,  
    @YELLOW int = 2,   
    @RED int = 3,   
    @CLEAR int = 4  
    
   
 SET NOCOUNT ON  
     
  IF OBJECT_ID('tempdb..#temp_value_items') IS NOT NULL    
    Drop Table #temp_value_items  
  
  IF OBJECT_ID('tempdb..#temp_master_items') IS NOT NULL    
    Drop Table #temp_master_items  
  
 -- Create Temp tables  
  
 select checklists.checkListID into #temp_master_items from checkLists join checkListItems on checkListItems.checkListID = checkLists.checkListItemID Where GroupID = @Group And SubGroupID = @SubGroup  And GroupType in ('ALL', @GroupType) And SubGroupType 
in ('ALL', @GroupSubType)  
    
 Select itemvalue, count(*) as 'item_count' into #temp_value_items from checkListValues  where subRecordID = @RecordID  group by itemvalue  
  
  -- Count items not Checked in Master  
  
 set @Total_No_Result = (select count(*) from #temp_master_items TMI where TMI.checkListID not in (Select checkListID from checkListValues where subRecordID = @Recordid))  
  
 if isnumeric(@Total_No_Result) <> 1  
  Set @Total_No_Result = 0  
  
  Set @Passed_Items = (Select item_count from #temp_value_items where itemvalue = @PASSED)  
  
  Set @Failed_Items = (Select item_count from #temp_value_items where itemvalue = @FAILED)  
    
  Set @NA_Items = (Select item_count from #temp_value_items where itemvalue = @NA)  
  
  Set @None_Items = (Select item_count from #temp_value_items where itemvalue = @NONE)  
     
   if isnumeric(@Passed_Items) <> 1  
  Set @Passed_Items = 0   
  
   if isnumeric(@Failed_Items) <> 1  
  Set @Failed_Items = 0  
   
 if isnumeric(@NA_Items) <> 1  
  Set @NA_Items = 0  
   
 if isnumeric(@None_Items) <> 1  
  Set @None_Items = 0  
  
 Set @None_items = @None_Items + @Total_No_Result    
   
 Set @Total_Items = @Passed_Items + @Failed_Items + @NA_Items + @None_Items  
         
 if @Total_Items > 0 and  (@Total_Items = @Passed_Items) and @Result = -1  
  Set @Result = @GREEN  

  if @Total_Items > 0 and  ( @Total_Items =  @NA_Items) and @Result = -1  
  Set @Result = @CLEAR  
  
  if @Total_Items = @None_Items or (@Passed_Items = 0 and @Failed_Items = 0 and @None_items = 0 and @NA_Items = 0) and @Result = -1  
  Set @Result = @CLEAR  
  
 if @Failed_Items > 0 and @Result = -1  
  Set @Result = @RED  
  
 if (@Total_Items = @Passed_Items + @NA_Items + @None_Items and @Failed_Items = 0) and @Result = -1  
  Set @Result = @YELLOW  

  
        
  IF OBJECT_ID('tempdb..#temp_value_items') IS NOT NULL    
    Drop Table #temp_value_items  
  
  IF OBJECT_ID('tempdb..#temp_master_items') IS NOT NULL    
    Drop Table #temp_master_items  
   
  Set @Resultado = @result  
    
END  
  
GO
