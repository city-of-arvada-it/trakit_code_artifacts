USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GblSearchConditionsCenter]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_GblSearchConditionsCenter]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GblSearchConditionsCenter]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[tsp_GblSearchConditionsCenter](
@ContactID Varchar(1000) = Null,
@vStartDate Varchar(100) = Null,
@vEndDate Varchar(100) = Null,
@DateType Varchar(100) = Null,
@Department Varchar(100) = Null,
@Group Varchar(500) = Null,
@Type Varchar(500) = Null,
@ActivityNo Varchar(100) = Null,
@Status Varchar(100) = Null,
@ShowOverdueItems INT = Null

)


As

Declare @SQL Varchar(8000)
Declare @WHERE Varchar(5000)
Declare @StartDate Datetime 
Declare @EndDate Datetime

Set @WHERE = ''
Set @StartDate  = Null
Set @EndDate  = Null
If @vStartDate = '' Set @vStartDate = Null
If @vStartDate = '1/1/0001 12:00:00 AM' Set @vStartDate = Null
If @vEndDate = '' Set @vEndDate = Null
If @vEndDate = '1/1/0001 12:00:00 AM' Set @vEndDate = Null

If @vStartDate Is Null Set @vStartDate = Convert(Varchar,GetDate(),101)
If @vEndDate Is Null Set @vEndDate = Convert(Varchar,GetDate(),101)

Set @vStartDate = CONVERT(Varchar,CONVERT(Datetime,@vStartDate),101)
Set @vEndDate = CONVERT(Varchar,CONVERT(Datetime,@vEndDate),101)

Print(@vStartDate)
Print(@vEndDate)

Set @StartDate = CONVERT(Datetime,@vStartDate)
Set @EndDate = CONVERT(Datetime,DATEADD(dd,1,@vEndDate))


If @ContactID = '' Set @ContactID = Null
If @Department = '' Set @Department = Null
If @DateType = '' Set @DateType = Null
If @vStartDate = '' Set @vStartDate = Null
If @vEndDate = '' Set @vEndDate = Null
If @Group = '' Set @Group = Null
If @Type = '' Set @Type = Null


If @ContactID Is Not Null Set @WHERE = @WHERE + CHAR(10) + ' AND CONTACT IN (''' + REPLACE(@ContactID,',',''',''') + ''')'
If @Department Is Not Null Set @WHERE = @WHERE + CHAR(10) + ' AND DEPARTMENT = ''' + @Department + ''''
If @Status Is Not Null Set @WHERE = @WHERE + CHAR(10) + ' AND STATUS = ''' + @Status + ''''
If @Group Is Not Null Set @WHERE = @WHERE + CHAR(10) + ' AND tGroup IN (''' + REPLACE(@Group,',',''',''') + ''')'
If @Type Is Not Null Set @WHERE = @WHERE + CHAR(10) + ' AND Condition_Type IN (''' + REPLACE(@Type,',',''',''') + ''')'

If @vStartDate Is Not Null Set @StartDate = CONVERT(Datetime,@vStartDate)
If @vEndDate Is Not Null Set @EndDate = CONVERT(Datetime,DATEADD(dd,1,@vEndDate))
Print(@StartDate)
Print(@EndDate)
 
If @DateType Is Not Null
 Begin
	If @StartDate Is Not Null 
	 Begin
		Set @WHERE = @WHERE + ' AND ( '
		Set @WHERE = @WHERE + CHAR(10) + ' ( ' + @DateType + ' >= Convert(Datetime,''' + Convert(Varchar,@StartDate) + ''')'
		If @EndDate Is Null Set @WHERE = @WHERE + ' ) '
	 End
	 
	If @EndDate Is Not Null 
	 Begin
		If @StartDate Is Null Set @WHERE = @WHERE + ' AND ( ( ' ELSE Set @WHERE = @WHERE + ' AND '
		Set @WHERE = @WHERE + CHAR(10) + ' ' + @DateType + ' < Convert(Datetime,''' + Convert(Varchar,@EndDate) + ''') ) '
	 End
	 
	 --If @ShowOverdueItems Is Null Set @WHERE = @WHERE + ')'
	 If @ShowOverdueItems Is Null and @DateType = 'Date_Required'
	 Begin
	  Set @WHERE = @WHERE + ' AND (DATE_SATISFIED is null))'
     End

	 If @ShowOverdueItems Is Null and (@DateType = 'Date_Satisfied' or @DateType ='Date_Added')
	 Begin
	  Set @WHERE = @WHERE + ' )'
	End

 If @ShowOverdueItems Is Not Null 
 Begin
 if @DateType = 'Date_Satisfied'
	Set @WHERE = @WHERE + CHAR(10) + ' OR ( DATE_REQUIRED < CAST(FLOOR(CAST(getdate() AS float)) AS DATETIME) AND DATE_SATISFIED is not null) )'
	else
	Set @WHERE = @WHERE + CHAR(10) + ' OR ( DATE_REQUIRED < CAST(FLOOR(CAST(getdate() AS float)) AS DATETIME) AND DATE_SATISFIED is null) )'
 End
End

Create Table #Results(ActivityNo Varchar(40),ActivityTypeID int,RecordID Varchar(40),Loc_RecordID Varchar(40),tGroup Varchar(40),AttachmentTotal INT Default 0, NotesTotal INT Default 0, RestrictionsTotal INT Default 0, FeesDue Money Default 0, RestrictionsSummary Varchar(5000) Default '',RestrictionsRecordIDUsed Varchar(40),RECORD_TYPE Varchar(60),RECORD_STATUS Varchar(60),SITE_ADDR Varchar(500),InspectorName Varchar(150))

Set @SQL = '
Insert Into #Results(ActivityNo,ActivityTypeID,RecordID,tGroup,Loc_RecordID,RECORD_TYPE,RECORD_STATUS,SITE_ADDR,InspectorName)
Select Top 500 ActivityNo,r.ActivityTypeID,r.RecordID,tGroup,Loc_RecordID,ACTIVITYTYPE,b.status,SITE_ADDR,contact
From [dbo].[Conditions]  r join [dbo].[ActivityType] a
on a.ActivityTypeID=r.ActivityTypeID
join  tvw_GblActivities b
on b.ActivityNo=r.ActivityID
Where 
 (CONDITION_NOTES Is Null OR CONDITION_NOTES NOT LIKE ''VOIDED (%'')
' + @WHERE
PRINT(@SQL)
EXEC(@SQL)
 


Print 'Note Count'
Update #Results Set
NotesTotal = n.Total
From #Results tr
 JOIN (
	Select Total=Count(*),SubGroupRecordID From Prmry_Notes
	WHERE SubGroup = 'CONDITION'
	Group By SubGroupRecordID
 ) n
  ON tr.RecordID = n.SubGroupRecordID

  
Print 'Restriction Count'
Update #Results Set
RestrictionsTotal = isnull(r.Total,0)
From #Results tr
 JOIN (
	Select Total=Count(*),Loc_RecordID From Geo_Restrictions2
	Where Date_Cleared Is Null AND Restriction_Notes Not Like 'VOIDED (%'
	Group By Loc_RecordID
 ) r
  ON tr.Loc_RecordID = r.Loc_RecordID
  

IF OBJECT_ID(N'tempdb..#Restrictions') IS NOT NULL
             BEGIN
                    DROP TABLE #Restrictions
             END
SELECT      DISTINCT
         loc_recordid,

         RESTRICTION=STUFF(
               (SELECT      '<br />' + summary
               FROM      
			   (select loc_recordid, summary= isnull(x2.Restriction_Type,'') + ' ('
				+ Convert(Varchar,isnull(x2.Date_Added,''),101) + ') ' + SubString(x2.Restriction_Notes,1,50)
               FROM      Geo_RESTRICTIONS2 x2
			   where x2.Date_Cleared Is Null AND x2.Restriction_Notes Not Like 'VOIDED (%')
			   --and  loc_recordid='CONV:151116183544066713'
			    AS x2
               WHERE   
			    x.loc_recordid = x2.loc_recordid
			   
               FOR XML PATH('')), 1, 1, '')  
			  into #Restrictions 
FROM      Geo_RESTRICTIONS2 as x 
where loc_Recordid in(select loc_recordid from #results)
ORDER BY   loc_recordid 

Print 'update #results'
	Update #Results Set
	RestrictionsSummary = RESTRICTION	
	From #Results tr
	JOIN #Restrictions r
	 ON tr.Loc_RecordID = r.Loc_RecordID
	 
  
Print 'Attachments'
Update #Results Set
AttachmentTotal = isnull(a.total,0)
From #Results tr
 JOIN (
	Select Total=Count(*),ActivityID,ActivityTypeID From attachments
	Group By ActivityID,ActivityTypeID
 ) a
  ON tr.ActivityNo = a.ActivityID
  and tr.ActivityTypeID=a.ActivityTypeID
 

Print ' Fees Due'
Update #Results Set
FeesDue =  a.Total 
From #Results tr
JOIN (	
Select ActivityID ,ActivityTypeID,Total=Sum(amount)-sum(PAID_AMOUNT)  From fees	
		Group By ActivityID ,ActivityTypeID
		having  Sum(amount)-sum(PAID_AMOUNT) >0
 ) a
  ON tr.ActivityNo = a.ActivityID
and tr.ActivityTypeID=a.ActivityTypeID




Select tr.AttachmentTotal,tr.NotesTotal,tr.RestrictionsTotal,tr.FeesDue,tr.RestrictionsSummary,tr.Loc_RecordID,tr.Record_Type,tr.Record_Status,tr.Site_Addr,vc.*
From tvw_GblConditions vc
JOIN #Results tr
  ON vc.RecordID = tr.RecordID






GO
