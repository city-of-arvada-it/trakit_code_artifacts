USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ModelHomeBaseValuation_Add]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_ModelHomeBaseValuation_Add]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ModelHomeBaseValuation_Add]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  
Create PROCEDURE [dbo].[tsp_T9_ModelHomeBaseValuation_Add]               
			  @ValuationCode varchar(255) = '',
			  @ModelRecordID varchar(10),
			  @Quantity varchar(10),
              @UserID varchar(255) = '',
              @OrderId varchar(255) = '0'
AS
BEGIN

       Declare @SQL Varchar(8000)
       Declare @date varchar(255) = getdate()

              if @OrderId = '0'
                     begin
                          set  @OrderId = (select count(RecordID) +1  from Prmry_ModelHomeBaseValuations)
                     end

       if @ModelRecordID <> ''
              Begin

					Insert Into Prmry_ModelHomeBaseValuations (ModelRecordID, ValuationCode,  Quantity,  ORDERID, LAST_MODIFIED_BY, LAST_MODIFIED)
					Values (@ModelRecordID, @ValuationCode,@Quantity,@OrderId,@UserID,@date)

              end

END






GO
