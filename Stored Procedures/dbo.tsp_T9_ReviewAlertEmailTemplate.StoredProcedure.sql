USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ReviewAlertEmailTemplate]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_ReviewAlertEmailTemplate]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ReviewAlertEmailTemplate]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  
-- ==================================================================
-- This proc is used by the WUM to save Review Alert Email Templates
-- 2015 FEB 15	EWH - initial version
-- ==================================================================
CREATE PROCEDURE [dbo].[tsp_T9_ReviewAlertEmailTemplate]

@RecordID int, -- a value of -1 means we need to insert a record
@Subject varchar(1000),
@Body varchar(max)

AS
BEGIN
SET NOCOUNT ON;

if @RecordID = -1
begin
	--insert record
	insert into Prmry_ReviewControlAlertEmail ([Subject], [Body]) values(@Subject, @Body)
end
else
begin
	-- update record
	update Prmry_ReviewControlAlertEmail set [subject]=@Subject, [body]=@Body where [id]=@RecordID
end

END




GO
