USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearch_ListDepartmentPermissions]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_AdvSearch_ListDepartmentPermissions]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearch_ListDepartmentPermissions]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[tsp_T9_AdvSearch_ListDepartmentPermissions]
@UserID varchar(20) =  null,
@DepartmentID int = -1
As
Begin
if @UserID is not null
	begin
		select M.DepartmentID, D.DepartmentName, CanGet, CanShare, CanRename, CanDelete from Prmry_AdvSearch_DepartmentMembers M
		join Prmry_AdvSearch_Departments D on D.DepartmentID = M.DepartmentiD
		where userid = @UserID			
	end
 else 
	if @DepartmentID > 0
		begin
			select M.DepartmentID, D.DepartmentName, CanGet, CanShare, CanRename, CanDelete from Prmry_AdvSearch_DepartmentMembers M
			join Prmry_AdvSearch_Departments D on D.DepartmentID = M.DepartmentiD
			where M.DepartmentID = @DepartmentID
		end
end

GO
