USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[lp_focus_72hr_PermitIssue]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[lp_focus_72hr_PermitIssue]
GO
/****** Object:  StoredProcedure [dbo].[lp_focus_72hr_PermitIssue]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* 
--Ychavez 3/11/2014 - FOR FOCUS Measures report
-------------------------------------------------------------------------------------------
--80% of permits requiring additional review will be issued within 72 hours of initial application.
-- (Decks, 
--Fence, Basement Finish, Remodel, Misc, Retaining Wall, Structural Repair, Addition, Detached Garage)
--applied date is within 72 hours of issued date
--exec lp_focus_72hr_PermitIssue @APPLIED_FROM = '01/01/2014' , @APPLIED_TO = '01/10/2014'
-------------------------------------------------------------------------------------------
*/
CREATE procedure [dbo].[lp_focus_72hr_PermitIssue]
		@APPLIED_FROM datetime = null,
		@APPLIED_TO datetime = null
as
begin


	--1st measure--
-------------------------------------------------------------------------------------------
--80% of permits requiring additional review will be issued within 72 hours of initial application. (Decks, 
--Fence, Basement Finish, Remodel, Misc, Retaining Wall, Structural Repair, Addition, Detached Garage)
--applied date is within 72 hours of issued date
-------------------------------------------------------------------------------------------
--DROP TABLE #tmp_measure
Select 
case when DATEDIFF(day, applied, issued) < 3 then 'less than 72 hours' else
'out of measure'
end as measure
,0 as count_less_72_hours, 0 as count_outofmeasure, 0 as total_permits, *
into #tmp_measure
 from permit_main
where permittype in ('RESIDENTIAL', 'FENCE','RETAIN WALL' )
		and permitsubtype in('REMODEL', 'BASEMENT FINISH', 'ADDITION', 'DECK', 'DETACHED GARAGE', 
			'FOUNDATION REPAIR', 'MISCELLANEOUS', 'MOVING', 'STRUCTURAL REPAIR'
		, 'COMFENC', 'COMMERCIAL', 'PILLARS', 'SFDFENC', 'SINGLE FAMILY', 'SUBDIVISION')
AND ISSUED IS NOT NULL AND APPLIED IS NOT NULL
AND STATUS NOT IN ('WITHDRAWN', 'DENIED', 'VOID    ', 'VOID')

------------------------------------------------------------------------------------------
--CALCULATE TOTAL PERMITS / PERMITS IN MEASURE - OUT OF MEASURE / LESS THAN 72 MEASURE--
------------------------------------------------------------------------------------------

--drop table #tmp_count72
select COUNT(PERMIT_NO) as count_less_72_hours
into #tmp_count72
FROM #tmp_measure 
WHERE applied between @APPLIED_FROM and @APPLIED_TO
and measure = 'less than 72 hours'


--drop table #tmp_outofmeasure
select COUNT(PERMIT_NO) as count_outofmeasure
into #tmp_outofmeasure
FROM #tmp_measure 
WHERE applied between  @APPLIED_FROM and @APPLIED_TO
and measure = 'out of measure'

update #tmp_measure
set count_less_72_hours =  b.count_less_72_hours
from #tmp_count72 b


update #tmp_measure
set count_outofmeasure =  b.count_outofmeasure
from #tmp_outofmeasure b

select distinct sum(count_less_72_hours + count_outofmeasure) as total_permits--, count_less_72_hours
into #tmp_totpermits
 from #tmp_outofmeasure,  #tmp_count72


update #tmp_measure
set total_permits =  b.total_permits
from #tmp_totpermits b

--need to combine all the results into one table
--USE THESE TWO NUMBERS TO CALCULATE IN REPORT--
--select (count_less_72_hours  /total_permits) as total
--select * from #tmp_measure


select
SUM(cast(count_less_72_hours as decimal)) / Sum(cast(total_permits as decimal)) * 100 as percent_permits_adtl_review, total_permits, count_less_72_hours
from #tmp_measure
group by total_permits, count_less_72_hours


--select
--* --SUM(cast(total_permits as decimal)) / Sum(cast(count_less_72_hours  as decimal)) as test
--from #tmp_measure
end
GO
