USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_e3_AwaitingResposeForPublic]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_e3_AwaitingResposeForPublic]
GO
/****** Object:  StoredProcedure [dbo].[tsp_e3_AwaitingResposeForPublic]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  -- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[tsp_e3_AwaitingResposeForPublic]
	@etrakit_name as varchar(300) = '',
	@getpermit as varchar(1) = '0',
	@getproject as varchar(1) = '0',
	@getlicense as varchar(1) = '0',
	@activityno as varchar(15) = '',
	@reviewrecordid as varchar(30) = ''

AS
BEGIN

declare @sqlstring as nvarchar(max) = null
declare @etrakitpublicusername as varchar(60) = ''
declare @activityfilter as varchar(500) = ''
declare @reviewrecordidfilter as varchar(500) = ''

select @etrakitpublicusername = pref_value from etrakit3_preferences where  pref_name = 'LOGIN_PUBLIC_REGISTERED_USERID'

IF @getpermit = '1'
BEGIN
	IF EXISTS (SELECT 1 
			   FROM INFORMATION_SCHEMA.TABLES 
			   WHERE TABLE_TYPE='BASE TABLE' 
			   AND TABLE_NAME='tmpPermitResponse') 
	   DROP TABLE tmpPermitResponse

	declare @permitAllowingResponse as varchar(max) = null

	select @permitAllowingResponse = isnull(pref_value, '') from Etrakit3_Preferences where pref_name = 'Perm_ReviewStatusAllowingResponse'

	if @activityno <> ''
	BEGIN
		SET @activityfilter = ' pm.permit_no = ''' +  @activityno + ''' and ' 
	END

	if @reviewrecordid <> ''
	BEGIN
		SET @reviewrecordidfilter = ' pr.recordid = ''' +  @reviewrecordid + ''' and ' 
	END 

	if @permitAllowingResponse <> ''
	BEGIN
		set @sqlstring = 'SELECT * INTO tmpPermitResponse FROM (select pm.recordid, ''PERMIT'' AS record_group,  pm.permit_no AS record_no, pm.SITE_ADDR as site_address, pm.PermitType as record_type, pr.REVIEWTYPE as review_type, ''RESPOND'' as respond, UPPER(pr.status) AS status, pm.loc_recordid as loc_recordid, pr.recordid as review_recordid
		from Etrakit_Links el 
		inner join permit_reviews pr on ' + @reviewrecordidfilter + 'pr.PERMIT_NO = el.SUBLINK_NO and pr.STATUS in (' + @permitAllowingResponse + ')
		inner join permit_main pm on ' + @activityfilter + 'pm.permit_no = pr.permit_no and (pm.EXPIRED is null or pm.EXPIRED > getdate())
		left join geo_ownership geo on geo.recordid = pm.loc_recordid
		where el.ETRAKIT_NAME = ''' + @etrakit_name + ''' and ''' + @etrakitpublicusername + ''' not in (select top 1 USERID from prmry_notes where subgrouprecordid  = pr.recordid order by dateentered desc)
		) permits ORDER BY record_no'

		EXECUTE sp_executesql @sqlstring

		--print @sqlstring

		SELECT * FROM tmpPermitResponse
	END 
END

IF @getproject = '1'
BEGIN
	IF EXISTS (SELECT 1 
			   FROM INFORMATION_SCHEMA.TABLES 
			   WHERE TABLE_TYPE='BASE TABLE' 
			   AND TABLE_NAME='tmpProjectResponse') 
	   DROP TABLE tmpProjectResponse

	declare @projectAllowingResponse as varchar(max) = null

	select @projectAllowingResponse = isnull(pref_value, '') from Etrakit3_Preferences where pref_name = 'Proj_ReviewStatusAllowingResponse'

	if @activityno <> ''
	BEGIN
		SET @activityfilter = ' pm.project_no = ''' +  @activityno + ''' and ' 
	END

	if @reviewrecordid <> ''
	BEGIN
		SET @reviewrecordidfilter = ' pr.recordid = ''' +  @reviewrecordid + ''' and ' 
	END 

	IF @projectAllowingResponse <> ''
	begin
		set @sqlstring = 'SELECT * INTO tmpProjectResponse FROM (select pm.recordid, ''PROJECT'' AS record_group,  pm.project_no AS record_no, pm.SITE_ADDR as site_address, pm.ProjectType as record_type, pr.REVIEWTYPE as review_type, ''RESPOND'' as respond, UPPER(pr.status) AS status, pm.loc_recordid as loc_recordid, pr.recordid as review_recordid
		from Etrakit_Links el 
		inner join project_reviews pr on ' + @reviewrecordidfilter + 'pr.project_no = el.SUBLINK_NO and pr.STATUS in (' + @projectAllowingResponse + ')
		inner join project_main pm on ' + @activityfilter + 'pm.project_no = pr.project_no and (pm.EXPIRED is null or pm.EXPIRED > getdate())
		left join geo_ownership geo on geo.recordid = pm.loc_recordid
		where el.ETRAKIT_NAME = ''' + @etrakit_name + ''' and ''' + @etrakitpublicusername + ''' not in (select top 1 USERID from prmry_notes where subgrouprecordid  = pr.recordid order by dateentered desc)
		) project ORDER BY record_no'

		EXECUTE sp_executesql @sqlstring

		SELECT * FROM tmpProjectResponse
	end
END

IF @getlicense = '1'
BEGIN
	IF EXISTS (SELECT 1 
			   FROM INFORMATION_SCHEMA.TABLES 
			   WHERE TABLE_TYPE='BASE TABLE' 
			   AND TABLE_NAME='tmpLicenseResponse') 
	   DROP TABLE tmpLicenseResponse

	declare @licenseAllowingResponse as varchar(max) = null

	select @licenseAllowingResponse = isnull(pref_value, '') from Etrakit3_Preferences where pref_name = 'Lic2_ReviewStatusAllowingResponse'

	if @reviewrecordid <> ''
	BEGIN
		SET @reviewrecordidfilter = ' pr.recordid = ''' +  @reviewrecordid + ''' and ' 
	END 


	if @licenseAllowingResponse <> ''
	BEGIN
		set @sqlstring = 'SELECT * INTO tmpLicenseResponse FROM (select lm.recordid, ''LICENSE'' AS record_group,  lm.license_no AS record_no, lm.SITE_ADDR as site_address, lm.LICENSE_TYPE as record_type, pr.REVIEWTYPE as review_type, ''RESPOND'' as respond, UPPER(pr.status) AS status, lm.loc_recordid as loc_recordid, pr.recordid as review_recordid
		from Etrakit_Links el 
		inner join license2_reviews pr on ' + @reviewrecordidfilter + ' pr.license_no = el.SUBLINK_NO and pr.STATUS in (' + @licenseAllowingResponse + ')
		inner join license2_main lm on lm.license_no = pr.license_no
		left join geo_ownership geo on geo.recordid = lm.loc_recordid
		where el.ETRAKIT_NAME = ''' + @etrakit_name + ''' and ''' + @etrakitpublicusername + ''' not in (select top 1 USERID from prmry_notes where subgrouprecordid  = pr.recordid order by dateentered desc)
		) license2 ORDER BY record_no'

		EXECUTE sp_executesql @sqlstring

		SELECT * FROM tmpLicenseResponse
	END
END



END



GO
