USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ITR_ExportViolationsInfo]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_ITR_ExportViolationsInfo]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ITR_ExportViolationsInfo]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  
					  
					  					  					  /*
tsp_ExportViolationsInfo @DBName='DEV', @FilePath='C:\CRW\iTrakitExports\', @FileName='ViolationsInfoExport'

Select * from Prmry_ViolationsInfo

*/

CREATE Procedure [dbo].[tsp_ITR_ExportViolationsInfo](
@DBName Varchar(50),
@FilePath Varchar(1000),
@FileName Varchar(150),
@ToggleXP_CMDShell INT = 1
)

As

SET NOCOUNT ON

Declare @SQL Varchar(8000)

If @ToggleXP_CMDShell Is Null Set @ToggleXP_CMDShell = 1

IF EXISTS (SELECT * FROM tempdb.sys.objects WHERE object_id = OBJECT_ID(N'tempdb..ViolationsInfo') AND type in (N'U'))
	Drop Table tempdb..ViolationsInfo

Create Table tempdb..ViolationsInfo([RECORDID] Varchar(200),[CATEGORY] Varchar(200)
      ,[TITLE] Varchar(200)
      ,[DESCRIPTION] Varchar(8000))


Insert Into tempdb..ViolationsInfo(RECORDID,CATEGORY,TITLE,[DESCRIPTION])
Values('RECORDID','CATEGORY','TITLE','DESCRIPTION')
Set @SQL = '
Insert Into tempdb..ViolationsInfo([RECORDID],[CATEGORY],[TITLE],[DESCRIPTION])
Select 
RECORDID=CASE WHEN RECORDID = '''' THEN Null ELSE RECORDID END,
CATEGORY=CASE WHEN CATEGORY = '''' THEN Null ELSE REPLACE(REPLACE(REPLACE(CATEGORY,CHAR(9),''''),CHAR(10),''''),CHAR(13),'''') END,
TITLE=CASE WHEN TITLE = '''' THEN Null ELSE REPLACE(REPLACE(REPLACE(TITLE,CHAR(9),''''),CHAR(10),''''),CHAR(13),'''') END,
[DESCRIPTION]=CASE WHEN [DESCRIPTION] = '''' THEN Null ELSE Replace(Replace(Replace(Replace(Replace([DESCRIPTION],CHAR(9),''''),CHAR(10),''''),CHAR(13),''''),CHAR(11),''''),CHAR(124),'' '') END
From ' + @DBName + '..Prmry_ViolationsInfo
--WHERE CATEGORY <> ''VIOLATION''
ORDER BY Category,Title'
--Print(@SQL)
Exec(@SQL)

Select * From tempdb..ViolationsInfo


Declare @vDate Varchar(100)
Declare @OutputFileName Varchar(1000)

Set @vDate = Replace(Convert(Varchar,GetDate(),102),'.','_')
Print(@vDate)

--Set @OutputFileName = '"' + @FilePath + @FileName + '_' + @vDate + '.csv"'
Set @OutputFileName = '"' + @FilePath + @FileName + '.csv"'

If @ToggleXP_CMDShell = 1
 Begin
	--Enable xp_cmdshell
	Set @SQL = '
	EXEC sp_configure ''show advanced options'', 1
	RECONFIGURE
	EXEC sp_configure ''xp_cmdshell'', 1
	RECONFIGURE
	'
	Print(@SQL)
	Exec(@SQL)
 End

select @sql = 'bcp "select * from tempdb..ViolationsInfo" queryout ' + @OutputFileName + ' -c -t"|" -T -S'
+ @@servername
--Print(@SQL)
exec master..xp_cmdshell @sql

Drop Table tempdb..ViolationsInfo

Set @SQL = '
DELETE ' + @DBName + '.dbo.iTrakitFeeds Where [FileName] = ''' + @FileName + '.csv''

INSERT INTO ' + @DBName + '.dbo.iTrakitFeeds([filename],[data])
SELECT ''' + @FileName + '.csv'', * FROM OPENROWSET(
  BULK N''' + REPLACE(@OutputFileName,'"','') + ''', SINGLE_BLOB
) bt'
Print(@SQL)
Exec(@SQL)

If @ToggleXP_CMDShell = 1
 Begin
	-- Disable xp_cmdshell
	Set @SQL = '
	EXEC sp_configure ''xp_cmdshell'', 0
	RECONFIGURE
	EXEC sp_configure ''show advanced options'', 0
	RECONFIGURE
	'
	Print(@SQL)
	Exec(@SQL)
 End










GO
