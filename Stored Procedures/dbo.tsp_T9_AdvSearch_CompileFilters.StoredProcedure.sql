USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearch_CompileFilters]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_AdvSearch_CompileFilters]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AdvSearch_CompileFilters]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO





CREATE PROCEDURE [dbo].[tsp_T9_AdvSearch_CompileFilters]

/*
	-- udtt_T9_AdvSearch_Detail columns not used here
	ItemID int,
	Seq int

	-- udtt_T9_AdvSearch_Detail columns used here:
	RecordID int,
	TableName varchar(255),
	FieldName varchar(255),
	Datatype varchar(255),
	Filter varchar(255),
	Value1 varchar(max),
	Value2 varchar(max),
*/
	@Detail udtt_T9_AdvSearch_Detail READONLY,
	@FormatTablesAsColumns bit = 0 -- in Adv Search 3 we consolidate columns that are not the main table, the main udf table, or the 'additional' table

AS
BEGIN


/*
-- [udtt_T9_AdvSearch_DetailFilters] columns:
	[FilterID] [int] IDENTITY(1,1) NOT NULL,
	[TableName] [varchar](255) NULL,
	[FieldName] [varchar](255) NULL,
	[DisplayName] [varchar](255) NULL,
	[CompiledFilter] [varchar](max) NULL
*/

-- initialize @f and @remaining
declare @remaining [udtt_T9_AdvSearch_FilterChecklist] -- [FilterID] [int], [DetailID] [int], [Done] [bit]
declare @f [udtt_T9_AdvSearch_DetailFilters] -- RecordID int, CompiledFilter varchar(max)
if @FormatTablesAsColumns=0
begin
	-- the main, udf, and add'l tables that do not get consolidated 
	insert into @f ([TableName],[FieldName],[DisplayName],[CompiledFilter])
	select [TableName],[FieldName],[DisplayName], ''
	from @Detail
	order by Seq

	-- create a checklist to complete								-- there should only ever be 1 TableName per row in @f
	insert into @remaining ([DetailID],[FilterID],[Done])
	select d.[RecordID], f.[FilterID], 0							-- FilterID is an identity column so it should never be null
	from @Detail d 													-- table names are the same in main, udf, and additional tables
		left join @f f on d.[TableName] = f.[TableName]				-- make sure to join on all the columns since there could be more than 1 for a table
					  and d.[FieldName] = f.[FieldName]
					  and d.[DisplayName] = f.[DisplayName]
	where d.[Filter] is not null and d.[Filter]<>''					-- no need to run through the items that do not have a filter

end
else
begin
	-- the tables that are NOT the main, udf, and add'l tables. These get their filters consolidated and renamed as the table name
	insert into @f ([TableName],[FieldName],[DisplayName],[CompiledFilter])
	select [TableName], 'c', dbo.tfn_T9_AdvSearch_GetFriendlyTableName([TableName]), '' -- use the friendly name for the table
	from @Detail
	group by [TableName]

	-- create a checklist to complete										-- there should only ever be 1 TableName per row in @f
	insert into @remaining ([DetailID],[FilterID],[Done])
	select d.[RecordID], f.[FilterID], 0									-- FilterID is an identity column so it should never be null
	from @Detail d left join @f f on d.[TableName] = f.[TableName]			-- table names are preceded with an underscore
	where d.[Filter] is not null and d.[Filter]<>''							-- no need to run through the items that do not have a filter

end


-- local variables used in loop
declare @DetailID int
declare @FilterID int
declare @FieldName varchar(255)
declare @Datatype varchar(255) 
declare @Filter varchar(255) 
declare @Value1 varchar(max) 
declare @Value2 varchar(max) 
declare @s varchar(max) -- this is the segment created from the data from each row in @Detail and stored in @f as CompiledFilter
declare @temp varchar(max) -- temporarily hold previous filter info



-- loop :( wish there was a better way
while (select count(*) from @remaining where [Done] = 0) > 0
begin
	-- get IDs to work on
	select top 1 @DetailID=DetailID, @FilterID=FilterID from @remaining where [Done] = 0

	-- testing
	print 'start @DetailID=' + cast(@DetailID as varchar) + '   @FilterID=' + cast(@FilterID as varchar)


	-- get data for the detail ID
	select top 1 @FieldName= '[' + TableName +'].[' + FieldName + ']', @Datatype = Datatype, @Filter = Filter, @Value1 = Value1, @Value2 = Value2 from @Detail d where RecordID = @DetailID


	print '   @FieldName=' + @FieldName + '   @Datatype=' + @Datatype + '   @Filter=' + @Filter + '   @Value1=' + @Value1 + '   @Value2=' + @Value2


	-- determine @Value1 and @Value2 if we have calculated values
	if @Value1 in
	(
		'^lastCalendarYear^',
		'^lastFiscalYear^',
		'^lastMonth^',
		'^lastQuarter^',
		'^lastWeek^',
		'^nextCalendarYear^',
		'^nextFiscalYear^',
		'^nextMonth^',
		'^nextQuarter^',
		'^nextWeek^',
		'^thisCalendarYear^',
		'^thisFiscalYear^',
		'^thisMonth^',
		'^thisQuarter^',
		'^thisWeek^',
		'^yesterday^',
		'^today^',
		'^tomorrow^',
		'^todayPlus2^',
		'^todayPlus3^',
		'^todayPlus4^',
		'^todayPlus5^',
		'^todayPlus7^',
		'^todayPlus14^',
		'^todayPlus30^'
	)
	begin
		-- local variables
		declare @d1 date
		set @d1 = null
		declare @d2 date
		set @d2 = null
		declare @today date
		set @today = getdate()
		
		declare @v varchar(max) -- copy of @Value1
		set @v = @Value1
		
		if @v = '^today^' set @d1 = @today
		else
		begin
			if @v = '^yesterday^' set @d1 = DATEADD(d, -1, @today)
			else
			begin
				if @v = '^tomorrow^' set @d1 = DATEADD(d, 1, @today)
				else
				begin
					if @v = '^todayPlus2^' 
					begin
						set @d1 = @today
						set @d2 = DATEADD(d, 2, @today)
					end 
					else
					begin
						if @v = '^todayPlus3^' 
						begin
							set @d1 = @today
							set @d2 = DATEADD(d, 3, @today)
						end 
						else
						begin
							if @v = '^todayPlus4^' 
							begin
								set @d1 = @today
								set @d2 = DATEADD(d, 4, @today)
							end 
							else
							begin
								if @v = '^todayPlus5^' 
								begin
									set @d1 = @today
									set @d2 = DATEADD(d, 5, @today)
								end 
								else
								begin
									if @v = '^todayPlus7^' 
									begin
										set @d1 = @today
										set @d2 = DATEADD(d, 7, @today)
									end 
									else
									begin
										if @v = '^todayPlus14^' 
										begin
											set @d1 = @today
											set @d2 = DATEADD(d, 14, @today)
										end 
										else
										begin
											if @v = '^todayPlus30^'
											begin
												set @d1 = @today
												set @d2 = DATEADD(d, 30, @today)
											end 
											else
											begin
												if @v = '^thisWeek^'
												begin
													set @d1 = DATEADD(week, DATEDIFF(week, 0, @today), 0)
													set @d2 = dateadd(d, -1, DATEADD(week, 1, @d1)) -- add 1 week, subtract 1 day
												end 
												else
												begin
													if @v = '^thisQuarter^'
													begin
														set @d1 = DATEADD(q, DATEDIFF(q, 0, @today), 0)
														set @d2 = dateadd(d, -1, DATEADD(q, 1, @d1)) -- add 1 quarter, subtract 1 day
													end 
													else
													begin
														if @v = '^thisMonth^'
														begin
															set @d1 = DATEADD(m, DATEDIFF(m, 0, @today), 0)
															set @d2 = dateadd(d, -1, DATEADD(m, 1, @d1))
														end 
														else
														begin
															-- TODO
															if @v = '^thisFiscalYear^'
															begin
																set @Value1=''
																set @Value2=''
															end 
															else
															begin
																if @v = '^thisCalendarYear^'
																begin
																	set @d1 = DATEADD(year, DATEDIFF(year, 0, @today), 0)
																	set @d2 = dateadd(d, -1, DATEADD(year, 1, @d1)) -- add 1 year, subtract 1 day
																end 
																else
																begin
																	if @v = '^nextWeek^'
																	begin
																		set @d1 = DATEADD(week, 1, DATEADD(week, DATEDIFF(week, 0, @today), 0))
																		set @d2 = dateadd(d, -1, DATEADD(week, 1, @d1)) -- add 1 week, subtract 1 day
																	end 
																	else
																	begin
																		if @v = '^nextQuarter^'
																		begin
																			set @d1 = DATEADD(q, 1, DATEADD(q, DATEDIFF(q, 0, @today), 0)) -- add 1 quarter to the beginning of this quarter
																			set @d2 = dateadd(d, -1, DATEADD(q, 1, @d1)) -- add 1 quarter, subtract 1 day
																		end 
																		else
																		begin
																			if @v = '^nextMonth^'
																			begin
																				set @d1 = dateadd(m, 1, DATEADD(m, DATEDIFF(m, 0, @today), 0))
																				set @d2 = dateadd(d, -1, DATEADD(m, 1, @d1))
																			end 
																			else
																			begin
																				-- TODO
																				if @v = '^nextFiscalYear^'
																				begin
																					set @Value1=''
																					set @Value2=''
																				end 
																				else
																				begin
																					if @v = '^nextCalendarYear^'
																					begin
																						set @d1 = DATEADD(year, 1, DATEADD(year, DATEDIFF(year, 0, @today), 0))
																						set @d2 = dateadd(d, -1, DATEADD(year, 1, @d1)) -- add 1 year, subtract 1 day
																					end 
																					else
																					begin
																						if @v = '^lastWeek^'
																						begin
																							set @d1 = DATEADD(week, -1, DATEADD(week, DATEDIFF(week, 0, @today), 0))
																							set @d2 = dateadd(d, -1, DATEADD(week, 1, @d1)) -- add 1 week, subtract 1 day
																						end 
																						else
																						begin
																							if @v = '^lastQuarter^'
																							begin
																								set @d1 = DATEADD(q, -1, DATEADD(q, DATEDIFF(q, 0, @today), 0)) -- subtract 1 quarter to the beginning of this quarter
																								set @d2 = dateadd(d, -1, DATEADD(q, 1, @d1)) -- add 1 quarter, subtract 1 day
																							end 
																							else
																							begin
																								if @v = '^lastMonth^'
																								begin
																									set @d1 = dateadd(m, -1, DATEADD(m, DATEDIFF(m, 0, @today), 0))
																									set @d2 = dateadd(d, -1, DATEADD(m, 1, @d1))
																								end 
																								else
																								begin
																									-- TODO
																									if @v = '^lastFiscalYear^'
																									begin
																										set @Value1=''
																										set @Value2=''
																									end
																									else
																									begin
																										if @v = '^lastCalendarYear^'
																										begin
																											set @d1 = DATEADD(year, -1, DATEADD(year, DATEDIFF(year, 0, @today), 0))
																											set @d2 = dateadd(d, -1, DATEADD(year, 1, @d1)) -- add 1 year, subtract 1 day
																										end
																									end
																								end
																							end
																						end
																					end
																				end
																			end
																		end
																	end
																end
															end
														end
													end
												end
											end
										end
									end
								end
							end
						end
					end
				end
			end
		end

		-- set @Value1 and Value2 with whatever dates we calculated above
		set @Value1 = case when @d1 is null then '' else convert(varchar, @d1, 101) end
		set @Value2 = case when @d2 is null then '' else convert(varchar, @d2, 101) end
	end


	-- Now create the segment now that we have @Value1 and @Value2
	set @s=''
	if @Filter = 'is not nothing nor blank' set @s = '(' + @FieldName + ' is not null and ' + @FieldName + '<>' + '''' + '''' + ') '
	else
	begin
		if @Filter = 'is nothing or blank' set @s = '(' + @FieldName + ' is null or ' + @FieldName + '=' + '''' + '''' + ') '
		else
		begin
			if @Filter = 'in' and @Value1 is not null and @Value1<>''
			begin
				if @Datatype in ('float','int','money','numeric','real','smallint') set @s = @FieldName + ' in (' + replace(@Value1,'|',',') + ') '
				else set @s = @FieldName + ' in (''' + replace(@Value1,'|',''',''') + ''') '     
			end
			else
			begin
				if @Filter = 'not in' and @Value1 is not null and @Value1<>''
				begin
					if @Datatype in ('float','int','money','numeric','real','smallint') set @s = @FieldName + ' not in (' + replace(@Value1,'|',',') + ') '
					else set @s = @FieldName + ' not in (''' + replace(@Value1,'|',''',''') + ''') '   				
				end
				else
				begin
					if @Filter = 'contains' and @Value1 is not null and @Value1<>'' set @s = @FieldName + ' like ''' + '%' + @Value1 + '%''' 
					else
					begin
						if @Filter = 'begins with' and @Value1 is not null and @Value1<>'' set @s = @FieldName + ' like ''' + @Value1 + '%''' 
						else
						begin
							if @Filter = 'ends with' and @Value1 is not null and @Value1<>'' set @s = @FieldName + ' like ''' + '%' + @Value1 + ''''
							else
							begin
								if @Filter = 'is' and @Value1 is not null and @Value1<>''
								begin
									if @Datatype in ('float','int','money','numeric','real','smallint') set @s = @FieldName + '=' + @value1 + ' '
									--else set @s = @FieldName + ' like ''' + @Value1 +  ''' '  -- Changed = operator to like. GP: 9175
									else set @s = @FieldName + '=''' + @Value1 +  ''' '  -- EWH Mar 26, 2018 #126041: this was correct the way it was originally.
								end
								else
								begin
									if @Filter = 'is not' and @Value1 is not null and @Value1<>''
									begin
										if @Datatype in ('float','int','money','numeric','real','smallint') set @s = @FieldName + '<>' + @Value1 + ' '
										else set @s = @FieldName + '<>''' + @Value1 +  ''' ' 
									end
									else
									begin
										if @Filter = 'greater than or equal to' and @Value1 is not null and @Value1<>''
										begin         
											if @Datatype in ('float','int','money','numeric','real','smallint') set @s = @FieldName + '>=' + @Value1 +  ' ' 
											else
											begin
												if @Datatype in ('date','datetime','datetime2') 
												begin 
													-- if this is a date search and we have a range, include the range in the greater than or equal to (use @FieldName >= @Value1)
													-- this is the default behavior, but here in case people change their minds on this behavior
													--if @Value2 <> '' set @s = @FieldName + '>''' + @Value2 + '''' 
													--else 
													set @s = @FieldName + '>=''' + @Value1 + ''' '   
												end
												else  set @s = @FieldName + '>=''' + @value1 +  ''' ' 
											end
										end
										else
										begin
											if @Filter = 'greater than' and @Value1 is not null and @Value1<>''
											begin         
												if @Datatype in ('float','int','money','numeric','real','smallint') set @s = @FieldName + '>' + @Value1 + ' ' 
												else
												begin
													if @Datatype in ('date','datetime','datetime2') 
													begin 
														-- if this is a date search and we have a range, DO NOT include the range in the greater than (use @FieldName > @Value2)
														if @Value2 <> '' set @s = @FieldName + '>''' + @Value2 + ''' ' 
														else set @s = @FieldName + '>''' + @Value1 + ''' '   
													end
													else set @s = @FieldName + '>''' + @Value1 +  ''' ' 
												end
											end
											else
											begin
												if @Filter = 'less than or equal to' and @Value1 is not null and @Value1<>''
												begin         
													if @Datatype in ('float','int','money','numeric','real','smallint') set @s = @FieldName + '<=' + @Value1 +  ' ' 
													else 
													begin 
														if @Datatype in ('date','datetime','datetime2') 
														begin 
															-- if this is a date search and we have a range, include the range in the less than (use @FieldName <= @Value2)
															if @Value2 <> '' set @s = @FieldName + '<=''' + @Value2 + ''' ' 
															else set @s = @FieldName + '<=''' + @Value1 + ''' '   
														end
														else set @s = @FieldName + '<=''' + @Value1 + ''' ' 
													end
												end
												else
												begin
													if @Filter = 'less than' and @Value1 is not null and @Value1<>''
													begin         
														if @Datatype in ('float','int','money','numeric','real','smallint') set @s = @FieldName + '<' + @Value1 +  ' ' 
														else 
														begin 
															if @Datatype in ('date','datetime','datetime2') 
															begin 
																-- if this is a date search and we have a range, DO NOT include the range in the less than (use @FieldName < @Value1)
																-- this is the default behavior, but here in case people change their minds on this behavior
																--if @Value2 <> '' set @s = @FieldName + '<''' + @value2 + ''''
																--else 
																set @s = @FieldName + '<''' + @Value1 + ''' '  
															end
															else set @s = @FieldName + '<''' + @Value1 + ''' ' 
														end
													end
													else
													begin
														if @Filter = 'is between' and @Value1 is not null and @Value1<>'' and @Value2 is not null and @Value2<>''
														begin
															if @Datatype in ('float','int','money','numeric','real','smallint') set @s = @FieldName + ' between ' + @Value1 +  ' and ' + @Value2 + ' ' 
															else set @s = @FieldName + ' between ''' + @Value1 +  ''' and ''' + @Value2 + ''' ' 
														end   														
														else
														begin
															if @Filter = 'is not between' and @Value1 is not null and @Value1<>'' and @Value2 is not null and @Value2<>''
															begin
																if @Datatype in ('float','int','money','numeric','real','smallint') set @s = @FieldName + ' not between ' + @Value1 +  ' and ' + @Value2 + ' ' 
																else set @s = @FieldName + ' not between ''' + @Value1 +  ''' and ''' + @Value2 + ''' ' 
															end
														end
													end											
												end											
											end											
										end
									end
								end
							end
						end
					end
				end
			end
		end
	end

	if @s is not null or @s <>''
	begin
		-- update the filter info for the row
		if @FormatTablesAsColumns = 1
		begin
			-- if we are concatenating the columns, get the existing filter info for the row first as we have to append the filter info
			set @temp = (select [CompiledFilter] from @f where FilterID = @FilterID)
			if @temp is null or @temp = ''
			begin
				-- no filter values yet
				if @s is not null and @s <> '' update @f set [CompiledFilter] = @s where FilterID = @FilterID -- no need to update an empty value with an empty value
			end
			else
			begin
				-- there is already a filter value here
				if @s is not null and @s <> '' update @f set [CompiledFilter] = @temp + ' AND ' + @s where FilterID = @FilterID -- no need to update if @s is null or empty string
			end
		end
		else
		begin
			-- if we are working with the main / udf / add'l tables just update the filter for the row
			update @f set [CompiledFilter] = @s where FilterID = @FilterID
		end
	end

	-- update @remaining so we don't go through this row again
	update @remaining set [Done]=1 where DetailID=@DetailID and FilterID=@FilterID

	-- testing
	--print 'end   @DetailID=' + cast(@DetailID as varchar) + '   @FilterID=' + cast(@FilterID as varchar)

end

-- return the data 
select [TableName],[FieldName],[DisplayName],[CompiledFilter] from @f

END
GO
