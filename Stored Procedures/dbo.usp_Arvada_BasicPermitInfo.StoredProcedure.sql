USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_BasicPermitInfo]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[usp_Arvada_BasicPermitInfo]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_BasicPermitInfo]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_Arvada_BasicPermitInfo]
	@PermitNo varchar(15)
as
/**********************************************************************************************************************
* Creator:      	 
* Date:         	 
* Title:			 
* Description:  	 
* Examples:    

EXECUTE dbo.usp_Arvada_BasicPermitInfo @permitNo = 'COMM13-00110'
					 
* Modifications:  	
  Developer Name		Date		Description
  ------------------	----------	----------------------------------------------------------- 
*
**********************************************************************************************************************/

set nocount on
 
 --------------------------
 --- Declare Variables   --
 --------------------------

 
 --------------------------
 --- Create Temp Tables  --
 --------------------------

	 
 --------------------------
 --- Body of Procedure   --
 --------------------------


select 
	m.PERMIT_NO,
	m.SITE_ADDR,
	m.SITE_SUBDIVISION,
	m.other_date1, 
	m.finaled,
	u.username as FinaledBy,
	p.Name as OwnerName,
	p.Address1 as OwnerAddress1,
	p.address2 as OwnerAddress2,
	p.City as OwnerCity,
	p.[state] as OwnerState,
	p.zip as OwnerZip,
	p.phone as OwnerPhone,
	p.OTHER_NAME as OwnerOtherName,
	p.Email as OwnerEmail,
	p.BUS_License as OwnerBusLicense,
	pu.CPR_OCCUPANCY,
	pu.CPR_TTLOCCLOAD,
	pu.CPR_CONSTRTYPE,
	pu.FSI_FIRESYSPROV,
	pu.FSI_SPRINKRQD,
	pu.tco_date_expired,
	pu.CPR_USE,
	pu.CPR_CODE_YEAR,
	dbo.udf_AddressBlock(p.Address1, p.City, p.[state], p.zip) as OwnerAddressBlock
from dbo.PERMIT_MAIN m
left join dbo.Prmry_Users u
	on m.finaled_BY = u.userid
left join dbo.permit_people p
	on p.PERMIT_NO = m.PERMIT_NO
	and p.nameType = 'OWNER'
left join dbo.Permit_UDF pu
	on pu.permit_no = m.permit_no
where 
	m.PERMIT_NO = @PermitNo

GO
