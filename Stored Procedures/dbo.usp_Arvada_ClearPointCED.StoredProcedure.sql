USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_ClearPointCED]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[usp_Arvada_ClearPointCED]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_ClearPointCED]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec dbo.usp_Arvada_BasicFocusMeasures '06/01/2015','06/30/2015'

CREATE PROCEDURE [dbo].[usp_Arvada_ClearPointCED]
(
	@StartDate datetime,
	@EndDate DATETIME ,
	@ReportType VARCHAR(50) = null
)

AS 

BEGIN

SET NOCOUNT ON

/*
exec dbo.[usp_Arvada_ClearPointCED] '02/01/2020','02/28/2020','Inspections'
exec dbo.[usp_Arvada_ClearPointCED] '02/01/2020','02/28/2020','BldgReviews'
exec dbo.[usp_Arvada_ClearPointCED] '02/01/2020','02/28/2020','TCO'



*/

 --------------------------
 --- Declare Variables   --
 --------------------------

 --remove time from dates
 select @startdate = convert(Date, @startDate), 
		@EndDate = convert(Date, @EndDate)
 --------------------------
 --- Create Temp Tables  --
 --------------------------
 
	 
 --------------------------
 --- Body of Procedure   --
 --------------------------
 if @ReportType = 'Inspections'
 begin
	--98% of Building permit inspections will conducted on the day scheduled					
	SELECT 
		PERMIT_NO, 
		InspectionType, 
		INSPECTOR, SCHEDULED_DATE, 
		COMPLETED_DATE, 
		dbo.[fn_Arvada_WorkDays](SCHEDULED_DATE, COMPLETED_DATE) as WorkDaysBetween,
		case when dbo.[fn_Arvada_WorkDays](SCHEDULED_DATE, COMPLETED_DATE) != 0 then 1 else 0 end as ExtraDays
	FROM Permit_Inspections
	WHERE SCHEDULED_DATE >= @StartDate
	and Scheduled_Date < dateadd(day,1,@endDate)
	AND INSPECTOR IN ('AQ', 'AS', 'BU', 'CARS', 'C1', 'DA', 'DK', 'DR', 'DA1', 'DR1', 'KCA', 'NR', 'DMCP', 'CCI', 'DPL', 'DSTR', 'DELE')
	and isnull(REMARKS,'') not like 'VOIDED%' 

 end
 else if @ReportType = 'BldgReviews'
 begin
        -- On an ongoing basis, an average of all non-over the counter commercial building permits 1st review will be reviewed within 20 business days.					
		--SELECT  AVG(DATEDIFF(D, DATE_SENT, DATE_RECEIVED)) as average_days
		--FROM Permit_Reviews R
		--WHERE DATE_DUE BETWEEN '2020-02-01' AND '2020-02-29'
		--AND REVIEWTYPE = 'BUILDING-COMMERCIAL'
		--AND isnull(REMARKS,'') not like 'VOIDED%'

		SELECT 
			r.PERMIT_NO, 
			r.REVIEWTYPE, 
			m.description, 
			r.DATE_SENT, 
			r.DATE_RECEIVED, 
			r.DATE_DUE,
			dbo.[fn_Arvada_WorkDays]( r.DATE_SENT, r.DATE_RECEIVED) as DaysForReview
		FROM Permit_Reviews R
		left join Permit_Main m
		on r.PERMIT_NO = m.PERMIT_NO
		WHERE r.DATE_DUE >= @StartDate 
		AND	r.DATE_DUE < dateadd(day,1,@endDate)
		AND r.REVIEWTYPE = 'BUILDING-COMMERCIAL'
		AND isnull(r.REMARKS,'') not like 'VOIDED%'
 end
 else if @ReportType = 'TCO'
 begin
	--------RUNNING TOTAL OF ALL TCO'S ISSUED/EXPIRED AND NO CO ISSUED-------------------
	SELECT 
		m.PERMIT_NO, 
		m.STATUS, 
		m.FINALED, 
		m.OTHER_DATE1, 
		m.PermitType, 
		SITE_ADDR, 
		SITE_SUBDIVISION, 
		description, 
		u.TCO_ISSUED_BY, 
		u.TCO_ALLOWED_TIME, 
		u.TCO_DATE_EXPIRED, 
		u.TCO_DATE_ISSUED, 
		u.TCO_ITEMS_COMPLETE,
		case when month(TCO_DATE_ISSUED) = month(@startdate)
					and year(tco_Date_issued) = year(@startdate)
					then 1
					else 0 end
					as InCurrentMonth
	FROM Permit_Main M
	inner join permit_udf u
		on m.permit_no = u.permit_no
	WHERE U.TCO_DATE_ISSUED < dateadd(day,1,@endDate)
	AND M.STATUS NOT IN ('CO ISSUED', 'FINALED', 'VOID')
 end
 else if @ReportType = 'ResPermits'
 begin
		 --------# of residential permits issued---------This is annual--
		------count NSF/Townhomes--------------
		SELECT 
			DATEADD(month, DATEDIFF(month, 0, issued), 0) as MonthValue,
			COUNT(PERMIT_NO)
		FROM PERMIT_MAIN
		WHERE 
			ISSUED >= dateadd(year,-1,@StartDate)
		AND	ISSUED < dateadd(day,1,@endDate)
		AND PERMITSUBTYPE IN ('NEW DUPLEX', 'NEW SF ATTACHED', 'NEW SF DETACHED', 'NEW SFA', 'NEW SINGLE FAMILY')
		and status not in ('void', 'withdrawn')
		group by  
			DATEADD(month, DATEDIFF(month, 0, issued), 0)
		order by MonthValue


 end
 else if @ReportType = 'MultiFamily'
 begin
	-------mulitfamily unit count----------
	select 
		U.CPR_MF_UNIT, 
		m.permit_no, datepart(YEAR, ISSUED)
	from permit_udf U
	LEFT JOIN Permit_Main M 
		ON U.PERMIT_NO = M.PERMIT_NO
	where 
			PERMITSUBTYPE IN ('NEW MULTI FAMILY', 'multi-family', 'multi family')
		and PermitType not in ('PROPTY MAINT' , 'FLOODPL DEV')
		AND ISSUED >= '01/01/'+ convert(varchar(4), year(@StartDate))
		AND	ISSUED < dateadd(day,1,@endDate)
		and status not in ('void', 'withdrawn')
	group by U.CPR_MF_UNIT, m.PERMIT_NO, datepart(YEAR, ISSUED)
	order by m.permit_no, DATEPART(year, issued)


 end
 else if @ReportType = 'ResMisc'
 begin 
	 --------95% of misc residential within 1 day--------weekend/holiday add------
	select 
		m.PERMIT_NO, 
		m.STATUS, 
		m.applied, 
		m.approved, 
		r.DATE_DUE, 
		r.DATE_RECEIVED, 
		 dbo.[fn_Arvada_WorkDays]( r.DATE_DUE, r.DATE_RECEIVED) 
	from permit_main m
	left join Permit_Reviews r
	on m.PERMIT_NO = r.PERMIT_NO
	where applied >= @startdate
	and applied < dateadd(Day,1,@endDate) 
	and PermitType in ('MISC', 'RESIDENTIAL ELECTRICAL', 'RESIDENTIAL MECHANICAL', 'RESIDENTIAL PLUMBING', 'RESMISC', 'ROOF')
	and m.STATUS not in ('VOID', 'withdrawn')
	and REVIEWTYPE = 'ONLINE APPLICATION-BID'
	and isnull(REMARKS,'') not like 'VOIDED%'
	and DATEDIFF(D, DATE_DUE, DATE_RECEIVED) > 0
	order by datediff(d, applied, APPROVED) asc
 end
 else if @ReportType = 'NumComPermits'
 begin

	-----NUMBER OF COMMERCIAL PERMITS ISSUED------
	SELECT PERMIT_NO, DESCRIPTION, PermitSubType
	FROM Permit_Main
	WHERE PermitType = 'COMMERCIAL'
	AND ISSUED >= @startdate
	and ISSUED < dateadd(Day,1,@endDate)  
	AND STATUS NOT IN ('VOID', 'WITHDRAWN')

 end
  else if @ReportType = 'NumComPermits'
 begin

		-----number of CO's without issuing a TCO--------
		select 
			m.PERMIT_NO, 
			m.STATUS, 
			m.FINALED, 
			m.OTHER_DATE1, 
			m.PermitType, 
			SITE_ADDR, 
			SITE_SUBDIVISION, 
			description, 
			u.TCO_ISSUED_BY, 
			u.TCO_ALLOWED_TIME, 
			u.TCO_DATE_EXPIRED, 
			u.TCO_DATE_ISSUED, 
			u.TCO_ITEMS_COMPLETE
		from Permit_Main m
		inner join permit_udf u
			on m.permit_no = u.permit_no
		where    
		  OTHER_DATE1 >= @startdate
		and OTHER_DATE1 < dateadd(Day,1,@endDate) 
		and u.TCO_DATE_EXPIRED is null
		and PermitType in ('COMMERCIAL', 'NEW SINGLE FAMILY')
		and m.STATUS in ('CO ISSUED', 'FINALED')
 end
 else if @ReportType = 'PermitsIssuedBy'
 begin
		 -----# OF PERMITS ISSUED BY COUNTER VS. ECON/EPRS-----put in case -
		SELECT 
			COUNT(PERMIT_NO) as CountofPermits, 
			DATEPART(MONTH, ISSUED) as MonthIssued, 
			case when ISSUED_BY IN ('WEB', 'ECON', 'EPRS') then 'etrakit' else 'counter' end IssuedBy
		FROM Permit_Main
		WHERE 
		 ISSUED >= @startdate
		and ISSUED < dateadd(Day,1,@endDate) 
		AND PREFIX NOT IN ('ROW', 'SPE', 'FDT', 'FENC', 'PLNR', 'PCBMP', 'SITE')
		GROUP BY 
			DATEPART(MONTH, ISSUED), 
			case when ISSUED_BY IN ('WEB', 'ECON', 'EPRS') then 'etrakit' else 'counter' end
		order by 
			IssuedBy,
			MonthIssued

 end

  

END -- PROC END

 
GO
