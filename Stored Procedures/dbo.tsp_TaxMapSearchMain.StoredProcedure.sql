USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_TaxMapSearchMain]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_TaxMapSearchMain]
GO
/****** Object:  StoredProcedure [dbo].[tsp_TaxMapSearchMain]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE Procedure [dbo].[tsp_TaxMapSearchMain](
	@Top INT = NULL,
	@Group Varchar(50) = NULL,
	@Section0 Varchar(12) = NULL,
	@Section1 Varchar(12) = NULL,
	@Section2 Varchar(12) = NULL,
	@Section3 Varchar(12) = NULL,
	@Section4 Varchar(12) = NULL,
	@Section5 Varchar(12) = NULL,
	@Section6 Varchar(12) = NULL,
	@Section7 Varchar(12) = NULL,
	@Section8 Varchar(12) = NULL,
	@Section9 Varchar(12) = NULL,
	@ExcludedGroups Varchar(250) = NULL,
	@ActivityGroupsOnly INT = NULL
)

AS
BEGIN

DECLARE @SQL Varchar(8000)
DECLARE @vTop Varchar(50)	
DECLARE @TempSQL Varchar(1000)

DECLARE @CombinedSections Varchar(100)

If @Group = '' Set @Group = NULL
If @ActivityGroupsOnly Is Null Set @ActivityGroupsOnly = 0
If @ExcludedGroups = '' Set @ExcludedGroups = Null
If @ExcludedGroups Is Not Null 
 Begin
	Set @ExcludedGroups = CONCAT('''' , @ExcludedGroups , '''')
	Set @ExcludedGroups = REPLACE(@ExcludedGroups,',',''',''')
 End


If @Top Is Null Set @Top = 0
Set @vTop = ''
If @Top > 0 Set @vTop = CONCAT(' Top ' , Convert(Varchar,@Top) , ' ')

Create Table #Results(RecordID Varchar(30),Loc_RecordID Varchar(30),Tgroup Varchar(8),ActivityNo Varchar(30),Site_APN Varchar(50),Site_Addr Varchar(100), Site_City Varchar(30),RecordType Varchar(30) Default 'Activity')

Print 'Insert Parcels Into #Results'
BEGIN
 	SET @SQL = 'Insert Into #Results(RecordID,Loc_RecordID,ActivityNo,Tgroup,Site_APN,Site_Addr,Site_City,RecordType)
	SELECT RecordID,Loc_RecordID,ActivityNo,Tgroup,Site_APN,Site_Addr, city,''Parcel''
	From tvw_GblActivityParcels WHERE SITE_APN like '

    SET @Section0 = CONCAT(@Section0, '%')

	--Section0 will always exist with at least 1 character (%).
	IF (charIndex(' ', @Section0) > 0)
	BEGIN
        --Need to ensure that every section ends with a % in case users fail to add a space.
		SET @Section0 = REPLACE(@Section0, ' ', '%')

		--We want to remove any instance of %% as it causes errors for the search results, but we want to keep the original Section Length.
		WHILE (charIndex('%%', @Section0) > 0)
		BEGIN
			SET @Section0 = REPLACE(@Section0, '%%', '%')
		END
	END

    SET @CombinedSections = @Section0

	IF @Section1 IS NOT NULL 
	BEGIN
        SET @Section1 = CONCAT(@Section1, '%')

		IF (charIndex(' ', @Section1) > 0)
		BEGIN
			SET @Section1 = CONCAT(REPLACE(@Section1, ' ', '%'), '%')

			--We want to remove any instance of %% as it causes errors for the search results, but we want to keep the original Section Length.
			WHILE (charIndex('%%', @Section1) > 0)
			BEGIN
				SET @Section1 = REPLACE(@Section1, '%%', '%')
			END
		END

        SET @CombinedSections = CONCAT(@CombinedSections , @Section1)
	END

	IF @Section2 IS NOT NULL 
	BEGIN
        SET @Section2 = CONCAT(@Section2, '%')

		IF (charIndex(' ', @Section2) > 0)
		BEGIN
			SET @Section2 = CONCAT(REPLACE(@Section2, ' ', '%'), '%')

			--We want to remove any instance of %% as it causes errors for the search results, but we want to keep the original Section Length.
			WHILE (charIndex('%%', @Section2) > 0)
			BEGIN
				SET @Section2 = REPLACE(@Section2, '%%', '%')
			END
		END

        SET @CombinedSections = CONCAT(@CombinedSections , @Section2)
	END

	IF @Section3 IS NOT NULL 
	BEGIN
        SET @Section3 = CONCAT(@Section3, '%')

		IF (charIndex(' ', @Section3) > 0)
		BEGIN
			SET @Section3 = CONCAT(REPLACE(@Section3, ' ', '%'), '%')

			--We want to remove any instance of %% as it causes errors for the search results, but we want to keep the original Section Length.
			WHILE (charIndex('%%', @Section3) > 0)
			BEGIN
				SET @Section3 = REPLACE(@Section3, '%%', '%')
			END
		END

        SET @CombinedSections = CONCAT(@CombinedSections , @Section3)
	END

	IF @Section4 IS NOT NULL 
	BEGIN
        SET @Section4 = CONCAT(@Section4, '%')

		IF (charIndex(' ', @Section4) > 0)
		BEGIN
			SET @Section4 = CONCAT(REPLACE(@Section4, ' ', '%'), '%')

			--We want to remove any instance of %% as it causes errors for the search results, but we want to keep the original Section Length.
			WHILE (charIndex('%%', @Section4) > 0)
			BEGIN
				SET @Section4 = REPLACE(@Section4, '%%', '%')
			END
		END

        SET @CombinedSections = CONCAT(@CombinedSections , @Section4)
	END

	IF @Section5 IS NOT NULL 
	BEGIN
        SET @Section5 = CONCAT(@Section5, '%')

		IF (charIndex(' ', @Section5) > 0)
		BEGIN
			SET @Section5 = CONCAT(REPLACE(@Section5, ' ', '%'), '%')

			--We want to remove any instance of %% as it causes errors for the search results, but we want to keep the original Section Length.
			WHILE (charIndex('%%', @Section5) > 0)
			BEGIN
				SET @Section5 = REPLACE(@Section5, '%%', '%')
			END
		END

        SET @CombinedSections = CONCAT(@CombinedSections , @Section5)
	END

	IF @Section6 IS NOT NULL 
	BEGIN
        SET @Section6 = CONCAT(@Section6, '%')

		IF (charIndex(' ', @Section6) > 0)
		BEGIN
			SET @Section6 = CONCAT(REPLACE(@Section6, ' ', '%'), '%')

			--We want to remove any instance of %% as it causes errors for the search results, but we want to keep the original Section Length.
			WHILE (charIndex('%%', @Section6) > 0)
			BEGIN
				SET @Section6 = REPLACE(@Section6, '%%', '%')
			END
		END

        SET @CombinedSections = CONCAT(@CombinedSections , @Section6)
	END

	IF @Section7 IS NOT NULL 
	BEGIN
        SET @Section7 = CONCAT(@Section7, '%')

		IF (charIndex(' ', @Section7) > 0)
		BEGIN
			SET @Section7 = CONCAT(REPLACE(@Section7, ' ', '%'), '%')

			--We want to remove any instance of %% as it causes errors for the search results, but we want to keep the original Section Length.
			WHILE (charIndex('%%', @Section7) > 0)
			BEGIN
				SET @Section7 = REPLACE(@Section7, '%%', '%')
			END
		END

        SET @CombinedSections = CONCAT(@CombinedSections , @Section7)
	END

	IF @Section8 IS NOT NULL 
	BEGIN
        SET @Section8 = CONCAT(@Section8, '%')

		IF (charIndex(' ', @Section8) > 0)
		BEGIN
			SET @Section8 = CONCAT(REPLACE(@Section8, ' ', '%'), '%')

			--We want to remove any instance of %% as it causes errors for the search results, but we want to keep the original Section Length.
			WHILE (charIndex('%%', @Section8) > 0)
			BEGIN
				SET @Section8 = REPLACE(@Section8, '%%', '%')
			END
		END

        SET @CombinedSections = CONCAT(@CombinedSections , @Section8)
	END

	IF @Section9 IS NOT NULL 
	BEGIN
        SET @Section9 = CONCAT(@Section9, '%')

		IF (charIndex(' ', @Section9) > 0)
		BEGIN
			SET @Section9 = CONCAT(REPLACE(@Section9, ' ', '%'), '%')

			--We want to remove any instance of %% as it causes errors for the search results, but we want to keep the original Section Length.
			WHILE (charIndex('%%', @Section9) > 0)
			BEGIN
				SET @Section9 = REPLACE(@Section9, '%%', '%')
			END
		END

        SET @CombinedSections = CONCAT(@CombinedSections , @Section9)
	END

    SET @SQL = CONCAT(@SQL, '''' , @CombinedSections , '''')

	IF @Group IS NOT NULL 
		BEGIN
			SET @TempSQL = CONCAT(' AND ( tgroup = ''', @Group, ''' )ORDER BY ActivityNo, Loc_RecordID')
			SET @SQL = CONCAT(@SQL, @TempSQL)
		END
	ELSE
		BEGIN
			IF @ExcludedGroups Is Not NULL
				SET @SQL = CONCAT(@SQL, ' AND tgroup NOT IN (' , @ExcludedGroups , ')')

			SET @SQL = CONCAT(@SQL, ' ORDER BY Tgroup DESC, ActivityNo, Loc_RecordID ')
		END

	Print(@SQL)
	Exec(@SQL)
END

BEGIN
	Print 'Insert Activities Into #Results'

	SET @SQL = CONCAT('Insert Into #Results(RecordID,Loc_RecordID,ActivityNo,Tgroup,Site_APN,Site_Addr,Site_City)
	SELECT RecordID,Loc_RecordID,ActivityNo,Tgroup,Site_APN,Site_Addr,Site_City
	From tvw_GblActivities WHERE SITE_APN like ', '''' , @CombinedSections , '''')

	IF @ActivityGroupsOnly = 1
		BEGIN
			Print 'Insert Into #Results - @ActivityGroupsOnly = 1'
			SET @SQL = CONCAT(@SQL, ' AND ( tgroup in (''PERMIT'',''CASE'',''PROJECT'',''LICENSE2'',''PAYMENT'') )')

			If @ExcludedGroups Is Not NULL
			 BEGIN
				SET @TempSQL = CONCAT(@SQL, ' AND ( tgroup NOT IN (', @ExcludedGroups, ') )')
			 END

			Set @SQL = CONCAT(@SQL, ' ORDER BY Tgroup DESC, ActivityNo, Loc_RecordID')
		END
	ELSE
		BEGIN
			Print CONCAT('Insert Into #Results - Group = ' , @Group)
			IF @Group IS NOT NULL
			 BEGIN			
				SET @SQL = CONCAT(@SQL, ' AND ( tgroup = ''',@Group, ''' )ORDER BY ActivityNo, Loc_RecordID')

				Print(@SQL)
				Exec(@SQL)
			 END
			ELSE
			 BEGIN
			 	Print 'Insert Into #Results - Group Is Null'
			 	If @ExcludedGroups Is Not Null
					Set @SQL = CONCAT(@SQL, ' AND ( tgroup NOT IN (', @ExcludedGroups , ') )')

				Set @SQL = CONCAT(@SQL, ' ORDER BY Tgroup DESC, ActivityNo, Loc_RecordID ')

				Print(@SQL)
				Exec(@SQL)
			 END
			 	Set @SQL = CONCAT('Delete #Results Where Tgroup = ''GEO''
					AND Site_APN not like ''' , @CombinedSections , '''')
		END

	Print(@SQL)
	Exec(@SQL)
END

Set @SQL = CONCAT('
SELECT ' , @vTop , '
ACTIVITYNO = tr.ActivityNo,
TITLE, 
ACTIVITYTYPE, 
ACTIVITYSUBTYPE, 
SITE_APN = tr.Site_Apn, 
SITE_ADDR = tr.Site_Addr,
SITE_CITY = tr.Site_City,
OWNER_NAME,
SITE_SUBDIVISION, 
RECORDID = tr.RecordID, 
LOC_RECORDID = tr.Loc_RecordID, 
[STATUS], 
Tgroup = tr.Tgroup, 
x, 
y, 
GEOTYPE, 
SITE_ALTERNATE_ID, 
PARENT_PROJECT_NO, 
PARENT_PERMIT_NO, 
PARENT_SITE_APN, 
PARENT_RECORDID, 
PARENT_BUS_LIC_NO, 
PARENT_AEC_ST_LIC_NO, 
DatePropertyList, 
DATE1, 
DATE2, 
DATE3, 
DATE4, 
DATE5, 
DATE6, 
DATE7, 
DATE8,
tr.RecordType
From #Results tr
LEFT JOIN tvw_GblActivities va
 ON tr.RecordID = va.RecordID AND tr.Tgroup = va.Tgroup AND tr.RecordType = ''Activity'' AND tr.ActivityNo = va.ActivityNo
ORDER BY tr.Tgroup DESC, tr.ActivityNo, tr.Loc_RecordID
 ')
Print(@SQL)
Exec(@SQL)

END

GO
