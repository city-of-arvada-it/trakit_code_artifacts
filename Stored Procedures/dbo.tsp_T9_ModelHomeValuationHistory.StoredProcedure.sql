USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ModelHomeValuationHistory]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_ModelHomeValuationHistory]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ModelHomeValuationHistory]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  
CREATE PROCEDURE [dbo].[tsp_T9_ModelHomeValuationHistory]
             @PermitNO varchar(50)
AS
BEGIN


       if @PermitNO <> ''
              Begin
			  Select  Code, Description, isnull(optionName, 'BASE') OptionName, Quantity, UnitCost,Amount, RecordID, ModelRecordID, BuilderID from Permit_ModelHomeValuationHistory 
			  where Permit_No = @PermitNo					
			  order by code			


              end

END




GO
