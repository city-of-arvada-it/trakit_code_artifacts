USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ActivityAttachmentCount]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_ActivityAttachmentCount]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ActivityAttachmentCount]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  					  
					  					  					  



CREATE PROCEDURE [dbo].[tsp_ActivityAttachmentCount] (@Group varchar(40),@ActivityNo varchar(40),@SubGroup varchar(40)='',@SubGroupRecordID varchar(40)='', @TotalCount INT OUTPUT) 
--RETURNS INT
AS 
BEGIN 

	DECLARE @SQL Varchar(8000)
	DECLARE @WHERE Varchar(2000)
	DECLARE @TotalAttachmentsCount INT
	DECLARE @TableName varchar(40)
	DECLARE @ActivityNoField varchar(40)
	
	select @ActivityNoField =  CASE
		When PATINDEX('AEC%',@Group) = 1 then 'ST_LIC_NO'
		When PATINDEX('CASE%',@Group) = 1 then 'CASE_NO'
		When PATINDEX('LICENSE2%',@Group) = 1 then 'LICENSE_NO'
		when PATINDEX('PERMIT%',@Group) = 1 then 'PERMIT_NO'
		when PATINDEX('PROJECT%',@Group) = 1 then 'PROJECT_NO'
		when PATINDEX('GEO%',@Group) = 1 then 'LOC_RECORDID'
		End
		
	select @TableName =  CASE
		When PATINDEX('AEC%',@Group) = 1 then 'AEC_Attachments'
		When PATINDEX('CASE%',@Group) = 1 then 'CASE_Attachments'
		When PATINDEX('LICENSE2%',@Group) = 1 then 'LICENSE2_Attachments'
		when PATINDEX('PERMIT%',@Group) = 1 then 'Permit_Attachments'
		when PATINDEX('PROJECT%',@Group) = 1 then 'PROJECT_Attachments'
		when PATINDEX('GEO%',@Group) = 1 then 'GEO_Attachments'
		End
		
		
If @SubGroup Is Null Set @SubGroup = ''
If @SubGroupRecordID Is Null Set @SubGroupRecordID = ''




-- RB Drop table if it exists
IF OBJECT_ID('tempdb..#Results') IS NOT NULL  
	drop table #Results

Create Table #Results (TotalCount INT)

/* Handle Activity Level Attachment Counts */
If @SubGroup = '' AND @SubGroupRecordID = ''
 Begin
 
	Set @WHERE = ' 
	WHERE ' + @ActivityNoField + ' is not null and ' + @ActivityNoField + ' <> '''''
	
	Set @WHERE = @WHERE + ' AND ' + @ActivityNoField + ' = ''' + @ActivityNo + ''''
	
	 Set @SQL = '
	 Insert Into #Results(TotalCount)
	 Select TotalCount=Count(*) 
	 From ' + @TableName + '
	 ' + @WHERE + '
	 Group By ' + @ActivityNoField + '
	 '
	 Print(@SQL)
	 Exec(@SQL)
 
 End		
 
 Select @TotalCount = ( Select TotalCount From #Results )
  
		RETURN

		
   --Return @TotalAttachmentsCount

END















GO
