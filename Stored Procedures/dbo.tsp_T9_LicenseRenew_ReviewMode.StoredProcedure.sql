USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_LicenseRenew_ReviewMode]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_LicenseRenew_ReviewMode]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_LicenseRenew_ReviewMode]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  
CREATE PROCEDURE [dbo].[tsp_T9_LicenseRenew_ReviewMode](
	@PARAMETERSETID INT,
	@RENEW_DATE DateTime = GetDate,
	@USERID NVARCHAR(20) = 'CRW'
)
AS
BEGIN
	SET NOCOUNT ON;

-- LICENSE RENEWAL REVIEW (START)==========================================================================================================
--************************************************* Review - START *************************************************

	INSERT INTO LicenseRenew_EventLog (SourceMode,EventText,USERID) VALUES ('REVIEW', 'Review mode initiated.', @USERID)

--	GATHER LICENSE LIST
	EXEC tsp_t9_LicenseRenew_Preprocessor @PARAMETERSETID, @USERID


-- check and add column for reports-Renewal_Printed
IF NOT EXISTS( SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'License2_UDF' AND  COLUMN_NAME = 'Renewal_Printed')
	BEGIN
		ALTER TABLE LICENSE2_UDF ADD Renewal_Printed DateTime NULL
	END

-- Generate datea for balance forwards, for review tables
EXEC tsp_T9_LicenseRenew_CalculateBalanceForward 'LicenseRenew_Review_Fees', @USERID, @PARAMETERSETID
EXEC tsp_T9_LicenseRenew_CreateBalanceForwardFees 0, @PARAMETERSETID

 
--************************************************* Review - *****************************************************
-- the LicenseRenew_Review_Main can actually be pulled from a different table:
--		Licenses_To_Renew - this way they can be batched monthly or yearly, quarterly, etc.
--		This table is always cleared after commit.
-- Review Mode now has a pre-review process and final process, this one is REVIEWa instead of REVIEW
-- this whole bit should be a tsp_T9_LicenseRenew_Parameter_Execution... or something like that.
-----------------------------------------------------------------------------------------------------------------------------------------------------
DECLARE @PCount INT = (SELECT COUNT(*) FROM LicenseRenew_License_Parameters WHERE ParameterSetID = @ParameterSetID AND ActiveMode = 'REVIEWa')

IF @PCount > 0
	BEGIN
		INSERT INTO LicenseRenew_EventLog (SourceMode,EventText,USERID) VALUES ('REVIEWa', 'Executing pre-processing conditions for review.',@USERID)
		EXEC tsp_t9_LicenseRenew_ParameterProcessor @ParameterSetID, 'REVIEWa', @USERID
	END


--A. Only 'ACTIVE' licenses are in here, previously filtered to the correct list
UPDATE LicenseRenew_Review_Fees SET PAID = 0
UPDATE LicenseRenew_Review_SubFees SET PAID = 0
UPDATE LicenseRenew_Review_Fees SET PAID_AMOUNT = 0


-- D1. Fee Schedule List

-- remove prvious failures
TRUNCATE TABLE LicenseRenew_Process_Exceptions

DECLARE @SCHEDULE_EFFECTIVE_DATE as DATETIME
DECLARE @EFFECTIVE_FEE_SCHEDULE as varchar(200)

SET @SCHEDULE_EFFECTIVE_DATE = (SELECT MAX(EFFECTIVE_ON) FROM Prmry_FeeScheduleList WHERE Prmry_FeeScheduleList.EFFECTIVE_ON < @RENEW_DATE AND TEST <> 1)
SET @EFFECTIVE_FEE_SCHEDULE = (SELECT TOP 1 Prmry_FeeScheduleList.SCHEDULE_NAME FROM Prmry_FeeScheduleList WHERE TEST <> 1 AND EFFECTIVE_ON = @SCHEDULE_EFFECTIVE_DATE or TEST <> 1 AND EFFECTIVE_ON is null )

-- IF effective dates are NULL, then use current, since it is the default
IF @SCHEDULE_EFFECTIVE_DATE IS NULL
	BEGIN
		SET @SCHEDULE_EFFECTIVE_DATE = @RENEW_DATE
		SET @EFFECTIVE_FEE_SCHEDULE = 'Default'
	END

--D2. Move unmatched Fees info to exception table
INSERT INTO LicenseRenew_Process_Exceptions 
	SELECT GETDATE() AS [ProcDate], 
		LICENSE_NO AS [LICENSE_NO], 
		'FeeCode ' + LRF.CODE + ' not in fee schedule for renewal period beginning ' + cast(@RENEW_DATE as varchar) AS [DESC]
		FROM LicenseRenew_Review_Fees AS LRF
		WHERE LRF.CODE NOT IN (select PFS.CODE from Prmry_FeeSchedule PFS where SCHEDULE_NAME = @EFFECTIVE_FEE_SCHEDULE and nullif(PARENT_CODE,'') is null)
		
--D3. Remove excluded Fees from LicenseRenew_Review_Fees
DELETE FROM LicenseRenew_Review_Fees
WHERE LicenseRenew_Review_Fees.CODE NOT IN (select PFS.CODE from Prmry_FeeSchedule PFS where SCHEDULE_NAME = @EFFECTIVE_FEE_SCHEDULE)

--D4. Move unmatched SubFee Codes to exclusion table
INSERT INTO LicenseRenew_Process_Exceptions
	SELECT GETDATE() as [ProcDate],
	LICENSE_NO AS [LICENSE_NO],
	'SubFeeCode ' + LRSF.ITEM + ' not in fee schedule for renewal period beginning ' + cast(@RENEW_DATE as varchar)AS [DESC]
	FROM LicenseRenew_Review_SubFees LRSF
	WHERE LRSF.ITEM NOT IN (select PFS.CODE from Prmry_FeeSchedule PFS where SCHEDULE_NAME = @EFFECTIVE_FEE_SCHEDULE and PFS.PARENT_CODE <>lrsf.ITEM)

--D5. Remove excluded Fees from LicenseRenew_Review_SubFees
DELETE FROM LicenseRenew_Review_SubFees
WHERE LicenseRenew_Review_SubFees.ITEM NOT IN (select PFS.CODE from Prmry_FeeSchedule PFS where SCHEDULE_NAME = @EFFECTIVE_FEE_SCHEDULE)

-- rrr 7/31/2014 - Also should remove record from License_Main and LicenseRenew_License_List and insert into LicenseRenew_ExcludedLicense_List
----------------------------------------------------------------------------------------------------------------------------------------------
DELETE FROM LicenseRenew_Review_Main
WHERE LicenseRenew_Review_Main.LICENSE_NO IN (select distinct LICENSE_NO from LicenseRenew_Process_Exceptions)

DELETE FROM LicenseRenew_License_List 
WHERE LicenseRenew_License_List.LICENSE_NO IN (select distinct LICENSE_NO from LicenseRenew_Process_Exceptions)

----//MDN remove FEE and SUBFEE records that are being excluded as well
DELETE FROM LicenseRenew_Review_Fees
WHERE LicenseRenew_Review_Fees.LICENSE_NO IN (select distinct LICENSE_NO from LicenseRenew_Process_Exceptions)

DELETE FROM LicenseRenew_Review_SubFees
WHERE LicenseRenew_Review_SubFees.LICENSE_NO IN (select distinct LICENSE_NO from LicenseRenew_Process_Exceptions)


INSERT INTO LicenseRenew_ExcludedLicense_List (LICENSE_NO)
select distinct LICENSE_NO from LicenseRenew_Process_Exceptions
----------------------------------------------------------------------------------------------------------------------------------------------

-- D7. Update Formula in Review_Fees table and clear amount
UPDATE LicenseRenew_Review_Fees
SET LicenseRenew_Review_Fees.FORMULA = Prmry_FeeSchedule.FORMULA,
	LicenseRenew_Review_Fees.AMOUNT = Null
FROM LicenseRenew_Review_Fees INNER JOIN Prmry_FeeSchedule ON
		(LicenseRenew_Review_Fees.CODE = Prmry_FeeSchedule.CODE)
WHERE Prmry_FeeSchedule.SCHEDULE_NAME = @EFFECTIVE_FEE_SCHEDULE and parent_code is null

-- D8. Update Formula in Review_SubFees table and clear amount
UPDATE LicenseRenew_Review_SubFees
SET LicenseRenew_Review_SubFees.FORMULA = Prmry_FeeSchedule.FORMULA,
	LicenseRenew_Review_SubFees.AMOUNT = Null
FROM LicenseRenew_Review_SubFees INNER JOIN Prmry_FeeSchedule ON
		(LicenseRenew_Review_SubFees.ITEM = Prmry_FeeSchedule.CODE)
WHERE Prmry_FeeSchedule.SCHEDULE_NAME = @EFFECTIVE_FEE_SCHEDULE and Prmry_FeeSchedule.parent_code=LicenseRenew_Review_SubFees.code

-- make with the calculations
EXEC [tsp_T9_LicenseRenew_Calculations]


DECLARE @NoType INT = (SELECT COUNT(*) FROM LicenseRenew_Review_Main WHERE license_type NOT IN (SELECT typename FROM prmry_types WHERE groupname = 'license2'))

IF @NoType > 0
	BEGIN
		INSERT INTO LicenseRenew_EventLog (SourceMode,EventText,USERID) VALUES ('REVIEW - warning', CONVERT(NVARCHAR(20), @NoType) + ' licenses do not have a License Type in prmry_types. They will not be able to get an EXPIRED date. See LicenseRenew_Process_Exceptions for details.',@USERID)

		-- ADD PROCESS EXCEPTIONS FOR THESE LICENSES
		INSERT INTO LicenseRenew_Process_Exceptions SELECT getdate() as PROCESSING_DATE, License_No, License_type + ' is not in table prmry_types' AS EXCEPTION_DESC FROM LicenseRenew_Review_Main WHERE license_type NOT IN (SELECT typename FROM prmry_types WHERE groupname = 'license2')
	END

------------------------ FINAL PROCESSING (START)--------------------------------------------------------
DECLARE @ParamCount INT = (SELECT COUNT(*) FROM LicenseRenew_License_Parameters WHERE ParameterSetID = @ParameterSetID AND ActiveMode = 'REVIEW')

IF @ParamCount > 0
	BEGIN
		INSERT INTO LicenseRenew_EventLog (SourceMode,EventText,USERID) VALUES ('REVIEW', 'Executing final processing conditions for review.',@USERID)
		EXEC tsp_t9_LicenseRenew_ParameterProcessor @ParameterSetID, 'REVIEW', @USERID
	END


end






GO
