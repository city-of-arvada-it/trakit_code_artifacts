USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_t9_CheckListRulesAddUpdate]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_t9_CheckListRulesAddUpdate]
GO
/****** Object:  StoredProcedure [dbo].[tsp_t9_CheckListRulesAddUpdate]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  

                             
create Procedure [dbo].[tsp_t9_CheckListRulesAddUpdate]
       @List as CheckListRulesType readonly
as
Begin

       --For now always delete everything for this individual group / sub group etc combination, then re-insert.
       --This will clear out any non-existing rules for this update as well.
       Delete R from ChecklistRules R Join @List L on L.GroupID = R.GroupID And L.GroupType = R.GroupType 
       and L.SubGroupID =   R.SubGroupID and L.SubGroupType = R.SubGroupType 
       

       Insert into CheckListRules(GroupID, GroupType, SubGroupID, SubGroupType, StaticRuleID, RuleValue)
       Select GroupID, GroupType, SubGroupID, SubGroupType, StaticRuleID, RuleValue from @List where CLRuleID = 0
       
       


End





GO
