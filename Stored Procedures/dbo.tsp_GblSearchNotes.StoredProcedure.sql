USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GblSearchNotes]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_GblSearchNotes]
GO
/****** Object:  StoredProcedure [dbo].[tsp_GblSearchNotes]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[tsp_GblSearchNotes](  
@Top INT = 0,  
@ActivityGroup Varchar(50),  
@ActivityNo Varchar(30),  
@SubGroup Varchar(50) = Null,  
@SubGroupRecordID Varchar(30) = Null,  
@UserID Varchar(6) = Null,  
@NoteID INT = Null,  
@SubGroupOverride INT = 0,  
@NoteSearchText Varchar(100) = Null,  
@CountOnly INT = 0  
)  
  
As  
  
Declare @SQL Varchar(8000)  
  
  
If @Top Is Null Set @Top = 0  
If @NoteID < 0 Set @NoteID = Null  
  
  
Create Table #Results(  
NoteID INT,  
UserID Varchar(6),  
UserName Varchar(128),  --GP:12-18-2018 - Changed Varchar(30) >> Varchar(128) which matched with Prmry_Users table.
ActivityNo Varchar(30),  
SubGroupRecordID Varchar(30) Default Null,  
Notes Varchar(8000),   --rrr 9-24-2014  - changed from 7500 length to 8000  
DateEntered Datetime,  
ActivityGroup Varchar(50),  
SubGroup Varchar(50),  
FlagForSelection INT Default 0,  
attachmentRecID Varchar(50),  
markUpID Varchar(50),  
markupEXT Varchar(250),  
markupName Varchar(50),  
TSAnnoID INT  Default -1  
)  
  
  
Set @SQL = 'Insert Into #Results(NoteID,UserID,ActivityNo,SubGroupRecordID,Notes,DateEntered,ActivityGroup,SubGroup,attachmentRecID,markUpID,markupEXT,markupName,TSAnnoID)'  
  
If @Top > 0 Set @SQL = @SQL + 'Select Top ' + CONVERT(Varchar,@Top) + ' '  
If @Top < 1 Set @SQL = @SQL + 'Select '  
  
Set @SQL = @SQL + 'NoteID,UserID,ActivityNo,SubGroupRecordID,Notes,DateEntered,ActivityGroup,SubGroup,attachmentRecID,markUpID,markupEXT,markupName,TSAnnoID  
From Prmry_Notes  
WHERE ActivityGroup = ''' + @ActivityGroup + ''' AND ActivityNo = ''' + @ActivityNo + ''''  
  
If @SubGroup Is Not Null   
 Begin  
 If @SubGroup IN ('Action','Chronology')  
  Set @SQL = @SQL + ' AND SubGroup IN (''Action'',''Chronology'')'   
 ELSE  
  Set @SQL = @SQL + ' AND SubGroup = ''' + @SubGroup + ''''  
 End  
If @SubGroupRecordID Is Not Null Set @SQL = @SQL + ' AND SubGroupRecordID = ''' + @SubGroupRecordID + ''''  
If @UserID Is Not Null Set @SQL = @SQL + ' AND UserID = ''' + @UserID + ''''  
If @NoteID Is Not Null Set @SQL = @SQL + ' AND NoteID = ''' + Convert(Varchar,@NoteID) + ''''  
  
  
If @SubGroup Is Null AND @SubGroupOverride = 0   
 Set @SQL = @SQL + '   
 AND SubGroup Is Null'  
  
  
Print(@SQL)  
Exec(@SQL)  
  
Update ts Set  
UserName = pu.UserName  
From #Results ts  
JOIN Prmry_Users pu  
 ON ts.UserID = pu.UserID  
   
   
If @CountOnly = 1  
 Begin  
 Select Total=Count(*) From #Results   
 RETURN  
 End  
   
If @NoteSearchText Is Not Null  
 Begin  
   
 Set @SQL = '  
 Update ts Set  
 FlagForSelection = 1  
 From #Results ts  
 WHERE Notes Like ''%' + @NoteSearchText + '%''  
 '  
 Print(@SQL)  
 Exec(@SQL)  
  
 Select * From #Results WHERE FlagForSelection = 1 Order By DateEntered   
 End  
ELSE  
 Begin  
 Select * From #Results Order By DateEntered   
 END
GO
