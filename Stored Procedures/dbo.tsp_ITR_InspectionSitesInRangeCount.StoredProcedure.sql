USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ITR_InspectionSitesInRangeCount]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_ITR_InspectionSitesInRangeCount]
GO
/****** Object:  StoredProcedure [dbo].[tsp_ITR_InspectionSitesInRangeCount]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  


                                
-- =============================================
-- Author:        Micah Neveu, built from tsp_ITR_INSPECTIONSITESINRANGE
-- Create date: 4/22/2014
-- Description:   Used for ITrakit Inspections, returns a throttled back list of records
--                      ordered by distance from CENTER of provided selection points
-- Change history:
--                      4/25/2014, commented out dates below marked CHANGED1
-- =====================================================================  
CREATE PROCEDURE [dbo].[tsp_ITR_InspectionSitesInRangeCount] (
      -- Add the parameters for the stored procedure here
--@x FLOAT,
--@y FLOAT,
--@d FLOAT

 @xN DECIMAL(18,10),          -- Latitude Northern boundary
@xS DECIMAL(18,10),          -- Latitude Southern boundary
@yW DECIMAL(18,10),          -- Longitude Western boundary
@yE DECIMAL(18,10),    -- Longitude Eastern boundary
@Samp INT
)

AS
BEGIN
      -- SET NOCOUNT ON added to prevent extra result sets from
      -- interfering with SELECT statements.
      SET NOCOUNT ON;
      
      SELECT DISTINCT loc_recordid,
            --((@xN - @xS)* .5) + @xS as LatMid,
            --@yE - ((@yE - @yW) * .5) As LonMid,
            --LAT,
            --ABS(LAT - (((@xN - @xS)* .5) + @xS)) As LatDiff,
            --LON,
            --ABS(LON - (@yE - ((@yE - @yW) * .5))) As LonDiff,
            ABS(LAT - (((@xN - @xS)* .5) + @xS)) + ABS(LON - (@yE - ((@yE - @yW) * .5))) As TotDiff,
            ROW_NUMBER() OVER(ORDER BY ABS(LAT - (((@xN - @xS)* .5) + @xS)) + ABS(LON - (@yE - ((@yE - @yW) * .5)))) as Ranked 
            INTO #tempActivities
      FROM tvw_ITR_Activities
      WHERE (LAT <= @xN AND LAT >= @xS) 
            AND (LON >= @yW AND LON <= @yE) and (ABS(LAT - (((@xN - @xS)* .5) + @xS)) + ABS(LON - (@yE - ((@yE - @yW) * .5)))) <= 100
      
      
      -- Cases
      SELECT /*TOP(@Samp)*/ vw.loc_recordid, vw.Tgroup, vw.recordid, ACTIVITYNO, vw.site_apn, vw.site_addr, vw.site_city, vw.site_state, vw.site_zip, 
            vw.geotype, LAT, LON, ISNULL(x,0)as x, ISNULL(y,0)as y, RESTRICTIONS.RECORDID as restriction_id, RESTRICTIONS.DATE_ADDED as restriction_add_date,
            RESTRICTIONS.RESTRICTION_TYPE as restriction_type, RESTRICTIONS.RESTRICTION_NOTES as restriction_notes, RESTRICTIONS.OTHER_NOTES as restriction_notes2, 
            DATE1, DATE2, DATE3, DATE4, DATE5, DATE6, DATE7, vw.DatePropertyList,
            vw.status AS [actStatus], ACTIVITYTYPE AS actType, ACTIVITYSUBTYPE as actSubType,
            actMain.CASE_NAME AS [DESCRIPTION], '' AS actJobValue, actMain.SITE_LOT_NO, actMain.SITE_BLOCK, actMain.SITE_SUBDIVISION, actMain.SITE_TRACT,
            actFees.BALANCE_DUE as feeBalance, actInsp.InspectionType, '' AS Company
            
            INTO #tempResults
            
      FROM tvw_ITR_Activities vw INNER JOIN #tempActivities temp ON (vw.loc_recordid = temp.loc_recordid)
            LEFT JOIN CASE_MAIN actMain ON actMain.CASE_NO = vw.ACTIVITYNO
            LEFT JOIN CASE_MAIN actFees ON actFees.CASE_NO = vw.ACTIVITYNO
            LEFT JOIN CASE_INSPECTIONS actInsp on vw.ACTIVITYNO = actInsp.CASE_NO
            LEFT JOIN (select RECORDID, DATE_ADDED, DATE_CLEARED, RESTRICTION_TYPE, RESTRICTION_NOTES, OTHER_NOTES, LOC_RECORDID
                              from Geo_RESTRICTIONS2
                              where DATE_CLEARED IS NULL AND RESTRICTION_NOTES NOT LIKE 'VOIDED (%'   
                           UNION
                           select RECORDID, '' AS DATE_ADDED, '' AS DATE_CLEARED, RESTRICTION_TYPE, RESTRICTION_NOTES, OTHER_NOTES, LOC_RECORDID
                            from Geo_RESTRICTIONS 
                            where RESTRICTION_TYPE <> 'NONE') AS RESTRICTIONS ON RESTRICTIONS.LOC_RECORDID = vw.loc_recordid
      WHERE Tgroup = 'CASE' 
            AND (DATE5 IS NOT NULL AND DATE5 <> '' AND IsDate(DATE5) = 1)
            AND (DATE1 IS NULL OR DATE1 = '')
      UNION
      -- Permits
      SELECT /*TOP(@Samp)*/ vw.loc_recordid, vw.Tgroup, vw.recordid, ACTIVITYNO, vw.site_apn, vw.site_addr, vw.site_city, vw.site_state, vw.site_zip, 
            vw.geotype, LAT, LON, ISNULL(x,0)as x, ISNULL(y,0)as y, RESTRICTIONS.RECORDID as restriction_id, RESTRICTIONS.DATE_ADDED as restriction_add_date,
            RESTRICTIONS.RESTRICTION_TYPE as restriction_type, RESTRICTIONS.RESTRICTION_NOTES as restriction_notes, RESTRICTIONS.OTHER_NOTES as restriction_notes2,
            DATE1, DATE2, DATE3, DATE4, DATE5, DATE6, DATE7, vw.DatePropertyList,
            vw.status AS [actStatus], ACTIVITYTYPE AS actType, ACTIVITYSUBTYPE as actSubType,
            actMain.DESCRIPTION, actMain.JOBVALUE AS actJobValue, actMain.SITE_LOT_NO, actMain.SITE_BLOCK, actMain.SITE_SUBDIVISION, actMain.SITE_TRACT,
            actFees.BALANCE_DUE as feeBalance, actInsp.InspectionType, '' AS Company
      FROM tvw_ITR_Activities vw INNER JOIN #tempActivities temp ON (vw.loc_recordid = temp.loc_recordid)
            LEFT JOIN PERMIT_MAIN actMain ON actMain.PERMIT_NO = vw.ACTIVITYNO
            LEFT JOIN Permit_Main actFees ON actFees.PERMIT_NO = vw.ACTIVITYNO
            LEFT JOIN Permit_Inspections actInsp on vw.ACTIVITYNO = actInsp.PERMIT_NO
            LEFT JOIN (select RECORDID, DATE_ADDED, DATE_CLEARED, RESTRICTION_TYPE, RESTRICTION_NOTES, OTHER_NOTES, LOC_RECORDID
                              from Geo_RESTRICTIONS2
                              where DATE_CLEARED IS NULL AND RESTRICTION_NOTES NOT LIKE 'VOIDED (%'   
                           UNION
                           select RECORDID, '' AS DATE_ADDED, '' AS DATE_CLEARED, RESTRICTION_TYPE, RESTRICTION_NOTES, OTHER_NOTES, LOC_RECORDID
                            from Geo_RESTRICTIONS 
                            where RESTRICTION_TYPE <> 'NONE') AS RESTRICTIONS ON RESTRICTIONS.LOC_RECORDID = vw.loc_recordid
      WHERE Tgroup = 'PERMIT' 
            --AND (DATE2 IS NULL OR DATE2 = '')  --4/25/2014 CHANGED1
            -- rrr 6/5/13 - Adminitrack Issue No. 21386 - Include those that are issued, not those approved
            --AND (DATE2 IS NOT NULL AND DATE2 <> '' AND ISDATE(DATE2) = 1) 
            AND (DATE4 IS NULL OR DATE4 = '')
            AND (DATE3 IS NULL OR DATE3 = '' OR GETDATE() < Date3)
      UNION 
      -- Projects
      SELECT /*TOP(@Samp)*/ vw.loc_recordid, vw.Tgroup, vw.recordid, ACTIVITYNO, vw.site_apn, vw.site_addr, vw.site_city, vw.site_state, vw.site_zip, 
            vw.geotype, LAT, LON, ISNULL(x,0)as x, ISNULL(y,0)as y, RESTRICTIONS.RECORDID as restriction_id, RESTRICTIONS.DATE_ADDED as restriction_add_date,
            RESTRICTIONS.RESTRICTION_TYPE as restriction_type, RESTRICTIONS.RESTRICTION_NOTES as restriction_notes, RESTRICTIONS.OTHER_NOTES as restriction_notes2,
            DATE1, DATE2, DATE3, DATE4, DATE5, DATE6, DATE7, vw.DatePropertyList,
            vw.status AS [actStatus], ACTIVITYTYPE AS actType, ACTIVITYSUBTYPE as actSubType,
            actMain.PROJECT_NAME AS [DESCRIPTION], '' AS actJobValue, actMain.SITE_LOT_NO, actMain.SITE_BLOCK, actMain.SITE_SUBDIVISION, actMain.SITE_TRACT,
            actFees.BALANCE_DUE as feeBalance, actInsp.InspectionType, '' AS Company
      FROM tvw_ITR_Activities vw INNER JOIN #tempActivities temp ON (vw.loc_recordid = temp.loc_recordid)
            LEFT JOIN PROJECT_MAIN actMain ON actMain.PROJECT_NO = vw.ACTIVITYNO
            LEFT JOIN Project_Main actFees ON actFees.PROJECT_NO = vw.ACTIVITYNO
            LEFT JOIN PROJECT_INSPECTIONS actInsp on vw.ACTIVITYNO = actInsp.PROJECT_NO
            LEFT JOIN (select RECORDID, DATE_ADDED, DATE_CLEARED, RESTRICTION_TYPE, RESTRICTION_NOTES, OTHER_NOTES, LOC_RECORDID
                              from Geo_RESTRICTIONS2
                              where DATE_CLEARED IS NULL AND RESTRICTION_NOTES NOT LIKE 'VOIDED (%'   
                           UNION
                           select RECORDID, '' AS DATE_ADDED, '' AS DATE_CLEARED, RESTRICTION_TYPE, RESTRICTION_NOTES, OTHER_NOTES, LOC_RECORDID
                            from Geo_RESTRICTIONS 
                            where RESTRICTION_TYPE <> 'NONE') AS RESTRICTIONS ON RESTRICTIONS.LOC_RECORDID = vw.loc_recordid
      WHERE Tgroup = 'PROJECT' 
            AND (DATE3 IS NULL OR DATE3 = '')
      UNION 
      -- License1
            SELECT /*TOP(@Samp)*/ vw.loc_recordid, vw.Tgroup, vw.recordid, ACTIVITYNO, vw.site_apn, vw.site_addr, vw.site_city, vw.site_state, vw.site_zip, 
            vw.geotype, LAT, LON, ISNULL(x,0)as x, ISNULL(y,0)as y, RESTRICTIONS.RECORDID as restriction_id, RESTRICTIONS.DATE_ADDED as restriction_add_date,
            RESTRICTIONS.RESTRICTION_TYPE as restriction_type, RESTRICTIONS.RESTRICTION_NOTES as restriction_notes, RESTRICTIONS.OTHER_NOTES as restriction_notes2,
            DATE1, DATE2, DATE3, DATE4, DATE5, DATE6, DATE7, vw.DatePropertyList,
            vw.status AS [actStatus], ACTIVITYTYPE AS actType, ACTIVITYSUBTYPE as actSubType,
            licBus.SITE_DESCRIPTION AS [DESCRIPTION], '' AS actJobValue, licBus.SITE_LOT_NO, licBus.SITE_BLOCK, licBus.SITE_SUBDIVISION, licBus.SITE_TRACT,
            (actFees.AMOUNT - actFees.PAID_AMOUNT) as feeBalance, actInsp.InspectionType, licBus.Company AS Company
      FROM tvw_ITR_Activities vw INNER JOIN #tempActivities temp ON (vw.loc_recordid = temp.loc_recordid)
            LEFT JOIN LICENSE_MAIN actMain ON actMain.BUS_LIC_NO = vw.ACTIVITYNO
            LEFT JOIN License_Business licBus ON licBus.BUSINESS_NO = actMain.BUSINESS_NO
            LEFT JOIN LICENSE_FEES actFees ON actFees.BUS_LIC_NO = vw.ACTIVITYNO
            LEFT JOIN LICENSE_INSPECTIONS actInsp on vw.ACTIVITYNO = actInsp.BUS_LIC_NO
            LEFT JOIN (select RECORDID, DATE_ADDED, DATE_CLEARED, RESTRICTION_TYPE, RESTRICTION_NOTES, OTHER_NOTES, LOC_RECORDID
                              from Geo_RESTRICTIONS2
                              where DATE_CLEARED IS NULL AND RESTRICTION_NOTES NOT LIKE 'VOIDED (%'   
                           UNION
                           select RECORDID, '' AS DATE_ADDED, '' AS DATE_CLEARED, RESTRICTION_TYPE, RESTRICTION_NOTES, OTHER_NOTES, LOC_RECORDID
                            from Geo_RESTRICTIONS 
                            where RESTRICTION_TYPE <> 'NONE') AS RESTRICTIONS ON RESTRICTIONS.LOC_RECORDID = vw.loc_recordid
      WHERE Tgroup = 'LICENSE'  
            AND (DATE2 IS NULL OR DATE2 = '' OR DATE2 > GETDATE())
      UNION
      --License2
      SELECT /*TOP(@Samp)*/ vw.loc_recordid, vw.Tgroup, vw.recordid, ACTIVITYNO, vw.site_apn, vw.site_addr, vw.site_city, vw.site_state, vw.site_zip, 
            vw.geotype, LAT, LON, ISNULL(x,0)as x, ISNULL(y,0)as y, RESTRICTIONS.RECORDID as restriction_id, RESTRICTIONS.DATE_ADDED as restriction_add_date,
            RESTRICTIONS.RESTRICTION_TYPE as restriction_type, RESTRICTIONS.RESTRICTION_NOTES as restriction_notes, RESTRICTIONS.OTHER_NOTES as restriction_notes2,
            DATE1, DATE2, DATE3, DATE4, DATE5, DATE6, DATE7, vw.DatePropertyList,
            vw.status AS [actStatus], ACTIVITYTYPE AS actType, ACTIVITYSUBTYPE as actSubType,
            actMain.SITE_DESCRIPTION AS [DESCRIPTION], '' AS actJobValue, actMain.SITE_LOT_NO, actMain.SITE_BLOCK, actMain.SITE_SUBDIVISION, actMain.SITE_TRACT,
            actFees.BALANCE_DUE as feeBalance, actInsp.InspectionType, vw.TITLE As Company
      FROM tvw_ITR_Activities vw INNER JOIN #tempActivities temp ON (vw.loc_recordid = temp.loc_recordid)
      LEFT JOIN LICENSE2_MAIN actMain ON actMain.LICENSE_NO = vw.ACTIVITYNO
            LEFT JOIN LICENSE2_MAIN actFees ON actFees.LICENSE_NO = vw.ACTIVITYNO
            LEFT JOIN LICENSE2_INSPECTIONS actInsp on vw.ACTIVITYNO = actInsp.LICENSE_NO
            LEFT JOIN (select RECORDID, DATE_ADDED, DATE_CLEARED, RESTRICTION_TYPE, RESTRICTION_NOTES, OTHER_NOTES, LOC_RECORDID
                              from Geo_RESTRICTIONS2
                              where DATE_CLEARED IS NULL AND RESTRICTION_NOTES NOT LIKE 'VOIDED (%'   
                           UNION
                           select RECORDID, '' AS DATE_ADDED, '' AS DATE_CLEARED, RESTRICTION_TYPE, RESTRICTION_NOTES, OTHER_NOTES, LOC_RECORDID
                            from Geo_RESTRICTIONS 
                            where RESTRICTION_TYPE <> 'NONE') AS RESTRICTIONS ON RESTRICTIONS.LOC_RECORDID = vw.loc_recordid
      WHERE Tgroup = 'LICENSE2'   
            AND (DATE2 IS NULL OR DATE2 = '' OR DATE2 > GETDATE())

      
      ORDER BY loc_recordid, Tgroup, ACTIVITYNO, restriction_id
      
      if (SELECT COUNT(*) FROM #tempResults) <= @Samp
            BEGIN
                  SELECT * FROM #tempResults
            END
      ELSE
            BEGIN
                  --SELECT COUNT(*) as TotalCount FROM #tempResults

                  ALTER TABLE #tempResults ADD dist decimal(18,10)
                  
                  DECLARE @xCenter DECIMAL(18,10)
                  DECLARE @yCenter DECIMAL(18,10)
                  
                  SET @xCenter = 0.5 * (@xN + @xS)
                  SET @yCenter = 0.5 * (@yW + @yE)
                  
                  UPDATE #tempResults SET dist = SQRT((@xCenter - LAT)*(@xCenter - LAT) + (@yCenter - LON)*(@yCenter - LON))
                  
                  SELECT TOP(@Samp) * FROM #tempResults ORDER BY dist
                  
            END         
END




GO
