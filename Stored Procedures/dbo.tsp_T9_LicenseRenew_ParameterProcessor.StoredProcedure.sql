USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_LicenseRenew_ParameterProcessor]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_LicenseRenew_ParameterProcessor]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_LicenseRenew_ParameterProcessor]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  
					  
-- =============================================
-- Author:		Micah Neveu
-- create date: 9/2/2014
-- Description:	Processes specific parameters based on parameter set id and active mode
-- =============================================
CREATE PROCEDURE [dbo].[tsp_T9_LicenseRenew_ParameterProcessor] 
	-- Add the parameters for the stored procedure here
	@PARAMETERSETID INT,
	@ACTIVEMODE NVARCHAR(20),
	@USERID NVARCHAR(20) = 'CRW' 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	

DECLARE @PCount INT = (SELECT COUNT(*) FROM LicenseRenew_License_Parameters WHERE ParameterSetID = @ParameterSetID AND ActiveMode = @ACTIVEMODE)

IF @PCount > 0
	BEGIN
		INSERT INTO LicenseRenew_EventLog (SourceMode,EventText,USERID) VALUES (@ACTIVEMODE, 'Executing parameters...',@USERID)
	END

DECLARE @actionParam VARCHAR(2048)
DECLARE paramCursor CURSOR
LOCAL SCROLL STATIC
FOR
	SELECT ParameterValue FROM LicenseRenew_License_Parameters WHERE ParameterSetID = @ParameterSetID AND ActiveMode = @ACTIVEMODE
	OPEN paramCursor
	FETCH NEXT FROM paramCursor INTO @actionParam
	WHILE @@FETCH_STATUS = 0
	BEGIN
		EXEC (@actionParam)
		INSERT INTO LicenseRenew_EventLog (SourceMode,EventText,USERID) VALUES (@ACTIVEMODE, 'Processed Command: ' + @actionParam, @USERID)
		FETCH NEXT FROM paramCursor INTO @actionParam
	END
CLOSE paramCursor
DEALLOCATE paramCursor

END






GO
