USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[ALP_GetUdfInfoForLicenseNumbers]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[ALP_GetUdfInfoForLicenseNumbers]
GO
/****** Object:  StoredProcedure [dbo].[ALP_GetUdfInfoForLicenseNumbers]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================
-- Author:		Erick Hampton
-- Revised:		Feb 15, 2017
-- Description:	Gets UDF info for a supplied list of license numbers
-- ==============================================================================
CREATE PROCEDURE [dbo].[ALP_GetUdfInfoForLicenseNumbers]

	@LicenseNumbers [ALP_LicenseNumberList] READONLY

AS
BEGIN

	select list.LICENSE_NO [LicenseNumber], udf.Caption, udf.UDF_NAME, udf.DataType, udf.UDF_VALUE 
	from @LicenseNumbers list 
		left join [ALP_UdfInfoForLicenses] udf on list.LICENSE_NO = udf.LicenseNumber
	
END

GO
