USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_LicenseRenew_CreateBalanceForwardFees]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_LicenseRenew_CreateBalanceForwardFees]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_LicenseRenew_CreateBalanceForwardFees]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  -- =============================================
-- Author:		<Micah Neveu>
-- Create date: <12/11/2014>
-- Description:	<Adds the balance forward fee for each record in the balance forwards table>
-- =============================================
CREATE PROCEDURE [dbo].[tsp_T9_LicenseRenew_CreateBalanceForwardFees] 
		@INLIVE INT,
		@ParameterSetID INT = 0	
AS
BEGIN
	SET NOCOUNT ON;
	
	-- add new balance forwards
	-- for those that exist in the balance forward, but do not have an entry for the license
	DECLARE @BALFWDCODE NVARCHAR(12)
	SET @BALFWDCODE = (SELECT ParameterValue FROM LicenseRenew_License_Parameters WHERE ActiveMode = 'BALANCE' AND ParameterSetID = @ParameterSetID)

	DECLARE @SQL NVARCHAR(max)
	DECLARE @FEES_TABLE_NAME NVARCHAR(600) = 'LicenseRenew_Review_Fees'
	
	IF @INLIVE = 1
	BEGIN
		SET @FEES_TABLE_NAME = 'License2_Fees'
		-- add the Calc_Value
		ALTER TABLE License2_Fees ADD Calc_Value FLOAT NULL
	END
	
	SET @SQL='
		insert into ' + @FEES_TABLE_NAME +' 
			(  [RECORDID],license_no,[CODE],[DESCRIPTION],[DETAIL],[FEETYPE],[AMOUNT],[ASSESSED_DATE],[PAID],[QUANTITY], [Calc_Value])
			 SELECT ''GLR:'' + right((''00'' + str(DATEPART(yy, GETDATE()))),2)
			 + right((''00'' + ltrim(str(DATEPART(mm, GETDATE())))),2)
			 + right((''00'' + ltrim(str(DATEPART(d, GETDATE())))),2)
			 + right((''00'' + ltrim(str(DATEPART(hh, GETDATE())))),2)
			 + right((''00'' + ltrim(str(DATEPART(n, GETDATE())))),2)
			 + right((''00'' + ltrim(str(DATEPART(ss, GETDATE())))),2)
			 + right((''000000''+ CONVERT(varchar,ROW_NUMBER() OVER (ORDER BY (SELECT NULL))+111)),6) RECid,
			 tbl.* from
			 (select license_no,''' + @BALFWDCODE + ''' as  code, cast(DatePart(year,getdate()) as varchar)+'' Balance Forward'' as description,''0'' 
			 as detail,
			 ''FEE'' as feetype,[BF_Amount], getdate() as assest_date,''0'' as paid,''1'' as quantity, [BF_Amount] as Calc_Value
			 from [dbo].[LicenseRenew_BalanceForwards] where license_no not in (select license_no from ' + @FEES_TABLE_NAME + ' where 
			 code=''' + @BALFWDCODE + ''' and paid=0)) as tbl'
	
	EXEC (@SQL)
	-- update the comment to show how the BF was generated
 	SET @SQL = 'UPDATE ' + @FEES_TABLE_NAME + ' SET COMMENTS = (SELECT NOTES FROM LicenseRenew_BalanceForwards 
		WHERE LicenseRenew_BalanceForwards.License_No = ' + @FEES_TABLE_NAME + '.LICENSE_NO) WHERE ' + @FEES_TABLE_NAME + '.CODE = ''' + @BALFWDCODE + ''' '
	EXEC (@SQL)

	IF @INLIVE = 1
	BEGIN
		SET @FEES_TABLE_NAME = 'License2_Fees'
		-- add the Calc_Value
		ALTER TABLE License2_Fees DROP COLUMN Calc_Value
	END

    	
END



GO
