USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_ProjectCustomFieldData]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[usp_Arvada_ProjectCustomFieldData]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_ProjectCustomFieldData]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_Arvada_ProjectCustomFieldData]
	@projectNo VARCHAR(30)
as
/**********************************************************************************************************************
* Creator:      	 
* Date:         	 
* Title:			 
* Description:  	 
* Examples:    
			EXECUTE dbo.usp_Arvada_ProjectCustomFieldData 'FDP2015-0017'
					 
* Modifications:  	
  Developer Name		Date		Description
  ------------------	----------	----------------------------------------------------------- 
*
**********************************************************************************************************************/

set nocount on
 
 --------------------------
 --- Declare Variables   --
 --------------------------
 DECLARE 
	@projectType VARCHAR(50),
	@s INT,
	@e INT,
	@fieldName VARCHAR(50)	,
	@sql NVARCHAR(MAX)
 
 --------------------------
 --- Create Temp Tables  --
 --------------------------

	 
 --------------------------
 --- Body of Procedure   --
 --------------------------

SELECT @projectType = projectType
FROM dbo.PROJECT_MAIN
WHERE PROJECT_NO = @projectNo

CREATE TABLE #CustomFields
(
	GroupName VARCHAR(50),
	TypeName VARCHAR(50),
	ScreenName VARCHAR(50),
	DataField VARCHAR(50),
	Caption VARCHAR(150),
	DateFieldConverted VARCHAR(50),
	DataValue NVARCHAR(MAX),
	rid INT IDENTITY(1,1)
)

INSERT INTO  #CustomFields
(
	GroupName ,
	TypeName ,
	ScreenName ,
	DataField ,
	Caption ,
	DateFieldConverted
)
SELECT  
	pt.groupname,
	pt.TypeName,
	screens.DataField AS ScreenName,
	pt.DataField,
	pt.Caption,
	CASE WHEN CHARINDEX('.', pt.datafield) > 0 THEN 'u.[' + SUBSTRING(pt.dataField,CHARINDEX('.', pt.datafield)+1, LEN(pt.datafield)) + ']' ELSE 'm.[' + pt.dataField + ']' END
FROM dbo.Prmry_TypesUDF pt 
INNER JOIN dbo.Prmry_TypesUDF Screens
	ON Screens.Screen = pt.Screen
	AND Screens.UDF = 'TITL'
	AND Screens.groupname = pt.groupname
	AND Screens.TypeName = pt.typename
WHERE 
	pt.UDF != 'TITL'
and	
	pt.groupname = 'projects'
AND 
	pt.TypeName = @projectType
ORDER BY 
	pt.groupname, 
	pt.typename,  
	pt.ORDERID

SELECT
	@s = MIN(rid),
	@e = MAX(rid)
FROM #CustomFields

WHILE @s <= @e
BEGIN
	SELECT @fieldName = DateFieldConverted
	FROM #CustomFields
	WHERE rid = @s

	SET @sql = '
	UPDATE f 
	SET DataValue = (SELECT TOP 1 '+@fieldName+' FROM dbo.project_udf u inner join dbo.project_main m on m.projecT_no = u.project_no WHERE u.PROJECT_NO = '''+@projectNo+''')
	FROM #CustomFields f
	WHERE rid = ' + CONVERT(VARCHAR(100), @s)+'
	'

	EXEC(@sql)


	SET @s = @s+1
end

SELECT 
	GroupName ,
	TypeName ,
	ScreenName ,
	DataField ,
	Caption ,
	DateFieldConverted ,
	DataValue
FROM #CustomFields
ORDER BY rid

 
GO
