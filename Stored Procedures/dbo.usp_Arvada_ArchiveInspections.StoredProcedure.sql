USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_ArchiveInspections]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[usp_Arvada_ArchiveInspections]
GO
/****** Object:  StoredProcedure [dbo].[usp_Arvada_ArchiveInspections]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_Arvada_ArchiveInspections]
	@Permit_No varchar(50) 
	
as
/**********************************************************************************************************************
* Creator:      	 
* Date:         	 
* Title:			 
* Description:  	 
* Examples:    
			EXECUTE dbo.usp_Arvada_ArchiveInspections 'COMM14-00167', '01/01/2016'
					 
* Modifications:  	
  Developer Name		Date		Description
  ------------------	----------	----------------------------------------------------------- 
*
**********************************************************************************************************************/

set nocount on
 
 --------------------------
 --- Declare Variables   --
 --------------------------

 
 --------------------------
 --- Create Temp Tables  --
 --------------------------

	 
 --------------------------
 --- Body of Procedure   --
 --------------------------

 
select 
	i.permit_no,
	i.InspectionType,
	i.RESULT,
	r.UserName as Inspector,
	i.REMARKS,
	i.SCHEDULED_DATE,
	i.SCHEDULED_TIME,
	i.COMPLETED_DATE,
	stuff(
			(	select CHAR(13) + pn.notes 
				from dbo.Prmry_Notes AS pn  
				WHERE   pn.SubGroupRecordID = i.recordid
				for xml path(''), root('MyString'), type 
			).value('/MyString[1]','nvarchar(max)') ,1,1,''
		  ) as notes
from dbo.Permit_Inspections i
left join dbo.PERMIT_INSPECTIONS_UDF u
	on u.PERMIT_NO = i.recordid
left join dbo.prmry_users r
	on r.userid = i.inspector
where 
	i.permit_no = @Permit_No 
order by 
	InspectionType,
	scheduled_Date  
GO
