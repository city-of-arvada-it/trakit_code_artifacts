USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ModelHomeCategories_Populate]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_ModelHomeCategories_Populate]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_ModelHomeCategories_Populate]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
					  					  					  
CREATE PROCEDURE [dbo].[tsp_T9_ModelHomeCategories_Populate]
        @List as PopulateMdlHomeValCat readonly,  
		@userid as varchar(50)
                       
AS
BEGIN

	DECLARE @SQL Varchar(8000)
	Declare @date varchar(255) = getdate()

	truncate table Prmry_ModelHomeValuationCategory

	INSERT INTO Prmry_ModelHomeValuationCategory (ValuationCategory, ValuationCode, UnitCost, ORDERID)
	SELECT ValuationCategory,ValuationCode,UnitCost, ORDERID FROM @List


	set @SQL = 'Update Prmry_ModelHomeValuationCategory set LAST_MODIFIED = ''' + @date + ''';'
	exec(@sql)	
	
	set @SQL = 'Update Prmry_ModelHomeValuationCategory set LAST_MODIFIED_BY = ''' + @userid + ''';'
	exec(@sql)			

END





GO
