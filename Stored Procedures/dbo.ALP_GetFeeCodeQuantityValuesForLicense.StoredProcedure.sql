USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[ALP_GetFeeCodeQuantityValuesForLicense]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[ALP_GetFeeCodeQuantityValuesForLicense]
GO
/****** Object:  StoredProcedure [dbo].[ALP_GetFeeCodeQuantityValuesForLicense]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =====================================================================================================
-- Author:		Erick Hampton
-- Create date: Feb 20, 2017
-- Description:	Get the quantity for each fee code / subfee item for a specified license number 
-- Notes:		NULL or varying quantity values for a fee code are not included
-- =====================================================================================================
CREATE PROCEDURE [dbo].[ALP_GetFeeCodeQuantityValuesForLicense]

	@LicenseNumber varchar(30) = NULL

AS
BEGIN
	
	select [FeeCode], max([Quantity]) [Quantity] -- using max(), but could be min() or anything that reports the lone value
	from 
		(
		select CODE [FeeCode], Quantity from LICENSE2_FEES where Quantity is not null and LICENSE_NO=@LicenseNumber
		union
		select ITEM [FeeCode], Quantity from LICENSE2_SUBFEES where Quantity is not null and LICENSE_NO=@LicenseNumber
		) a
	group by [FeeCode]
	having count([Quantity]) = 1 -- ignore multiple fee codes with different quantity values

END

GO
