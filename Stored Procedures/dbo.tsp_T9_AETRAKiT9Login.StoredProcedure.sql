USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AETRAKiT9Login]    Script Date: 2/2/2022 12:25:32 PM ******/
DROP PROCEDURE [dbo].[tsp_T9_AETRAKiT9Login]
GO
/****** Object:  StoredProcedure [dbo].[tsp_T9_AETRAKiT9Login]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*
	INSERTS OR UPDATES TRAKiT9_LOGIN Table value for the corresponding UID
*/
-- =====================================================================
-- Created By: 
--	HAN - 11/11/13
-- =====================================================================  
CREATE Procedure [dbo].[tsp_T9_AETRAKiT9Login](
@UID VARCHAR(50),
@LoginID VARCHAR(50),
@SessionTimeout INT
)

AS
BEGIN
	DELETE from TRAKIT9_login where UID in(select UID from TRAKIT9_login where UID= @UID group by UID having  count(*) >1)

IF EXISTS (select * from TRAKIT9_login where UID = @UID)
	UPDATE  TRAKIT9_LOGIN SET loginID = @LoginID, pingTime = GETDATE(), LastLogin = GETDATE(), loginExpireDate = DATEADD(mi,@SessionTimeout ,GETDATE()) where UID = @UID
ELSE
	INSERT INTO  TRAKIT9_LOGIN ([UID], loginID, pingTime, LastLogin, loginExpireDate) values(@UID ,@LoginID , GETDATE(), GETDATE(), DATEADD(mi,@SessionTimeout ,GETDATE()))
END

GO
