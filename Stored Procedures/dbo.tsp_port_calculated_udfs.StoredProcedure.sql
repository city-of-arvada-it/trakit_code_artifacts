USE [CRW_TEST]
GO
/****** Object:  StoredProcedure [dbo].[tsp_port_calculated_udfs]    Script Date: 2/2/2022 12:25:33 PM ******/
DROP PROCEDURE [dbo].[tsp_port_calculated_udfs]
GO
/****** Object:  StoredProcedure [dbo].[tsp_port_calculated_udfs]    Script Date: 2/2/2022 12:25:33 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[tsp_port_calculated_udfs] 
AS
BEGIN
	Declare @GroupName varchar(60)
	Delete from CalculatedColumnUdfs

	Set @GroupName = 'PERMITS'
	Insert into CalculatedColumnUdfs (ColumnName, GroupName, Formula)
	SELECT cc.name as ColumnName, @GroupName, cc.definition as Formula
	FROM sys.Computed_columns as cc
	join sys.tables as ac on ac.object_id = cc.object_id
	where ac.name = 'permit_udf'

	Set @GroupName = 'AEC'
	Insert into CalculatedColumnUdfs (ColumnName, GroupName, Formula)
	SELECT cc.name as ColumnName, @GroupName, cc.definition as Formula
	FROM sys.Computed_columns as cc
	join sys.tables as ac on ac.object_id = cc.object_id
	where ac.name = 'aec_udf'
 
	Set @GroupName = 'LICENSE2'
	Insert into CalculatedColumnUdfs (ColumnName, GroupName, Formula)
	SELECT cc.name as ColumnName, @GroupName, cc.definition as Formula
	FROM sys.Computed_columns as cc
	join sys.tables as ac on ac.object_id = cc.object_id
	where ac.name = 'license2_udf'	

	Set @GroupName = 'PROJECTS'
	Insert into CalculatedColumnUdfs (ColumnName, GroupName, Formula)
	SELECT cc.name as ColumnName, @GroupName, cc.definition as Formula
	FROM sys.Computed_columns as cc
	join sys.tables as ac on ac.object_id = cc.object_id
	where ac.name = 'project_udf'

	Set @GroupName = 'VIOLATIONS'
	Insert into CalculatedColumnUdfs (ColumnName, GroupName, Formula)
	SELECT cc.name as ColumnName, @GroupName, cc.definition as Formula
	FROM sys.Computed_columns as cc
	join sys.tables as ac on ac.object_id = cc.object_id
	where ac.name = 'Case_Violations_UDF'

	Set @GroupName = 'GEO'
	Insert into CalculatedColumnUdfs (ColumnName, GroupName, Formula)
	SELECT cc.name as ColumnName, @GroupName, cc.definition as Formula
	FROM sys.Computed_columns as cc
	join sys.tables as ac on ac.object_id = cc.object_id
	where ac.name = 'Geo_UDF'

END 
GO
